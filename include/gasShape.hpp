// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_GAS_SHAPE_H
#define FARGOCA_GAS_SHAPE_H

#include <iostream>
#include <nlohmann/json_fwd.hpp>
#include <numbers>

#include "boost/property_tree/ptree.hpp"

#include "precision.hpp"
#include "fundamental.hpp"
#include "h5io.hpp"

namespace fargOCA {
  struct GasShape {
    /// \brief Disk scale height/radius ratio at code units's R0
    /// The scale height is the increase in altitude where d(density)/dz = e.
    /// https://en.wikipedia.org/wiki/Scale_height
    real aspectRatio;
    /// \brief the radial section at which we consider there is gas.
    struct Radius {
      real min, max;
    } radius;
    /// \brief Bad thing will happen if this is not 2PI
    static constexpr real sector = 2*PI;
    real opening;
    /// \brief We only consider the half of the disk.
    bool half;
    
    GasShape(real ratio, 
             real rMin, real rMax, 
             real diskOpening, bool halfDisk)
      : aspectRatio(ratio),
        radius({rMin, rMax}),
        opening(diskOpening),
        half(halfDisk) {}
    GasShape(nlohmann::json const& j);
    
    static GasShape diff(GasShape const& s1, GasShape const& s2);

    /// \brief For testing only.
    explicit GasShape() : GasShape(1, 42, 56, 0.001, false) {}

    void dump(std::ostream& out, int tab=0) const;
    void writeH5(HighFive::Group& grp) const;
    void readH5(HighFive::Group const& grp);
  };
  
  std::ostream& operator<<(std::ostream& out, GasShape const& s);

  GasShape diff(GasShape const& s1, GasShape const& s);
  real max(GasShape const& s);

  void to_json(nlohmann::json&, GasShape const&);
  void from_json(nlohmann::json const&, GasShape&);
  
}

#endif
