// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_CONFIG_PROPERTIES_H
#define FARGOCA_CONFIG_PROPERTIES_H

#include "boost/property_tree/string_path.hpp"

namespace fargOCA {
  namespace configProperties {
    inline char separator() { return '.';}
    inline std::string str(boost::property_tree::path const& p) { return p.dump(); }
    inline std::string str(std::string const& s) { return s; }
    template<typename... Ts>  
    inline std::string str(boost::property_tree::path const& p, Ts... ts) { return p.dump()+separator()+str(ts...); }
    template<typename... Ts> 
    inline std::string str(std::string const& s, Ts... ts) { return s+separator()+str(ts...); }

    boost::property_tree::path const ACCRETE_ONTO_PLANET = "AccreteOntoPlanet";
    boost::property_tree::path const ACCRETION_TIME = "AccretionTime";
    boost::property_tree::path const ACCRETION_TIME_INVERSE = "AccretionTimeInverse";
    boost::property_tree::path const ACTIVE_DENSITY = "ActiveDensity";
    boost::property_tree::path const ADIABATIC     = "Adiabatic";
    boost::property_tree::path const ADIABATIC_EOS = "AdiabaticEoS";
    boost::property_tree::path const ALPHA         = "Alpha";
    boost::property_tree::path const ARTIFICIAL    = "Artificial";
    boost::property_tree::path const ASPECT_RATIO  = "AspectRatio";
    boost::property_tree::path const BEGIN_TIME    = "BeginTime";
    boost::property_tree::path const BETA          = "Beta";
    boost::property_tree::path const BOUNDARIES = "Boundaries";
    boost::property_tree::path const CAVITY = "Cavity";
    boost::property_tree::path const CENTRAL_BODY = "CentralBody";
    boost::property_tree::path const CENTER_IN_CELL = "CenterInCell";
    boost::property_tree::path const CFL_SECURITY = "CFLSecurity";
    boost::property_tree::path const CHANGE = "Change";
    boost::property_tree::path const CODE_UNITS = "CodeUnits";
    boost::property_tree::path const CONFIG = "Config";
    boost::property_tree::path const COOLING = "Cooling";
    boost::property_tree::path const DENSITY = "Density";
    boost::property_tree::path const DESCRIPTION_TAG = "Description";
     boost::property_tree::path const DISK = "Disk";
    boost::property_tree::path const DISTANCE = "Distance";
    boost::property_tree::path const DRAGGING_COEF = "DraggingCoef";
    boost::property_tree::path const DUST_TO_GAS = "DustToGas";
    boost::property_tree::path const ECCENTRICITY = "Eccentricity";
    boost::property_tree::path const ENGINE = "Engine";
    boost::property_tree::path const END = "End";
    boost::property_tree::path const END_MASS = "EndMass";
    boost::property_tree::path const END_TIME = "EndTime";
    boost::property_tree::path const EPSILON = "Epsilon";
    boost::property_tree::path const ENERGY_TIME_STEP = "EnergyTimeStep";
    boost::property_tree::path const EXCLUDE_HILL = "ExcludeHill";
    boost::property_tree::path const FEEL = "Feel";
    boost::property_tree::path const FILTER = "Filter";
    boost::property_tree::path const FINAL_MASS = "FinalMass";
    boost::property_tree::path const FLARING_INDEX = "FlaringIndex";
    boost::property_tree::path const FLAT = "Flat";
    boost::property_tree::path const FULL_ENERGY_EOS = "FullEnergyEoS";
    boost::property_tree::path const GAS_SHAPE = "GasShape";
    boost::property_tree::path const GRID = "Grid";
    boost::property_tree::path const GUIDING_CENTER = "GuidingCenter";
    boost::property_tree::path const GUIDING_PLANET = "GuidingPlanet";
    boost::property_tree::path const HALF = "Half";
    boost::property_tree::path const HILL_CUT_FACTOR = "HillCutFactor";
    boost::property_tree::path const HILL_RADIUS = "HillRadius";
    boost::property_tree::path const INCLINATION = "Inclination";
    boost::property_tree::path const INDEX = "Index";
    boost::property_tree::path const INDIRECT_FORCES = "IndirectForces";
    boost::property_tree::path const INITIAL_MASS = "InitialMass";
    boost::property_tree::path const INIT = "Init";
    boost::property_tree::path const LABEL = "Label";
    boost::property_tree::path const LABELS = "Labels";
    boost::property_tree::path const LAYERS = "Layers";
    boost::property_tree::path const LONGITUDE_OF_NODE = "LongitudeOfNode";
    boost::property_tree::path const LONGITUDE_OF_PERICENTER = "LongitudeOfPericenter";
    boost::property_tree::path const LUMINOSITY = "Luminosity";
    boost::property_tree::path const MASS = "Mass";
    boost::property_tree::path const MAX = "Max";
    boost::property_tree::path const MEAN_LONGITUDE = "MeanLongitude";
    boost::property_tree::path const MEAN_MOLECULAR_WEIGHT = "MeanMolecularWeight";
    boost::property_tree::path const MINIMUM = "Minimum";
    boost::property_tree::path const MIN = "Min";
    boost::property_tree::path const MRI_ALPHA = "MRIAlpha";
    boost::property_tree::path const MRI_TEMPERATURE = "MRITemperature";
    boost::property_tree::path const MRI_TO_DZ = "MRIToDz";
    boost::property_tree::path const NAME = "Name";    
    boost::property_tree::path const NB_STEP = "NbStep";
    boost::property_tree::path const OMEGA = "Omega";
    boost::property_tree::path const OPACITY = "Opacity";
    boost::property_tree::path const OPACITY_LAW = "OpacityLaw";
    boost::property_tree::path const OPENING = "Opening";
    boost::property_tree::path const ORBITAL_ELEMENTS = "OrbitalElements";
    boost::property_tree::path const OTHERS = "Others";
    boost::property_tree::path const OUTER_TEMPERATURE = "OuterTemperature";
    boost::property_tree::path const OUTPUT = "Output";
    boost::property_tree::path const PHYSIC = "Physic";
    boost::property_tree::path const PLANETARY_SYSTEM = "PlanetarySystem";
    boost::property_tree::path const PLANET_CONFIG = "PlanetConfig";
    boost::property_tree::path const PLANETS = "Planets";
    boost::property_tree::path const RADII   = "Radii";
    boost::property_tree::path const RADIAL  = "Radial";
    boost::property_tree::path const RADIUS  = "Radius";
    boost::property_tree::path const RADIATIVE = "Radiative";
    boost::property_tree::path const RATE = "Rate";
    boost::property_tree::path const RATIO = "Ratio";
    boost::property_tree::path const REFERENTIAL = "Referential";
    boost::property_tree::path const RELEASE = "Release";
    boost::property_tree::path const RESOLUTION = "Resolution";
    boost::property_tree::path const R_STAR = "RStar";
    boost::property_tree::path const SECTOR  = "Sector";
    boost::property_tree::path const SECTORS = "Sectors";
    boost::property_tree::path const SEMI_MAJOR_AXIS = "SemiMajorAxis";
    boost::property_tree::path const SHADOW_ANGLE = "ShadowAngle";
    boost::property_tree::path const SIZE = "Size";
    boost::property_tree::path const SIZES = "Sizes";
    boost::property_tree::path const SLOPE = "Slope";
    boost::property_tree::path const SMOOTHING = "Smoothing";
    boost::property_tree::path const SOLID_ACCRETION = "SolidAccretion";
    boost::property_tree::path const SOLVER = "Solver";
    boost::property_tree::path const SPACING = "Spacing";
    boost::property_tree::path const STAR = "Star";
    boost::property_tree::path const STAR_ACCRETION = "StarAccretion";
    boost::property_tree::path const STAR_RADIATION = "StarRadiation";
    boost::property_tree::path const START = "Start";
    boost::property_tree::path const STELLAR = "Stellar";
    boost::property_tree::path const STEP = "Step";
    boost::property_tree::path const STEPS = "Steps";    
    boost::property_tree::path const STEPS_PER_OUTPUT = "StepsPerOutput";
    boost::property_tree::path const TAPER = "Taper";
    boost::property_tree::path const TEMPERATURE = "Temperature";
    boost::property_tree::path const TIMESCALE = "Timescale";
    boost::property_tree::path const TIME_STEP = "TimeStep";
    boost::property_tree::path const TRACKERS = "Trackers";
    boost::property_tree::path const TRANSITION_RADIUS = "TransitionRadius";
    boost::property_tree::path const TRANSPORT = "Transport";
    boost::property_tree::path const T_STAR = "TStar";
    boost::property_tree::path const TYPE = "Type";
    boost::property_tree::path const VALUE = "Value";
    boost::property_tree::path const VELOCITIES = "Velocities";
    boost::property_tree::path const VISCOSITY = "Viscosity";
    boost::property_tree::path const WIDTH = "Width";
    boost::property_tree::path const WIND = "Wind";
    boost::property_tree::path const Z_BOUNDARY_TEMPERATURE = "ZBoundaryTemperature";
    
  };
}

#endif // FARGOCA_CONFIG_PROPERTIES_H
