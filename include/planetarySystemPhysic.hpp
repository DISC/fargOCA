// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_PLANETARY_SYSTEM_PHYSIC_H
#define FARGOCA_PLANETARY_SYSTEM_PHYSIC_H

#include <iostream>
#include <map>
#include <variant>

#include <nlohmann/json_fwd.hpp>

#include "precision.hpp"
#include "mathAddOns.hpp"
#include "fundamental.hpp"
#include "h5io.hpp"
#include "config.hpp"

namespace fargOCA {
  class CodeUnits;
  
  struct OrbitalElements {
    real semiMajorAxis;
    real eccentricity;
    real inclination;
    real meanLongitude;
    real longitudeOfPericenter;
    real longitudeOfNode;
  };

  std::ostream& operator<<(std::ostream& out, OrbitalElements const& oe);

  struct PlanetarySystemPhysic {

    struct Planet {
      std::string name;
      OrbitalElements orbitalElements;
      
      std::optional<real> accretionTime;
      bool feelDisk;
      bool feelOthers;
      
      struct Luminosity {
        /// \brief User specified luminosity contribution of solid accretion onto the planet in erg/s
        real       userSolidAccretion;
	
	Luminosity(real acc);
	Luminosity(HighFive::Group const& grp);
	Luminosity() = default;
	
	void dump(std::ostream& out, int tab = 0) const;
        void writeH5(HighFive::Group& grp) const;
      };
      
      std::optional<Luminosity> luminosity;
      
      struct MassTaper {
        real initialMass;
        real finalMass;
        real beginTime;
        real endTime;

        MassTaper(real i, real f, real b, real e);
        MassTaper(HighFive::Group const& grp);
        MassTaper() = default;

        bool before(real time) const { return time <= beginTime; }
        bool active(real time) const { return time > beginTime && time <= endTime; }
        bool after(real time)  const { return time > endTime; }
        real massAt(real time) const;

        void dump(std::ostream& out, int tab = 0) const;
        void writeH5(HighFive::Group& grp) const;
        void readH5(HighFive::Group const& grp);
      };
      using Mass = std::variant<real,MassTaper>;
      Mass mass;
      
      real initialMass() const;
      /// \brief Luminosity contribution of solid accretion onto the planet in code unit
      std::optional<real> solidAccretion(CodeUnits const& units) const;

      std::optional<real> radius;
      std::optional<real> hillRadius;

      Planet(std::string nm,
             std::variant<real,MassTaper> mass,
             OrbitalElements elements,
             std::optional<real> acc,
             bool fdisk,
             bool fothers,
             std::optional<Luminosity> luminosity = std::nullopt,
             std::optional<real> radius           = std::nullopt,
             std::optional<real> hillRadius       = std::nullopt);
      Planet(HighFive::Group grp);
      Planet() = default;
      
      void dump(std::ostream& out, int tab) const;
      void writeH5(HighFive::Group& grp) const;
    };

    std::map<std::string, Planet> planets;
    Planet const& planet(std::string name) const { return planets.find(name)->second; }    
    
    bool excludeHill;
    bool accreteOntoPlanets;
    struct Engine {
      std::string name;
      Config  config;
      
      void dump(std::ostream& out, int tab) const;
      void writeH5(HighFive::Group& grp) const;
      void readH5(HighFive::Group const& grp);
      void exportJsonCfg(boost::property_tree::ptree& cfg) const;
    };
    Engine engine;
    void dump(std::ostream& out, int tab = 0) const;
    void writeH5(HighFive::Group grp) const;
    void readH5(HighFive::Group const& grp);
    
    PlanetarySystemPhysic( std::map<std::string, Planet> const& plts,
                           bool ex, bool acc, Engine const& engine);
    PlanetarySystemPhysic(HighFive::Group grp);
    PlanetarySystemPhysic() = default;
  };

  void to_json(nlohmann::json& j, PlanetarySystemPhysic const& p);
  void from_json(nlohmann::json const& j, PlanetarySystemPhysic& p);

  void to_json(nlohmann::json& j, PlanetarySystemPhysic::Engine const& p);
  void from_json(nlohmann::json const& j, PlanetarySystemPhysic::Engine& p);
  
  void to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet const& p);
  void from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet& p);

  void to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet::Luminosity const& p);
  void from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet::Luminosity& p);

  void to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet::MassTaper const& p);
  void from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet::MassTaper& p);

  void to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet::Mass const& p);
  void from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet::Mass& p);

}

#endif
