// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>
#include <sysexits.h>

int
main(int argc, char *argv[]) {
  namespace fs = std::filesystem;
  std::string utils_dir = fs::path(argv[0]).parent_path().native();
  std::cerr << argv[0] << " is obsolete, please use " << (utils_dir+"/disk_load_config") 
            << " instead. Bye.\n";
  return EX_USAGE;
}
