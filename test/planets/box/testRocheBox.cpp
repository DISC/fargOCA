// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "precision.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "gridDispatch.hpp"
#include "gasShape.hpp"
#include "grid.hpp"
#include "log.hpp"
#include "environment.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"

using namespace fargOCA;

namespace nl = nlohmann;
using json = nlohmann::json;

int
main(int argc, char* argv[]) {
  Environment env;
  auto usage = [=]{ std::cout << argv[0] << " <config.json>\n"; };
  if (argc == 0) {
    usage();
    return -1;
  }
  std::ifstream config_file{argv[1]};
  if (!config_file) {
    return EX_NOINPUT;
  }
  json config {json::parse(config_file)};
  if (config.is_array()) {// depends on the compiler ?
    config = config[0];
  }
  auto  physic {config.template get<std::shared_ptr<DiskPhysic const>>()};
  auto  grid   {Grid::make(physic->shape, config["disk"]["grid"])};
  auto  dispatch   {GridDispatch::make(env.world(), *grid)};
  auto  disk       {Disk::make(*physic, *dispatch)};
  auto& jupiter {disk->system()->planet("Jupiter")};
  
  std::ostream& out {fargOCA::log(env.world(), std::cout)};
  
  out << "Gas shape: " << disk->physic().shape << '\n';
  disk->dispatch().dumpMpiLayout();

  for (real r : { 2, 3, 4 }) {
    env.world().barrier();
    jupiter.setPosition({r,0,0});
    auto box { getRocheBox(*disk, jupiter)};
    std::ostringstream log;
    out << "Jupiter position: " << jupiter.position() << std::endl;
    env.world().barrier();    
    for(int proc = 0; proc < env.world().size(); ++proc) {
      env.world().barrier();
      if (proc == env.world().rank()) {
        log << "On proc " << proc << " Jupiter Roche box: ";
        if (box) {
          log << "goes from " << box->first << " to " << box->second;
        } else {
          log << " is absent";
        }
        log << std::endl;
      }
    }
    std::string msg;
    fmpi::reduce( env.world(), log.str(),  msg, std::plus<std::string>(), 0);
    out << msg << std::endl;
  }
}

