// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_COMMUNICATOR_HPP
#define BOOST_NOOPMPI_COMMUNICATOR_HPP

#include <utility>
#include <iterator>

#include <boost/assert.hpp>
#include <optional>
#include <boost/static_assert.hpp>

#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>
#include <boost/noopmpi/detail/no_return.hpp>
#include <boost/noopmpi/status.hpp>
#include <boost/noopmpi/request.hpp>

namespace boost { namespace noopmpi {

/** @brief A constant representing "any process." */
    constexpr int any_source = 0;

/** @brief A constant representing "any tag." */
    constexpr int any_tag = 0;

/**
 * @brief Enumeration used to describe how to adopt a C @c MPI_Comm into
 * a Boost.MPI communicator.
 * We do not care about the the actual meaning.
 */
enum comm_create_kind { comm_duplicate, comm_take_ownership, comm_attach };

class group;
class intercommunicator;
class graph_communicator;
class cartesian_communicator;

/**
 * @brief A dummy communicator that does not do anything.
 *
 * There can be only one communicator, and no underlying C communicator.
 * The implementation will be simplified accordingly and calls failling 
 * that assumption will aborts.
 */
class BOOST_MPI_DECL communicator
{
public:
  /**
   * @brief Build a new Boost.MPI one process communicator.
   * \param valid true if not undefined
   */
  communicator(bool valid = true) : myValid(valid) {}
  
  virtual ~communicator() {}

  template<typename C, typename... TS>
  /**
   * Catch all builder.
   * @p comm.
   */
  communicator(C, TS... unused) : communicator(true) {}

  /**
   * Can't be called. There is no other communicator. 
   */
  communicator(const communicator& /* comm */, const boost::noopmpi::group& /* subgroup */) { detail::no_return(); }

  /**
   * @brief Determine the rank of the executing process in a
   * communicator.
   *
   * This routine is equivalent to @c MPI_Comm_rank.
   *
   *   @returns The rank of the process in the communicator, which
   *   will be a value in [0, size())
   */
  int rank() const { return 0; }

  /**
   * @brief Determine the number of processes in a communicator.
   *
   * This routine is equivalent to @c MPI_Comm_size.
   *
   *   @returns The number of processes in the communicator.
   */
  int size() const { return 1; }

  /**
   * This routine constructs a new group whose members are the
   * processes within this communicator. Equivalent to
   * calling @c MPI_Comm_group.
   */
  boost::noopmpi::group group() const;

  /**
   *  @brief Send data to another process.
   *
   *  There is no other proccess.
   */
  template<typename... TS> void send(TS... unused) const { detail::no_return(); }
  /**
   * @brief Receive data from a remote process.
   *
   *  There is no other proccess.
   */
  template<typename... TS> status recv(TS... unused) const { return detail::no_return<status>(); }

  /** @brief Send a message to remote process and receive another message
   *  from another process.
   * 
   *  There is no other proccess.
   */
  template<typename... TS> status sendrecv(TS... unused) const { return detail::no_return<status>(); }
  
  /**
   *  @brief Send a message to a remote process without blocking.
   *
   *  There is no other proccess.
   */
  template<typename... TS> request isend(TS... /* unused */) const { return detail::no_return<request>(); }

  /**
   *  @brief Prepare to receive a message from a remote process.
   *
   *  There is no other process.
   */
  template<typename... TS> request irecv(TS... /* unused */) const { return detail::no_return<request>(); }

  /**
   * @brief Waits until a message is available to be received.
   *
   * There is no message.
   */
  status probe(int /* source = 0 */, int /* tag = 0 */) const { return detail::no_return<status>(); }

  /**
   * @brief Determine if a message is available to be received.
   *
   * There is no message.
   */
  std::nullopt_t iprobe(int /* source = 0 */, int /* tag = 0 */) const { return std::nullopt; }

#ifdef barrier
  // linux defines a function-like macro named "barrier". So, we need
  // to avoid expanding the macro when we define our barrier()
  // function. however, some c++ parsers (doxygen, for instance) can't
  // handle this syntax, so we only use it when necessary.
  void (barrier)() const {}
#else
  /**
   * @brief wait for all processes within a communicator to reach the
   * barrier.
   *
   * but we're all alone.
   */
  void barrier() const {}
#endif

  /** @brief determine if this communicator is valid for
   * communication.
   *
   * evaluates @c true in a boolean context if this communicator is
   * valid for communication, i.e., does not represent
   * mpi_comm_null. otherwise, evaluates @c false.
   */
  operator bool() const { return myValid; }

  /**
   * @brief access the mpi communicator associated with a boost.mpi
   * communicator.
   */
  template<typename T> operator T() const { return T(); }

  /**
   * Split the communicator into multiple, disjoint communicators
   * each of which is based on a particular color.
   * 
   * There is only one communicator.
   */
  communicator split(int  /* color */) const  { return *this; }
  communicator split(int color, int /* key */) const { return split(color); }

  /**
   * Determine if the communicator is in fact an intercommunicator
   * and, if so, return that intercommunicator.
   *
   * But there is only one communicator.
   */
  std::optional<intercommunicator> as_intercommunicator() const;

  /**
   * Determine if the communicator has a graph topology and, if so,
   * return that @c graph_communicator. Even though the communicators
   * have different types, they refer to the same underlying
   * communication space and can be used interchangeably for
   * communication.
   *
   * @returns an @c optional containing the graph communicator, if this
   * communicator does in fact have a graph topology. Otherwise, returns
   * an empty @c optional.
   */
   std::optional<graph_communicator> as_graph_communicator() const;

  /**
   * Determines whether this communicator has a Graph topology.
   */
  bool has_graph_topology() const { return false; }

  /**
   * Determine if the communicator has a cartesian topology and, if so,
   * return that @c cartesian_communicator. Even though the communicators
   * have different types, they refer to the same underlying
   * communication space and can be used interchangeably for
   * communication.
   *
   * @returns an @c optional containing the cartesian communicator, if this
   * communicator does in fact have a cartesian topology. Otherwise, returns
   * an empty @c optional.
   */
  std::nullopt_t as_cartesian_communicator() const { return std::nullopt; }

  /**
   * Determines whether this communicator has a Cartesian topology.
   */
  bool has_cartesian_topology() const { return false; }

  /** 
   * @brief Abort the only task of the group.
   */
  void abort(int errcode) const;

 protected:
  bool myValid;
};

/**
 * @brief Determines whether two communicators are valid.
 *
 */
inline bool operator==(const communicator& comm1, const communicator& comm2)
{
  // Either world or null
  return bool(comm1) && bool(comm2);
}

/**
 * @brief True if only one is valid
 */
inline bool operator!=(const communicator& comm1, const communicator& comm2)
{
  return bool(comm1) || bool(comm2);
}

}} 

#endif // BOOST_NOOPMPI_COMMUNICATOR_HPP
