We provide a set of examples covering a quite large number of disk and planets cases. The examples are used to test the code therefore the total integration time
is short and only one output file is produced. However one can easily use the config.info files provided and increase NbStep to obtain a longer simulation (as well as modify  StepsPerOutput according to the desired sampling of the hydro fields)

* **20EM_planet_isothermal_disk** : locally isothermal Equation of State
   * **Planet** : 20 Earth mass planet embedded in a 3D disk without masstaper
   * **Output** : 1 output  file after 1/10 of orbit
   * **Transport**: fast, it is recommended to use fast (i.e. use FARGO algorithm) when the azimuthal grid spacing is regular
   * **Boundaries**: reflecting in both radial and vertical direction, commonly used for small mass planets (below 30 Earth masses)
* **20EM_planet_non_uniform_grid** : locally isothermal Equation of State
   * **Planet** : 20 Earth mass planet embedded in a 3D disk without masstaper, the grid is nonuniform, resolution is highest close to the planet, the configuration file provides the name of the files containing the grid
   * **Output** : 1 output  file after 1/100 of orbit
   * **Transport**: standard, we can not use FARGO algorithm with non uniform grid geometry in azimuth
   * **Boundaries**: reflecting in both radial and vertical direction, commonly used for small mass planets (below 30 Earth masses)
* **full_energy_disk**: evolution of a disk   without planet (r-z disk thanks to azimuthal symmetry) with Full energy equation of state, useful to find thermal equilibrium before expanding the disk in 3D and inserting planet(s)
  * **FullEnergyEos**:  true to have a disk description with thermal effects
  * **Output**:  1 output after 10 orbits at r=1 (PhysicalTime=100*0.628)
  * **Boundaries**: reflecting in both radial and vertical direction, recommended when disk is studied without planets like in this case
* **star_radiation_disk**: similar to the previous example considering also the radiation emitted from the star. This example is provided in a restart mode, this configuration requires a physical time of about 30 orbits at r=1 to reach a flared disk in equilibrium 
  * **Half**:  true, simulations with StarRadiation require a large radial and vertical domain to obtain a flared disk, it is therefore recommended to simulate only half of the disk (the code has a reflecting  condition at midplane in this case)
  * **FullEnergyEos**:  true to have a disk description with thermal effects
    * **StarRadiation**: true to consider also the radiation from the star
      * **Epsilon**: 5.e-4 this value is larger than typically used values 5.e-7-1.e-6 but it  is recommended for a first approach  to equilibrium, then one can refine by reducing epsilon 
  * **Output**: 1 output after 1/10 of orbit at r=1 (PhysicalTime=0.628) 

                     
