// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <string>

namespace fargOCA {
  namespace options {
    // longing for C++20's constexpr
    inline std::string full(std::string fullopt, char shortopt) { return fullopt + "," + shortopt; }
    inline char const* str(std::string const& option) { return option.c_str(); }

    static std::string const CONFIG_DEFAULTS = "config-defaults";
    static std::string const CD_ENABLE       = "enable";    
    static std::string const CD_DISABLE      = "disable";    
    static std::string const CD_REPORT       = "report";
    static std::string const TRACE           = "trace";
    static std::string const GAS_FROM        = "gas-from";
    static std::string const HELP            = "help";
    static std::string const HELP_H          =  full(HELP, 'h');
    static std::string const NB_STEPS        = "nb-steps";
    static std::string const NB_STEPS_N      = full(NB_STEPS, 'n');
    static std::string const MAX_STEP        = "max-step";
    static std::string const MAX_STEP_M      = full(MAX_STEP, 'm');
    static std::string const VERBOSE         = "verbose";
    static std::string const VERBOSE_V       = full(VERBOSE, 'v');
    static std::string const VERSION         = "version";
    
  }
}
