// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGO_BOUNDARIES_CONDITIONS_H
#define FARGO_BOUNDARIES_CONDITIONS_H

#include <string>
#include "precision.hpp"
#include "plugin.hpp"

namespace fargOCA {
  class Disk;
  struct BoundariesConditions final {};
  /// \brief This plugin modify a disk (suposedly its boundaries) for time step dt.
  /// The name used to call the plugin is provided as first argument.
  /// The disk those boundaries will be updated
  /// the time step to apply the condition
  using BoundariesConditionsPlugin = Plugin<BoundariesConditions, std::string, Disk&, real>;
}
#endif
