// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "loops.hpp"
#include "grid.hpp"

namespace fargOCA { 
  Kokkos::MDRangePolicy<Kokkos::Rank<3>>
  fullRange(GridSizes const& ext) {
    return range3D({0u,0u,0u}, {ext.nr,ext.ni,ext.ns});
  }
  
  Kokkos::MDRangePolicy<Kokkos::Rank<2>>
  fullFlatRange(GridSizes const& ext) {
    assert(ext.ni == 1);
    return range2D(std::array<size_t,2>({0u,0u}), std::array<size_t,2>({ext.nr,ext.ns}));
  }

  Kokkos::MDRangePolicy<Kokkos::Rank<2>>
  fullProfileRange(GridSizes const& ext) {
    return range2D(std::array<size_t,2>({ext.nr, ext.ni}));
  }
}

