// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>
#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "codeUnits.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "transportEngine.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace kk = Kokkos;
namespace HF = HighFive;

using std::optional;

void
write_radial_flow(Disk const& disk, real dt, std::ostream& out)
{
  auto const& gcoords = disk.dispatch().grid().global();
  size_t const gnr = gcoords.sizes().nr;
  auto flow = kk::create_mirror_view_and_copy(kk::HostSpace{},
                                              *disk.dispatch().joinManaged(ComputeFlow(disk, dt), 0));
  if (flow.size() != gnr) {
    std::cerr << "Mistmatch: " << flow.size() << " VS " << gnr << '\n';
    std::abort();
  }

  CodeUnits const& units = disk.physic().units;
  real const mdotSMyear = ipow<2>(units.distance())*units.density()*units.velocity()*YEAR/units.sunMass();
  if (disk.dispatch().comm().rank() == 0) {
    out << std::scientific << std::setprecision(12);
    auto radii = radialCoords(gcoords);
    for (size_t i = 0; i < gnr; i++) {
      out << radii(i) << '\t' << flow(i)*mdotSMyear << '\n';
    }
  }
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract radial flow.\n"
        << '\t' << cmd << " --disk <idisk>.h5 --delta <dt> [--output <file>].\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("delta", po::value<real>(), "The delta time.")
    ("output,o", po::value<std::string>(), "Output file, print on stdout by default.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  input_option.add("delta", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk", "delta" }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  Environment env(argc, argv);
  fmpi::communicator world;
  
  std::string ifname = vm["disk"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadOnly);
  auto disk = Disk::make(world, ifile.getGroup("/"));
  
  std::ofstream ofile;
  std::ostream& out = (bool(vm.count("output")) 
                       ? (ofile.open(vm["output"].as<std::string>()), ofile) 
                       : std::cout);
  write_radial_flow(*disk, vm["delta"].as<real>(), out);
  return 0;
}
