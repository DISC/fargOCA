// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_LOCALIZED_H
#define FARGOCA_LOCALIZED_H

#include <iomanip>
#include <ostream>
#include <limits>
#include <ostream>
#include <Kokkos_Core.hpp>
#include "precision.hpp"

namespace fargOCA {
  template<typename T>
  struct Localized {
    size_t i, h, j;
    T value;
  };

  template<typename T>
  std::ostream&
  operator<<(std::ostream& out, Localized<T> const& l) {
    out << "{(" << l.i << ',' << l.h << ',' << l.j << ")," << l.value << '}';
    return out;
  }

  template<>
  std::ostream&
  operator<<(std::ostream& out, Localized<real> const& l) {
    out << "{(" << l.i << ',' << l.h << ',' << l.j << "),"
	<< std::setprecision(15) << l.value << '}';
    return out;
  }
  
  template<typename T>
  KOKKOS_INLINE_FUNCTION
  bool
  operator<(Localized<T> const& l1, Localized<T> const& l2) {
    return l1.value < l2.value;
  }
}

namespace Kokkos {
  template<typename T>
  struct reduction_identity<fargOCA::Localized<T>> {
    static size_t constexpr indexMax = std::numeric_limits<size_t>::max();
    static T      constexpr valueMax = std::numeric_limits<T>::max();
    static T      constexpr valueMin = std::numeric_limits<T>::min();
    
    KOKKOS_FORCEINLINE_FUNCTION static constexpr fargOCA::Localized<T> min() {
      return { indexMax, indexMax, indexMax, valueMax};
    }
    
    KOKKOS_FORCEINLINE_FUNCTION static constexpr fargOCA::Localized<T> max() {
      return { indexMax, indexMax, indexMax, valueMin};
    }
  };
}

#endif
