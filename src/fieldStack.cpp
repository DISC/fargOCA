// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include<sstream>
#include<ostream>

#include "fieldStack.hpp"

namespace fargOCA {
  namespace detail {
    std::string
    makeTempLabel(std::string base, int idx) {
      std::ostringstream label;
      label << base << "_" << idx;
      return label.str();
    }
    
    template<typename T> 
    arr3d<T> 
    FieldViewUtils<T,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>::make(GridSizes const& sizes, std::string name) {
      return scalarView<T>(name, sizes);
    }
  }
  
#ifdef NDEBUG
  bool FieldStack<void>::ourSetUninitialized2NaN = false;
#else
  bool FieldStack<void>::ourSetUninitialized2NaN = true;
#endif 

  template<typename T, GridDirection... Ds>
  FieldStack<T,Ds...>::FieldStack(GridSizes const& sizes, std::string name, int hint) 
    : myExtents(sizes),
      myName(name),
      myStack(),
      myIdx(0) {
    myStack.reserve(hint);
    increase(hint);
  }

  template<typename T, GridDirection... Ds>
  FieldStack<T, Ds...>::~FieldStack() {}

  template<typename T, GridDirection... Ds>
  void
  FieldStack<T,Ds...>::increase(int n) {
    while (n-- > 0) {
      int sz = myStack.size();
      myStack.push_back(utils::make(myExtents, detail::makeTempLabel(myName, sz)));
    }
  }
  
  template<typename T, GridDirection... Ds>
  void
  FieldStack<T, Ds...>::pop() {
    assert(myIdx>0);
    --myIdx;
    if (myStack.at(myIdx).use_count() != 1) {
      // This view is logicallly dead, our stack should be the 
      // only one using it.
      Kokkos::abort("A local stack view escaped!!!"); 
    }
  }
  
  template class FieldStack<real, GridDirection::RADIAL, GridDirection::PHI, GridDirection::THETA>;
  template class FieldStack<int, GridDirection::RADIAL, GridDirection::PHI, GridDirection::THETA>;

}
