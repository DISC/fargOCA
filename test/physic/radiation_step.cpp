// Copyright 2018, Elena Lega, elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <filesystem>
#include <chrono>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "codeUnits.hpp"
#include "h5Labels.hpp"
#include "stellarRadiationSolver.hpp"
#include "simulation.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

po::variables_map
read_cmdline(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract radial surface density \n"
        << '\t' << cmd << " --disk <idisk>.h5 :"
        << "print  rmed and radial surface density from disk density in g/cm^2.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("verbose,v", "More verbose output.")
    ("disk", po::value<std::string>(),     "HDF5 file containing the disk.")
    ("nb-step,n", po::value<int>()->default_value(10),     "Number of steps.")
    ("nb-threads", po::value<int>(),     "Number of thread.")
    ("output,o", po::value<std::string>()->default_value("advanced.h5"), "Output file, print on stdout by default.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk" }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  return vm;
}

int
main(int argc, char* argv[]) {
  po::variables_map vm = read_cmdline(argc, argv);
  Environment env(argc, argv);
  fmpi::communicator world;
  
  std::string ifname = vm["disk"].as<std::string>();
  auto simulation{Simulation::make(env.world(), h5::getRoot(ifname), ".")};
  
  if (bool(vm.count("verbose"))) {
    simulation->disk().physic().units.dump(log(world));
    simulation->disk().physic().dump(log(world));
  }
  real timeStep = simulation->userTimeStep();
  int  nb = vm["nb-step"].as<int>();

  simulation->disk().dispatch().log() << "Radial dispatch: ";
  harr1d<int const> radii = simulation->disk().dispatch().nbRadii();
  for(int i = 0; i < int(radii.size()); ++i) { simulation->disk().dispatch().log() << radii(i) << ' '; }
  real timeLeft = timeStep;
  std::chrono::duration<double> totalTime{0};
  while (nb-- > 0) {
    if (timeLeft <= 0) {
      simulation->disk().dispatch().log() << nb <<  " no more time left in time step.\n";
      break;
    }
    real dt       = simulation->disk().convergingTimeStep(timeLeft);
    simulation->disk().dispatch().log() << nb <<  " advancing with converging time step(" << timeLeft << "): " 
				       << dt << ' ' << std::flush; 
    auto const start{std::chrono::steady_clock::now()};
    simulation->disk().stellarRadiationSolver().advance(dt);
    auto const end{std::chrono::steady_clock::now()};
    const std::chrono::duration<double> elapsed{end - start};
    simulation->disk().dispatch().log() << " -> " << elapsed.count() << "s\n";
    timeLeft -= dt;
    totalTime += elapsed;
  }
  simulation->disk().dispatch().log() << "Total time: " << totalTime.count() << "s\n";
  h5::createH5(simulation, env.world(), vm["output"].as<std::string>());

  return 0;
}
