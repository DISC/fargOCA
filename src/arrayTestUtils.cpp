// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "arrayTestUtils.hpp"
#include "gridDispatch.hpp"

#include "Kokkos_MathematicalFunctions.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  
  using kk::fabs;
  using kk::fmax;

  std::optional<real>
  maxAbsDiff(arr1d<real const> a1, arr1d<real const> a2) {
    real diff = 0;
    kk::parallel_reduce(kk::RangePolicy<>(0, a1.size()),
                        KOKKOS_LAMBDA(int l, real& tmp) {
                          tmp = fmax(fabs(a1(l)-a2(l)), tmp);
                        }, kk::Max<real>(diff));
    if (std::isnan(diff)) {
      return std::nullopt;
    } else {
      return diff;
    }
  }

  std::optional<real>
  maxRelDiff(arr1d<real const> a1, arr1d<real const> a2) {
    real diff = 0;
    int sz = a1.size();
    kk::parallel_reduce(kk::RangePolicy<>(0,sz),
                        KOKKOS_LAMBDA(int l, real& tmp) {
                          real v1 = a1(l);
                          real v2 = a2(l);
                          real rdiff = v1 == v2 ? 0 : fabs(v1-v2)/((fabs(v1)+fabs(v2))/2);
                          if (rdiff<0) {
                            kk::abort("negative abs value");
                          }
                          tmp = fmax(rdiff, tmp);
                        }, kk::Max<real>(diff));
    if (std::isnan(diff)) {
      return std::nullopt;
    } else {
      return diff;
    }
  }

}
