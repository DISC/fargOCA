// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <string>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "gasShape.hpp"
#include "diskProfiles.hpp"
#include "gridDispatch.hpp"
#include "arrayTestUtils.hpp"
#include "loops.hpp"

namespace HF = HighFive;

int
main(int argc, char* argv[]) {
  using namespace fargOCA;

  Environment  env(argc, argv);
  fmpi::communicator world;
  
  size_t const gnr = 20*world.size();
  size_t const ni  = 10;
  size_t const ns  = 11;
  
  GasShape     shape(1, 2, 20, 0.1, false);
  std::cout << std::scientific;
  std::cout.precision(10);
  
  auto grid = GridDispatch::make(world, *Grid::make(shape, {gnr, ni, ns}, GridSpacing::ARITHMETIC));
  Grid const& coords = grid->grid();
  size_t const nr    = coords.sizes().nr;
  arr2d<real> radv    = profileView("velocity/radial", coords);
  arr2d<real> phiv    = profileView("velocity/phi", coords);
  arr2d<real> thetav  = profileView("velocity/theta", coords);
  arr2d<real> density = profileView("density", coords);
  arr2d<real> energy  = profileView("energy", coords);

  size_t iBase = grid->radiiIndexRange().first;
  Kokkos::parallel_for(range2D({0,0}, {nr,ni}),
                       KOKKOS_LAMBDA(size_t const i, size_t const h) {
                         size_t const ii = iBase+i;
                         radv(i,h)    = ii*10+h+0.1;
                         phiv(i,h)    = ii*10+h+0.2;
                         thetav(i,h)  = ii*10+h+0.3;
                         density(i,h) = ii*10+h+0.4;
                         energy(i,h)  = ii*10+h+0.5;
                       });
  
  DiskProfiles profiles1(*grid, radv, phiv, thetav, density, energy);
  {
    int writer = 0;
    std::optional<HF::File>  file;
    std::optional<HF::Group>   root;
    if (writer == grid->comm().rank()) {
      file = HF::File("profile.h5", HF::File::Overwrite);
      root = file->getGroup("/");
    }
    profiles1.writeH5(root, writer);
  }
  world.barrier();
  HF::File h5("profile.h5", HF::File::ReadOnly);
  DiskProfiles profiles2(*grid, h5.getGroup("/"));
  bool radvok      = checkAbsRelDiff(maxAbsRelDiff(profiles1.velocity().radial(), profiles2.velocity().radial()), 1e-17, 1e-7, std::cout);
  bool phivok      = checkAbsRelDiff(maxAbsRelDiff(profiles1.velocity().phi(), profiles2.velocity().phi()), 1e-17, 1e-7, std::cout);
  bool thetavok    = checkAbsRelDiff(maxAbsRelDiff(profiles1.velocity().theta(), profiles2.velocity().theta()), 1e-17, 1e-7, std::cout);
  bool densityok   = checkAbsRelDiff(maxAbsRelDiff(profiles1.density(), profiles2.density()), 1e-17, 1e-7, std::cout);
  bool energyok    = checkAbsRelDiff(maxAbsRelDiff(*profiles1.energy(), *profiles2.energy()), 1e-17, 1e-7, std::cout);
  
  bool gradvok    = fmpi::all_reduce(world, radvok,    std::logical_and());
  bool gphivok    = fmpi::all_reduce(world, phivok,    std::logical_and());
  bool gthetavok  = fmpi::all_reduce(world, thetavok,  std::logical_and());
  bool gdensityok = fmpi::all_reduce(world, densityok, std::logical_and());
  bool genergyok  = fmpi::all_reduce(world, energyok,  std::logical_and());
  
  int failed = 0;
  if (!gradvok)    { ++failed; std::cout << "Max radvdiff: "    << failed << '\n'; }
  if (!gphivok)    { ++failed; std::cout << "Max phivdiff: "    << failed << '\n'; }
  if (!gthetavok)  { ++failed; std::cout << "Max thetavdiff: "  << failed << '\n'; }
  if (!gdensityok) { ++failed; std::cout << "Max densitydiff: " << failed << '\n'; }
  if (!genergyok)  { ++failed; std::cout << "Max energydiff: "  << failed << '\n'; }
  return failed;
}
