// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <forward_list>
#include "scalarField.hpp"

#include "environment.hpp"
#include "arrayTestUtils.hpp"
#include "gridDispatch.hpp"
#include "loops.hpp"

using namespace fargOCA;
namespace kk = Kokkos;

template<class REF>
bool
compare(arr3d<real const>  val, Grid const& grid, REF ref, std::ostream& log, real const tol) {
  auto host = kk::create_mirror_view_and_copy(kk::HostSpace{}, val);
  GridSizes gsz = grid.sizes();
  bool passed = true;
  std::ostringstream diags;
  
  for(size_t i = 0; i < gsz.nr; ++i) {
    for(size_t h = 0; h < gsz.ni; ++h) {
      for(size_t j = 0; j < gsz.ns; ++j) {
        real const diff = std::abs(host(i,h,j) - ref(i,h,j));
        if (diff > tol) {
          passed = false;
          diags << "Problem at " << "/(" << i << ',' << h << ',' << j << ") : abs(a-b) is "
                << diff << " -> " << host(i,h,j) << " vs " << ref(i,h,j) << std::endl;
        }
      }
    }
  }
  if (!passed) {
    log << diags.str();
  }
  return passed;
}

std::string yesno(bool b) { return b ? std::string("yes") : std::string("no"); }

int 
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  real const tol = argc > 1 ? std::stof(argv[1]) : 1e-15;
  std::cout << "Running test with tol: " << tol << '\n';

  GasShape shape(1, 42, 56, 0.001, false); // we don't really care
  GridSpacing constexpr SPACING =  GridSpacing::ARITHMETIC;
  std::shared_ptr<Grid const> coords = Grid::make(shape, {100, 4, 4}, SPACING);
  auto grid = GridDispatch::make(world, *coords);

  auto const& lcoords = grid->grid().template as<SPACING>();
  ScalarField bob("bob", *grid);
  ScalarField alice("alice", *grid);
  arr3d<real> bobv = bob.data();
  size_t const imin = grid->radiiIndexRange().first;
  auto gval = KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) -> real { return i+real(h)/10+real(j)/100; };
  auto lval = KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) -> real { return gval(imin+i, h, j); };
  
  Kokkos::parallel_for(fullRange(lcoords), KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) { bobv(i,h,j) = lval(i,h,j);  });
  auto bmerged = bob.merged(0);
  {
    bool passed = true;
    if (bmerged) {
      passed = compare(*bmerged, lcoords.global(), gval, std::cout, tol);
    }
    passed = fmpi::all_reduce(grid->comm(), passed, std::logical_and<bool>());
    if (!passed) {
      return -1;
    }
  }
  {
    alice.dispatch(bmerged, 0);
    bool passed = {compare(alice.data(), lcoords, lval, std::cout, tol)};
    passed = fmpi::all_reduce(grid->comm(), passed, std::logical_and<bool>());
    return passed ? 0 : 1;
  }
}
