// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License

#include "simulationConfig.hpp"
#include "simulation.hpp"
#include "diskPhysic.hpp"
#include "configProperties.hpp"
#include "config.hpp"
#include "momentum.hpp"

namespace fargOCA {
  using nlohmann::json;
  using namespace configProperties;
  
  json
  loadJsonFromPTree(fmpi::communicator const& comm, std::string ptreeFname) {
    // We do the config translation by going through a simulation object
    // whih sound ooverkill. But this is a transition solution
    
    auto simulation = Simulation::makeFromPropertyTree(comm, ptreeFname);
    nlohmann::json cfg = configuration(*simulation);
    return cfg;
  }

  namespace {
    bool
    checkMRI2Dz(json const& jroot, std::ostream& log) {
      auto jviscosity = jroot[snake(DISK)][snake(PHYSIC)][snake(VISCOSITY)];
      if (jviscosity[snake(TYPE)].get<std::string>() == "MRIToDz") {
	auto jconfig = jviscosity[snake(CONFIG)];
	if (!jconfig.contains("transitionRadius")) {
	  log << "Property '" << "transitionRadius" << "' is mandatory in vidcosity config when MRI to dz is configured.\n";
	  return false;
	}
      }
      return true;
    }

    bool 
    checkAccretionViscosityDensityConsistency(json const& jroot, std::ostream& log) {
      auto jphysic = jroot[snake(DISK)][snake(PHYSIC)];
      CodeUnits units(jphysic[snake(CODE_UNITS)]);
      auto jviscosity = jphysic[snake(VISCOSITY)];
      std::string const viscosityType = jviscosity[snake(TYPE)].get<std::string>();
      std::optional<json> jstarAccretion;
      if (jphysic.contains(snake(STAR_ACCRETION))) {
	jstarAccretion = jphysic[snake(STAR_ACCRETION)];
      }
      if (viscosityType != "MRIToDz") {
        if (bool(jstarAccretion)
	    && (*jstarAccretion)[snake(TYPE)].get<StarAccretion>() == StarAccretion::CONSTANT) {
	  std::string const viscosityType = jviscosity[snake(TYPE)].get<std::string>();
	  auto jdensity = jphysic[snake(DENSITY)];
	  if (viscosityType == "constant") {
            if (jdensity[snake(SLOPE)].get<real>() > 0) {
              log << "Fatal: you need a 0 density slope (set " << snake(SLOPE) << " to 0 in Density config)"
                  << " for constant star accretion rate (" << StarAccretion::CONSTANT << ") in constant viscosity disc. " <<  '\n'
                  << "Or remove star accretion parameters from config file (gas accretion will occur at rate: 2pi*r*v_r*Sigma)" << "'.\n";
              return false;
            } else {
              real const densityStart = jdensity[snake(START)].get<real>();
	      real const rate         = (*jstarAccretion)[snake(RATE)].get<real>();
              real const accretionRate =  DiskPhysic::StarAccretion::convertUserAccretionRate(rate, units);
              real const densFromAccretion = accretionRate/(3*PI*densityStart);
              if(std::abs(densFromAccretion-densityStart)/densityStart > 0.01){
                log << "Your initial density for required accretion rate should be: " << densFromAccretion  << '\n'
                    << "You have:" << densityStart << '\n'
                    << "FATAL restart with: " << densFromAccretion<< '\n'
                    << "Or remove star accretion parameters from config file (gas accretion will occur at rate: 2pi*r*v_r*Sigma)" << '\n';
                return false;
              }
            }
	  } else if (viscosityType == "constant_alpha") {
	    real const densitySlope = jdensity[snake(SLOPE)].get<real>();
	    real const flaringIndex = jphysic[snake(FLARING_INDEX)].get<real>();
	    if ((densitySlope - 2.*flaringIndex-0.5)>0.01){
	      log << "FATAL: You need the relation 2*flaring-slope+0.5=0 to be satisfied for constant accretion rate in alpha disc" <<  '\n'
		  << "Your value is:" << densitySlope - 2*flaringIndex-0.5 << '\n'
		  << "Or remove star accretion parameters from config file (gas accretion will occur at rate: 2pi*r*v_r*Sigma)"  << '\n';
	      return false;
	    } else {
              real const densityStart = jdensity[snake(START)].get<real>();
	      real const rate         = (*jstarAccretion)[snake(RATE)].get<real>();
              real const accretionRate =  DiskPhysic::StarAccretion::convertUserAccretionRate(rate, units);
	      real const aspectRatio    = jphysic[snake(GAS_SHAPE)][snake(ASPECT_RATIO)].get<real>();
	      real const viscosityValue = *jviscosity[snake(CONFIG)].get<Config>().value<real>("value");
	      real const densFromAccretion = accretionRate/(3*PI*viscosityValue*ipow<2>(aspectRatio));
	      
	      if(std::abs(densFromAccretion-densityStart)/densityStart > 0.01) {
		log << "FATAL: Your initial density for required accretion rate should be: " << densFromAccretion  << '\n'
		    << "You have:" << densityStart << '\n'
		    << "Restart with: " << densFromAccretion<< '\n'
		    << "Or remove star accretion parameters from config file (gas accretion will occur at rate: 2pi*r*v_r*Sigma)"  << '\n';
		return false;
	      }
	    }
	  }
	} else if (viscosityType == "ViscoForCavity") {
	  if (bool(jstarAccretion)
	      && (*jstarAccretion)[snake(TYPE)].get<StarAccretion>() == StarAccretion::CONSTANT) {
	    log << "You have required Viscosity For Cavity and provided a constant star accretion rate ok!" << '\n';
	  } else {
	    log << "!!!!!! FATAL: You have required Viscosity For Cavity therefore you need to provide a star accretion rate"
		<< " of type constant "  << "\n";
	    return false;
	  }
	}
      }
      return true;
    }

    bool
    checkReferentialRotation(json const& jroot, std::ostream& log) {
      auto jphysic = jroot[snake(DISK)][snake(PHYSIC)];
      auto referential = jphysic.at(snake(REFERENTIAL)).get<DiskPhysic::Referential>();
      
      bool pass = true;
      switch (referential.type) {
      case DiskReferential::COROTATING:
        if (referential.omega) {
	  log << "Fatal: Cannot specify both " << referential.type << " and Omega.\n";
	  pass = false;
	}
	if (!referential.guidingPlanet) {
	  log << "Fatal: Need a guiding planet for " << referential.type << " referential.\n";
	  pass = false;
	}
        break;
      default:
	break;
      }
      return pass;
    }
    
    bool
    checkDensityMin(json const& jroot, std::ostream& log) {
      auto jphysic = jroot[snake(DISK)][snake(PHYSIC)];
      auto density     = jphysic.at(snake(DENSITY)).get<DiskPhysic::Density>();      
      if (density.minimum < DENSITY_MIN) {
        log << "Please restart with minimum larger than 1.e-25 gr/cm^3\n";
        return false;
      }
      return true;
    }


  }
  
  bool
  checkConfig(json const& jroot, std::ostream& log) {
    int fatal = 0;
    if (!checkMRI2Dz(jroot, log)) { ++fatal; }
    if (!checkAccretionViscosityDensityConsistency(jroot, log)) { ++fatal; }
    if (!checkReferentialRotation(jroot, log)) { ++fatal; }
    if (!checkDensityMin(jroot, log)) { ++fatal; }
    if (fatal>0) {
      return false;
    } else {
      return true;
    }
  }

  namespace {
    // Just replace upper case C with _c
    std::string
    snakeHelper(std::string s) {
      using size_type = std::string::size_type;
      using char_type = std::string::value_type;    
      auto pos = s.begin();
      auto upper = [](char_type c) { return bool(std::isupper(c)); };
      while (true) {
	auto found = std::find_if(pos, s.end(), upper);
	if (found == s.end()) {
	  break;
	} else {
	  *found = char(std::tolower(*found));
	  if (found != s.begin()) {
	    found = s.insert(found, '_');
	    ++found; // skip '_'
	  }
	  pos = ++found; // skip lowered letter
	}
      }
      return s;
    }

    struct PropComp {
      using path = boost::property_tree::path;
      using less = std::less<std::string>;
      bool operator()(path const& p1, path const& p2) const {
	return less()(str(p1), str(p2));
      }
    };
    using Prop2SnakeMap = std::map<boost::property_tree::path, std::string, PropComp>;
    
    Prop2SnakeMap
    initProp2SnakeMap() {
      Prop2SnakeMap map;
      map[ADIABATIC_EOS]   = "adiabatic";
      map[CFL_SECURITY]    = "minimal_time_step";
      map[EXCLUDE_HILL]    = "exclude_Hill";
      map[FULL_ENERGY_EOS] = "radiative";
      map[HILL_CUT_FACTOR] = "Hill_cut_factor";
      map[HILL_RADIUS]     = "Hill_radius";
      return map;
    }

    Prop2SnakeMap&
    prop2SnakeMap() {
      static Prop2SnakeMap map = initProp2SnakeMap();
      return map;
    }
  }

  std::string
  snake(std::string s) {
    using pair = Prop2SnakeMap::value_type;
    Prop2SnakeMap& known = prop2SnakeMap();
    auto found = known.find(s);
    if (found == known.end()) {
      // save work for next time
      found = known.insert(pair(s,snakeHelper(s))).first;
    }
    return found->second;
  }

  std::string
  snake(boost::property_tree::path const& p) {
    if (!p.single()) {
      std::ostringstream diag;
      diag << "Snake case convertaer is not supposed non elementary propserties like '"
	   << p.dump() << '\'';
      throw std::domain_error(diag.str());
    }
    return snake(p.dump());
  }
}
