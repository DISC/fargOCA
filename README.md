**FARGOCA** is a grid-based hydrodynamical code that run on CPU and GPU clusters specifically designed for the study of protoplanetary disks and for planets-disk interactions.
It uses MPI for multi-node parallelism and Kokkos for intra node parallelism (OpenMP, CUDA and HIP backends). 

The main features of FARGOCA are:

 * Spherical geometry.
 * 2 and 3 dimensional calculations.
 * FARGO algorithm for orbital advection calculations.
 * Full Energy equation taking into account viscous heating, star radiation, radiation from planets accreting solids and radiative cooling with multiple opacity prescriptions.
