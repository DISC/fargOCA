<!-- Copyright 2020, Gabriele Pichierri, gabe@caltech.edu

 This file is part of FargOCA.

 FargOCA is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.

 FargOCA is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.
-->

# CPU build

## Environement

You can load the environment with

```
$ source <srcdir|builddir>/contrib/build/Caltech/central/env.sh
```

## Configuring and building

The commands to build the code are

```
$ cmake -lmpi -DKokkos_ENABLE_OPENMP=On -DCMAKE_BUILD_TYPE=Release -DGENERATE_DOC=NO <srcdir>
$ make
```

The `-DGENERATE_DOC=NO` flag in the `cmake` command avoids building the documentation which may results in unnecessary warnings/errors.

# GPU build

## Environement

You can load the environment with

```
$ source <srcdir|builddir>/contrib/build/Caltech/central/env_GPU.sh
```

## Configuring and building

The commands to build the code are the following.

For running the code on the p100 GPU's (older but more numerous, available on all nodes at the time of writing):

```
$ cmake -lmpi -DCMAKE_BUILD_TYPE=Release -DKokkos_ENABLE_CUDA=On -DKokkos_ARCH_PASCAL60=On -DKokkos_ENABLE_OPENMP=On -DKokkos_ENABLE_CUDA_LAMBDA=On -DFARGO_NO_PYTHON=On -DGENERATE_DOC=NO <srcdir>
$ make
```

For running the code on the (newer but fewer) v100 GPU's:

```
$ cmake -lmpi -DCMAKE_BUILD_TYPE=Release -DKokkos_ENABLE_CUDA=On -DKokkos_ARCH_AMPERE80=On -DKokkos_ENABLE_OPENMP=On -DKokkos_ENABLE_CUDA_LAMBDA=On -DFARGO_NO_PYTHON=On -DGENERATE_DOC=NO <srcdir>
$ make
```