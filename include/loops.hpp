// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_LOOPS_H
#define FARGOCA_LOOPS_H

#include <cassert>
#include <array>

#include <Kokkos_Core.hpp>

#include "arrays.hpp"

/// \file Shortcuts to help writing Kokkos parallel operations.
namespace fargOCA {
  struct GridSizes;

  /// \brief A 1D range based Kokkos policy
  /// \param begin the first index
  /// \param end the one past the end index
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::RangePolicy<>>::type 
  range1D(Int begin, Int end) {
    assert(begin < end);
    return Kokkos::RangePolicy<>{begin, end};
  }

  /// \brief A 1D range based Kokkos policy
  /// \param begin the first index
  /// \param end the one past the end index
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::RangePolicy<>>::type 
  range1D(Int N) {
    return range1D<Int>(0, N);
  }
  
  /// \brief A 2D range based Kokkos policy
  /// \param begin the 2 first indexes
  /// \param end the 2 one past the end indexes
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::MDRangePolicy<Kokkos::Rank<2>>>::type
  range2D(std::array<Int,2> begin, std::array<Int,2> end) {
    assert(begin[0] < end[0]);
    assert(begin[1] < end[1]);
    return Kokkos::MDRangePolicy<Kokkos::Rank<2>>{{begin[0], begin[1]}, {end[0], end[1]}};
  }

  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::MDRangePolicy<Kokkos::Rank<2>>>::type
  range2D(std::array<Int,2> end) {
    return range2D({Int(0), Int(0)}, end);
  }
  
  /// \brief A 3D range based Kokkos policy
  /// \param begin the 3 first indexes
  /// \param end the 3 one past the end indexes
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::MDRangePolicy<Kokkos::Rank<3>>>::type
  range3D(std::array<Int,3> begin, std::array<Int,3> end) {
    return Kokkos::MDRangePolicy<Kokkos::Rank<3>>({begin[0], begin[1], begin[2]}, {end[0], end[1], end[2]});
  }
  
  /// \brief A range based Kokkos policy covering a whole view.
  /// \param view the covered view
  template<typename T>
  auto
  range(arr3d<T> view) {
    assert(0 < view.extent_int(0));
    assert(0 < view.extent_int(1));
    assert(0 < view.extent_int(2));
    return range3D<int>({0,0,0}, {view.extent_int(0), view.extent_int(1), view.extent_int(2)}); 
  }

  /// \brief A range based Kokkos policy covering a whole view.
  /// \param view the covered view
  template<typename T>
  auto
  range(arr2d<T> view) {
    assert(0 < view.extent_int(0));
    assert(0 < view.extent_int(1));
    return range2D<int>({0,0}, {view.extent_int(0), view.extent_int(1)}); 
  }

  /// \brief A range based Kokkos policy covering a whole view.
  /// \param view the covered view
  template<typename T>
  auto
  range(arr1d<T> view) {
    assert(0 < view.extent_int(0));
    return range1D<int>({view.extent_int(0)});
  }

  /// \brief A range based Kokkos policy covering a whole grid.
  /// \param ext the sizes
  Kokkos::MDRangePolicy<Kokkos::Rank<3>> fullRange(GridSizes const& ext);
  
  /// \brief A range based Kokkos policy covering a whole grid.
  /// \param view the covered view
  template<class COORDS>
  auto
  fullRange(COORDS const& coords) {
    return fullRange(coords.sizes());
  }

  /// \brief A range based Kokkos policy covering a whole grid.
  /// \param ext the sizes
  Kokkos::MDRangePolicy<Kokkos::Rank<2>> fullFlatRange(GridSizes const& ext);

  /// \brief A range based Kokkos policy covering a whole grid.
  /// \param view the covered view
  template<class COORDS>
  auto
  fullFlatRange(COORDS const& coords) {
    return fullFlatRange(coords.sizes());
  }

  /// \brief A range based Kokkos policy covering a whole grid profile.
  /// \param view the covered view
  Kokkos::MDRangePolicy<Kokkos::Rank<2>> fullProfileRange(GridSizes const& ext);
  
  /// \brief A range based Kokkos policy covering a whole grid profile.
  /// \param view the covered view
  template<class COORDS>
  auto
  fullProfileRange(COORDS const& coords) {
    return fullProfileRange(coords.sizes());
  }

  /// \brief A host 3D range based Kokkos policy
  /// \param begin the 3 first indexes
  /// \param end the 3 one past the end indexes
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::MDRangePolicy<Kokkos::Rank<3>,
                                                Kokkos::DefaultHostExecutionSpace>>::type
  hRange3D(std::array<Int,3> begin, std::array<Int,3> end) {
    assert(begin[0] < end[0]);
    assert(begin[1] < end[1]);
    assert(begin[2] < end[2]);
    return Kokkos::MDRangePolicy<Kokkos::Rank<3>,Kokkos::DefaultHostExecutionSpace>({begin[0], begin[1], begin[2]}, {end[0], end[1], end[2]});
  }
  
  /// \brief A host range based Kokkos policy covering a whole grid.
  /// \param view the covered view
  template<class COORDS>
  auto
  hFullRange(COORDS const& grid) {
    auto gsz = grid.sizes();
    return hRange3D(std::array<size_t,3>({0u,0u,0u}), std::array<size_t,3>({gsz.nr, gsz.ni, gsz.ns})); 
  }

  /// \brief A host 1D range based Kokkos policy
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>>::type
  hRange1D(Int begin, Int end) {
    return Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(begin,end);
  }

  /// \brief A host 1D range based Kokkos policy
  template<typename Int = size_t>
  inline
  typename std::enable_if<std::is_integral<Int>::value, 
                          Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>>::type
  hRange1D(Int nb) {
    return Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(Int(0), nb);
  }
  
}

#endif
