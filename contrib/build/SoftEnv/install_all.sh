#!/usr/bin/env bash

#pip3 install --user --upgrade cmake

if [[ $# -lt 2 ]]
then
    echo "usage $0 <builddir> <installdir>"
    exit -1
fi
builddir=$1
installdir=$2

echo "./install_gcc.sh $builddir $installdir/gcc/13.2.0 13.2.0"
echo "./install_hdf5.sh $builddir $installdir/hdf5/1.14.3 1.14.3"
echo "./install_boost.sh $builddir $installdir/boost/1.84.0 boost-1.84.0"

./install_gcc.sh $builddir $installdir/gcc/13.2.0 13.2.0
source $installdir/gcc/13.2.0/env.sh


./install_hdf5.sh $builddir $installdir/hdf5/1.14.3 1.14.3
source $installdir/hdf5/1.14.3/env.sh

./install_boost.sh $builddir $installdir/boost/1.84.0 boost-1.84.0
source $installdir/boost/1.84.0/env.sh

(
echo "source $installdir/gcc/13.2.0/env.sh"
echo "source $installdir/hdf5/1.14.3/env.sh"
echo "source $installdir/boost/1.84.0/env.sh"
) > $installdir/env.sh
