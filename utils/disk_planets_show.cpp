// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "h5io.hpp"
#include "simulation.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extracts planet informations out of disk file or a planetary system log.\n"
        << "If the file contains a planets_log group, planetary system log is assumed.\n"
        << '\t' << cmd << " <file>.h5 [option]+ \nValid options are.\n";
  
  po::options_description opts(usage.str());
  
  opts.add_options()
    ("help,h", "This help message.")
    ("file,f", po::value<std::string>(), "HDF5 file containing the disk file or planetary system log.")
    ("output,o", po::value<std::string>()->default_value("planet%1%.dat"), "text file where to write the log.")
    ("path,p", po::value<std::string>()->default_value("/"),
     "The disk's path into the file.");

  po::positional_options_description input_option;
  input_option.add("file", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"file"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
}

void
show(Simulation const& simulation, boost::format oformat) {
  if (simulation.disk().dispatch().master()) {
    for (auto const& [n, p] : simulation.disk().system()->planets()) {
      std::string fname = str(oformat%n);
      std::ofstream out(fname, std::ios::out|std::ios::ate|std::ios::app);
      out << std::scientific;
      out.precision(14);
      Triplet const& pos = p.position();
      Triplet const& vel = p.velocity();
      out << simulation.step()
          << ' ' << pos.x << ' ' << pos.y << ' ' << pos.z
          << ' ' << vel.x << ' ' << vel.y << ' ' << vel.z
          << ' ' << p.mass()
          << ' ' << simulation.disk().physicalTime() << ' ' << simulation.disk().system()->keplerianFrequency(p)
          << '\n';
    }
  }
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);
  
  std::string ifname = vm["file"].as<std::string>();
  auto simulation = Simulation::make(env.world(), h5::getRoot(ifname), ".");
  boost::format ofmt{vm["output"].as<std::string>()};
  show(*simulation, ofmt);
  
  return EX_OK;
}
