// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <string>

#include <Kokkos_Core.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include "environment.hpp"
#include "gridDispatch.hpp"
#include "loops.hpp"

std::string statusStr(bool status) {
  if (status) {
    return "PASS";
  } else {
    return "FAILED";
  }
}

using namespace fargOCA;

namespace kk = Kokkos;
namespace kkm = Kokkos::Experimental;

bool
testGridSpacing(fmpi::communicator const& /* comm */,
                GridSpacing tag, std::string str) {
  bool status = true;
  if (boost::lexical_cast<GridSpacing>(str)  != tag)  {
    std::cerr << str << " did not convert to " << tag << '\n';
    status = false;
  }
  boost::to_upper(str);
  if (boost::lexical_cast<std::string>(tag) != str)  {
    std::cerr << tag << " did not convert to " << str << '\n';
    status = false;
  }
  return status;
}

bool
testGridSpacings(fmpi::communicator const& comm) {
  bool status = true;
  
  if (!testGridSpacing(comm, GridSpacing::ARITHMETIC, "Arithmetic"))
    status = false;
  if (!testGridSpacing(comm, GridSpacing::LOGARITHMIC, "Logarithmic"))
    status = false;
  
  return status;
}

std::vector<real> arithData();
std::vector<real> logData();

template<GridSpacing GS>
bool
testGridRads(GridDispatch const& grid, std::vector<real> const& ref, std::string name ) {

  auto radii = grid.grid().global().as<GS>().radii();
  size_t const nr = grid.grid().global().sizes().nr;
  bool passed = true;
  real const tol = 1e-8;
  std::cout << "Test coord values for " << grid.grid().global().radialSpacing() << '\n';
  for(size_t i = 0; i < nr; ++i) {
    real const delta = kk::abs(radii(i)-ref[i]);
    if (delta > tol) {
      std::cerr << "Problem at " << i << ": " << radii(i) << " vs " << ref[i] << '\n';
      passed = false;
    }
  }
		      
  return passed;
}

size_t constexpr globalNRad = 100;

int
main(int argc, char* argv[]) {
  Environment  env(argc, argv);
  fmpi::communicator world;
  bool gsStatus = fmpi::all_reduce(world,testGridSpacings(world), std::logical_and<bool>());

  size_t const nSec = 2;
  size_t const nInc = 1;
  GasShape     shape(1, 2, 20, 0.1, false);
  std::cout << std::scientific;
  std::cout.precision(10);
  std::cout << "Will test with shape: " << shape << '\n';
  auto arith = GridDispatch::make(world, *Grid::make(shape, {globalNRad,nInc,nSec}, GridSpacing::ARITHMETIC));
  bool arithStatus = testGridRads<GridSpacing::ARITHMETIC>(*arith, arithData(), "arithmetic");

  auto log   = GridDispatch::make(world, *Grid::make(shape, {globalNRad,nInc,nSec}, GridSpacing::LOGARITHMIC));
  bool logStatus = testGridRads<GridSpacing::LOGARITHMIC>(*log, logData(), "logarithmic");

  bool status = gsStatus && arithStatus && logStatus;
  std::cout << statusStr(status) << '\n';
  return status ? 0 : 1;
}

std::vector<real>
arithData() {
  std::vector<real> raw({ 2.0000000000e+00, 2.1800000000e+00, 2.3600000000e+00, 2.5400000000e+00, 2.7200000000e+00, 2.9000000000e+00, 3.0800000000e+00, 3.2600000000e+00, 3.4400000000e+00, 3.6200000000e+00, 3.8000000000e+00, 3.9800000000e+00, 4.1600000000e+00, 4.3400000000e+00, 4.5200000000e+00, 4.7000000000e+00, 4.8800000000e+00, 5.0600000000e+00, 5.2400000000e+00, 5.4200000000e+00, 5.6000000000e+00, 5.7800000000e+00, 5.9600000000e+00, 6.1400000000e+00, 6.3200000000e+00, 6.5000000000e+00, 6.6800000000e+00, 6.8600000000e+00, 7.0400000000e+00, 7.2200000000e+00, 7.4000000000e+00, 7.5800000000e+00, 7.7600000000e+00, 7.9400000000e+00, 8.1200000000e+00, 8.3000000000e+00, 8.4800000000e+00, 8.6600000000e+00, 8.8400000000e+00, 9.0200000000e+00, 9.2000000000e+00, 9.3800000000e+00, 9.5600000000e+00, 9.7400000000e+00, 9.9200000000e+00, 1.0100000000e+01, 1.0280000000e+01, 1.0460000000e+01, 1.0640000000e+01, 1.0820000000e+01, 1.1000000000e+01, 1.1180000000e+01, 1.1360000000e+01, 1.1540000000e+01, 1.1720000000e+01, 1.1900000000e+01, 1.2080000000e+01, 1.2260000000e+01, 1.2440000000e+01, 1.2620000000e+01, 1.2800000000e+01, 1.2980000000e+01, 1.3160000000e+01, 1.3340000000e+01, 1.3520000000e+01, 1.3700000000e+01, 1.3880000000e+01, 1.4060000000e+01, 1.4240000000e+01, 1.4420000000e+01, 1.4600000000e+01, 1.4780000000e+01, 1.4960000000e+01, 1.5140000000e+01, 1.5320000000e+01, 1.5500000000e+01, 1.5680000000e+01, 1.5860000000e+01, 1.6040000000e+01, 1.6220000000e+01, 1.6400000000e+01, 1.6580000000e+01, 1.6760000000e+01, 1.6940000000e+01, 1.7120000000e+01, 1.7300000000e+01, 1.7480000000e+01, 1.7660000000e+01, 1.7840000000e+01, 1.8020000000e+01, 1.8200000000e+01, 1.8380000000e+01, 1.8560000000e+01, 1.8740000000e+01, 1.8920000000e+01, 1.9100000000e+01, 1.9280000000e+01, 1.9460000000e+01, 1.9640000000e+01, 1.9820000000e+01, 2.0000000000e+01 });
  return std::move(raw);
}

std::vector<real>
logData() {
  std::vector<real> raw({ 2.0000000000e+00, 2.0465859846e+00, 2.0942570961e+00, 2.1430386105e+00, 2.1929563923e+00, 2.2440369086e+00, 2.2963072430e+00, 2.3497951099e+00, 2.4045288692e+00, 2.4605375416e+00, 2.5178508236e+00, 2.5764991034e+00, 2.6365134771e+00, 2.6979257652e+00, 2.7607685292e+00, 2.8250750892e+00, 2.8908795415e+00, 2.9582167763e+00, 3.0271224969e+00, 3.0976332378e+00, 3.1697863849e+00, 3.2436201947e+00, 3.3191738149e+00, 3.3964873049e+00, 3.4756016575e+00, 3.5565588201e+00, 3.6394017172e+00, 3.7241742733e+00, 3.8109214359e+00, 3.8996891995e+00, 3.9905246299e+00, 4.0834758893e+00, 4.1785922617e+00, 4.2759241790e+00, 4.3755232479e+00, 4.4774422771e+00, 4.5817353055e+00, 4.6884576306e+00, 4.7976658380e+00, 4.9094178314e+00, 5.0237728630e+00, 5.1407915655e+00, 5.2605359838e+00, 5.3830696079e+00, 5.5084574067e+00, 5.6367658625e+00, 5.7680630063e+00, 5.9024184533e+00, 6.0399034408e+00, 6.1805908650e+00, 6.3245553203e+00, 6.4718731386e+00, 6.6226224297e+00, 6.7768831228e+00, 6.9347370091e+00, 7.0962677847e+00, 7.2615610954e+00, 7.4307045819e+00, 7.6037879264e+00, 7.7809028999e+00, 7.9621434111e+00, 8.1476055561e+00, 8.3373876694e+00, 8.5315903760e+00, 8.7303166448e+00, 8.9336718430e+00, 9.1417637923e+00, 9.3547028257e+00, 9.5726018465e+00, 9.7955763874e+00, 1.0023744673e+01, 1.0257227680e+01, 1.0496149205e+01, 1.0740635927e+01, 1.0990817477e+01, 1.1246826504e+01, 1.1508798747e+01, 1.1776873107e+01, 1.2051191721e+01, 1.2331900037e+01, 1.2619146890e+01, 1.2913084581e+01, 1.3213868960e+01, 1.3521659508e+01, 1.3836619418e+01, 1.4158915688e+01, 1.4488719201e+01, 1.4826204826e+01, 1.5171551501e+01, 1.5524942333e+01, 1.5886564694e+01, 1.6256610323e+01, 1.6635275422e+01, 1.7022760764e+01, 1.7419271799e+01, 1.7825018763e+01, 1.8240216787e+01, 1.8665086016e+01, 1.9099851720e+01, 1.9544744419e+01, 2.0000000000e+01 });
  return std::move(raw);
}
 
