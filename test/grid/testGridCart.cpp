// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <limits>
#include <algorithm>
#include <numeric>
#include <unistd.h>

#include <boost/lexical_cast.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "log.hpp"
#include "io.hpp"
#include "scalarField.hpp"
#include "cartesianGridCoords.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;
namespace kk = Kokkos;
namespace HF = HighFive;

template<typename T> using opt = std::optional<T>;

/// Write array as if it was the MPI component of a ScalarField object
bool
writeH5(GridDispatch const& grid, std::string fname, arr3d<real const> local) {
  ScalarField tmp("znort", grid);
  kk::deep_copy(tmp.data(), local);
  h5::createH5(tmp, grid.comm(), fname);
  return true;
}

bool
test(fmpi::communicator const& world, size_t const radius, size_t const sector, size_t const layer, bool dump, std::string odir) {
  std::ostringstream fmt;
  fmt << "grid_" << radius << '_' << sector << '_' << layer;
  std::string fbase = fmt.str();
  GasShape shape(0.05, 42, 56, 0.001, false);
  auto grid = GridDispatch::make(world, *Grid::make(shape, {radius, layer, sector}, GridSpacing::ARITHMETIC ));
  int const dim = grid->grid().dim();
  
  if (dump) {
    std::cout << "Writing\n";
    ::unlink((fbase+"_x.h5").c_str());
    ::unlink((fbase+"_y.h5").c_str());
    if (dim == 3) { ::unlink((fbase+"_z.h5").c_str()); }

    writeH5(*grid, fbase+"_x.h5", grid->cartesian().x);
    writeH5(*grid, fbase+"_y.h5", grid->cartesian().y);
    if (dim > 2) { writeH5(*grid, fbase+"_z.h5", grid->cartesian().z); }
    return true;
  } else {
    std::cout << "Testing\n";
    ScalarField x("x", *grid);
    x.readH5(odir+"/"+fbase+"_x.h5");
    ScalarField y("y", *grid);
    y.readH5(odir+"/"+fbase+"_y.h5");
    ScalarField z("z", *grid);
    if (dim==3) z.readH5(odir+"/"+fbase+"_z.h5");

    bool xok = checkAbsRelDiff(maxAbsRelDiff(x.data(), grid->cartesian().x), 1e-12, 1e-12, std::cout);
    bool yok = checkAbsRelDiff(maxAbsRelDiff(y.data(), grid->cartesian().y), 1e-12, 1e-12, std::cout);
    bool zok = (dim == 3 && checkAbsRelDiff(maxAbsRelDiff(z.data(), grid->cartesian().z), 1e-12, 1e-12, std::cout));
    
    std::cout << "avrg X= " << average(x.data())
              << ", Y= " << average(y.data())
              << ", Z= " << average(z.data()) << '\n';
    std::cout << "max ok X= " << xok << ", Y= " << yok << ", Z= " << zok << '\n';
    bool passed = xok && yok && zok;
    if (passed) {
      std::cout << "PASSED\n";
    } else {
      std::cout << "FAILED\n";
    }
    return passed;
  }
}

int main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  std::string odir = ".";
  if (argc == 2) {
    odir = argv[1];
  }
  const bool dump = false;
  bool fat  = test(world, 51, 4, 3, dump, odir);
  return fat ? 0 : 1;
}
