// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_PLUGIN_CONFIG_H
#define FARGO_PLUGIN_CONFIG_H

#include <memory>
#include <map>
#include <list>
#include <functional>
#include <optional>

#include "boost/lexical_cast.hpp"

#include "precision.hpp"
#include "shared.hpp"
#include "plugin.hpp"
#include "config.hpp"

namespace fargOCA {

  struct PluginConfig final {
    PluginConfig(std::initializer_list<std::pair<std::string, Config>> cfgs = {});
    PluginConfig(ConfigMap const& cfgs);
    PluginConfig(HighFive::Group root);
    PluginConfig(PluginConfig const&) = default;
    PluginConfig(PluginConfig&&) = default;

    PluginConfig& operator=(PluginConfig&&) = default;
                   
    /// \brief Trackers configuration
    ConfigMap const&         namedConfigs() const    { return myConfigs; }
    Config const&            config(std::string name) const;
    std::vector<std::string> enabled() const;
    
    void dump(std::ostream& out, int tab = 0) const;
    void writeH5(HighFive::Group grp) const;
  private:
    void sort();
  private:
    ConfigMap myConfigs;
  };
  
  void to_json   (nlohmann::json& j,       PluginConfig const& c);
  void from_json (nlohmann::json const& j, PluginConfig& c);
}

#endif 
