// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_NONBLOCKING_HPP
#define BOOST_NOOPMPI_NONBLOCKING_HPP

#include <optional>
#include <cassert>
#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>

namespace boost { namespace noopmpi {

template<typename ForwardIterator, typename OutputIterator> 
OutputIterator 
wait_all(ForwardIterator first, ForwardIterator last, OutputIterator out) {}

template<typename ForwardIterator> 
void wait_all(ForwardIterator /* first */, ForwardIterator /* last */) {}
}}

#endif // BOOST_NOOPMPI_STATUS_HPP
