// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <string.h>
#include <string>
#include <cstdio>
#include <cassert>
#include <cerrno>
#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <unistd.h>

#include <boost/format.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/null.hpp>

#include "allmpi.hpp"

#include "precision.hpp"
#include "log.hpp"

namespace fargOCA {

  bool
  PathExists(std::string const& path) {
    return access(path.c_str(), F_OK|R_OK) == 0;
  }
  
  bool
  FileBackup(std::string const& absPath) {
    static std::string const SUFFIX = "_old";
    bool ok = true;
    std::string backupName = absPath + SUFFIX;
    FILE* input = fopen(absPath.c_str(), "r");
    if (!input) {
      std::cerr << "Could not open " << absPath << ": '" << strerror(errno) << "'\n";
      ok = false;
    } else {
      FILE* output = fopen(backupName.c_str(), "w");
      if (!output) {
        std::cerr << "Could not open " << backupName << ": '" << strerror(errno) << "'\n";
        abort();
      }
      char c;
      while((c=fgetc(input)) != EOF) {
        fputc(c, output);
      }
      fclose(input);
      fclose(output);
    }
    return ok;
  }

  void
  Unlink(std::string path) {
    ::unlink(path.c_str());
  }

  std::string
  tabs(int t) {
    std::string s;
    while(t--) { s = s+"\t"; }
    return s;
  }
  
  std::string
  yesno(bool b) {
    return b ? std::string("yes") : std::string("no");
  }
}
