# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

set(NOOPMPI_RUNTIME_SRCS
  communicator.cpp
  environment.cpp
  graph_communicator.cpp
  request.cpp)

add_library(noopmpi SHARED ${NOOPMPI_RUNTIME_SRCS})
target_link_libraries(noopmpi Boost::boost)
set_target_properties(noopmpi PROPERTIES POSITION_INDEPENDENT_CODE 1)
