module purge

module load develop

module load CCE-GPU-4.0.0

module load gcc/13.2.0
module load cray-hdf5/1.14.3.1
module load cray-python/3.11.5
module load boost/1.86.0-mpi
module load cmake ninja

export CC=cc
export CXX=CC
export FC=ftn
export MPICH_GPU_SUPPORT_ENABLED=1
export CTI_SLURM_OVERRIDE_MC=1 
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
