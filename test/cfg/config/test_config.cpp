// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "precision.hpp"

#include "config.hpp"

using namespace fargOCA;

int
main() {
  Config::Properties props;
  props["znort"]       = "true";
  props["gneh"]        = "false";
  props["bluck"]       = "2.0e-3";
  props["dieantwoord"] = "42";  
  
  Config config(props);
  int failed = 0;
  if (!config.enabled("znort")) { 
    std::cout << "'znort' should be enabled.\n";
    failed++;
  }
  if (config.enabled("gneh")) {
    std::cout << "'gneh' should not be enabled.\n";
    failed++;
  }
  if (*config.value<bool>("gneh")) {
    std::cout << "'gneh' value should be false.\n";
    failed++;
  }
  if (std::abs(*config.value<real>("bluck") - 2.0e-3) > 2*std::numeric_limits<real>::epsilon()) {
    std::cout << "'bluck', as a real, should close to 2.0e-3.\n";
    failed++;
  }
  if (bool(config.value<std::string>("yukyukyuk"))) {
    std::cout << "\"yukyukyuk\" should not be set.\n";
    failed++;
  }
  if (config.value<int>("Bluck", 42) != 42) {
    std::cout << "config.value<int>(\"Bluck\", 42)  should be 42.\n";
    failed++;
  }
  if (config.value<int>("dieantwoord", 32) != 42) {
    std::cout << "config.value<int>(\"dieantwoord\", 32)  should be 42.\n";
    failed++;
  }
  try {
    std::optional<std::string> ostr = config.value<std::string>("Bluck");
    std::cout << "config.value<string>(\"Bluck\") is '";
    if (ostr) {
      std::cout << *ostr << "' which is weird.\n";
      ++failed;
    } else {
      std::cout << "std::nullopt which is fine.\n";
    }
    std::optional<real> oreal = config.value<real>("Bluck");
    std::cout << "config.value<real>(\"Bluck\") is '";
    if (oreal) {
      std::cout << *oreal << "' which is weird.\n";
      ++failed;
    } else {
      std::cout << "std::nullopt which is fine.\n";
    }
    real r = oreal.value();
    failed++;
    std::cout << "*config.value<real>(\"Bluck\") (" << r << ") should raise more concern than that.\n";
  } catch(std::exception const& e) {
    std::cout << "*config.value<real>(\"Bluck\"), did raise an issue as expected.\n";
    std::cout << e.what() << '\n';
  }
  Config uConfig = toUpper(config);
  if (!uConfig.enabled("ZNORT")) {   
    std::cout << "'ZNORT' should be enabled.\n";
    failed++;
  }
  if (std::abs(*uConfig.value<real>("BLUCK") - 2.0e-3) > 2*std::numeric_limits<real>::epsilon()) {
    std::cout << "'BLUCK', as a real, should close to 2.0e-3.\n";
    failed++;
  }
  Config lConfig = toLower(uConfig);
  if (!lConfig.enabled("znort")) {   
    std::cout << "'znort' should be enabled.\n";
    failed++;
  }
  if (std::abs(*config.value<real>("bluck") - 2.0e-3) > 2*std::numeric_limits<real>::epsilon()) {
    std::cout << "'bluck', as a real, should close to 2.0e-3.\n";
    failed++;
  }
  return failed;
}
