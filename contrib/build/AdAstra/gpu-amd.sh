module purge

module load develop

module load GCC-GPU-4.0.0
module load boost/1.86.0-mpi cray-hdf5/1.14.3.1
module unload amd-mixed
module load PrgEnv-amd/8.5.0
module load craype-accel-amd-gfx90a craype-x86-trento
module load PrgEnv-amd

export CC=amdclang
export CXX=amdclang++

export MPICH_GPU_SUPPORT_ENABLED=1
export CTI_SLURM_OVERRIDE_MC=1 

echo cmake -G Ninja -DCMAKE_CXX_COMPILER="amdclang++"  -DCMAKE_C_COMPILER="amdclang" -DCMAKE_CXX_FLAGS="$(CC --cray-print-opts)"                     -DCMAKE_CXX_STANDARD=17                     -DKokkos_ENABLE_HIP=ON                     -DKokkos_ARCH_AMD_GFX90A=ON -DAMDGPU_TARGETS=gfx90a                 
