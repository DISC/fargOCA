// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_H5_LABELS_H
#define FARGOCA_H5_LABELS_H

#include <string>
#include <initializer_list>

/// \file Labels used in HDF5 files

namespace fargOCA { 
  namespace h5 { 
    namespace labels {
      static std::string const ADIABATIC                = "adiabatic";
      static std::string const ARTIFICIAL               = "artificial";
      static std::string const ALPHA_CONFIGURATION      = "alpha_configuration";
      static std::string const BOUNDARIES               = "boundaries";

      static std::string const CENTRAL_BODY_MASS        = "central_body_mass";
      static std::string const CENTRAL_BODY_RADIUS      = "central_body_radius";      
      static std::string const CENTRAL_BODY_TEMPERATURE = "central_body_temperature";      
      static std::string const CODE_UNITS               = "code_units";
      static std::string const CONFIGURATION            = "configuration";
      static std::string const CONFIG                   = "config";
      static std::string const CONSTANT                 = "constant";
      static std::string const CONSTANT_ALPHA           = "constant_alpha";
      static std::string const COOLING                  = "cooling";
      static std::string const DEAD_DENSITY             = "dead_density";
      static std::string const DENSITY                  = "density";
      static std::string const DISK                     = "disk";
      static std::string const DISTANCE                 = "distance";
      static std::string const ENERGY                   = "energy";
      static std::string const ENGINE                   = "engine";
      static std::string const FARGO_FILE_FORMAT        = "FargOCA_file_format";
      static std::string const FULL_ENERGY              = "full_energy";
      static std::string const FRAME_OMEGA              = "frame_omega";
      static std::string const GRID                     = "grid";
      static std::string const GUIDING_PLANET           = "guiding_planet";
      static std::string const HANDLER                  = "handler";
      static std::string const INIT                     = "init";
      static std::string const LONGITUDE_OF_NODE        = "longitude_of_node";
      static std::string const LONGITUDE_OF_PERICENTER  = "longitude_of_pericenter";
      static std::string const MAJOR                    = "major";
      static std::string const MEAN_LONGITUDE           = "mean_longitude";
      static std::string const MEAN_MOLECULAR_WEIGHT    = "mean_molecular_weight";
      static std::string const MINOR                    = "minor";
      static std::string const NAME                     = "name";
      static std::string const NB_STEPS                 = "nb_steps";
      static std::string const ORBITAL_ELEMENTS         = "orbital_elements";
      static std::string const OUTPUT                   = "output";
      static std::string const PARAMETERS               = "parameters";
      static std::string const PHI                      = "phi";
      static std::string const PHYSIC                   = "physic";
      static std::string const PLANETARY_SYSTEM         = "planetary_system";
      static std::string const PLANETS                  = "planets";
      static std::string const PRESSURE                 = "pressure";
      static std::string const PROFILES                 = "profiles";
      static std::string const RADIAL                   = "radial";
      static std::string const RADIATIVE                = "full_energy";
      static std::string const RADIATIVE_ENERGY         = "radiative_energy";
      static std::string const REFERENTIAL              = "referential";
      static std::string const RELEASE                  = "release";
      static std::string const SEMI_MAJOR_AXIS          = "semi_major_axis";
      static std::string const SOUND_SPEED              = "sound_speed";
      static std::string const SOURCE_CODE              = "source_code";
      static std::string const STEP                     = "step";
      static std::string const STEPS                    = "steps";
      static std::string const STAR                     = "star";
      static std::string const TEMPERATURE              = "temperature";
      static std::string const THETA                    = "theta";
      static std::string const TIME_STEP                = "time_step";
      static std::string const TRACERS                  = "tracers";
      static std::string const TRACKERS                 = "trackers";
      static std::string const TYPE                     = "type";
      static std::string const USER                     = "user";
      static std::string const VALUE                    = "value";
      static std::string const VELOCITY                 = "velocity";
      static std::string const VISCOSITY                = "viscosity";
      
      std::string path(std::initializer_list<std::string> l);
    }
  }
}

#endif 
