// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>
#include <sysexits.h>

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/program_options.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "configLoader.hpp"
#include "configProperties.hpp"
#include "utils.hpp"
#include "simulation.hpp"
#include "h5Labels.hpp"
#include "momentum.hpp"
#include "jsonio.hpp"

namespace po  = boost::program_options;
namespace pt  = boost::property_tree;
namespace fs  = std::filesystem;
namespace HF  = HighFive;
namespace h5l = fargOCA::h5::labels;

using namespace fargOCA;
using namespace h5l;

template<typename T> using opt = std::optional<T>;

namespace cfg = fargOCA::configProperties;

bool
read_cmd_line(int argc, char *argv[], po::variables_map& vm) {
  std::ostringstream usage;
  usage << argv[0] << " <file>.h5 <config>.info [options]+\n"
        << "Load config section from the <config>.info property tree file into the <file>.h5 hdf5 disk file\n."
        << "Will look for the physic, planeraty system and output sections.\n"
        << "Original file will be backed up into <file>.h5-bak first.\n"
        << "Supported options";
  po::options_description opts(usage.str());
  opts.add_options()
    ("help", "produce this message.")
    ("config-file", po::value<std::string>(), "The configuration file.")
    ("disk-file", po::value<std::string>(), "The output hdf5 physic file.")
    ("force", "Overwrite backup file if it already exists. ")
    ;
  
  po::positional_options_description input_option;
  input_option.add("disk-file", 1);
  input_option.add("config-file", 1);
  po::store(po::command_line_parser(argc, argv).
            options(opts).positional(input_option).run(), vm);
  po::notify(vm);
  if (bool(vm.count("help"))) {
    std::cout << opts;
    return false;
  } 
  return true;
}

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world {env.world()};
  
  po::variables_map vm{};
  if (!read_cmd_line(argc, argv, vm)) {
    return 1;
  }
  std::string const disk_file   {vm["disk-file"].as<std::string>()};
  fs::path    const config_file {vm["config-file"].as<std::string>()};
  
  std::ostream& log {fargOCA::log(world)};

  if (config_file.extension() != ".json") {
    if (config_file.extension() == ".info") {
      std::cerr << "Info file is not supported, convert to json and re run.\n";
    } else {
      std::cerr << "Unknown config file format '" << config_file.extension() << "'\n";
    }
    return EX_CONFIG;    
  }
  
  json config {json::parse(std::ifstream{config_file.native()})};
  HF::File file(disk_file, HF::File::ReadWrite);
  HF::Group root {file.getGroup("/")};
  auto physic {config.get<std::shared_ptr<DiskPhysic const>>()};
  if (physic) {
    std::cout << "Physic section found, reloading..." << std::flush;
    if (root.exist(PHYSIC)) {
      root.unlink(PHYSIC);
    }
    physic->writeH5(root.createGroup(PHYSIC));
    std::cout << "done.\n";
  } else {
    std::cout << "Physic section found.\n";
  }
  {
    using namespace h5l;
    using h5l::OUTPUT;
    using h5l::TRACKERS;
    using namespace jsonio;
    auto tracking {get_optional<PluginConfig>(config, {TRACKERS})};
    if (tracking) {
      std::cout << "Output section found, reloading..." << std::flush;
      if (root.exist(OUTPUT)) {
	root.unlink(OUTPUT);
      }
      tracking->writeH5(root.createGroup(OUTPUT).createGroup(TRACKERS));
      std::cout << "done.\n";
    }
  }
  {
    using namespace h5l;
    using namespace jsonio;
    using h5l::DISK;
    using h5l::PHYSIC;
    using h5l::PLANETARY_SYSTEM;
    std::string planetary_physic_path {h5l::path({DISK, PHYSIC, PLANETARY_SYSTEM})};
    auto system_physic {get_optional<PlanetarySystemPhysic>(config, {DISK, PHYSIC, PLANETARY_SYSTEM})};
    if (system_physic) {
      std::cout << "Planetary system section found, reloading..." << std::flush;
      if (root.exist(planetary_physic_path)) {
	root.unlink(planetary_physic_path);
      }
      system_physic->writeH5(root.createGroup(planetary_physic_path));
      std::cout << "done.\n";
    } else {
      std::cout << "No planetary system section found.\n";
    }
  }
  {
    using namespace h5l;
    using namespace jsonio;
    using h5l::NB_STEPS;
    using h5l::TIME_STEP;
    auto nbsteps  {get_optional<int>(config, {NB_STEPS})};
    auto timestep {get_optional<real>(config, {TIME_STEP})};
    if (nbsteps) {
      std::cout << "Reloading nb steps..." << std::flush;
      h5::writeAttribute(root, NB_STEPS, *nbsteps);
      std::cout << "done.\n";
    }
    if (timestep) {
      std::cout << "Reloading time step..." << std::flush;
      h5::writeAttribute(root, TIME_STEP, *timestep);
      std::cout << "done.\n";
    }
  }
  return 0;
}
