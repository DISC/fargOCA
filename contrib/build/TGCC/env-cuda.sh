# Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

echo ">>>>Loading Fargo GCC11/cuda environment"

module switch dfldatadir/gen7233
module switch datadir/gen7233
module use $ALL_CCCHOME/modfiles
module load git/2.25.2
source $ALL_CCCHOME/git/completion/2.25.2.bash
module load git-lfs/3.2.0
module load cmake/3.22.2
module load flavor/buildcompiler/gcc/11 flavor/buildmpi/openmpi/4.0
module load gnu/11.2.0
export CC=gcc
export CXX=g++
export FC=gfortran
module load mpi/openmpi/4.0.5
module load boost/1.74.0
module load hdf5/gnu11/1.12.2
module load cuda/11.6
