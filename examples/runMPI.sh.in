#!/usr/bin/env bash
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

nmpi=1
engine=
[[ -n "$cluster" ]] || export cluster="ignored"

while getopts "n:e:" opt
do
    case $opt in
	n) nmpi=$OPTARG
	   ;;
	e) engine=$OPTARG
	   ;;
	c) cluster=$OPTARG
	   ;;
	:) echo "Option -$OPTARG requires an argument." >&2
	   exit 1
	   ;;
	?) echo "Option -$OPTARG unknown." >&2
	   exit 2
           ;;
    esac
done

outdir=@CMAKE_CURRENT_BINARY_DIR@/out$nmpi$engine
rm -rvf $outdir
mkdir -p -v $outdir

nbsockets=$(grep "physical id" /proc/cpuinfo | sort -u | wc -l)
corespersocket=$(grep "core id" /proc/cpuinfo  | sort -u | wc -l)
let nbcores=$nbsockets*$corespersocket
let nthread=$nbcores/$nmpi
export OMP_NUM_THREADS=$nthread
echo "There are $nbcores cores and $nbsockets sockets on $(hostname)"
echo "launching $nmpi processes with $nthread thread each."

export OMP_PROC_BIND=spread
export OMP_PLACES=threads

if [[ -f @CMAKE_CURRENT_BINARY_DIR@/config$engine.info ]]
then
    @MPILAUNCH@ -v -c $cluster -n 1 \
    @CMAKE_BINARY_DIR@/utils/info2json \
     @CMAKE_CURRENT_BINARY_DIR@/config$engine.info \
     @CMAKE_CURRENT_BINARY_DIR@/config$engine.json || exit -1
fi

@MPILAUNCH@ -v -c $cluster -n $nmpi @CMAKE_BINARY_DIR@/fargoInit $outdir @CMAKE_CURRENT_BINARY_DIR@/config$engine.json -v || exit -1
[[ -f @CMAKE_CURRENT_BINARY_DIR@/add_tracers.sh ]] && source @CMAKE_CURRENT_BINARY_DIR@/add_tracers.sh
@MPILAUNCH@ -v -c $cluster -n $nmpi @CMAKE_BINARY_DIR@/fargOCA $outdir --verbose --uninit-temp-fields-to-NaN || exit -1

@CMAKE_CURRENT_BINARY_DIR@/check.sh $nmpi $engine
