// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <filesystem>
#include "boost/program_options.hpp"
#include "boost/algorithm/string.hpp"

#include <highfive/H5File.hpp>

#include "environment.hpp"
#include "allmpi.hpp"
#include "simulation.hpp"
#include "disk.hpp"

using namespace fargOCA;
namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

bool
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << '\t' << cmd << " <disk.h5>"
        << "Write indirect fields (sound speed, viscosity,..) in the hdf5 file.\n";
  
  po::options_description visible(usage.str());
  po::options_description hidden;
  
  visible.add_options()
    ("help,h", "This help message.")
    ("disk-only",  "assume the file only contain a disk, not the simulation specific data/attributes.")
    ("ofile",   po::value<std::string>(), "output file, over write input file if not provided.");
  
  hidden.add_options()
    ("disk",   po::value<std::string>(), "the simulation hdf5 snapshot");

  po::positional_options_description pos; 
  pos.add("disk", 1);
  try {
    po::options_description opts;
    opts.add(visible).add(hidden);
    po::store(po::command_line_parser(argc, argv).options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch(const po::error& e) {
    std::cerr << "Couldn't parse command line arguments properly:\n"
              << e.what() << '\n' << '\n'
              << visible << '\n';
    return false;
  }
  if(vm.count("help")) {
    std::cout << visible << "\n";
    return false;
  }
  for(std::string o : {"disk"}) {
    if (!bool(vm.count(o))) {
      std::cerr << "Missing mandatory option '" << o << "'.\n";
      return false;
    }
  }
  return true;
}

int
main(int argc, char** argv) {
  Environment env(argc, argv);
  if (env.world().size() != 1) {
    std::cerr << "Command '" << argc << "' is only supported on one MPI process.\n";
    return EX_USAGE;    
  }
  po::variables_map vm;
  if (!read_cmd_line(argc, argv, vm)) {
    return EX_USAGE;
  }

  auto export_indirects = [&]<class D>(shptr<D> snapshot) -> bool {
    std::string ipath {vm["disk"].as<std::string>()};
    if (!fs::exists(ipath)) {
      std::cerr << "Not such file: " << ipath << '\n';
      return EX_NOINPUT;
    }

    {
      HF::File ifile{ipath, HF::File::ReadOnly};
      snapshot = D::make(env.world(), ifile.getGroup("/"));
    }
    {
      std::string opath {vm.count("ofile")>0 ? vm["ofile"].as<std::string>() : ipath};
      HF::File ofile {opath, HF::File::Truncate};
      snapshot->writeH5(ofile.getGroup("/"), 0, true);
    }
    return EX_OK;
  };
  return bool(vm.count("disk-only")) ? export_indirects(shptr<Disk>{}) : export_indirects(shptr<Simulation>{});
}

