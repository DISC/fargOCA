// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2020, Gabriele Pichierri, pichierri<at>mpia.de
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "planetarySystemEngine.hpp"
#include "planetarySystem.hpp"
#include "configProperties.hpp"
#include "loops.hpp"
#include "codeUnits.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace kkm = Kokkos::Experimental;

#define LBD KOKKOS_LAMBDA
  
  std::string const RUNGE_KUTTA = "Runge_Kutta";
  
  PlanetarySystemEngine::MakerMap&
  PlanetarySystemEngine::makers() {
    static MakerMap m;
    return m;
  }
  
  std::vector<std::string>
  PlanetarySystemEngine::available() {
    std::vector<std::string> names;
    for (auto const& [k,e] : makers()) { names.push_back(k); }
    return names;
  }

  std::unique_ptr<PlanetarySystemEngine>
  PlanetarySystemEngine::make(std::string name, PlanetarySystem& s, Config const& cfg) {
    auto found = makers().find(name);
    if (found == makers().end()) {
      std::cerr << "Unknown planetary system engine '" << name << "'.\n"
                << "Available engines are: ";
      for (auto e : available()) { std::cerr << e << " "; }
      std::cerr << '\n';
      std::exit(EX_USAGE);
    }
    return (found->second)(name, s, cfg);
  }
  
  PlanetarySystemEngine::PlanetarySystemEngine(std::string n, PlanetarySystem& s, Config const& cfg)
    : myName(n), mySystem(s), myConfig(cfg) {}

  PlanetarySystemEngine::~PlanetarySystemEngine() {}
  
  namespace detail {

    enum CoordIdx : int       { X = 0, Y = 1, Z = 2 };

    /// Probably transcoded from fortran
    void
    DerivMotionRK5(DiskPhysic const& physic,
		   arr3d<real const> iplanets,
                   arr1d<real const> mass,
                   arr1d<bool>       feelOthers,
                   real              dt,
                   arr3d<real>       oplanets)
    {
      int np = iplanets.extent(0);
      constexpr int POS = 0;
      constexpr int VEL = 1;
      real const centralBodyMass = physic.units.centralBodyMass();
      arr1d<real> GinvDist3("G/dist(planets)^3", np);
      kk::parallel_for("G/dist(planets)^3", kk::RangePolicy<>(0, np),
                       LBD(int i) {
                         auto pos = kk::subview(iplanets, i, POS, kk::ALL());
                         GinvDist3(i) = G/ipow<3>(norm(pos(X), pos(Y), pos(Z)));
                       });
      
      kk::parallel_for(kk::TeamPolicy<>(np, kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         int i = team.league_rank();
                         oplanets(i,POS,X) = iplanets(i,VEL,X);
                         oplanets(i,POS,Y) = iplanets(i,VEL,Y);
                         oplanets(i,POS,Z) = iplanets(i,VEL,Z);
                         oplanets(i,VEL,X) = -GinvDist3(i)*centralBodyMass*iplanets(i,POS,X);
                         oplanets(i,VEL,Y) = -GinvDist3(i)*centralBodyMass*iplanets(i,POS,Y);
                         oplanets(i,VEL,Z) = -GinvDist3(i)*centralBodyMass*iplanets(i,POS,Z);
                         kk::parallel_for(kk::TeamThreadRange(team, np),
                                          [&](int j) {
                                            if( j == i || feelOthers(i) ) {
                                              oplanets(i,VEL,X) -= GinvDist3(j)*mass(j)*iplanets(j,POS,X);
                                              oplanets(i,VEL,Y) -= GinvDist3(j)*mass(j)*iplanets(j,POS,Y);
                                              oplanets(i,VEL,Z) -= GinvDist3(j)*mass(j)*iplanets(j,POS,Z);
                                            }
                                            if ((j != i) && feelOthers(i) ) {
                                              Triplet dji(iplanets(j,POS,X)-iplanets(i,POS,X),
                                                          iplanets(j,POS,Y)-iplanets(i,POS,Y),
                                                          iplanets(j,POS,Z)-iplanets(i,POS,Z));
                                              real const dist3 = ipow<3>(dji.norm());
                                              oplanets(i,VEL,X) += G*mass(j)/dist3*dji.x;
                                              oplanets(i,VEL,Y) += G*mass(j)/dist3*dji.y;
                                              oplanets(i,VEL,Z) += G*mass(j)/dist3*dji.z;
                                            }
                                          });
                       });
      
      kk::parallel_for(kk::MDRangePolicy<kk::Rank<3>>({0,0,0},{np,2,3}),
                       LBD(int i, int pv, int c) {
                         oplanets(i,pv,c) *= dt;
                       });
    }
    
    void
    TranslatePlanetRK5(arr3d<real const> iplanets, 
                       real c1,
                       real c2,
                       real c3,
                       real c4,
                       real c5,
                       arr3d<real const> k1,
                       arr3d<real const> k2,
                       arr3d<real const> k3,
                       arr3d<real const> k4,
                       arr3d<real const> k5,
                       arr3d<real> oplanets)
                       
    {
      kk::parallel_for("translate_planet_RK5", kk::MDRangePolicy<kk::Rank<3>>({0,0,0}, {iplanets.extent(0),iplanets.extent(1),iplanets.extent(2)}),
                       LBD(int i, int f, int t) {
                         oplanets(i,f,t) = iplanets(i,f,t)+c1*k1(i,f,t)+c2*k2(i,f,t)+c3*k3(i,f,t)+c4*k4(i,f,t)+c5*k5(i,f,t); 
                       });
    }
    
    void
    RungeKunta(DiskPhysic const& physic, arr3d<real const> iplanets,
               arr1d<real const> mass,
               arr1d<bool> feelOthers,
               real dt,
               arr3d<real> oplanets)
    {
      arr3d<real> k1("k1", iplanets.layout());
      arr3d<real> k2("k2", iplanets.layout());
      arr3d<real> k3("k3", iplanets.layout());
      arr3d<real> k4("k4", iplanets.layout());
      arr3d<real> k5("k5", iplanets.layout());
      arr3d<real> k6("k6", iplanets.layout());
      
      DerivMotionRK5 (physic, iplanets, mass, feelOthers, dt, k1);
      TranslatePlanetRK5 (iplanets, 0.2, 0.0, 0.0, 0.0, 0.0, k1, k2, k3, k4, k5, oplanets);
      DerivMotionRK5 (physic, oplanets, mass, feelOthers, dt, k2);
      TranslatePlanetRK5 (iplanets, 0.075, 0.225, 0.0, 0.0, 0.0, k1, k2, k3, k4, k5, oplanets);
      DerivMotionRK5 (physic, oplanets, mass, feelOthers, dt, k3);
      TranslatePlanetRK5 (iplanets, 0.3, -0.9, 1.2, 0.0, 0.0, k1, k2, k3, k4, k5, oplanets);
      DerivMotionRK5 (physic, oplanets, mass, feelOthers, dt, k4);
      TranslatePlanetRK5 (iplanets, -11.0/54.0, 2.5, -70.0/27.0, 35.0/27.0, 0.0, k1, k2, k3, k4, k5, oplanets);
      DerivMotionRK5 (physic, oplanets, mass, feelOthers, dt, k5);
      TranslatePlanetRK5 (iplanets, 1631.0/55296.0, 175.0/512.0, 575.0/13824.0, 44275.0/110592.0, 253.0/4096.0, k1, k2, k3, k4, k5, oplanets);
      DerivMotionRK5 (physic, oplanets, mass, feelOthers, dt, k6);
      TranslatePlanetRK5 (iplanets, 
                          37.0/378.0, 0, 250.0/621.0, 125.0/594.0, 512.0/1771.0, 
                          k1, k2, k3, k4, k6, oplanets);
    }
  }

  class RungeKuttaEngine 
    : public PlanetarySystemEngine {
  public:
    RungeKuttaEngine(std::string name, PlanetarySystem& s, Config const& cfg) : PlanetarySystemEngine(name, s, cfg) {}
    
    virtual void advance(real dt) override {
      std::map<std::string, Planet>& planets = system().planets();
      int nb = planets.size();
      if (nb > 0) {
        constexpr int POS = 0;
        constexpr int VEL = 1;

        arr3d<real> iplanets("planets_pos_vel", nb,2,3);
        arr1d<real> mass("planets_mass", nb);
        arr1d<bool> feelOthers("feel others", nb);
        {
          arr3d<real>::HostMirror hiplanets         = kk::create_mirror(iplanets);
          arr1d<real>::HostMirror hmass       = kk::create_mirror(mass);
          arr1d<bool>::HostMirror hfeelOthers = kk::create_mirror(feelOthers);
          int i = 0;
          for (auto const& [name, planet] : planets) {
            using namespace detail;
            Triplet pos = planet.position();
            Triplet vel = planet.velocity();
            hiplanets(i,POS,X)      = pos.x;
            hiplanets(i,POS,Y)      = pos.y;
            hiplanets(i,POS,Z)      = pos.z;
            hiplanets(i,VEL,X)      = vel.x;
            hiplanets(i,VEL,Y)      = vel.y;
            hiplanets(i,VEL,Z)      = vel.z;
            hmass(i)          = planet.mass();
            hfeelOthers(i)    = system().physic().planetarySystem()->planet(name).feelOthers;
            ++i;
          }
          kk::deep_copy(iplanets, hiplanets);
          kk::deep_copy(mass, hmass);
          kk::deep_copy(feelOthers, hfeelOthers);
        }
        arr3d<real> oplanets("oplanets",planets.size(),2,3);
        detail::RungeKunta (system().physic(), iplanets, mass, 
                            feelOthers,
                            dt, oplanets);
        arr3d<real>::HostMirror hoplanets = kk::create_mirror(oplanets);
        kk::deep_copy(hoplanets, oplanets);
        {
          int i = 0;
          for (auto& [name, planet] : planets) {
            using namespace detail;
            planet.setPosition(Triplet(hoplanets(i,POS,X), hoplanets(i,POS,Y), hoplanets(i,POS,Z)));
            planet.setVelocity(Triplet(hoplanets(i,VEL,X), hoplanets(i,VEL,Y), hoplanets(i,VEL,Z)));
            ++i;
          }
        }
      } 
    }
  };
  
  namespace detail {
    PlanetarySystemEngine::Declared<RungeKuttaEngine> RungeKutta(RUNGE_KUTTA);
  }
}
