# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load git git-lfs cmake/3.25.2
module load intel-oneapi-all/2023.1
module load cuda/12.2.0 openmpi/4.1.1-cuda boost/1.74.0-mpi-cuda hdf5/1.12.0 

unset Kokkos_DIR
export CC=icx
export CXX=icpx
export FC=ifx
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
