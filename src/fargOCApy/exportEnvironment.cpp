// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>

#include "environment.hpp"
#include "fargOCApy.hpp"

namespace py = pybind11;

namespace fargOCA {
  namespace py11 {
    void exportCommunicator(py::module_& m) {
      py::class_<fmpi::communicator>(m, "communicator")
        .def(py::init<>());
    }
    
    void exportEnvironment(py::module_& m) {
      py::class_<Environment, std::shared_ptr<Environment>>(m, "Environment")
        .def(py::init<>())
        .def_property_readonly("world", 
                               [](Environment const& e) { return e.world(); },
                               py::return_value_policy::reference_internal)
        .def("__repr__",
             [](Environment const& env) {
               return "fargOCApy.Environment for Kokkos and MPI";
             });
      
      // this one is always needed nayway
      m.attr("env") = pybind11::cast(std::make_shared<Environment>());
    }
  }
}
