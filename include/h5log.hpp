// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_H5LOG_H
#define FARGOCA_H5LOG_H

#include "h5io.hpp"

namespace fargOCA { 
  namespace h5 {
    
    /// \brief Return a group for logging an entity of given type and name.
    /// The log group is a direct sub group of root, has the same name as
    /// the logged entity, and contains a dataset named "log".
    /// If it already exists, the group is returned.
    /// Otherwise such a group is created and a dataset the the Entity HDF5
    /// type description is created.
    /// \param root parent group
    /// \param name entity name
    /// \temp  L entity type
    template<class L>
    HighFive::Group
    getNamedLogGroup(HighFive::Group root, std::string name) {
      if (!root.exist(name)) {
        using namespace HighFive;
        auto logGrp = root.createGroup(name);
        DataSpace logSpace({0}, {DataSpace::UNLIMITED});
        DataSetCreateProps props;
        props.add(Chunking(std::vector<hsize_t>({1})));
        logGrp.createDataSet("log", logSpace, create_datatype<L>(), props);
      }
      return root.getGroup(name);
    }
    
    template<class E>
    void
    writeLog(HighFive::Group root, std::string name, std::vector<E> const& log) {
      if (log.size() > 0) {
        using namespace HighFive;
        // get current state
        Group     logGrp    = h5::getNamedLogGroup<E>(root, name);
        DataSet   dataSet   = logGrp.getDataSet("log");
	size_t const base = dataSet.getDimensions()[0];
	std::vector<size_t> sz = {base+log.size()};
        dataSet.resize(sz);
        dataSet.select({base}, {log.size()}).write(log);
      }
    }
    
    template<class E>
    void
    writeLogs(HighFive::Group root, std::map<std::string, std::vector<E>> const& logs) {
      for(auto const& [name,log] : logs) {
        writeLog(root, name, log);
      }
    }

    template<class E>
    std::vector<E>
    readLog(HighFive::Group root, std::string name) {
      using namespace HighFive;
      std::vector<E> log;
      Group     logGrp    = h5::getNamedLogGroup<E>(root, name);
      DataSet   dataSet   = logGrp.getDataSet("log");
      dataSet.read(log);
      return log;
    }
    
    template<class E>
    std::map<std::string, std::vector<E> > 
    readLogs(HighFive::Group root) {
      std::map<std::string, std::vector<E>> logs;
      for(std::string name : root.listObjectNames()) {
        logs[name] = readLog<E>(root, name);
      }
      return logs;
    }
  }
}
#endif
