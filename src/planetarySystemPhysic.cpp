// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <cassert>

#include <boost/algorithm/string.hpp>

#include "planetarySystemPhysic.hpp"
#include "io.hpp"
#include "optIO.hpp"
#include "codeUnits.hpp"
#include "h5Labels.hpp"
#include "simulationConfig.hpp"
#include "jsonio.hpp"
#include "configProperties.hpp"

namespace fargOCA {
  namespace HF = HighFive;
  namespace lb = h5::labels;

  std::ostream&
  operator<<(std::ostream& out, OrbitalElements const& oe) {
    std::cout << "{a:" << oe.semiMajorAxis
              << ",e:" << oe.eccentricity
              << ",i:" << oe.inclination
              << ",l:" << oe.meanLongitude
              << ",v:" << oe.longitudeOfPericenter
              << ",O:" << oe.longitudeOfNode 
              << "}";
    return out;
  }

  PlanetarySystemPhysic::PlanetarySystemPhysic( std::map<std::string, Planet> const& plts,
                                                bool ex, bool acc, Engine const& eng) 
    : planets(plts),
      excludeHill(ex), accreteOntoPlanets(acc), engine(eng)
  {
    assert(planets.size() > 0);
  }
  
  PlanetarySystemPhysic::PlanetarySystemPhysic(HighFive::Group grp) {
    readH5(grp);
    assert(planets.size() > 0);
  }
  
  void
  PlanetarySystemPhysic::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Planetary System Config:\n"
        << tabs(tab+1) << "Release:\n"
        << tabs(tab+1) << " * excludeHill: " << yesno(excludeHill) << '\n'
        << tabs(tab+1) << " * accreteOntoPlanets: " << yesno(accreteOntoPlanets) << '\n';
    out << tabs(tab+1) << "Planets: ";
    if (planets.size() == 0) {
      out << "Nope.\n";
    } else {
      out << "\n";
      for( auto const& np : planets) {
        out << tabs(tab+1) << "* " << np.first << '\n';
        np.second.dump(out, tab+2);
      }
    }
    engine.dump(out, tab+1);
  }

  void
  PlanetarySystemPhysic::writeH5(HighFive::Group grp) const {
    assert(planets.size() > 0);
    using h5::writeAttribute;
    {
      auto planetsGrp = grp.createGroup("planets");
      for (auto const& [name, planet] : planets) {
        auto planetGrp = planetsGrp.createGroup(name);
        planet.writeH5(planetGrp);
      }
    }
    writeAttribute(grp, "exclude_hill", excludeHill);
    writeAttribute(grp, "accrete_onto_planets", accreteOntoPlanets);
    engine.writeH5(grp);
  }
  
  void
  PlanetarySystemPhysic::readH5(HF::Group const& grp) {
    auto planetsGrp = grp.getGroup("planets");
    for (std::string name : planetsGrp.listObjectNames()) {
      planets.emplace(name, Planet(planetsGrp.getGroup(name)));
    }
    excludeHill        = bool(grp.getAttribute("exclude_hill").read<int>());
    accreteOntoPlanets = bool(grp.getAttribute("accrete_onto_planets").read<int>());
    engine.readH5(grp);
  }

  real 
  PlanetarySystemPhysic::Planet::initialMass() const {
    struct {
      real operator()(real r) const { return r;}
      real operator()(MassTaper const& t) const { return t.initialMass; }
    } visitor;
    return std::visit(visitor, mass);    
  }
  
  void
  PlanetarySystemPhysic::Planet::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "mass: \t";
    if (std::holds_alternative<real>(mass)) {
      out << std::get<real>(mass) << '\n';
    } else if (std::holds_alternative<MassTaper>(mass)) {
      out << "taper \n";
      std::get<MassTaper>(mass).dump(out, tab+1);
    } else {
      out << "<error>\n";
    }
    out << tabs(tab) << "radius: \t" << radius << '\n'
        << tabs(tab) << "Hill radius: \t" << hillRadius << '\n'
        << tabs(tab) << "orbital elements: \n"
        << tabs(tab+1) << "semi major axis: \t"     << orbitalElements.semiMajorAxis << '\n'
        << tabs(tab+1) << "eccentricity: \t" << orbitalElements.eccentricity << '\n'
        << tabs(tab+1) << "inclination: \t"  << orbitalElements.inclination << '\n'
        << tabs(tab+1) << "inclination: \t"  << orbitalElements.inclination << '\n'
        << tabs(tab+1) << "mean longitude: \t"  << orbitalElements.meanLongitude << '\n'
        << tabs(tab+1) << "longitude of pericenter: \t"  << orbitalElements.longitudeOfPericenter << '\n'
        << tabs(tab+1) << "longitude of node: \t"  << orbitalElements.longitudeOfNode << '\n'
        << tabs(tab) << "accretion time: \t" << accretionTime << '\n'
        << tabs(tab) << "Hill radius: \t" << hillRadius  << '\n'
        << tabs(tab) << "feel: disk->" << yesno(feelDisk) << ", other planet->" << yesno(feelOthers) << '\n'
        << tabs(tab) << "luminosity: ";
    if (!luminosity) {
      out << "nope\n";
    } else {
      out << "\n";
      luminosity->dump(out, tab+1);
    }
  }

  PlanetarySystemPhysic::Planet::Planet(std::string nm, 
                                        std::variant<real,MassTaper> m,
                                        OrbitalElements elements,
                                        std::optional<real> acc,
                                        bool fdisk,
                                        bool fothers,
                                        std::optional<Luminosity> sacc,
                                        std::optional<real> r,
                                        std::optional<real> hr)
  : name(nm),
    mass(m),
    orbitalElements(elements),
    accretionTime(acc),
    feelDisk(fdisk),
    feelOthers(fothers),
    luminosity(sacc ? std::optional<Luminosity>(std::in_place, *sacc) : std::nullopt),
    radius(r),
    hillRadius(hr)
  {
  }
  
  void
  PlanetarySystemPhysic::Planet::writeH5(HighFive::Group& grp) const {
    grp.createAttribute(lb::NAME, name);
    if (std::holds_alternative<real>(mass)) {
      grp.createAttribute("mass", std::get<real>(mass));
    } else if (std::holds_alternative<MassTaper>(mass)) {
      auto sg = grp.createGroup("mass_taper");
      std::get<MassTaper>(mass).writeH5(sg);
    }
    grp.createAttribute("semi_major_axis", orbitalElements.semiMajorAxis);
    grp.createAttribute("eccentricity", orbitalElements.eccentricity);
    grp.createAttribute("inclination", orbitalElements.inclination);
    grp.createAttribute("mean_longitude", orbitalElements.meanLongitude);
    grp.createAttribute("longitude_of_pericenter", orbitalElements.longitudeOfPericenter);
    grp.createAttribute("longitude_of_node", orbitalElements.longitudeOfNode);
    if (accretionTime) {
      grp.createAttribute("accretion_time", *accretionTime);
    }
    grp.createAttribute("feel_disk", int(feelDisk));
    grp.createAttribute("feel_others", int(feelOthers));
    if (luminosity) {
      auto sg = grp.createGroup("luminosity");
      luminosity->writeH5(sg);
    }
    if (radius)     { grp.createAttribute("radius", *radius); }
    if (hillRadius) { grp.createAttribute("Hill_radius", *hillRadius); }
  }
  
  PlanetarySystemPhysic::Planet::Planet(HighFive::Group grp)
    : Planet(*h5::attributeValue<std::string>(grp, lb::NAME),
	     grp.exist("mass_taper")
	     ? std::variant<real,MassTaper>(MassTaper(grp.getGroup("mass_taper")))
	     : std::variant<real,MassTaper>(*h5::attributeValue<real>(grp, "mass")),
	     {*h5::attributeValue<real>(grp, "semi_major_axis"),
	      *h5::attributeValue<real>(grp, "eccentricity"),
	      *h5::attributeValue<real>(grp, "inclination"),
	      h5::attributeValue(grp, "mean_longitude", real(0)),
	      h5::attributeValue(grp, "longitude_of_pericenter", real(0)),
	      h5::attributeValue(grp, "longitude_of_node", real(0))},
	     h5::attributeValue<real>(grp, "accretion_time"),
	     bool(*h5::attributeValue<int>(grp,"feel_disk")),
	     bool(*h5::attributeValue<int>(grp,"feel_others")),
	     grp.exist("luminosity")
	     ? std::optional<Luminosity>(std::in_place, grp.getGroup("luminosity"))
	     : std::nullopt,
	     h5::attributeValue<real>(grp, "radius"),
	     h5::attributeValue<real>(grp, "Hill_radius"))
  {}
  
  PlanetarySystemPhysic::Planet::Luminosity::Luminosity(real acc)
    : userSolidAccretion(acc)
  {
    assert(acc > 0);
  }

  PlanetarySystemPhysic::Planet::Luminosity::Luminosity(HighFive::Group const& grp)
    : Luminosity(*h5::attributeValue<real>(grp, "solid_accretion")) {}
  
  std::optional<real>
  PlanetarySystemPhysic::Planet::solidAccretion(CodeUnits const& units) const {
    if (luminosity) {
      return luminosity->userSolidAccretion*ipow<3>(units.time())/(units.centralBodyMass()*units.sunMass())/ipow<2>(units.distance());
    } else {
      return std::nullopt;
    }
  }
  
  void
  PlanetarySystemPhysic::Planet::Luminosity::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Planet Luminosity:\n"
        << tabs(tab+1) << "* solid accretion (user): " << userSolidAccretion << '\n';
  }

  void
  PlanetarySystemPhysic::Planet::Luminosity::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "solid_accretion", userSolidAccretion);
  }

  PlanetarySystemPhysic::Planet::MassTaper::MassTaper(real i, real f, real b, real e)
    : initialMass(i), finalMass(f),
      beginTime(b), endTime(e) {
    if (initialMass < 0 || finalMass < 0) {
      std::cerr << "Planet masses cannot, to our current knowledge, be negative.\n";
      std::exit(EINVAL);
    }
    if (beginTime >= endTime) {
      std::cerr << "Mass taper must start before it finishes.\n";
      std::exit(EINVAL);
    }
  }

  PlanetarySystemPhysic::Planet::MassTaper::MassTaper(HighFive::Group const& grp) {
    readH5(grp);
  }
  
  real
  PlanetarySystemPhysic::Planet::MassTaper::massAt(real time) const {
    if (before(time)) {
      return initialMass;
    } else if (active(time)) {
      real dmass = finalMass - initialMass;
      real taper = (time - beginTime)/(endTime-beginTime);
      taper = taper > 1 ? 1 : ipow<2>(std::sin(taper*PI/2));
      return dmass*taper + initialMass;
    } else {
      return finalMass;
    }
  }
  
  void
  PlanetarySystemPhysic::Planet::MassTaper::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Planet MassTaper:\n"
        << tabs(tab+1) << "* mass (initial,final) " << initialMass << ", " << finalMass << '\n'
        << tabs(tab+1) << "* time (begin,end) " << beginTime << ", " << endTime << '\n';
  }

  void
  PlanetarySystemPhysic::Planet::MassTaper::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "initial_mass", initialMass);
    h5::readAttribute(grp, "final_mass", finalMass);
    h5::readAttribute(grp, "begin_time", beginTime);
    h5::readAttribute(grp, "end_time", endTime);
  }
  
  void
  PlanetarySystemPhysic::Planet::MassTaper::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "initial_mass", initialMass);
    h5::writeAttribute(grp, "final_mass", finalMass);
    h5::writeAttribute(grp, "begin_time", beginTime);
    h5::writeAttribute(grp, "end_time", endTime);
  }

  void
  PlanetarySystemPhysic::Engine::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Engine: " << name << "\n";
    config.dump(out, tab+1);
  }

  void
  PlanetarySystemPhysic::Engine::readH5(HighFive::Group const& grp) {
    using namespace h5::labels;
    if (grp.exist(ENGINE)) {
      auto eng = grp.getGroup(ENGINE);
      h5::readAttribute(eng, NAME, name);
      config = Config(eng.getGroup(CONFIG));
    } else {
      name = "Runge_Kutta";
    }
  }
  
  void
  PlanetarySystemPhysic::Engine::writeH5(HighFive::Group& grp) const {
    using namespace h5::labels;
    auto eng = grp.createGroup(ENGINE);    
    h5::writeAttribute(eng, NAME, name);
    config.writeH5(eng, "config");
  }

  using namespace configProperties;
  using namespace jsonio;
  
  void
  to_json(nlohmann::json& j, PlanetarySystemPhysic const& s) {
    json planets;
    for(auto const& [name,planet] : s.planets) {
      planets[name] = planet;
    }
    j[snake(PLANETS)] = planets;
    set_to(j, snake(EXCLUDE_HILL), s.excludeHill);
    set_to(j, snake(ACCRETE_ONTO_PLANET), s.accreteOntoPlanets);
    set_to(j, snake(ENGINE), s.engine);
  }
  
  void
  from_json(nlohmann::json const& j, PlanetarySystemPhysic& s) {
    using Planet = PlanetarySystemPhysic::Planet;
    std::vector<Planet> planets;
    json jplanets = j.at(snake(PLANETS));
    for(auto const& jplanet : jplanets) {
      Planet planet;
      from_json(jplanet, planet);
      s.planets.insert(std::make_pair(planet.name, planet));
    }
    get_to(j, snake(EXCLUDE_HILL), s.excludeHill);
    get_to(j, snake(ACCRETE_ONTO_PLANET), s.accreteOntoPlanets);
    get_to(j, snake(ENGINE), s.engine);
  }
  
  void
  to_json(nlohmann::json& j, PlanetarySystemPhysic::Engine const& e) {
    set_to(j, snake(NAME), e.name);
    set_to(j, snake(CONFIG), e.config);
  }
  
  void
  from_json(nlohmann::json const& j, PlanetarySystemPhysic::Engine& e) {
    get_to(j, snake(NAME), e.name);
    get_to(j, snake(CONFIG), e.config);
  }
  
  void
  to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet const& p) {
    set_to(j, snake(NAME), p.name);
    set_to(j, snake(ORBITAL_ELEMENTS), p.orbitalElements);
    set_to(j, snake(ACCRETION_TIME), p.accretionTime);
    nlohmann::json jfeel;
    jfeel[snake(DISK)]   = p.feelDisk;
    jfeel[snake(OTHERS)] = p.feelOthers;
    j[snake(FEEL)] = jfeel;
    set_to(j, snake(LUMINOSITY), p.luminosity);
    set_to(j, snake(MASS), p.mass);
    set_to(j, snake(RADIUS), p.radius);
    set_to(j, snake(HILL_RADIUS), p.hillRadius);
  }
  
  void
  from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet& p) {
    get_to(j, snake(NAME), p.name);
    get_to(j, snake(ORBITAL_ELEMENTS), p.orbitalElements);
    get_to(j, snake(ACCRETION_TIME), p.accretionTime);
    nlohmann::json jfeel = j.at(snake(FEEL));
    get_to(jfeel, snake(DISK),   p.feelDisk);
    get_to(jfeel, snake(OTHERS), p.feelOthers);
    if (j.contains(snake(LUMINOSITY))) {
      p.luminosity = PlanetarySystemPhysic::Planet::Luminosity{};
      get_to(j, snake(LUMINOSITY), p.luminosity);
    }
    get_to(j, snake(MASS), p.mass);
    get_to(j, snake(RADIUS), p.radius);
    get_to(j, snake(HILL_RADIUS), p.hillRadius);
  }

  void
  to_json(nlohmann::json& j, OrbitalElements const& e) {
    set_to(j, snake(SEMI_MAJOR_AXIS), e.semiMajorAxis);
    set_to(j, snake(ECCENTRICITY), e.eccentricity);
    set_to(j, snake(INCLINATION), e.inclination);
    set_to(j, snake(MEAN_LONGITUDE), e.meanLongitude);
    set_to(j, snake(LONGITUDE_OF_PERICENTER), e.longitudeOfPericenter);
    set_to(j, snake(LONGITUDE_OF_NODE), e.longitudeOfNode);
  }

  void
  from_json(nlohmann::json const& j, OrbitalElements& e) {
    get_to(j, snake(SEMI_MAJOR_AXIS), e.semiMajorAxis);
    get_to(j, snake(ECCENTRICITY), e.eccentricity);
    get_to(j, snake(INCLINATION), e.inclination);
    get_to(j, snake(MEAN_LONGITUDE), e.meanLongitude);
    get_to(j, snake(LONGITUDE_OF_PERICENTER), e.longitudeOfPericenter);
    get_to(j, snake(LONGITUDE_OF_NODE), e.longitudeOfNode);
  }
  
  void
  to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet::Luminosity const& p) {
    set_to(j, snake(SOLID_ACCRETION), p.userSolidAccretion);
  }

  void
  from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet::Luminosity& p) {
    get_to(j, snake(SOLID_ACCRETION), p.userSolidAccretion);
  }

  void
  to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet::MassTaper const& p) {
    set_to(j, snake(INITIAL_MASS), p.initialMass);
    set_to(j, snake(FINAL_MASS), p.finalMass);
    set_to(j, snake(BEGIN_TIME), p.beginTime);
    set_to(j, snake(END_TIME), p.endTime);
  }
  
  void
  from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet::MassTaper& p) {
    get_to(j, snake(INITIAL_MASS), p.initialMass);
    get_to(j, snake(FINAL_MASS), p.finalMass);
    get_to(j, snake(BEGIN_TIME), p.beginTime);
    get_to(j, snake(END_TIME), p.endTime);
  }

  void
  to_json(nlohmann::json& j, PlanetarySystemPhysic::Planet::Mass const& m) {
    using MassTaper = PlanetarySystemPhysic::Planet::MassTaper;
    if (std::holds_alternative<real>(m)) {
      j = std::get<real>(m);
    } else if (std::holds_alternative<MassTaper>(m)) {
      nlohmann::json jtaper = std::get<MassTaper>(m);
      j[snake(TAPER)] = jtaper;
    } else {
      std::abort();
    }
  }
  
  void
  from_json(nlohmann::json const& j, PlanetarySystemPhysic::Planet::Mass& m) {
    using MassTaper = PlanetarySystemPhysic::Planet::MassTaper;
    if (j.is_object()) { // more than a real
      if (j.contains(snake(TAPER))) {
	MassTaper mass;
	get_to(j, snake(TAPER), mass);
	m = mass;
      } else {
	std::cerr << "Error: Mass is neither specified as a value or as a Taper spec.\n";
	std::abort();
      }
    } else {
      m = j.get<real>();
    }
  }
}
