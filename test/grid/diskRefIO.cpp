// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>

#include "diskPhysic.hpp"

using namespace fargOCA;
using namespace std;

int 
main() {
  
  DiskReferential arr[] = {
    DiskReferential::CONSTANT,
    DiskReferential::COROTATING
  };
  bool passed = true;
  for (auto w : arr) {
    ostringstream out;
    out << w;
    DiskReferential r = static_cast<DiskReferential>(-1);
    istringstream in(out.str());
    in >> r;
    if ( w == r ) {
      std::cout << w << " PASSED\n";
    } else {
      std::cout << w << " FAILED (read " << r
		<< " from " << out.str() << ")\n";
      passed = false;
    }
  }
  if (passed) {
    return 0;
  } else {
    return 1;
  }
}
