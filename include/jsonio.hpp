// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_DETAILS_JSON_UTILS_H
#define FARGOCA_DETAILS_JSON_UTILS_H

#include <nlohmann/json.hpp>

#include "configProperties.hpp"

namespace fargOCA {
  namespace jsonio {

    using namespace configProperties;

    template<typename ENUM>
    using Enum2Str = std::pair<ENUM, std::string>;

    template<typename ENUM>
    using Enum2StrVec = std::vector<Enum2Str<ENUM>>;
    
    /// \brief map enum values to strings
    /// Need to be specialized for each enum to map in json
    template<typename ENUM>  Enum2StrVec<ENUM>  buildEnumMap();
    
    template<typename ENUM>
    void
    to_json(nlohmann::json& j, ENUM const& e) {
      static_assert(std::is_enum<ENUM>::value, " must be an enum!");
      auto const& m = buildEnumMap<ENUM>();
      auto it = std::find_if(std::begin(m), std::end(m), [e](auto const& e2s) { return e == e2s.first; });
      j = (it == std::end(m) ? std::string("<ERROR>") : it->second);
    }
    
    template<typename ENUM>
    void from_json(nlohmann::json const& j, ENUM& e) {
      static_assert(std::is_enum<ENUM>::value, " must be an enum!");
      auto const& m = buildEnumMap<ENUM>();
      auto it = std::find_if(std::begin(m), std::end(m), [j](auto const& e2s) { return j == e2s.second; });
      e = (it == std::end(m) ? static_cast<ENUM>(-1) : it->first);
    }
    
    template<typename T>
    std::optional<T>
    get_optional(nlohmann::json const& cfg, std::string key) {
      if (cfg.contains(key)) {
	return std::optional<T>(cfg.at(key).get<T>());
      } else {
	return std::nullopt;
      }
    }

    template<typename T>
    std::optional<T>
    get_optional(nlohmann::json const& cfg, std::forward_list<std::string> keys) {
      if (keys.empty()) {
	return std::optional<T>(cfg.get<T>());
      } else {
	auto key {keys.front()};
	keys.pop_front();
	if (keys.empty()) {
	  return get_optional<T>(cfg, key);
	} else {
	  return get_optional<T>(cfg.at(key), keys);
	}
      }
    }

    template<typename T>
    std::shared_ptr<T>
    get_shared(nlohmann::json const& cfg, std::string key) {
      if (cfg.contains(key)) {
	return std::shared_ptr<T>(cfg.at(key).get<T>());
      } else {
	return nullptr;
      }
    }

    template<typename T>
    void
    get_to(nlohmann::json const& cfg, std::string key, std::optional<T>& v) {
      v = get_optional<T>(cfg, key);
    }

    template<typename T>
    void
    get_to(nlohmann::json const& cfg, std::string key, std::shared_ptr<T>& v) {
      v = get_shared<T>(cfg, key);
    }
    
    template<typename T>
    void
    get_to(nlohmann::json const& cfg, std::string key, T& v) {
      cfg[key].get_to<T>(v);
    }
    
    template<typename T>
    void
    set_to(nlohmann::json& cfg, std::string key, T const& v) {
      cfg[key] = v;
    }
    
    template<typename T>
    void
    set_to(nlohmann::json& cfg, std::string key, std::optional<T> const& v) {
      if (v) {
	cfg[key] = *v;
      }
    }

    template<typename T>
    void
    set_to(nlohmann::json& cfg, std::string key, std::shared_ptr<T> const& v) {
      if (v) {
	cfg[key] = *v;
      }
    }
  }
}

#endif

