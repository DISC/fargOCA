// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_PLANETARY_SYSTEM_H
#define FARGOCA_PLANETARY_SYSTEM_H

#include <memory>
#include <vector>
#include <set>

#include <boost/property_tree/ptree.hpp>
#include <optional>
#include <boost/format.hpp>

#include <highfive/H5File.hpp>

#include "precision.hpp"
#include "tuple.hpp"
#include "diskPhysic.hpp"
#include "shared.hpp"
#include "momentum.hpp"
#include "timeStamped.hpp"

namespace fargOCA {
  class PlanetarySystemEngine;

  /// \brief Planet description.
  class Planet {
  public:
    Planet() = default;
    Planet(Planet const&) = default;
    constexpr Planet(real mass, Triplet position, Triplet velocity, 
                     real radius, real hillRadius )
      : myPosition(position), myVelocity(velocity), myMass(mass),
        myRadius(std::isnan(radius) ? real(-1) : radius ),
        myHillRadius(std::isnan(radius) ? real(-1) : hillRadius) {}
    
    constexpr Planet(real mass, Triplet position, Triplet velocity, 
                     std::optional<real> radius     = std::nullopt,
                     std::optional<real> hillRadius = std::nullopt)
      : Planet(mass, position, velocity, 
               radius.value_or(real(-1)),
               hillRadius.value_or(real(-1)))
    {}

    /// \brief Current mass.
    real           mass()        const { return myMass; } 
    void           setMass(real m) { myMass = m;}
    /// \brief Current position.
    KOKKOS_INLINE_FUNCTION
    Triplet const& position()    const { return myPosition; }
    void           setPosition(Triplet const& p) { myPosition = p; }
    /// \brief Current velocity.
    Triplet const& velocity()    const { return myVelocity; }
    void           setVelocity(Triplet const& v) { myVelocity = v; }

    std::optional<real> radius() const;
    void                setRadius(real r);

    /// \brief Hill radius specified by user if any
    std::optional<real> userHillRadius() const;
    void                setHillRadius(real r);
    
    Momentum       momentum() const { return Momentum(velocity(), mass()); }
    void           dump(std::ostream& out) const;

    void writeH5(HighFive::Group& grp) const;
    bool readH5(HighFive::Group const& grp);
    
    /// \brief Check if radius and Hill is handled by the planet data set.
    /// \param ds the planet dataset, the behavior is undefined if it'snot a planet data set.
    /// \return Wether the data set handle the planet radius and Hill radius
    static bool         hasRadiusAndHill(HighFive::DataSet const& ds);

    Planet abs() const;
    Planet adiff(Planet const& p) const;
    Planet rdiff(Planet const& p) const;

  private:
    Triplet  myPosition;
    Triplet  myVelocity;
    real     myMass;
    real     myRadius;
    real     myHillRadius;
  };
  std::ostream& operator<<(std::ostream& out, Planet const& p);
  /// \brief With absolute value members.
  inline Planet abs(Planet const& p) { return p.abs(); }
  /// \brief With absolute value diff members.
  inline Planet adiff(Planet const& p1, Planet const& p2) { return p1.adiff(p2); }
    /// \brief With relative value diff members.
  inline Planet rdiff(Planet const& p1, Planet const& p2) { return p1.rdiff(p2); }

  TimeStamped<Planet> adiff(TimeStamped<Planet> const& t1, TimeStamped<Planet> const& t2);
  TimeStamped<Planet> rdiff(TimeStamped<Planet> const& t1, TimeStamped<Planet> const& t2);

  /// \brief The eccentricity of the planet orbiting the central body
  real eccentricity(CodeUnits const& centralBody, Planet const& planet);
  /// \brief The keplerian frequency of the planet orbiting the central body
  real keplerianFrequency(CodeUnits const& centralBody, Planet const& planet);
  /// \brief The hill radius of the planet orbiting the central body
  real theoreticalHillRadius(CodeUnits const& units, Planet const& planet);
  /// \brief User specified Hill radius or, failing that, theoritical Hill radius.
  real HillRadius(CodeUnits const& units, Planet const& planet);

  class PlanetarySystem 
    : public std::enable_shared_from_this<PlanetarySystem> {
  public:

    /// \brief Build a new PlanetarySystem with initial planet state from physic.system.planets
    static shptr<PlanetarySystem> make(DiskPhysic const& cfg);
    /// \brief Build a new PlanetarySystem with provided planets.
    static shptr<PlanetarySystem> make(DiskPhysic const& cfg, std::map<std::string, Planet> const& planets);
    /// \brief Build a new PlanetarySystem with planets state read from state.    
    static shptr<PlanetarySystem> make(DiskPhysic const& cfg, HighFive::Group const& state);
    virtual ~PlanetarySystem();

    shptr<PlanetarySystem>       shared()       { return shared_from_this(); }
    shptr<PlanetarySystem const> shared() const { return shared_from_this(); }

    DiskPhysic const& physic() const { return *myPhysic; }

    /// \brief Rotate by angle '-angle' around z axis
    void rotate(real angle);
    void advance(real dt);
    void listPlanets(std::ostream& out = std::cout) const;
    void applyMassTaper(real physicalTime);

    std::map<std::string, Planet> const& planets() const { return myPlanets; }
    std::map<std::string, Planet>&       planets()       { return myPlanets; }

    bool          hasPlanet(std::string name) const;
    Planet const& planet(std::string name)    const;
    Planet&       planet(std::string name) { return const_cast<Planet&>(const_cast<PlanetarySystem const&>(*this).planet(name)); }
    real          eccentricity(Planet const& planet) const { return fargOCA::eccentricity(physic().units, planet); }
    real          keplerianFrequency(Planet const& planet) const { return fargOCA::keplerianFrequency(physic().units, planet); }
    real          theoreticalHillRadius(Planet const& planet) const { return fargOCA::theoreticalHillRadius(physic().units, planet); }
    /// \brief User specified Hill radius or, failing that, theoritical Hill radius.
    real          HillRadius(Planet const& planet) const { return fargOCA::HillRadius(physic().units, planet); }
    
    typedef boost::property_tree::ptree ptree;
  
    void looseOnAccretion(real d)       { myLostOnAccretion += d; }
    real lostOnAccretion()        const { return myLostOnAccretion; }
    
    /// \brief Planet position.
    /// \param guidingCenter if true... well...
    std::optional<Triplet> guidingPlanetPosition() const;
    
    /// \brief Object frequency provided to trajectory positions.
    static real frequency(Triplet prev, Triplet next, real dt);

    void dump(std::ostream& out) const;
    void writeH5(HighFive::Group& grp) const;
    
    typedef std::vector<TimeStamped<Planet>> PlanetLog;
    typedef std::map<std::string, PlanetLog> SystemLog;

    /// \brief Write log to dhf5 group.
    /// \param log the states to be logged
    static void writeH5Log(HighFive::Group root, std::string name, PlanetLog const& log);
    static void writeH5Logs(HighFive::Group root, SystemLog const& log);
    static PlanetLog readH5Log(HighFive::Group root, std::string name);
    static SystemLog readH5Logs(HighFive::Group root);
    
    static shptr<PlanetarySystem>       load(ptree const& cfg, DiskPhysic const& physic, std::ostream& log);
    void log(real time, SystemLog& log) const;

  private:
    PlanetarySystem(DiskPhysic const& cfg);
    PlanetarySystem(DiskPhysic const& cfg, std::map<std::string,Planet> const& planets);
    bool readH5(HighFive::Group const& grp);

    friend void swap(PlanetarySystem&,PlanetarySystem&);
  
  private:
    shptr<DiskPhysic const>                 myPhysic;
    real                                    myLostMass;
    real                                    myLostMassZ;
    real                                    myLostOnAccretion;
    Triplet                                 myPrimaryAcceleration;
    std::map<std::string, Planet>           myPlanets;
    std::unique_ptr<PlanetarySystemEngine>  myEngine;
  };

  void swap(PlanetarySystem&,PlanetarySystem&);
  
  // Iterative computation of eccentric anomaly.
  real eccAnomaly(real meanAnomaly, real eccentricity, int steps = 29);
  // Convert from orbital elements to cartesian position and velocity
  // \param nsteps the number of step used in the solver that compute the eccentric anomaly
  void orbital2Cartesian(OrbitalElements const& orbitalElements, 
                         real standardGravitationalParameter,
                         Triplet& position, Triplet& velocity,
                         int nsteps = 50);
}

template <> HighFive::DataType HighFive::create_datatype<fargOCA::Planet>();
template <> HighFive::DataType HighFive::create_datatype<fargOCA::TimeStamped<fargOCA::Planet>>();

#endif
