#!/usr/bin/env python3

import sys
import h5py as h5
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='''
Display the differences between a given field of two gas disk description files.
Example:
   {} ref/gas9.h5 out/gas9.h5 velocity/theta
'''.format(sys.argv[0]))
parser.add_argument('file', metavar='H5FILE', type=str, nargs=1,
                    help='HDF5 field file')
parser.add_argument('path', metavar='PATH', type=str, nargs=1,
                    help='Location of the field into the HDF5 file.')

args = parser.parse_args()

path = args.path[0]
file = h5.File(args.file[0], 'r')

field = np.array(file[path]['polar_grid'])
fmax = np.abs(field).max()

flat   = field.shape[1] == 1

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def plot(f, xymesh, scl_min, scl_max, pos, name):
    plt.subplot(2,2,pos)
    plt.title(name)
    x,y = xymesh
    plt.pcolormesh(y,x,f, cmap='RdBu', vmin=scl_min, vmax=scl_max)
    plt.axis([y.min(), y.max(), x.min(), x.max()])
    plt.colorbar()
    
xymesh = None
if flat:
    print("this is a flat grid, displaying as it is.")
    xcoords = range(0,field.shape[2])
    ycoords = range(0,field.shape[0])
    xymesh = np.meshgrid(ycoords, xcoords);
    field  = np.mean(field, 1)
    plot(field, xymesh, -fmax, fmax, 1, "{}".format(path))    
else:
    print("this is a fat grid, merging sectors before displaying.")
    (nr,ni,ns) = field.shape
    mfield  = np.mean(field, 2)
    plot(mfield, np.meshgrid(range(ni), range(nr)), -fmax, fmax, 1, "average")
    afield = np.abs(field)
    midx = np.unravel_index(np.argmax(afield), field.shape)
    print(str(midx))
    plot(field[midx[0],:,:], np.meshgrid(range(ns), range(ni)), -fmax, fmax, 2, "nr={}".format(midx[0]))
    plot(field[:,midx[1],:], np.meshgrid(range(ns), range(nr)), -fmax, fmax, 3, "ni={}".format(midx[1]))
    plot(field[:,:,midx[2]], np.meshgrid(range(ni), range(nr)), -fmax, fmax, 4, "ns={}".format(midx[2]))

plt.show()
