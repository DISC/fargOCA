// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGO_SIMULATION_TRACERS_H
#define FARGO_SIMULATION_TRACERS_H

#include <chrono>
#include "boost/format.hpp"

#include "simulationTrackingPlugin.hpp"
#include "planetarySystem.hpp"
#include "hillForce.hpp"

/// \file Specific tracer implementations

namespace fargOCA {
  class Simulation;
  /// \brief Will print planets orbital elements in texts files.
  /// This is a utility used in both command line tools and trackers.
  /// \param simulation the snapshop of the simulation
  /// \param fmt the format of the files name, %1% will be replaced by planet name
  void logOrbits(Simulation const& simulation, boost::format fmt, bool verbose);
}
  
#endif

