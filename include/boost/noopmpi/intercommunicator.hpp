// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_INTERCOMMUNICATOR_HPP
#define BOOST_NOOPMPI_INTERCOMMUNICATOR_HPP

#include <boost/noopmpi/communicator.hpp>
#include <boost/noopmpi/group.hpp>

namespace boost { namespace noopmpi {

class group;

/**
 * @brief Communication facilities among processes in different
 * groups.
 * 
 * There is only one group and it's a singleton.
 */
class BOOST_MPI_DECL intercommunicator : public communicator
{
public:
  /** @brief Cannot be actually called in a one process world. */
  template<typename... TS>
    intercommunicator(TS... unused) : communicator() { detail::no_return(); }
  
  /** @brief Cannot be called  */
  int local_size() const { return detail::no_return<int>(); }
  
  /** @brief Cannot be called  */
  noopmpi::group local_group() const  { return detail::no_return<noopmpi::group>(); }
  
  /** @brief Cannot be called  */
  int local_rank() const { return detail::no_return<int>(); }
  
  /** @brief Cannot be called  */
  int remote_size() const { return detail::no_return<int>(); }

  /** @brief Cannot be called  */
  noopmpi::group remote_group() const { return detail::no_return<noopmpi::group>(); }

  /** @brief Cannot be called  */
  communicator merge(bool /* high */) const { return detail::no_return<communicator>(); }
};

}}

#endif
