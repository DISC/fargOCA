// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <exception>
#include <numeric>

#include <sysexits.h>

#include <boost/format.hpp>

#include <highfive/H5File.hpp>
#include <Kokkos_MathematicalFunctions.hpp>
#include <nlohmann/json.hpp>

#include "mpioperations.hpp"

#include "disk.hpp"
#include "codeUnits.hpp"
#include "transportEngine.hpp"
#include "version.hpp"
#include "hillForce.hpp"
#include "stellarRadiationSolver.hpp"
#include "gridCellSizes.hpp"
#include "boundariesConditionsPlugin.hpp"
#include "diskProfiles.hpp"
#include "scalarFieldInterpolators.hpp"
#include "momentum.hpp"
#include "arraysUtils.hpp"
#include "cartesianGridCoords.hpp"
#include "viscosityFieldPlugin.hpp"
#include "optIO.hpp"
#include "loops.hpp"
#include "log.hpp"
#include "timeStamped.hpp"
#include "h5Labels.hpp"
#include "localized.hpp"
#include "configProperties.hpp"
#include "simulationConfig.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace h5l = h5::labels;

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;
  
#define LBD KOKKOS_LAMBDA

  using kk::sqrt;
  using kk::sin;
  using kk::cos;
  using kk::abs;
  using kk::fabs;
  using kk::fmax;
  using kk::fmin;
  using kk::pow;
  using kk::tanh;
  using kk::exp;
  
  Disk::~Disk() {}

  Disk::Disk(NoInit,
             shptr<PlanetarySystem>    sys,
             shptr<DiskPhysic const>   physic,
             shptr<GridDispatch const> grid)
    : mySystem(sys),
      myPhysic(physic),
      myDispatch(grid),
      myVelocity(*grid),
      myDensity("gasdens", *grid),
      mySoundSpeed("sound_speed", *grid),
      myPressure("pressure", *grid), 
      myViscosity("viscosity", *grid),
      myTemperature(std::nullopt),
      myDeadDensity(std::nullopt),
      myEnergy(std::nullopt),
      myRadiativeEnergy(std::nullopt),
      mySoundSpeedClean(false),
      myPressureClean(false),
      myViscosityClean(false),
      myStellarRadiationSolver(),
      myProfiles(),
      myTracers(),
      myFrameOmega(0),
      myPhysicalTime(0)
  {
    assert(!mySystem || mySystem->physic().shared() == myPhysic->shared());
    initGasMemory();
    if (debug::activatedTrace("compute_dt")) {
      declareUserField("dt_field");
    }
  }


  /// \brief The "init" constructor.
  /// Used when building from an init configuration file.
  /// This constructor can have same non trivial task to perform.
  Disk::Disk(shptr<PlanetarySystem>    sys,
             shptr<DiskPhysic const>   physic,
             shptr<GridDispatch const> dispatch)
    : Disk(NoInit(), sys, physic, dispatch) {
    switch (dispatch->grid().radialSpacing()) {
    case ARTH: initGasValue<ARTH>(); break;
    case LOGR: initGasValue<LOGR>(); break;
    }
    flashProfiles(); // based on non rotating frame
    adjustFrameVelocity(initialFrameVelocity());
  }
  
  shptr<Disk>
  Disk::make(DiskPhysic   const& physic,
             GridDispatch const& dispatch) {
    return shptr<Disk>(new Disk(PlanetarySystem::make(physic), physic.shared(), dispatch.shared()));
  }
  
  shptr<Disk>
  Disk::make(fmpi::communicator const& comm, HighFive::Group const& group) {
    using namespace h5l;
    CodeUnits units = (group.exist(CODE_UNITS)
		       ? CodeUnits(group.getGroup(CODE_UNITS))
		       : CodeUnits());
    shptr<DiskPhysic const>  physic = DiskPhysic::make(units, group.getGroup(PHYSIC));
    auto coords = Grid::make(physic->shape, group.getGroup(GRID));
    shptr<GridDispatch const> dispatch   = GridDispatch::make(comm, *coords);
    shptr<Disk> disk(new Disk(NoInit(), shptr<PlanetarySystem>(), physic, dispatch));
    disk->readH5(group);
    return disk;
  }

  // Factorization could be in order...
  void
  Disk::setGhosts() {
    std::vector<ScalarField*> fields;
    fields.reserve(5);
    fields.push_back(&myDensity);
    fields.push_back(&myVelocity.radial());
    fields.push_back(&myVelocity.theta());
    fields.push_back(&myVelocity.phi());
    if (myEnergy) {
      fields.push_back(&myEnergy.value());
      if (myRadiativeEnergy) {
        fields.push_back(&myRadiativeEnergy.value());
      }
    }

    for (ScalarField& f : myTracers) {
      fields.push_back(&f);
    }
    ScalarField::setGhosts(fields);
  }
  
  namespace {
    template<GridSpacing GS>
    void
    adjustThetaVelocity(Coords<GS> const& coords, real const domega, arr3d<real> vtheta) {
      auto radMed = coords.radiiMed();
      auto phiMed = coords.phiMed();
      kk::parallel_for("adjust_frame_vel", fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         vtheta(i,h,j) -= domega*radMed(i)*sin(phiMed(h));
                       });
    }
  }
  
  void
  Disk::adjustFrameVelocity(real const domega)  {
    auto const& coords = dispatch().grid();
    arr3d<real> vtheta = velocity().theta().data();
    switch (coords.radialSpacing()) {
    case ARTH: adjustThetaVelocity(coords.as<ARTH>(), domega, vtheta); break;
    case LOGR: adjustThetaVelocity(coords.as<LOGR>(), domega, vtheta); break;
    }
    myFrameOmega += domega;
  }
  
  namespace {
    bool abortOnFailedGasCheck = false;
  }

  void
  Disk::abortOnGasCheck(bool v) {
    if (!debug::enabled && v) {
      std::cerr  << "Warning: not in debug mode, Disk::abortOnGasCheck(true) is ignored.\n";
    }
    abortOnFailedGasCheck = v;
  }
  bool 
  Disk::abortOnGasCheck() { 
    return abortOnFailedGasCheck;
  }
  
  namespace {
    bool
    checkPositive(ScalarField const& field, bool abrt) {
      real fmin = field.min();
      if (fmin < 0) {
        std::cerr << "Problem with " << field.name() 
                  << " those minimal value is " << fmin << '\n';
        if (abrt) {
          std::abort();
        }
        return false;
      } else {
        return true;
      }
    }
                       
    bool
    checkPositive(std::optional<ScalarField> const& grid, bool abrt) {
      return !bool(grid) || checkPositive(*grid, abrt);
    }

    Triplet
    diskAcceleration(Disk const& disk, std::string planet)  {
      HillForce force(disk, planet);
      return (disk.physic().planetarySystem()->excludeHill
              ? force.inner.noHillSphere+force.outer.noHillSphere
              : force.inner.complete+force.outer.complete);
    }
  }

  void
  Disk::advanceSystemFromDisk(real dt, Triplet gasAcceleration) {
    if (system()) {
      for (auto& [name, planet] : system()->planets()) {
        if (physic().planetarySystem()->planet(name).feelDisk) {
          // If planets feel disk  they feel indirect term from disk too
          Triplet acceleration = diskAcceleration(*this, name);
          acceleration -= gasAcceleration;
          Triplet vel = planet.velocity() + dt * acceleration;
          planet.setVelocity(vel);
        }
      }
    }
  }

  [[maybe_unused]]
  bool 
  Disk::checkPhysicalQuantities() const {
    bool abrt = abortOnGasCheck();
    bool status = (checkPositive(energy(), abrt)
                   && checkPositive(density(), abrt)
                   && checkPositive(radiativeEnergy(), abrt));
    return abrt ? status : true;
  }
    
  template<GridSpacing GS>
  void
  Disk::computeSoundSpeed() {
    assert(checkPhysicalQuantities());
    // We compute the sound speed, for the isothermal case remember that 
    // the disk.physic().flaringIndex is included in the aspectratio vector
    auto const& coords = dispatch().grid().as<GS>();
    if (physic().adiabatic) {
      real const adiabaticIndex = physic().adiabatic->index;
      real x = adiabaticIndex*(adiabaticIndex-1); // \todo if you know what x stand for, fell free to rename
      assert(x>=0);
      arr3d<real>       ss = mySoundSpeed.data();
      arr3d<real const> e  = energy()->data();
      arr3d<real const> d  = density().data();
      kk::parallel_for("sound_speed_adiabatic", fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) { ss(i,h,j) = sqrt(x*e(i,h,j)/d(i,h,j));});
    } else {
      auto         radMed  = coords.radiiMed();
      arr3d<real>  ss = mySoundSpeed.data();
      auto const aspectRatio = physic().aspectRatio;
      size_t const nr  = coords.sizes().nr;
      arr1d<real>       ssi("sound_speed_values", nr);
      kk::parallel_for("ss_values", kk::RangePolicy<>(0, nr),
                       LBD(size_t const i) { ssi(i) = aspectRatio(radMed(i))*sqrt(G/(ipow<3>(radMed(i))))*radMed(i);});
      kk::parallel_for("ass_ss_values", fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) { ss(i,h,j) = ssi(i); });
    }
    mySoundSpeedClean = true;
  }
  
  ScalarField const&
  Disk::soundSpeed() const {
    if (!mySoundSpeedClean) {
      switch (dispatch().grid().radialSpacing()) {
      case ARTH: const_cast<Disk&>(*this).computeSoundSpeed<ARTH>(); break;
      case LOGR: const_cast<Disk&>(*this).computeSoundSpeed<LOGR>(); break;
      }
    }
    return mySoundSpeed;
  }
  
  void
  Disk::computeViscosity() {
    auto const& evaluator = ViscosityFieldPlugin::get(physic().viscosity.type).evaluator();
    evaluator(*this, myViscosity.data());
    myViscosityClean = true;
  }
    
  ScalarField const&
  Disk::viscosity()  const {
    if (!myViscosityClean) {
      const_cast<Disk&>(*this).computeViscosity();
    }
    return myViscosity;
  }
  
  real
  Disk::frameVelocity() const {
    if (physic().referential.omega) {
      real const diff = std::abs(*physic().referential.omega - myFrameOmega);
      if (diff > 0.001) {
        dispatch().log() << "##Omega mismatch: " << physic().referential.omega
                         << " VS " << myFrameOmega << '\n';
        std::abort();
      }
    }
    return myFrameOmega;
  }

  namespace {
    real
    flatSmoothing(DiskPhysic const& physic, Planet const& planet, real physicalTime) {
      real distance = planet.position().norm();
      // When smoothing type is Flat we  consider that the smoothing lenght in units of disk scale height, default  unit  is the Hill radius
      real const aspectRatioPl = physic.aspectRatio(distance); 
      real const scaleHeightPl = aspectRatioPl*distance;    // Flaring index is taken into account already in physic().aspectRatio
      return physic.smoothing.smooth(scaleHeightPl, physicalTime);
    }
    
    real
    fatSmoothing(DiskPhysic const& physic, Planet const& planet, real physicalTime) {
      return physic.smoothing.smooth(theoreticalHillRadius(physic.units, planet),
				     physicalTime);
    }
  }
  
  template<GridSpacing GS>
  void
  Disk::computeSystemPotential(std::optional<Triplet> gasAcceleration, arr3d<real> potential) const
  {
    auto const& coords = dispatch().grid().as<GS>();
    auto radMed = coords.radiiMed();
    CartesianGridCoords const cartesian = dispatch().cartesian();
    kk::deep_copy(potential, real(0));
    if (system()) {
      for (auto const& np : system()->planets()) {
        Planet const& planet          = np.second;
        real const planetDistance     = planet.position().norm();
        real const invPlanetDistance3 =  1/pow(planetDistance, 3);
        real const planetMass         = planet.mass();
        Triplet const planetPosition  = planet.position();
        if (physic().smoothing.flat) {
          real const smoothing = flatSmoothing(physic(), planet, physicalTime());
          kk::parallel_for("system_pot_flat_smooth", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             Triplet const cellPosition       = cartesian.coords(i,h,j);
                             real    const distance2          = (planetPosition-cellPosition).norm2();
                             real    const indirectPlanetTerm = (planetPosition*cellPosition).sum()*invPlanetDistance3;
                             potential(i,h,j) += G*planetMass*(indirectPlanetTerm - 1/sqrt(distance2+pow(smoothing,2)));
                           });
        } else {
          real const smoothing = fatSmoothing(physic(), planet, physicalTime());
          kk::parallel_for("system_pot_fat_smooth", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             Triplet const cellPosition       = cartesian.coords(i,h,j);
                             real    const distance           = (planetPosition-cellPosition).norm();
                             real    const indirectPlanetTerm = (planetPosition*cellPosition).sum()*invPlanetDistance3;
                             real contribFactor = indirectPlanetTerm;
                             if (distance <= smoothing) {  /* 3D potential from Kley and Bitsch 2009 */ 
                               real const smooth = distance/smoothing;
                               contribFactor -= (pow(smooth,4)-2*pow(smooth,3)+2*smooth)/distance;
                             } else {
                               contribFactor -= 1/distance;
                             }
                             potential(i,h,j) += G*planetMass*contribFactor;
                           });
        }
      }
    }
    {
      real const NaN = std::numeric_limits<real>::quiet_NaN();
      bool   const  indirectForces = physic().referential.indirectForces;
      Triplet const acc = indirectForces ? *gasAcceleration : Triplet(NaN);
      real const centralBodyMass = physic().units.centralBodyMass();
      kk::parallel_for("apply_star_acceleration", fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) { 
                         potential(i,h,j) -=  G*centralBodyMass/radMed(i);  
                         if (indirectForces) {
                           potential(i,h,j) +=  (cartesian.coords(i,h,j)*acc).sum();
                         }
                       });
    }
  }

  void
  Disk::computeSystemPotential(std::optional<Triplet> gasAcceleration, arr3d<real> potential) const {
    switch (dispatch().grid().radialSpacing()) {
    case ARTH: Disk::computeSystemPotential<ARTH>(gasAcceleration, potential); break;
    case LOGR: Disk::computeSystemPotential<LOGR>(gasAcceleration, potential); break;
    }
  }
  
  void
  Disk::computeTemperature() {
    assert(!myTemperature);
    if (physic().adiabatic) {
      
      myTemperature = ScalarField("temperature", dispatch());
      real const specificHeat = physic().adiabatic->specificHeat();
      arr3d<real>       temperature = myTemperature->data();
      arr3d<real const> density     = this->density().data();
      arr3d<real const> energy      = this->energy()->data();
      kk::parallel_for("compute_temperature", fullRange(dispatch().grid()),
                       LBD(size_t const i, size_t const h, size_t const j) { 
                         temperature(i,h,j) = energy(i,h,j)/(specificHeat*density(i,h,j));
                       });
    }
  }

  std::optional<ScalarField> const&
  Disk::temperature() const {
    if (!myTemperature) {
      const_cast<Disk&>(*this).computeTemperature();
    }
    return myTemperature;
  }
  
  void
  Disk::trashTemperature() {
    myTemperature = std::nullopt;
  }
  
  void
  Disk::computePressure() {
    kk::Profiling::pushRegion("computePressure");
    arr3d<real> pressure = myPressure.data();
    auto const& coords = dispatch().grid();
    if (physic().adiabatic) {
      real indexm = physic().adiabatic->index-1;
      arr3d<real const> energy= this->energy()->data();
      kk::parallel_for("adiabatic_pressure", fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) {  pressure(i,h,j) = indexm*energy(i,h,j);});
    } else {
      arr3d<real const> density    = this->density().data();
      arr3d<real const> soundSpeed = this->soundSpeed().data();
      kk::parallel_for("pressure", fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) {  pressure(i,h,j) = density(i,h,j)*pow(soundSpeed(i,h,j), 2);});
    }
    myPressureClean = true;
    kk::Profiling::popRegion();
  }
  
  ScalarField const&
  Disk::pressure() const {
    if (!myPressureClean) {
      const_cast<Disk&>(*this).computePressure();
    }
    return myPressure;
  }

  template<GridSpacing GS>
  real
  Disk::momentum() const {
    auto const& coords = dispatch().grid().as<GS>();
    GridSizes const ls = coords.sizes();
    
    arr3d<real const> density   = myDensity.data();
    arr3d<real const> vtheta    = myVelocity.theta().data();
    arr3d<real const> cellSizes = dispatch().cellSizes().volume();
    
    auto radMed = coords.radiiMed();
    auto phiMed = coords.phiMed();
    
    real total = 0;
    range_t const managedRadii  = dispatch().managedRadii(1);
    bool const flat  = coords.dim() < 3;
    size_t const niMin  = flat ? 0 : 1;
    size_t const niMax  = flat ? 1 : ls.ni-1;
    real const omega = frameVelocity();
    
    kk::parallel_reduce("momentum", range3D({begin(managedRadii), niMin, 1}, {end(managedRadii), niMax, ls.ns}),
                        LBD(size_t const i, size_t const h, size_t const j, real& tmp) {
                          real const sinPhi =  flat ? real(1) : std::sin(phiMed(h));
                          tmp += cellSizes(i,h,j)*(density(i,h,j)+density(i,h,j-1))*radMed(i)*sinPhi*(vtheta(i,h,j)+omega*radMed(i)*sinPhi);
                        }, total);
    return fmpi::all_reduce(dispatch().comm(), total, std::plus<real>())/2;
  }
  
  real
  Disk::momentum() const {
    switch (dispatch().grid().radialSpacing()) {
    case ARTH: return Disk::momentum<ARTH>(); break;
    case LOGR: return Disk::momentum<LOGR>(); break;
    }
    std::abort();
  }
  
  real
  Disk::totalMass() const {
    auto const& coords = dispatch().grid();
    GridSizes const ls = coords.sizes();
    
    arr3d<real const> density  = myDensity.data();
    arr3d<real const> cellSizes = dispatch().cellSizes().volume();

    range_t const managedRadii = dispatch().managedRadii(1);
    size_t const nrMin = begin(managedRadii);
    size_t const nrMax = end(managedRadii);
    size_t const niMin = coords.dim() == 3 ? 1       : 0;
    size_t const niMax = coords.dim() == 3 ? ls.ni-1 : 1;
    
    real total = 0;
    kk::parallel_reduce("total_mass", range3D({nrMin, niMin, 0}, {nrMax, niMax, ls.ns}),
                        LBD(size_t const i, size_t const h, size_t const j, real& tmp) { tmp += cellSizes(i,h,j)*density(i,h,j); },
                        total);
    return fmpi::all_reduce(dispatch().comm(), total, std::plus<real>());
  }

  real
  Disk::totalEnergy() const {
    if (physic().adiabatic) {
      auto const& coords = dispatch().grid();
      GridSizes const ls = coords.sizes();
      arr3d<real const> gasenergy = myEnergy->data();
      arr3d<real const> cellSizes = dispatch().cellSizes().volume();

      range_t const managedRadii = dispatch().managedRadii(1);
      size_t const nrMin = begin(managedRadii);
      size_t const nrMax = end(managedRadii);
      size_t const niMin = coords.dim() == 3 ? 1    : 0;
      size_t const niMax = coords.dim() == 3 ? ls.ni-1 : 1;
      real total = 0;
      kk::parallel_reduce("total_energ", range3D({nrMin, niMin, 0}, {nrMax, niMax, ls.ns}),
                          LBD(size_t const i, size_t const h, size_t const j, real& tmp) { tmp += cellSizes(i,h,j)*gasenergy(i,h,j); },
                          total);
      return fmpi::all_reduce(dispatch().comm(), total, std::plus<real>());
    } else {
      return 0;
    }
  }

  real
  Disk::kineticRadial() const {
    auto const& coords = dispatch().grid();
    GridSizes const ls = coords.sizes();
    
    arr3d<real const> density   = myDensity.data();
    arr3d<real const> vrad      = myVelocity.radial().data();
    arr3d<real const> cellSizes = dispatch().cellSizes().volume();
    
    range_t const managedRadii = dispatch().managedRadii(1);
    size_t const nrMin = begin(managedRadii);
    size_t const nrMax = end(managedRadii);
    size_t const niMin = coords.dim() == 3 ? 1    : 0;
    size_t const niMax = coords.dim() == 3 ? ls.ni-1 : 1;
    real total = 0;

    kk::parallel_reduce("kinetic_dadial", range3D({nrMin, niMin, 1}, {nrMax, niMax, ls.ns}),
                        LBD(size_t const i, size_t const h, size_t const j, real& tmp) { 
                          tmp += cellSizes(i,h,j)*(density(i,h,j)+density(i-1,h,j))*pow(vrad(i,h,j), 2);
                        }, total);
    return fmpi::all_reduce(dispatch().comm(), total, std::plus<real>())/4;
  }

  real
  Disk::kineticPolar() const {
    auto const& coords = dispatch().grid();
    GridSizes const ls = coords.sizes();
    
    arr3d<real const> density   = myDensity.data();
    arr3d<real const> vphi      = myVelocity.phi().data();
    arr3d<real const> cellSizes = dispatch().cellSizes().volume();
    range_t const managedRadii = dispatch().managedRadii(1);
    size_t const nrMin = begin(managedRadii);
    size_t const nrMax = end(managedRadii);
    if (coords.dim() == 3) {
      real total = 0;
      kk::parallel_reduce("kinetic_polar", range3D({nrMin, 1, 1}, {nrMax, ls.ni-1, ls.ns}),
                          LBD(size_t const i, size_t const h, size_t const j, real& tmp) { 
                            tmp += cellSizes(i,h,j)*(density(i,h,j)+density(i,h-1,j))*pow(vphi(i,h,j), 2);
                          }, total);
      return fmpi::all_reduce(dispatch().comm(), total, std::plus<real>()) / 4;
    } else {
      return 0;
    }
  }

  real
  Disk::kineticKeplerian() const {
    auto const& coords = dispatch().grid();
    GridSizes const ls = coords.sizes();
    
    arr3d<real const> density   = myDensity.data();
    arr3d<real const> vtheta    = myVelocity.theta().data();
    arr3d<real const> cellSizes = dispatch().cellSizes().volume();

    range_t const managedRadii = dispatch().managedRadii(1);
    size_t const nrMin = begin(managedRadii);
    size_t const nrMax = end(managedRadii);
    size_t const niMin = coords.dim() == 3 ? 1    : 0;
    size_t const niMax = coords.dim() == 3 ? ls.ni-1 : 1;
    real total = 0;
    kk::parallel_reduce("kinetic_polar", range3D({nrMin, niMin, 0}, {nrMax, niMax, ls.ns}),
                        LBD(size_t const i, size_t const h, size_t const j, real& tmp) {
                          size_t const jm = j == 0 ? ls.ns-1 : j-1;
                          tmp += cellSizes(i,h,j)*(density(i,h,j)+density(i,h,jm))*pow(vtheta(i,h,j), 2);
                        }, total);
    return fmpi::all_reduce(dispatch().comm(), total, std::plus<real>())/4;
  }

  void
  Disk::mapStateFields(std::function<void (ScalarField&)> f) {
    f(myDensity);
    f(myVelocity.radial());
    f(myVelocity.theta());
    f(myVelocity.phi());
    if (myEnergy)          { f(*myEnergy); }
    if (myRadiativeEnergy) { 
      f(*myRadiativeEnergy);
    }    
  }

  void
  Disk::mapStateFields(std::function<void (ScalarField const&)> f) const {
    const_cast<Disk&>(*this).mapStateFields([&f](ScalarField& g) { f(g); });
  }

  bool
  Disk::readH5(std::string fname) {
    HighFive::File file(fname, HighFive::File::ReadOnly);
    bool status = readH5(file.getGroup("/"));
    return status;
  }

  namespace {
    std::vector<std::string>
    extractTracerNames(HighFive::Group const& root) {
      using h5l::TRACERS;
      return root.exist(TRACERS) ? root.getGroup(TRACERS).listObjectNames() : std::vector<std::string>();
    }

    std::vector<std::string>
    extractUserFieldsNames(HighFive::Group const& root) {
      using h5l::USER;
      return root.exist(USER) ? root.getGroup(USER).listObjectNames() : std::vector<std::string>();
    }
  }
  
  bool
  Disk::readH5(HighFive::Group const& root) {
    using namespace HighFive;
    using namespace h5l;

    myFrameOmega   = root.getAttribute(FRAME_OMEGA).read<real>();
    if (root.hasAttribute(PHYSICAL_TIME)) {
      myPhysicalTime = root.getAttribute(PHYSICAL_TIME).read<real>();
    }
    Group gridGroup = root.getGroup(GRID);
    
    if (root.exist(PLANETARY_SYSTEM)) {
      mySystem = PlanetarySystem::make(physic(), root.getGroup(PLANETARY_SYSTEM));
    }
    importGas(root);
    if (physic().radiative()) {
      myStellarRadiationSolver = StellarRadiationSolver::restart(*this, root);
      if (!myStellarRadiationSolver) {
        dispatch().log() << "Could not reload stellar radiation solver.\n";
      }
    }
    myTracers.clear();
    for (std::string marker : extractTracerNames(root)) {
      using namespace boost;
      ScalarField tracer(marker, dispatch());
      h5::loadDispatched(tracer, root, str(format("tracers/%1%")%marker));
      addTracer(std::move(tracer));
    }
    myUserFields.clear();
    for (std::string fname : extractUserFieldsNames(root)) {
      using namespace boost;
      declareUserField(fname);
      h5::loadDispatched(getUserField(fname), root, str(format("user/%1%")%fname));
    }
    trashDerivedFields();

    if (root.exist("profiles")) {
      myProfiles.reset(new DiskProfiles(dispatch(), root.getGroup("profiles")));
    } else {
      auto initial =  Disk::make(physic(), dispatch());
      myProfiles.reset(new DiskProfiles(initial->profiles()));
      dispatch().log() << "No profiles found in HDF5 file.\n"
                       << "Consider using 'disk_profiles_update'.\n";
    }

    return true;
  }

  void
  Disk::cleanGasMinimums() {
    // rebind can introduce weird values through interpolation.
    // check some basic properties:

    real const densityMin = physic().density.minimum/physic().units.density();;
    arr3d<real> density = mdensity().data();
    auto const& coords = dispatch().grid();
      
    kk::parallel_for("density_min", fullRange(coords), LBD(size_t const i, size_t const h, size_t const j) {density(i,h,j) = fmax(density(i,h,j), densityMin);} );
    if (physic().adiabatic) {
      arr3d<real> energy = menergy()->data();
      kk::parallel_for("energy_min", fullRange(coords), LBD(size_t const i, size_t const h, size_t const j) {energy(i,h,j) = fmax(energy(i,h,j), real(0)); });
      if (physic().adiabatic->radiative) {
        arr3d<real> radiative = radiativeEnergy()->data();
        kk::parallel_for("radiative_min", fullRange(coords), LBD(size_t const i, size_t const h, size_t const j) {radiative(i,h,j) = fmax(radiative(i,h,j), real(0)); });
      }
    }
  }

  bool
  Disk::importGas(HighFive::Group const& root) {
    using namespace h5l;
    h5::loadDispatched(velocity(), root, VELOCITY);
    h5::loadDispatched(mdensity(), root, DENSITY);
    if (bool(physic().adiabatic)) {
      h5::loadDispatched(*menergy(), root, ENERGY);
      if (bool(physic().adiabatic->radiative)) {
        h5::loadDispatched(*radiativeEnergy(), root, RADIATIVE_ENERGY);
      }
    }
    cleanGasMinimums();
    return true;
  }

  bool
  Disk::writeH5(std::optional<HighFive::Group> group, int writer, bool indirectFields) const {
    using namespace h5l;

    assert(bool(group) == (writer == dispatch().comm().rank()));  // group only exist on writer

    if (group) {
      physic().units.writeH5(h5::createSubGroup(*group, CODE_UNITS));
      h5::writeAttribute(*group, SOURCE_CODE, source_version());
      auto gridGroup = h5::createSubGroup(*group, GRID);
      dispatch().grid().global().writeH5(gridGroup); 
      if (!myUserFields.empty()) {
        h5::createSubGroup(*group, USER);
      }
      h5::createSubGroup(*group, TRACERS);
      h5::writeAttribute(*group, FRAME_OMEGA, myFrameOmega);
      h5::writeAttribute(*group, PHYSICAL_TIME, myPhysicalTime);
      
      if (system()) {
        auto grp = h5::createSubGroup(*group, PLANETARY_SYSTEM);
        system()->writeH5(grp);
      }
    }
    myVelocity.writeH5(h5::createSubGroup(group, VELOCITY), writer);
    profiles().writeH5(h5::createSubGroup(group, PROFILES), writer);
    // fields
    typedef std::map<std::string, ScalarField const&> field_map;
    field_map fields;
    typedef field_map::value_type pair;
    fields.insert(pair("density", density()));
    if (energy()) {
      fields.insert(pair("energy", *energy()));
    }
    if (radiativeEnergy()) {
      fields.insert(pair(RADIATIVE_ENERGY, *radiativeEnergy()));
      stellarRadiationSolver().checkpoint(group, writer);
    }
    for (ScalarField const& f : myTracers) {
      using namespace boost;
      fields.insert(pair(str(format("tracers/%1%")%f.name()), f));
    }
    for (auto& [name,field] : myUserFields) {
      std::ostringstream fmt;
      fmt << "/user/" << name;
      fields.insert(pair(fmt.str(), field));
    }
    if (indirectFields) {
      for (auto& [name,field] : namedFields()) {
        fields.insert(pair(name, field));
      }
    }
    for(auto const&[path, field] : fields ) {
      std::optional<arr3d<real const>> data  = field.merged(writer);
      if (group) {
        auto subGroup = h5::createSubGroup(*group, path);
        field.writeField(subGroup, *data);
      }
    }
    if (group) {
      auto physicGrp = h5::createSubGroup(*group, PHYSIC);
      physic().writeH5(physicGrp);
    }
    return true;    
  }

  void
  Disk::writeH5(std::string fname) const {
    h5::createH5(*this, dispatch().comm(), fname);
  }
  
  bool
  Disk::declareUserField(std::string name) {
    return myUserFields.insert(std::make_pair(name, ScalarField(name, dispatch()))).second;
  }

  ScalarField&
  Disk::getUserField(std::string name) {
    //assert(hasUserField(name));
    return myUserFields.find(name)->second;
  }
   
  ScalarField const&
  Disk::getUserField(std::string name) const {
    //assert(hasUserField(name));
    return myUserFields.find(name)->second;
  }
   
  void
  Disk::applyBoundaryConditions(real dt) { 
    debug::ScopedDump dump(*this, debug::BOUNDARIES_CONDITIONS);
    for (auto name  : physic().boundaries.enabled()) {
      auto& handler = BoundariesConditionsPlugin::get(name);
      auto fct      = handler.evaluator();
      fct(name, *this, dt);
    }
    assert(checkPhysicalQuantities());
  }
   
  bool
  Disk::hasTracer(std::string marker) const {
    return (std::find_if(myTracers.begin(), myTracers.end(), 
                         [=](ScalarField const& f) { return f.name() == marker; })
            != myTracers.end());
  }
  
  void
  Disk::addTracer(ScalarField&& tracer) {
    if (hasTracer(tracer.name())) {
      dispatch().log() << "A tracer named '" << tracer.name() << "' is already registered.\n";
      std::exit(EX_DATAERR);
    }
    myTracers.emplace_front(std::move(tracer));
  }

  void
  Disk::addTracer(ScalarField const& tracer) {
    addTracer(ScalarField(tracer));
  }

  
  void
  Disk::importGas(Disk const& gas, real tol) {
    std::optional<GasShape> compatibility = compatible(dispatch().grid().global(), gas.dispatch().grid().global());
    if (!compatibility || max(*compatibility) > tol) {
      std::cerr << "Importing gas from an incompatible grid.\n";
      std::abort();
    }

    kk::deep_copy(myVelocity.radial().data(), gas.myVelocity.radial().data());
    kk::deep_copy(myVelocity.phi().data(), gas.myVelocity.phi().data());
    kk::deep_copy(myVelocity.theta().data(), gas.myVelocity.theta().data());
    
    kk::deep_copy(myDensity.data(), gas.myDensity.data());
    kk::deep_copy(mySoundSpeed.data(), gas.mySoundSpeed.data());
    kk::deep_copy(myPressure.data(), gas.myPressure.data());
    kk::deep_copy(myViscosity.data(), gas.myViscosity.data());
    
    assert(bool(myEnergy) == bool(gas.myEnergy));
    if (myEnergy) {
      kk::deep_copy(myEnergy->data(), gas.myEnergy->data());
    }
    assert(bool(myRadiativeEnergy) == bool(gas.myRadiativeEnergy));
    if (myRadiativeEnergy) {
      kk::deep_copy(myRadiativeEnergy->data(), gas.myRadiativeEnergy->data());
      stellarRadiationSolver().importGas(gas.stellarRadiationSolver());
    }
    myTracers = gas.myTracers;
    myUserFields.clear();
    for(auto const& kv : gas.myUserFields) {
      myUserFields.emplace(kv.first, kv.second);
    }
  }

  void
  Disk::rebind(GridDispatch const& nextDispatch, ScalarFieldInterpolators const& interpolators) {
    myDispatch = nextDispatch.shared();
    myVelocity.rebind(dispatch(), interpolators);
    myDensity.rebind(dispatch(), interpolators);
    if (myEnergy) {
      myEnergy->rebind(dispatch(), interpolators);
    }
    if (myRadiativeEnergy) {
      myRadiativeEnergy->rebind(dispatch(), interpolators);
      stellarRadiationSolver().rebind(dispatch(), interpolators);
    }
    for (ScalarField& field : myTracers) {
      field.rebind(dispatch(), interpolators);
    }
    for (auto&[key,field] : myUserFields) {
      field.rebind(dispatch(), interpolators);
    }
    cleanGasMinimums();

    trashDerivedFields();
    mySoundSpeed = ScalarField(mySoundSpeed.name(), dispatch());
    myPressure   = ScalarField(myPressure.name(), dispatch());
    myViscosity  = ScalarField(myViscosity.name(), dispatch());
    if (myProfiles) {
      myProfiles.reset(new DiskProfiles(dispatch(), profiles()));
    }
  }

  void 
  Disk::rebind(GridDispatch const& newDispatch) {
    rebind(newDispatch, ScalarFieldInterpolators::get()); 
  }

  Triplet
  Disk::computeGasAcceleration() const {
    auto const& coords = dispatch().grid();
    GridSizes const ls = coords.sizes();

    range_t const managedRadii = dispatch().managedRadii();
    size_t const nrMin = begin(managedRadii);
    size_t const nrMax = end(managedRadii);  
    size_t const niMax = physic().shape.half ? ls.ni - 1 : ls.ni;
    
    arr3d<real const> dens   = density().data();
    arr3d<real const> volume = dispatch().cellSizes().volume();
    CartesianGridCoords cartesian = dispatch().cartesian();
    
    Triplet contribution(0,0,0);
    kk::parallel_reduce("compute_acceleration", range3D({nrMin, 0, 0}, {nrMax, niMax, ls.ns}),
                        LBD(size_t const i, size_t const h, size_t const j, Triplet& tmp) {
                          Triplet cellPos  = cartesian.coords(i,h,j);
                          real    invDist3 = pow(cellPos.norm2(),-real(3)/2);
                          real    cellMass = volume(i,h,j)*dens(i,h,j);
                          Triplet contribution = G*invDist3*cellMass*cellPos;
                          tmp += contribution;
                        }, kk::Sum<Triplet>(contribution));
    Triplet acceleration = fmpi::all_reduce(dispatch().comm(), contribution, std::plus<Triplet>());
    if (physic().shape.half) {
      // We set to zero the vertical component of the non inertial force due to the 
      // star acceleration in case of HalfDisk to be used only for symetric disks
      acceleration.z = 0;
      acceleration  *= 2;
    }
    return acceleration;
  }
  
  template<GridSpacing GS>
  real
  Disk::localConvergingTimeStep() const {
    // Courant–Friedrichs–Lewy condition
    debug::ScopedDump dump(*this, debug::COMPUTE_DT);
    
    auto const& coords = dispatch().grid().as<GS>();
    GridSizes const ls = coords.sizes();
    auto vel = velocity().data();
    arr3d<real const> vt         = vel.theta;
    arr3d<real const> vr         = vel.radial;
    arr3d<real const> vp         = vel.phi;
    arr3d<real const> visc       = viscosity().data();
    arr3d<real const> soundspeed = soundSpeed().data();
    arr3d<real const> dtheta     = dispatch().cellSizes().dTheta();
    
    auto radii      = coords.radii();
    auto dRadii     = coords.dRadii();
    auto radMed     = coords.radiiMed();
    auto phiMed     = coords.phiMed();
    auto dPhiMed    = coords.dPhiMed();
    auto thetaMed   = coords.thetaMed();
      
    arr1d<real> avgVt = averageOnThetaPhi(velocity().theta().data());
    bool const tracedDtField = debug::activatedTrace("compute_dt");
    arr3d<real> dtField;
    if (tracedDtField) {
      dtField  = const_cast<Disk*>(this)->getUserField("dt_field").data();
    }
    real const CFLSecurity         = physic().CFLSecurity;
    bool const fastTransport       = physic().transport == Transport::FAST;
    bool const artificialViscosity = physic().viscosity.artificial;
    bool const flat = coords.dim() < 3;
    Localized<real> fieldDt;
    {
      range_t const managedRadii = dispatch().managedRadii(1);
      size_t const nrMin = begin(managedRadii);
      size_t const nrMax = end(managedRadii);  
      kk::parallel_reduce("CFL::field", range3D<size_t>({nrMin, flat ? 0u : 1u, 0u}, {nrMax, ls.ni, ls.ns}),
                          LBD(size_t const i, size_t const h, size_t const j, Localized<real>& ldt) {
                            real const dxrad = dRadii(i);
                            real const dxphi = radMed(i)*dPhiMed(h);
                            real const accountedVt = fastTransport ? (vt(i,h,j) - avgVt(i)) : vt(i,h,j);
                            Triplet const dt1(soundspeed(i,h,j) / fmin(dxphi, fmin(dxrad, dtheta(i,h,j))),
                                              fabs(vr(i,h,j))/dxrad,
                                              fabs(accountedVt)/dtheta(i,h,j));
                            Triplet dt2(0,
                                        visc(i,h,j)*4/pow(fmin(fmin(dxrad,dtheta(i,h,j)), dxphi),2),
                                        fabs(vp(i,h,j))/dxphi);
                            if (artificialViscosity) {
                              size_t const jp = nsnext(ls.ns, j);
                              real dvr = vr(i+1,h,j)-vr(i,h,j);
                              real dvt = vt(i,h,jp)-vt(i,h,j);
                              real dvp = h == ls.ni-1 ? 0 : vp(i,h+1,j)-vp(i,h,j);
                                
                              dvr = dvr >= 0 ? 0 : -dvr;
                              dvt = dvt >= 0 ? 0 : -dvt;   
                              dvp = dvp >= 0 ? 0 : -dvp;
                                
                              dt2.x = 4*kk::max({dvr/dxrad, dvt/dtheta(i,h,j), dvp/dxphi})*pow(CVNR,2);
                            }
                            real const cellDt = CFLSecurity/sqrt(dt1.norm2()+dt2.norm2());
			    if (tracedDtField) {
			      dtField(i,h,j) = cellDt;
			    }
                            if (cellDt < ldt.value) {
                              ldt = {i,h,j, cellDt};
                            }
                          }, kk::Min<Localized<real>>(fieldDt));
      if (debug::activatedTrace(debug::DT_FIELD)) {
	std::ofstream log(debug::traceLog(debug::DT_FIELD));
	log << "Field dt on proc " << dispatch().comm().rank() << ' ' << fieldDt << std::endl;
      }
    }
    {
      real vtDt;
      range_t const managedRadii = dispatch().managedRadii(0,1);
      kk::parallel_reduce("CFL::theta_velocity", range2D({managedRadii.first, 0}, {managedRadii.second, ls.ns}),
                          LBD(size_t const i, size_t const j, real& ldt) {
                            real const cellDt = (thetaMed(j+1)-thetaMed(j))*CFLSecurity/fabs(avgVt(i)/radMed(i)-avgVt(i+1)/radMed(i+1));
                            if (cellDt < ldt) {
                              ldt = cellDt;
                            }
                          }, kk::Min<real>(vtDt));
      if (debug::activatedTrace(debug::DT_VT)) {
	std::ofstream log(debug::traceLog(debug::DT_VT));
	log << "Field dt on proc " << dispatch().comm().rank() << ' ' << vtDt << '\n';
      }
      return std::min({fieldDt.value, vtDt});
    }
  }

  real
  Disk::localConvergingTimeStep() const {
    switch (dispatch().grid().radialSpacing()) {
    case ARTH: return Disk::localConvergingTimeStep<ARTH>(); break;
    case LOGR: return Disk::localConvergingTimeStep<LOGR>(); break;
    }
    std::abort();
  }
  
  real
  Disk::convergingTimeStep() const {
    real const localGasDt  = localConvergingTimeStep();
    return fmpi::all_reduce(dispatch().comm(), localGasDt, fmpi::minimum<real>());
  }
  
  real
  Disk::convergingTimeStep(real maxdt) const {
    real const computed = convergingTimeStep();
    real const nbsteps  = std::ceil(maxdt/computed);
    real const rounded  = maxdt/nbsteps;
    return rounded;
  }

  template<GridSpacing GS>
  void
  Disk::computeDeadDensity() {
    ScalarField dead("dead_density", dispatch());
      
    auto const& coords = dispatch().grid().as<GS>();
    GridSizes const ls = coords.sizes();

    arr3d<real const> density = this->density().data();
    if (!myDeadDensity) {
      myDeadDensity = ScalarField("dead_density", dispatch());
    }
    arr3d<real> deaddens  = myDeadDensity->data();
  
    auto radMed = coords.radiiMed();
    auto phi = coords.phi();
    bool const half = physic().shape.half;

    size_t const hm = half ? ls.ni : (ls.ni %2 == 0 ? ls.ni/2 : (ls.ni-1)/2);
    CodeUnits const& units = physic().units;
    real const densityCGS  = units.density();
    real const distanceCGS = units.distance();
      
    kk::parallel_for(kk::TeamPolicy<>(ls.nr*ls.ns, kk::AUTO),
                     LBD(kk::TeamPolicy<>::member_type const& team) {
                       size_t const r = team.league_rank();
                       size_t const i = r/ls.ns;
                       size_t const j = r-i*ls.ns;
                       // 0 up to hm
                       kk::parallel_scan(kk::TeamThreadRange(team, 0, hm),
                                         [&](size_t const h, real& result, bool const& last) {
                                           if (h==0) {
                                             result = 0;
                                           } else {
                                             result += density(i,h,j)*densityCGS*radMed(i)*distanceCGS*(phi(h+1)-phi(h));
                                           }
                                           if (last) {
                                             deaddens(i,h,j) = result;
                                           }
                                         });
                       // ni-1 down to hm
                       kk::parallel_scan(kk::TeamThreadRange(team, 0, ls.ni-hm),
                                         [&](size_t const h, real& result, bool const& last) {
                                           size_t const hp = ls.ni-1 - h;
                                           if (h==0) {
                                             result = 0;
                                           } else {
                                             result += density(i,hp,j)*densityCGS*radMed(i)*distanceCGS*(phi(hp)-phi(hp-1));
                                           }
                                           if (last) {
                                             deaddens(i,hp,j) = result;
                                           }
                                         });
                     });
  }                                           
  
  std::optional<ScalarField> const&
  Disk::deadDensity() const {
    if(physic().starAccretion){
      if (physic().starAccretion->type == StarAccretion::WIND
          && !myDeadDensity) {
        Disk& self = const_cast<Disk&>(*this);
        switch (dispatch().grid().radialSpacing()) {
        case ARTH: self.computeDeadDensity<ARTH>(); break;
        case LOGR: self.computeDeadDensity<LOGR>(); break;
        }
      }
    }
    return myDeadDensity;
  }
  
  void
  Disk::trashViscosity() {
    myViscosityClean = false;
  }
    
  void
  Disk::trashPressure() {
    myPressureClean = false;
  }
  
  void
  Disk::trashDeadDensity() {
    myDeadDensity = std::nullopt;
  }
  
  void
  Disk::trashDensity() {
    if (physic().adiabatic) {
      trashSoundSpeed();
      trashTemperature();
    } else {
      trashPressure();
    }
    if (physic().starAccretion && physic().starAccretion->type == StarAccretion::WIND) {
      trashDeadDensity();
    }
  }

  
  ScalarField&
  Disk::mdensity() {
    trashDensity();
    return myDensity;
  }
  
  void
  Disk::trashSoundSpeed() {
    mySoundSpeedClean = false;
    if (!physic().adiabatic) {
      trashPressure();
    }
    // We might not have to:
    trashViscosity();
  }
  
  void
  Disk::trashEnergy() {
    if (physic().adiabatic) {
      trashSoundSpeed();
      trashPressure();
      if (physic().adiabatic) {
        trashTemperature();
      }
    }
  }

  std::optional<ScalarField>&
  Disk::menergy() {
    trashEnergy();
    return myEnergy;
  }
  
  void
  Disk::trashDerivedFields() { 
    trashPressure();
    trashSoundSpeed();
    trashViscosity();
    trashTemperature();
  }
    
  template<GridSpacing GS>
  void
  Disk::initRadialVelocity() {
    auto const& coords = dispatch().grid().as<GS>();
    auto radii  = coords.radii();
    auto radMed = coords.radiiMed();
    auto phi = coords.phi();
    real rcav = physic().density.cavity.radius;
    arr3d<real> vradial = myVelocity.radial().data();
    
    if (physic().starAccretion && physic().starAccretion->type == StarAccretion::WIND) {
      real const torqueCoeff       = physic().starAccretion->torqueCoeff();
      real const steepness         = physic().starAccretion->wind->filter;
      real const userActiveDensity = physic().starAccretion->wind->userActiveDensity;
      arr3d<real const> dead = deadDensity()->data();
      real const denormMin = std::numeric_limits<real>::denorm_min();
      kk::parallel_for("radial_init_wind", fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) {
			 real const x = -steepness*(1 - dead(i,h,j)/userActiveDensity);
			 real const vr = (x >= expMaxParam<real>
					  ? real(0)
					  : -2*torqueCoeff/(1+exp(x))/radii(i));
			 if (!(vr <= 0 || vr > denormMin)) {
			   kk::abort("Weird initial value for radial velocity");
			 }
			 vradial(i,h,j) = vr;
		       });
    } else if (rcav > 0 ) {
     // if the user didn't give an mdot as a star accretion rate, the code should give an error
     // else:
          real const start = physic().density.start;
          real const width = physic().density.cavity.width;
          real const rate  = physic().starAccretion->rate();
          real const slope = physic().density.slope;
          arr3d<real const> viscosity = this->viscosity().data();
          kk::parallel_for(fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) { 
                           vradial(i,h,j) = -1.5*viscosity(i,h,j)/radii(i);
                       });
    } else {
      real  const slope          = physic().density.slope;
      real  const flaringIndex   = physic().flaringIndex;
      arr3d<real const> viscosity = this->viscosity().data();
      kk::parallel_for(fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) { 
                         vradial(i,h,j) = (i == 0
                                           ? real(0)
                                           : (-3*(viscosity(i,h,j)*pow(radMed(i),(0.5-slope))
                                                  -viscosity(i-1,h,j)*pow(radMed(i-1),(0.5-slope)))
                                              /(pow(radii(i),(0.5-slope))
                                                *(radMed(i)-radMed(i-1)))));
                       });
      if (dispatch().first()) {
        kk::deep_copy(ring(vradial, 0), ring(vradial, 1));
      }
    }
  }

  namespace {
    template<GridSpacing GS>
    void
    initialDensityProfile2D( Disk const& disk, arr3d<real> density) {
      DiskPhysic const&  physic = disk.physic();
      auto const& coords    = disk.dispatch().grid().as<GS>();
      auto fullRange = fullFlatRange(coords);
      auto radMed    = coords.radiiMed();
      real rcav      = physic.density.cavity.radius;
      if(rcav > 0 ) { 
        real const start = physic.density.start;
        real const width = physic.density.cavity.width;
        real const slope = physic.density.slope;
        // Cavity modeled as in Robert Meheut 2020
        kk::parallel_for(fullRange,
                         LBD(size_t const  i, size_t const  j) {
                           density(i,0,j) = start*(pow(radMed(i)/rcav,-slope))*(1+tanh((radMed(i)-rcav)/width))/2; 
                         });
      } else {
        real start = physic.density.start;
        real slope = physic.density.slope;
        kk::parallel_for(fullRange,
                         LBD(size_t const  i, size_t const  j) { density(i,0,j) = start*pow(radMed(i),-slope); });
      }
    }
      
    template<GridSpacing GS>
    void
    initialDensityProfile3D( Disk const& disk, arr3d<real> density) {
      auto const& coords = disk.dispatch().grid().as<GS>();
      DiskPhysic const&   physic = disk.physic();
        
      auto radMed = coords.radiiMed();
      auto phiMed = coords.phiMed();
        
      real const aspectRatio  = physic.shape.aspectRatio;
      real const start        = physic.density.start;
      real const flaringIndex = physic.flaringIndex;
      real const slope        = physic.density.slope;
      real const width        = physic.density.cavity.width;
      real const radius       = physic.density.cavity.radius;
        
      kk::parallel_for(fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) {    
                         real distance = radMed(i)*sin(phiMed(h));
                         real densityfactor = ((start/aspectRatio/sqrt(2*PI))
                                               * pow(distance,-1-slope-flaringIndex));
                         // We keep this density distribution instead of the previous which comes
                         // from hydrostatic equilibrium since it is less steep and this favorize the 
                         // transition to equilibrium in Radiative and especially stellar case.
                         // Inital conditions from Nelson vertical shear instability 
                         real cs      =  aspectRatio*pow(distance, flaringIndex-0.5);
                         density(i,h,j) = densityfactor*exp(1/cs/cs*(1/radMed(i)-1/distance));
                         if(radius > 0){
                           density(i,h,j) *= (radius/radMed(i))*0.5*(1+tanh((radMed(i)-radius)/width));
                         }
                       });
    }
  }

  void
  Disk::signal(DiskInitEvent event) {
    using std::string;
    using h5::labels::HANDLER;
    for(auto const& [name, config] : physic().init.namedConfigs()) {
      auto f = DiskInitPlugin::get(name, config).evaluator();
      f(name, *this, event);
    }
  }
  
  template<GridSpacing GS>
  void
  Disk::initDensity() {
    Grid const& grid = this->dispatch().grid();
    GridSpacing spacing = grid.radialSpacing();
    
    switch (grid.dim()) {
    case 2:
      switch (spacing) {
      case ARTH: initialDensityProfile2D<ARTH>(*this, myDensity.data()); break;
      case LOGR: initialDensityProfile2D<LOGR>(*this, myDensity.data()); break;
      }; break;
    case 3:
      switch (spacing) {
      case ARTH: initialDensityProfile3D<ARTH>(*this, myDensity.data()); break;
      case LOGR: initialDensityProfile3D<LOGR>(*this, myDensity.data()); break;
      }; break;
    }
  }
    
  template<GridSpacing GS>
  void
  Disk::initEnergy() {
    auto const& coords = dispatch().grid().as<GS>();
    assert(physic().adiabatic);
    assert(physic().adiabatic->index != real(1)); // if only for rounding....
      
    arr3d<real>       energy      = myEnergy->data();
    auto              radMed      = coords.radiiMed();
    auto const        aspectRatio = physic().aspectRatio;
    real              index       = physic().adiabatic->index;
    arr3d<real const> density     = this->density().data();
      
    switch (coords.dim()) {
    case 3:
      {
        auto phiMed = coords.phiMed();
        kk::parallel_for(fullRange(coords),
                         LBD(size_t const  i, size_t const  h, size_t const  j) {
                           real const distance = radMed(i)*sin(phiMed(h));
                           energy(i,h,j) = (pow(aspectRatio(radMed(i)), 2)
                                            *density(i,h,j)
                                            /distance/(index-1));
                         });
      }
      break;
    case 2:
      {
        real const molecularWeight = physic().meanMolecularWeight;
        kk::parallel_for(fullFlatRange(coords),
                         LBD(size_t const  i, size_t const  j) {
                           real const distance = radMed(i);
                           energy(i,0,j) = (ipow<2>(aspectRatio(distance))
                                            *density(i,0,j)
                                            /distance/(index-1));
                         });
      }
      break;
    default:
      std::abort();
    }
  }
    
  template<GridSpacing GS>
  void
  Disk::initThetaVelocity() {
    using namespace std;
    auto const& coords = dispatch().grid().as<GS>();
    GridSizes const ls = coords.sizes();

    arr3d<real> theta = myVelocity.theta().data();
      
    auto radMed = coords.radiiMed();
    auto phiMed = coords.phiMed();
      
    real const slope        = physic().density.slope;
    real const flaringIndex = physic().flaringIndex;
    real const sigma        = physic().density.cavity.width;
      
    auto const aspectRatio = physic().aspectRatio;
      
    switch(coords.dim()) {
    case 3: {
      arr2d<real> thetaih = profileView("velocity_theta", coords);
      kk::parallel_for("init_theta_velocity", fullProfileRange(coords),
                       LBD(size_t const  i, size_t const  h) {
                         real const dist  = radMed(i)*sin(phiMed(h));
                         real const omega = sqrt(G/pow(dist, 3));
                         // Formula from Nelson  paper on Vertical Shear Instability 2013
                         thetaih(i,h) = (omega*dist*
                                         sqrt(((flaringIndex-slope-2)
                                               *pow(aspectRatio(radMed(i)),2)
                                               + 2*flaringIndex
                                               - (2*flaringIndex-1)*dist/radMed(i))));
                       });
      kk::parallel_for("ass_theta_velocity", fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) { theta(i,h,j) = thetaih(i,h); });
    }
      break;
    case 2: {
      real const rcav  = physic().density.cavity.radius;
      arr1d<real> thetai("velocity_theta", ls.nr);
      if(rcav > 0) {
        kk::parallel_for("init_theta_velocity", ls.nr,
                         LBD(size_t const  i) {
                           real const dist = radMed(i);
                           real const omega = sqrt(G/pow(dist, 3));
                           thetai(i) = (omega*dist
                                        *sqrt(1-pow(aspectRatio(dist),2)
                                        *(1+slope-2*flaringIndex-(dist/sigma)
                                        *(1-tanh((dist-rcav)/sigma)))));
                         });
      } else {
        kk::parallel_for("init_theta_velocity", ls.nr,
                         LBD(size_t const  i) {
                           real dist  = radMed(i);
                           real omega = sqrt(G/pow(dist, 3));
                           thetai(i) = (omega*dist
                                        *sqrt(1-pow(aspectRatio(radMed(i)),2)
                                              *(1+slope-2*flaringIndex)));
                         });
      }
      kk::parallel_for("ass_theta_velocity", fullFlatRange(coords),
                       LBD(size_t const  i, size_t const  j) { theta(i,0,j) = thetai(i); });
    }
      break;
    default:
      std::abort();
    }
  }
  
  void
  Disk::initGasMemory() {
    if (physic().adiabatic) {
      myEnergy = ScalarField("gasenergy", dispatch());
      if (physic().adiabatic->radiative) {
        myRadiativeEnergy = ScalarField("gasradenergy", dispatch());
      }
    }
  }
  
  template<GridSpacing GS>
  void
  Disk::initVelocity() {
    initRadialVelocity<GS>();
    kk::deep_copy(myVelocity.phi().data(), 0);
    initThetaVelocity<GS>();
  }

  template<GridSpacing GS>
  void
  Disk::initGasValue() {
    initGasMemory();
    initDensity<GS>();
    signal(DiskInitEvent::DENSITY);
    initVelocity<GS>();
    signal(DiskInitEvent::VELOCITY);
    if (physic().adiabatic) {
      initEnergy<GS>();
      signal(DiskInitEvent::ENERGY);
      if (physic().adiabatic->radiative) {
        myStellarRadiationSolver = StellarRadiationSolver::initial(*this);
      }
    }
  }

  void
  Disk::flashProfiles(DiskProfiles const& p) {
    myProfiles.reset(new DiskProfiles(p));
  }

  void
  Disk::flashProfiles() {
    using namespace h5l;
    auto const& coords = dispatch().grid();
    arr2d<real> densityp = profileView(DENSITY, coords);
    arr3d<real const> ddensity = density().data();
    arr2d<real> vradialp = profileView(path({VELOCITY, RADIAL}), coords);
    arr3d<real const> dvradial = velocity().radial().data();
    arr2d<real> vphip = profileView(path({VELOCITY, PHI}), coords);
    arr3d<real const> dvphi = velocity().phi().data();
    arr2d<real> vthetap = profileView(path({VELOCITY, THETA}), coords);
    arr3d<real const> dvtheta = velocity().theta().data();
    kk::parallel_for("flash_profiles", fullProfileRange(coords),
                     LBD(size_t const  i, size_t const  h) {
                       densityp(i,h) = ddensity(i,h,0);
                       vradialp(i,h) = dvradial(i,h,0);
                       vphip(i,h)    = dvphi(i,h,0);
                       vthetap(i,h)  = dvtheta(i,h,0);
                     });
      
    std::optional<arr2d<real>> energyp;
    if (physic().adiabatic) {
      energyp = profileView("energy", coords);
      arr3d<real const> denergy = energy()->data();
      arr2d<real> energyProfile = *energyp;
      kk::parallel_for("flash_energy_profiles", fullProfileRange(coords),
                       LBD(size_t const  i, size_t const  h) { energyProfile(i,h) = denergy(i,h,0); });
    }
    myProfiles.reset(new DiskProfiles(dispatch(),
                                      vradialp, vphip, vthetap,
                                      densityp,
                                      energyp));
  }
  
  real
  Disk::initialFrameVelocity() {
    switch (physic().referential.type) {
    case DiskReferential::CONSTANT:
      return *physic().referential.omega;
    case DiskReferential::COROTATING: {
      std::string planet = *physic().referential.guidingPlanet;
      return system()->keplerianFrequency(system()->planet(planet));
    }
    default:
      std::abort();
    }
  }

  Disk::NamedFields::NamedFields(Disk const& disk)
    : myMap() {
    using namespace h5l;
    typedef Map::value_type pair;
    myMap.insert(pair(path({VELOCITY,RADIAL}), disk.velocity().radial()));
    myMap.insert(pair(path({VELOCITY, THETA}), disk.velocity().theta()));    
    myMap.insert(pair(path({VELOCITY, PHI}), disk.velocity().phi()));    
    myMap.insert(pair(DENSITY, disk.myDensity));
    myMap.insert(pair(SOUND_SPEED, disk.soundSpeed()));
    myMap.insert(pair(PRESSURE, disk.pressure()));
    myMap.insert(pair(VISCOSITY, disk.viscosity()));
    if (disk.temperature()) {
      myMap.insert(pair(TEMPERATURE, *disk.temperature()));
    }
    if (disk.deadDensity()) {
      myMap.insert(pair(DEAD_DENSITY, *disk.deadDensity()));
    }
    if (disk.energy()) {
      myMap.insert(pair(ENERGY, *disk.energy()));
    }
    if (disk.radiativeEnergy()) {
      myMap.insert(pair(RADIATIVE_ENERGY, *disk.radiativeEnergy()));
      disk.stellarRadiationSolver().collectFields(myMap);
    }
    {
      std::string section(TRACERS);
      for (ScalarField const& trace: disk.tracers()) {
        std::string path = section + "/" + trace.name();
        myMap.insert(pair(path, trace));
      }
    }
    if (false) {
      std::string section(USER);
      for (auto const&[key, field] : disk.myUserFields) {
        std::string path = section + "/" + key;
        myMap.insert(pair(path, field));
      }
    }    
  }
  
  Disk::NamedFields::~NamedFields() {}

  /// \brief Returns the smallest box containing the intersection
  /// between the Roche sphere and the local managed grid.
  /// \returns std::nullopt if the Roche sphere is not on the managed grid.
  template <GridSpacing GS>
  std::optional<std::pair<GridSizes, GridSizes>> 
  computeRocheBox(Disk const& disk, Planet const& planet) {
    auto const& dispatch {disk.dispatch()};
    auto const& coords   {dispatch.grid().as<GS>()};
    GridSizes const ls   {coords.sizes()};
    auto const& units    {disk.physic().units};
    
    real const planetDist {planet.position().norm()};
    real const RocheLimit {std::cbrt(units.centralBodyMass()/3*planet.mass())*planetDist};
    range_t managedRadii  {disk.dispatch().managedRadii()};
    
    // ignore ghost cell, they'll be handled on other MPI slices
    size_t iBeg {begin(managedRadii)};
    size_t iEnd {end(managedRadii)};
    {
      real const minRoche {planetDist-RocheLimit};
      real const maxRoche {planetDist+RocheLimit};
      auto radii = coords.radii();      
      if (radii(iBeg) > maxRoche || radii(iEnd-1) < minRoche) {
        // Roche sphere does not intersect with the managed disk area
        return std::nullopt;
      } else {
        while (radii(iBeg+1) < minRoche)  { ++iBeg; }
        while (radii(iEnd-2) > maxRoche) { --iEnd; }
        assert(iBeg < iEnd);
      }
    }
    real hAngle = std::atan2(planet.position().y, planet.position().x);
    size_t jMin = 0;
    size_t jMax = ls.ns;
    {
      auto theta = coords.theta();
      while((theta(jMin) < hAngle - 2*RocheLimit/planetDist) && (jMin < ls.ns)) { ++jMin; }
      while((theta(jMax) > hAngle + 2*RocheLimit/planetDist) && (jMax > 0))  { --jMax; }
    }
    real vAngle = std::acos(planet.position().z/planetDist);
    size_t hMin, hMax;
    switch (dispatch.grid().dim()) {
    case 3:
      hMin = 0;
      hMax = ls.ni;
      {
        auto phi = coords.phi();
        while((phi(hMin) < vAngle - 2*RocheLimit/planetDist) && (hMin < ls.ni)) { ++hMin; }
        while((phi(hMax) > vAngle + 2*RocheLimit/planetDist) && (hMax > 0))  { --hMax; }
      }
      break;
    case 2:
      hMin = 0;
      hMax = 1;
      break;
    default: std::abort();
    }
    if (disk.physic().shape.half) { hMax = ls.ni-2; }  // Avoid mirror gridcell 
    if (iBeg < iEnd && hMin < hMax && jMin < jMax) {
      GridSizes beg = {iBeg, hMin, jMin};
      GridSizes end = {iEnd, hMax, jMax};
      return std::make_pair(beg,end);
    } else {
      return std::nullopt;
    }
  }

  std::optional<std::pair<GridSizes, GridSizes>> 
  getRocheBox(Disk const& disk, Planet const& planet) {
    switch (disk.dispatch().grid().radialSpacing()) {
    case GridSpacing::ARITHMETIC:  return computeRocheBox<GridSpacing::ARITHMETIC>(disk, planet);
    case GridSpacing::LOGARITHMIC: return computeRocheBox<GridSpacing::LOGARITHMIC>(disk, planet);
    }
  }
  
  using namespace configProperties;
  
  void
  to_json(nlohmann::json& j, SimuConfigWrapper<Disk> const& c) {
    j[snake(GRID)]   = c.subject.dispatch().grid().global();
    DiskPhysic const& physic = c.subject.physic();
    j[snake(PHYSIC)] = physic;
    if (physic.planetarySystem()) {
      j[snake(PLANETARY_SYSTEM)] = *physic.planetarySystem();
    }
  }
}
