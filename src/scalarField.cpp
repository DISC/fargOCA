// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <fstream>
#include <iostream>
#include <cassert>
#include <limits>
#include <numeric>
#include <list>
#include <filesystem>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include "mpidatatype_fwd.hpp"
#include "mpioperations.hpp"
#include "mpirequest.hpp"
#include "arraysUtils.hpp"
#include "scalarField.hpp"
#include "scalarFieldInterpolators.hpp"
#include "log.hpp"
#include "io.hpp"
#include "loops.hpp"
#include "h5io.hpp"
#include "arraysUtils.hpp"
#include "environment.hpp"

namespace fargOCA {
  namespace fs  = std::filesystem;
  namespace kk = Kokkos;
#define LBD KOKKOS_LAMBDA
#ifdef NDEBUG
#  define NLBD [&]
#else
#  define NLBD [=]
#endif

  namespace {

    void
    adjustRadius(ScalarField const& g, ScalarField& next, ScalarFieldInterpolators const& interpolators) {
      GridDispatch const& dispatch = next.dispatch();
      auto const& coords = dispatch.grid();
      GridSizes const gsz = coords.sizes();
      
      auto hNextRadii = hostRadiiValues(coords.global(), next.gridPos().radius);
      auto nextView = kk::create_mirror(next.data());
      for(size_t h=0; h < gsz.ni; ++h) {
        for(size_t j=0; j < gsz.ns; ++j) {
          auto interpolated = interpolators.radial(g, h, j);
          for (size_t i=0; i < gsz.nr; ++i) {
            nextView(i,h,j) = interpolated(hNextRadii(i));
          }
        }
      }
      kk::deep_copy(next.data(), nextView);
    }
    
    void
    adjustLayer(ScalarField const& g, ScalarField& next, ScalarFieldInterpolators const& interpolators) {
      if (g.dispatch().grid().dim() == 2) {
        // as for now, we do not inflate disk
        kk::deep_copy(next.data(), g.data());
      } else {
        GridDispatch const& dispatch = next.dispatch();
        auto const& coords = dispatch.grid();
        auto nextLayers = hostLayersValues(coords.global(), next.gridPos().layer);
        GridSizes const gsz = coords.sizes();
        auto nextView = kk::create_mirror(next.data());
        for(size_t i=0; i < gsz.nr; ++i) {
          for(size_t j=0; j < gsz.ns; ++j) {
            auto interpolated = interpolators.phi(g, i, j);
            for(size_t h=0; h < gsz.ni; ++h) {
              nextView(i,h,j) = interpolated(nextLayers(h));
            }
          }
        }
        kk::deep_copy(next.data(), nextView);
      }
    }
    
    void
    adjustSector(ScalarField const& g, ScalarField & next, ScalarFieldInterpolators const& interpolators) {
      if (g.dispatch().grid().sizes().ns == 1) {
        kk::deep_copy(next.data(), g.data());
      } else {
        GridDispatch const& dispatch        = next.dispatch();
        auto const& coords = dispatch.grid();
        auto nextSectors = hostSectorsValues(coords.global(), next.gridPos().sector);
        GridSizes const gsz = coords.sizes();
        auto nextView     = kk::create_mirror(next.data());
        for(size_t i=0; i < gsz.nr; ++i) {
          for(size_t h=0; h < gsz.ni; ++h) {
            auto interpolated = interpolators.theta(g, i, h);
            for(size_t j=0; j < gsz.ns; ++j) {
              nextView(i,h,j) = interpolated(nextSectors(j));
            }
          }
        }
        kk::deep_copy(next.data(), nextView);
      }
    }
    
    void
    convertMonoProc(ScalarField const& prev, ScalarField& next, ScalarFieldInterpolators const& interpolators) {
      GridDispatch const& nextDispatch = next.dispatch();
      assert(prev.dispatch().comm().size() == 1);
      GridDispatch const& prevDispatch = prev.dispatch();
      Grid const& prevCoords = prevDispatch.grid().global();
      Grid const& nextCoords = nextDispatch.grid().global();
      
      // Adjust on radius
      auto nextRadiusCoords = Grid::make(prevCoords.shape(), {nextCoords.sizes().nr, prevCoords.sizes().ni, prevCoords.sizes().ns}, prevCoords.radialSpacing());
      auto nextRadiusDispatch = GridDispatch::make(nextDispatch.comm(), *nextRadiusCoords);
      ScalarField nextRadScalarField("radius buffer", *nextRadiusDispatch, next.gridPos());
      adjustRadius(prev, nextRadScalarField, interpolators);
      
      // Adjust on sector
      auto nextSectorCoords = Grid::make(prevCoords.shape(), {nextCoords.sizes().nr, prevCoords.sizes().ni, nextCoords.sizes().ns}, prevCoords.radialSpacing());
      auto nextSectorDispatch = GridDispatch::make(nextDispatch.comm(), *nextSectorCoords);
      ScalarField nextSectorScalarField("sector buffer", *nextSectorDispatch, next.gridPos());
      adjustSector(nextRadScalarField, nextSectorScalarField, interpolators);
      
      // Adjust on layer
      auto nextLayerCoords = Grid::make(prevCoords.shape(), {nextCoords.sizes().nr, nextCoords.sizes().ni, nextCoords.sizes().ns}, prevCoords.radialSpacing());
      auto nextLayerDispatch = GridDispatch::make(nextDispatch.comm(), *nextLayerCoords);
      if (!compatible(nextLayerDispatch->grid().global(), nextDispatch.grid().global(), 1e-13)) {
	nextLayerDispatch->log() << "Rebind probably failed. Proceeding anyway.\n";
      }
      adjustLayer(nextSectorScalarField, next, interpolators);
    }
  }
    
  void
  ScalarField::convert(ScalarField const& prev, ScalarFieldInterpolators const& interpolators, int where) {
    GridDispatch const& nextDispatch = dispatch();
    fmpi::communicator const& comm = prev.dispatch().comm();
    assert(comm == nextDispatch.comm());
    assert(comm.size() >= where);
    
    bool here = where == comm.rank();
    fmpi::communicator single = comm.split(int(here));
    auto merged = prev.merged(where);
    if (here) {
      auto centNextDispatch = GridDispatch::make(single, nextDispatch);
      auto centPrevDispatch = GridDispatch::make(single, prev.dispatch());
      ScalarField centPrev(prev.name(), *centPrevDispatch);
      centPrev.dispatch(merged, 0);
      ScalarField centNext("centralized buffer", *centNextDispatch);
      convertMonoProc(centPrev, centNext, interpolators);
      merged = centNext.merged(0);
    }
    dispatch(merged, where);
  }
  
  ScalarField::ScalarField(std::string const& name, shptr<GridDispatch const> g, GridPositions const& pos) 
    : myDispatch(g),
      myData(scalarView<real>(name, g->grid().sizes())),
      myGridPos(pos)
  {
  }

  ScalarField::ScalarField(ScalarField&& o) 
    : myDispatch(std::move(o.myDispatch)),
      myData(std::move(o.myData)),
      myGridPos(o.myGridPos)
  {
    o.myDispatch.reset();
  }

  ScalarField&
  ScalarField::operator=(ScalarField&& g) {
    myDispatch  = std::move(g.myDispatch);
    myData      = std::move(g.myData);
    myGridPos   = g.myGridPos;
    g.myDispatch.reset();
    return *this;
  }
  
  ScalarField::ScalarField(ScalarField const& pg,
                           GridDispatch const& g,
                           ScalarFieldInterpolators const& interpolators) 
    : ScalarField(pg.name(), g.shared(), pg.gridPos()) {
    convert(pg, interpolators, 0);
  }
      
  ScalarField::ScalarField(ScalarField const& pg, GridDispatch const& g)
    : ScalarField(pg, g, ScalarFieldInterpolators::get()) {}
  
  ScalarField::ScalarField(ScalarField const& g)
    : ScalarField(g.name(), g.myDispatch, g.myGridPos) {
    kk::deep_copy(myData, g.myData);
  }

  ScalarField::ScalarField(std::string name , GridDispatch const& g, GridPositions const& pos)
    : ScalarField(name, g.shared(), pos) {}

  ScalarField::~ScalarField() {}

  void
  ScalarField::rebind(GridDispatch const& g, ScalarFieldInterpolators const& interpolators) {
    rebindTo(g);
  }

  void
  ScalarField::rebind(GridDispatch const& g) {
    rebindTo(g);
  }

  real
  ScalarField::min() const {
    auto mngd = dispatch().managedView(data());
    real lmin;
    kk::Min<real> minReducer(lmin);
    kk::parallel_reduce("field_div", kk::RangePolicy<>(0,mngd.size()),
                        LBD(int const i, real& pmin) { minReducer.join(pmin, mngd.data()[i]); },
                        minReducer);
    return fmpi::all_reduce(dispatch().comm(), lmin, fmpi::minimum<real>());
  }

  namespace details {
    template<class DeviceSpace>
    struct RdGhostImpl {
      static_assert(!std::is_same<DeviceSpace, kk::HostSpace>::value,
                    "wrong space for device");
      arr3d<real>                       device;
      kk::View<real ***, kk::HostSpace> host;
      RdGhostImpl(arr3d<real> outer) 
        : device(outer),
          host(outer.label()+"_mpi_host",
               outer.extent(0),
               outer.extent(1),
               outer.extent(2)
               ) {}
      void read() {
        fargOCA::cHost2Dev(host, device);
      }
    };
    
    template<>
    struct RdGhostImpl<kk::HostSpace> {
      arr3d<real> host;
      RdGhostImpl(arr3d<real> outer) : host(outer) {}
      void read() {}
    };
    
    template<class DeviceSpace>
    struct WrGhostImpl {
      static_assert(!std::is_same<DeviceSpace, kk::HostSpace>::value,
                    "wrong space for device");
      decltype(fargOCA::dev2CHost(std::declval<arr3d<real>>())) host;
      WrGhostImpl(arr3d<real> inner) 
        : host(fargOCA::dev2CHost(inner)) {}
    };
    
    template<>
    struct WrGhostImpl<kk::HostSpace> {
      arr3d<real> host;
      WrGhostImpl(arr3d<real> inner) : host(inner) {}
    };
    
  }
  struct RdGhost 
    : public details::RdGhostImpl<arr3d<real>::memory_space> {
    using RdGhostImpl::RdGhostImpl;
  };

  struct WrGhost 
    : public details::WrGhostImpl<arr3d<real>::memory_space> {
    using WrGhostImpl::WrGhostImpl;
  };
    
  void
  ScalarField::setGhosts() {
    std::vector<ScalarField*> me(1);
    me[0] = this;
    setGhosts(me);
  }

  namespace {
    template<class DevGhostsView, class CommGhostsView>
    void
    setGhostsImpl(GridDispatch const& dispatch,
                   std::vector<ScalarField*> const& fields) {
      fmpi::communicator const& comm = dispatch.comm();
      
      // The message tag is associated with the sender
      // For process N, inner tag is 2N, outer 2N+1
      int innerTag = 2*comm.rank();
      int outerTag = innerTag+1;
      
      auto const& coords = dispatch.grid();
      GridSizes const gsz = coords.sizes();
      size_t const nrGhost = dispatch.ghostSize();
      
      bool const lastProc  = dispatch.last();
      bool const firstProc = dispatch.first();
      
      // Communication buffers
      std::optional<CommGhostsView> outerSend, outerRecv, innerSend, innerRecv;
      fmpi::request outerSendReq, outerRecvReq, onnerSendReq, innerRecvReq;
      if (!lastProc) {
        outerRecv     = CommGhostsView("outer_recv",  fields.size(), nrGhost, gsz.ni, gsz.ns);
        outerRecvReq  = comm.irecv(comm.rank()+1, outerTag+1, outerRecv->data(), outerRecv->size());
        DevGhostsView packed("outgoing_packed_outer_ghosts",  fields.size(), nrGhost, gsz.ni, gsz.ns);
        for(size_t i = 0; i < fields.size(); ++i) {
          kk::deep_copy(kk::subview(packed, i, kk::ALL(), kk::ALL(), kk::ALL()), 
                        kk::subview(fields[i]->data(),
                                    range(gsz.nr-2*nrGhost, gsz.nr-nrGhost), kk::ALL(), kk::ALL()));
        }
        outerSend    = kk::create_mirror_view_and_copy(typename CommGhostsView::memory_space{}, packed);
        outerSendReq = comm.isend(comm.rank()+1, outerTag,   outerSend->data(), outerSend->size());
      } 
      if (!firstProc) {
        innerRecv = CommGhostsView("inner_recv",  fields.size(), nrGhost, gsz.ni, gsz.ns);
        innerRecvReq = comm.irecv(comm.rank()-1, innerTag-1, innerRecv->data(), innerRecv->size());
        DevGhostsView packed("outgoing_packed_inner_ghosts",  fields.size(), nrGhost, gsz.ni, gsz.ns);
        for(size_t i = 0; i < fields.size(); ++i) {
          kk::deep_copy(kk::subview(packed, i, kk::ALL(), kk::ALL(), kk::ALL()), 
                        kk::subview(fields[i]->data(),
                                    range(nrGhost, 2*nrGhost), kk::ALL(), kk::ALL()));
          
        }
        innerSend    = kk::create_mirror_view_and_copy(typename CommGhostsView::memory_space{}, packed);
        onnerSendReq = comm.isend(comm.rank()-1, innerTag,   innerSend->data(), innerSend->size());
      }
      // Wait for the data
      if (!firstProc) {
        innerRecvReq.wait();
        DevGhostsView packed = kk::create_mirror_view_and_copy(typename DevGhostsView::memory_space{}, *innerRecv);
        for(size_t i = 0; i < fields.size(); ++i) {
          kk::deep_copy(kk::subview(fields[i]->data(),
                                    range(0, nrGhost), kk::ALL(), kk::ALL()),
                        kk::subview(packed, i, kk::ALL(), kk::ALL(), kk::ALL()));
        }
        innerRecv = std::nullopt;
      }
      if (!lastProc) {
        outerRecvReq.wait();
        DevGhostsView packed = kk::create_mirror_view_and_copy(typename DevGhostsView::memory_space{}, *outerRecv);
        for(size_t i = 0; i < fields.size(); ++i) {
          kk::deep_copy(kk::subview(fields[i]->data(),
                                    range(gsz.nr-nrGhost, gsz.nr), kk::ALL(), kk::ALL()),
                        kk::subview(packed, i, kk::ALL(), kk::ALL(), kk::ALL()));
        }
        outerRecv = std::nullopt;
      }
      if (!lastProc)  { outerSendReq.wait(); }
      if (!firstProc) { onnerSendReq.wait(); }
    }
  }
  
  void
  ScalarField::setGhosts(std::vector<ScalarField*> const& fields) {
    if (fields.size() > 0) {
      GridDispatch const& dispatch = fields.front()->dispatch();
      if (dispatch.comm().size() > 1) {
        // Store a packed view with all fields ghost areas on the device
        typedef kk::View<real****> DevGhostsView;
        if (mpiAddressableMemorySpace(arr3d<real const>::memory_space{})) {
          setGhostsImpl<DevGhostsView, DevGhostsView>(dispatch, fields);
        } else {
          // Go through the mirror view.
          setGhostsImpl<DevGhostsView, DevGhostsView::HostMirror>(dispatch, fields);
        }
        for(size_t i = 0; i < fields.size(); ++i) {
          fields[i]->myNbDirtyGhosts = 0; // beware of async implementation.
        }
      }
    }
  }

  namespace {
    [[maybe_unused]]
    bool
    checkScalarFieldDims(std::vector<size_t> const& dims, GridDispatch const& dispatch) {
      if (dims.size() < 2 || dims.size() > 3) {
        dispatch.log() << "Wrong number of dimensions " << dims.size() << '\n';
        return false;
      } 
      if (dispatch.grid().dim() == 2 && dims[1] != 1) {
        dispatch.log() << "Unexpected number of layers " << dims[1] << ", should be 1.\n";
        return false;
      } 
      auto const& coords = dispatch.grid().global();
      auto gsz = coords.sizes();
      if (dims[0] != gsz.nr || dims[1] != gsz.ni || dims[2] != gsz.ns) {
        dispatch.log() << "Unexpected dimensions: (" << dims[0] << "," << dims[1] << "," << dims[2]
                   << ") instead of (" << coords.global().sizes() << "}\n";
        return false;
      }
      return true;
    }
  }

  void
  ScalarField::readH5(HighFive::File const& file, std::string path) {
    readH5(file.getGroup(path));
  }
  
  void
  ScalarField::readH5(HighFive::Group const& group) {
    using namespace HighFive;
    size_t const nr  = dispatch().grid().sizes().nr;
    DataSet fieldDS = group.getDataSet(h5::POLAR_GRID);
    std::vector<size_t> dims = fieldDS.getDimensions();
    assert(checkScalarFieldDims(dims, dispatch()));
    dims[0] = nr; // only read local section
    std::vector<size_t> offsets({dispatch().radiiIndexRange().first, 0u, 0u}); // from first local ring
    harr3d<real> hdata(data().label(), dims[0], dims[1], dims[2]);
    fieldDS.select(offsets, dims).read(hdata.data());
    myGridPos = GridPositions( fieldDS.getAttribute(h5::RADIUS_POSITION).read<GridPosition>(),
			       fieldDS.getAttribute(h5::SECTOR_POSITION).read<GridPosition>(),
			       dispatch().grid().dim() == 3 
			       ? fieldDS.getAttribute(h5::LAYER_POSITION).read<GridPosition>()
			       : GridPosition::MED);
    cHost2Dev(hdata, data());
  }
  
  void
  ScalarField::readH5(std::string fname, std::string path) {
    using namespace HighFive;
    File  file(fname, File::ReadOnly);
    readH5(file.getGroup(path));
  }

  void
  ScalarField::writeH5(std::optional<HighFive::Group> group, int writer) const {
    assert(bool(group) == (dispatch().comm().rank()==writer));
    auto data = merged(writer);
    if (writer == dispatch().comm().rank()) {
      writeField(*group, *data);
    }
  }
  
  void
  ScalarField::writeField(HighFive::Group group, arr3d<real const> merged) const {
    assert(compatible(dispatch().grid().global().sizes(), merged));
    h5::writePolarGrid(group, merged, gridPos());
  }

  std::optional<arr3d<real>>
  ScalarField::merged(int where) const {
    return dispatch().joinManaged(data(), where);
  }

  void
  ScalarField::dispatch(std::optional<arr3d<real const>> merged, int where) {
    dispatch().dispatch(merged, data(), where);
  }

  void
  ScalarField::ensureCleanGhosts(int nb) {
    assert(nb <= dispatch().ghostSize());
    int ghostsLeft = dispatch().ghostSize() - myNbDirtyGhosts;
    if (nb > ghostsLeft) {
      setGhosts();
      assert(myNbDirtyGhosts == 0);
      nb -= ghostsLeft;
      if (nb > 0) {
        myNbDirtyGhosts = nb;
      }
    }
  }
  
  void
  ScalarField::trashGhosts(int nb) {
    myNbDirtyGhosts += nb;
    assert(nb <= dispatch().ghostSize());
  }

  void
  swap(ScalarField& g1, ScalarField& g2) {
    std::swap(g1.myDispatch, g2.myDispatch);
    std::swap(g1.myData,     g2.myData); 
    std::swap(g1.myGridPos,  g2.myGridPos);
  }

}
