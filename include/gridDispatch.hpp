// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_GRID_DISPATCH_H
#define FARGOCA_GRID_DISPATCH_H

#include <vector>
#include <iosfwd>
#include <string>
#include <memory>
#include <highfive/H5File.hpp>

#include "allmpi.hpp"

#include "precision.hpp"
#include "cartesianGridCoords.hpp"
#include "gridCellSizes.hpp"
#include "debug.hpp"
#include "shared.hpp"
#include "arrays.hpp"
#include "fieldStack.hpp"

namespace fargOCA {
  class CartesianGridCoords;
  class GridCellSizes;
  class Grid;

  template<typename Int1, typename Int2>
  typename std::enable_if<std::is_integral<Int1>::value && std::is_integral<Int2>::value, range_t>::type
  range(Int1 left, Int2 right) { return range_t(size_t(left), size_t(right)); }
  
  /// \brief Decribe the way the grid is dispatched on the MPI prorcesses.
  class GridDispatch
    : public std::enable_shared_from_this<GridDispatch> {
  public:

    /// \brief Local Grid coordinates 
    Grid const& grid() const { return *myGrid; }

    auto const& coords(auto gs) const { return grid().as(gs); }
    
    template<class T> using opt = std::optional<T>;

    /// \brief Zeus-like overlap kernel. 2:transport; 2: source, 1:viscous stress.
    static int const DEFAULT_GHOST_SIZE = 5;    

    static shptr<GridDispatch const>
    make(fmpi::communicator const& comm, GridDispatch const& dispatch, size_t ghosts = DEFAULT_GHOST_SIZE);

    static shptr<GridDispatch const>
    make(fmpi::communicator const& comm, Grid const& grid, size_t ghosts = DEFAULT_GHOST_SIZE);

    static shptr<GridDispatch const>
    make(GridDispatch const& grid, size_t ghosts);
    
    ~GridDispatch();

    shptr<GridDispatch const> shared() const { return shared_from_this(); }
    
    fmpi::communicator const& comm() const { return myComm; }
    bool master() const { return myComm.rank() == 0; }
    bool first()  const { return myComm.rank() == 0; }
    bool last()   const { return myComm.rank() == myComm.size() -1; }
    size_t ghostSize() const { return myGhostSize; }

    /// \brief The radii managed by this process.
    /// Ghost are excluded
    /// \param gMinShift  if first proc, will return this instead of 0
    /// \param gMaxShift  if last proc, will remove this instead of 0
    range_t managedRadii(size_t gMinShift, size_t gMaxShift) const;
    range_t managedRadii(size_t gShift = 0) const { return managedRadii(gShift, gShift); }
    
    template<typename T, class... RP>
    Kokkos::View<T***, RP...>
    managedView(Kokkos::View<T***, RP...> v) const { return Kokkos::subview(v, managedRadii(),  Kokkos::ALL(), Kokkos::ALL()); }
    template<typename T, class... RP>
    Kokkos::View<T**, RP...>
    managedView(Kokkos::View<T**, RP...> v) const { return Kokkos::subview(v, managedRadii(), Kokkos::ALL()); }
    template<typename T, class... RP>
    Kokkos::View<T*, RP...>
    managedView(Kokkos::View<T*, RP...> v) const { return Kokkos::subview(v, managedRadii()); }
    
    /// \brief Radii stored on this process
    range_t radiiIndexRange() const { return myRadiiIndexRange; }

    template<typename T, class... RP>
    Kokkos::View<T***, RP...>
    localView(Kokkos::View<T***, RP...> v) const { return Kokkos::subview(v, radiiIndexRange(),  Kokkos::ALL(), Kokkos::ALL()); }
    template<typename T, class... RP>
    Kokkos::View<T**, RP...>
    localView(Kokkos::View<T**, RP...> v) const { return Kokkos::subview(v, radiiIndexRange(), Kokkos::ALL()); }
    template<typename T, class... RP>
    Kokkos::View<T*, RP...>
    localView(Kokkos::View<T*, RP...> v) const { return Kokkos::subview(v, radiiIndexRange()); }

    std::optional<arr3d<real>> joinManaged(arr3d<real const> local, opt<int> where) const;
    std::optional<arr2d<real>> joinManaged(arr2d<real const> local, opt<int> where) const;
    std::optional<arr1d<real>> joinManaged(arr1d<real const> local, opt<int> where) const;
    arr3d<real> joinManaged(arr3d<real const> local) const { return *joinManaged(local, std::nullopt); }
    arr2d<real> joinManaged(arr2d<real const> local) const { return *joinManaged(local, std::nullopt); }
    arr1d<real> joinManaged(arr1d<real const> local) const { return *joinManaged(local, std::nullopt); }
    
    void dispatch(std::optional<arr3d<real const>> merged, arr3d<real> local, int where) const;

    GridCellSizes       cellSizes() const { return *myCellSizes; }
    CartesianGridCoords cartesian() const { return *myCartesianCoords; }
    
    std::ostream& log(int p = 0) const;

    void dumpMpiLayout() const;

    /// \brief Stored radii per proc
    harr1d<int const> nbRadii(int slotSize = 1)              const;
    /// \brief Managed radii per proc
    harr1d<int const> nbManagedRadii(int slotSize = 1)       const;
    
    template <typename T, GridDirection... Ds> FieldStack<T,Ds...>& temporaries() const;
    template <typename T> FieldStack3D<T>& temporaries3D() const { return temporaries<T,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>(); }
    
    template<typename T, size_t SZ> auto local3DFields() const { return std::make_shared<LocalFieldStack3D<T,SZ>>(temporaries3D<T>()); }
    
  private:
    GridDispatch(fmpi::communicator const& comm, Grid const& grid, int ghosts);

    void initLocalRadial();
    void initRadiusSizes();

    template<typename AT, class... P >
    void 
    joinManagedHelper(Kokkos::View<AT,P...> local, std::optional<int> where, 
                      std::optional<Kokkos::View<typename Kokkos::View<AT,P...>::non_const_data_type, P...>>& global) const;
    /// \brief Stored radii per proc
    harr1d<int const> nbRadiiDispls(int slotSize = 1)        const;
    /// \brief Managed radii per proc
    harr1d<int const> nbManagedRadiiDispls(int slotSize = 1) const;

  private:
    fmpi::communicator const& myComm;
    // The communicator's rank and size are not supposed to change
    range_t                      myRadiiIndexRange;
    std::shared_ptr<Grid const>  myGrid;
    harr1d<int>                  myNbRadii;
    harr1d<int>                  myNbManagedRadii;
    harr1d<int>                  myNbRadiiDispls;
    harr1d<int>                  myNbManagedRadiiDispls;
    int                          myGhostSize;
    
    std::unique_ptr<GridCellSizes>       myCellSizes;
    std::unique_ptr<CartesianGridCoords> myCartesianCoords;
    std::unique_ptr<FieldStack3D<real>>  myRealTemporaries3D;
    std::unique_ptr<FieldStack3D<int>>   myIntTemporaries3D;
  };

  inline
  range_t
  GridDispatch::managedRadii(size_t gMinShift, size_t gLastShift) const {
    size_t begin = size_t(first() ? gMinShift : ghostSize());
    size_t end   = size_t(grid().sizes().nr - (last()  ? gLastShift : ghostSize()));
    return {begin, end};
  }

  template<typename T> auto KOKKOS_INLINE_FUNCTION ring(arr3d<T> a,       int i) { return Kokkos::subview(a, i, Kokkos::ALL(), Kokkos::ALL()); }
  template<typename T> auto KOKKOS_INLINE_FUNCTION ring(arr3d<T const> a, int i) { return Kokkos::subview(a, i, Kokkos::ALL(), Kokkos::ALL()); }
  template<typename T> auto KOKKOS_INLINE_FUNCTION ring(arr3d<T> a,       int i, int j) { return Kokkos::subview(a, i, j, Kokkos::ALL()); }
  template<typename T> auto KOKKOS_INLINE_FUNCTION ring(arr3d<T const> a, int i, int j) { return Kokkos::subview(a, i, j, Kokkos::ALL()); }

  template<size_t INC=1>
  size_t
  KOKKOS_INLINE_FUNCTION nsnext(size_t ns, size_t j) {
    if constexpr(debug::assertInKokkosInline) {
      assert(INC < ns);
      assert(j   < ns);
    }
    size_t const jp = j+INC;
    return jp < ns ? jp : jp -ns;
  }

  template<size_t DEC=1>
  size_t
  KOKKOS_INLINE_FUNCTION nsprev(size_t ns, size_t j) {
    if constexpr(debug::assertInKokkosInline) {    
      assert(DEC < ns);
      assert(j   < ns);
    }
    return j < DEC ? (j+ns)-DEC :j-DEC;
  }
}

#endif
