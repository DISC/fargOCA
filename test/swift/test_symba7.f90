! Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
!
! This file is part of FargOCA.
! FargOCA is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! any later version.
!
! FargOCA is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

program test_symba7
  use symba7
  implicit none  
  
  type(s7_integrator), pointer :: s7itg
  type(C_ptr)                  :: cs7itg
  double precision    :: tstop,dt
  logical(C_int16_t) :: merge
  merge = .false.
  tstop = 100.0
  dt = 0.25d0
  
  cs7itg = s7_new(3, 0.d0, merge, 1.d-8)
  call C_F_POINTER(cs7itg, s7itg)
  call s7_set_boundaries(cs7itg, -1.d0, 1000.d0, -1.d0, -1.d0)
  call s7_set_planet(cs7itg,1, 1d0, 0d0, 0d0, 0d0, 0d0, 0d0, 0d0, 0d0, 0d0)
  call s7_set_planet(cs7itg,2, 1d-4, 1d0, 0d0, 0d0, 0d0, 1d0, 0d0, 1d-3, 3.218297949d-2)
  call s7_set_planet(cs7itg,3, 1d-4, 1.3782407725d0, 0d0, 0d0, 0d0, 8.517996421d-1, 0d0, 1d-3, 4.435589451d-2)
  call s7_write(s7itg)

  do while ( (s7itg%time .le. tstop) .and. (s7itg%nbodies.gt.1) )
     call s7_whole_step(s7itg, dt)
     call s7_write(s7itg)
  enddo
  call s7_delete(cs7itg)
  call util_exit(0)

end program test_symba7

