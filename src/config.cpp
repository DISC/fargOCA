// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>
#include <fstream>
#include <iomanip>
#include <memory>

#include <nlohmann/json.hpp>
#include <highfive/H5File.hpp>

#include "h5Labels.hpp"
#include "h5io.hpp"
#include "io.hpp"

#include "config.hpp"

namespace fargOCA {
  
  Config::Config(boost::property_tree::ptree const& properties) 
    : myProperties() {
    for (auto const& [n,v] : properties) {
      myProperties[n] = v.data();
    }
  }

  Config::Config(HighFive::Group g)
    : myProperties() {
    for(std::string const& name : g.listAttributeNames()) {
      myProperties[name] = *h5::attributeValue<std::string>(g, name);
    }
  }

  void
  Config::dump(std::ostream& out, int tab) const {
    for (auto const& [name, value] : myProperties) {
      out << tabs(tab) << "* " << name << " -> " << value << '\n';
    }
  }

  void
  Config::dump(ConfigMap const& configs, std::ostream& out, int tab) {
    for(auto const& [name, cfg] : configs) {
      out << tabs(tab) << "> " << name << '\n';
      cfg.dump(out, tab+1);
    }
  }
  
  void
  Config::writeH5(HighFive::Group parent, std::string gname) const {
    auto g = parent.createGroup(gname);
    for(auto const& [k,v] : myProperties) {
      g.createAttribute(k, v);
    }
  }

  void
  Config::writeH5(ConfigMap const& configs, HighFive::Group& container) {
    for(auto const& [name, cfg] : configs) {
      cfg.writeH5(container, name);
    }
  }
  
  void
  Config::readH5(ConfigMap& configs, HighFive::Group const& container) {
    for (std::string name : container.listObjectNames()) {
      configs.push_back(std::make_pair(name, Config(container.getGroup(name))));
    }
  }
  
  template<> 
  std::optional<std::string>
  Config::value(std::string name) const {
    auto const& iter = myProperties.find(name);
    if (iter == myProperties.end()) {
      return std::nullopt;
    } else {
      return iter->second;
    }
  }

  template<> 
  std::optional<bool>
  Config::value(std::string name) const {
    std::optional<std::string> sv = value<std::string>(name);
    if (bool(sv)) {
      std::istringstream input(*sv);
      bool b = false;
      input >> std::boolalpha >> b;
      if (!input.bad()) {
        return b;
      } else {
        std::cerr << "Tried to interpret " << *sv << " as bool\n";
        throw boost::bad_lexical_cast();
        return std::nullopt;
      }
    } else {
      return std::nullopt;
    }
  }

  bool
  Config::enabled(std::string name, bool def) const { return value<bool>(name).value_or(def); }

  Config
  toUpper(Config const& properties) {
    Config::Properties m;
    for(auto& [k,v] : properties.registered()) {
      std::string K = k;
      std::transform(k.begin(), k.end(), K.begin(), 
                     [](unsigned char c){ return std::toupper(c); });
      m[K] = v;
    }
    return Config(m);
  }

  Config
  toLower(Config const& properties) {
    Config::Properties m;
    for(auto& [K,v] : properties.registered()) {
      std::string k = K;
      std::transform(K.begin(), K.end(), k.begin(), 
                     [](unsigned char c){ return std::tolower(c); });
      m[k] = v;
    }
    return Config(m);
  }

  ConfigCheckFct
  propertyNameChecker(std::set<std::string> allowed) {
    return [=](Config const& properties, std::ostream& log) -> bool {
      for(auto [k,v] : properties.registered()) {
        if (k == h5::labels::HANDLER) {
          continue;
        }
        if (allowed.find(k) == allowed.end()) {
          log << "Illegal property '" << k << "'(=" << v << ") used. "
              << "Autorized properties are: ";
          for (std::string n : allowed) {
            log << n << " ";
          }
          log << '\n';
          return false;
        }
      }
      return true;
    };
  }

  using json = nlohmann::json;

  void
  to_json(json& j, Config const& p) {
    for(auto const& [k,v] : p.registered()) {
      j[k] = v;
    }
  }
  
  void
  from_json(json const& j, Config& p) {
    Config::Properties m;
    for(auto const& [k,v] : j.items()) {
      m[k] = v.get<std::string>();
    }
    p = Config(m);
  }

  void
  to_json(json& j, ConfigMap const& p) {
    for(auto const& [name,cfg] : p) {
      j[name] = cfg;
    }
  }
  
  void
  from_json(json const& j, ConfigMap& m) {
    for(auto const& [name,cfg] : j.items()) {
      Config p{};
      cfg.get_to(p);
      m.push_back(std::make_pair(name,p));
    }
  }
}
