// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2020, Gabriele Pichierri, pichierri<at>mpia.de
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <variant>

#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <highfive/H5File.hpp>

#include "planetarySystem.hpp"
#include "planetarySystemEngine.hpp"
#include "diskPhysic.hpp"
#include "fundamental.hpp"
#include "log.hpp"
#include "h5io.hpp"
#include "h5log.hpp"
#include "optIO.hpp"
#include "configProperties.hpp"
#include "codeUnits.hpp"

using namespace std;
namespace pt = boost::property_tree;

namespace fargOCA {
  using namespace configProperties;

  std::ostream& 
  operator<<(std::ostream& out, Planet const& p) {
    out << "{m:" << p.mass()
	<< ",pos:" << p.position() << ",vel:" << p.velocity()
	<< ", r:" << p.radius() << ", hr:" << p.userHillRadius() << "}";
    return out;
  }
    
  real
  eccAnomaly(real meanAnomaly, real eccentricity, int nsteps) {
    /*
       Kepler's Equation solver: 
       As described in https://www.aanda.org/articles/aa/full_html/2018/11/aa33162-18/aa33162-18.html
       Returns the eccentric anomaly capE given a mean anomaly capM and the eccentricity
       Recommended value for nsteps is 50
    */
    real const capM = meanAnomaly;
    // capE stand for Eccentric Anomaly
    real capE    = 2*PI*std::floor(capM/(2*PI)+0.5);
    real a;
    for (int i = 1; i <= nsteps; ++i) {
        a = PI/std::pow(2,i);
        if (capE - eccentricity*std::sin(capE) > capM ) {
            a = -a;
        }
        capE += a;
    }
    return capE;
  }
  
  void
  orbital2Cartesian(OrbitalElements const& orbEl, real gm, Triplet& pos, Triplet& vel, int nsteps) {
    /*
      Translates a set of orbital elements (see below) into heliocentric positions and velocities
      Based on orbel_el2xv.f from swift_symba

      OrbitalElements orbEl contains:
      a = semi-major axis => orbEl.distance
      e = eccentricity    => orbEl.eccentricity
      inc = inclination   => orbEl.inclination
      lambda = mean longitude => orbEl.meanLongitude
      varpi = longitude of the pericenter => orbEl.longitudeOfPericenter
      capom = longitude of ascending node => orbEl.longitudeOfNode

      Here we also need:
      omega = argument of pericenter = lambda - varpi
      capm = mean anomaly = varpi - capom

      gm = standard gravitational parameter
      
      nsteps is the number of steps for our Kepler's Equation solver (eccAnomaly); recommended value is nsteps = 50

      Output:
      x,y,z  =  position of planet    => pos
      vx,vy,vz =  velocity of planet  => vel
      
      # Start by defining matrix elements
      # call orbel_scget(omega,sp,cp)
      # call orbel_scget(capom,so,co)
      # call orbel_scget(inc,si,ci)
      
      sp = sin(omega)
      cp = cos(omega)
      
      so = sin(capom)
      co = cos(capom)
      
      si = sin(inc)
      ci = cos(inc)
    */

    real omega = std::fmod(orbEl.longitudeOfPericenter - orbEl.longitudeOfNode, 2*PI);
    real capm  = std::fmod(orbEl.meanLongitude - orbEl.longitudeOfPericenter, 2*PI);


    real sp = std::sin(omega);
    real cp = std::cos(omega);
    
    real so = std::sin(orbEl.longitudeOfNode);
    real co = std::cos(orbEl.longitudeOfNode);
    
    real si = std::sin(orbEl.inclination);
    real ci = std::cos(orbEl.inclination);
    
    real d11 = cp*co - sp*so*ci;
    real d12 = cp*so + sp*co*ci;
    real d13 = sp*si;
    real d21 = -sp*co - cp*so*ci;
    real d22 = -sp*so + cp*co*ci;
    real d23 = cp*si;
    /*
      # case of an elliptical orbit (we exclude parabolic/hyperbolic orbits for simplicity)
      
      # first solve Kepler's equation
      # cape = orbel_ehybrid(e,capm) replaced by our own solver
    */
    real cape = orbEl.eccentricity == 0 ? capm : eccAnomaly(capm, orbEl.eccentricity, nsteps);
    real scap = std::sin(cape);
    real ccap = std::cos(cape);
    real sqe = std::sqrt(1 - ipow<2>(orbEl.eccentricity));
    real sqgma = std::sqrt(gm*orbEl.semiMajorAxis);
    real xfac1 = orbEl.semiMajorAxis*(ccap - orbEl.eccentricity);
    real xfac2 = orbEl.semiMajorAxis*sqe*scap;
    real ri = 1/(orbEl.semiMajorAxis*(1 - orbEl.eccentricity*ccap));
    real vfac1 = -ri * sqgma * scap;
    real vfac2 = ri * sqgma * sqe * ccap;
      
    
    pos.x = d11*xfac1 + d21*xfac2;
    pos.y = d12*xfac1 + d22*xfac2;
    pos.z = d13*xfac1 + d23*xfac2;
    vel.x = d11*vfac1 + d21*vfac2;
    vel.y = d12*vfac1 + d22*vfac2;
    vel.z = d13*vfac1 + d23*vfac2;
  }
  
  void
  Planet::dump(std::ostream& out) const {
    out << "{pos: " << position() << ", vel:" << velocity() << ", mass: "<< mass() << "}";
  }

  real
  eccentricity(CodeUnits const& units, Planet const& planet) {
    Triplet const angularMomentum = cross(planet.position(), planet.velocity());
    real const mass = planet.mass() + units.centralBodyMass();
    real const energy = planet.velocity().norm2()/2 - mass/planet.position().norm();
    real const semiMajorAxis = mass/(2*energy);
    real const eccentricity = std::sqrt(1 - angularMomentum.norm2()/(mass*semiMajorAxis));
    return eccentricity;
  }

  void
  PlanetarySystem::dump(std::ostream& out) const {
    for (auto const& np : myPlanets) { 
      np.second.dump(out); 
      out << std::endl;
    }
  }

  void
  Planet::writeH5(HighFive::Group& grp) const {
    grp.createDataSet("state", *this);
  }

  bool
  Planet::hasRadiusAndHill(HighFive::DataSet const& ds) {
    try {
      using namespace HighFive;
      DataType tp = ds.getDataType();
      if (tp.getClass() == DataTypeClass::Compound) {
        CompoundType ct(std::move(tp));
        int missed = 2;
        for (auto mbr : ct.getMembers()) {
          if (mbr.name == "radius" || mbr.name == "Hill_radius") {
            missed--;
          }
        }
        assert(missed == 0 || missed == 2); // both present or missing
        return missed == 0;
      } else {
        return false;
      }
    } catch (std::exception const& e) {
      std::cerr << "Probing radius and Hill radius of something not a planet.\n";
      throw e;
    }
  }

  bool
  Planet::readH5(HighFive::Group const& grp) {
    if (grp.exist("state")) {
      HighFive::DataSet ds = grp.getDataSet("state");
      if (!hasRadiusAndHill(ds)) {
        std::cerr << "Old planet format detected, please run disk_upgrade on disk file.\n";
        std::exit(EX_DATAERR);
      }
      ds.read(*this);
      if (std::isnan(myHillRadius)) { myHillRadius = -1; }
      if (std::isnan(myRadius)) { myRadius = -1; }
    } else {
      std::cerr << "Format error in planet section. Please consider running disk_upgrade on disk files.\n";
      std::exit(EX_DATAERR);
      return false;
    }
    return true;
  }
  
  bool
  PlanetarySystem::hasPlanet(std::string name) const {
    return myPlanets.find(name) != myPlanets.end();
  }

  Planet const&
  PlanetarySystem::planet(std::string name) const {
    return myPlanets.find(name)->second;
  }

  namespace {
    std::map<std::string,Planet>
    initPlanets(DiskPhysic const& physic) {
      std::map<std::string,Planet> planets;
      CodeUnits const& units = physic.units;
      for( auto const& [name,planetPhysic] : physic.planetarySystem()->planets) {
        Triplet pos, vel;
        real const mass = planetPhysic.initialMass();
        real const grav = G*(units.centralBodyMass()+mass);
        orbital2Cartesian(planetPhysic.orbitalElements, grav, pos, vel);
        planets[name] = Planet{mass, pos, vel, planetPhysic.radius, planetPhysic.hillRadius};
      }
      return planets;
    }
  }
  
  PlanetarySystem::PlanetarySystem(DiskPhysic const& physic,
                                   std::map<std::string,Planet> const& planets)
    : myPhysic(physic.shared()),
      myLostMass(0),
      myLostMassZ(0),
      myLostOnAccretion(0),
      myPrimaryAcceleration(0,0,0),
      myPlanets(planets),
      myEngine(PlanetarySystemEngine::make(physic.planetarySystem()->engine.name, 
                                           *this,
                                           physic.planetarySystem()->engine.config))
  {}

  PlanetarySystem::PlanetarySystem(DiskPhysic const& physic)
    : PlanetarySystem(physic, initPlanets(physic)) {}
  
  PlanetarySystem::~PlanetarySystem() {}
  
  shptr<PlanetarySystem>
  PlanetarySystem::make(DiskPhysic const& cfg) {
    if (bool(cfg.planetarySystem())) {
      return shptr<PlanetarySystem>(new PlanetarySystem(cfg)); 
    } else {
      return nullptr;
    }
  }
  
  shptr<PlanetarySystem>
  PlanetarySystem::make(DiskPhysic const& cfg, std::map<std::string,Planet> const& planets) {
    return shptr<PlanetarySystem>(new PlanetarySystem(cfg, planets));
  }

  shptr<PlanetarySystem>
  PlanetarySystem::make(DiskPhysic const& cfg, HighFive::Group const& state) {
    auto system = make(cfg);
    if (!system->readH5(state)) {
      return nullptr;
    } else {
      return system;
    }
  }
  
  void
  PlanetarySystem::listPlanets( std::ostream& out ) const
  {
    for (auto const& np : planets()) {
      std::string name    = np.first;
      Planet const& planet = np.second;
      out << name << "-> mass: " << planet.mass() << ", pos.: " << planet.position() << ", vel.: " << planet.velocity() << '\n';
    }
  }
  
  void
  PlanetarySystem::rotate(real angle)
  {
    real sint = sin(angle);
    real cost = cos(angle);
    for (auto& np: myPlanets) { 
      Planet& p = np.second;
      Triplet pos = p.position();
      p.setPosition(Triplet(pos.x*cost + pos.y*sint,
                            -pos.x*sint + pos.y*cost,
                            pos.z));
      Triplet vel = p.velocity();
      p.setVelocity(Triplet(vel.x*cost+vel.y*sint,
                            -vel.x*sint+vel.y*cost,
                            vel.z));
    }
  }
  
  void
  PlanetarySystem::advance(real dt) {
    myEngine->advance(dt);
  }

  real
  keplerianFrequency(CodeUnits const& units, Planet const& planet) {
    real const coupleMass = planet.mass() + units.centralBodyMass();
    real const distance   = planet.position().norm();
    real const specificEnergy = planet.velocity().norm2()/2 - coupleMass/distance;
    real const omega = std::sqrt(coupleMass/std::pow(std::abs(coupleMass/(2*specificEnergy)),3));
    return omega;
  }

  real
  theoreticalHillRadius(CodeUnits const& units, Planet const& planet) {
    return planet.position().norm() * std::cbrt(planet.mass()/(3*units.centralBodyMass()));
  }
  
  std::optional<real>
  Planet::userHillRadius() const {
    if (std::isnan(myHillRadius) || myHillRadius < 0) {
      return std::nullopt;
    } else {
      return myHillRadius;
    }
  }

  real
  HillRadius(CodeUnits const& units, Planet const& planet) {
    std::optional<real> uhr = planet.userHillRadius();
    return bool(uhr) ? *uhr : theoreticalHillRadius(units, planet);
  }
  
  void
  Planet::setHillRadius(real r) {
    assert(!std::isnan(r));
    myHillRadius = r;
  }

  std::optional<real> 
  Planet::radius() const {
    if (std::isnan(myRadius) || myRadius < 0) {
      return std::nullopt;
    } else {
      return myRadius;
    }
  }
  
  void
  Planet::setRadius(real r) {
    assert(!std::isnan(r));
    myRadius = r;
  }

  std::optional<Triplet>
  PlanetarySystem::guidingPlanetPosition() const {
    if (physic().referential.type == DiskReferential::COROTATING) {
      Planet p = planet(*physic().referential.guidingPlanet);
      if (*physic().referential.guidingCenter) {
        Triplet gpos = p.position();
        real m  = p.mass() + physic().units.centralBodyMass();
        Triplet vel = p.velocity();
        real d    = p.position().norm();
        real ener = 0.5*(p.velocity().norm2()) - m/d;
        real a    = -0.5*m/ener;
        real tmp  =  1 - cross(gpos,vel).norm2()/(m*a);
        if(tmp >= 0){
          real e   = sqrt(tmp);
          if (e >= 1e-8 ) {
            real arg = (1.0-d/a)/e;
            real E = (fabs(arg) >= 1.0
                      ? PI*(1-arg/fabs(arg))/2
                      : acos((1.0-d/a)/e));
            real vdotr = (gpos*vel).sum();
            if (vdotr < 0) {
              E = -E;
            }
            real M  = E-e*sin(E);
            real cw = (cos(E) -e)/(1.0 - e*cos(E));
            real sw = sqrt(1.0 - e*e)*sin(E)/(1.0 - e*cos(E));
            real perihelionPA = atan2(sw,cw);
            gpos.x = a*cos(M+perihelionPA);
            gpos.y = a*sin(M+perihelionPA);
            return gpos;
          } 
        }
      }
      return p.position();
    } else {
      return std::nullopt;
    }
  }

  void
  PlanetarySystem::applyMassTaper(real physicalTime) {
    typedef PlanetarySystemPhysic::Planet Conf;
    real const centralBodyMass = physic().units.centralBodyMass();
    for (auto& [name, planet] : myPlanets) {
      Conf const& conf = physic().planetarySystem()->planet(name);
      if (std::holds_alternative<Conf::MassTaper>(conf.mass)) {
        Conf::MassTaper const& taper = std::get<Conf::MassTaper>(conf.mass);
        real const massBefore =  planet.mass();
        planet.setMass(taper.massAt(physicalTime));
        real const massAfter = planet.mass();

        Triplet const pos = planet.position();
        Triplet const vel = planet.velocity();       
        real const angleth = atan(pos.y/pos.x); 
        real const sint = sin(angleth);
        real const cost = cos(angleth);
        Triplet const velPolar = Triplet(vel.x*cost+vel.y*sint,
					 -vel.x*sint+vel.y*cost,
					 vel.z);
	// real vthetaBefore    = -vel.x*sint+vel.y*cost;
	real const vthetaAfter  = velPolar.y*sqrt((centralBodyMass+massAfter)/(centralBodyMass+massBefore));
	planet.setVelocity(Triplet(velPolar.x*cost-vthetaAfter*sint,
                                   velPolar.x*sint+vthetaAfter*cost,
                                   vel.z));

      }
    }
  }
  
  real
  PlanetarySystem::frequency (Triplet prev, Triplet next, real dt) {
    using std::sqrt;
    real d2 = sqrt(next.norm2());
    real d1 = sqrt(prev.norm2());
    real cross = sqrt(Triplet(prev.x*next.y-next.x*prev.y, prev.y*next.z-prev.z*next.y, prev.z*next.x-prev.x*next.z).norm2());
    return asin(cross/(d1*d2))/dt;
  }

  shptr<PlanetarySystem>
  PlanetarySystem::load(ptree const& cfg, DiskPhysic const& physic, std::ostream& log) {
    typedef std::optional<pt::ptree> optree;
    optree sysCfg;
    auto planetCfg = cfg.get_child_optional(PLANETARY_SYSTEM/PLANETS);
    if (bool(planetCfg)) {
      sysCfg = *planetCfg;
      return PlanetarySystem::make(physic);
    } else {
      log << "Not planetary system description inside property file.\n";
      return nullptr;
    }
  }

  void
  PlanetarySystem::writeH5(HighFive::Group& grp) const {
    auto planetsGrp = grp.createGroup("planets");
    for (auto const& [name, planet] : myPlanets) {
      auto planetGrp = planetsGrp.createGroup(name);
      planet.writeH5(planetGrp);
    }
  }

  bool
  PlanetarySystem::readH5(HighFive::Group const& grp) {
    auto planetsGrp = grp.getGroup("planets");
    myPlanets.clear();
    for (std::string name : planetsGrp.listObjectNames()) {
      Planet planet;
      planet.readH5(planetsGrp.getGroup(name));
      myPlanets[name] = planet;
    }
    return true;
  }
  
  void
  swap(PlanetarySystem& s1, PlanetarySystem& s2) {
    std::swap(s1.myPhysic, s2.myPhysic);
    std::swap(s1.myLostMass, s2.myLostMass);
    std::swap(s1.myLostMassZ, s2.myLostMassZ);
    std::swap(s1.myLostOnAccretion, s2.myLostOnAccretion);
    std::swap(s1.myPrimaryAcceleration, s2.myPrimaryAcceleration);
    std::swap(s1.myPlanets, s2.myPlanets);
  }

  void
  PlanetarySystem::writeH5Log(HighFive::Group root, std::string name, PlanetLog const& log) {
    return h5::writeLog(root, name, log);
  }
  
  void
  PlanetarySystem::writeH5Logs(HighFive::Group root, SystemLog const& logs) {
    return h5::writeLogs(root, logs);
  }
  
  PlanetarySystem::PlanetLog
  PlanetarySystem::readH5Log(HighFive::Group root, std::string name) {
    return h5::readLog<TimeStamped<Planet>>(root, name);
  }

  PlanetarySystem::SystemLog
  PlanetarySystem::readH5Logs(HighFive::Group root) {
    return h5::readLogs<TimeStamped<Planet>>(root);
  }

  void
  PlanetarySystem::log(real time, SystemLog& log) const {
    for(auto const& [n,p] : myPlanets) {
      log[n].push_back({time, p});
    }
  }
  
  namespace detail {
    real rdiff(real r1, real r2) {
      using std::abs;
      real d;
      if (r1 == r2) {
        d = real(0);
      } else {
        d = abs(r1 - r2)/(abs(r1)+abs(r2))/2;
        if (!std::isnormal(d)) {
          std::cerr << "rdiff(" << r1 << ',' << r2 << ")=" << d << '\n';
        }
      }
      return d;
    }
    using namespace detail;

    real adiffNaN(real r1, real r2) {
      using std::isnan;
      using std::abs;
      return abs(isnan(r1) != isnan(r2)
		 ? std::numeric_limits<real>::quiet_NaN()
		 : (isnan(r1) && isnan(r2)
		    ? real(0)
		    : abs(r1-r2)));
    };

    real rdiffNaN(real r1, real r2) {
      using std::isnan;
      using std::abs;
      return abs(isnan(r1) != isnan(r2)
		 ? std::numeric_limits<real>::quiet_NaN()
		 : (isnan(r1) && isnan(r2)
		    ? real(0)
		    : rdiff(r1,r2)));
    };

    Triplet rdiff3(Triplet const& t1, Triplet const& t2) {
      return Triplet(rdiff(t1.x, t2.x),
		     rdiff(t1.y, t2.y), 
		     rdiff(t1.z, t2.z));
    };
  }

  Planet
  Planet::abs() const {
    using std::abs;
    return Planet(abs(myMass),
		  abs(myPosition), abs(myVelocity),
		  abs(myRadius), abs(myHillRadius));
  }


  Planet
  Planet::rdiff(Planet const& p) const {
    using namespace std;
    using namespace detail;
    
    return Planet(detail::rdiff(myMass, p.myMass),
		  rdiff3(myPosition, p.myPosition), rdiff3(myVelocity, p.myVelocity),
		  rdiffNaN(myRadius, p.myRadius), rdiffNaN(myHillRadius, p.myHillRadius));
  }
  
  Planet
  Planet::adiff(Planet const& p) const {
    using std::abs;
    using namespace detail;
    
    return Planet(abs(mass()-p.mass()),
		  abs(position()-p.position()), abs(velocity()-p.velocity()),
		  adiffNaN(myRadius, p.myRadius), adiffNaN(myHillRadius, p.myHillRadius));
  }

  TimeStamped<Planet>
  adiff(TimeStamped<Planet> const& t1, TimeStamped<Planet> const& t2) {
    using std::abs;
    using namespace detail;    
    return { abs(t1.time - t2.time), adiff(t1.state, t2.state) };
  }

  TimeStamped<Planet>
  rdiff(TimeStamped<Planet> const& t1, TimeStamped<Planet> const& t2) {
    using namespace std;
    using namespace detail;
    return { detail::rdiff(t1.time, t2.time), rdiff(t1.state, t2.state) };
  }
}

template<>
HighFive::DataType
HighFive::create_datatype<fargOCA::Planet>() {
  using namespace HighFive;
  using namespace fargOCA;
  typedef fargOCA::real real;
  static CompoundType tp({{"position",    create_datatype<Triplet>()},
			  {"velocity",    create_datatype<Triplet>()},
			  {"mass",        create_datatype<real>()}, 
			  {"radius",      create_datatype<real>()}, 
			  {"Hill_radius", create_datatype<real>()}});
  return tp;
}
  

template<>
HighFive::DataType
HighFive::create_datatype<fargOCA::TimeStamped<fargOCA::Planet>>() {
  return fargOCA::create_datatype_TimeStamped<fargOCA::Planet>();
}
