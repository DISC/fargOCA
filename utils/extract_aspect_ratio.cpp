// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include <boost/program_options.hpp>
#include <boost/format.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "loops.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "hillForce.hpp"

using namespace fargOCA;

namespace po  = boost::program_options;
namespace fs  = std::filesystem;
namespace kk  = Kokkos;
namespace kkm = Kokkos::Experimental;
namespace HF  = HighFive;

#define LBD KOKKOS_LAMBDA

GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

using std::optional;

template<GridSpacing GS>
void
write_aspect_ratio_impl(Disk& gas, std::ostream& out)
{
  GridDispatch const& dispatch = gas.dispatch();
  auto const& coords = dispatch.grid().as<GS>();
  GridSizes const ls = coords.sizes();
  GridSizes const gs = coords.global().sizes();

  range_t const managedRadii = dispatch.managedRadii();
  auto radMed = coords.radiiMed();
  
  arr1d<real> aspratio("aspaect_ratio", ls.nr);
  int hm = ( gas.physic().shape.half
             ? ls.ni - 1
             : ( ls.ni %2 == 0 
                 ? ls.ni/2 : (ls.ni-1)/2));
  // Here we compute the aspectratio at j=ls.ns/2 for a planet at azimuth 0
  // in the new version the grid starts at -pi
  arr3d<real const> cs = gas.soundSpeed().data();
  kk::parallel_for(kk::RangePolicy<>(begin(managedRadii), end(managedRadii)),
                   LBD(int i) {
                     real omega = sqrt(G/radMed(i));
                     aspratio(i) = cs(i,hm,ls.ns/2)/omega;
                   });
  arr1d<real> globalaspratio = dispatch.joinManaged(aspratio);
  
  arr1d<real> aspratiomean("aspratiomean", ls.nr);
  kk::parallel_for(kk::TeamPolicy<>(size(managedRadii), kk::AUTO),
                   LBD(kk::TeamPolicy<>::member_type const& team) {
                     int i = team.league_rank() + begin(managedRadii);
                     real csm = 0;
                     kk::parallel_reduce(kk::TeamVectorRange(team, ls.ns),
                                         [&](int j, real& partial) {
                                           partial += cs(i,hm,j);
                                         }, csm);
                     real omega = kk::sqrt(G/radMed(i));
                     csm /= ls.ns;
                     aspratiomean(i) = csm/omega;
                   });
  arr1d<real> globalaspratiomean = dispatch.joinManaged(aspratiomean);
  
  if (bool(gas.physic().adiabatic)) {
    real adiabaticIdx = gas.physic().adiabatic->index;
    kk::parallel_for(kk::RangePolicy<>(0, gs.nr),
                     LBD(int i) {
                       globalaspratio(i)     /= kk::sqrt(adiabaticIdx);
                       globalaspratiomean(i) /= kk::sqrt(adiabaticIdx);
                     });
  }
  if (dispatch.master()) {
    out << std::scientific;
    auto gRadMed  = radialMedCoords(dispatch.grid().global());
    auto hGlobalAspectRatio     = kk::create_mirror_view_and_copy(kk::HostSpace{}, globalaspratio);
    auto hMeanGlobalAspectRatio = kk::create_mirror_view_and_copy(kk::HostSpace{}, globalaspratiomean);
    for (size_t i = 0; i < gs.nr; ++i) {
      out << std::setprecision(8) << gRadMed(i) << '\t'
          << std::setprecision(16) << hGlobalAspectRatio(i) << '\t' 
          << hMeanGlobalAspectRatio(i) << '\n';
    }
  }
}

void
write_aspect_ratio(Disk& gas, std::ostream& out) {
  switch(gas.dispatch().grid().radialSpacing()) {
  case ARTH: write_aspect_ratio_impl<ARTH>(gas, out); break;
  case LOGR: write_aspect_ratio_impl<LOGR>(gas, out); break;
  }
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract aspect ratio.\n"
        << '\t' << cmd << " --disk <idisk>.h5 : "
        << "print aspect ratio from disk.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("output,o", po::value<std::string>(), "Output file, print on stdout by default.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk" }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opts << '\n';
      std::exit(1);
    }
  }
  Environment env(argc, argv);
  fmpi::communicator world;
  
  std::string ifname = vm["disk"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadOnly);
  auto disk = Disk::make(world, ifile.getGroup("/"));
  
  std::ofstream ofile;
  std::ostream& out = (bool(vm.count("output")) 
                       ? (ofile.open(vm["output"].as<std::string>()), ofile) 
                       : std::cout);

  write_aspect_ratio(*disk, out);
  return 0;
}
