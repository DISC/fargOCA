#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import sys
import h5py as h5
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse
import math 

parser = argparse.ArgumentParser(description='''
Display all the fields of a gas disk midplane for 3D simulations
and vertically integrated quantities for 2D ones.

Example:
   {} ref/gas9.h5
'''.format(sys.argv[0]))
parser.add_argument('ifile', metavar='GAS.h5', type=str, nargs=1,
                    help='HDF5 gas description file.')
parser.add_argument('paths', metavar='PATH', type=str, nargs='*',
                    help='The explicit list of fields to display. Defaults: all.')
parser.add_argument('-g','--grid', dest='grid_type', choices=['polar'], help='choose polar for polar grid, default rectangular')
parser.add_argument('-M','--maxcb', type=float, dest='maxcb',
                    help='max value for the color bar, default max of the field is plotted')
parser.add_argument('-m','--mincb', type=float, dest='mincb',
                    help='min value for the color bar, default min of the field is plotted')
parser.add_argument('-p','--plot', dest='plot_type',choices=['contrast','orbit'],help='choose contrast to plot (f-<f>/<f>), <> is the average on azimuthal coordinate, orbit to superpose the planetary orbit')
parser.add_argument('-l','--level', dest='vert_level',type=int,
                   help='choose the level for horizontal slice (from 0, top  of the disk to Layers-1 (bottom of the disk or midplane for halfdisk) , default is midplane, (only for density default is surface density)') 
parser.add_argument('-s','--scale', dest='plot_scale',choices=['log'],default='lin',
                    help='choose log for logscale plot (density,energy,vortensity), default linear')
parser.add_argument('R0', nargs='?', type=float, default=5.2, 
                    help='Provide the unit of distance default 5.2(AU)')





args = parser.parse_args()
disk_filename = args.ifile[0]
disk = h5.File(disk_filename, 'r')


# FargOCA units
AU     = 1.496e13  # Astronomical Unit in [cm] 
GRAVC  = 6.67e-8    # Gravitation Constant in [cm3/g/s2]
XMSOL  = 1.989e33  # Solar Mass in [gr]
XMSTAR = 1.0       # Amount of Solar Masses
R0scale = args.R0
R0     = R0scale*AU    # Unit of distance in cm
YEAR   = (365.2524*24.*3600.) # one year in s
VOL0   = (R0*R0*R0) # Volume unit in cm**3
TIME0  = (math.sqrt(VOL0 / GRAVC / (XMSTAR*XMSOL)))  # time unit in s
V0     = (R0 / TIME0)  # Velocity in cm/s
XM0    = (XMSOL * XMSTAR)  # Mass unit in g
RHO0   = (XM0 / VOL0)  # Density unit in g/cm**3
YEAR   = (365.2524*24.*3600.) # one year in s
mdotSMyear =R0*R0*RHO0*V0*YEAR/XMSOL
codet2y=R0scale**1.5/2./math.pi


data_space_name = 'polar_grid'


def extract_field_paths(obj,data_space):
    """Extract all object paths storing a field from an hdf5 object.

    An object path is considered to store a field if it contains a
    'polar_grid' object which provide a 3D floating point array.
    2D object have a 2cnd dimention of size one.
    """
    
    fields = []
    def collect_fields(path):
        try:
            if data_space in obj[path]: fields.append(path)
        except TypeError:
            pass
    obj.visit(collect_fields)
    return fields

# Collect paths
paths = []

if len(args.paths) > 0:
    paths = args.paths
else:
    paths = extract_field_paths(disk,data_space_name)

if len(paths)==0:
    print("No field found in {}.".format(gas_filename))

# Display grid:
nb_col = 3
nb_line = (len(paths)+nb_col)/nb_col
if(len(paths) == 1):
    nb_col=1
    nb_line=1

def get_fields(disk, paths, data_space_name):
    fields = {}
    for p in paths:
        fields[p] = np.array(disk[p][data_space_name])
    return fields


def replaceZeroes(data):
  min_nonzero = np.min(data[np.nonzero(data)])
  data[data == 0] = min_nonzero
  return data

fields = get_fields(disk, paths, 'polar_grid')


# Assume all shapes are equal
shape = disk[paths[0]]['polar_grid'].shape
flat   = shape[1] == 1
xymesh = None

if flat:
     print("this is a flat grid, displaying as it is.")
     radii   = np.array(disk['grid']['radii'])
     sectors = np.array(disk['grid']['sectors'])
     xymesh = np.meshgrid(sectors, R0scale*radii)
     # drop layer dim
     for p,f in fields.items():
         fields[p]=f[:,0,:]
else:
     print("this is a fat grid displaying a slice")
     half = disk['physic/shape/gas'].attrs['half']
     radii   = np.array(disk['grid']['radii'])
     sectors  = np.array(disk['grid']['sectors'])
     layers = disk['grid/layers'][:]
     xymesh  = np.meshgrid(sectors, radii*R0scale)
# find midplane
     for p,f in fields.items():
         nr,nphi,nt=fields[p].shape
         midplane = int(nphi/2)
         print(p)
         if args.vert_level:
             level =  args.vert_level
             print("displaying vertical slice at level:",layers[level])
             fields[p] = f[:,level,:]
         else:
             if(half):  # half disk
                if "density" in p:
                   fields[p]=f.sum(axis=-2)
                   deltaphi = layers[1]-layers[0]
                   for i in range (0,nr-1): 
                        fields[p][i,:] *=deltaphi*2*radii[i]
                   flat = True
                else:   
                    fields[p] = f[:,nphi-1,:]
             else: # full disk
                if "density" in p:
                    fields[p]=f.sum(axis=-2)
                    deltaphi = layers[1]-layers[0]
                    for i in range (0,nr-1): 
                        fields[p][i,:] *=deltaphi*2*radii[i]
                    flat = True
                else:
                    fields[p] = f[:,midplane,:]
   

def plot(f, xymesh, pos, name):
    if(args.grid_type == 'polar'):
       ax =plt.subplot(nb_col, nb_line,pos,polar=True)
       ax.set_xticklabels(('0','$\pi/4$','$\pi/2$','$3\pi/4$','$-\pi$','$-3\pi/4$','$-\pi/2$','$-\pi/4$'),fontsize='small')
       ax.tick_params(axis='y', colors='yellow')
    else:
       plt.subplot(nb_col, nb_line,pos)
       plt.ylabel('r (AU) ',size=8)
       plt.xlabel('$\\theta$',size=8)
      
    physical_time = disk['/'].attrs['physical_time']
    namefig='time='+str(int(physical_time*codet2y))+'y'
    plt.title(namefig,position=(0,1.2),fontsize=10)
    plt.subplots_adjust(hspace=0.8,wspace=0.5,left=0.1, right=0.9)
    x,y = xymesh
   
# Max and min values for plot

    if "velocity" in name:
        f = f*V0/100.
    elif "density" in name:
        if(args.plot_scale == 'log'):  # log scale 
          if(flat):
             f=np.log10(f*RHO0*R0) # surface density for 2D disks
             print('Plot log(density)')
          else:
             f=np.log10(f*RHO0)  # volume density for 3D disks
        else:
            if(flat):
              f=f*RHO0*R0 # surface density for 2D disks
            else:
              f=f*RHO0
    elif "energy" in name:
        if(args.plot_scale == 'log'): # log scale 
          f=np.log10(f*XM0/math.pow(TIME0,3)/R0) 
        else:
          f=f*XM0/math.pow(TIME0,3)/R0
    elif "vortensity" in name:
        if(args.plot_scale == 'log'): # log scale 
          f = replaceZeroes(f)
          f=np.where(abs(f)> 1.e-5,np.log10(abs(f)),-5)
        else:
          print("Consider f")
          f=f
    elif "mdotmap" in name: 
        f=f*mdotSMyear

# Prepare f-<f>/<f> to be plotted
 
    if(args.plot_type == 'contrast'):
       nr,nphi,nt=disk[name+'/polar_grid'].shape
       average = np.zeros(shape=(nr))
       tmp = np.zeros(shape=(nr,nt))
       average= f.mean(axis=-1)
       print(average.max(),average.min())
       
       for i in range(0,nr):
         for j in range (0,nt):
           f[i,j] =(f[i,j]-average[i])
           if("velocity/radial" in name):
             eps = abs(average).min()+0.01
             f[i,j] =(f[i,j]-average[i])
             if(average[i] > eps):
               f[i,j] /= average[i]
             else:
               f[i,j] = eps
           else:
             f[i,j] /= average[i]
#    if args.maxcb  and (args.maxcb <f.max()):
    if args.maxcb:
       vmax = args.maxcb
    else:
       vmax = f.max()
#    if args.mincb and (args.mincb >f.min()):
    if args.mincb:
       vmin = args.mincb
    else:
       vmin = f.min()
    print('Mincb:',args.mincb)
    print(name,'max:',vmax,'min:',vmin)
    
# Plot on rectangular or polar grid     
       
    plt.pcolormesh(x,y,f,cmap= 'inferno', vmin= vmin , vmax=vmax)
    cbar=plt.colorbar(fraction=.15,pad=0.15,shrink=0.7,orientation='vertical')

# Label suited to path

    if "velocity" in name:  
      cbar.set_label('$v(m/s)$',size=7)
    elif "density" in name:
      if(flat):
        if(args.plot_scale == 'log'):
          cbar.set_label('$\log(\\Sigma),g/cm^2$',size=7)
        
        elif(args.plot_type == 'contrast'):
          cbar.set_label('$(\\Sigma-\\bar \\Sigma)/\\bar \\Sigma$',size=7)
        else:
          cbar.set_label('$\\Sigma,g/cm^2$',size=7)
      else:
        if(args.plot_scale == 'log'):
          cbar.set_label('$\log(\\rho),g/cm^3$',size=7)
        elif(args.plot_type == 'contrast'):
          cbar.set_label('$(\\Sigma-\\bar \\Sigma)/\\bar \\Sigma$',size=7)
        else:
          cbar.set_label('$\\rho,g/cm^3$',size=7)
    elif "energy" in name:
        if(args.plot_scale == 'log'):
          cbar.set_label('$log(E), erg/s$',size=7)
        else:
           cbar.set_label('E, erg/s$',size=7)
    elif "mdotmap" in name: 
        cbar.set_label('$M_{\odot}$/y',size=7)
    else: 
        cbar.set_label('Code units',size=7)
        
    #plt.axis([y.min(), y.max(), x.min(), x.max()])

    cbar.ax.tick_params(labelsize=6)

    if(args.plot_type == 'orbit'):
          planet=disk['/planetary_system/planets/BigEart']['state']
          x,y,z=planet['position']
          erre = R0scale*(x**2+y**2)**0.5   #Valid only for planet at z=0
          print(erre)
          from orbit_module import orbit
          Orbit = orbit(erre)
          plt.plot(Orbit[0],Orbit[1],'w--',linewidth=1.5)
    
    

for i,(p,f) in enumerate(fields.items()):
    plot(f, xymesh, i+1, p)

plt.show()
