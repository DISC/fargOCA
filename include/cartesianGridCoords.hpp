// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_CARTESIAN_GRID_COORDS_H
#define FARGOCA_CARTESIAN_GRID_COORDS_H

#include <memory>
#include <tuple>

#include "precision.hpp"
#include "arrays.hpp"
#include "tuple.hpp"

namespace fargOCA {
  class Grid;
  
  /// \brief Provide the cartesian coordinates of grid points
  class CartesianGridCoords {
  public:
    /// \brief For empty grid, does not support any action
    CartesianGridCoords();
    CartesianGridCoords(Grid const& coords);

    CartesianGridCoords(CartesianGridCoords const& c) = default;
    ~CartesianGridCoords() = default;

    arr3d<real const> const x;
    arr3d<real const> const y;
    arr3d<real const> const z;
    
    /// \brief coordinates of the ihj-th cell.
    KOKKOS_INLINE_FUNCTION
    Triplet coords(int i, int h, int j) const { return Triplet(x(i,h,j),y(i,h,j),z(i,h,j)); }
  private:
    CartesianGridCoords(std::tuple<arr3d<real const>, arr3d<real const>, arr3d<real const>> coords);
  };
}

#endif
