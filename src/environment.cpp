// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "fargoConfig.hpp"

#include <sysexits.h>

#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/algorithm/string.hpp>

#include <Kokkos_Core.hpp>

#include "mpiGpuAware.hpp"
#include "environment.hpp"
#include "fargoConfig.hpp"

#ifdef ENABLE_PETSC
#include "petscsys.h"   
#include "petscerror.h"   
#endif

namespace po = boost::program_options;

namespace fargOCA {
  namespace {
    std::vector<std::string>
    cstr2vec(int argc, char** argv) {
      std::vector<std::string> strs;
      for( char** sptr = argv; sptr < argv+argc; ++sptr) {
        strs.push_back(std::string(*sptr));
      }
      return strs;
    }
  }
  
  namespace {
    po::options_description const&
    makeCmdLineOptions() {
      static po::options_description options;
      options.add_options()
        ("init", po::value<std::string>()->default_value("mpi:kokkos"),
         "Specify the component initialization order as a comma separated list of components. Possible components are 'mpi' and 'kokkos'.");
      return options;
    }
  }
  
  Environment* Environment::ourInstance;  

  po::options_description const&
  Environment::cmdLineOptions() {
    static po::options_description const& options = makeCmdLineOptions();
    return options;
  }

  void
  Environment::parseArgs(std::vector<std::string> args) {
    std::vector<std::string> components;
    std::vector<char*> ptrs;
    if (!args.empty()) {
      for(std::string const& s : args) {
        assert(!s.empty());
        ptrs.push_back(const_cast<char*>(s.c_str()));
      }
      int argc = args.size();
      po::parsed_options parsed = po::command_line_parser(argc, ptrs.data()).options(cmdLineOptions()).allow_unregistered().run();
      po::variables_map vm;
      po::store(parsed, vm);
      po::notify(vm);
      boost::split(components, vm["init"].as<std::string>(), boost::is_any_of(":"));
      if (components.size() > 2) {
        std::cerr << "No more than 2 component in --init, got '" << vm["init"].as<std::string>() << "'\n";
        std::exit(EX_USAGE);
      }
    }
    int    argc = ptrs.size();
    ptrs.push_back(nullptr); // There is a bug in open MPI mandating argv[argc] == NULL
    char** argv = ptrs.empty() ? nullptr : ptrs.data();

    for(std::string c : { "mpi", "kokkos"}) {
      if (std::find(components.begin(), components.end(), c) == components.end()) {
        components.push_back(c);
      }
    }
    assert(components.size() == 2);
    for(std::string c : components) {
      if (c == "mpi") {
        if (!myMpiEnv) {
          myMpiEnv = std::make_unique<fmpi::environment>(argc, argv, fmpi::threading::funneled);
          myWorld  = std::make_unique<fmpi::communicator>();
        }
      } else if (c == "kokkos") {
        Kokkos::initialize(argc, ptrs.data());
      } else {
        std::cerr << "Invalid component name '" << c << "'\n";
        std::exit(EX_USAGE);
      }
    }
#ifdef ENABLE_PETSC
#ifndef   FARGO_SEQ
    PETSC_COMM_WORLD = MPI_Comm(*myWorld);
#endif
    PetscCallAbort(*myWorld, PetscInitialize(&argc, &argv, nullptr, "FargOCA Petsc solver"));
#endif
  }
  
  Environment::Environment(std::vector<std::string> args) 
    : myMpiEnv(nullptr),
      myWorld(nullptr) {
    parseArgs(args);
    assert(!ourInstance);
    ourInstance = this;
  }
  
  Environment::Environment() : Environment(std::vector<std::string>()) {}
  
  Environment::Environment(int argc, char** argv) : Environment(cstr2vec(argc, argv)) {}

  Environment::~Environment() {
    Kokkos::finalize();
#ifdef ENABLE_PETSC
    PetscCallAbort(*myWorld, PetscFinalize());
#endif
    ourInstance = nullptr;
  }
  
  fmpi::communicator const& 
  Environment::world()  {
    assert(ourInstance);
    return *(ourInstance->myWorld);
  }

  void
  Environment::printConfig(std::ostream& log) {
    assert(ourInstance);
    std::ostringstream msg;
    log << "Kokkos configuration" << std::endl;
    if (Kokkos::hwloc::available()) {
      msg << "hwloc( NUMA[" << Kokkos::hwloc::get_available_numa_count()
          << "] x CORE[" << Kokkos::hwloc::get_available_cores_per_numa()
          << "] x HT[" << Kokkos::hwloc::get_available_threads_per_core()
          << "] )" << std::endl;
    }
    Kokkos::print_configuration(msg);
    log << msg.str();
    log << "GPU direct support: ";
    if (hasGpuDirect()) {
      log << "available.\n";
    } else {
      log << "unavailable.\n";
    }
  }
  
  namespace {
    namespace GpuDirect {
#if defined(MPI_GPU_AWARE_CRAYMPICH_API_SUPPORT)
      inline
      bool
      requested() {
	const char* enabled = std::getenv("MPICH_GPU_SUPPORT_ENABLED");
	return (enabled != nullptr && enabled[0] == '1');
      }
      
      /// Rely on MPIX_GPU_query_support. It requires that we have
      /// MPIX_GPU_SUPPORT_CUDA defined to plug into the MPIX_GPU_query_support.
      /// https://www.mpich.org/static/docs/v4.0.x/www3/MPIX_GPU_query_support.html
      ///
      inline
      bool supported() {
	bool yep = false;
	
	const auto askMPIx = [](int gpu_kind) -> int {
			       int supported;
			       if (::MPIX_GPU_query_support(gpu_kind, &supported) == MPI_SUCCESS) {
				 return bool(supported);
			       } else {
				 return false;
			       }
			     };
	
#if defined(MPIX_GPU_SUPPORT_CUDA)
	if (askMPIx(MPIX_GPU_SUPPORT_CUDA)) { yep = true; }
#endif
	
#if defined(MPIX_GPU_SUPPORT_ZE)
	if (askMPIx(MPIX_GPU_SUPPORT_ZE)) { yep = true; }
#endif
	
#if defined(MPIX_GPU_SUPPORT_HIP)
	if (askMPIx(MPIX_GPU_SUPPORT_HIP)) { yep = true; }
#endif
	return yep;
      }
      
#elif defined(MPI_GPU_AWARE_OPENMPI_API_SUPPORT)
      inline
      bool
      requested() {
	const char* enabled = std::getenv("MPICH_GPU_SUPPORT_ENABLED");
	return (enabled != nullptr && enabled[0] == '1');
      }

      inline bool
      supported() {
	bool yep = false;
	
#if defined(MPI_GPU_AWARE_OPENMPI_API_SUPPORT) && defined(OMPI_HAVE_MPI_EXT_CUDA)
	if (::MPIX_Query_cuda_support() == 1) { yep = true; }
#endif
	
	// #if defined(MPI_GPU_AWARE_OPENMPI_API_SUPPORT) && defined(OMPI_HAVE_MPI_EXT_ZERO)
	//         if(::MPIX_Query_zero_support() != 1) {
	//             return -1;
	//         }
	//         result |= 1 << 2;
	// #endif
	
#if defined(MPI_GPU_AWARE_OPENMPI_API_SUPPORT) && defined(OMPI_HAVE_MPI_EXT_ROCM)
	if (::MPIX_Query_rocm_support() == 1) { yep = true; }
#endif
	
	return yep;
      }
#elif !defined(MPI_GPU_AWARE_API_SUPPORT)
      inline bool requested() { return false; }
      inline bool supported() { return false; }
#endif
      inline
      bool
      available() {
	return (fmpi::environment::initialized()
		&& requested()
		&& supported());
      }
    }
  }
  bool
  Environment::hasGpuDirect() {
    static const bool yep = GpuDirect::available();
    return yep;
  }
}
