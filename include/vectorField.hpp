// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_VECTOR_FIELD_H
#define FARGOCA_VECTOR_FIELD_H

#include <highfive/H5File.hpp>

#include "scalarField.hpp"
#include "diskPhysic.hpp"

namespace fargOCA {
  class VectorField {
  public:
    VectorField(GridDispatch const& grid);
    VectorField(VectorField const&) = default;
    ~VectorField();
    
    GridDispatch const& dispatch() const { return myRadial.dispatch(); }
    
    ScalarField&       radial()       { return myRadial; }
    ScalarField const& radial() const { return myRadial; }
    ScalarField&       theta()        { return myTheta; }
    ScalarField const& theta() const  { return myTheta; }
    ScalarField&       phi()          { return myPhi; }
    ScalarField const& phi() const    { return myPhi; }
    
    /// \brief Rebind to a new grid.
    void rebind(GridDispatch const& nextGrid, ScalarFieldInterpolators const& interpolators);
    /// \brief Rebind to a new grid with default interpolation
    void rebind(GridDispatch const& nextGrid);

    vec3d<real>       data()       { return { radial().data(), phi().data(), theta().data() }; }
    vec3d<real const> data() const { return { radial().data(), phi().data(), theta().data() }; }

    /// \brief Write field into group.
    /// \param group the optional group to write into, must be set at root only.
    /// \param root the MPI proc on which to write.
    void writeH5(std::optional<HighFive::Group> group, int root) const;
    ///\brief Read field from group.
    /// Global operation
    void readH5(HighFive::Group const& group);

  private:
    ScalarField myRadial;
    ScalarField myTheta;
    ScalarField myPhi;
  };

  inline
  void 
  swap(VectorField& v1, VectorField& v2) {
    swap(v1.radial(), v2.radial());
    swap(v1.theta(),  v2.theta());
    swap(v1.phi(),    v2.phi());
  }
}

#endif
