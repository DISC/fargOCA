// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <nlohmann/json.hpp>

#include "gasShape.hpp"
#include "io.hpp"
#include "simulationConfig.hpp"
#include "h5io.hpp"
#include "configProperties.hpp"

namespace fargOCA {
  using json = nlohmann::json;
  
  void
  GasShape::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "half", half);
    h5::writeAttribute(grp, "aspect_ratio",   aspectRatio);
    h5::writeAttribute(grp, "radius_maximum", radius.max);
    h5::writeAttribute(grp, "radius_minimum", radius.min);
    h5::writeAttribute(grp, "sector",        sector);
    h5::writeAttribute(grp, "opening",       opening);
  }
  void
  GasShape::readH5(HighFive::Group const& grp)  {
    h5::readAttribute(grp, "half", half);
    h5::readAttribute(grp, "aspect_ratio",   aspectRatio);
    h5::readAttribute(grp, "radius_maximum", radius.max);
    h5::readAttribute(grp, "radius_minimum", radius.min);
    h5::readAttribute(grp, "opening",       opening);
  }

  using namespace configProperties;
  
  void
  to_json(nlohmann::json& j, GasShape const& gs) {
    j[snake(ASPECT_RATIO)] = gs.aspectRatio;
    j[snake(RADIUS)] = {{snake(MIN), gs.radius.min},
		      {snake(MAX), gs.radius.max}};
    j[snake(SECTOR)] = gs.sector;
    j[snake(OPENING)] = gs.opening;
    j[snake(HALF)] = gs.half;
  }
  
  void
  from_json(nlohmann::json const& j, GasShape& gs) {
    gs = GasShape(j.at(snake(ASPECT_RATIO)).get<real>(),
		  j.at(snake(RADIUS)).at(snake(MIN)).get<real>(), j.at(snake(RADIUS)).at(snake(MAX)).get<real>(),
		  j.at(snake(OPENING)).get<real>(),
		  j.at(snake(HALF)).get<bool>());
  }

  GasShape::GasShape(nlohmann::json const& j) {
    j.get_to(*this);
  }
  
  void
  GasShape::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Shape:\n"
        << tabs(tab+1) << "* aspectRatio:" << aspectRatio << '\n'
        << tabs(tab+1) << "* radius, min:" << radius.min << ", max:" << radius.max << '\n'
        << tabs(tab+1) << "* sector:" << sector << '\n'
        << tabs(tab+1) << "* opening:" << opening << '\n'
        << tabs(tab+1) << "* half:" << yesno(half) << '\n';
  }
  std::ostream&
  operator<<(std::ostream& out, GasShape const& s) {
    out << "{ratio:" << s.aspectRatio 
        << ",radii:{" << s.radius.min << "," << s.radius.max 
        << "},sec:" << s.sector 
        << ",open:" << s.opening
        << ",half:" << yesno(s.half) << "}";
    return out;
  }

  GasShape
  GasShape::diff(GasShape const& s1, GasShape const& s2) {
    return GasShape(std::abs(s1.aspectRatio-s2.aspectRatio),
		    std::abs(s1.radius.min - s2.radius.min),
		    std::abs(s1.radius.max - s2.radius.max),
                    std::abs(s1.opening - s2.opening),
                    s1.half == s2.half);
  }

  GasShape
  diff(GasShape const& s1, GasShape const& s2) {
    return GasShape(s1.aspectRatio - s2.aspectRatio,
                    s1.radius.min - s2.radius.min,
                    s1.radius.max - s2.radius.max,
                    s1.opening - s2.opening,
                    s1.half - s2.half);
  }
  real
  max(GasShape const& s) {
    real m  = 0;
    for(real x : { s.aspectRatio, s.radius.min, s.radius.max, s.opening, real(s.half)}) {
      m = std::max(m, std::abs(x));
    }
    return m;
  }
}
