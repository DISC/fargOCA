// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_UTILS_UPGRADE_H
#define FARGOCA_UTILS_UPGRADE_H

#include <highfive/H5File.hpp>

#include "tuple.hpp"
#include "planetarySystem.hpp"
#include "timeStamped.hpp"

namespace fargOCA {
  struct PlanetV0 {
    Triplet  myPosition;
    Triplet  myVelocity;
    real     myMass;
  };
  
  struct PlanetV1 {
    PlanetV1(PlanetV1 const& v1) = default;
    PlanetV1(PlanetV1&& v1) = default;
    PlanetV1() = default;
    PlanetV1(PlanetV0 const& v0) 
      : myPosition(v0.myPosition),
        myVelocity(v0.myVelocity),
        myMass(v0.myMass),
        myRadius(std::numeric_limits<real>::quiet_NaN()),
        myHillRadius(std::numeric_limits<real>::quiet_NaN()) {}
    
    PlanetV1& operator=(PlanetV1 const& v1) = default;
    
    Triplet  myPosition;
    Triplet  myVelocity;
    real     myMass;
    real     myRadius     = std::numeric_limits<real>::quiet_NaN();
    real     myHillRadius = std::numeric_limits<real>::quiet_NaN();
    
  };
}

template <> HighFive::DataType HighFive::create_datatype<fargOCA::PlanetV0>();
template <> HighFive::DataType HighFive::create_datatype<fargOCA::PlanetV1>();
template <> HighFive::DataType HighFive::create_datatype<fargOCA::TimeStamped<fargOCA::PlanetV0>>();
template <> HighFive::DataType HighFive::create_datatype<fargOCA::TimeStamped<fargOCA::PlanetV1>>();

namespace fargOCA {
  enum class DiskReferentialV0 : int
    {
     CONSTANT = 0,       //< user specified constant
     CIRCULAR_3BODY = 1, //< DO NOT USE
     COROTATING = 2     //< match the speed of the guiding planet
    };
  
  template<> std::vector<DiskReferentialV0> const& enumValues<DiskReferentialV0>();  
  std::ostream& operator<<(std::ostream& out, DiskReferentialV0 t);
  std::istream& operator>>(std::istream& in, DiskReferentialV0& t);
  
}

template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::DiskReferentialV0>() { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::DiskReferentialV0>(); }

namespace fargOCA {
  enum class ViscosityType {
    CONSTANT = 0, //< Gas viscosity is constant on all disk
    ALPHA    = 1  //< Gas viscosity is modeled with Shakura & Sunyaev 1973
  };

  template<> std::vector<ViscosityType> const& enumValues<ViscosityType>();
  std::ostream&  operator<<(std::ostream& out, ViscosityType v);
  std::istream&  operator>>(std::istream& in, ViscosityType& v);
}

template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::ViscosityType>()       { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::ViscosityType>(); }

#endif
