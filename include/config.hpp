// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_CONFIG_H
#define FARGOCA_CONFIG_H

#include <string>
#include <list>
#include <set>
#include <optional>

#include <highfive/H5File.hpp>
#include <nlohmann/json_fwd.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>

namespace fargOCA {

  class Config final {
  public:
    using Properties = std::map<std::string, std::string>;
    
    Config(Properties const& m) : myProperties(m) {}
    Config(boost::property_tree::ptree const& cfgMap);
    Config(HighFive::Group group);
    Config() = default;
    Config(Config&& ) = default;
    Config(Config const& ) = default;
    Config& operator=( Config const& ) = default;
    Config& operator=( Config&& ) = default;
    ~Config() = default;

    /// \brief Retrieve an optional parameter value interpreted as the specified type.
    /// The returned value exist only if the parameter was specified in the config file.
    /// If the parameter was specified, but could not be interpreted into the requested
    /// type, a boost::bad_lexical_cast exception is raised.
    /// \param name the parameter name
    /// \returns an optional value if appropriate.
    template<typename T> std::optional<T> value(std::string name) const;
    
    /// \brief Retrieve an parameter value interpreted as the specified type.
    /// If no such parameter was specified, a default value is returned.
    /// If the parameter was specified, but could not be interpreted into the requested
    /// type, a boost::bad_lexical_cast exception is raised.
    /// \param name the parameter name
    /// \param def the default value to be returned if needed.
    /// \returns an optional value if appropriate.    
    template<typename T> T value(std::string name, T const& def) const;
    std::string value(std::string name, char const* def) const { return value<std::string>(name, def); }

    /// \brief Same as (bool(value<bool>(name)) ? *value<bool>(name) : def)
    bool enabled(std::string name, bool def = false) const;

    /// \brief User registered properties
    Properties const& registered() const { return myProperties; } 

    /// \brief A sequence of named configuration.
    /// Usages like plugins sometime requires to be able to find a configuration by key,
    /// hence the name.
    /// But it can be important to apply them in a user defined order, hence the list
    using ConfigMap = std::list<std::pair<std::string, Config>>;

    void dump(std::ostream& out, int tab = 0) const;
    static void dump(ConfigMap const& configs, std::ostream& out, int tab = 0);

    void writeH5(HighFive::Group parent, std::string gname) const;

    static void writeH5(ConfigMap const& p, HighFive::Group& container);
    static void readH5(ConfigMap& p, HighFive::Group const& container);

  private:
    Properties myProperties;
  };

  /// \brief A prototype for a configuration checker.
  /// The function can report problems on a log and return the status of the check.
  typedef std::function<bool (Config const& config, std::ostream& log)> ConfigCheckFct;
  
  /// \brief Produce a basic enumerated key checkers.
  /// The returned function will control that the user specified properties are limited to the specified set.
  ConfigCheckFct propertyNameChecker(std::set<std::string> allowed);

  template<> std::optional<std::string> Config::value(std::string name) const;
  template<> std::optional<bool>        Config::value(std::string name) const;
  
  template<typename T> 
  std::optional<T>
  Config::value(std::string name) const {
    auto ov = value<std::string>(name);
    if (ov) {
      return boost::lexical_cast<T>(*ov);
    } else {
      return std::nullopt;
    }
  }

  template<typename T> T Config::value(std::string name, T const& def) const { return value<T>(name).value_or(def); }
  
  /// \brief Same properties with upper case names
  Config toUpper(Config const& properties);
  /// \brief Same properties with lower case names
  Config toLower(Config const& properties);

  /// \brief Check whether registered keys are in a given set.
  /// Reports on log if provided
  bool checkKeys(Config const& properties, std::set<std::string> const& allowed, std::ostream& out);

  void to_json  (nlohmann::json& j,       Config const& p);
  void from_json(nlohmann::json const& j, Config& p);

  using ConfigMap = Config::ConfigMap;
  void to_json  (nlohmann::json& j,       ConfigMap const& p);
  void from_json(nlohmann::json const& j, ConfigMap& p);
}

#endif

