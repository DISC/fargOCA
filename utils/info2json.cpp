// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>
#include <sysexits.h>

#include <boost/property_tree/info_parser.hpp>

#include <boost/program_options.hpp>
#include <nlohmann/json.hpp>

#include "allmpi.hpp"
#include "simulationConfig.hpp"
#include "environment.hpp"

namespace po  = boost::program_options;
namespace pt  = boost::property_tree;
namespace fs  = std::filesystem;

bool
read_cmd_line(int argc, char *argv[], po::variables_map& vm) {
  std::ostringstream usage;
  usage << argv[0] << " <iconfig>.info <oconfig>.json [otions]+\n"
        << "Convert <iconfig>.info into <oconfig>.json\n."
        << "COnvert a boost properties info config file to json.\n"
        << "Supported options";
  po::options_description opts(usage.str());
  opts.add_options()
    ("help", "produce this message.")
    ("iconfig", po::value<std::string>(), "The info configuration file.")
    ("oconfig", po::value<std::string>(), "The json configiration file.")
    ;
  
  po::positional_options_description input_option;
  input_option.add("iconfig", 1);
  input_option.add("oconfig", 1);
  po::store(po::command_line_parser(argc, argv).
            options(opts).positional(input_option).run(), vm);
  po::notify(vm);
  if (bool(vm.count("help"))) {
    std::cout << opts;
    return false;
  }
  if (vm.count("iconfig") == 0 || vm.count("oconfig") == 0) {
    std::cerr << "Missing parameter. Usage: \n";
    std::cout << opts;
    return false;
  }
  return true;
}

using namespace fargOCA;

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  po::variables_map vm;

  if (!read_cmd_line(argc, argv, vm)) {
    return 1;
  }
  std::string iconfig_fname = vm["iconfig"].as<std::string>();
  std::string oconfig_fname = vm["oconfig"].as<std::string>();
  if (!fs::exists(iconfig_fname)) {
    std::cerr << "'" << iconfig_fname << "' not found, bye.\n";
    return EX_NOINPUT;
  }

  auto j = loadJsonFromPTree(env.world(), iconfig_fname);
  std::ofstream out(oconfig_fname);
  out << std::setw(4) <<  j;

  return fs::exists(oconfig_fname) ? EXIT_SUCCESS : EX_SOFTWARE;
}
