// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <cassert>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <regex>
#include <filesystem>
#include <sysexits.h>

#include <Kokkos_Core.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"

#include "debug.hpp"
#include "io.hpp"
#include "environment.hpp"
#include "log.hpp"
#include "version.hpp"
#include "simulation.hpp"
#include "disk.hpp"
#include "options.hpp"
#include "codeUnits.hpp"
#include "h5Labels.hpp"

using namespace fargOCA;

namespace fs      = std::filesystem;
namespace po      = boost::program_options;
namespace HF      = HighFive;
namespace opt     = fargOCA::options;
using namespace opt;

namespace {
  bool               readCmdLine(int argc, char *argv[], std::ostream& log, po::variables_map& vm);
  std::optional<int> startIndex(std::string dirname, std::ostream& log);
  HF::File           diskFile(std::string diskDir, int index);
  int                maxStep(Simulation const& simu, po::variables_map vm);
}

int
main(int argc,  char *argv[])
{
  Environment env(argc, argv);
  fmpi::communicator const& world = env.world();
  
  std::ostream& log = fargOCA::log(world);
  po::variables_map vm;
  if (readCmdLine(argc, argv, log, vm)) {
    bool verbose = bool(vm.count(opt::VERBOSE));
    bool version = bool(vm.count(VERSION)) || verbose;
    if (version) {
      log << "Source code id: " << source_version() << '\n'
          << "Branch: " << source_branch() << '\n';
      if (!verbose) {
        return EX_USAGE;
      }
    }
    if (debug::enabled) {
      if (vm["gas-check"].as<std::string>() == "abort") {
        Disk::abortOnGasCheck(true);
      }
    }
    if (bool(vm.count("uninit-temp-fields-to-NaN"))) {
      FieldStack<void>::setUninitialized2NaN(true);
    }
    std::string dirname = vm["directory"].as<std::string>();
    fargOCA::debug::outputDir(dirname);
    std::optional<int> start = startIndex(dirname, log);
    if (bool(start)) {
      using namespace h5::labels;
      HF::File storage = diskFile(dirname, *start);
      auto simulation{Simulation::make(world, storage.getGroup("/"), dirname)};
      if (!simulation->disk().physic().check(log)) {
        exit(EX_CONFIG);
      }
      if (verbose) {
        env.printConfig(log);
        simulation->dump();
	world.barrier();
      }
      int endStep = maxStep(*simulation, vm);
      simulation->start();
      while( simulation->step() <= endStep) {
        simulation->doStep();
      }
    } else {
      return EX_NOINPUT;
    }
  } else {
    return EX_USAGE;
  }
}

namespace {

  std::optional<int> 
  startIndex(std::string dirname, std::ostream& log) {
    fs::path odir(dirname);
    if (!fs::exists(odir)) {
      log << odir.native() << " does not exists.\n";
      return std::nullopt;
    }
    if (!fs::is_directory(odir)) {
      log << odir.native() << " is not a directory.\n";
      return std::nullopt;
    }
    std::optional<int> index;
    for (fs::directory_iterator itr(odir); itr != fs::directory_iterator(); ++itr ) {
      std::string fname = itr->path().filename().native();
      const std::regex regex("disk(\\d+)\\.h5");
      std::smatch match;
      if (std::regex_match(fname, match, regex)) {
        if (match.size() == 2) {
          int idx = boost::lexical_cast<int>(match[1].str());
          log << "Got " << fname << " of index " << idx << '\n';
          if (!index || idx > *index) {
            index = idx;
          }
        }
      }
    }
    if (index) {
      log << "Will start at " << boost::str(boost::format("%1%/disk%2%.h5")%odir.native()%*index) << '\n';
    }
    return index;
  }
 
  bool
  readCmdLine(int argc, char *argv[], std::ostream& log, 
              po::variables_map& vm) {
    std::ostringstream usage;
    usage << "usage: " << argv[0] << " <odir> [options]+\n"
          << "Runs a simulation starting with the 'disk<N>.h5' with the highest "
          << "<N> found in <odir> and writing all subsequent disk<N>.h5 file into "
          << "that same <odir> (thus making checkpointing implicit).\n"
          << "More documentation can be found at 'https://gitlab.oca.eu/DISC/fargOCA/wikis'.";
    po::options_description visible(usage.str());
    visible.add_options()
      (str(HELP_H), "Produce this help message.")
      ("format", po::value<std::string>(),
       "The disk path name pattern, where %1% stands for the file numerical index. "
       "The file matching the pattern with the highest index will be used as the start file.")
      (str(VERBOSE_V), "Display configuration.")
      (str(VERSION), "Display version and exit.")
      ("uninit-temp-fields-to-NaN", "Set uninitialized temporari fields to NaN (or max value).")
      (str(NB_STEPS_N), po::value<int>(), "Run nb step of simulation, regardless of the number of steps initially requested. Beware of restarts...")
      (str(MAX_STEP_M), po::value<int>(), "Override the number of steps initially requested. Great for restarts...")
      ("profile,p", "Print time steps durations, comm infos etc....")
      ("nb-threads", po::value<int>(), "Explicitly specify the number of OpenMP threads.")
      (str(TRACE), po::value<std::vector<std::string>>()->multitoken(), "Label of enabled trace.");
    if (debug::enabled) {
      visible.add_options()
        ("gas-check", po::value<std::string>()->default_value("trace"),
         "In debug mode, define what we do on gas check fail. "
         "Possible values are \"abort\" and \"trace\".");
    }

    po::options_description hidden;
    hidden.add_options()
      ("directory", po::value<std::string>(), "output directory");  
    po::positional_options_description pos;
    pos.add("directory", 1);

    po::options_description all;
    all.add(visible).add(hidden);
    all.add(Environment::cmdLineOptions());
    
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(all).positional(pos).run();
    po::store(parsed, vm);
    po::notify(vm);
    if (bool(vm.count(HELP))) {
      log << visible << '\n';
      return false;
    };
    if (bool(vm.count(VERSION))) {
      log << "Source code id: " << source_version() << '\n'
          << "Branch: " << source_branch() << '\n';
      return false;
    }
    if (bool(vm.count(TRACE))) {
      for (auto label : vm[TRACE].as<std::vector<std::string>>()) {
	debug::activateTrace(label);
      }
    }
    
    if (!bool(vm.count("directory"))) {
      log << "Missing mandatory 'directory' parameter.\n"
          << visible << '\n';
      return false;
    }

    return true;
  }  

  HF::File 
  diskFile(std::string diskDir, int index) {
    std::string diskFmt = diskDir+"/disk%1%.h5";
    return HF::File((boost::format(diskFmt)%index).str(), HF::File::ReadOnly);
  }
  
  template<typename T>
  std::optional<T> optionValue(po::variables_map const& vm, std::string name) {
    if (bool(vm.count(name))) {
      return vm[name].as<T>();
    } else {
      return std::nullopt;
    }
  }
  
  int
  maxStep(Simulation const& simu, po::variables_map vm){
    auto remaining = optionValue<int>(vm, NB_STEPS);
    auto overall   = optionValue<int>(vm, MAX_STEP);
    if (overall && remaining) {
      std::cerr << "can specify both '" << NB_STEPS << "' and '" << MAX_STEP << "'\n";
      std::exit(EX_CONFIG);
    }
    return (overall 
            ? *overall
            : (remaining 
               ? simu.step() + *remaining - 1
               : simu.nbSteps()));
  }  
}
