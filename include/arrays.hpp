// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_ARRAYS_H
#define FARGOCA_ARRAYS_H

#include <Kokkos_Core.hpp>

#include "environment.hpp"
#include "precision.hpp"

/// \file Define more commonly used arrays
namespace fargOCA {
  template <typename T> using arr1d  = Kokkos::View<T*>;
  template <typename T> using arr2d  = Kokkos::View<T**>;
  template <typename T> using arr3d  = Kokkos::View<T***>;
  template <typename T> using harr1d = typename Kokkos::View<T*,   Kokkos::HostSpace>;
  template <typename T> using harr2d = typename Kokkos::View<T**,  Kokkos::HostSpace>;
  template <typename T> using harr3d = typename Kokkos::View<T***, Kokkos::HostSpace>;

  template <typename T>
  struct vec3d final {
    arr3d<T> radial, phi, theta;
    vec3d(arr3d<T> r, arr3d<T> p, arr3d<T> t) : radial(r), phi(p), theta(t) {}
    template<typename M,
             std::enable_if_t<std::is_const_v<T> && std::is_same_v<std::remove_cv_t<T>, M>,bool> = true>
    vec3d(vec3d<M> const& v) : vec3d(v.radial, v.phi, v.theta) {}
  };

  /// \brief Index range type.
  /// Will always be a pair of positive indexes.
  typedef std::pair<size_t, size_t> range_t;
  
  KOKKOS_INLINE_FUNCTION
  size_t size(range_t const& r)  { return r.second - r.first; }
  KOKKOS_INLINE_FUNCTION
  size_t begin(range_t const& r) { return r.first; }
  KOKKOS_INLINE_FUNCTION
  size_t end(range_t const& r)   { return r.second; }

  template<typename MS> inline bool mpiAddressableMemorySpace(MS const&) { return false; }
  inline bool mpiAddressableMemorySpace(Kokkos::HostSpace const&) { return true; }
#ifdef Kokkos_ENABLE_CUDA
  inline bool mpiAddressableMemorySpace(Kokkos::CudaSpace const&) { return Environment::hasGpuDirect(); }
#endif
#ifdef Kokkos_ENABLE_HIP
  inline bool mpiAddressableMemorySpace(Kokkos::HIPSpace const&) { return Environment::hasGpuDirect(); }
#endif
  template<class... RP>
  bool
  mpiAddressableView(Kokkos::View<RP...> const&) { 
    return mpiAddressableMemorySpace(typename Kokkos::View<RP...>::memory_space{});
  }

  template<typename T, class...PL>
  typename std::enable_if<std::is_same<typename Kokkos::View<T const*,PL...>::memory_space,
				       Kokkos::DefaultHostExecutionSpace>::value,
			  harr1d<T const>>::type
  hostROValues(Kokkos::View<T const*,PL...> values) {
    return values;
  }
  
  template<typename T, class...PL>
  typename std::enable_if<!std::is_same<typename Kokkos::View<T const*,PL...>::memory_space,
					Kokkos::DefaultHostExecutionSpace>::value,
			  harr1d<T const>>::type
  hostROValues(Kokkos::View<T const*,PL...> values) {
    harr1d<T> copy(values.label(), values.size());
    Kokkos::deep_copy(copy, values);
    return copy;
  }

  inline
  harr1d<real const>
  hostROValues(arr1d<real const> values) {
    harr1d<real> copy(values.label(), values.size());
    Kokkos::deep_copy(copy, values);
    return copy;
  }

  template<typename T, typename FCT>
  harr1d<T const>
  hostROValues(FCT fct, size_t n) {
    harr1d<T> copy("fct_copy", n);
    Kokkos::parallel_for("from_function", Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0,n),
                         KOKKOS_LAMBDA(size_t const i) {
                           copy(i) = fct(i);
                         });
    return copy;
  }
}

#endif
