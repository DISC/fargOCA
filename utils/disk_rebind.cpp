// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <optional>
#include <filesystem>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"

#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "grid.hpp"
#include "gridDispatch.hpp"
#include "disk.hpp"
#include "simulation.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

bool
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Rebind existing disk to a new grid.\n"
        << '\t' << cmd << " <idisk>.h5 <nr> <ni> <ns> [options]+\n"
        << "Map <idisk>.h5 to grid <grid>.h5 and write into <odisk>.h5.\n";
  
  po::options_description opts(usage.str());
  opts.add_options()
    ("help,h", "This help message.")
    ("ifile", po::value<std::string>(), "HDF5 file containing the existing disk to be rebinded.")
    ("ofile", po::value<std::string>(), "HDF5 file where to write the rebinded disk. Over write input if not specified")
    ("nr", po::value<size_t>(), "radial dimension of the output disk, keep existing if 0.")
    ("ni", po::value<size_t>(), "altitude dimension of the output disk, keep existing if 0.")
    ("ns", po::value<size_t>(), "azimutal dimension of the output disk, keep existing if 0.")
    ("disk-only", "assume stored file is a disk, without simulation specific data or attributes.")
    ("spacing", po::value<GridSpacing>(), "radial spacing of the output disk, can be 'arithmetic' or 'logarithmic', keep existing if not specified.");

  try {
    po::positional_options_description input_options;
    input_options.add("ifile", 1);
    input_options.add("nr", 1);
    input_options.add("ni", 1);
    input_options.add("ns", 1);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_options).run(), vm);    
    po::notify(vm);
    for (std::string opt : {"ifile", "nr", "ni", "ns"}) {
      if (!bool(vm.count(opt))) {
        std::cerr << "Missing mandatory option '<" << opt << "'>.\n";
        std::cerr << opts;
        return false;
      }
    }
    if (vm.count("help") > 0) {
      std::cout << opts;
      return false;
    }
    return true;
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    return false;
  }
}

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);

  po::variables_map vm;
  if (!read_cmd_line(argc,argv,vm)) {  return EX_USAGE;  }
  if (env.world().size() > 1) {
    std::cerr << "Command '" << argv[0] << "' can only run on one MPI process.\n";
    return EX_USAGE;
  }

  shptr<Simulation> simulation {};
  {
    std::string ifname = vm["ifile"].as<std::string>();
    if (!fs::exists(ifname)) {
      std::cerr << "Could not open '" << ifname << "'.\n";
      return EX_NOINPUT;
    }
    simulation = Simulation::make(env.world(), HF::File(ifname, HF::File::ReadOnly).getGroup("/"));
  }
  auto const& old_grid {simulation->disk().dispatch().grid().global()};  
  GridSizes new_sizes { vm["nr"].as<size_t>(), vm["ni"].as<size_t>(), vm["ns"].as<size_t>() };
  {
    auto old {old_grid.sizes()};
    if (new_sizes.nr == 0) { new_sizes.nr = old.nr; }
    if (new_sizes.ni == 0) { new_sizes.ni = old.ni; }
    if (new_sizes.ns == 0) { new_sizes.ns = old.ns; }
  };
  GridSpacing               new_spacing  {vm.count("spacing") ? vm["spacing"].as<GridSpacing>() : old_grid.radialSpacing()};
  shptr<Grid const>         new_grid     {Grid::make(old_grid.shape(), new_sizes, new_spacing)};
  shptr<GridDispatch const> new_dispatch {GridDispatch::make(env.world(), *new_grid)};
  simulation->disk().rebind(*new_dispatch);
  {
    std::string ofile { vm[bool(vm.count("ofile")) ? "ofile" : "ifile"].as<std::string>()}; 
    simulation->writeH5(HF::File{ofile, HF::File::Truncate}.getGroup("/"));
    return EX_OK;
  }
}
