// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include "environment.hpp"
#include "io.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "transportEngine.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

std::string 
output_fname(po::variables_map& vm) {
  std::string ifile = vm["disk"].as<std::string>();
  std::string basename = ifile;
  {
    auto pos = ifile.find_last_of(".h5");
    if (pos != std::string::npos) {
      basename = ifile.substr(0, pos-2);
    }
  }
  return basename + "_temperature.h5";
}

void
write_field(HighFive::Group& group, ScalarField const& field, std::vector<std::string>  path) {
  if (path.size() == 0) {
    field.writeH5(group, 0);
  } else {
    std::string dir = path.front();
    path.erase(path.begin());
    if (group.exist(dir)) {
      if (path.size() ==0) {
        group.unlink(dir); // last component, must remove and create....
        auto child = group.createGroup(dir);
        write_field(child, field, path);
      } else {
        auto child = group.getGroup(dir);
        write_field(child, field, path);
      }
    } else {
      auto child = group.createGroup(dir);
      write_field(child, field, path);
    }
  }
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract temperature.\n"
        << '\t' << cmd << " <idisk>.h5 [option]+.\n";
  
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("output,o", po::value<std::string>(),
     "Output file. If not provided, and --inplace is not used, "
     "will write in <disk>_temperatur.h5 where <disk>.h5 is the input file.")
    ("in-place", po::value<std::string>(), "Add the mdot field to the disk file at the provided path.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  Environment env(argc, argv);
  fmpi::communicator world;
  std::string ifname = vm["disk"].as<std::string>();
  if (world.size() > 1) {
    std::cerr << "As for now, '" << argv[0] << "' must be launched wit only one proc.\n";
    return EX_USAGE;
  } 
  auto disk = Disk::make(world, HF::File(ifname, HF::File::ReadOnly).getGroup("/"));
  if(!disk->physic().adiabatic) {
    disk->dispatch().log() << "Temperature not supported on this disk.\n";
    return EX_CONFIG;
  } else {
    ScalarField field(*disk->temperature());
    if (bool(vm.count("in-place"))) {
      std::string path = vm["in-place"].as<std::string>();
      std::vector<std::string> components;
      boost::split(components, path, boost::is_any_of("/"));
      HF::File ifile(ifname, HF::File::ReadWrite);
      auto root = ifile.getGroup("/");
      write_field(root, field, components);
    } else {
      std::string ofname = output_fname(vm);
      h5::createH5(field, world, ofname);
    }
  }
  return 0;
}
