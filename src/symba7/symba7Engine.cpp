// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "planetarySystemEngine.hpp"
#include "planetarySystem.hpp"
#include "codeUnits.hpp"
#include "swift/symba7_integrator_api.hpp"

extern "C" {
  void s7_set_boundaries(s7_integrator *self, double rmin, double rmax, double rmaxu, double qmin) {
    self->set_boundaries(rmin, rmax, rmaxu, qmin);
  }

  s7_integrator* s7_new(int nb, double t0, int16_t merge, double small) {
    return new s7_integrator(nb, t0, merge, small);
  }

  void s7_delete(s7_integrator* self) {
    delete self;
  }

  void s7_set_planet(s7_integrator* self,
                     int i,
                     double m, 
                     double px, double py, double pz,
                     double vx, double vy, double vz,
                     double r, double rh) {
    self->set_planet(i-1, m, px,py,pz, vx,vy,vz, r,rh);
  }
}

namespace fargOCA {
  class Symba7Engine 
    : public PlanetarySystemEngine {
  public:
    Symba7Engine(std::string name, PlanetarySystem& s, Config const& config) 
      : PlanetarySystemEngine(name, s, config),
        myIntegrator(std::make_unique<s7_integrator>(system().planets().size()+1, real(0), false, 1.0e-8))
    {
      CodeUnits const& units = system().physic().units;
      myIntegrator->set_boundaries(cfg().value("rmin",  units.sunRadius()/units.distance()),
                                   cfg().value("rmax",  s.physic().shape.radius.max),
                                   cfg().value("rmaxu", s.physic().shape.radius.max),
                                   cfg().value("qmin",  units.sunRadius()/units.distance()));
      for (auto const& [name,planet] : s.physic().planetarySystem()->planets) {
        if (!bool(planet.radius)) {
          std::cerr << "Problem with planet '" << name << "': radius must be provided explicitly to use Symba7 integrator.\n";
          std::exit(EX_CONFIG);
        }
      }
    }
    
    ~Symba7Engine() {
    }
    
    void writeSystem() {
      // Register the sun
      int bodyIdx = 0;
      CodeUnits const& units = system().physic().units;
      myIntegrator->set_planet( bodyIdx++,
                                real(units.centralBodyMass()),
                                0.0, 0.0, 0.0,
                                0.0, 0.0, 0.0, 
                                real(units.sunRadius()/units.distance()), 0.0);
      for (auto& np : system().planets()) {
        Planet& planet = np.second;
        Triplet pos = planet.position();
        Triplet vel = planet.velocity();
        myIntegrator->set_planet(bodyIdx++,
                                 planet.mass(),
                                 pos.x, pos.y, pos.z,
                                 vel.x, vel.y, vel.z,
                                 *planet.radius(),
                                 system().HillRadius(planet));
      }
    }
    
    void readSystem() {
      // Register the sun
      int bodyIdx = 1;
      for (auto& [name, planet] : system().planets()) {
        int i = bodyIdx++;
        planet = myIntegrator->get_planet(i);
        if (!bool(system().physic().planetarySystem()->planet(name).hillRadius)) {
          planet.setHillRadius(-1);
        }
      }
    }
  
    virtual void advance(real dt) override {
      writeSystem();
      std::optional<real> dtmax = cfg().value<real>("dtmax");
      if (!dtmax) { // user didn't care => use dt
        myIntegrator->step(dt);
      } else {
        // Go by user specified steps.
        real udt = *dtmax;
        real left = dt;
        while (left > 0) {
          myIntegrator->step(std::min(left, udt));
          left -= udt;
        }
      }
      readSystem();
    }
  private:
    std::unique_ptr<s7_integrator> myIntegrator;
  };
  
  namespace detail {
    PlanetarySystemEngine::Declared<Symba7Engine> symba7("Symba7");
  }
}

