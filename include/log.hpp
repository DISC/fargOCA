// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_LOG_H
#define FARGOCA_LOG_H

#include <iostream>
#include <string>

#include "mpicommunicator.hpp"

namespace fargOCA {
  /** \brief Only print on proc 0 */
  std::ostream& log(fmpi::communicator const& comm, std::ostream& = std::cout);

  /** \brief Only print if cond */
  std::ostream& log(bool cond, std::ostream& = std::cout);

  template<class OStream, typename... Args >
  std::unique_ptr<std::ostream> make0Stream(bool cond, Args... args) {
    if (cond) {
      return std::make_unique<OStream>(args...);
    } else {
      return std::make_unique<std::ostream>(nullptr);
    }
  }
}

#endif

