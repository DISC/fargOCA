// Copyright 2025, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2025, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2025, Maya Tatarelli  maya.tatarelli<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_SIMULATION_STEP__PLUGIN_H
#define FARGO_SIMULATION_STEP__PLUGIN_H

#include <functional>
#include <map>
#include <set>
#include <string>

#include "plugin.hpp"

namespace fargOCA {
  class Simulation;

  /// \brief Indicate the simulation callback points
  /// There is a BEFORE_<step name>_STEP and AFTER_<step name>_STEP for each step
  enum class SimulationStepEvent { BEFORE_STEP,
                                   BEFORE_ACCRETION_STEP,
                                   AFTER_ACCRETION_STEP,
                                   BEFORE_VELOCITY_STEP,
                                   AFTER_VELOCITY_STEP,
                                   BEFORE_VISCOSITY_STEP,
                                   AFTER_VISCOSITY_STEP,
                                   BEFORE_ADIABATIC_STEP,
                                   AFTER_ADIABATIC_STEP,
                                   BEFORE_ENERGY_STEP,
                                   AFTER_ENERGY_STEP,
                                   BEFORE_TRANSPORT_STEP,
                                   AFTER_TRANSPORT_STEP,
                                   AFTER_STEP };

  using SimulationStepPlugin  = Plugin<SimulationStepEvent, std::string, Simulation&, SimulationStepEvent>;
  
  template<> inline std::string Plugin<SimulationStepEvent, std::string, Simulation&, SimulationStepEvent>::id() { return "simulation_step"; }  
}
#endif 
