# Building on Occigen

## Environment

FargOCA is regularly  tested on [Occigen](https://www.cines.fr/calcul/materiels/occigen/).

Assuming the source are installed in  <srcdir>, you can load the supported environment with:
```
$ source <srcdir>/contrib/build/Occigen/env.sh
...
$
```

 * **Git LFS**
You need to explicitly load the git module in order to have lfs. 

 * **CC and CXX**
Note that on *occigen*, loading the compiler module does not set the `CC` and `CXX` environment variables. So you might need to:
```
$ export CC=icc
$ export CXX=icpc
```

## Build

Once this environment is loaded, you just need to run `cmake`:
```
[elena@login0:<builddir>]$ cmake <srcdir> ..
```
 * **Testing** platform specific scripts allows you to run the test in parallel
   * `<builddir>/tools/dev/occTest.sh` dispatch the tests on the cluster and track the result. You can interrupt the monitoring phase and resume it later with...
  *  `<builddir>/tools/dev/occTrackTest.sh` track the dispatched tests.

refer to those script help (`-h`) option for more details.
