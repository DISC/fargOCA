// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <filesystem>
#include "boost/program_options.hpp"
#include "boost/algorithm/string.hpp"

#include <highfive/H5File.hpp>

#include "environment.hpp"
#include "allmpi.hpp"
#include "arrayTestUtils.hpp"
#include "disk.hpp"

using namespace fargOCA;
namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

bool
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << '\t' << cmd << " diskpath1.h5 diskpath2.h5 [path] [options]+: "
        << "compare the content of field located at [path] in 2 disk encoded in the provided HDF5 files. "
        << "If [path] is not provided, all field are compared. \n" 
        << "[path] can be specified as a simple path or as a triplet path:atol:rtol. "
        << "In the later case, the provided absolute and relative diferences will overide the default ones.\n";
  
  po::options_description visible(usage.str());
  po::options_description hidden;

  visible.add_options()
    ("help,h", "This help message.")
    ("rtol",   po::value<real>()->default_value(1e-10), 
     "the default relative autorized tolerance. If provided and not satisfied, the program will return a non 0 status.")
    ("atol",   po::value<real>()->default_value(1e-11), 
     "the default absolute autorized tolerance. If provided and not satisfied, the program will return a non 0 status.")
    ("common",  "compare all common fields (implicit if no path provided).")
    ("verbose", "verbose mode, indicates what is compared, what is not, how and why.")
    ("all",  "compare all fields, if the disk do not contain the same field names, return a non 0 status.");

  hidden.add_options()
    ("disk1",  po::value<std::string>()->required(), "first HDF5 input file.")
    ("disk2",  po::value<std::string>()->required(), "second HDF5 input file.")
    ("path",   po::value<std::vector<std::string>>()->composing(), 
     "the path to the field to compare. If not provided, all fields are compared.");

  po::positional_options_description pos; 
  pos.add("disk1", 1);
  pos.add("disk2", 1);
  pos.add("path",  -1);
  try {
    po::options_description opts;
    opts.add(visible).add(hidden);
    po::store(po::command_line_parser(argc, argv).options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch(const po::error& e) {
    std::cerr << "Couldn't parse command line arguments properly:\n"
              << e.what() << '\n' << '\n'
              << visible << '\n';
    return false;
  }
  if(vm.count("help")) {
    std::cout << visible << "\n";
    return false;
  }
  for(std::string o : {"disk1", "disk2"}) {
    if (!bool(vm.count(o))) {
      std::cerr << "Missing mandatory option '" << o << "'.\n";
      return false;
    }
  }
  return true;
}

struct field_config {
  std::string path;
  real atol, rtol;
};

void
dump(std::ostream& out, std::string label, std::optional<std::vector<field_config>> const& cfgs) {
  out << label;
  if (!cfgs) {
    out << " N/A.\n";
  } else {
    if (cfgs->size() == 0) {
      out << " None.\n";
    } else {      
      out << ":\n";
      for (field_config const& cfg : *cfgs) {
        out << '\t' << cfg.path << "/atol:" << cfg.atol << "/rtol:" << cfg.rtol << '\n'; 
      }
    }
  }
}

/// \brief Extract common subfield names and complain according to the user wishes
std::optional<std::vector<std::string>>
check_common_disk_fields(Disk const& disk1, Disk const& disk2, po::variables_map vm) {
  std::optional<std::vector<std::string>> common = std::vector<std::string>();
  std::vector<std::string> fields1, fields2, skipped1, skipped2;
  for (auto const& p : disk1.namedFields()) { fields1.push_back(p.first); }
  for (auto const& p : disk2.namedFields()) { fields2.push_back(p.first); }
  std::sort(fields1.begin(), fields1.end());
  std::sort(fields2.begin(), fields2.end());
  std::set_intersection(fields1.begin(), fields1.end(),
                        fields2.begin(), fields2.end(),
                        std::back_inserter(*common));
  std::set_difference(common->begin(), common->end(), 
                      fields1.begin(), fields1.end(),
                      std::back_inserter(skipped1));
  std::set_difference(common->begin(), common->end(), 
                      fields2.begin(), fields2.end(),
                      std::back_inserter(skipped2));
  if (vm.count("verbose")) {
    std::cout << "Common field(s): ";
    std::copy(common->begin(), common->end(), std::ostream_iterator<std::string>(std::cout, " "));
    std::cout << std::endl;
    if (skipped1.size() > 0) {
      std::cout << "The following field(s) could not be found in '" << vm["disk1"].as<std::string>() << "\n";
      std::copy(skipped1.begin(), skipped1.end(), std::ostream_iterator<std::string>(std::cout, " "));
      std::cout << std::endl;
    }
    if (skipped2.size() > 0) {
      std::cout << "The following field(s) could not be found in '" << vm["disk2"].as<std::string>() << "\n";
      std::copy(skipped2.begin(), skipped2.end(), std::ostream_iterator<std::string>(std::cout, " "));
      std::cout << std::endl;
    }    
  }
  if (bool(vm.count("all"))) {
    if (skipped1.size() > 0) {
      common = std::nullopt;
    }
    if (skipped2.size() > 0) {
      common = std::nullopt;
    }
  }
  return common;
}

/// \brief Dis user specified a field more than one ?
bool
check_for_duplicates(std::vector<std::string> descs) {
  std::vector<std::string> field_names;
  std::transform(descs.begin(), descs.end(), std::back_inserter(field_names), 
                 [](std::string f) -> std::string { std::vector<std::string> desc; return boost::split(desc, f, boost::is_any_of(":"))[0]; });
  std::sort(field_names.begin(), field_names.end());
  auto duplicate = std::adjacent_find(field_names.begin(), field_names.end());
  if (duplicate != field_names.end()) {
    std::cerr << "Error: field '" << *duplicate << "' specified more than once\n.";
    return false;
  } else {
    return true;
  }
}

bool
has_field(GridDispatch const& grid, std::string fname, std::string path) {
  ScalarField field(path, grid);
  try {
    field.readH5(fname, path);
    return true;
  } catch (std::exception const& e) {
    std::cerr << e.what() << "'\n";
    return false;
  }
}

/// \brief Extract the explicit fields to check from the command line.
/// \param common the common field of the 2 disk. Explicitly configured fields will be removed from it.
std::optional<std::vector<field_config>>
extract_explicit_fields(GridDispatch const& grid, po::variables_map const& vm, std::vector<std::string>& common) {
  std::optional<std::vector<field_config>> results = std::vector<field_config>();
  if (bool(vm.count("path"))) {
    bool ok = true;
    std::vector<field_config> configs;
    std::vector<std::string> descs = vm["path"].as<std::vector<std::string>>();
    if (!check_for_duplicates(descs)) {
      ok = false;
    } else {
      for(std::string desc : descs) {
        std::vector<std::string> tuple;
        boost::split(tuple, desc, boost::is_any_of(":"));
        field_config cfg;
        try {
          switch (tuple.size()) {
          case 1:
            cfg = {tuple[0], vm["atol"].as<real>(), vm["rtol"].as<real>()};
            break;
          case 2:
            cfg = {tuple[0], boost::lexical_cast<real>(tuple[1]), vm["rtol"].as<real>()};
            break;
          case 3:
            cfg = {tuple[0], boost::lexical_cast<real>(tuple[1]), boost::lexical_cast<real>(tuple[2])};
            break;
          default:
            std::cerr << "Error while parsing config spec '" << desc << "'. must be of the form <string>[:<atol>[:<rtol>]].";
            ok = false;
            continue;
          }
        } catch (boost::bad_lexical_cast& e) {
          std::cerr << "Error: unable to interpret user spec '" << desc << "'.\n";
          ok = false;
          continue;
        }
        auto found = std::find(common.begin(), common.end(), cfg.path);
        if (found == common.end()) {
          bool extension = true;
          {
            // Not an "official" field, could be some user extention
            if (!has_field(grid, vm["disk1"].as<std::string>(), cfg.path)) {
              extension = false;
            }
            if (!has_field(grid, vm["disk2"].as<std::string>(), cfg.path)) {
              extension = false;
            }
          }
          if (!extension) {
            std::cerr << "Could not find '" << cfg.path << "' in both disks. "
                      << "Allowed fields are: ";
            std::copy(common.begin(), common.end(), std::ostream_iterator<std::string>(std::cerr, " "));
            std::cerr << std::endl;
            ok = false;
            continue;
          } else {
            configs.push_back(cfg);
          }
        } else {
          common.erase(found);
          configs.push_back(cfg);
        }
      }
    }
    if (ok) {
      results = configs;
    } else {
      results = std::nullopt;
    }
  }
  if (vm.count("verbose")) {
    dump(std::cout, "Explicitly compared by user", results);
  }
  return results;
}

std::optional<std::vector<field_config>>
extract_field_configs(Disk const& disk1, Disk const& disk2, po::variables_map vm) {
  std::optional<std::vector<std::string>> common = check_common_disk_fields(disk1, disk2, vm);
  if (common) {
    std::optional<std::vector<field_config>> configs = extract_explicit_fields(disk1.dispatch(), vm, *common);
    if (bool(vm.count("all")) || !bool(vm.count("path"))) {
      std::vector<field_config> implicit;
      for(std::string field : *common) {
        implicit.push_back({field, vm["atol"].as<real>(), vm["rtol"].as<real>()});
      }
      if (vm.count("verbose")) {
        dump(std::cout, "Implicitly compared by user", implicit);
      }
      configs->insert(configs->end(), implicit.begin(), implicit.end());
    }
    return configs;
  } else {
    return std::nullopt;
  }
}

bool
compare_fields(ScalarField const& field1, ScalarField const& field2, field_config const& cfg, std::ostream& log ) {
  log << "Field '" << cfg.path << "':\t [avg:" << average(field2.data(), field1.data()) << "] ";
  return checkAbsRelDiff(maxAbsRelDiff(field1.data(), field2.data()), cfg.atol, cfg.rtol, log);
}

ScalarField
get_field(GridDispatch const& dispatch, Disk::NamedFields const& officials, std::string fname, std::string path) {
  try {
    return ScalarField(officials.at(path));
  } catch(std::out_of_range const& e) {
    // maybe a user registered field
    try {
      ScalarField f(path, dispatch);
      f.readH5(fname, path);
      return f;
    } catch(std::exception const& e) {
      std::cout << path << "Not found in " << fname;
      throw e;
    }
  }
}

int
main(int argc, char** argv) {
  Environment env(argc, argv);
  fmpi::communicator world;
  
  po::variables_map vm;
  if (!read_cmd_line(argc, argv, vm)) {
    return EX_USAGE;
  }
  std::string path1 = vm["disk1"].as<std::string>();
  std::string path2 = vm["disk2"].as<std::string>();
  HF::File file1(path1, HF::File::ReadOnly);
  HF::File file2(path2, HF::File::ReadOnly);

  shptr<Disk> disk1 = Disk::make(world, (file1.getGroup("/")));
  shptr<Disk> disk2 = Disk::make(world, (file2.getGroup("/")));
  
  Disk::NamedFields fields2 = disk2->namedFields();
  Disk::NamedFields fields1 = disk1->namedFields();
  bool passed = true;
  std::optional<std::vector<field_config>> configs = extract_field_configs(*disk1, *disk2, vm);
  if (!configs) {
    passed = false;
  } else {
    for (field_config const& cfg : *configs) {
      ScalarField field1 = get_field(disk1->dispatch(), fields1, path1, cfg.path);
      ScalarField field2 = get_field(disk2->dispatch(), fields2, path2, cfg.path);
      if (!compare_fields(field1, field2, cfg, std::cout)) {
        passed = false;
      }
      std::cout << '\n';
    }
  }
  if (passed) {
    std::cout << "PASSED\n";
    return EX_OK;
  } else {
    std::cout << "FAILED\n"; 
    return -1;
  }
}

