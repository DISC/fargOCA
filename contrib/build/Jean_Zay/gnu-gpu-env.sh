# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load git/2.39.1 git-lfs ninja/1.12.1 cmake/3.31.4 
module load gcc/13.3.0 cuda/12.4.1 openmpi/4.1.5-cuda
module load hdf5/1.12.0 boost/1.86.0-mpi-cuda 

unset Kokkos_DIR
export CC=gcc
export CXX=g++
export FC=gfortran
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
