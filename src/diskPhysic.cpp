// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <Kokkos_MathematicalFunctions.hpp>

#include "diskPhysic.hpp"
#include "codeUnits.hpp"
#include "log.hpp"
#include "debug.hpp"
#include "loops.hpp"
#include "io.hpp"
#include "h5io.hpp"
#include "optIO.hpp"
#include "arraysUtils.hpp"
#include "viscosityFieldPlugin.hpp"
#include "h5Labels.hpp"
#include "diskInitPlugin.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace kkm = Kokkos::Experimental;

  using namespace h5::labels;
  
#define LBD KOKKOS_LAMBDA

  void
  DiskPhysic::linkBack() {
    if (adiabatic) {
      adiabatic->cfg = shared();
      if (adiabatic->radiative) {
        adiabatic->radiative->cfg = shared();
        if (adiabatic->radiative->star) {
          adiabatic->radiative->star->cfg = shared();
        }
      }
    }
    if (starAccretion) {
      starAccretion->cfg = shared();
      if (starAccretion->wind) {
	starAccretion->wind->cfg = shared();
      }
    }
  }
  
  DiskPhysic::DiskPhysic(CodeUnits&& u,
                         real flaringIdx,
                         GasShape  dshape,
                         Referential ref,
                         PluginConfig bcCfgs,
                         PluginConfig initCfgs,
                         PluginConfig stepsCfgs,                         
                         std::optional<StarAccretion> starAccCfg,
                         Viscosity viscCfg,
                         std::optional<Adiabatic> adiab,
                         Density densCfg,
                         Smoothing smoothCfg,
                         std::optional<PlanetarySystemPhysic> planets,
                         real cfl,
                         real kdrag,
                         real hillCut,
                         Transport transp,
                         real mu)
  : units(u),
    shape({dshape}),
    referential(ref),
    boundaries(bcCfgs),
    init(initCfgs),
    steps(stepsCfgs),
    starAccretion(starAccCfg),
    viscosity(viscCfg),
    adiabatic(adiab),
    density(densCfg),
    smoothing(smoothCfg),
    myPlanetarySystem(planets),
    flaringIndex(flaringIdx),
    CFLSecurity(cfl),
    draggingCoef(kdrag),
    hillCutFactor(hillCut),
    transport(transp),
    meanMolecularWeight(mu),
    aspectRatio(shape.aspectRatio, flaringIndex)
  {}

  shptr<DiskPhysic const> DiskPhysic::make(CodeUnits const& units, HighFive::Group const& group) {
    std::shared_ptr<DiskPhysic const> ptr(new DiskPhysic(CodeUnits(units), group));
    DiskPhysic& self = const_cast<DiskPhysic&>(*ptr);    
    self.linkBack();
    return  ptr;
  }
  
  DiskPhysic::AspectRatio::AspectRatio() 
    : AspectRatio(std::numeric_limits<real>::quiet_NaN(),
		  std::numeric_limits<real>::quiet_NaN()) {}
		  
  DiskPhysic::DiskPhysic(CodeUnits&& u, HighFive::Group group)
    : units(u) {
    readH5(group);
    aspectRatio = AspectRatio(shape.aspectRatio, flaringIndex);
  }

  DiskPhysic::~DiskPhysic() {}

  shptr<DiskPhysic const>
  DiskPhysic::make(CodeUnits&& units,
                   real flaringIdx,
                   GasShape shape,
                   Referential ref,
                   PluginConfig bounds,
                   PluginConfig init,
                   PluginConfig steps,
                   std::optional<StarAccretion> starAcc,
                   Viscosity viscosity,
                   std::optional<Adiabatic> adiabatic,
                   Density density,
                   Smoothing smoothing,
                   std::optional<PlanetarySystemPhysic> planets,
                   real CFLSecurity,
                   real draggingCoef,
                   real hillCutFactor,
                   Transport transport,
                   real mu) {
    std::shared_ptr<DiskPhysic const> ptr(new DiskPhysic(std::move(units),
							 flaringIdx,
                                                         shape,
                                                         ref,
                                                         bounds,
                                                         init,
                                                         steps,
                                                         starAcc,
                                                         viscosity,
                                                         adiabatic,
                                                         density,
                                                         smoothing,
                                                         planets,
                                                         CFLSecurity,
                                                         draggingCoef,
                                                         hillCutFactor,
                                                         transport,
                                                         mu));
    DiskPhysic& self = const_cast<DiskPhysic&>(*ptr);    
    self.linkBack();
    return ptr;
  }
  
  real
  DiskPhysic::hillCut(real d, real rh) const {
    //see (Crida & Kley 2008) for details
    real nd = d/(rh*hillCutFactor);//normalized distance
    return 1/(std::exp(-10*(nd-1))+1);
  }

  real
  DiskPhysic::StarAccretion::convertUserAccretionRate(real userAccretionRate, CodeUnits const& units) {
    // Why not give the correct value to start with ?
    return (userAccretionRate
            *(units.centralBodyMass()/YEAR) //convert to cgs
            *units.time()); //then to code units
  }

  real
  DiskPhysic::StarAccretion::convertUserAccretionRate(real userAccretionRate) const {
    // Why not give the correct value to start with ?
    CodeUnits const& units = cfg.lock()->units;
    return convertUserAccretionRate(userAccretionRate, units);
  }

  real
  DiskPhysic::StarAccretion::rate() const {
    return convertUserAccretionRate(userRate);
  }

  std::ostream&
  operator<<(std::ostream& out, DiskReferential t) {
    switch (t) {
    case DiskReferential::CONSTANT:
      out << "Constant";
      break;
    case DiskReferential::COROTATING:
      out << "CoRotating";
      break;
    default:
      out << "Error";
      out.setstate(out.failbit);
    }
    return out;
  }
  std::istream&
  operator>>(std::istream& in, DiskReferential& r) {
    std::string tk;
    in >> tk;
    boost::to_upper(tk);
    if (tk == "FIXED") {
      r = static_cast<DiskReferential>(256);
      in.setstate(in.failbit);
    } else if (tk == "CONSTANT") {
      r = DiskReferential::CONSTANT;
    } else if (tk == "CIRCULAR3BODY" || tk == "CIRCULAR_3BODY" || tk == "RC3BP") {
      r = static_cast<DiskReferential>(256);
      in.setstate(in.failbit);
    } else if (tk == "COROTATING") {
      r = DiskReferential::COROTATING;
    } else {
      in.setstate(in.failbit);
      r = static_cast<DiskReferential>(256);
    }
    return in;
  }
  
  template<> std::vector<DiskReferential> const& enumValues<DiskReferential>() {
    static std::vector<DiskReferential> values = { DiskReferential::CONSTANT, DiskReferential::COROTATING };
    return values;
  }

  std::ostream&
  operator<<(std::ostream& out, Transport t) {
    switch(t) {
    case Transport::NORMAL:
      out << "Normal";
      break;
    case Transport::FAST:
      out << "Fast";
      break;
    default:
      out << "transport<" << int(t) << ">";
      break;
    }
    return out;
  }

  std::istream& operator>>(std::istream& in, Transport& t) {
    std::string tk;
    in >> tk;
    boost::to_upper(tk);
    if (tk == "NORMAL" || tk == "STANDARD") {
      t = Transport::NORMAL;
    } else if (tk == "FAST" || tk == "FARGO") {
      t = Transport::FAST;
    } else {
      in.setstate(in.failbit);
    }
    return in;
  }

  template<> std::vector<Transport> const& enumValues<Transport>() {
    static std::vector<Transport> values = { Transport::NORMAL, Transport::FAST };
    return values;
  }

  std::ostream& 
  operator<<(std::ostream& out, StarAccretion sa) {
    switch(sa) {
    case StarAccretion::CONSTANT:
      out << "constant";
      break;
    case StarAccretion::WIND:
      out << "wind";
      break;
    default:
      out << "<bad star accretion type(" << int(sa) << ")>";
      out.setstate(std::ios_base::failbit);
    }
    return out;
  }

  std::istream&
  operator>>(std::istream& in, StarAccretion& sa) {
    std::string tk;
    in >> tk;
    if (in) {
      boost::to_upper(tk);
      if (tk == "CONSTANT") {
        sa = StarAccretion::CONSTANT;
      } else if (tk == "WIND") {
        sa = StarAccretion::WIND;
      } else {
        std::cerr << "Unknown StarAccretion type: '" << tk << "'\n"
                  << "Possible values are: " 
                  <<  StarAccretion::CONSTANT << " " << StarAccretion::WIND << '\n';
        in.setstate(std::ios_base::failbit);
        sa = static_cast<StarAccretion>(-1);
      }
    } else {
      sa = static_cast<StarAccretion>(-1);
    }
    return in;
  }
  template<> std::vector<StarAccretion> const& enumValues<StarAccretion>() {
    static std::vector<StarAccretion> values = {StarAccretion::CONSTANT, StarAccretion::WIND };
    return values;
  };

  std::ostream&
  operator<<(std::ostream& out, OpacityLaw l) {
    switch(l) {
    case OpacityLaw::BELL_LIN:
      out << "BELL_LIN";
      break;
    case OpacityLaw::CONSTANT:
      out << "CONSTANT";
      break;
    default:
      std::abort();
    }
    return out;
  }

  std::istream&  operator>>(std::istream& in, OpacityLaw& l) {
    std::string tk;
    in >> tk;
    boost::to_upper(tk);
    if (in) {
      if (tk == "BELL_LIN" || tk == "0" ) {
        l = OpacityLaw::BELL_LIN;
      } else if  (tk == "CONSTANT" || tk == "1" ) {
        l = OpacityLaw::CONSTANT;
      } else {
        std::cerr << "Unknown OpacityLaw: '" << tk << "'\n"
                  << "Possible values are: "
                  << OpacityLaw::BELL_LIN << ' '
                  << OpacityLaw::CONSTANT << '\n';
        in.setstate(std::ios_base::failbit);
        l = static_cast<OpacityLaw>(-1);
        std::abort();
      }
    } else {
      in.setstate(std::ios_base::failbit);
      std::abort();
    }    
    return in;
  }
  
  template<> std::vector<OpacityLaw> const& enumValues<OpacityLaw>() {
    static std::vector<OpacityLaw> values = { OpacityLaw::BELL_LIN, OpacityLaw::CONSTANT };
    return values;
  };

  std::ostream&
  operator<<(std::ostream& out, CoolingType l) {
    switch(l) {
    case CoolingType::BETA:
      out << "beta";
      break;
    case CoolingType::RADIATIVE:
      out << "radiative";
      break;
    default:
      std::abort();
    }
    return out;
  }

  std::istream& 
  operator>>(std::istream& in, CoolingType& l) {
    std::string tk;
    in >> tk;
    if (in) {
      boost::to_upper(tk);
      if (tk == "BETA") {
        l = CoolingType::BETA;
      } else if  (tk == "RADIATIVE") {
        l = CoolingType::RADIATIVE;
      } else {
        std::cerr << "Unknown CoolingType: '" << tk << "'\n"
                  << "Possible values are: "
                  << CoolingType::BETA << ' '
                  << CoolingType::RADIATIVE << '\n';
        in.setstate(std::ios_base::failbit);
        l = static_cast<CoolingType>(-1);
	in.setstate(std::ios_base::failbit);
      }
    } else {
      in.setstate(std::ios_base::failbit);
      std::abort();
    }
    return in;
  }

  template<> std::vector<CoolingType> const& enumValues<CoolingType>() {
    static std::vector<CoolingType> values = { CoolingType::BETA, CoolingType::RADIATIVE };
    return values;
  }  

  void
  DiskPhysic::Referential::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Referential:\n"
        << tabs(tab+1) << type << '\n'
        << tabs(tab+1) << "omega: " << omega << '\n'
        << tabs(tab+1) << "indirectForces: " << yesno(indirectForces) << '\n';
    if (type == DiskReferential::COROTATING) {
      out << tabs(tab+1) << "guidingCenter: " << yesno(*guidingCenter) << '\n';
      out << tabs(tab+1) << "guidingPlanet: " << guidingPlanet << '\n';
    }
  }

  void
  DiskPhysic::Referential::writeH5(HighFive::Group grp) const {
    h5::writeAttribute(grp, TYPE, type);
    if (type == DiskReferential::COROTATING) {
      h5::writeAttribute(grp, "guiding_center", *guidingCenter);
      h5::writeAttribute(grp, "guiding_planet", *guidingPlanet);
    }
    if (omega) {
      h5::writeAttribute(grp, "omega", *omega);
    }
    h5::writeAttribute(grp, "indirectForces", indirectForces);
  }

  void
  DiskPhysic::Referential::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, TYPE, type);
    if (type == DiskReferential::COROTATING) {
      h5::readAttribute(grp, "guiding_planet", guidingPlanet);
      if (!guidingPlanet) {
	std::cerr << "Guiding planet needed and none provided. Will try to guess.\n";
      }
    }
    h5::readAttribute(grp, "guiding_center", guidingCenter);
    h5::readAttribute(grp, "omega", omega);
    h5::readAttribute(grp, "indirectForces", indirectForces);
  }

  void
  DiskPhysic::Density::Cavity::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Cavity:\n"
        << tabs(tab+1) << "radius: " << radius << '\n'
        << tabs(tab+1) << "ratio: " << ratio << '\n'
        << tabs(tab+1) << "width: " << width << '\n';
  }

  void
  DiskPhysic::Density::Cavity::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "radius", radius);
    h5::writeAttribute(grp, "ratio", ratio);
    h5::writeAttribute(grp, "width", width);
  }

  void
  DiskPhysic::Density::Cavity::readH5(HighFive::Group const& grp)  {
    h5::readAttribute(grp, "radius", radius);
    h5::readAttribute(grp, "ratio", ratio);
    h5::readAttribute(grp, "width", width);
  }

  DiskPhysic::Density::Cavity
  DiskPhysic::Density::Cavity::diff(Cavity const& c1, Cavity const& c2) {
    return Cavity{std::abs(c1.radius - c2.radius),
                  std::abs(c1.ratio - c2.ratio),
                  std::abs(c1.width - c2.width)};
  }    

  void
  DiskPhysic::StarAccretion::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Star Accretion:\n"
        << tabs(tab+1) << type << '\n'
        << tabs(tab+1) << "* userAccretionRate (Solar mass per year): " << userRate << '\n'
        << tabs(tab+2) << " actual accretion rate (code units): " << rate() << '\n';
    if (type == Type::WIND) {
      out << tabs(tab+1) << "* torqueCoeff: " << torqueCoeff() << '\n';
      wind->dump(out, tab+1);
    }
  }

  real
  DiskPhysic::StarAccretion::Wind::activeDensity() const { 
    CodeUnits const& units = cfg.lock()->units;
    return userActiveDensity*ipow<2>(units.distance())/(units.centralBodyMass()*units.sunMass()); 
  }

  void
  DiskPhysic::StarAccretion::Wind::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Wind :\n"
        << tabs(tab+1) << "* activeDensity (user in g/cm^2) : " << userActiveDensity << '\n'
        << tabs(tab+1) << "* filter: " << filter << '\n'
        << tabs(tab+1) << "* activeDensity*R0^2/XM0 (code units) : " << activeDensity() << '\n';
  }

  void
  DiskPhysic::StarAccretion::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, TYPE, type);
    h5::writeAttribute(grp, "rate", userRate);
    if (wind) {
      auto wg = grp.createGroup("wind");
      wind->writeH5(wg);
    }
  }

  DiskPhysic::StarAccretion::StarAccretion(HighFive::Group grp)
    : StarAccretion(*h5::attributeValue<Type>(grp, TYPE),
		    *h5::attributeValue<real>(grp, "rate"),
		    std::nullopt) {
    if (grp.exist("wind")) {
      wind = Wind(grp.getGroup("wind"));
    }
  }

   
  void
  DiskPhysic::StarAccretion::Wind::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "active_density", userActiveDensity);
    h5::writeAttribute(grp, "filter", filter);
  }

  DiskPhysic::StarAccretion::Wind::Wind(HighFive::Group grp)
    : Wind(*h5::attributeValue<real>(grp, "active_density"),
	   *h5::attributeValue<real>(grp, "filter"))
  {}

  void
  DiskPhysic::Viscosity::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Viscosity :\n"
        << tabs(tab+1) << "* artificial : " << yesno(artificial) << '\n'
        << tabs(tab+1) << "* type : " << type << '\n';
    config.dump(out, tab+2);
  }
  
  void
  DiskPhysic::Viscosity::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, TYPE, type);
    h5::writeAttribute(grp, ARTIFICIAL, artificial);
    config.writeH5(grp, CONFIGURATION);
  }
  
  void
  DiskPhysic::Viscosity::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, ARTIFICIAL, artificial);
    h5::readAttribute(grp, TYPE, type);
    config = Config(grp.getGroup(CONFIGURATION));
  }
  
  DiskPhysic::Adiabatic::Radiative::Radiative(Opacity const& o,
					      Solver  const& sv,
					      real                zt,
                                              real                d2g,
                                              std::optional<real> ets,
                                              std::optional<Star> s)
    : opacity(o),
      solver(sv),
      userZBoundaryTemperature(zt),
      dustToGas(d2g),
      energyTimeStep(ets),
      star(s),
      cfg()
  {    
  }

  real
  DiskPhysic::Adiabatic::Radiative::Star::fStar() const {
    CodeUnits const& units = cfg.lock()->units;
    return (units.StefanBoltzmannCst()
	    *ipow<4>(units.centralBodyTemperature()/units.temperature())
	    *ipow<2>(units.centralBodyRadius()*units.sunRadius()/units.distance()));
  }
  
  void 
  DiskPhysic::Adiabatic::Radiative::Star::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Star Radiation:\n"
        << tabs(tab+1) << "* fStar: " << fStar() << '\n';
  }
  
  void 
  DiskPhysic::Adiabatic::BetaCooling::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "BetaCooling:\n"
        << tabs(tab+1) << "* time scale: " << timeScale << '\n';
  }

  void 
  DiskPhysic::Adiabatic::RadiativeCooling::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "RadiativeCooling:\n";
  }
  
  real
  DiskPhysic::Adiabatic::Radiative::zBoundaryTemperature() const {
    return userZBoundaryTemperature/cfg.lock()->units.temperature();
  }

  real
  DiskPhysic::Adiabatic::Radiative::zBoundaryEnergy() const {
    return ipow<4>(zBoundaryTemperature())*cfg.lock()->units.radiationCst();
  }

  void
  DiskPhysic::Adiabatic::Radiative::Solver::dump(std::ostream& out, int tab) const {
    out << tabs(tab+1) << "* stellar radiation solver: " << label << '\n';
    config.dump(out, tab+2);
  }
  
  void
  DiskPhysic::Adiabatic::Radiative::dump(std::ostream& out, int tab) const {
    out << tabs(tab) <<  "Equation of State, full energy\n"
	<< tabs(tab+1) << "* opacity:\n";
    opacity.dump(out, tab+1);
    out << tabs(tab+1) << "* solver:\n";
    solver.dump(out, tab+1);
    out << tabs(tab+1) << "* zBoundaryTemperature (user): " << userZBoundaryTemperature   << '\n'
        << tabs(tab+1) << "* zBoundaryTemperature (used): " << zBoundaryTemperature() << '\n'
        << tabs(tab+1) << "* zBoundaryEnergy (used): " << zBoundaryEnergy() << '\n'
        << tabs(tab+1) << "* energyTimeStep:" << energyTimeStep << '\n'
        << tabs(tab+1) << "* dust to gas: " << dustToGas << '\n';


    if (star) {
      star->dump(out, tab+1);
    }
  }

  void
  DiskPhysic::Adiabatic::Radiative::Opacity::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "* type: " << type << '\n'
	<< tabs(tab) << "* config: " << '\n';
    config.dump(out, tab+1);
  }

  // That stuff need to be inline (sort of) to please CUDA (sort of)
  namespace details {
    KOKKOS_INLINE_FUNCTION
    real
    LinOpacity(real temp, real rho)
    {
      using kk::pow;
      using kk::cbrt;
      real const t234 = 1.6e3;
      real const t456 = 5.7e3;
      real const t678 = 2.28e6;
      
      real ts4   = 1.e-4*temp; //   to avoid overflow
      if (temp < t234*pow(rho,4.44444444e-2)){
        //  coefficients for opacity laws in cgs units.
        real const ak[] = { 2.e-4, 2.e16, 5.e-3};
        //   disjoint opacity laws
        real o1 = ak[0]*ipow<2>(temp);
        real o2 = ak[1]/ipow<7>(temp);
        real o3 = ak[2]*temp;
        //   parameters used for smoothing
        real o1an = ipow<2>(o1);
        real o2an = ipow<2>(o2);
        //   smoothed and continuous opacity law for regions 1, 2, and 3.
        return pow((ipow<2>(o1an*o2an/(o1an+o2an))
                    +ipow<4>(o3/(1+1.e22/ipow<10>(temp)))),
                   0.25);
      } else if (temp < t456*pow(rho,2.381e-2)) {
        // coefficients for opacity laws T_4 units.
        real const bk[] = { 50, 2.e-2, 2.e4 };
        
        //   disjoint opacity laws for 3, 4, and 5.
        real o4 = bk[1]*ipow<2>(cbrt(rho))/ipow<9>(ts4);
        real o5 = bk[2]*ipow<2>(cbrt(rho))*ipow<3>(ts4);
        //   parameters used for smoothing
        real o4an = ipow<4>(o4);
        real o3an = ipow<4>(bk[0]*ts4);
        //  smoothed and continuous opacity law for regions 3, 4, and 5.
        return pow(((o4an*o3an/(o4an+o3an))
                    +ipow<4>(o5/(1+6.561e-5/ipow<8>(ts4)))),
                   0.25);
      } else if (temp < t678*pow(rho,2.267e-1) || rho <= 1.0e-10){
        // coefficients for opacity laws 3, 4, 5, 6, 7, and 8 in T_4 units.
        real const bk[] { 2.e4, 1.e4, 1.5e10 };
        //   disjoint opacity laws for 5, 6, and 7.
        real o5 = bk[0]*ipow<2>(cbrt(rho))*ipow<3>(ts4);
        real o6 = bk[1]*cbrt(rho)*ipow<10>(ts4);
        real o7 = bk[2]*rho/(ipow<2>(ts4)*sqrt(ts4));
        //   parameters used for smoothing
        real o6an = ipow<2>(o6);
        real o7an = ipow<2>(o7);
        return pow((ipow<2>(o6an*o7an/(o6an+o7an))
                    +ipow<4>(o5/(1+ipow<10>(ts4/(1.1*pow(rho,0.04762)))))),
                   0.25);
      } else {
        if constexpr(debug::assertInKokkosInline) {
          assert(false);
        }
        // no scattering! (So undefined behavior is ok ?)
        return kkm::signaling_NaN_v<real>;
      }
    }
  }
  
  void
  DiskPhysic::Adiabatic::Radiative::kappa(arr3d<real const> temperature, arr3d<real const> density, arr3d<real> k) const {
    CodeUnits const& units = cfg.lock()->units;
    real const densityCGS     = units.density();
    real const distanceCGS    = units.distance();
    real const temperatureCGS = units.temperature();
    switch (opacity.type) {
    case OpacityLaw::CONSTANT:
      {
	real const kappa  = *opacity.config.value<real>("kappa");
	kk::deep_copy(k, kappa*densityCGS*distanceCGS+kkm::epsilon<real>::value);
      }
      break;
    case OpacityLaw::BELL_LIN:
      kk::parallel_for("Plank_opacity", range(k),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         k(i,h,j) = details::LinOpacity(temperature(i,h,j)*temperatureCGS, density(i,h,j)*densityCGS)*densityCGS*distanceCGS+1.0e-300;
                       });
      break;
    default:
      std::abort();
    }
  }    

  DiskPhysic::Adiabatic::Adiabatic(std::optional<Radiative> r, std::optional<Cooling> c, real i)
    : radiative(r), cooling(c), index(i), cfg() {}
                                   
  real
  DiskPhysic::Adiabatic::specificHeat() const {
    return R/cfg.lock()->meanMolecularWeight/(index-1);
  }
  
  void
  DiskPhysic::Adiabatic::dump(std::ostream& out, int tab) const {
    out << tabs(tab) <<  "Equation of State, adiabatic\n"
        << tabs(tab+1) << "* index: " << index  << '\n';
    if (radiative) {
      radiative->dump(out, tab+1);
    }
    if (cooling) {
      if (std::holds_alternative<BetaCooling>(*cooling)) {
	std::get<BetaCooling>(*cooling).dump(out, tab+1);
      } else if (std::holds_alternative<RadiativeCooling>(*cooling)) {
	std::get<RadiativeCooling>(*cooling).dump(out, tab+1);
      } else {
	std::abort();
      }
    }
  }

  void
  DiskPhysic::Adiabatic::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "index", index);
    if (grp.exist("full_energy")) {
      radiative = Radiative();
      radiative->readH5(grp.getGroup("full_energy"));
    }
    if (grp.exist(COOLING)) {
      HighFive::Group cgrp = grp.getGroup(COOLING);
      if (!cgrp.hasAttribute(TYPE)) {
	std::cerr << "Warning: no type attribute in cooling spec, going with "
		  << CoolingType::BETA << " but consider running utility disk_upgrade on the disk.\n";
      }
      CoolingType type = h5::attributeValue(cgrp, TYPE, CoolingType::BETA);
      switch(type) {
      case CoolingType::BETA:
	cooling = BetaCooling();
	std::get<BetaCooling>(*cooling).readH5(cgrp);
	break;
      case CoolingType::RADIATIVE:
	cooling = RadiativeCooling();
	std::get<RadiativeCooling>(*cooling).readH5(cgrp);
	break;
      default:
	std::abort();
      }
    }
  }
  
  void
  DiskPhysic::Adiabatic::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "index", index);
    if (radiative) {
      auto full = grp.createGroup("full_energy");
      radiative->writeH5(full);
    }
    if (cooling) {
      auto cool = grp.createGroup(COOLING);
      if (std::holds_alternative<BetaCooling>(*cooling)) {
	h5::writeAttribute(cool, TYPE, CoolingType::BETA);
	std::get<BetaCooling>(*cooling).writeH5(cool);
      } else if (std::holds_alternative<RadiativeCooling>(*cooling)) {
	h5::writeAttribute(cool, TYPE, CoolingType::RADIATIVE);
	std::get<RadiativeCooling>(*cooling).writeH5(cool);
      } else {
	std::abort();
      }
    }
  }

  void
  DiskPhysic::Adiabatic::Radiative::Solver::readH5(HighFive::Group const& radiative) {
    // the solver config is still inlined in the radiative config
    std::optional<std::string> solverName = h5::attributeValue<std::string>(radiative, "solver");
    label = solverName.value_or("legacy");
    if (!solverName) {
      std::cerr << "No solver configuration in radiative configuration. Using default. "
                << "Please consider running `disk_upgrade`.\n";
    }
    config = Config(radiative.getGroup("solver_configuration"));
  }
  
  void
  DiskPhysic::Adiabatic::Radiative::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "z_boundary_temperature", userZBoundaryTemperature);
    h5::readAttribute(grp, "opacity_law", opacity.type);
    if (opacity.type == OpacityLaw::CONSTANT) {
      real constKappa = *h5::attributeValue<real>(grp, "const_kappa");
      opacity.config = Config::Properties{{"kappa", std::to_string(constKappa)}};
    }
    solver.readH5(grp);
    h5::readAttribute(grp, "dust_to_gas", dustToGas);
    h5::readAttribute(grp, "energy_time_step", energyTimeStep);

    if (grp.exist("star")) {
      star = Star();
      star->readH5(grp.getGroup("star"));
    }
  }

  void
  DiskPhysic::Adiabatic::Radiative::Solver::writeH5(HighFive::Group& radiative) const {
    // the solver config is still inlined in the radiative config
    h5::writeAttribute(radiative, "solver", label);
    config.writeH5(radiative, "solver_configuration");
  }
  
  void
  DiskPhysic::Adiabatic::Radiative::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "z_boundary_temperature", userZBoundaryTemperature);
    h5::writeAttribute(grp, "opacity_law", opacity.type);
    if (opacity.type == OpacityLaw::CONSTANT) {
      h5::writeAttribute(grp, "const_kappa", *opacity.config.value<real>("kappa"));
    }
    solver.writeH5(grp);
    h5::writeAttribute(grp, "dust_to_gas", dustToGas);
    h5::writeAttribute(grp, "energy_time_step", *energyTimeStep);
    if (star) {
      auto sg = grp.createGroup("star");
      star->writeH5(sg);
    }
  }
  
  void
  DiskPhysic::Adiabatic::Radiative::Star::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "shadow_angle", shadowAngle);
  }

  void
  DiskPhysic::Adiabatic::Radiative::Star::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "shadow_angle", shadowAngle);
  }
  
  void
  DiskPhysic::Adiabatic::BetaCooling::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "time_scale", timeScale);
  }

  void
  DiskPhysic::Adiabatic::BetaCooling::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "time_scale", timeScale);
  }

  void
  DiskPhysic::Adiabatic::RadiativeCooling::readH5(HighFive::Group const& grp) {
  }

  void
  DiskPhysic::Adiabatic::RadiativeCooling::writeH5(HighFive::Group& grp) const {
  }
  
  void
  DiskPhysic::Density::dump(std::ostream& out, int tab) const {
    out << tabs(tab) <<  "Density\n"
        << tabs(tab+1) << "* slope: " << slope << '\n'
        << tabs(tab+1) << "* start: " << start << '\n'
        << tabs(tab+1) << "* minimum: " << minimum << '\n';
    cavity.dump(out, tab+1);
  }

  void
  DiskPhysic::Smoothing::dump(std::ostream& out, int tab) const {
    out << tabs(tab) <<  "Smoothing:\n"
        << tabs(tab+1) << "* change: " << yesno(change) << '\n'
        << tabs(tab+1) << "* flat (non cubic): " << yesno(flat) << '\n'
        << tabs(tab+1) << "* taper: " << taper << '\n'
        << tabs(tab+1) << "* size: " << size << '\n';
  }

  void
  DiskPhysic::Density::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "slope", slope);
    h5::readAttribute(grp, "start", start);
    h5::readAttribute(grp, "minimum", minimum);
    cavity.readH5(grp.getGroup("cavity"));
  }

  void
  DiskPhysic::Density::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "slope", slope);
    h5::writeAttribute(grp, "start", start);
    h5::writeAttribute(grp, "minimum", minimum);
    auto cavityGrp = grp.createGroup("cavity");
    cavity.writeH5(cavityGrp);
  }
 
  void
  DiskPhysic::Smoothing::readH5(HighFive::Group const& grp) {
    h5::readAttribute(grp, "change", change);
    h5::readAttribute(grp, "flat", flat);
    h5::readAttribute(grp, "taper", taper);
    h5::readAttribute(grp, "size", size);
  }    

  void
  DiskPhysic::Smoothing::writeH5(HighFive::Group& grp) const {
    h5::writeAttribute(grp, "change", change);
    h5::writeAttribute(grp, "flat", flat);
    h5::writeAttribute(grp, "taper", taper);
    h5::writeAttribute(grp, "size", size);
  }    
    
  void
  DiskPhysic::dump(std::ostream& out, int tab) const {
    out << tabs(tab) <<  "Physic:\n";
    out << tabs(tab+1) << "Shape:\n";
    shape.dump(out, tab+2);
    referential.dump(out, tab+1);
    out << tabs(tab) << "Boundary Conditions:\n";
    boundaries.dump(out, tab+1);
    out << tabs(tab) << "Initialization plugins:\n";
    init.dump(out, tab+1);    
    if(starAccretion){
      starAccretion->dump(out, tab+1);
    }
    viscosity.dump(out, tab+1);
    if (adiabatic) {
      adiabatic->dump(out, tab+1);
    }
    density.dump(out, tab+1);
    smoothing.dump(out, tab+1);
    out << tabs(tab+1) << "* flaringIndex: " << flaringIndex << '\n'
        << tabs(tab+1) << "* CFLSecurity: " << CFLSecurity << '\n'
        << tabs(tab+1) << "* draggingfCoef: " << draggingCoef << '\n'
        << tabs(tab+1) << "* transport: " << transport << '\n'
        << tabs(tab+1) << "* mean molecular weight : " << meanMolecularWeight << '\n';
    if (planetarySystem()) {
      planetarySystem()->dump(out, tab+1);
    } else {
      out << tabs(tab+1) << "No planetary system.\n";
    }
  }
  
  namespace detail {
    template <class C>
    void readSubCfg(HighFive::Group const& grp, std::string name, C& c) {
      c.readH5(grp.getGroup(name));
    }
    template <class C>
    void readSubCfg(HighFive::Group const& grp, std::string name, std::optional<C>& c) {
      if (grp.exist(name)) {
        c = C();
        c->readH5(grp.getGroup(name));
      }
    }
    void readSubCfg(HighFive::Group const& grp, std::string name, std::optional<PlanetarySystemPhysic>& c) {
      if (grp.exist(name)) {
        c = PlanetarySystemPhysic(grp.getGroup(name));
      }
    }
    void
    checkReferential(HighFive::Group const& grp) {
      if (!grp.exist("referential")) {
        std::cerr << "Could not find \"referential\" group in \"physic\" group in HDF5 file.\n"
                  << "Consider running \'disk_upgrade\' on your hdf5 file.\n"
                  << "See https://gitlab.oca.eu/DISC/fargOCA/wikis/User's-Guide/File-Format-Migration#hdf5-snapshot-updates for (a few) more details.\n";
        throw std::runtime_error("Misplaced referential group");
      }
    }
  }
  
  void
  DiskPhysic::readH5(HighFive::Group const& grp) {
    detail::readSubCfg(grp, "gas_shape", shape);
    detail::checkReferential(grp);
    detail::readSubCfg(grp, REFERENTIAL, referential);
    if (grp.exist(BOUNDARIES)) {
      boundaries = PluginConfig{grp.getGroup(BOUNDARIES)};
    }
    if (grp.exist(INIT)) {
      init = PluginConfig{grp.getGroup(INIT)};
    }
    if (grp.exist(STEPS)) {
      steps = PluginConfig{grp.getGroup(STEPS)};
    }    
    if (grp.exist("star_accretion")) {
      starAccretion = StarAccretion(grp.getGroup("star_accretion"));
    }
    detail::readSubCfg(grp, "viscosity", viscosity); 
    detail::readSubCfg(grp, ADIABATIC, adiabatic); 
    detail::readSubCfg(grp, "density", density); 
    detail::readSubCfg(grp, "smoothing", smoothing); 
    detail::readSubCfg(grp, PLANETARY_SYSTEM, myPlanetarySystem); 
    assert(!myPlanetarySystem || myPlanetarySystem->planets.size() > 0);
    if (referential.type == DiskReferential::COROTATING) {
      if (!referential.guidingPlanet) {
        if (bool(myPlanetarySystem) && myPlanetarySystem->planets.size() == 1 ) {
          std::string planet = myPlanetarySystem->planets.begin()->first;
          referential.guidingPlanet = planet;
          std::cerr << "Only " << planet << " was found, so we'll go with it as a guiding planet.\n";
        } else {
          std::cerr << "Not guiding planet specified and " << myPlanetarySystem->planets.size() << " stored. Can't gues, bye!\n";
          std::abort();
        }
      }
    }
    
    h5::readAttribute(grp, "flaring_index", flaringIndex);
    h5::readAttribute(grp, "CFL_security", CFLSecurity);
    h5::readAttribute(grp, "dragging_coef", draggingCoef);
    h5::readAttribute(grp, "hill_cut_factor", hillCutFactor);
    h5::readAttribute(grp, "transport", transport);
    if (grp.hasAttribute(MEAN_MOLECULAR_WEIGHT)) {
      h5::readAttribute(grp, MEAN_MOLECULAR_WEIGHT, meanMolecularWeight);
    } else {
      std::cerr << "No mean molecular weight found in physic. Will go with " << DEFAULT_MEAN_MOLECULAR_WEIGHT
                << " but please consider running disk_upgrade.\n";
      meanMolecularWeight =  DEFAULT_MEAN_MOLECULAR_WEIGHT;
    }
  }
  
  namespace detail {
    template<class C>
    void writeSubCfg(HighFive::Group& grp, std::string name, C const& c) {
      auto g = grp.createGroup(name);
      c.writeH5(g);
    }
    template<class C>
    void writeSubCfg(HighFive::Group& grp, std::string name, std::optional<C> const& c) {
      if (c) {
        writeSubCfg(grp, name, *c);
      }
    }
  }
  
  void
  DiskPhysic::writeH5(HighFive::Group grp) const {
    detail::writeSubCfg(grp, "gas_shape", shape);
    detail::writeSubCfg(grp, REFERENTIAL, referential);
    detail::writeSubCfg(grp, "boundaries", boundaries);
    detail::writeSubCfg(grp, INIT, init);
    detail::writeSubCfg(grp, STEPS, steps);
    detail::writeSubCfg(grp, "star_accretion", starAccretion);
    detail::writeSubCfg(grp, "viscosity", viscosity); 
    detail::writeSubCfg(grp, ADIABATIC, adiabatic); 
    detail::writeSubCfg(grp, "density", density); 
    detail::writeSubCfg(grp, "smoothing", smoothing); 
    detail::writeSubCfg(grp, PLANETARY_SYSTEM, myPlanetarySystem); 

    h5::writeAttribute(grp, "flaring_index", flaringIndex);
    h5::writeAttribute(grp, "CFL_security", CFLSecurity);
    h5::writeAttribute(grp, "dragging_coef", draggingCoef);
    h5::writeAttribute(grp, "hill_cut_factor", hillCutFactor);
    h5::writeAttribute(grp, "transport", transport);
    h5::writeAttribute(grp, MEAN_MOLECULAR_WEIGHT, meanMolecularWeight);
  }

  real
  DiskPhysic::Smoothing::smooth(real smoothingLengthUnit, real physicalTime) const  {
    if (change) {
      real smoothTaper    = physicalTime/(taper*2*PI);
      real maxSmoothTaper = smoothTaper > 1 ? 1 : ipow<2>(std::sin(smoothTaper*PI/2));
      return (1-maxSmoothTaper/2)*size*smoothingLengthUnit;
    }  else{
      return smoothingLengthUnit*size;
    }
  }

  bool
  DiskPhysic::check(std::ostream& log) const {    
    int failed = 0;
    if (!DiskInitPlugin::check(init, log)) { ++failed; }

    {
      std::ostringstream errs;
      auto const& vchecker = ViscosityFieldPlugin::get(viscosity.type).configChecker();
      if (!vchecker(viscosity.config, errs)) {
	log << "Problem with viscosity specification '" << viscosity.type << "': " 
	    << errs.str() << '\n';
	++failed;
      }
    }
    return failed == 0;
  }

  
}
