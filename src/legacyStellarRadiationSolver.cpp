// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <utility>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include <boost/algorithm/string.hpp>

#include "allmpi.hpp"

#include "stellarRadiationSolver.hpp"

#include "fundamental.hpp"
#include "codeUnits.hpp"
#include "planetarySystem.hpp"
#include "fundamental.hpp"
#include "viscosityTensor.hpp"
#include "gridCellSizes.hpp"
#include "loops.hpp"

#include "cuda_workarounds.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace kkm = Kokkos::Experimental;

  using kk::sin;
  using kk::cos;
  using kk::pow;

#define LBD KOKKOS_LAMBDA
#ifdef NDEBUG
#  define NLBD [&]
#else
#  define NLBD [=]
#endif

  static std::string DERIVATIVE_H5_NAME = "radiative_energy_derivative";
  
  /// \brief Compute the star radiation effect on the disk.
  /// The used solver is not as parallel as it could be.
  template<GridSpacing GS>
  class LegacySolver:
    public StellarRadiationSolver {
  public:
    LegacySolver(Disk& disk);
    LegacySolver(Disk& disk, HighFive::Group grp)
    : StellarRadiationSolver(disk),
      derivative(DERIVATIVE_H5_NAME, disk.dispatch())
    {
      h5::loadDispatched(derivative, grp, DERIVATIVE_H5_NAME);
    }
    
    ~LegacySolver();
    
    virtual void advance(real dt) override;

    virtual void checkpoint(std::optional<HighFive::Group> group, int where) const override {
      std::optional<HighFive::Group> subGroup;
      if (group) {
        auto root = group->getGroup("/");
        subGroup  = h5::createSubGroup(root, DERIVATIVE_H5_NAME);
      }
      derivative.writeH5(subGroup, where);
    }
    virtual void importGas(StellarRadiationSolver const& solver) override {
      LegacySolver const* other = dynamic_cast<LegacySolver const*>(&solver);
      if (other) {
        kk::deep_copy(derivative.data(), other->derivative.data());
      } else {
        kk::deep_copy(derivative.data(), real(0));
      }
    }
    virtual void rebind(GridDispatch const& next, ScalarFieldInterpolators const& interpolators) override {
      derivative.rebind(next, interpolators);
    }
    virtual void collectFields(Disk::NamedFields::Map& map) const override {
      typedef Disk::NamedFields::Map::value_type pair;
      map.insert(pair(DERIVATIVE_H5_NAME, derivative));
    }
  cuda_private:
    // to make brain dead CUDA happy
    void cudaCtor();

  private:
    ScalarField derivative;
  };
  
  template<GridSpacing GS>
  LegacySolver<GS>::~LegacySolver() {} // for vtable...
 
  namespace {
    template<GridSpacing GS>
    struct Diffusion {
    public:
      Diffusion(Disk const& disk);
      Diffusion(Diffusion<GS> const& diff) = delete;
      ~Diffusion() { }

      Disk const& disk() const { return myDisk; }
      
      real simpleOpacity() const {
        CodeUnits const& units = disk().physic().units;
        return  disk().physic().adiabatic->radiative->dustToGas*100*3.5*units.density()*units.distance(); 
      }
      void computeOpticalDepth(Disk const& disk, arr3d<real> depth) const;
      arr3d<real const> stellarRadiation() const { return views.template get<STELLAR_RADIATION>(); }
      arr3d<real const> RosselandOpacity() const { return views.template get<ROSSELAND_OPACITY>(); }
      
    private:
      Disk const& myDisk;
      enum { STELLAR_RADIATION,
             ROSSELAND_OPACITY,
             LOCAL_SIZE };
      LocalFieldStack3D<real, LOCAL_SIZE> views;
      
      // scoped variable only
      static void *operator new     (size_t) = delete;
      static void *operator new[]   (size_t) = delete;
      static void  operator delete  (void*)  = delete;
      static void  operator delete[](void*)  = delete;
      
    public: // for cuda
      void initStellarHeating(Disk const& disk);
    };
    
    /// \brief According to Eq.B6 in Bitsch et al. 2013, A&A, 549, A124
    template<GridSpacing GS>
    class Eta {
    public:
      Eta(Disk const& disk, Diffusion<GS> const& diffusion, arr3d<real const> planetRadiation, real dt)
        : myDisk(disk), 
          views(disk.dispatch().temporaries3D<real>()) {
        cudaCtor(diffusion, planetRadiation, dt);
      }
      Eta(Eta<GS> const& eta) = delete;

      // Eta has 2 components, called eta1 and eta2...
      template<int IDX> arr3d<real>       get()       { return views.get<IDX-1>(); }
      template<int IDX> arr3d<real const> get() const { return views.get<IDX-1>(); }
      
      Disk const& disk() const { return myDisk; }

    cuda_private:
      void cudaCtor(Diffusion<GS> const& diffusion, arr3d<real const> planetRadiation, real dt);
      
    private:
      Disk const& myDisk;
      LocalFieldStack3D<real, 2> views;

    };
    
    template<GridSpacing GS>
    struct Matrix {
    public:
      Matrix(Disk const& disk, Diffusion<GS> const& ctx, Eta<GS> const& eta, real dt);
      Matrix(Matrix<GS> const& other) = delete;
      
      void solve(ScalarField& radiativeEnergyHint, real dt) const;
      
      Disk const& disk() const { return myDisk; }

    cuda_private:
      void cudaCtor(Diffusion<GS> const& ctx, Eta<GS> const& eta, real dt);

    private:
      Disk const& myDisk;
      enum : int { LOW_R, LOW_P, LOW_T, DIAG, UP_T, UP_P, UP_R, RHS, NB_FIELDS };
      LocalFieldStack3D<real, NB_FIELDS> views;
    };
    
    template<GridSpacing GS>
    arr3d<real> planetsRadiation(Disk const& disk);
  
    template<GridSpacing GS>
    Diffusion<GS>::Diffusion(Disk const& disk) 
      :  myDisk(disk), 
         views(disk.dispatch().temporaries3D<real>()) {
      disk.physic().adiabatic->radiative->kappa(disk.temperature()->data(),
                                                disk.density().data(),
                                                views.template init<ROSSELAND_OPACITY>());
      initStellarHeating(disk);
    }
    
    template<GridSpacing GS>
    void
    Diffusion<GS>::computeOpticalDepth(Disk const& disk, arr3d<real> depth)  const
    {
      GridDispatch const& dispatch = disk.dispatch();
      fmpi::communicator const& world = dispatch.comm();
      auto const& coords = dispatch.grid().as<GS>();
      auto gsz = coords.sizes();
      
      auto radii = coords.radii();
      
      arr3d<real const> dens = disk.density().data();
      /// to avoid implicit this capture warning by cuda:
      real const opacity = simpleOpacity();

      bool innerRing = dispatch.first();
      
      arr2d<real> outFlow("proc_out_flow", gsz.ni, gsz.ns);
      arr3d<real> outFlows("procs_out_flow", dispatch.comm().size(), gsz.ni, gsz.ns);
      
      int ghostSize = dispatch.ghostSize();
      kk::parallel_for(kk::TeamPolicy<>(gsz.ni, kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const h = team.league_rank();
                         kk::parallel_for(kk::TeamThreadRange(team, gsz.ns),
                                          NLBD(size_t const j) {
                                            kk::parallel_scan(kk::ThreadVectorRange(team, gsz.nr),
                                                              NLBD(int i, real& result, bool const& last) {
                                                                if (i == 0) {
                                                                  if (innerRing) {
                                                                    // double it to account for star to inner ring absortion
                                                                    result = 2*dens(i,h,j)*opacity*(radii(i+1)-radii(i));
                                                                  } else {
                                                                    result = 0;
                                                                  } 
                                                                } else {
                                                                  // accumulate optical depth for each layer
                                                                  result += dens(i,h,j)*opacity*(radii(i+1)-radii(i));
                                                                }
                                                                if (last) {
                                                                  depth(i,h,j) = result;
                                                                }
                                                              });
                                          });
                       });
      kk::deep_copy(outFlow, ring(depth,gsz.nr-2*ghostSize));
      // gather out going flows in a world.size grid, out goin for proc N is at radius N:
      if (mpiAddressableView(outFlow)) {
          fmpi::all_gather(world, outFlow.data(), outFlow.size(), outFlows.data());
      } else {
        auto hostFlow  = kk::create_mirror_view_and_copy(kk::HostSpace{}, outFlow);
        auto hostFlows = kk::create_mirror_view(kk::HostSpace{}, outFlows);
        fmpi::all_gather(world, hostFlow.data(), hostFlow.size(), hostFlows.data());
        kk::deep_copy(outFlows, hostFlows);
      }

      int rank = dispatch.comm().rank();
      // Sum all incomming flow (between 0 and rank) into a single layer(gsz.ni,gsz.ns)
      kk::parallel_for(kk::TeamPolicy<>(gsz.ni,kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const h = team.league_rank();
                         kk::parallel_for(kk::TeamThreadRange(team, gsz.ns),
                                          NLBD(size_t const j) {
                                            kk::parallel_reduce(kk::ThreadVectorRange(team, rank), 
                                                                NLBD(size_t const k, real& tmp) {
                                                                  tmp += outFlows(k,h,j);
                                                                }, outFlow(h,j));
                                          });
                       });
      // and add it over nr
      kk::parallel_for(fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         depth(i,h,j) += outFlow(h,j);
                       });
    }
    
    template<GridSpacing GS>
    void
    Diffusion<GS>::initStellarHeating(Disk const& disk) {
      GridDispatch const& dispatch = disk.dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      
      auto radii  = coords.radii();
      auto phiMed = coords.phiMed();
      arr3d<real const> dens             = disk.density().data();
      arr3d<real>       stellarRadiation = views.template init<STELLAR_RADIATION>();

      real const opacity = simpleOpacity();

      LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
      computeOpticalDepth(disk, locals.get<0>());
      arr3d<real const> tau = locals.get<0>();
      
      real const fStar = (bool(disk.physic().adiabatic->radiative->star) 
                          ? disk.physic().adiabatic->radiative->star->fStar()
                          : 0);
      if (fStar > 0) { // optimisation, fStar == 0 would not do anything
        real const shadowAngle = disk.physic().adiabatic->radiative->star->shadowAngle;
        kk::parallel_for("init_stell_heating", fullRange(coords),
                         LBD(int i, int h, int j) {
                           real const deltar   = (radii(i+1)-radii(i));
                           real const fstarvol = fStar/(radii(i)*radii(i)+radii(i)*deltar+deltar*deltar/3);
                           if (i > 0 ) {
                             stellarRadiation(i,h,j) = ((phiMed(h)/PI*180.0) <= (90-shadowAngle) || (phiMed(h)/PI*180.0) >= (90+shadowAngle)
                                                        ? (dens(i,h,j)*opacity*deltar < 1
                                                           ? fstarvol*exp(-tau(i-1,h,j))*dens(i,h,j)*opacity
                                                           : fstarvol*exp(-tau(i-1,h,j))*(1-exp(-dens(i,h,j)*opacity*deltar))/deltar)
                                                        : real(0));
                           } else {
                             stellarRadiation(i,h,j) = 0;
                           }
                         });
      } else {
        kk::deep_copy(stellarRadiation, 0);
      }
    }
    template<GridSpacing GS>
    void
    Eta<GS>::cudaCtor(Diffusion<GS> const& diffusion, arr3d<real const> planetRadiation, real dt) {
      arr3d<real const> density     = disk().density().data();
      arr3d<real const> temperature = disk().temperature()->data();
      arr3d<real const> stellarRadiation = diffusion.stellarRadiation();

      arr3d<real> eta1 = get<1>();
      arr3d<real> eta2 = get<2>();

      LocalFieldStack3D<real,1> locals(disk().dispatch().template temporaries3D<real>());

      ViscosityTensor::computeQPlus(disk(), locals.get<0>());
      arr3d<real const> qplus            = locals.get<0>();
      arr3d<real const> RosselandOpacity = diffusion.RosselandOpacity();
      real const specificHeat = disk().physic().adiabatic->specificHeat();
      real const dustToGas    = disk().physic().adiabatic->radiative->dustToGas;
      real const StefanBoltzmann = disk().physic().units.StefanBoltzmannCst();
      real const lightSpeed      = disk().physic().units.lightSpeed();
      
      kk::parallel_for("init_stell_heating", fullRange(disk().dispatch().grid()),
                       LBD(int i, int h, int j) {
                         real const PlankOpacity = dustToGas*100*RosselandOpacity(i,h,j);
                         real const denom = 1+16*dt*PlankOpacity*StefanBoltzmann*ipow<3>(temperature(i,h,j))/specificHeat;
                         eta1(i,h,j) = (temperature(i,h,j)
                                        + dt*(12*PlankOpacity/specificHeat*StefanBoltzmann*ipow<4>(temperature(i,h,j))
                                              + (stellarRadiation(i,h,j)+qplus(i,h,j)+planetRadiation(i,h,j))/(density(i,h,j)*specificHeat))) / denom;
                         eta2(i,h,j) = dt*lightSpeed*PlankOpacity/(specificHeat*denom);
                       });
    }
    
    namespace {
      struct PlanetData {
        Triplet position;
        real    solidAccretion;
      };
      /// Retrieve the input data for the planet radiation computation in a Kokkos friendly format
      arr1d<PlanetData>
      collectPlanetRadiationComputationInputs(Disk const& disk) {
        assert(disk.system());
        DiskPhysic const& physic = disk.physic();
        harr1d<PlanetData> hPlanets("planets data", disk.system()->planets().size());
        int i = 0;
        for (auto const& [name, cfg] : physic.planetarySystem()->planets) {
          if (cfg.luminosity) {
            hPlanets(i++) = { disk.system()->planet(name).position(), *cfg.solidAccretion(physic.units) };
          }
        }
        resize(hPlanets, i);
        arr1d<PlanetData> planets("planets data", hPlanets.size());
        kk::deep_copy(planets, hPlanets);
        return planets;
      }
    }
    
    template<GridSpacing GS>
    arr3d<real>
    planetsRadiation(Disk const& disk) {
      GridDispatch const& dispatch = disk.dispatch();
      arr3d<real> planetRadiation = scalarView("planet_radiation", dispatch.grid());
      
      if (disk.system()) {
        bool halfDisk = disk.physic().shape.half;
        arr3d<real const> cellVolumes = dispatch.cellSizes().volume();
        arr1d<PlanetData> planets = collectPlanetRadiationComputationInputs(disk);
        auto const& coords = dispatch.grid().as<GS>();
        GridSizes const gsz = coords.sizes();
        
        auto radii = coords.radii();        
        auto theta = coords.theta();
        arr1d<int>        planetsSector("planet sector", planets.size());
        kk::parallel_for(kk::TeamPolicy<>(planets.size(), kk::AUTO),
                         LBD(kk::TeamPolicy<>::member_type const& team) {
                           size_t const p = team.league_rank();
                           PlanetData const& planet = planets(p);
                           
                           Triplet const pos  = planet.position;
                           real const rplanet = pos.norm();
                           real const thpl    = atan2(pos.y, pos.x);
                           kk::parallel_for(kk::TeamVectorRange(team, gsz.ns),
                                            NLBD(size_t const j) {
                                              if (theta(j) < thpl && thpl <= theta(j+1)) {
                                                planetsSector(p) = j;
                                              }
                                            });
                           kk::parallel_for(kk::TeamThreadRange(team, gsz.nr-1),
                                            NLBD(size_t const i) {
                                              if(radii(i) <= rplanet && rplanet < radii(i+1)) {
                                                if(halfDisk){
                                                  size_t const h = gsz.ni-2;  // the planet is at  midplane
                                                  size_t const j = planetsSector(p);
                                                  real const accretionContribution = planet.solidAccretion/(8*cellVolumes(i,h,j));
                                                  planetRadiation(i,  h,j)   = accretionContribution;
                                                  planetRadiation(i,  h,j-1) = accretionContribution;
                                                  planetRadiation(i-1,h,j)   = accretionContribution;
                                                  planetRadiation(i-1,h,j-1) = accretionContribution;
                                                } else {
                                                  size_t const h = gsz.ni/2; // the planet is at  midplane
                                                  size_t const j = planetsSector(p);
                                                  real const accretionContribution = planet.solidAccretion/(8*cellVolumes(i,h,j));
                                                  planetRadiation(i,  h,  j)   = accretionContribution;
                                                  planetRadiation(i,  h,  j-1) = accretionContribution;
                                                  planetRadiation(i-1,h,  j)   = accretionContribution;
                                                  planetRadiation(i-1,h,  j-1) = accretionContribution;
                                                  planetRadiation(i,  h,  j+1) = accretionContribution;
                                                  planetRadiation(i,  h+1,j-1) = accretionContribution;
                                                  planetRadiation(i-1,h+1,j)   = accretionContribution;
                                                  planetRadiation(i-1,h+1,j-1) = accretionContribution;
                                                }
                                              }
                                            });
                         });
      }
      return planetRadiation;
    }

    template<GridSpacing GS>
    Matrix<GS>::Matrix(Disk const& disk, Diffusion<GS> const& ctx, Eta<GS> const& eta, real dt)
      : myDisk(disk),
        views(disk.dispatch().temporaries3D<real>())
    {
      cudaCtor(ctx,eta,dt);
    }

    template<GridSpacing GS>    
    void
    Matrix<GS>::cudaCtor(Diffusion<GS> const& diffusion, Eta<GS> const& eta, real dt) {
      assert(disk().checkPhysicalQuantities());
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();
      
      arr3d<real const> density           = disk().density().data();
      arr3d<real const> temperature       = disk().temperature()->data();
      arr3d<real const> radiativeEnergy   = disk().radiativeEnergy()->data();
      arr3d<real const> eta1              = eta.template get<1>();
      arr3d<real const> eta2              = eta.template get<2>();
      arr3d<real const> RosselandOpacity  = diffusion.RosselandOpacity();

      LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
      arr3d<real> diffcoeff = locals.get<0>();
      
      auto radMed   = coords.radiiMed();
      auto phiMed   = coords.phiMed();
      auto dPhiMed  = coords.dPhiMed();
      auto radii    = coords.radii();
      auto phi      = coords.phi();
      auto thetaMed = coords.thetaMed();
      
      real const zBorderEnergy = disk().physic().adiabatic->radiative->zBoundaryEnergy();
      bool const halfDisk   = disk().physic().shape.half;
      real const lightSpeed      = disk().physic().units.lightSpeed();

      using kk::abs;
      bool const hasInnerRing = dispatch.first();
      bool const hasOuterRing = dispatch.last();

      { // diffusion coefficient

	auto inner = LBD(size_t const i, size_t const h, size_t const j) -> real {
	  KOKKOS_ASSERT((i>0 && i<gsz.nr-1) && (h>0 && h<gsz.ni-1));
	  real const sinPhiMed   = sin(phiMed(h));
	  size_t const jm = nsprev(gsz.ns,j);
	  size_t const jp = nsnext(gsz.ns,j);
	  Triplet const grader((radiativeEnergy(i+1,h,j)-radiativeEnergy(i-1,h,j))/(radMed(i+1)-radMed(i-1)),
			       (radiativeEnergy(i,h,jp)-radiativeEnergy(i,h,jm))/(radMed(i)*sinPhiMed*(thetaMed(jp)-thetaMed(jm))),
			       (radiativeEnergy(i,h+1,j)-radiativeEnergy(i,h-1,j))/(radMed(i)*(phiMed(h+1)-phiMed(h-1))));
	  
	  real const coeff  = 1/(RosselandOpacity(i,h,j)*density(i,h,j));
	  real const erre   = coeff*grader.norm()/radiativeEnergy(i,h,j);
	  real const lambda = (erre <= 2
			       ? 2/(3+sqrt(9 +10*erre*erre))
			       : 10/(10*erre+9+sqrt(81+180*erre)));
	  return lambda*coeff*lightSpeed;
	};
	
	auto globalRadialBorder = LBD(size_t const i, size_t const h, size_t const j) -> real {
	  KOKKOS_ASSERT(h>0 && h<gsz.ni-1);
	  KOKKOS_ASSERT((i == 0 && hasInnerRing) || (i == gsz.nr-1 && hasOuterRing));
	  KOKKOS_ASSERT(radiativeEnergy(i,h,j) > 0);
	  
	  real const sinPhiMed = sin(phiMed(h));
	  size_t const jm = nsprev(gsz.ns,j);
	  size_t const jp = nsnext(gsz.ns,j);
	  real const coeff = 1/(RosselandOpacity(i,h,j)*density(i,h,j));
	  Triplet const grader(0,
			       (radiativeEnergy(i,h,jp)-radiativeEnergy(i,h,jm))/(radMed(i)*sinPhiMed*(thetaMed(jp)-thetaMed(jm))),
			       (radiativeEnergy(i,h+1,j)-radiativeEnergy(i,h-1,j))/(radMed(i)*(phiMed(h+1)-phiMed(h-1))));
	  real const erre = coeff*grader.norm()/radiativeEnergy(i,h,j);
	  KOKKOS_ASSERT(erre>=0);
	  real const lambda = ( erre <= 2
				? 2/(3+sqrt(9 +10*erre*erre))
				: 10/(10*erre+9+sqrt(81+180*erre)));
	  return lambda*coeff*lightSpeed;
	};
	
	auto radialBorder = LBD(size_t const i, size_t const h, size_t const j) -> real {
	  KOKKOS_ASSERT(i == 0 || i == gsz.nr-1);
	  KOKKOS_ASSERT(h>0 && h<gsz.ni-1);
	  if (i == 0) {
	    if (hasInnerRing) {
	      return globalRadialBorder(i,h,j);
	    } else {
	      return inner(i+1,h,j);
	    }
	  } else {
	    if (hasOuterRing) {
	      return globalRadialBorder(i,h,j);
	    } else {
	      return inner(i-1,h,j);
	    }
	  }
	};
	
	auto layerBorder = LBD(size_t const i, size_t const h, size_t const j) -> real {
	  KOKKOS_ASSERT(i>0 && i<gsz.nr-1);
	  KOKKOS_ASSERT(h==0 || h==gsz.ni-1);
	  size_t const h2 = h == 0 ? h+1 : h-1;
	  return inner(i,h2,j);
	};
	
	auto corner = LBD(size_t const i, size_t const h, size_t const j) -> real {
	  KOKKOS_ASSERT(i == 0 || i == gsz.nr-1);
	  KOKKOS_ASSERT(h == 0 || h == gsz.ni-1);
	  size_t h2 = h == 0 ? h+1 : h-1;	
	  return radialBorder(i,h2,j);
	};
	
	auto diffCoeffValue = LBD(size_t const i, size_t const h, size_t const j) -> real {
	  if (radiativeEnergy(i,h,j) > 0) {
	    if ((i>0 && i<gsz.nr-1) && (h>0 && h<gsz.ni-1)) {
	      return inner(i,h,j);
	    } else {
	      // deal with the borders
	      if (i>0 && i<gsz.nr-1) {
		KOKKOS_ASSERT(h == 0 || h == gsz.ni-1);
		if (h == gsz.ni-1 && halfDisk) {
		  return inner(i,h-1,j);
		} else {
		  return layerBorder(i,h,j);
		}
	      } else if (h>0 && h<gsz.ni-1) {
		return radialBorder(i,h,j);
	      } else {
		return corner(i,h,j);
	      }
	    }
	  } else {
	    return 0;
	  }
	};
	
	kk::parallel_for(fullRange(coords),
			 LBD(size_t const i, size_t const h, size_t const j) {
			   diffcoeff(i,h,j) = diffCoeffValue(i,h,j);
			 });
      }
      
      auto dtheta = dispatch.cellSizes().dTheta();
      arr3d<real> aar  = views.template get<LOW_R>();
      arr3d<real> aat  = views.template get<LOW_T>();
      arr3d<real> aap  = views.template get<LOW_P>();
      arr3d<real> ccr  = views.template get<UP_R>();
      arr3d<real> cct  = views.template get<UP_T>();
      arr3d<real> ccp  = views.template get<UP_P>();
      arr3d<real> diag = views.template get<DIAG>();
      arr3d<real> rhs  = views.template get<RHS>();

      real const dustToGas   = disk().physic().adiabatic->radiative->dustToGas;
      real const StefanBoltzmann = disk().physic().units.StefanBoltzmannCst();
      
      kk::parallel_for("matrix initialization", fullRange(coords),
                       LBD(size_t const i,  size_t const h, size_t const j) 
                       {
			 real const radii2        = ipow<2>(radii(i));
			 real const radSup2       = ipow<2>(radii(i+1));
			 real const invRadMed     = 1/radMed(i);
			 real const invDiffRad    = 1/(radii(i+1)-radii(i));
			 real const invDiffRadMed = 1/(radMed(i)-radMed(i-1));
                         
			 size_t const jm = nsprev(gsz.ns,j);
			 size_t const jp = nsnext(gsz.ns,j);
			 
			 real const invDPhiMed  = 1/(radMed(i)*(phiMed(h+1)-phiMed(h)));
			 real const invDPhiMedP = 1/(radMed(i)*(phiMed(h+2)-phiMed(h+1)));
			 real const denom = 0.5*dt;
			 real const invdtheta2 = real(1)/ipow<2>(dtheta(i,h,j));
                         
			 if (i < gsz.nr-1) {
			   ccr(i,h,j) = -(dt/2)*(diffcoeff(i+1,h,j)+diffcoeff(i,h,j))*radSup2*invDiffRad*invDiffRadMed*invRadMed*invRadMed;
			 } else {
			   ccr(i,h,j) = -dt*(diffcoeff(i,h,j))*radSup2*invDiffRad*invDiffRadMed*invRadMed*invRadMed;
			 }
			 if(i > 0) {
			   aar(i,h,j) = -(dt/2)*(diffcoeff(i,h,j)+diffcoeff(i-1,h,j))*radii2*invDiffRad*invDiffRadMed*invRadMed*invRadMed;
			 } else {
			   aar(i,h,j) = -dt*(diffcoeff(i,h,j))*radii2*invDiffRad*invDiffRadMed*invRadMed*invRadMed;
			 }
			 cct(i,h,j) = -denom*(diffcoeff(i,h,j)+diffcoeff(i,h,jp))*invdtheta2;
			 aat(i,h,j) = -denom*(diffcoeff(i,h,j)+diffcoeff(i,h,jm))*invdtheta2;
			 if (h < gsz.ni-1) {
			   ccp(i,h,j) = -(dt/2)*(diffcoeff(i,h+1,j)+diffcoeff(i,h,j))*invDPhiMedP*sin(phi(h+1))/(cos(phi(h))-cos(phi(h+1)))/radMed(i);
			 } else {
			   ccp(i,h,j) = -dt*(diffcoeff(i,h,j))*invDPhiMedP*sin(phi(h+1))/(cos(phi(h))-cos(phi(h+1)))/radMed(i);
			 }
			 if (h>0) {
			   aap(i,h,j) = -(dt/2)*(diffcoeff(i,h,j)+diffcoeff(i,h-1,j))*invDPhiMed *sin(phi(h))/(cos(phi(h))-cos(phi(h+1)))/radMed(i);
			 } else {
			   aap(i,h,j) = -dt*(diffcoeff(i,h,j))*invDPhiMed *sin(phi(h))/(cos(phi(h))-cos(phi(h+1)))/radMed(i);
			 }
			 real const temperature3 = ipow<3>(temperature(i,h,j));
			 real const PlankOpacity = dustToGas*100*RosselandOpacity(i,h,j);
                         
			 diag(i,h,j)  = (1
					 +dt*density(i,h,j)*PlankOpacity*(lightSpeed-16.0*StefanBoltzmann*temperature3*eta2(i,h,j))
					 -aar(i,h,j)-ccr(i,h,j)-aat(i,h,j)-cct(i,h,j)-aap(i,h,j)-ccp(i,h,j));
			 if (hasInnerRing && i == 1)        { diag(i,h,j) += aar(i,h,j); }
			 if (hasOuterRing && i == gsz.nr-2) { diag(i,h,j) += ccr(i,h,j); }
			 if (halfDisk     && h == gsz.ni-2) { diag(i,h,j) += ccp(i,h,j); }
			 rhs(i,h,j) = radiativeEnergy(i,h,j)+4*dt*density(i,h,j)*PlankOpacity*StefanBoltzmann*temperature3*(4*eta1(i,h,j)-3*temperature(i,h,j));
                       });
    }

    template<GridSpacing GS>
    void
    Matrix<GS>::solve(ScalarField& radiativeEnergyField, real /* dt */) const
    {
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();
      
      bool const halfDisk = disk().physic().shape.half;
      
      arr3d<real>       radiativeEnergy = radiativeEnergyField.data();
      
      arr3d<real const> aar  = views.template get<LOW_R>();
      arr3d<real const> aat  = views.template get<LOW_T>();
      arr3d<real const> aap  = views.template get<LOW_P>();
      arr3d<real const> ccr  = views.template get<UP_R>();
      arr3d<real const> cct  = views.template get<UP_T>();
      arr3d<real const> ccp  = views.template get<UP_P>();
      arr3d<real const> diag = views.template get<DIAG>();
      arr3d<real const> rhs  = views.template get<RHS>();
      
      bool const hasInnerRing = dispatch.first();
      bool const hasOuterRing = dispatch.last();
      
      // Compute the norm of rhs and distribute the result back to all processes 
      real anormrhs = 0;
      {
        auto mngrhs = dispatch.managedView(rhs);
        kk::parallel_reduce("local_norm0", range(mngrhs),
                            LBD(int i, int h, int j, real& tmp) { tmp += mngrhs(i,h,j); }, anormrhs);
        anormrhs = fmpi::all_reduce(dispatch.comm(), anormrhs, std::plus<real>());
      }
      real anorm = anormrhs;
      int count = 0;
      Config config = disk().physic().adiabatic->radiative->solver.config;
      int  const maxCount         = config.value("max_iterations", 50000);
      real const epsilon          = config.value("epsilon", real(5.0e-7));
      int  const refreshNormEvery = config.value("halting_test_rate", 10);
      bool const verbose          = config.value("verbose", false);
      std::optional<int const>  residualDumpIndex = config.value<int>("residual_dump_index");
      LocalFieldStack3D<real,2> locals(dispatch.temporaries3D<real>());
      arr3d<real> resid = locals.get<0>();
      arr3d<real> radiativeEnergyOld = locals.get<1>();
      real const omega = 1.8;          

      while(anorm > epsilon*anormrhs && count < maxCount){
        ++count;
        // We alternate the processing of even an odd radii, 
        // and all processes need to be "synchronized" in order
        // to be independent of the number of MPI processes
        bool doEvenRadii = dispatch.radiiIndexRange().first%2 == 0;
        kk::deep_copy(resid, real(0));
        bool const refreshNorm = count % refreshNormEvery == 0 || count > maxCount;
        for (int m=0; m<2; m++){
          doEvenRadii = !doEvenRadii;
          // we skip indexes 0 and nr-1 
          int nbEvenRadii =  gsz.nr/2 - 1; 
          int nbOddRadii  =  gsz.nr%2 == 0 ? nbEvenRadii : nbEvenRadii + 1;
          kk::deep_copy(radiativeEnergyOld, radiativeEnergy);
	  for ( size_t h = 0; h < gsz.ni-1; ++h) {
	    size_t const half_nr = doEvenRadii ? nbEvenRadii : nbOddRadii;
	    kk::parallel_for("Solver_loop", range2D({half_nr, gsz.ns}),
			     LBD(size_t const i2, size_t const j) {
			       size_t const i = (doEvenRadii ? 2 : 1) + 2*i2;
			       if (h == 0) {
				 resid(i,h,j) = 0;
			       } else {
				 size_t const jm = nsprev(gsz.ns,j);
				 size_t const jp = nsnext(gsz.ns,j);
				 real const scale = omega/diag(i,h,j);
				 real residual = scale*(+ aat(i,h,j)*radiativeEnergyOld(i,h,jm)
							+ cct(i,h,j)*radiativeEnergyOld(i,h,jp)
							+ diag(i,h,j)*radiativeEnergyOld(i,h,j)
							+ aap(i,h,j)*(radiativeEnergyOld(i,h-1,j)
                                                                      -resid(i,h-1,j))
							- rhs(i,h,j));
				 if (!(hasInnerRing && i == 1)) {
				   residual += scale*aar(i,h,j)*radiativeEnergyOld(i-1,h,j);
				 }
				 if (!(hasOuterRing && i == gsz.nr-2)) {
				   residual += scale*ccr(i,h,j)*radiativeEnergyOld(i+1,h,j);
				 }
				 if (!(halfDisk && h == gsz.ni-2)) {
				   residual += scale*ccp(i,h,j)*radiativeEnergyOld(i,h+1,j);
				 }
				 resid(i,h,j) = residual;
			       }
			       radiativeEnergy(i,h,j) = radiativeEnergyOld(i,h,j)-resid(i,h,j);
			     });;
	  }
        }
        radiativeEnergyField.setGhosts();
        if (refreshNorm) {
          arr3d<real const> mResid = dispatch.managedView(resid);
          arr3d<real const> mDiag    = dispatch.managedView(diag);
          real lenergy = 0;
          kk::parallel_reduce("local_norm", range(mResid),
                              LBD(size_t const i, size_t const h, size_t const j, real& tmp) {
                                tmp += kk::abs(mResid(i,h,j)*mDiag(i,h,j)/omega);
                              }, lenergy);
          anorm = fmpi::all_reduce(dispatch.comm(), lenergy, std::plus<real>());
          if (verbose) {
            dispatch.log() << "Solver: count=" << count
                           << ", norm=" << anorm << ", "
                           << epsilon << "*rhs=" << anormrhs << '\n';
          }
        }
	if (residualDumpIndex && (*residualDumpIndex == count || *residualDumpIndex == count-1) ) {
	  namespace HF = HighFive;
	  std::ostringstream filename;
	  filename << "resid" << count << ".h5";
	  HF::File ofile(filename.str(), HF::File::Overwrite);
	  auto g = ofile.createGroup("resid");
	  h5::writePolarGrid(g, resid, GridPosition::INF);
	}
      }
    }
  }

  template<GridSpacing GS>  void
  LegacySolver<GS>::cudaCtor() {
    real const dt = disk().convergingTimeStep();
    GridDispatch const& dispatch = disk().dispatch();
    Diffusion<GS> diffusion(disk());
    Eta<GS> eta(disk(), diffusion, planetsRadiation<GS>(disk()), dt);
    auto const& coords = dispatch.grid().as<GS>();
    GridSizes const gsz = coords.sizes();
    
    arr3d<real const> temper    = disk().temperature()->data();
    arr3d<real>       energyrad = disk().radiativeEnergy()->data();
    arr3d<real const> eta1      = eta.template get<1>();
    arr3d<real const> eta2      = eta.template get<2>();
    
    kk::parallel_for("init_energy_rad", range3D({1,1,0}, {gsz.nr-1, gsz.ni-1, gsz.ns}),
                     LBD(size_t const i, size_t const h, size_t const j) { 
                       energyrad(i,h,j) = (temper(i,h,j) - eta1(i,h,j))/eta2(i,h,j);
                     });
    real const zBorderEnergy = disk().physic().adiabatic->radiative->zBoundaryEnergy();
    if (dispatch.first()) {
      kk::deep_copy(ring(energyrad, 0), ring(energyrad, 1));
    }
    if (dispatch.last()) {
      kk::deep_copy(ring(energyrad, gsz.nr-1), ring(energyrad, gsz.nr-2));
    }
    for (size_t i=0; i<gsz.nr; i++){
      kk::deep_copy(ring(energyrad, i, 0), zBorderEnergy);
    }
    if (disk().physic().shape.half){
      for (size_t i = 0; i<gsz.nr; ++i){
        kk::deep_copy(ring(energyrad, i, gsz.ni-1), ring(energyrad, i, gsz.ni-2));
      }
    } else {
      for (size_t i = 0; i<gsz.nr; ++i){
        kk::deep_copy(ring(energyrad, i, gsz.ni-1), zBorderEnergy);
      }
    }
    kk::deep_copy(derivative.data(), 0);
  }

  template<GridSpacing GS>
  LegacySolver<GS>::LegacySolver(Disk& dsk)
    : StellarRadiationSolver(dsk),
      derivative(DERIVATIVE_H5_NAME, disk().dispatch()) {
    cudaCtor();
  }

  template<GridSpacing GS>  
  void
  LegacySolver<GS>::advance(real dt) {
    Disk const& cdisk = disk();
    auto const& coords = disk().dispatch().grid().template as<GS>();
    Diffusion<GS> diffusion(cdisk);
    Eta<GS> eta(cdisk, diffusion, planetsRadiation<GS>(cdisk), dt);
    arr3d<real const> eta1 = eta.template get<1>();
    arr3d<real const> eta2 = eta.template get<2>();

    ScalarField  radiativeEnergyField(*disk().radiativeEnergy());
    auto radiativeEnergyBackup = scalarView("saved_radiative_energy", coords);
    auto radiativeEnergy       = radiativeEnergyField.data();
    kk::deep_copy(radiativeEnergyBackup, radiativeEnergyField.data());
    auto dradiative = derivative.data();
    kk::parallel_for("radiative_energy_prospect", fullRange(coords), 
                     LBD(int i, int h, int j) { radiativeEnergy(i,h,j) += dradiative(i,h,j)*dt; });
    Matrix<GS>(cdisk, diffusion, eta, dt).solve(radiativeEnergyField, dt);
    auto energy = disk().menergy()->data();
    real specificHeat = cdisk.physic().adiabatic->specificHeat();
    auto density = cdisk.density().data();
    auto currentRadiativeEnergy = disk().radiativeEnergy()->data();
    kk::parallel_for("radiative_energy_dispatch", fullRange(coords),
                     LBD(int i, int h, int j) { 
                       energy(i,h,j) = (specificHeat*density(i,h,j)
                                        *(eta1(i,h,j)+eta2(i,h,j)*radiativeEnergy(i,h,j)));
                       dradiative(i,h,j) = (radiativeEnergy(i,h,j)-radiativeEnergyBackup(i,h,j))/dt;
                     });
    kk::deep_copy(disk().radiativeEnergy()->data(), radiativeEnergy);                           
  }

  namespace details {
    StellarRadiationSolver::Declarator<LegacySolver> legacy("legacy");
  }
}
