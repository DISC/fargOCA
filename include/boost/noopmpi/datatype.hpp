// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_DATATYPE_HPP
#define BOOST_NOOPMPI_DATATYPE_HPP

#include <boost/serialization/access.hpp>
namespace boost{ namespace noopmpi {
  /*** @brief Won't be used. */
  template <typename T> struct is_mpi_datatype {};
}}

#endif // BOOST_NOOPMPI_DATATYPE_HPP
