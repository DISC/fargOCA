// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <sstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/algorithm/string.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "h5io.hpp"
#include "simulation.hpp"
#include "codeUnits.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Check the difference in planets log.\n"
        << '\t' << cmd << " <file1>.h5 <file2>.h5 [option]+ \nValid options are.\n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("verbose", "Print more stuff.")
    ("ofile,o", po::value<std::string>()->default_value("planetsdiff.h5"), "if present, will contains the diff.")
    ("time-tolerance,t", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on time")
    ("mass-tolerance,m", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on mass")
    ("position-tolerance,p", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on posiion")
    ("velocity-tolerance,v", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on velocity")
    ("radius-tolerance,r", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on radius")
    ("hradius-tolerance,r", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on Hill radius")
    ;
  po::options_description hidden;
  hidden.add_options()
    ("file1", po::value<std::string>(), "HDF5 file containing the first  planetary system log.")
    ("file2", po::value<std::string>(), "HDF5 file containing the second planetary system log.");
  
  try {
    po::positional_options_description pos;
    pos.add("file1", 1).add("file2",1);

    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_USAGE);
  } 
  for(std::string opt : {"file1", "file2"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
}

std::pair<real, real>
parseTolerance(std::string str) {
  std::vector<std::string> tuple;
  boost::split(tuple, str, boost::is_any_of(":"));
  if (tuple.size() != 2) {
    std::cerr << str << " is not a well formed tolerance specification, must be of the form <absolute tolerance>:<relative tolerance>.\n";
    std::exit(-1);
  }
  try {
    real abs = boost::lexical_cast<real>(tuple[0]);
    real rel = boost::lexical_cast<real>(tuple[1]);
    if (abs<0) {
      std::cerr << tuple[0] << " must be positive.\n";
      std::exit(-1);
    }
    if (rel<0) {
      std::cerr << tuple[1] << " must be positive.\n";
      std::exit(-1);
    }
    return std::make_pair(abs, rel);
  } catch (boost::bad_lexical_cast& e) {
    std::cerr << str << " is not a correct tolerance specification, must use positive floating point.\n";
    std::exit(-1);
  }
}

std::pair<TimeStamped<Planet>,TimeStamped<Planet>>
parseTolerance(po::variables_map& vm) {
  auto timeTol     = parseTolerance(vm["time-tolerance"].as<std::string>());
  auto massTol     = parseTolerance(vm["mass-tolerance"].as<std::string>());
  auto positionTol = parseTolerance(vm["position-tolerance"].as<std::string>());
  auto velocityTol = parseTolerance(vm["velocity-tolerance"].as<std::string>());
  auto radiusTol   = parseTolerance(vm["radius-tolerance"].as<std::string>());
  auto hradiusTol  = parseTolerance(vm["hradius-tolerance"].as<std::string>());
  TimeStamped<Planet> abs = {timeTol.first, Planet(massTol.first, 
                                                   Triplet(positionTol.first, 
                                                           positionTol.first, 
                                                           positionTol.first),
                                                   Triplet(velocityTol.first, 
                                                           velocityTol.first, 
                                                           velocityTol.first),
                                                   radiusTol.first,
                                                   hradiusTol.first )};
  TimeStamped<Planet> rel = {timeTol.second, Planet(massTol.second,
                                                    Triplet(positionTol.second, 
                                                            positionTol.second, 
                                                            positionTol.second),
                                                    Triplet(velocityTol.second, 
                                                            velocityTol.second, 
                                                            velocityTol.second),
                                                    radiusTol.second,
                                                    hradiusTol.second)};
  return std::make_pair(abs,rel);
}

/// \brief returns
///   * 0 if both unset
///   * nothing if only one is set
///   * the max if both set
std::optional<real>
omax(std::optional<real> o1, std::optional<real> o2) {
  if (!o1 && !o2) {
    return real(0); 
  } else if (bool(o1) != bool(o2)) {
    return std::nullopt; // can't be compared
  } else {
    return std::max(*o1, *o2);
  }
}

/// \brief returns
///   * 0 if both unset
///   * nothing if only one is set
///   * the diff if both set
std::optional<real>
odiff(std::optional<real> o1, std::optional<real> o2) {
  if (!o1 && !o2) {
    return real(0); 
  } else if (bool(o1) != bool(o2)) {
    return std::nullopt; // can't be compared
  } else {
    return (*o1 - *o2);
  }
}

struct DiffCollector {
  DiffCollector(optional<TimeStamped<Planet>>& a,
                optional<TimeStamped<Planet>>& r) : amax(a), rmax(r) {}
      
  TimeStamped<Planet>
  operator()(TimeStamped<Planet> const& p1, TimeStamped<Planet> const& p2) {
    TimeStamped<Planet> ad = adiff(p1, p2);
    if (!amax) {
      amax = ad;
    } else {
      amax  = { std::max(ad.time, amax->time),
                Planet(std::max(ad.state.mass(),  amax->state.mass()),
                       max(ad.state.position(),   amax->state.position()),
                       max(ad.state.velocity(),   amax->state.velocity()),
                       std::max(ad.state.radius().value_or(-1),    amax->state.radius().value_or(-1)),
                       std::max(ad.state.userHillRadius().value_or(-1), amax->state.userHillRadius().value_or(-1))) };
    }
    TimeStamped<Planet> rd = rdiff(p1, p2);    
    if (!rmax) {
      rmax = rd;
    } else {
      rmax = {  std::max(rd.time, rmax->time),
                Planet(std::max(rd.state.mass(),  rmax->state.mass()),
                       max(rd.state.position(),   rmax->state.position()),
                       max(rd.state.velocity(),   rmax->state.velocity()),
                       std::max(rd.state.radius().value_or(-1),     rmax->state.radius().value_or(-1)),
                       std::max(rd.state.userHillRadius().value_or(-1), rmax->state.userHillRadius().value_or(-1))) };
    }
    return { p1.time - p2.time,
             Planet(p1.state.mass() - p2.state.mass(),
                    p1.state.position() - p2.state.position(),
                    p1.state.velocity() - p2.state.velocity(),
                    p1.state.radius().value_or(-1) - p2.state.radius().value_or(-1),
                    p1.state.userHillRadius().value_or(-1) - p2.state.userHillRadius().value_or(-1)) };
  }
  
  optional<TimeStamped<Planet>>& amax;
  optional<TimeStamped<Planet>>& rmax;
};

bool check(TimeStamped<Planet> amax, TimeStamped<Planet> rmax, 
           TimeStamped<Planet> atol, TimeStamped<Planet> rtol, 
           std::ostream& log) {
  auto ok = [](TimeStamped<Planet> p, TimeStamped<Planet> m) {
    return (p.time <= m.time
            && p.state.mass() <= m.state.mass()
            && p.state.position().x <= m.state.position().x
            && p.state.position().y <= m.state.position().y
            && p.state.position().z <= m.state.position().z
            && p.state.velocity().x <= m.state.velocity().x
            && p.state.velocity().y <= m.state.velocity().y
            && p.state.velocity().z <= m.state.velocity().z
            && *p.state.radius() <= *m.state.radius()
	    && *p.state.userHillRadius() <= *m.state.userHillRadius());
  };
  bool good = true;
  log << "Maximum absolute diff: " << amax;
  if (!ok(amax, atol)) {
    log << "(>" << atol << ")";
    good = false;
  }
  log << "\n";
  
  log << "Maximum relative diff: " << rmax;
  if (!ok(rmax, rtol)) {
    log << "(>" << rtol << ")";
    good = false;
  }
  log << "\n";
  
  return good;
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);
  
  std::string fname1 = vm["file1"].as<std::string>();
  std::string fname2 = vm["file2"].as<std::string>();
  
  // Init with default values. We're only interested in diff anyway
  auto logs1 = PlanetarySystem::readH5Logs(HF::File(fname1, HF::File::ReadOnly).getGroup("/"));
  auto logs2 = PlanetarySystem::readH5Logs(HF::File(fname2, HF::File::ReadOnly).getGroup("/"));
  bool good       = true; // prove me wrong
  bool compatible = true;
  std::ostream& vlog = log(bool(vm.count("verbose")));
  if (logs1.size() != logs2.size()) {
    good       = false;
    compatible = false;
    std::cerr << "File '" << fname1 << "' contains the log for " << logs1.size() 
              << "' planet when '" << fname2 << "' contains the logs for " << logs2.size() << "\n";
  }
  auto tol = parseTolerance(vm);
  TimeStamped<Planet> const& abstol = tol.first;
  TimeStamped<Planet> const& reltol = tol.second;
  std::map<std::string, std::vector<TimeStamped<Planet>>> dlogs;
  for(auto const& kv1 : logs1) {
    std::string name = kv1.first;
    vlog << "comparing log of planet '" << name << "'\n";
    if (logs2.find(name) == logs2.end()) {
      std::cerr << "No log for '" << name << "' in '" << fname2 << "'\n";
      good = false;
      compatible = false;
      continue;
    }
    auto log1 = kv1.second;
    auto log2 = logs2.at(name);
    if (log1.size() != log2.size()) {
      std::cerr << "Logs for '" << name << "' do not have the same size in both files (" 
                << log1.size() << " vs " << log2.size() << ")\n";
      good = false;
      compatible = false;
      continue;
    } else {
      vlog << "Number of records: " << log1.size() << "'\n";
      if (log1.size() > 0) {
        optional<TimeStamped<Planet>> amax = std::nullopt;
        optional<TimeStamped<Planet>> rmax = std::nullopt;
        std::transform(log1.begin(), log1.end(), log2.begin(),
                       std::back_inserter(dlogs[name]), DiffCollector(amax, rmax));
        std::ostringstream trace;
        //trace << std::scientific;
        trace.precision(8);
        if (!check(*amax, *rmax, abstol, reltol, trace)) {
          good = false;
        }
        log(world) << trace.str();
      }
    }
  }
  if (compatible) {
    if  (vm.count("ofile")) {
      std::string ofname = vm["ofile"].as<std::string>();
      HF::File ofile(ofname, HF::File::Overwrite);
      PlanetarySystem::writeH5Logs(ofile.getGroup("/"), dlogs);
      vlog << "Output written in '" << ofname << "'\n";
    } 
  }

  std::string status = "NOT CLOSE ENOUGH";
  if (good) {
    status = "CLOSE ENOUGH";
  }
  vlog << status << '\n';
  return good ? 0 : -1;
}
