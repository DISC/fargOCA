// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_KKMATH_HPP
#define FARGOCA_KKMATH_HPP

#include <Kokkos_MathematicalFunctions.hpp>

namespace fargOCA {
  // std::hypot with 3 arguments is not available in Kokkos right now, but we need it on device.
  // We will remove this workaround  once we switch to Kokkos 4.0
  KOKKOS_INLINE_FUNCTION
  real
  hypot3(real x, real y, real z)  {
    using Kokkos::abs;
    using Kokkos::sqrt;
    x = abs(x);
    y = abs(y);
    z = abs(z);
    if (real a = (x < y 
                  ? (y < z 
                     ? z 
                     : y)
                  : (x < z 
                     ? z 
                     : x))) {
      return a * sqrt((x / a) * (x / a)
                      + (y / a) * (y / a)
                      + (z / a) * (z / a));
    } else {
      return {};
    }
  }
}

#endif
