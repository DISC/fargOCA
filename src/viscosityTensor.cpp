// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <cmath>
#include <cstdio>
#include <string>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "viscosityTensor.hpp"
#include "gridCellSizes.hpp"
#include "loops.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace kkm = Kokkos::Experimental;
#define LBD KOKKOS_LAMBDA

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

  using kk::pow;
  using kk::sin;
  using kk::cos;

  arr3d<real>
  divergence(VectorField const& v) {
    arr3d<real> div = scalarView("divergence", v.dispatch().grid());
    divergence(v.dispatch(), v.radial().data(), v.phi().data(), v.theta().data(), div);
    return div;
  }
  
  namespace details {
    template<GridSpacing GS>
    void
    divergence(GridDispatch const& grid, arr3d<real const> vr, arr3d<real const> vp, arr3d<real const> vt, arr3d<real> out) {
      auto const& coords = grid.grid().as<GS>();
      GridSizes const gsz = coords.sizes();
      
      auto radii   = coords.radii();
      auto dradii  = coords.dRadii();
      auto radMed  = coords.radiiMed();
      auto phi     = coords.phi();
      auto dphi    = coords.dPhi();
      auto phiMed  = coords.phiMed();
      arr3d<real const> dtheta  = grid.cellSizes().dTheta();
      
      kk::parallel_for("VTensor3D ctor D",
                       fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         if (i == gsz.nr-1 || h == gsz.ni-1) {
                           out(i,h,j) = 0;
                         } else {
                           real const radSup2    = ipow<2>(radii(i+1));
                           real const radii2     = ipow<2>(radii(i));
                           real const invRadMed  = 1/radMed(i);
                           real const invDiffRad = 1/dradii(i);
                           real const sinPhiMed = sin(phiMed(h));
                           real const sinPhiP   = sin(phi(h+1));
                           real const sinPhi    = sin(phi(h));
                           real const dPhi      = radMed(i)*dphi(h);
                           size_t const jp = nsnext(gsz.ns,j);
                           out(i,h,j)  = ((vr(i+1,h,j)*radSup2-vr(i,h,j)*radii2)*invDiffRad*ipow<2>(invRadMed)
                                          + (vt(i,h,jp)-vt(i,h,j))/dtheta(i,h,j)
                                          + (vp(i,h+1,j)*sinPhiP-vp(i,h,j)*sinPhi)/dPhi/sinPhiMed);
                         }
                       });
    }
  }
  
  void
  divergence(GridDispatch const& grid, arr3d<real const> vr, arr3d<real const> vp, arr3d<real const> vt, arr3d<real> out) {
    switch (grid.grid().radialSpacing()) {
    case ARTH: details::divergence<ARTH>(grid, vr, vp, vt, out); break;
    case LOGR: details::divergence<LOGR>(grid, vr, vp, vt, out); break;
    }
  }
  
  namespace details {
    class Tensor {
    public:
      virtual ~Tensor();
      
      Disk const& disk() const { return myDisk; }
      
      virtual void advanceVelocity(VectorField& v, real dt) const = 0;
      virtual void computeQPlus(arr3d<real> qplus) const = 0;
      
      template<template <GridSpacing GS> class TENSOR>
      static
      auto makeGS(Disk const& disk) {
        std::unique_ptr<Tensor> engine;
        switch (disk.dispatch().grid().radialSpacing()) {
        case ARTH: engine = std::make_unique<TENSOR<ARTH>>(disk); break;
        case LOGR: engine = std::make_unique<TENSOR<LOGR>>(disk); break;
        }
        return engine;
      }
      /// \brief Make a Tensor adapted to the Disk
      static std::unique_ptr<Tensor> make(Disk const& disk);
      
    protected:
      Tensor(Disk const& disk);
      template <typename T> FieldStack<T>& temporaries() const;
    private:
      Disk const&   myDisk;
    };
    
    template<typename T>
    FieldStack<T>&
    Tensor::temporaries() const { 
      return myDisk.dispatch().temporaries<T>(); 
    }
  
    template<GridSpacing GS>
    class Tensor3D 
      : public Tensor {
    public:
      Tensor3D(Disk const& disk);
      ~Tensor3D();

      virtual void advanceVelocity(VectorField& v, real dt) const override;
      virtual void computeQPlus(arr3d<real> qplus) const override;

    private:
      enum T3D : int {
        VELOCITY_DIV,
          D_RR,
          D_RT,
          D_TT,
          D_PP,
          D_RP,
          D_TP,
          
          TAU_RR,
          TAU_RT,
          TAU_TT,
          TAU_RP,
          TAU_PP,
          TAU_TP,
          LOC_SIZE
          };
      
      LocalFieldStack3D<real, LOC_SIZE> views;

    cuda_private:
      void cudaCtor();
    };

    Tensor::Tensor(Disk const& disk)
      : myDisk(disk) {}
  
    Tensor::~Tensor() {}

    template<GridSpacing GS>
    Tensor3D<GS>::Tensor3D(Disk const& disk)
      : Tensor(disk),
        views(disk.dispatch().temporaries3D<real>()) {
      cudaCtor();
    }
    
    template<GridSpacing GS>
    void
    Tensor3D<GS>::cudaCtor() {
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();
      
      arr3d<real const> rho    = disk().density().data();
      arr3d<real const> vr     = disk().velocity().radial().data();
      arr3d<real const> vp     = disk().velocity().phi().data();
      arr3d<real const> vt     = disk().velocity().theta().data();
      arr3d<real const> visc   = disk().viscosity().data();
      auto radMed = coords.radiiMed();
      auto radii  = coords.radii();
      auto phiMed = coords.phiMed();
      auto phi    = coords.phi();
      arr3d<real const> dtheta = dispatch.cellSizes().dTheta();
      
      fargOCA::LocalFieldStack3D<real, LOC_SIZE>& views = this->views;
      fargOCA::divergence(dispatch,
                          disk().velocity().radial().data(),
                          disk().velocity().phi().data(),
                          disk().velocity().theta().data(),
                          views.template get<VELOCITY_DIV>());
      arr3d<real const> vdiv = views.template get<VELOCITY_DIV>();

      arr3d<real> dRR = views.template init<D_RR>();
      arr3d<real> dRT = views.template init<D_RT>();
      arr3d<real> dTT = views.template init<D_TT>();
      arr3d<real> dPP = views.template init<D_PP>();
      arr3d<real> dRP = views.template init<D_RP>();
      arr3d<real> dTP = views.template init<D_TP>();

      arr3d<real> tauRR = views.template init<TAU_RR>();
      arr3d<real> tauRT = views.template init<TAU_RT>();
      arr3d<real> tauTT = views.template init<TAU_TT>();
      arr3d<real> tauRP = views.template init<TAU_RP>();
      arr3d<real> tauPP = views.template init<TAU_PP>();
      arr3d<real> tauTP = views.template init<TAU_TP>();
      real const third = real(1)/3;
      real const NaN   = std::numeric_limits<real>::quiet_NaN();
      kk::parallel_for("VTensor3D ctor D",
                       fullRange(dispatch.grid()),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         size_t const jp = nsnext(gsz.ns,j);
                         size_t const jm = nsprev(gsz.ns,j);
                         
                         real const invRadMed     = 1/radMed(i);
                         real const cosPhiMed     = std::cos(phiMed(h));
                         real const sinPhiMed     = std::sin(phiMed(h));
                         real const sinPhi        = std::sin(phi(h));
                         real const invDPhiMed    = 1/(radMed(i)*(phiMed(h+1)-phiMed(h)));
                         real const invDiffRad    = 1/(radii(i+1)-radii(i));
                         real const invDPhi       = 1/(radMed(i)*(phi(h+1)-phi(h)));
                         
                         if (i < gsz.nr-1 && h < gsz.ni-1) {
                           dRR(i,h,j) = (vr(i+1,h,j)-vr(i,h,j))*invDiffRad;
                           dTT(i,h,j) = ((vt(i,h,jp)-vt(i,h,j))/dtheta(i,h,j)+0.5*(vr(i+1,h,j)+vr(i,h,j))*invRadMed
                                         +(vp(i,h+1,j)+vp(i,h,j))*0.5*invRadMed*cosPhiMed/sinPhiMed);
                           dPP(i,h,j) = (vp(i,h+1,j)-vp(i,h,j))*invDPhi+0.5*(vr(i+1,h,j)+vr(i,h,j))*invRadMed;
                         } else {
                           dRR(i,h,j) = NaN; dTT(i,h,j) = NaN; dPP(i,h,j) = NaN;
			 }

                         if (i > 0) {
                           real const invRadMedM    = 1/radMed(i-1);
                           real const invDiffRadMed = 1/(radMed(i)-radMed(i-1));
                           dRT(i,h,j) = (radii(i)*(vt(i,h,j)*invRadMed-vt(i-1,h,j)*invRadMedM)*invDiffRadMed+    
					 (vr(i,h,j)-vr(i,h,jm))/dtheta(i,h,j))/2;
                           tauRT(i,h,j) = (rho(i,h,j)+rho(i-1,h,j)+rho(i,h,jm)+rho(i-1,h,jm))*visc(i,h,j)*dRT(i,h,j)/2;
                           if ( h > 0) {
                             dRP(i,h,j) = ((vr(i,h,j)-vr(i,h-1,j))*invDPhiMed+    
					   radii(i)*(vp(i,h,j)/radMed(i)-vp(i-1,h,j)/radMed(i-1))*invDiffRadMed)/2;
                             tauRP(i,h,j) = (rho(i,h,j)+rho(i,h-1,j)+rho(i-1,h,j)+rho(i-1,h-1,j))*visc(i,h,j)*dRP(i,h,j)/2;
                           } else {
                             dRP(i,0,j) = NaN; tauRP(i,0,j) = NaN;
			   }
                         } else {
                           dRT(0,h,j) = NaN; tauRT(0,h,j) = NaN;
			 }
                         if (h > 0) {
                           real const sinPhiMedP    = std::sin(phiMed(h-1));
                           dTP(i,h,j)   = ((vp(i,h,j)-vp(i,h,jm))/dtheta(i,h,j)+    
					   sinPhi*(vt(i,h,j)/sinPhiMed-vt(i,h-1,j)/sinPhiMedP)*invDPhiMed)/2;
                           tauTP(i,h,j) = (rho(i,h,j)+rho(i,h-1,j)+rho(i,h,jm)+rho(i,h-1,jm))*visc(i,h,j)*dTP(i,h,j)/2;
                         } else {
			   dTP(i,h,j)   = NaN; tauTP(i,h,j) = NaN;
			 }
                         if (i < gsz.nr-1) {
                           tauRR(i,h,j) = 2*rho(i,h,j)*visc(i,h,j)*(dRR(i,h,j)-third*vdiv(i,h,j));
                           tauTT(i,h,j) = 2*rho(i,h,j)*visc(i,h,j)*(dTT(i,h,j)-third*vdiv(i,h,j));
                           tauPP(i,h,j) = 2*rho(i,h,j)*visc(i,h,j)*(dPP(i,h,j)-third*vdiv(i,h,j));
                         } else {
                           tauRR(i,h,j) = NaN; tauTT(i,h,j) = NaN; tauPP(i,h,j) = NaN;
			 }
                       });
    }
    template<GridSpacing GS>
    Tensor3D<GS>::~Tensor3D() {}

    template<GridSpacing GS>
    void
    Tensor3D<GS>::advanceVelocity(VectorField& v, real dt) const
    {
      // Dev note: v is usually the same as disk().velocity()
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();

      arr3d<real const> rho = disk().density().data();
      arr3d<real>       vr  = v.radial().data();
      arr3d<real>       vt  = v.theta().data();
      arr3d<real>       vp  = v.phi().data();

      arr3d<real const>  dtheta = dispatch.cellSizes().dTheta();
      auto radii  = coords.radii();        
      auto radMed = coords.radiiMed();
      auto phi    = coords.phi();
      auto phiMed = coords.phiMed();

      arr3d<real const> dRR = views.template get<D_RR>();
      arr3d<real const> dRT = views.template get<D_RT>();
      arr3d<real const> dTT = views.template get<D_TT>();
      arr3d<real const> dPP = views.template get<D_PP>();
      arr3d<real const> dRP = views.template get<D_RP>();
      arr3d<real const> dTP = views.template get<D_TP>();

      arr3d<real const> tauRR = views.template get<TAU_RR>();
      arr3d<real const> tauRT = views.template get<TAU_RT>();
      arr3d<real const> tauTT = views.template get<TAU_TT>();
      arr3d<real const> tauRP = views.template get<TAU_RP>();
      arr3d<real const> tauPP = views.template get<TAU_PP>();
      arr3d<real const> tauTP = views.template get<TAU_TP>();
      /* Now we can update velocities */
      /* in 3D with the viscous source term */
      /* of Navier-Stokes equations */
      kk::parallel_for("VTensor3D update vtheta",
                       range3D({1,1,0},{gsz.nr-1,gsz.ni-1,gsz.ns}),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         size_t const jp = nsnext(gsz.ns, j);
                         size_t const jm = nsprev(gsz.ns, j);
                         
                         real const radii2        = ipow<2>(radii(i));
                         real const radMed2       = ipow<2>(radMed(i));
                         real const radMedM2      = ipow<2>(radMed(i-1));
                         real const radSup2       = ipow<2>(radii(i+1));
                         real const invRadMed     = 1/radMed(i);
                         real const invDiffRad    = 1/(radii(i+1)-radii(i));
                         real const sinPhi        = sin(phi(h));
                         real const cosPhi        = cos(phi(h));
                         real const sinPhiP       = sin(phi(h+1));
                         real const sinPhiMed     = sin(phiMed(h));
                         real const cosPhiMed     = cos(phiMed(h));
                         real const invDPhi       = 1/(radMed(i)*(phi(h+1)-phi(h)));
                         real const invDPhiMed    = 1/(radMed(i)*(phiMed(h+1)-phiMed(h)));
                         real const invRadInf     = 1/radii(i);
                         real const invDiffRadMed = 1/(radMed(i)-radMed(i-1));
                         
                         vt(i,h,j) += (dt
                                       *(invRadMed*invRadMed*(radSup2*tauRT(i+1,h,j)-radii2*tauRT(i,h,j))*invDiffRad+ 
                                         (tauTP(i,h+1,j)*sinPhiP-tauTP(i,h,j)*sinPhi)*invDPhi/sinPhiMed+
                                         (tauTT(i,h,j)-tauTT(i,h,jm))/dtheta(i,h,j)+    
                                         0.5*(tauRT(i,h,j)+tauRT(i+1,h,j))*invRadMed+
                                         0.5*(tauTP(i,h+1,j)+tauTP(i,h,j))*invRadMed*cosPhiMed)/sinPhiMed
                                       /(0.5*(rho(i,h,j)+rho(i,h,jm))));
                         vp(i,h,j) += (dt*
                                       (invRadMed*invRadMed*(radSup2*tauRP(i+1,h,j)-radii2*tauRP(i,h,j))*invDiffRad+ 
                                        (tauPP(i,h,j)*sinPhiMed-tauPP(i,h-1,j)*sinPhiMed)*invDPhiMed/sinPhi+
                                        (tauTP(i,h,jp)-tauTP(i,h,j))/dtheta(i,h,j)+    
                                        0.5*(tauRP(i,h,j)+tauRP(i+1,h,j))*invRadMed+
                                        -0.5*(tauTT(i,h-1,j)+tauTT(i,h,j))*invRadMed*cosPhi/sinPhi)           
                                       /(0.5*(rho(i,h,j)+rho(i,h-1,j))));
                         
                         vr(i,h,j) += (dt
                                       *(ipow<2>(invRadInf)*(radMed2*tauRR(i,h,j)-radMedM2*tauRR(i-1,h,j))*invDiffRadMed
                                         +(tauRP(i,h+1,j)*sinPhiP-tauRP(i,h,j)*sinPhi)*invDPhi/sinPhiMed 
                                         +(tauRT(i,h,jp)-tauRT(i,h,j))/dtheta(i,h,j)
                                         -0.5*((tauPP(i,h,j)+tauPP(i-1,h,j)+tauTT(i,h,j)+tauTT(i-1,h,j))*invRadInf))
                                       /(0.5*(rho(i,h,j)+rho(i-1,h,j))));
                       });
    }

    template<GridSpacing GS>
    void
    Tensor3D<GS>::computeQPlus(arr3d<real> qplus) const 
    {
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();

      arr3d<real const> dens = disk().density().data();   
      arr3d<real const> visc = disk().viscosity().data();

      arr3d<real const> vdiv = views.template get<VELOCITY_DIV>();
      arr3d<real const> dRR  = views.template get<D_RR>();
      arr3d<real const> dRT  = views.template get<D_RT>();
      arr3d<real const> dTT  = views.template get<D_TT>();
      arr3d<real const> dPP  = views.template get<D_PP>();
      arr3d<real const> dRP  = views.template get<D_RP>();
      arr3d<real const> dTP  = views.template get<D_TP>();
      
      bool const first = dispatch.first();
      bool const last  = dispatch.last();
      real const third = real(1)/3;
      auto inner = LBD(size_t const i, size_t const h, size_t const j) -> real
	{
	 KOKKOS_ASSERT(i > 0 && i < gsz.nr-1  && h > 0 && h < gsz.ni - 1);
	 size_t const jp = nsnext(gsz.ns, j);
	 
	 real const drtm = (dRT(i,h,j)+dRT(i+1,h,j)+dRT(i,h,jp)+dRT(i+1,h,jp))/4;
	 real const drpm = (dRP(i,h,j)+dRP(i,h+1,j)+dRP(i+1,h,j)+dRP(i+1,h+1,j))/4;
	 real const dtpm = (dTP(i,h,j)+dTP(i,h,jp)+dTP(i,h+1,j)+dTP(i,h+1,jp))/4; /* value of drt, drp,dtp centered */
	 return (2*dens(i,h,j)*visc(i,h,j)*(norm2(dRR(i,h,j), dTT(i,h,j), dPP(i,h,j))
					    + 2*norm2(drtm,drpm,dtpm)
					    - third*ipow<2>(vdiv(i,h,j))));
	};
      // Radiative flux from KBK 2009 Eq. 8
      kk::parallel_for("VTensor3D Q+ ",
                       fullRange(coords),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         size_t const jp = nsnext(gsz.ns, j);
                         if (i > 0 && i < gsz.nr-1
                             && h > 0 && h < gsz.ni - 1) { 
                           qplus(i,h,j) = inner(i,h,j);
			 } else if (i == 0) {
                           if (first) {
                             if (h == 0) {
                               qplus(0,0,j) = inner(1,1,j);
                             } else if (h == gsz.ni-1) {
                               qplus(0,gsz.ni-1,j) = inner(1,gsz.ni-2,j);
                             } else {
                               qplus(0,h,j) = inner(1,h,j);
                             }
                           } else {
                             // we do not care about this ghost
                           }
			 } else if (i == gsz.nr-1) {
                           if (last) {
                             if (h == 0) {
                               qplus(gsz.nr-1,h,j) = inner(gsz.nr-2,1,j);
                             } else if (h == gsz.ni-1) {
                               qplus(gsz.nr-1,h,j) = inner(gsz.nr-2,gsz.ni-2,j);
                             } else {
                               qplus(gsz.nr-1,h,j) = inner(gsz.nr-2,h,j);
                             }
                           } else {
                             // we do not care about this ghost
                           }
			 } else if (h == 0) {
			   qplus(i,0,j) = inner(i,1,j);
			 } else if (h == gsz.ni-1) {
			   qplus(i,gsz.ni-1,j) = inner(i,gsz.ni-2,j);
			 } else {
                           // We have to be on a ghost we do not care about
			   KOKKOS_ASSERT((!last && i == gsz.nr-1)||(!first && i == 0));
			 }
                       });
    }
    
    template<GridSpacing GS>
    class Tensor2D 
      : public Tensor {
    public:
      Tensor2D(Disk const& disk);
      ~Tensor2D();
    
      virtual void advanceVelocity(VectorField& v, real dt) const override;
      virtual void computeQPlus(arr3d<real> qplus) const override;

    private:
      enum { VELOCITY_DIV,
             D_RR,
             D_RT,
             D_TT,

             TAU_RR,
             TAU_RT,
             TAU_TT,
             LOC_SIZE
      };
      LocalFieldStack3D<real, LOC_SIZE> views;
    public:
      void cudaCtor();
    };
  
    template<GridSpacing GS>
    Tensor2D<GS>::Tensor2D(Disk const& g)
      : Tensor(g),
        views(g.dispatch().temporaries3D<real>())
    {
      cudaCtor();
    }
  
    template<GridSpacing GS>
    void
    Tensor2D<GS>::cudaCtor() {
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();

      arr3d<real const> rho = disk().density().data();
      arr3d<real const> vr  = disk().velocity().radial().data();
      arr3d<real const> vt  = disk().velocity().theta().data();
      arr3d<real const> visc = disk().viscosity().data();
      auto radii = coords.radii();
      auto radMed = coords.radiiMed();
      arr3d<real const> dtheta = dispatch.cellSizes().dTheta();

      arr3d<real> vdiv = views.template init<VELOCITY_DIV>();
      
      arr3d<real> dRR = views.template init<D_RR>();
      arr3d<real> dRT = views.template init<D_RT>();
      arr3d<real> dTT = views.template init<D_TT>();

      arr3d<real> tauRR = views.template init<TAU_RR>();
      arr3d<real> tauRT = views.template init<TAU_RT>();
      arr3d<real> tauTT = views.template init<TAU_TT>();
      
      real const third = real(1)/3;

      kk::parallel_for("VTensor2D<GS>::ctor dRR dTT divV", fullFlatRange(coords),
                       LBD(size_t const i, size_t const j) {
                         size_t const jp = nsnext(gsz.ns,j);
                         size_t const jm = nsprev(gsz.ns,j);
                         
                         real const invRadMed     = 1/radMed(i);
                         if (i < gsz.nr-1) {
                           real const invDiffRad    = 1/(radii(i+1)-radii(i));
                           
                           dRR(i,0,j) = (vr(i+1,0,j)-vr(i,0,j))*invDiffRad;
                           dTT(i,0,j) = (vt(i,0,jp)-vt(i,0,j))/dtheta(i,0,j)+0.5*(vr(i+1,0,j)+vr(i,0,j))*invRadMed;
                           vdiv(i,0,j)  = ((vr(i+1,0,j)*radii(i+1)-vr(i,0,j)*radii(i))*invDiffRad*invRadMed
                                                         +(vt(i,0,jp)-vt(i,0,j))/dtheta(i,0,j));
                         } else {
                           dRR(i,0,j)  = 0;
                           dTT(i,0,j)  = 0;
                           vdiv(i,0,j) = 0;
                         }
                         if (i > 0) {
                           real const invRadMedM    = 1/radMed(i-1);
                           real const invDiffRadMed = 1/(radMed(i)-radMed(i-1));
                           
                           dRT(i,0,j) = 0.5*(radii(i)*(vt(i,0,j)*invRadMed-vt(i-1,0,j)*invRadMedM)*invDiffRadMed+
                                             (vr(i,0,j)-vr(i,0,jm))/dtheta(i,0,j));
                           tauRT(i,0,j) = 2*0.25*(rho(i,0,j)+rho(i-1,0,j)+rho(i,0,jm)+rho(i-1,0,jm))*visc(i,0,j)*dRT(i,0,j);
                         } else {
                           dRT(i,0,j)   = 0;
                           tauRT(i,0,j) = 0;
                         }
                         tauRR(i,0,j) = 2*rho(i,0,j)*visc(i,0,j)*(dRR(i,0,j)-third*vdiv(i,0,j));
                         tauTT(i,0,j) = 2*rho(i,0,j)*visc(i,0,j)*(dTT(i,0,j)-third*vdiv(i,0,j));
                       });
    }
    
    template<GridSpacing GS>
    Tensor2D<GS>::~Tensor2D() {}
  
    template<GridSpacing GS>
    void
    Tensor2D<GS>::advanceVelocity(VectorField& v, real dt) const {
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();
    
      arr3d<real const> rho = disk().density().data();
      arr3d<real>       vr  = v.radial().data();
      arr3d<real>       vt  = v.theta().data();
      arr3d<real const> dtheta = dispatch.cellSizes().dTheta();
  
      auto radMed = coords.radiiMed();
      auto radii  = coords.radii();

      arr3d<real const> tauRR = views.template get<TAU_RR>();
      arr3d<real const> tauRT = views.template get<TAU_RT>();
      arr3d<real const> tauTT = views.template get<TAU_TT>();

      /* Now we can update velocities */
      /* of the 2D casewith the viscous source term */
      /* of Navier-Stokes equations */
    
      kk::parallel_for("VTensor2D vtheta",
                       range2D({1,0},{gsz.nr,gsz.ns}),
                       LBD(size_t const i, size_t const j) {
                         size_t const jp = nsnext(gsz.ns,j);
                         size_t const jm = nsprev(gsz.ns,j);
                         
                         real const invRadMed     = 1/radMed(i);
                         real const invDiffRad    = 1/(radii(i+1)-radii(i));
                         real const invRadInf     = 1/radii(i);
                         real const invDiffRadMed = 1/(radMed(i)-radMed(i-1));
                         
                         if (i < gsz.nr-1) {
                           vt(i,0,j) += dt*invRadMed*((radii(i+1)*tauRT(i+1,0,j)-radii(i)*tauRT(i,0,j))*invDiffRad+
                                                      (tauTT(i,0,j)-tauTT(i,0,jm))/dtheta(i,0,j)/radMed(i)+    
                                                      0.5*(tauRT(i,0,j)+tauRT(i+1,0,j)))/(0.5*(rho(i,0,j)+rho(i,0,jm)));
                         }
                         
                         vr(i,0,j) += (dt*invRadInf*((radMed(i)*tauRR(i,0,j)-radMed(i-1)*tauRR(i-1,0,j))*invDiffRadMed
                                                     + (tauRT(i,0,jp)-tauRT(i,0,j))/dtheta(i,0,j)/radMed(i)
                                                     - 0.5*(tauTT(i,0,j)+tauTT(i-1,0,j)))
                                       /(0.5*(rho(i,0,j)+rho(i-1,0,j))));
                       });
    }

    template<GridSpacing GS>
    void 
    Tensor2D<GS>::computeQPlus(arr3d<real> qplus) const {
      GridDispatch const& dispatch = disk().dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const gsz = coords.sizes();
      
      arr3d<real const> dens = disk().density().data();   
      arr3d<real const> visc = disk().viscosity().data();
      
      arr3d<real const> vdiv = views.template get<VELOCITY_DIV>();
      arr3d<real const> dRR  = views.template get<D_RR>();
      arr3d<real const> dRT  = views.template get<D_RT>();
      arr3d<real const> dTT  = views.template get<D_TT>();
      
      bool const first = dispatch.first();
      bool const last  = dispatch.last();
      real const third = real(1)/3;
      real const NaN   = std::numeric_limits<real>::quiet_NaN();
      // "normal" case
      auto inner = LBD(size_t const i, size_t const j) -> real
	{
	 KOKKOS_ASSERT(i > 0 && i < gsz.nr-1);
	 size_t const jp = nsnext(gsz.ns, j);
	 real const drtm = (dRT(i,0,j)+dRT(i+1,0,j)+dRT(i,0,jp)+dRT(i+1,0,jp))/4;
	 /* value of drt, centered */
	 return 2*dens(i,0,j)*visc(i,0,j)*(norm2(dRR(i,0,j),dTT(i,0,j))+2*ipow<2>(drtm)-third*ipow<2>(vdiv(i,0,j)));
	 
	};
      // Radiative flux from KBK 2009 Eq. 8
      kk::parallel_for("VTensor2D Q+ ",
		       fullFlatRange(coords),             
		       LBD(size_t const i, size_t const j) {
                         if (i > 0 && i < gsz.nr-1) {
                           qplus(i,0,j) = inner(i,j);
			 } else if (first && i == 0) {
			   qplus(0,0,j) = inner(1,j);
			 } else if (last && i == gsz.nr-1) {
			   qplus(gsz.nr-1,0,j) = inner(gsz.nr-2,j);
			 } else {
			   qplus(i,0,j) = NaN;
			 }
                       });
      
      
    }

    std::unique_ptr<Tensor>
    Tensor::make(Disk const& disk) {
      auto const& coords = disk.dispatch().grid();
      switch(coords.dim()) {
      case 3: return makeGS<Tensor3D>(disk); break;
      case 2: return makeGS<Tensor2D>(disk); break;
      default: std::abort();
      }
    }
  }
  
  void
  ViscosityTensor::advanceVelocity(Disk& disk, real dt) {
    details::Tensor::make(disk)->advanceVelocity(disk.velocity(), dt);
  }
  
  void
  ViscosityTensor::computeQPlus(Disk const& disk, arr3d<real> qplus) {
    details::Tensor::make(disk)->computeQPlus(qplus);
  }
}
