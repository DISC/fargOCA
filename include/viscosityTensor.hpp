// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGO_VISCOSITY_TENSOR_H
#define FARGO_VISCOSITY_TENSOR_H

#include <memory>

#include "precision.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "shared.hpp"

namespace fargOCA {

  /// \brief The viscosity tensor is applied to the disk velocity field.
  /// 
  /// The actual implementation is adapted to the disk dimension.
  class ViscosityTensor {
  public:
    /// \brief Extract the viscosity tensor from the disk, and apply it to the disk velocity field.
    /// \param dt the velocity will be advanced for dt
    static void advanceVelocity(Disk& disk, real dt);
    /// \brief I have no clue.
    static void computeQPlus(Disk const& disk, arr3d<real> qplus);
  };
  
  // Not sure this is good enough to be put in velocity class
  arr3d<real> divergence(VectorField const& v);
  /// \brief Compute the divergence of a vector field
  /// \param grid the grid descrition
  /// \param radial the radial component of the vector field
  /// \param phi the altitude component of the vector field
  /// \param theta the azimutal component of the vector field
  /// \param div the result
  void divergence(GridDispatch const& grid, arr3d<real const> radial, arr3d<real const> phi, arr3d<real const> theta, arr3d<real> div);
}
#endif 
