// Copyright (C) 2005-2006 Douglas Gregor <doug.gregor@gmail.com>

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// Message Passing Interface 1.1 -- 7.1.1. Environmental Inquiries
#include <boost/noopmpi/environment.hpp>

#include <cassert>
#include <string>
#include <iostream>
#include <limits>

#include "boost/noopmpi/environment.hpp"
#include "boost/asio/ip/host_name.hpp"

namespace boost { namespace noopmpi {
bool environment::is_initialized  = false;

namespace threading {
std::istream& operator>>(std::istream& in, level& l)
{
  std::string tk;
  in >> tk;
  if (!in.bad()) {
    if (tk == "single") {
      l = single;
    } else if (tk == "funneled") {
      l = funneled;
    } else if (tk == "serialized") {
      l = serialized;
    } else if (tk == "multiple") {
      l = multiple;
    } else {
      in.setstate(std::ios::badbit);
    }
  }
  return in;
}

std::ostream& operator<<(std::ostream& out, level l)
{
  switch(l) {
  case single:
    out << "single";
    break;
  case funneled:
    out << "funneled";
    break;
  case serialized:
    out << "serialized";
    break;
  case multiple:
    out << "multiple";
    break;
  default:
    out << "<level error>[" << int(l) << ']';
    out.setstate(std::ios::badbit);
    break;
  }
  return out;
}

} // namespace threading

int environment::max_tag()
{
  int num_reserved_tag = 2;
  return std::numeric_limits<int>::max() - num_reserved_tag;
}

int environment::collectives_tag()
{
  return max_tag() + 1;
}

std::string environment::processor_name()
{
  return boost::asio::ip::host_name();
}

threading::level environment::thread_level()
{
  // we are not doing anything anyway
  return threading::multiple;
}

bool environment::is_main_thread()
{
  // sure we are not doing anything anyway
  return true;
}

std::pair<int, int> environment::version()
{
  return std::make_pair(BOOST_NOOPMPI_VERSION, BOOST_NOOPMPI_SUBVERSION);
}

} } // end namespace boost::mpi
