// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_STATUS_HPP
#define BOOST_NOOPMPI_STATUS_HPP

#include <optional>
#include <cassert>
#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>

namespace boost { namespace noopmpi {

class request;
class communicator;

/** @brief Contains information about a message that has been or can
 *  be received.
 *
 *  But we do not send or receive messages.
 */
class BOOST_MPI_DECL status
{
 public:
  template<typename T> status(T const& s) {}
  status() {}
  
  /**
   * Retrieve the source of the message.
   */
  int source() const { assert(0); return -1; }

  /**
   * Retrieve the message tag.
   */
  int tag() const { assert(0); return -1; }

  /**
   * Retrieve the error code.
   */
  int error() const { assert(0); return -1; }

  /**
   * Determine whether the communication associated with this object
   * has been successfully cancelled.
  */
  bool cancelled() const { assert(0); return false; }

  /**
   * Determines the number of elements of type @c T contained in the
   * message. 
   * There is no message.
   */
  template<typename T> std::optional<int> count() const { return std::nullopt; }

};

}} 

#endif // BOOST_NOOPMPI_STATUS_HPP
