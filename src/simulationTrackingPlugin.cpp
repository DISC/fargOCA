// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <highfive/H5File.hpp>
#include <nlohmann/json.hpp>

#include "simulationTrackingPlugin.hpp"

#include "io.hpp"
#include "h5io.hpp"
#include "pluginImpl.hpp"

namespace fargOCA {
  template class Plugin<SimulationTracking,std::string, Simulation const&, SimulationEvent>;
}
