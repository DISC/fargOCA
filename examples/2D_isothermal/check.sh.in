#!/usr/bin/env bash
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

nmpi=$1
post_dir=@CMAKE_BINARY_DIR@/utils
outdir=@CMAKE_CURRENT_BINARY_DIR@/out$nmpi
golddir=@CMAKE_CURRENT_SOURCE_DIR@/out
export PYTHONPATH=$PYTHONPATH:@CMAKE_SOURCE_DIR@/tools/check

set -x

failed=0
for i in 0 5 10
do
    echo Checking Step $i
    $post_dir/disk_diff $outdir/disk$i.h5  $golddir/disk$i.h5 --verbose --all \
	--atol=1e-14 --rtol=1e-13 \
	velocity/radial:2e-13:5e-9 \
	density:5e-15:1e-12 \
	pressure:5e-17:12e-12 \
	velocity/theta:1e-13:5e-13  || let failed++
    $post_dir/disk_radial_flow $outdir/disk$i.h5 --delta 0.075701 --output $outdir/gasflow$i.dat  || let failed++
    @CMAKE_BINARY_DIR@/utils/column_diff --verbose \
	$golddir/gasflow$i.dat $outdir/gasflow$i.dat \
	1e-14:1e-14 1e-12:5e-8 \
	|| let failed++
done

@CMAKE_BINARY_DIR@/utils/column_diff --verbose \
    $golddir/monitor.dat $outdir/monitor.dat \
    1e-12:1e-11 1e-12:1e-11 1e-12:1e-11 1e-12:1e-9 1e-12:1e-8 1e-12:1e-9 \
    || let failed++

if [[ $failed -gt 0 ]]
then 
    echo FAILED
    exit 1
else
    echo PASSED
    exit 0
fi
