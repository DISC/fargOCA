// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGO_HILL_FORCE_H
#define FARGO_HILL_FORCE_H

#include <boost/format.hpp>

#include <highfive/H5File.hpp>

#include "mpidatatype.hpp"

#include "precision.hpp"
#include "tuple.hpp"
#include "scalarField.hpp"
#include "planetarySystem.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"

namespace fargOCA {
  /// \brief A tribute to George W. Hill (1838-1914)
  /// Details on the forces that the disk applies on a planet 
  /// divide as inner and outer disk (the force arising from each
  /// part is evaluated separately) and then subdivided into 
  /// the force arising from all the disk material and the force that
  /// excludes part of the Hill sphere. 
  struct HillForce {
    struct HillAreas {
      Triplet complete;      /**< Without Hill sphere avoidance  */ 
      Triplet noHillSphere;  /**< With Hill sphere avoidance  */

      KOKKOS_INLINE_FUNCTION
      HillAreas() : complete(), noHillSphere() {}
      HillAreas(Triplet const& c, Triplet const& h) : complete(c), noHillSphere(h) {}
      HillAreas(HillAreas&& other) = default;
      HillAreas(HillAreas const& other) = default;
      HillAreas& operator=(HillAreas &&) = default;
      HillAreas& operator=(HillAreas const&) = default;
      KOKKOS_INLINE_FUNCTION
      HillAreas& operator+=(HillAreas const& o) { complete += o.complete; noHillSphere += o.noHillSphere; return *this;}
    };
    
    HillAreas inner;     /**< Force arising from the inner disk */
    HillAreas outer;     /**< Force arising from the outer disk */

    KOKKOS_INLINE_FUNCTION
    HillForce() : inner(), outer() {}
    HillForce(Disk const& disk, std::string planet);
    HillForce(HillForce&& other) = default;
    HillForce(HillForce const& other) = default;
    HillForce(HillAreas const& i, HillAreas const& o) : inner(i), outer(o) {}
    HillForce(Triplet const& ic, Triplet const& ih,
              Triplet const& oc, Triplet const& oh) : inner(ic,ih), outer(oc,oh) {}
    HillForce& operator=(HillForce&& other) = default;
    HillForce& operator=(HillForce const& other) = default;
    KOKKOS_INLINE_FUNCTION
    HillForce& operator+=(HillForce const& o) { inner += o.inner; outer += o.outer; return *this; }
    
    void operator*=(real f);

  private:
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* version */)
    {
      ar & inner.complete;
      ar & inner.noHillSphere;
      ar & outer.complete;
      ar & outer.noHillSphere;
    }
  };
  
  HillForce operator+(HillForce const& f1, HillForce const& f2);
  
  /// \brief Compute the torque contribution from all radii.
  /// \returns a pair of <complete,no Hill sphere> contribution
  std::pair<arr1d<real>, arr1d<real>> GetTorqueContribs(Disk const& disk, std::string planet);
  
  std::ostream& operator<<(std::ostream& out, HillForce const& h);

  struct TimeStampedHillForce {
    real      time;
    Planet    planet;
    HillForce force;
    
    static void writeH5Log(HighFive::Group root, std::string name, std::vector<TimeStampedHillForce> const& log);
    static void writeH5Logs(HighFive::Group root, std::map<std::string, std::vector<TimeStampedHillForce>> const& log);
    static std::vector<TimeStampedHillForce> readH5Log(HighFive::Group root, std::string name);
    static std::map<std::string, std::vector<TimeStampedHillForce>> readH5Logs(HighFive::Group root);
  };
}

#if defined(ENABLE_PARALLEL)
namespace boost {
  namespace mpi {
    template <>
    struct is_mpi_datatype<fargOCA::HillForce> : mpl::true_ { };
  } 
}
#endif

template<> HighFive::DataType HighFive::create_datatype<fargOCA::HillForce::HillAreas>();
template<> HighFive::DataType HighFive::create_datatype<fargOCA::HillForce>();
template<> HighFive::DataType HighFive::create_datatype<fargOCA::TimeStampedHillForce>();

#endif
