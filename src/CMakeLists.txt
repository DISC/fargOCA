# Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
# Copyright 2018, Clément Robert, clement.robert<at>oca.eu
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

configure_file(gen_version.sh.in gen_version.sh  @ONLY)

add_custom_target(genversion
  ${CMAKE_CURRENT_BINARY_DIR}/gen_version.sh ${CMAKE_CURRENT_BINARY_DIR}/version.cpp)

set_source_files_properties(version.cpp PROPERTIES GENERATED TRUE)

configure_file(fargoConfig.hpp.in fargoConfig.hpp @ONLY)

set(FARGO_RUNTIME_SRCS
  arrayTestUtils.cpp
  arraysUtils.cpp
  boundariesConditionsPlugin.cpp
  cartesianGridCoords.cpp
  codeUnits.cpp
  config.cpp
  configLoader.cpp
  debug.cpp
  disk.cpp
  diskInitPlugin.cpp
  diskPhysic.cpp
  diskProfiles.cpp
  environment.cpp
  fieldStack.cpp
  gasShape.cpp
  grid.cpp
  gridCellSizes.cpp
  gridDispatch.cpp
  gridSpacing.cpp
  h5io.cpp
  h5Labels.cpp
  hillForce.cpp
  io.cpp
  legacyStellarRadiationSolver.cpp
  log.cpp
  loops.cpp
  momentum.cpp
  physicExtractor.cpp
  physicIO.cpp
  planetarySystem.cpp
  planetarySystemEngine.cpp
  planetarySystemPhysic.cpp
  plugin.cpp
  pluginConfig.cpp
  rebind.cpp
  scalarField.cpp
  scalarFieldInterpolators.cpp
  simulation.cpp
  simulationConfig.cpp
  simulationStepPlugin.cpp  
  simulationTrackingPlugin.cpp
  simulationTrackingPlugins.cpp
  stellarRadiationSolver.cpp
  transportEngine.cpp
  tuple.cpp
  vectorField.cpp
  version.cpp
  viscosityFieldPlugin.cpp
  viscosityTensor.cpp
  )

add_library(fargort SHARED ${FARGO_RUNTIME_SRCS})
set_target_properties(fargort PROPERTIES POSITION_INDEPENDENT_CODE 1)
add_dependencies(fargort genversion)

target_link_libraries(fargort 
  PUBLIC
  Kokkos::kokkos
  Boost::headers
  Boost::serialization
  Boost::program_options
  Boost::chrono
  Boost::regex
  HighFive
  ${HDF5_C_LIBRARIES}
  nlohmann_json::nlohmann_json
  m)

if(ENABLE_PETSC) 
  target_link_libraries(fargort PUBLIC petsc)
endif()

if(ENABLE_PARALLEL)
  target_link_libraries(fargort 
    PUBLIC
    Boost::mpi
    ${MPI_C_LIBRARIES})
else()
  add_subdirectory(noopmpi)
  target_link_libraries(fargort 
    PUBLIC
    noopmpi)
endif()


if (Python3_Development_FOUND)
  add_subdirectory(fargOCApy)
else()
  message(WARN "  Skipping fargOCApy.")
endif()

if (USE_SYMBA7)
  add_subdirectory(swift)
  add_subdirectory(symba7)
  target_link_libraries(fargort PUBLIC symba7)
endif()
