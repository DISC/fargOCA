// Copyright 2025, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "arrayTestUtils.hpp"

#include "Kokkos_Core.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;

#define LBD KOKKOS_LAMBDA
  
  /// \brief Returns the averaged profile of a 3D field overs sectors
  void
  averageOnTheta(arr3d<real const> field, arr2d<real> profile) {
    size_t const nr {field.extent(0)};
    size_t const ni {field.extent(1)};
    size_t const ns {field.extent(2)};
    assert(nr == profile.extent(0) && ni == profile.extent(1));
    kk::parallel_for("average_over_sectors", kk::TeamPolicy<>(nr, kk::AUTO),
                     LBD(kk::TeamPolicy<>::member_type const& team) {
                       int i = team.league_rank();
                       kk::parallel_for(kk::TeamThreadRange(team, ni),
                                        [&](int h) {
                                          kk::parallel_reduce(kk::ThreadVectorRange(team, ns),
                                                              [&](int j, real& lNsSum) { lNsSum += field(i,h,j); },
                                                              profile(i,h));
                                          kk::single(kk::PerThread(team), [&]() { profile(i,h) /= ns;});
                                        });
                     });
  }

  arr2d<real>
  averageOnTheta(arr3d<real const> field) {
    size_t const nr {field.extent(0)};
    size_t const ni {field.extent(1)};
    std::string label {field.label()+"_profiled2d"};
    arr2d<real> profile(label, nr, ni);
    averageOnTheta(field, profile);
    return profile;
  }

  /// \brief Returns the flattened averaged profile of a 3D field overs sectors 
  void
  averageOnThetaPhi(arr3d<real const> field, arr1d<real> profile) {
    size_t const nr {field.extent(0)};
    size_t const ni {field.extent(1)};
    size_t const ns {field.extent(2)};
    assert(nr == profile.extent(0));
    kk::parallel_for(kk::TeamPolicy<>(nr, kk::AUTO),
                     LBD(kk::TeamPolicy<>::member_type const& team) {
                       int i = team.league_rank();
                       kk::parallel_reduce(kk::TeamThreadRange(team, ni),
                                           [&](int h, real& lisum) {
                                             real nsSum = 0;
                                             kk::parallel_reduce(kk::ThreadVectorRange(team, ns),
                                                                 [&](int j, real& lnsSum) { lnsSum += field(i,h,j); }, nsSum);
                                             lisum += nsSum;
                                           },
                                           profile(i));
                       team.team_barrier();
                       kk::single(kk::PerTeam(team), [&]() { profile(i) /= ns*ni; });
                     });
  }
  
  arr1d<real>
  averageOnThetaPhi(arr3d<real const> field) {
    size_t const nr {field.extent(0)};
    std::string label {field.label()+"_profiled1d"};
    arr1d<real> profile(label, nr);
    averageOnThetaPhi(field, profile);
    return profile;
  }
}
