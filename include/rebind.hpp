// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_REBIND_H
#define FARGOCA_REBIND_H

#include <Kokkos_Array.hpp>
#include "precision.hpp"

namespace fargOCA {
  namespace kk = Kokkos;
  
  KOKKOS_INLINE_FUNCTION
  size_t
  closest_coord_index(real pos, auto coords, size_t first, size_t last) {
    assert(first <= last);
    // Sycl does not like reccursivity
    while (true) {
      if (first == last) {
        return first;
      } else {
        if ((last-first) == 1) {
          if ((pos - coords(first)) < (coords(last) - pos)) {
            return first;
          } else {
            return last;
          }
        } else {
          size_t med = (first+last)/2;
          if (pos < coords(med)) {
            last = med;
          } else {
            first = med;
          }
          continue;
        }
      }
    }
  }

  KOKKOS_INLINE_FUNCTION
  real
  interpolation(Kokkos::Array<real const, 3> xs, Kokkos::Array<real const, 3> ys, real x) {
    Kokkos::Array<real,3> L  {
      (x-xs[1])*(x-xs[2])/((xs[0]-xs[1])*(xs[0]-xs[2])),
      (x-xs[0])*(x-xs[2])/((xs[1]-xs[0])*(xs[1]-xs[2])),
      (x-xs[0])*(x-xs[1])/((xs[2]-xs[0])*(xs[2]-xs[1]))
    };
    return L[0]*ys[0]+L[1]*ys[1]+L[2]*ys[2];
  }
}

#endif

