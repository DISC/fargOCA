#!/usr/bin/env bash

if [[ -d "@CMAKE_SOURCE_DIR@" ]]
then 
    # this is the script in the binary dir
    cd @CMAKE_SOURCE_DIR@
fi

etags -o TAGS $( find include/ -name "*.hpp" ;\
 find src/ -maxdepth 1 -name "*.cpp" ; \
 find utils -name "*.?pp" ; \
 find test -name "*.?pp" ; \
 find src/fargOCApy/ -name "*.?pp" ; \
 echo mainOCA.cpp mainInit.cpp)


