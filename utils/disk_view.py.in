#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import sys
sys.path.append("@CMAKE_BINARY_DIR@/src/pywrap")
sys.path.append("@Boost_LIBRARY_DIRS@")
sys.path.append("@Boost_LIBRARY_DIRS@/boost-python@Python3_VERSION_MAJOR@.@Python3_VERSION_MINOR@")

import numpy as np
import argparse
#import mpi
import fargocapy_seq as fargo
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse

parser = argparse.ArgumentParser(description='''
Display all the fields of a gas disk, possibly averaging along sectors for 3D.
Example:
   {} ref/disk.h5
'''.format(sys.argv[0]))
parser.add_argument('ifile', metavar='GAS.h5', type=str, nargs=1,
                    help='HDF5 gas description file.')
parser.add_argument('fields', metavar='FIELD', type=str, nargs='*',
                    help='(not implemented yet) The explicit list of fields to display. Defaults: all.')

args = parser.parse_args()
ifile = args.ifile[0]

def show(ifile):
    simulation = fargo.Simulation(ifile)
    fields = simulation.disk.fieldsSnapshot
    nb_col   = 3
    nb_line  = (len(fields)+nb_col)/nb_col
    
    for i,(name,field) in enumerate(fields.items()):
        plt.subplot(nb_line, nb_col, i+1)
        plt.title(name)
        arr = field.values
        grid = field.grid
        coords = grid.coords.local
        xymesh = None
        if grid.ni > 1: # 3D, average on sectors
            arr = np.mean(arr, 2)
            xymesh = np.meshgrid(coords.layers, coords.radii)
        else:
            arr = arr[:,0,:]
            xymesh = np.meshgrid(coords.sectors, coords.radii)
        x,y = xymesh
        plt.pcolormesh(y,x,arr,
                       **{ True:  {'cmap' : 'RdBu',    'vmin' : -np.abs(arr.max()), 'vmax' : + np.abs(arr.max())},
                           False: {'cmap' : 'viridis', 'vmin' : arr.min(),          'vmax' : arr.max()}
                       }[ "velocity" in name ])
        plt.axis([y.min(), y.max(), x.min(), x.max()])
        plt.colorbar()        

    plt.show()

env = fargo.Environment(sys.argv)

show(ifile)
