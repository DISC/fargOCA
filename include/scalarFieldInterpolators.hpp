// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_SCALAR_FIELD_INTERPOLATORS_H
#define FARGOCA_SCALAR_FIELD_INTERPOLATORS_H

#include <functional>
#include <string>
#include <vector>

#include "precision.hpp"

namespace fargOCA {
  class ScalarField;
  
  /// \brief Provides functions that interpolate the field values.
  /// Typically used when rebinding on new grid.
  /// Instances of this class are stateless.
  class ScalarFieldInterpolators {
  public:
    std::string name() const { return myName; }
    /// \brief a function that interpolate on 1 line
    typedef std::function<real (real)> Fct;
    /// \brief radial values for layer h sector j
    virtual Fct  radial(ScalarField const& f, int h, int j) const = 0;
    /// \brief phi values for radius i sector j
    virtual Fct  phi(ScalarField const& f, int i, int j)    const = 0;
    /// \brief theta values for radius i layer h
    virtual Fct  theta(ScalarField const& f, int i, int h)  const = 0;
   
    static ScalarFieldInterpolators const& get(std::string name);
    static std::string defaultName() { return "barycentric_rational_order3"; }
    static ScalarFieldInterpolators const& get() { return get(defaultName());}
    
    static std::vector<std::string> availables();

  protected:
    ScalarFieldInterpolators(std::string name);
    virtual ~ScalarFieldInterpolators();
    ScalarFieldInterpolators(ScalarFieldInterpolators const&) = delete;
  
  private:
    std::string myName;
  };
}

#endif
