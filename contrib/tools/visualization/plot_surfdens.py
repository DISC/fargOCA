#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import math
import sys
import argparse



parser = argparse.ArgumentParser(description='''
Displays the radial surface density (from extract_radial_surf_dens with output files SurfDens_n.dat), provided the initial and final disk numbers'''.format(sys.argv[0]))

parser.add_argument('starting_file_number', metavar='Ninit', type=int, 
                    help='Provide the initial disk number')
parser.add_argument('final_file_number', metavar='Nfinal', type=int,     
                    help='Provide the final disk number' )
parser.add_argument('R0', nargs='?', type=float, default=5.2, 
                    help='Provide the unit of distance default 5.2(AU)')
parser.add_argument('step', metavar='Step', type=int,
                    help='Provide the step between output files to be plotted' )
parser.add_argument('-s','--scale', dest='plot_scale',choices=['log'],default='lin',
                    help='choose log for logscale plot, default linear')


args = parser.parse_args()
ninit = args.starting_file_number
nfinal=args.final_file_number
R0scale = args.R0


diskinit='./disk'+str(ninit)+'.h5'
data = h5.File(diskinit,'r')
steps_per_output = data['/'].attrs['steps_per_output']
nfiles=(int)((nfinal-ninit)/steps_per_output/args.step)
print('Number of processed files:',nfiles)


#fig,ax =plt.subplots()


plt.xlabel('r (AU)')
plt.ylabel('$\\Sigma (g/cm^2)$')
for i in  range(ninit,nfinal,steps_per_output*args.step):
      data = 'disk'+str(i)+'.h5'
      disk=h5.File(data,'r')
      surfdens='SurfDens'+str(i)+'.dat'
      m=np.loadtxt(surfdens)   
      r=m[:,0];sigma=m[:,1]
      timeyears = int(disk['/'].attrs['physical_time']*math.pow(R0scale,1.5)/math.pi/2.)
      if(args.plot_scale == 'log'):  # log scale 
        f = np.log10(sigma)
        plt.plot(r*R0scale,f,label='time (y)={0:d}'.format(timeyears),linewidth=1.5)
      else: # linear scale
        plt.plot(r*R0scale,sigma,label='time (y)={0:d}'.format(timeyears),linewidth=1.0)

plt.legend(loc=1)
plt.grid(True,linestyle='--')

plt.show()
