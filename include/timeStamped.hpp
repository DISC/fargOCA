// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_TIME_STAMPED_H
#define FARGOCA_TIME_STAMPED_H

#include <iostream>
#include <string>

#include <highfive/H5File.hpp>

#include "precision.hpp"
#include "h5io.hpp"

namespace fargOCA {
  /// \brief a generic time stamped data 
  template <typename T>
  struct TimeStamped {
    real time;
    T    state;
  };

  std::string const PHYSICAL_TIME = "physical_time"; 

  template <typename T>
  std::ostream& operator<<(std::ostream& out, TimeStamped<T> const& ts) {
    out << "[ts:" << ts.time << ", state:" << ts.state << "]";
    return out;
  }

  template<class T>
  HighFive::DataType create_datatype_TimeStamped() {
    using namespace HighFive;
    using namespace fargOCA;
    static HighFive::CompoundType ct({
        {PHYSICAL_TIME, create_datatype<real>()},
          {"state", create_datatype<T>()}});
    return ct;
  }
}
#endif
