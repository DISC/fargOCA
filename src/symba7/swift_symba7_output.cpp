// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>

#include <highfive/H5File.hpp>

#include "fortran_API.hpp"

#include "planetarySystem.hpp"

constexpr int NTPMAX = 1001; // max number of test particles
constexpr int NSTATP = 3;    // Number of status parameters
constexpr int NPLMAX = 51;   // max number of planets, including the Sun 
constexpr int NSTAT  = NSTATP + NPLMAX - 1; // Number of status parameters

template<typename T> char const* buf(T *o) { return static_cast<char*>(static_cast<void*>(o)); }

extern "C" 
void FC_GLOBAL(io_write_frame_r, IO_WRITE_FRAME_R)(double *time, int *nbod, int *ntp, 
                                                   double *mass,
                                                   double *xh,  double *yh,  double *zh,
                                                   double *vxh, double *vyh, double *vzh,
                                                   double *xht, double *yht, double *zht,
                                                   double *vxht,double *vyht,double *vzht,
                                                   int    *istat,
                                                   char   *oname,
                                                   int    *iu,
                                                   char   *fopenstat) {
  static_assert(sizeof(double) == 8, "Double is not compatible with Fortran real*8");
  using namespace fargOCA;
  using namespace std;
  using namespace HighFive;

  static string ofname;
  if (ofname.size() == 0) {
    ofname = "system.h5";
  }
  File ofile(ofname, File::OpenOrCreate);
  Group  root = ofile.getGroup("/");
  map<string, vector<TimeStamped<Planet>>> state;
  for(int i= 0; i < *nbod; ++i) {
    ostringstream pname;
    pname << "body" << i;
    state[pname.str()].push_back({*time, Planet(mass[i], Triplet(xh[i], yh[i], zh[i]), Triplet(vxh[i], vyh[i], vzh[i]))});
  }
  PlanetarySystem::writeH5Logs(root, state);
}
