// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <ostream>
#include <fstream>
#include <iomanip>
#include <cerrno>
#include <cstring>
#include <filesystem>
#include <memory>
#include <chrono>

#include <boost/format.hpp>
#include <highfive/H5File.hpp>

#include "simulationTrackingPlugins.hpp"
#include "simulation.hpp"
#include "disk.hpp"
#include "codeUnits.hpp"
#include "h5io.hpp"
#include "hillForce.hpp"
#include "planetarySystem.hpp"
#include "momentum.hpp"
#include "log.hpp"

namespace fs = std::filesystem;
namespace HF = HighFive;

namespace fargOCA {
  namespace {
    /// \brief Allow a tracker to retrieve data specific to a simulation without having to store a state.
    /// We only keep weak point to simulation since trackers are usually static.
    template <typename T>
    using TrackerData = std::map<std::weak_ptr<Simulation const>, T, std::owner_less<std::weak_ptr<Simulation const>>>;
    auto weak(Simulation const& s) { return std::weak_ptr<Simulation const>(s.shared()); };
  }

  namespace {
    void
    HillForceTxtLogger (std::string name,
                        Simulation const& simulation,
                        SimulationEvent event)  {
      using SystemLog = std::map<std::string, std::vector<std::vector<real>>>;
      static TrackerData<SystemLog> systemLogs;
      
      auto const&       cfg        = simulation.trackingConfig().config(name);
      bool const        master     = simulation.disk().dispatch().master();
      std::ostream&     out        = fargOCA::log(master && cfg.value("verbose", false), std::cout);
      std::string const ofname     = simulation.opath(cfg.value<std::string>("ofile", "obsolete_hill_force_logger_use_hdf5_version_instead.txt"));
      int         const saveModulo = cfg.value("save_modulo", 1);
      
      auto log = [&]() -> void {
        SystemLog& log{systemLogs[weak(simulation)]};
        for (auto const& [name, planet] : simulation.disk().system()->planets()) {
          Triplet position = planet.position();
          HillForce fc(simulation.disk(), name);
          if (master) {
            real const x = position.x;
            real const y = position.y;
            std::vector<real> line;
            line.push_back(x*fc.inner.complete.y-y*fc.inner.complete.x);
            line.push_back(x*fc.outer.complete.y-y*fc.outer.complete.x);
            line.push_back(x*fc.inner.noHillSphere.y-y*fc.inner.noHillSphere.x);
            line.push_back(x*fc.outer.noHillSphere.y-y*fc.outer.noHillSphere.x);
            line.push_back(simulation.disk().physicalTime()-simulation.userTimeStep());
            log[name].push_back(line);
          }
        }
      };
      
      auto flush = [&]() -> void {
        SystemLog& log{systemLogs[weak(simulation)]};
        if (!log.empty()) {
          if (master) {
            for (auto const& [name, lines] : log) {
              std::ofstream ofile(ofname, std::ios::out | std::ios::app | std::ios::ate);
              ofile << std::scientific << std::setprecision(10);
              for (auto const& line : lines) {
                ofile << simulation.step() << '\t'; // this could be put in the log data
                std::copy(line.begin(), line.end(), std::ostream_iterator<real>(ofile, "\t"));
                ofile << '\n';
              }
            }
            log.clear();
          }
        }
      };
      
      auto init = [&]() {
        if (!simulation.disk().system()) {
          out << "Also, there is no system to track. Please disable and restart.\n";
          simulation.disk().dispatch().comm().abort(-1);
        }
        if (simulation.disk().dispatch().grid().dim() == 2) {
          out << "This tracker won't work very well on flat disk.\n";
        }
        systemLogs[weak(simulation)] = SystemLog{};
      };
      
      switch(event) {
      case SimulationEvent::STARTUP:
        init();
        break;
      case SimulationEvent::AFTER_USER_STEP:
        log();
        if (simulation.step() % saveModulo == 0) {
          flush();
        }
        break;
      default:
        break;    
      }
    }
  }
  SimulationTrackingPlugin GeorgeForceTxtLogger{"Hill_force_txt_logger",
                                                HillForceTxtLogger,
                                                "You have selected the tracker Hill_force_txt_logger.\n"
                                                "This is osolete, please move to 'Hill_force_hdf5_logger' tracker  "
                                                " along with the 'planet_disk_force' utility.",
                                                {"verbose", "ofile", "save_modulo", "flush"}};
  
  SimulationTrackingPlugin GeorgeForceHdf5Logger{
    "Hill_force_hdf5_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) {
      using SystemLog = std::map<std::string, std::vector<TimeStampedHillForce>>;
      static TrackerData<SystemLog> logs;
      
      auto const&       cfg    = simulation.trackingConfig().config(name);
      bool const        master = simulation.disk().dispatch().master();
      std::ostream&     out        = fargOCA::log(master && cfg.value("verbose", false), std::cout);
      std::string const ofname     = simulation.opath(cfg.value("ofile", "Hill.h5"));
      std::string const when       = cfg.value("when", "step");
      int         const saveModulo = cfg.value("save_modulo", 1);
      Disk const& disk = simulation.disk();
      
      auto log = [&]() -> void {
        SystemLog& plog = logs[weak(simulation)];
        for (auto const& [name,planet] : disk.system()->planets()) {
          out << "Log planet " << name << " at time " << disk.physicalTime() << '\n';
          plog[name].push_back({disk.physicalTime(), planet, HillForce(disk, name)});
        }
      };
      
      auto flush = [&]() -> void {
        SystemLog& plog = logs[weak(simulation)];
        out << "Updating '" << ofname << "'\n";
        if (master) {
          HF::File ofile(ofname, HF::File::ReadWrite);
          auto root = ofile.getGroup("/");
          TimeStampedHillForce::writeH5Logs(root, plog);
        }
        plog.clear();
      };
      
      switch (event) {
      case SimulationEvent::STARTUP:
        logs[weak(simulation)] = SystemLog{};
        simulation.disk().dispatch().comm().barrier();
        if (!fs::exists(ofname) && master) {
          out << "Creating '" << ofname << "'\n";
          HF::File ofile(ofname, HF::File::Overwrite);
        }
        simulation.disk().dispatch().comm().barrier();
        if (simulation.step() == 0) { log(); } // not a restart
        break;
      case SimulationEvent::AFTER_USER_STEP:
        if (when == "step") { log(); }
        if (simulation.step() % saveModulo == 0) { flush(); }
        break;
      case SimulationEvent::PLANETS_CHANGED:
        if (when == "changed") { log(); }
        break;
      default:
        break;
      }
    },
    "Keep track of the planets Hill forces in a compact HDF5 file.",
    {"verbose", "ofile", "save_modulo","when","flush"}
  };
  
  SimulationTrackingPlugin stepTimer{
    "step_timer",
    [](std::string name, Simulation const& simulation, SimulationEvent event) {
      using time_point = std::chrono::time_point<std::chrono::system_clock>;
      static TrackerData<time_point> starts;
      
      using namespace std::literals;
      
      switch (event) {
      case SimulationEvent::BEFORE_USER_STEP:
        starts[weak(simulation)] = std::chrono::system_clock::now();
        break;
      case  SimulationEvent::AFTER_USER_STEP:
        {
          auto const&      cfg {simulation.trackingConfig().config(name)};
          time_point const now {std::chrono::system_clock::now()};
          std::chrono::duration<double> const elapsed{now - starts[weak(simulation)]};
          if (simulation.disk().dispatch().master()) {
            std::cout << "In step " << simulation.step() << ": " << elapsed/1s << 's';
            if (cfg.enabled("physical")) {
              std::cout << ", physical: " << simulation.disk().physicalTime();
            }
            std::cout << std::endl;
          }
        }
        break;
      default:
        break;
      }
    },
    "Print the time spent in each user step",
    {"verbose", "physical"}
  };
  
  namespace {
    bool 
    userStepBoundary(SimulationEvent event, int step) {
      bool const firstStep = event == SimulationEvent::STARTUP && (step == 0);
      return firstStep || event == SimulationEvent::AFTER_USER_STEP;
    }
  }

  SimulationTrackingPlugin gasTxtLogger{
    "gas_txt_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      auto const&       cfg     = simulation.trackingConfig().config(name);
      bool const        master  = simulation.disk().dispatch().master();
      std::string       ofname  = simulation.opath(cfg.value("ofile", "gas.txt"));
      
      if (userStepBoundary(event, simulation.step())) {
        Disk const& disk = simulation.disk();
        // MPI global operation
        real const physicalTime  = disk.physicalTime();
        real const momentum      = disk.momentum();
        real const totalMass     = disk.totalMass();
        real const totalEnergy   = disk.totalEnergy();
        real const kineticRadial = disk.kineticRadial();
        real const kineticPolar  = disk.kineticPolar();
        
        if (master) {
          std::ofstream out(ofname, std::ofstream::app | std::ofstream::ate | std::ofstream::out);
          out.precision(12);
          out << std::scientific
              << physicalTime  << '\t' << momentum
              << '\t' << totalMass << '\t' << totalEnergy
              << '\t' << kineticRadial << '\t' << kineticPolar << std::endl;
        }
      }
    },
    "Log global disk properties (momentum, mass, velocity,..) in a file.",
    {"ofile"}
  };

  SimulationTrackingPlugin gasTracer{
    "gas_tracer",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      if (userStepBoundary(event, simulation.step())) {
        Disk const& disk{simulation.disk()};
        real const momentum      = disk.momentum();
        real const totalMass     = disk.totalMass();
        if (simulation.disk().dispatch().master())
          std::cout  << "Disk Momentum   : " << momentum << "\n"
                     << "Disk total Mass : " << totalMass << std::endl;
      }
    },
    "log momentum and mass on standard outpout",
    {}
  };
  
  namespace {
    void
    logPlanets(std::string name, Simulation const& simulation, SimulationEvent event, std::string ofname) {
      using SystemLog = std::map<std::string, std::vector<TimeStamped<Planet>>>;
      using NamedLogs = std::map<std::string, SystemLog>;
      static TrackerData<NamedLogs> logs;
      
      auto const&       cfg     = simulation.trackingConfig().config(name);
      bool const        master = simulation.disk().dispatch().master();
      std::ostream&     out    = fargOCA::log(master && cfg.value("verbose", false), std::cout);
      std::string const when  = cfg.value<std::string>("when", "step");
      bool const        flush = cfg.enabled("flush");
      bool const        verbose = cfg.enabled("verbose");
      int const         saveModulo = cfg.value("save_modulo", 1);
      
      auto print = [&]() -> void {
        SystemLog& plog = logs[weak(simulation)][name];
        if (master) {
          HF::File ofile(ofname, HF::File::ReadWrite);
          PlanetarySystem::writeH5Logs(ofile.getGroup("/"), plog);
        }
        plog.clear();
        out << "Updating file " << ofname << std::endl;
      };
      
      auto log = [&]() -> void {
        SystemLog& plog = logs[weak(simulation)][name];
        simulation.disk().system()->log(simulation.disk().physicalTime(), plog);
        if (flush) { print(); }
      };
      
      switch (event) {
      case SimulationEvent::STARTUP:
        if (!logs.contains(weak(simulation))) {
          logs[weak(simulation)] = NamedLogs{};
        }
        logs[weak(simulation)][name] = SystemLog{};
        
        if (!simulation.disk().system()) {
          out << "Sorry, cannot ask for planet tracking when no planet are present.\n";
          simulation.disk().dispatch().comm().abort(-1);
        }
        if (!fs::exists(ofname) && master) {
          HF::File ofile(ofname, HF::File::Overwrite);
        }
        if (simulation.step() == 0) { log();  } // not a restart
        break;
      case SimulationEvent::AFTER_USER_STEP:
        if (when == "step") { log(); }
        if (simulation.step() % saveModulo == 0) { print(); }
        break;
      case SimulationEvent::PLANETS_CHANGED:
        if (when == "changed") { log(); }
        break;
      default:
        break;
      }
    }
  }
  
  SimulationTrackingPlugin deprecatedPlanetTxtLogger{
    "planets_txt_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      logPlanets(name, simulation, event, "planets_fdh5_fwd.h5");
    },
    "Please stop using 'planets_txt_logger' and use 'planets_txt_logger' instead. "
    "output is forced in 'planets_fdh5_fwd.h5'",
    {"verbose", "flush", "when", "save_modulo"}
  };
  
  SimulationTrackingPlugin planetsHdf5Logger{
    "planets_hdf5_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      auto const&       cfg     = simulation.trackingConfig().config(name);
      std::string       ofname  = simulation.opath(cfg.value("ofile", "planets.h5"));
      logPlanets(name, simulation, event, ofname);
    },
    "Log planets in a compact, possibly fine grained, HDF5 format.",
    {"verbose", "flush", "when", "save_modulo", "ofile"}
  };
  
  SimulationTrackingPlugin simulationHdf5Logger{
    "hdf5_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      auto const&   cfg    = simulation.trackingConfig().config(name);
      bool const    master = simulation.disk().dispatch().master();
      std::ostream& out    = fargOCA::log(master && cfg.value("verbose", false), std::cout);
      
      if (event == SimulationEvent::STARTUP) {
        auto check = [&]() -> bool {
          int modulo                       = cfg.value("modulo", 1);
          std::optional<int> restartModulo = cfg.value<int>("restart_modulo");
          std::optional<bool> keepLast     = cfg.value<bool>("keep_last");
          if (modulo <= 0) {
            out << "Illegal value for 'modulo'.\n";
            return false;
          }
          if (bool(restartModulo) && bool(keepLast)) {
            out << "'restart_modulo' and 'keep_last' are redundant.\n";
            if (*keepLast) {
              out << "Force 'restart_modulo' to 1.\n";
              restartModulo = 1;
            } else {
              out << "Ignore 'keep_last'.\n";
            }
          }
          if (bool(restartModulo) && *restartModulo <= 0) {
            out << "Illegal value for 'restart_modulo'.\n";
            return false;
          }
          if (bool(restartModulo) && modulo < *restartModulo) {
            out << "Restart point frequency is no higher than disk snapshop frequency. Ignoring.\n";
            restartModulo = std::nullopt;
          }
          return true;
        };
        if (!check()) {
          std::abort();
        }
      } else if (event == SimulationEvent::AFTER_USER_STEP) {
        
        auto ofname = [&](int idx) -> std::string {
          boost::format fnameFmt(simulation.opath(cfg.value("ofile_fmt", "disk%1%.h5")));
          return (fnameFmt%idx).str();
        };
        
        auto save = [&]() -> void {
          std::string fname    = ofname(simulation.step());
          std::string tmpfname = fname + "-tmp";
          {
            auto file = h5::createFile(tmpfname, master);
            simulation.writeH5(h5::getRoot(file));
          }
          if (master) {
            fs::create_hard_link(tmpfname, fname);
            fs::remove(tmpfname);
          }
          out  << "Writing disk file '" << fname << "'\n";
        };
        
        std::optional<int> checkpointEvery = cfg.value<int>("restart_modulo");
        int                saveEvery       = *cfg.value<int>("modulo");
        if (cfg.value("keep_last", false)) { checkpointEvery = 1; }      
        
        if (bool(checkpointEvery) && (simulation.step() % *checkpointEvery == 0)) {
          save();
          int lastCheckpoint = simulation.step() - *checkpointEvery;
          if (lastCheckpoint % saveEvery != 0) { // don't touch if legit
            if (master) {
              std::string lastFname = ofname(lastCheckpoint);
              if (fs::exists(lastFname)) {
                fs::remove(lastFname);
              }
            }
          }
        } else if(simulation.step() % saveEvery == 0) {
          save();
        }
      }
    },
    "This is the main tracker, it will log whole simulation state.",
    {"restart_modulo", "modulo", "ofile_fmt", "verbose", "keep_last"}
  };

  SimulationTrackingPlugin planetaryAccretionLogger{
    "planetary_accretion_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      if (event == SimulationEvent::GAS_ACCRETION) {
        auto const&   cfg    = simulation.trackingConfig().config(name);
        bool const    master = simulation.disk().dispatch().master();
        for (auto const& [planet, momentum] : simulation.accretedGas()) { 
          if (master) {
            boost::format fmt(cfg.value("ofile_fmt", "accretion%1%.log"));
            std::string   ofilename   = simulation.opath((fmt%planet).str());
            std::ofstream output(ofilename, std::ios::out | std::ios::app);
            output << simulation.step() << " " << simulation.disk().physicalTime() << " " << momentum << '\n'; 
          }
        }
      }
    },
    "Track the gas accreted on the planets",
    {"ofile_fmt"}
  };
  
  SimulationTrackingPlugin dtLogger{
    "dt_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      auto const&       cfg    = simulation.trackingConfig().config(name);
      bool const        master = simulation.disk().dispatch().master();
      std::string       ofname = simulation.opath(cfg.value("ofile", "dt.txt"));
      
      if (master) {
        if (event == SimulationEvent::BEFORE_USER_STEP) {
          std::ofstream output(ofname, std::ios::out | std::ios::app);
          output << ">>> step " << simulation.step() << '\n';
        }
        else if (event == SimulationEvent::BEFORE_USER_STEP) {
          std::ofstream output(ofname, std::ios::out | std::ios::app);
          output << simulation.computedTimeStep().value_or(std::numeric_limits<real>::quiet_NaN()) << '\n';
        }
      }
    },
    "Keep track of the computed dt",
    {"ofile"}
  };
  
  SimulationTrackingPlugin gasAccelerationLogger{
    "acceleration_from_gas",
    [](std::string name, Simulation const& simulation, SimulationEvent event) -> void {
      auto const&       cfg    = simulation.trackingConfig().config(name);
      bool const        master = simulation.disk().dispatch().master();
      std::optional<std::string>  ofname = cfg.value<std::string>("ofile");
      
      auto print = [&](std::ostream& out, Triplet acceleration) -> void {
        out << simulation.disk().physicalTime() << '\t' << acceleration << '\n';
      };
      
      if (userStepBoundary(event, simulation.step())) {
        Triplet acceleration = simulation.disk().computeGasAcceleration();
        std::optional<std::string> ofname = cfg.value<std::string>("ofile");
        if (master) {
          if (ofname) {
            std::ofstream out(simulation.opath(*ofname), std::ofstream::app | std::ofstream::ate | std::ofstream::out);
            out.precision(12);
            out << std::scientific;
            print(out, acceleration);
          } else {
            print(std::cout, acceleration);
          }
        }
      }
    },
    "Trace the orbital elements from the system planets.",
    {"ofile", "verbose"}
  };
  
  namespace {
    void
    logOrbitsAux(Simulation const& simulation, Planet const& p, std::ostream& output) {    
      real const epsilon = std::numeric_limits<real>::epsilon();
      real x = p.position().x;
      real y = p.position().y;
      real z = p.position().z;
      
      real vx = p.velocity().x;
      real vy = p.velocity().y;
      real vz = p.velocity().z;
      
      real m = p.mass() + simulation.disk().physic().units.centralBodyMass();
      
      real vdotr = x*vx+y*vy+z*vz;
      real hx = y*vz-z*vy;
      real hy = z*vx-x*vz;
      real hz = x*vy-y*vx;
      real h2 = hx*hx + hy*hy + hz*hz;
      real h  = sqrt(h2);
      real d = sqrt(x*x+y*y+z*z);
      real ener = 0.5*(vx*vx+vy*vy+vz*vz) - m/d;
      real inc = acos(hz/h);
      real tmp = sqrt(hx*hx + hy*hy)/h;
      real capom, u;
      if(tmp <= 1.e-10 ){
        capom = 0.0;
        u = atan2(y,x);
      } else{
        capom = atan2(hx,-hy);
        u = (inc <= epsilon 
             ? PI/2
             : atan2(z/sin(inc) , x*cos(capom) + y*sin(capom)));
      }
      real a = -0.5*m/ener;
      tmp =  1 - h2/(m*a);
      real varpi;
      real e;
      real E;
      if(tmp>=1.e-10){
        e = sqrt(tmp);
        real arg = (1.0-d/a)/e;
        if (fabs(arg) >= 1.0) {
          E = PI*(1.-arg/fabs(arg))/2.;
        } else {
          E = acos(arg);
        }
        if (vdotr < 0) {
          E=-E;
        }
        real cw = (cos(E) -e)/(1.0 - e*cos(E));
        real sw = sqrt(1.0 - e*e)*sin(E)/(1.0 - e*cos(E));
        varpi=atan2(sw,cw);
      } else {
        e=0.0;
        varpi = u;
        E = u;
      }
      real M = E-e*sin(E);
      real omega=u-varpi;
      
      output << simulation.disk().physicalTime() << '\t' << a << '\t' << e
             << '\t' << inc*180/PI << '\t' << M << '\t' << omega << '\t' << capom << '\n';
    }
  }
  
  void
  logOrbits(Simulation const& simulation, boost::format fmt, bool verbose) {
    bool const    master {simulation.disk().dispatch().master()};
    std::ostream& log    {fargOCA::log(master && verbose, std::cout)};

    if (master) {
      using namespace boost;
      for (auto const& [name, planet] : simulation.disk().system()->planets()) {
        std::string ofname = str(fmt%name);
        std::ofstream output(ofname, std::ios::out | std::ios::app);
        output << std::scientific << std::setprecision(12);
        logOrbitsAux(simulation, planet, output);
        log << "Updated '" << ofname << "'\n";
      }
    }
  }
  
  SimulationTrackingPlugin orbitTextLogger{
    "orbital_elements_txt_logger",
    [](std::string name, Simulation const& simulation, SimulationEvent event) {
      auto const&       cfg     {simulation.trackingConfig().config(name)};
      bool const        master  {simulation.disk().dispatch().master()};
      boost::format     ofmt    {simulation.opath(cfg.value<std::string>("ofile_fmt", "orbit%1%.txt"))};
      bool const        verbose {cfg.enabled("verbose")};
      
      if (master) {
        if (userStepBoundary(event, simulation.step())) {
          logOrbits(simulation, ofmt, verbose);
        }
      }
    },
    "Trace the orbital elements from the system planets.",
    {"ofile_fmt", "verbose"}
  };  

}
