// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
// This file is part of FargOCA.
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <nlohmann/json.hpp>

#include "diskPhysic.hpp"
#include "codeUnits.hpp"
#include "configProperties.hpp"
#include "jsonio.hpp"
#include "simulationConfig.hpp"

namespace fargOCA {
  using namespace jsonio;
  using namespace configProperties;
    
  void
  to_json(json& cfg, DiskPhysic::Referential const& ref) {
    set_to(cfg, snake(TYPE), ref.type);
    set_to(cfg, snake(OMEGA), ref.omega);
    set_to(cfg, snake(GUIDING_PLANET), ref.guidingPlanet);
    set_to(cfg, snake(INDIRECT_FORCES), ref.indirectForces);
  }
    
  void
  from_json(json const& cfg, DiskPhysic::Referential& ref) {
    get_to(cfg, snake(TYPE), ref.type);
    get_to(cfg, snake(OMEGA), ref.omega);
    get_to(cfg, snake(GUIDING_PLANET), ref.guidingPlanet);
    get_to(cfg, snake(INDIRECT_FORCES), ref.indirectForces);
  }
    
  void
  to_json(json& j, DiskPhysic::Density::Cavity const& c) {
    set_to(j, snake(RADIUS), c.radius);
    set_to(j, snake(RATIO), c.ratio);
    set_to(j, snake(WIDTH), c.width);
  }
    
  void
  from_json(json const& j, DiskPhysic::Density::Cavity& c) {
    get_to(j, snake(RADIUS), c.radius);
    get_to(j, snake(RATIO), c.ratio);
    get_to(j, snake(WIDTH), c.width);
  }
    
  void
  to_json(json& j, DiskPhysic::Density const& d) {
    set_to(j, snake(CAVITY), d.cavity);
    set_to(j, snake(SLOPE), d.slope);
    set_to(j, snake(START), d.start);
    set_to(j, snake(MINIMUM), d.minimum);
  }
    
  void
  from_json(json const& j, DiskPhysic::Density& d) {
    get_to(j, snake(CAVITY), d.cavity);
    get_to(j, snake(SLOPE), d.slope);
    get_to(j, snake(START), d.start);
    get_to(j, snake(MINIMUM), d.minimum);
  }
    
  void
  to_json(json& j, DiskPhysic::StarAccretion::Wind const& w) {
    set_to(j, snake(ACTIVE_DENSITY), w.userActiveDensity);
    set_to(j, snake(FILTER), w.filter);
  }
    
  void
  from_json(json const& j, DiskPhysic::StarAccretion::Wind& w) {
    get_to(j, snake(ACTIVE_DENSITY), w.userActiveDensity);
    get_to(j, snake(FILTER), w.filter);
  }
    
  void
  to_json(json& j, DiskPhysic::StarAccretion const& s) {
    set_to(j, snake(TYPE), s.type);
    set_to(j, snake(RATE), s.userRate);
    set_to(j, snake(WIND), s.wind);
  }
    
  void
  from_json(json const& j, DiskPhysic::StarAccretion& s) {
    get_to(j, snake(TYPE), s.type);
    get_to(j, snake(RATE), s.userRate);
    get_to(j, snake(WIND),s.wind);
  }
    
  void
  to_json(json& j, DiskPhysic::Viscosity const& v) {
    set_to(j, snake(ARTIFICIAL), v.artificial);
    set_to(j, snake(TYPE), v.type);
    set_to(j, snake(CONFIG), v.config);
  }

  void
  from_json(json const& j, DiskPhysic::Viscosity& v) {
    get_to(j, snake(ARTIFICIAL), v.artificial);
    get_to(j, snake(TYPE), v.type);
    get_to(j, snake(CONFIG), v.config);
  }

  void
  to_json(json& j, DiskPhysic::Adiabatic::Radiative::Star const& s) {
    set_to(j, snake(SHADOW_ANGLE), s.shadowAngle);
  }

  void
  from_json(json const& j, DiskPhysic::Adiabatic::Radiative::Star& s) {
    get_to(j, snake(SHADOW_ANGLE), s.shadowAngle);
  }

  void
  to_json(json& j, DiskPhysic::Adiabatic::Cooling const& c) {
    if (std::holds_alternative<DiskPhysic::Adiabatic::BetaCooling>(c)) {
      to_json(j, std::get<DiskPhysic::Adiabatic::BetaCooling>(c));
    } else if (std::holds_alternative<DiskPhysic::Adiabatic::RadiativeCooling>(c)) {
      to_json(j, std::get<DiskPhysic::Adiabatic::RadiativeCooling>(c));
    } else {
      set_to(j, snake(COOLING), "ERROR");
    }
  }

  void
  from_json(json const& j, DiskPhysic::Adiabatic::Cooling& c) {
    CoolingType type;
    get_to(j, snake(TYPE), type);
    switch (type) {
    case CoolingType::BETA:
      {
	DiskPhysic::Adiabatic::BetaCooling b;
	from_json(j, b);
	c = b;
      }
      break;
    case CoolingType::RADIATIVE:
      {
	DiskPhysic::Adiabatic::RadiativeCooling r;
	from_json(j, r);
	c = r;
      }
      break;
    default:
      std::abort();
    }
  }

  void
  to_json(json& j, DiskPhysic::Adiabatic::Radiative::Solver const& s) {
    set_to(j, snake(LABEL),   s.label);
    set_to(j, snake(CONFIG), s.config);
  }

  void
  from_json(json const& j, DiskPhysic::Adiabatic::Radiative::Solver& s) {
    get_to(j, snake(LABEL), s.label);
    get_to(j, snake(CONFIG), s.config);
  }

  void
  to_json(json& j, DiskPhysic::Adiabatic::Radiative::Opacity const& o) {
    set_to(j, snake(TYPE),   o.type);
    set_to(j, snake(CONFIG), o.config);
  }

  void
  from_json(json const& j, DiskPhysic::Adiabatic::Radiative::Opacity& o) {
    get_to(j, snake(TYPE), o.type);
    get_to(j, snake(CONFIG), o.config);
  }

  void
  to_json(json& j, DiskPhysic::Smoothing const& s) {
    set_to(j, snake(CHANGE), s.change);
    set_to(j, snake(FLAT),   s.flat);
    set_to(j, snake(TAPER),  s.taper);
    set_to(j, snake(SIZE),   s.size);
  }

  void
  from_json(json const& j, DiskPhysic::Smoothing& s) {
    get_to(j, snake(CHANGE), s.change);
    get_to(j, snake(FLAT),   s.flat);
    get_to(j, snake(TAPER),  s.taper);
    get_to(j, snake(SIZE),   s.size);
  }
  
  void
  to_json(json& j, DiskPhysic::Adiabatic::Radiative const& r) {
    set_to(j, snake(SOLVER),  r.solver);
    set_to(j, snake(OPACITY), r.opacity);
    set_to(j, snake(Z_BOUNDARY_TEMPERATURE), r.userZBoundaryTemperature);
    set_to(j, snake(DUST_TO_GAS),      r.dustToGas);
    set_to(j, snake(ENERGY_TIME_STEP),  r.energyTimeStep);
    set_to(j, snake(STAR), r.star);
  }

  void
  from_json(json const& jconfig, DiskPhysic::Adiabatic::Radiative& r) {
    using Radiative = DiskPhysic::Adiabatic::Radiative;
    using Opacity   = Radiative::Opacity;
    using Solver    = Radiative::Solver;
    using Star      = Radiative::Star;
    
    auto    const star           = get_optional<Star>(jconfig, snake(STAR));
    Opacity const opacity        = jconfig.at(snake(OPACITY)).get<Opacity>();
    Solver  const solver         = jconfig.at(snake(SOLVER)).get<Solver>();
    real    const zTemperature   = jconfig.at(snake(Z_BOUNDARY_TEMPERATURE)).get<real>();
    real    const dust2Gas       = jconfig.at(snake(DUST_TO_GAS)).get<real>();
    auto    const energyTimeStep = get_optional<real>(jconfig, snake(ENERGY_TIME_STEP));
    r = Radiative(opacity,
		  solver,
		  zTemperature,
		  dust2Gas,
		  energyTimeStep,
		  star);
  }
  
  void
  to_json(json& j, DiskPhysic::Adiabatic const& a) {
    set_to(j, snake(RADIATIVE), a.radiative);
    set_to(j, snake(COOLING), a.cooling);
    set_to(j, snake(INDEX), a.index);
  }
  
  void
  from_json(json const& jconfig, DiskPhysic::Adiabatic& a) {
    using Adiabatic = DiskPhysic::Adiabatic;
    using Radiative = Adiabatic::Radiative;
    using Cooling   = Adiabatic::Cooling;

    auto const radiative = get_optional<Radiative>(jconfig, snake(RADIATIVE));
    auto const cooling   = get_optional<Cooling>(jconfig, snake(COOLING));
    real const index     = jconfig.at(snake(INDEX)).get<real>();

    a = DiskPhysic::Adiabatic(radiative, cooling, index);
  }

  void
  to_json(json& j, DiskPhysic::Adiabatic::BetaCooling const& c) {
    set_to(j, snake(TYPE), CoolingType::BETA);
    set_to(j, snake(TIMESCALE), c.timeScale);
  }
  
  void
  from_json(json const& j, DiskPhysic::Adiabatic::BetaCooling& c) {
    get_to(j, snake(TIMESCALE), c.timeScale);
  }

  void
  to_json(json& j, DiskPhysic::Adiabatic::RadiativeCooling const& c) {
    set_to(j, snake(TYPE), CoolingType::RADIATIVE);
  }
  
  void
  from_json(json const& j, DiskPhysic::Adiabatic::RadiativeCooling& c) {
  }

  void
  to_json(json& j, DiskPhysic const& p) {
    set_to(j, snake(CODE_UNITS),        p.units);
    set_to(j, snake(GAS_SHAPE),         p.shape);
    set_to(j, snake(REFERENTIAL),       p.referential);
    set_to(j, snake(BOUNDARIES),        p.boundaries);
    set_to(j, snake(INIT),              p.init);
    set_to(j, snake(STEPS),             p.steps);
    set_to(j, snake(STAR_ACCRETION),    p.starAccretion);
    set_to(j, snake(VISCOSITY),         p.viscosity);
    set_to(j, snake(ADIABATIC),         p.adiabatic);
    set_to(j, snake(DENSITY),           p.density);
    set_to(j, snake(SMOOTHING),         p.smoothing);
    set_to(j, snake(PLANETARY_SYSTEM),  p.planetarySystem());
    set_to(j, snake(CFL_SECURITY),      p.CFLSecurity);
    set_to(j, snake(DRAGGING_COEF),     p.draggingCoef);
    set_to(j, snake(HILL_CUT_FACTOR),   p.hillCutFactor);
    set_to(j, snake(TRANSPORT),         p.transport);
    set_to(j, snake(FLARING_INDEX),     p.flaringIndex);
    set_to(j, snake(MEAN_MOLECULAR_WEIGHT), p.meanMolecularWeight);
  }

  void
  from_json(json const& jconfig, std::shared_ptr<DiskPhysic const>& p) {
    using nlohmann::json;
    json jdisk   = jconfig.at(snake(DISK));
    json jphysic = jdisk.at(snake(PHYSIC));
    auto units       = jphysic.at(snake(CODE_UNITS)).get<CodeUnits>();
    auto shape       = jphysic.at(snake(GAS_SHAPE)).get<GasShape>();
    auto referential = jphysic.at(snake(REFERENTIAL)).get<DiskPhysic::Referential>();
    auto boundaries  = jphysic.at(snake(BOUNDARIES)).get<ConfigMap>();
    auto init        = jphysic.at(snake(INIT)).get<ConfigMap>();
    auto steps       = jphysic.at(snake(STEPS)).get<ConfigMap>();
    auto starAcc     = get_optional<DiskPhysic::StarAccretion>(jphysic, snake(STAR_ACCRETION));
    auto viscosity   = jphysic.at(snake(VISCOSITY)).get<DiskPhysic::Viscosity>();
    auto adiabatic   = get_optional<DiskPhysic::Adiabatic>(jphysic, snake(ADIABATIC));
    auto density     = jphysic.at(snake(DENSITY)).get<DiskPhysic::Density>();
    auto smoothing   = jphysic.at(snake(SMOOTHING)).get<DiskPhysic::Smoothing>();
    auto planets     = get_optional<PlanetarySystemPhysic>(jphysic, snake(PLANETARY_SYSTEM));
    auto CFLSecurity = jphysic.at(snake(CFL_SECURITY)).get<real>();
    auto kdrag       = jphysic.at(snake(DRAGGING_COEF)).get<real>();
    auto hillCutFact = jphysic.at(snake(HILL_CUT_FACTOR)).get<real>();
    auto transport           = jphysic.at(snake(TRANSPORT)).get<Transport>();
    auto flaringIndex        = jphysic.at(snake(FLARING_INDEX)).get<real>();
    auto meanMolecularWeight = jphysic.at(snake(MEAN_MOLECULAR_WEIGHT)).get<real>();
    
    p = DiskPhysic::make(std::move(units),
			 flaringIndex,
			 shape,
			 referential,
			 boundaries,
                         init,
                         steps,
			 starAcc,
			 viscosity,
			 adiabatic,
			 density,
			 smoothing,
			 planets,
			 CFLSecurity,
			 kdrag,
			 hillCutFact,
			 transport,
			 meanMolecularWeight);    
  }

  namespace jsonio {
    template<>
    Enum2StrVec<DiskReferential>
    buildEnumMap<DiskReferential>() {
      return {
	{DiskReferential::CONSTANT, "CONSTANT"},
	{DiskReferential::COROTATING, "COROTATING"}};
    }

    template<>
    Enum2StrVec<StarAccretion>
    buildEnumMap<StarAccretion>() {
      return {
	{StarAccretion::CONSTANT, "CONSTANT"},
	{StarAccretion::WIND, "WIND"}};
    }
    
    template<>
    Enum2StrVec<CoolingType>
    buildEnumMap<CoolingType>() {
      return {
	{CoolingType::BETA, "BETA"},
	{CoolingType::RADIATIVE, "RADIATIVE"}
      };
    }

    template<>
    Enum2StrVec<OpacityLaw>
    buildEnumMap<OpacityLaw>() {
      return {
	{OpacityLaw::BELL_LIN, "BELL_LIN"},
	{OpacityLaw::CONSTANT, "CONSTANT"}
      };
    }

    template<>
    Enum2StrVec<Transport>
    buildEnumMap<Transport>() {
      return {
	{Transport::NORMAL, "NORMAL"},
	{Transport::FAST, "FAST"}
      };
    }
  }
  
  void to_json(json& j, DiskReferential const& e)   { jsonio::to_json(j, e); }
  void from_json(json const& j, DiskReferential& e) { jsonio::from_json(j, e); }
  
  void to_json(json& j, StarAccretion const& e)   { jsonio::to_json(j, e); }
  void from_json(json const& j, StarAccretion& e) { jsonio::from_json(j, e); }

  void to_json(json& j, CoolingType const& e)   { jsonio::to_json(j, e); }
  void from_json(json const& j, CoolingType& e) { jsonio::from_json(j, e); }

  void to_json(json& j, OpacityLaw const& e)   { jsonio::to_json(j, e); }
  void from_json(json const& j, OpacityLaw& e) { jsonio::from_json(j, e); }

  void to_json(json& j, Transport const& e)   { jsonio::to_json(j, e); }
  void from_json(json const& j, Transport& e) { jsonio::from_json(j, e); }

}
