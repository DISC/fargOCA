// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>
#include <sysexits.h>

#include <boost/program_options.hpp>
#include <nlohmann/json.hpp>

#include "allmpi.hpp"
#include "simulationConfig.hpp"
#include "simulationTrackingPlugin.hpp"
#include "environment.hpp"

namespace po  = boost::program_options;
namespace fs  = std::filesystem;

bool
read_cmd_line(int argc, char *argv[], po::variables_map& vm) {
  std::ostringstream usage;
  usage << argv[0] << " Export configuration informations. \n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help", "produce this message.")
    ("disk,d", po::value<std::string>(), "If used, the output will be filtered on what is in that file. "
     "If not, everything potentialluy available will be exported.")
    ("output,o", po::value<std::string>(), "The output file, default to stdout.")    
    ("trackers,t", "Export the tracker configuration.")    
    ("boundaries-conditions,b", "Export the boundaries conditions configuration.")    
    ;
  
  po::store(po::command_line_parser(argc, argv).
            options(visible).run(), vm);
  po::notify(vm);
  if (bool(vm.count("help"))) {
    std::cout << visible;
    return false;
  }
  return true;
}

using namespace fargOCA;

std::string quote(std::string l) {
  static std::string q{"\""};
  return q+l+q;
}

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  po::variables_map vm;
  if (!read_cmd_line(argc, argv, vm)) {
    return 1;
  }
  if (vm.count("trackers")) {
    std::ostream& out = std::cout;
    out << "\"enum\": [";
    {
      auto trackers = SimulationTrackingPlugin::available();
      auto iter = trackers.begin();
      while (true) {
        std::string tracker = *iter++;
	out << quoted(tracker);
	if (iter == trackers.end()) {
	  out << "]";
	  break;
	} else {
	  out << ",";
	}
      }
    }
  }

  return EXIT_SUCCESS;
}
