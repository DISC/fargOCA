// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>
#include "fargOCApy.hpp"

namespace py = pybind11;

PYBIND11_MODULE(fargOCApy, m) {
  m.doc() = "The python interface to fargOCA.";
  py::module::import("kokkos");
  fargOCA::py11::exportCommunicator(m);
  fargOCA::py11::exportEnvironment(m);
  fargOCA::py11::exportSimulation(m);
  fargOCA::py11::exportDisk(m);
  fargOCA::py11::exportScalarField(m);
  fargOCA::py11::exportVectorField(m);
  fargOCA::py11::exportPlanetarySystem(m);
}
