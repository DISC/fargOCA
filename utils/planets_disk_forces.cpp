// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <iomanip>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "planetarySystem.hpp"
#include "h5io.hpp"
#include "hillForce.hpp"
#include "simulation.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extracts formated forces applied on the planets by the disk.\n"
        << '\t' << cmd << " <file>.h5 [option]+ \nValid options are.\n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("ofile-fmt,o", po::value<std::string>()->default_value("torque_%1%.txt"), "format of the ouput files (ex: planet%1%.txt). Planet name will be used to distinguish them.")
    ("no-header", "Do not print the header in the output file.")
    ("no-hill-sphere", "Print force with no Hill sphere contributions. (default: comple and no Hill sphere)")
    ("complete", "Print all forces. (default: complete and no Hill sphere)")
    ("sum",   "print the sum of inner and outer disk contributions. (default: print both)")
    ("inner", "only print inner disk contributions. (default: print both)")
    ("outer", "only print outer disk contributions. (default: print both)")
    ("precision,p", po::value<int>()->default_value(10), "The precision of flotaing point ouput.")
    ("ye-olde-format", "Legacy format.")
    ;    

  po::options_description hidden(usage.str());
  hidden.add_options()
    ("file", po::value<std::string>(), "HDF5 file containing the disk file or planetary system log.");
  
  try {
    po::positional_options_description pos;
    pos.add("file", 1);

    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_USAGE);
  }
  for(std::string opt : {"file"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
  if (vm.count("inner") + vm.count("outer") + vm.count("sum") > 1) {
    std::cerr << "Only one of '--inner', '--outer' or '--sum' can be used.\n";
    std::exit(EX_USAGE);
  }
  if (vm.count("no-hill-sphere") + vm.count("complete") > 1) {
    std::cerr << "Only one of '--no-hill-sphere', '--complete' can be used.\n";
    std::exit(EX_USAGE);
  }
}

enum disk_section { INNER, OUTER, SUM };
enum force_filter { COMPLETE, NO_HILL_SPHERE };

bool
do_print(po::variables_map const& vm, disk_section d) {
  switch (d) {
  case INNER: return !(bool(vm.count("sum")) || bool(vm.count("outer")));
  case OUTER: return !(bool(vm.count("sum")) || bool(vm.count("inner")));
  case SUM:   return bool(vm.count("sum"));
  default: std::abort();
  }
}

bool
do_print(po::variables_map const& vm, force_filter f) {
  switch (f) {
  case COMPLETE:       return !bool(vm.count("no-hill-sphere"));
  case NO_HILL_SPHERE: return !bool(vm.count("complete"));
  default: std::abort();
  }
}

bool
do_print(po::variables_map const& vm, disk_section d, force_filter f) {
  return do_print(vm, d) && do_print(vm, f);    
}

std::string 
label(disk_section d) {
  switch (d) {
  case INNER: return "inner";
  case OUTER: return "outer";
  case SUM:   return "     ";
  default: std::abort();
  }
}

std::string 
label(force_filter f) {
  switch (f) {
  case COMPLETE:       return "   complete   ";
  case NO_HILL_SPHERE: return "/w Hill sphere";
  default: std::abort();
  }
}

std::string
label(po::variables_map const& vm, disk_section d, force_filter f) {
  std::ostringstream fmt;
  if (do_print(vm, d, f)) {
    fmt << label(d) << " " << label(f) << " (x,y,z)";
  }
  return fmt.str();
}

template<typename T>
int
width(po::variables_map const& vm) {
  std::ostringstream str;
  str << std::scientific << std::setprecision(vm["precision"].as<int>()) << std::showpos << T(42) << '\t';
  return str.str().size();
}

template<typename T>
std::string
header_label(po::variables_map const& vm, std::string label, char align = '=') {
  std::ostringstream fmt;
  fmt << "%1$" << align << width<T>(vm) << "." << 42 << "S\t";
  return (boost::format(fmt.str())%label).str();
}

std::string
header_label(po::variables_map const& vm, disk_section d, force_filter f) {
  return do_print(vm, d, f) ? header_label<Triplet>(vm, label(vm, d, f)) : std::string();
}

void
print_header(std::ostream& ofile, po::variables_map const& vm) {
  ofile << header_label<real>(vm, "Phys. time", '-')
        << header_label<Triplet>(vm, "Position")
        << header_label<Triplet>(vm, "Velocity")
        << header_label<real>(vm, "Mass")
        << header_label(vm, INNER, COMPLETE)
        << header_label(vm, OUTER, COMPLETE)
        << header_label(vm, INNER, NO_HILL_SPHERE)
        << header_label(vm, OUTER, NO_HILL_SPHERE)
        << header_label(vm, SUM, COMPLETE)
        << header_label(vm, SUM, NO_HILL_SPHERE)
        << std::endl;
}

void
print_force(TimeStampedHillForce const& ts, std::ostream& ofile, po::variables_map const& vm) {
  ofile << ts.time << ",\t"
        << ts.planet.position() << ",\t"
        << ts.planet.velocity() << ",\t"
        << ts.planet.mass() << ",\t";
  if (do_print(vm, INNER, COMPLETE)) {
    ofile << ts.force.inner.complete << ",\t";
  }
  if (do_print(vm, OUTER, COMPLETE)) {
    ofile << ts.force.outer.complete << ",\t";
  }
  if (do_print(vm, INNER, NO_HILL_SPHERE)) {
    ofile << ts.force.inner.noHillSphere << ",\t";
  }
  if (do_print(vm, OUTER, NO_HILL_SPHERE)) {
    ofile << ts.force.outer.noHillSphere << ",\t";
  }
  if (do_print(vm, SUM, COMPLETE)) {
    ofile << (ts.force.inner.complete+ts.force.outer.complete) << ",\t";
  }
  if (do_print(vm, SUM, NO_HILL_SPHERE)) {
    ofile << (ts.force.inner.noHillSphere+ts.force.outer.noHillSphere) << ",\t";
  }
  ofile << std::endl;
}

void
legacy_print_force(TimeStampedHillForce const& ts, std::ostream& ofile) {
  real x = ts.planet.position().x;
  real y = ts.planet.position().y;
  ofile << x*ts.force.inner.complete.y-y*ts.force.inner.complete.x << '\t'
        << x*ts.force.outer.complete.y-y*ts.force.outer.complete.x << '\t'
        << x*ts.force.inner.noHillSphere.y-y*ts.force.inner.noHillSphere.x << '\t'
        << x*ts.force.outer.noHillSphere.y-y*ts.force.outer.noHillSphere.x << '\t'
        << ts.time << '\n';
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);
  
  std::string ifname = vm["file"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadOnly);
  auto logs = TimeStampedHillForce::readH5Logs(ifile.getGroup("/"));
  boost::format ofmt(vm["ofile-fmt"].as<std::string>());
  for(auto const& kv : logs) {
    std::string ofname = (ofmt%kv.first).str();
    log(world) << "writing '" << ofname << "'\n";
    std::ofstream ofile(ofname);
    if (!bool(vm.count("ye-olde-format"))) {
      if (!bool(vm.count("no-header"))) {
        print_header(ofile, vm);
      }
      ofile << std::scientific << std::setprecision(vm["precision"].as<int>()) << std::showpos;
      for(TimeStampedHillForce const& force : kv.second) {
        print_force(force, ofile, vm);
      }
    } else {
      for(TimeStampedHillForce const& force : kv.second) {
        legacy_print_force(force, ofile);
      }
    }
  }
  return 0;
}
