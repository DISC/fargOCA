// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <cmath>

#include "coords.hpp"
#include "rebind.hpp"

using namespace fargOCA;
namespace kk = Kokkos;
using std::size_t;

template<typename F>
bool test(F fct) {
  kk::View<real*>   results("results", 10);
  kk::View<real*>   diffs("diffs", 10);
  kk::View<real*>   xs("xs", 10);
  real const dx {0.5};
  kk::parallel_for("test", results.extent(0),
                   KOKKOS_LAMBDA(size_t i) {
                     kk::Array<real const,3> coords{2, 2+dx, 2+2*dx};
                     kk::Array<real const,3> values{fct(coords[0]), fct(coords[1]), fct(coords[2])};
                     real const begin {coords[0]};
                     real const end   {coords[2]};
                     real const delta {end-begin};
                     real const x = begin+i*delta/10;
                     xs(i)      = x;
                     results(i) = interpolation(coords, values, x);
                     diffs(i)   = results(i) - fct(x);
                   });
  auto hdiffs    {kk::create_mirror_view_and_copy(kk::HostSpace{}, diffs)};
  auto hxs      {kk::create_mirror_view_and_copy(kk::HostSpace{}, xs)};
  auto hresults {kk::create_mirror_view_and_copy(kk::HostSpace{}, results)};
  for(size_t i = 0; i < 10; ++i) {
    std::cout << i << ": x=" << hxs(i) << ", result: " << hresults(i) << ", expected: " << fct(hxs(i)) << ", diff:" << hdiffs(i) << '\n';
  }
  return true;
}

int
main() {
  kk::initialize();
  {
    test(KOKKOS_LAMBDA(real x) { return kk::pow(x, real{2}); });
    test(KOKKOS_LAMBDA(real x) { return kk::sin(x); });
    test(KOKKOS_LAMBDA(real x) { return kk::sqrt(x); });
    test(KOKKOS_LAMBDA(real x) { return kk::pow(x, real{3}); });
  }
  kk::finalize();
}
