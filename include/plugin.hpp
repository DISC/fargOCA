// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_PLUGIN_H
#define FARGO_PLUGIN_H

#include <functional>
#include <map>
#include <set>
#include <optional>
#include <string>

#include "precision.hpp"
#include "arrays.hpp"
#include "config.hpp"
#include "grid.hpp"

namespace fargOCA {
  class Config;
  class PluginConfig;
  
  /// \brief A generic plugin utility class
  /// \param FCT Usually a prototype implementing the plugin function
  /// \param ID a class used to uniquely identify the plugin.
  /// The class must provide  a static std::string id() method.
  /// Otherwise typeid(ID).name() will be used.
  template<class ID, typename... Args>
  class Plugin final {
  public:
    using Evaluator = std::function<void(Args...)>;
    
    Plugin(std::string name, Evaluator fct, std::string desc,
           std::set<std::string> allowed,
           ConfigCheckFct check);
    Plugin(std::string name, Evaluator fct, std::string desc,
           std::set<std::string> allowed);
    ~Plugin();
    
    static std::string           id() { return typeid(ID).name(); }
    static bool                  declared(std::string type);
    static std::set<std::string> available();
    static Plugin const&         get(std::string name);
    static bool                  check(PluginConfig const& config, std::ostream& log);
    static Plugin const&         get(std::string name, Config const& config);
    
    Evaluator const&       evaluator()            const { return myEvaluator; }
    std::set<std::string>  authorizedProperties() const { return myAuthorizedProperties; }
    ConfigCheckFct         configChecker()        const { return myConfigChecker; }
    std::string            name()                 const { return myName; }
    std::string            description()          const { return myDescription; }
    void operator()(Args... args) const { myEvaluator(args...); }
    
    static Plugin obsolete(std::string obsoleteName, Plugin const& fresh);
    
    template<template<GridSpacing GS> class Fct>
    static std::function<void(Args...)> selectSpacing();

  private:
    Plugin(Plugin const&) = default;
    Plugin(Plugin&&) = default;
    
  private:
    std::string                          myName;
    Evaluator                            myEvaluator;
    std::set<std::string>                myAuthorizedProperties;
    ConfigCheckFct                       myConfigChecker;
    std::string                          myDescription;
    
    typedef std::map<std::string, Plugin*>  EngineMap;
    static EngineMap&       engines();
  };
}
#endif 
