// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGO_TRANSPORT_ENGINE_H
#define FARGO_TRANSPORT_ENGINE_H

#include <vector>
#include <optional>
#include <forward_list>

#include "precision.hpp"
#include "arrays.hpp"
#include "grid.hpp"

namespace fargOCA {
  class Disk;
  class GridDispatch;
  class DiskPhysic;
  
  /// \brief Functions that handle the transport substep of a hydrodynamical time step.
  /// The FARGO algorithm is implemented here. The transport is
  /// performed in a manner similar to what is done for the ZEUS code (Stone
  /// & Norman, 1992), except for the momenta transport (we define a left
  /// and right momentum for each zone, which we declare zone centered; we
  /// then transport then normally, and deduce the new velocity in each zone
  /// by a proper averaging).
  /// \param disk used to provide grid dispatch, physic and referential rotation speed.
  /// nothing will be modified through this reference (but the disk fied can be provided as parameter).
  /// \param dt the converging time step
  /// \param density the density field of the fluid to which the transport is applied.
  /// \param velocity the velocity field of the fluid to which the transport is applied.
  /// \param extras a possibly empty set of field to which the transport can be applied (tracers typically)
  template<GridSpacing GS>
  void applyTransport(Disk const& disk, real dt,
                      arr3d<real> density, vec3d<real> velocity, std::vector<arr3d<real>> const& extras);
  
  /// \field Compute, for each cell, the ratio of an input quantity transmited to the next radial cell.
  /// \param density the imput quantity
  /// \param vradial the radial velocity of the fluid
  /// \param transmited the fraction transmited to the next cell
  /// \param dt delta time considered
  /// \return the fraction transmited to the next cell
  arr3d<real> getTransmitedFractionRadial(Disk const& disk,
                                          arr3d<real const> density,
                                          arr3d<real const> vradial,
                                          real dt);
  
  arr1d<real> ComputeFlow(Disk const& disk, real dt);
  arr1d<real> ComputeAngularMomentumFlow(Disk const& disk, real dt);
}
#endif 
