module purge

module load develop
module load spack-user-4.0.0

module load gcc/13.2.0
module load CCE-GPU-4.0.0
module load boost/1.85.0-mpi
module load cray-hdf5/1.12.2.9
module load cmake/3.27.9

export CC=cc
export CXX=CC
export FC=ftn
export MPICH_GPU_SUPPORT_ENABLED=1
export CTI_SLURM_OVERRIDE_MC=1 
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
