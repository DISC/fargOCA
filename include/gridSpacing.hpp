// Copyright 2025 Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_GRID_SPACING_H
#define FARGOCA_GRID_SPACING_H

#include <string>
#include <iostream>
#include <optional>

#include <nlohmann/json_fwd.hpp>

namespace fargOCA {

  /// \brief Represent the radial spacing of the grid
  enum class GridSpacing { ARITHMETIC, LOGARITHMIC };
  /// \bried Typed version  of GridSpacing
  template <GridSpacing GS> struct GridSpacingTp {
    static constexpr GridSpacing spacing = GS;
  };

  template<GridSpacing GS> constexpr GridSpacing spacing(GridSpacingTp<GS> gs) { return GS; }
  
  void to_json(nlohmann::json&, GridSpacing const&);
  void from_json(nlohmann::json const&, GridSpacing&);
  
  std::ostream& operator<<(std::ostream& out, GridSpacing  s);
  std::istream& operator>>(std::istream& in,  GridSpacing& s);

  class Simulation;
  class Disk;
  class GridDispatch;
  class Grid;

  /// \brief Extract spacing type from a sequence of parameters
  template <typename T> void radialSpacing(T const& d) {}
  /// \override
  GridSpacing radialSpacing(Grid const& d);
  /// \override
  GridSpacing radialSpacing(GridDispatch const& d);
  /// \override
  GridSpacing radialSpacing(Disk const& d);
  /// \override
  GridSpacing radialSpacing(Simulation const& s);
  
  /// \override
  template<typename T, typename... Args>
  GridSpacing radialSpacing(T const& g, Args const&... args) {
    if constexpr(std::is_same_v<GridSpacing, decltype(radialSpacing(g))>) {
      return radialSpacing(g);
    } else {
      static_assert(std::is_same_v<GridSpacing,decltype(radialSpacing(args...))>);
      return radialSpacing(args...);
    }
  }
}

#endif 
