// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_SIMULATION_TRACKING_PLUGIN_H
#define FARGO_SIMULATION_TRACKING_PLUGIN_H

#include "precision.hpp"
#include "plugin.hpp"

namespace fargOCA {
  class Simulation;

  enum class SimulationEvent {
    STARTUP, 
    BEFORE_USER_STEP,
    AFTER_USER_STEP,
    BEFORE_STEP,
    AFTER_STEP, 
    GAS_ACCRETION,
    PLANETS_CHANGED
  };
  
  struct SimulationTracking final {};
  using SimulationTrackingPlugin  = Plugin<SimulationTracking, std::string, Simulation const&, SimulationEvent>;
  template<> inline std::string Plugin<SimulationTracking, std::string, Simulation const&, SimulationEvent>::id() {
    return "simulation_tracking"; 
  }  
}

#endif 
