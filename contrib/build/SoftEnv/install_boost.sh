if [[ $# -lt 2 ]]
then
    echo "usage $0 <builddir> <installdir> [<boost version>]"
    exit -1
fi
builddir=$1
installdir=$2
branch=boost-1.84.0
if [[ $# -gt 2 ]]
then
    branch=$3
fi

mkdir -p $builddir $installdir

pushd $builddir

if [[ ! -d boost ]]
then
    git clone --recursive https://github.com/boostorg/boost.git || exit -1
fi

cd boost

cmakeroot=$(pwd)
git checkout master
git pull ||exit -1
git checkout $branch || exit -1
buldtype=Release
mkdir -p $builddir/$buildtype
pushd $builddir/$buildtype

cmake -DCMAKE_BUILD_TYPE=$buildtype -DCMAKE_INSTALL_PREFIX=$installdir -DBUILD_TESTING=Off -DBOOST_EXCLUDE_LIBRARIES=url -DBOOST_ENABLE_MPI=On -DBUILD_SHARED_LIBS=On  $cmakeroot  2>&1 | tee cmake.log
if [[ $? -ne 0 ]]
then
    echo "Something went wrong with cmake. Check cmake.log. Bye."
fi
make -j20 2>&1 | tee make.log
if [[ $? -ne 0 ]]
then
    echo "Something went wrong with make. Check make.log. Bye."
fi
mkdir -p $installdir
make -j install

popd

(
    echo "
if [[ ! \$LD_LIBRARY_PATH =~ $installdir/lib ]]
then 
    export LD_LIBRARY_PATH=$installdir/lib:$installdir/lib64:\$LD_LIBRARY_PATH
    export Boost_ROOT=$installdir
    export CMAKE_PREFIX_PATH=$installdir:$CMAKE_PREFIX_PATH
fi
"
) > $installdir/env.sh
