// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "boost/noopmpi/communicator.hpp"
#include "boost/noopmpi/intercommunicator.hpp"
#include "boost/noopmpi/graph_communicator.hpp"

namespace boost { namespace noopmpi {

void
communicator::abort(int errcode) const 
{
  std::abort();
}

std::optional<intercommunicator>
communicator::as_intercommunicator() const 
{
  return std::nullopt;
}

std::optional<graph_communicator>
communicator::as_graph_communicator() const {
  return std::nullopt;
}

}} 
