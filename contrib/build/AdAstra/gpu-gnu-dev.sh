module purge

module load develop
module load craype-x86-trento
module load craype-accel-amd-gfx90a
module load craype-network-ofi
module load PrgEnv-gnu/8.5.0
module load gcc-native/12.1
module load cray-dsmml/0.2.2
module load cray-mpich/8.1.28                                                          
module load cray-libsci/23.12.5
module load craype/2.7.30
module load amd-mixed/5.7.1
module load perftools-base/23.12.0
module load GCC-GPU-4.0.0
module load libfabric/1.20.1
module load boost/1.85.0-mpi
module load cray-hdf5/1.12.2.9

export CC=cc
export CXX=CC
export FC=ftn
export MPICH_GPU_SUPPORT_ENABLED=1
export CTI_SLURM_OVERRIDE_MC=1 
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
