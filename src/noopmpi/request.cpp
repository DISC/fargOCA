// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "boost/noopmpi/request.hpp"
#include "boost/noopmpi/status.hpp"

namespace boost { namespace noopmpi {

status
request::wait() { return detail::no_return<status>(); }

}}
