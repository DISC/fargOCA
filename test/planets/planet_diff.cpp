// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <limits>

#include "planetarySystem.hpp"
#include "tuple.hpp"

using namespace fargOCA;

typedef TimeStamped<Planet> tp;

bool
check(tp const& t1, tp const& t2) {
  std::cout << "t1: " << t1 << '\n';
  std::cout << "t2: " << t2 << '\n';
  std::cout << "adiff(t1,t2): " << adiff(t1,t2) << '\n';
  std::cout << "rdiff(t1,t2): " << rdiff(t1,t2) << '\n';
  return true;
}

int
main() {
  Triplet t1 = { 1, 2, 3 };
  Triplet t2 = { 41, 42, 43 };
  real nan = std::numeric_limits<real>::quiet_NaN();
  
  tp p1a = { real(1), Planet(1, t1, t2, nan, nan) };
  tp p2a = { real(1), Planet(1, t1*(1+1e-8), t2*(1-1e-8), nan, nan) };
  check(p1a, p2a);
  
  tp p1b = { real(1), Planet(1, t1, t2, 2, 3) };
  tp p2b = { real(1), Planet(1, t1*(1+1e-8), t2*(1-1e-8), 2, 3) };
  check(p1b, p2b);

  return 0;
}
