// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "loops.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace kk = Kokkos;
namespace HF = HighFive;

using std::optional;

template<GridSpacing GS>
ScalarField
compute_vortensity_map_impl(Disk const& disk) 
{
  GridDispatch const& dispatch = disk.dispatch();
  ScalarField vort("gasvortensity", dispatch);
  auto const& coords = dispatch.grid().template as<GS>();
  GridSizes const ls = coords.sizes();
  
  arr3d<real const> vr     = disk.velocity().radial().data();
  arr3d<real const> vt     = disk.velocity().theta().data();
  arr3d<real const> dens   = disk.density().data();
  arr3d<real>       vortmap = vort.data();
  real  const omegaFrame = disk.frameVelocity();

  auto radMed   = coords.radiiMed();
  auto thetaMed = coords.thetaMed();
  
  // The vortensity map
  
  kk::parallel_for("vortencity", fullRange(coords),
                   KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) {
                     if (i == ls.nr-1) {
                       vortmap(i,h,j)  = 0;
                     } else {
                       size_t const jp = nsnext(ls.ns, j);
                       size_t const jm = nsprev(ls.ns, j);
                       real const dvrt = ((vr(i,h,jp)-vr(i,h,j))/(thetaMed(j+1)-thetaMed(j))+
                                          (vr(i+1,h,j+1)-vr(i,h,j))/(thetaMed(j+1)-thetaMed(j))+
                                          (vr(i,h,j)-vr(i,h,jm))/(thetaMed(j)-thetaMed(j-1))+
                                          (vr(i,h,j)-vr(i+1,h,jm))/(thetaMed(j)-thetaMed(j-1)))/4;
                       real const dvtr = ((radMed(i+1)*vt(i,h,j)-radMed(i)*vt(i,h,j))/(radMed(i+1)-radMed(i))+
                                          (radMed(i+1)*vt(i+1,h,jp)-radMed(i)*vt(i,h,jp))/(radMed(i+1)-radMed(i))+
                                          (radMed(i)*vt(i,h,j)-radMed(i-1)*vt(i-1,h,j))/(radMed(i)-radMed(i-1))+
                                          (radMed(i)*vt(i,h,jp)-radMed(i-1)*vt(i-1,h,jp))/(radMed(i)-radMed(i-1)))/4;
                       vortmap(i,h,j)  = (((dvtr-dvrt)/radMed(i) + 2*omegaFrame) / dens(i,h,j));
                     }
                   });
  return vort;
}

GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

ScalarField
compute_vortensity_map(Disk const& disk) {
  switch (disk.dispatch().grid().radialSpacing()) {
  case ARTH: return compute_vortensity_map_impl<ARTH>(disk);
  case LOGR: return compute_vortensity_map_impl<LOGR>(disk);
  }
  std::abort();
}

std::string 
output_fname(po::variables_map& vm) {
  std::string ifile = vm["disk"].as<std::string>();
  std::string basename = ifile;
  {
    auto pos = ifile.find_last_of(".h5");
    if (pos != std::string::npos) {
      basename = ifile.substr(0, pos-2);
    }
  }
  return basename + "_vort.h5";
}

void
write_field(HighFive::Group& group, ScalarField const& field, std::vector<std::string>  path) {
  if (path.size() == 0) {
    field.writeH5(group, 0);
  } else {
    std::string dir = path.front();
    path.erase(path.begin());
    if (group.exist(dir)) {
      if (path.size() ==0) {
        group.unlink(dir); // last component, must remove and create....
        auto child = group.createGroup(dir);
        write_field(child, field, path);
      } else {
        auto child = group.getGroup(dir);
        write_field(child, field, path);
      }
    } else {
      auto child = group.createGroup(dir);
      write_field(child, field, path);
    }
  }
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract vortensity map.\n"
        << '\t' << cmd << " <idisk>.h5 [option]+.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("output,o", po::value<std::string>(),
     "Output file. If not provided, and --inplace is not used, "
     "will write in <disk>_vort.h5 where <disk>.h5 is the input file.")
    ("in-place", po::value<std::string>(), "Add the vortensity field to the disk file at the provided path.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  Environment env(argc, argv);
  fmpi::communicator world;
  std::string ifname = vm["disk"].as<std::string>();
  if (world.size() > 1) {
    std::cerr << "As for now, '" << argv[0] << "' must be launched wit only one proc.\n";
    return EX_USAGE;
  } 
  auto disk = Disk::make(world, HF::File(ifname, HF::File::ReadOnly).getGroup("/"));
  
  ScalarField vort = compute_vortensity_map(*disk);
  if (bool(vm.count("in-place"))) {
    std::string path = vm["in-place"].as<std::string>();
    std::vector<std::string> components;
    boost::split(components, path, boost::is_any_of("/"));
    HF::File ifile(ifname, HF::File::ReadWrite);
    auto root = ifile.getGroup("/");
    write_field(root, vort, components);
  } else {
    std::string ofname = output_fname(vm);
    h5::createH5(vort, world, ofname);
  }
  return 0;
}
