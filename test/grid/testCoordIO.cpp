// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <string>
#include <cstdio>

#include <highfive/H5File.hpp>

#include "environment.hpp"
#include "grid.hpp"

using namespace fargOCA;
namespace HF = HighFive;

bool
testH5(std::string name, std::shared_ptr<Grid const> g, GasShape const& shape) {
  std::string fname = name+".h5";
  std::remove(fname.c_str());
  {
    HF::File f(fname, HF::File::Overwrite);
    g->writeH5(f.getGroup("/"));
  }
  auto p = Grid::make(shape, HF::File(fname, HF::File::ReadOnly).getGroup("/"));
  std::optional<GasShape> compatibility = compatible(*p, *g);
  if (compatibility) {
    std::cout << name <<  " passed\n";
    return true;
  } else {
    std::cout << name << " failed\n";
    return false;
  }  
}

int 
main() {
  Environment env;
  int failed = 0;
  GasShape shape(1, 42, 56, 0.001, false);
  if (!testH5("fat",  Grid::make(shape, {12, 43, 78}, GridSpacing::ARITHMETIC ), shape)) { ++failed; }
  if (!testH5("flat", Grid::make(shape, {12, 1, 78},  GridSpacing::ARITHMETIC ), shape)) { ++failed; }
  if (failed > 0) {
    std::cout << failed << " test faileds\n";
  }
  return failed;
}
