// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <fstream>
#include <iostream>
#include <sstream>
#include <cassert>
#include <limits>
#include <numeric>

#include <boost/algorithm/string.hpp>

#include <highfive/H5File.hpp>

#include "h5io.hpp"
#include "tuple.hpp"
#include "planetarySystem.hpp"
#include "grid.hpp"
#include "scalarField.hpp"
#include "arraysUtils.hpp"
#include "h5Labels.hpp"

namespace fargOCA {
  namespace h5 {
    std::ostream& operator << ( std::ostream& out, FormatVersion const& v) {
      out << v.major << '.' << v.minor;
      return out;
    }

    FormatVersion FormatVersion::current() { return {1,2}; }
    // 1.2: Convert boundaries condition handler format to plugin format
    // 1.1: Added Disk Init plugin in diskPhysic
    // 1.0 initial format

    using namespace labels;
    
    void
    FormatVersion::write(HighFive::Group& root) const {
      auto g = createSubGroup(root, FARGO_FILE_FORMAT);
      writeAttribute(g, MAJOR, major);
      writeAttribute(g, MINOR, minor);
    }

    std::optional<FormatVersion>
    FormatVersion::read(HighFive::Group const& root) {
      if (root.exist(FARGO_FILE_FORMAT)) {
	auto g = root.getGroup(FARGO_FILE_FORMAT);
	FormatVersion v = {*attributeValue<int>(g, MAJOR), *attributeValue<int>(g, MINOR)};
	return  v;
      } else {
	return std::nullopt;
      }
    }
    
    std::optional<HighFive::File>
    createFile(std::string fname, bool cond) {
      if (cond) {
	return HighFive::File(fname, HighFive::File::Overwrite);
      }  else {
	return std::nullopt;
      }
    }

    std::optional<HighFive::Group>
    getRoot(std::optional<HighFive::File>& file) {
      if (file) {
	return file->getGroup("/");
      } else {
	return std::nullopt;
      }
    }
    
    template<>
    std::optional<std::string> attributeValue(HighFive::Group const& obj, std::string const& name) {
      using namespace HighFive;
      if (obj.hasAttribute(name)) {
        Attribute attr = obj.getAttribute(name);
        DataType  tp   = attr.getDataType();
        if (tp.isFixedLenStr()) { 
          size_t sz = attr.getStorageSize();
          std::vector<std::string::value_type> buf(sz+1);
          attr.read(buf.data(), tp);
          std::string res(buf.data(),buf.data()+sz);
          return res;
        } else if (tp.isVariableStr()) { 
          return attr.read<std::string>();
        } else {
          std::abort();
        }
      } else {
        return std::nullopt;
      }
    }

    template <>
    void
    readAttribute<std::string>(HighFive::Group const& obj, std::string const& name, std::string& value) {
      value = *attributeValue<std::string>(obj, name);
    }

    template <>
    void
    readAttribute<bool>(HighFive::Group const& obj, std::string const& name, bool& value) {
      int v = 0;
      readAttribute<int>(obj, name, v);
      value = bool(v);
    }

    template<> void readAttribute<bool>(HighFive::Group const& obj, std::string const& name, std::optional<bool>& opt) {
      if (obj.hasAttribute(name)) {
        bool b;
        readAttribute<bool>(obj, name, b);
        opt = b;
      } else {
        opt = std::nullopt;
      }
    }

    template<> void readAttribute<std::string>(HighFive::Group const& obj, std::string const& name, std::optional<std::string>& opt) {
      if (obj.hasAttribute(name)) {
        opt = attributeValue<std::string>(obj, name);
      } else {
        opt = std::nullopt;
      }
    }

    HighFive::Group
    createSubGroup(HighFive::Group parent, std::string path) {
      if (parent.exist(path)) { parent.unlink(path); }
      return parent.createGroup(path);
    }

    std::optional<HighFive::Group>
    createSubGroup(std::optional<HighFive::Group> parent, std::string path) {
      if (parent) {
        return createSubGroup(*parent, path);
      } else {
        return std::nullopt;
      }
    }

    template<class S>
    void
    writeH5OverGrid(S const& subject, std::string fname, unsigned int mode, int writer) {
      using namespace HighFive;
      std::optional<File> file;
      std::optional<Group> store;
      if (subject.dispatch().comm().rank() == writer) {
	file  = File(fname, mode);
	store = file->getGroup("/");
      }
      subject.writeH5(store, writer);
    }
    
    template<class S>
    void
    readH5OverGrid(S& subject, std::string fname) {
      using namespace HighFive;
      File file(fname, File::ReadOnly);
      Group store = file.getGroup("/");
      subject.readH5(store);
    }
    template void writeH5OverGrid<ScalarField>(ScalarField const& subject, std::string fname, unsigned int mode, int writer);
    template void readH5OverGrid<ScalarField>(ScalarField& subject, std::string fname);

    void
    writePolarGrid(HighFive::Group group, arr3d<real const> grid, GridPositions const& pos) {
      using namespace HighFive;
      GridSizes const sz = { grid.extent(0), grid.extent(1), grid.extent(2) };
      bool overwrite = group.exist(POLAR_GRID);
      DataSet fieldDS = (overwrite
			 ? group.getDataSet(POLAR_GRID)
			 : group.createDataSet(POLAR_GRID, DataSpace(sz.nr, sz.ni, sz.ns), create_datatype<real>()));
      harr3d<real const> hmerged = dev2CHost(grid);
      fieldDS.write_raw(hmerged.data());
      if (!overwrite) {
	fieldDS.createAttribute(RADIUS_POSITION, pos.radius);
	if (sz.ni > 1) {
	  fieldDS.createAttribute(LAYER_POSITION, pos.layer);
	}
	fieldDS.createAttribute(SECTOR_POSITION, pos.sector);
      }
    }

    template<>
    void
    readAttribute<DiskReferential>(HighFive::Group const& obj, std::string const& name, std::optional<DiskReferential>& opt) {
      if (obj.hasAttribute(name)) {
	try {
	  opt = attributeValue<DiskReferential>(obj, name);
	} catch(HighFive::AttributeException const& e) {
	  std::cerr << "Got an error trying to read DiskReferential attribute. "
		    << "Try running disk_upgrade on disk file and restart.\n"
		    << "Error was: " << e.what() << '\n';
	  throw e;
	}
      } else {
        opt = std::nullopt;
      }
    }

    HighFive::Group
    getRoot(std::string fname, unsigned int mode) {
      return HighFive::File(fname, mode).getGroup("/");
    }
  }
}
