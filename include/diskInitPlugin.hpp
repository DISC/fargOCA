// Copyright 2024, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_DISK_INIT_PLUGIN_H
#define FARGO_DISK_INIT_PLUGIN_H

#include <functional>
#include <map>
#include <set>
#include <string>

#include "plugin.hpp"

namespace fargOCA {
  class Disk;

  /// \brief Indicate when to intevene
  /// Disk fields are initialized in the following order:
  ///  1) density, 2) velocity, 3) energy
  /// the callback will be called after each step, with the matching enum value
  enum class DiskInitEvent { DENSITY, VELOCITY, ENERGY };

  using DiskInitPlugin   = Plugin<DiskInitEvent, std::string, Disk&, DiskInitEvent>;
  
  template<> inline std::string Plugin<DiskInitEvent, std::string, Disk&, DiskInitEvent>::id() { return "disk_init"; }  
}
#endif 
