// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include <highfive/H5File.hpp>

#include <Kokkos_MathematicalFunctions.hpp>

#include "boost/lexical_cast.hpp"

#include "grid.hpp"
#include "precision.hpp"
#include"environment.hpp"

using namespace fargOCA;
namespace HF = HighFive;

arr1d<real const>
getCoords(GridCoords const& grid, std::string dir) {
  if (dir == "radii") {
    return grid.radii();
  } else if (dir == "sectors") {
    return grid.theta();
  } else if (dir == "layers") {
    return grid.phi();
  } else {
    std::abort();
  }
}

namespace kkm = Kokkos::Experimental;

int
main(int argc, char* argv[]) {
  std::string direction(argv[1]);
  std::string g1fname(argv[2]);
  std::string path1(argv[3]);
  int         step1 = boost::lexical_cast<int>(argv[4]);
  std::string g2fname(argv[5]);
  std::string path2(argv[6]);
  int         step2 = boost::lexical_cast<int>(argv[7]);
  
  Environment env(argc, argv);

  GasShape   shape2;
  shape2.readH5(HF::File(g2fname, HF::File::ReadOnly).getGroup("/physic/gas_shape"));
  GridCoords grid1(shape2, HF::File(g1fname, HF::File::ReadOnly).getGroup(path1));
  GridCoords grid2(shape2, HF::File(g2fname, HF::File::ReadOnly).getGroup(path2));
  arr1d<real const> coords1 = getCoords(grid1, direction);
  arr1d<real const> coords2 = getCoords(grid2, direction);
  int nb1 = (coords1.size()-1) / step1;
  int nb2 = (coords2.size()-1) / step2;
  if (nb1 != nb2) {
    std::cerr << "Incompatible grids check: " << nb1 << " elements in " << g1fname 
              << " vs " << nb2 << " elements in " << g2fname << '\n';
    return -1;
  }
  real maxdelta = 0;

  Kokkos::parallel_reduce("diff_coords", Kokkos::RangePolicy<>(0, nb1),
                          KOKKOS_LAMBDA(int i, real& tmp) {
                            tmp = kkm::fmax(tmp, std::abs(coords1(i*step1) - coords2(i*step2)));
                          }, Kokkos::Max<real>(maxdelta));
  std::cout << "max delta " << maxdelta;
  if (maxdelta > 1e-8) {
    std::cout << " (> 1e-8)\n"
              << "FAILED\n";
    return -1;
  } else {
    std::cout << "\n"
              << "PASSED\n";
    return 0;
  }
}

