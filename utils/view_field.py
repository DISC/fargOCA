#!/usr/bin/env python3

import sys
import h5py as h5
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='''
Display the differences between a given field of two gas disk description files.
Example:
   {} out/disk2.h5 velocity/theta
'''.format(sys.argv[0]))
parser.add_argument('file', metavar='H5FILE', type=str, nargs=1,
                    help='HDF5 field file.')
parser.add_argument('path', metavar='PATH', type=str, nargs=1,
                    help='Location of the field into the HDF5 file.')
parser.add_argument('-a','--azimuth', dest='sector',type=int,
                    help='choose the sector to plot (from 0, to Sectors-1 ) , default is azimuthal average')

args = parser.parse_args()

path = args.path[0]
disk = h5.File(args.file[0], 'r')

field = np.array(disk[path]['polar_grid'])

flat   = field.shape[1] == 1

import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
        
xymesh = None
(nr,ni,ns) = field.shape
(radii,layers,sectors) = (range(nr), range(ni), range(ns))
if 'grid' in disk:
    (radii,layers,sectors) = (np.array(disk['grid']['radii']),
                              np.array(disk['grid']['layers']),
                              np.array(disk['grid']['sectors']))
if flat:    
    print("this is a flat grid, displaying as it is.")
    xymesh = np.meshgrid(sectors, radii)
    field  = np.mean(field, 1)
else:
    xymesh  = np.meshgrid(layers, radii)
    if args.sector:
        print("this is a fat grid, displaying sector {}".format(args.sector)) 
        field  = field[:,:,args.sector]
    else:
        print("this is a fat grid, merging sectors before displaying.")
        field  = np.mean(field, 2)

def plot(f, xymesh, scl_min, scl_max, pos, name):
    plt.subplot(2,2,pos)
    plt.title(name)
    x,y = xymesh
    plt.pcolormesh(y,x,f, cmap='RdBu', vmin=scl_min, vmax=scl_max)
    plt.axis([y.min(), y.max(), x.min(), x.max()])
    plt.colorbar()

field_max = np.abs(field).max()
plot(field, xymesh, -field_max, field_max, 1, "{}-1".format(path))
plt.show()
