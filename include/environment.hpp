// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_ENVIRONMENT_H
#define FARGOCA_ENVIRONMENT_H

#include <vector>
#include <string>
#include <iostream>

#include <boost/program_options.hpp>

#include "allmpi.hpp"

namespace fargOCA {
  class Environment {
  public:
    Environment();
    Environment(int argc, char** argv);
    Environment(std::vector<std::string> args);
    ~Environment();
    
    static fmpi::communicator const& world();
    static void printConfig(std::ostream& log);
    static bool hasGpuDirect();
    static boost::program_options::options_description const& cmdLineOptions();
  
  private:
    void parseArgs(std::vector<std::string>);
    
  private:
    std::unique_ptr<fmpi::environment>  myMpiEnv;
    std::unique_ptr<fmpi::communicator> myWorld;
    static Environment* ourInstance;
  };
  
}

#endif
