// Copyright (C) 2007 Trustees of Indiana University

// Authors: Douglas Gregor
//          Andrew Lumsdaine

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/** @file graph_communicator.hpp
 *
 *  This header defines facilities to support MPI communicators with
 *  graph topologies, using the graph interface defined by the Boost
 *  Graph Library. One can construct a communicator whose topology is
 *  described by any graph meeting the requirements of the Boost Graph
 *  Library's graph concepts. Likewise, any communicator that has a
 *  graph topology can be viewed as a graph by the Boost Graph
 *  Library, permitting one to use the BGL's graph algorithms on the
 *  process topology.
 */
#ifndef BOOST_NOOPMPI_GRAPH_COMMUNICATOR_HPP
#define BOOST_NOOPMPI_GRAPH_COMMUNICATOR_HPP

#include <boost/noopmpi/communicator.hpp>
#include <vector>
#include <utility>

// Headers required to implement graph topologies
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/assert.hpp>

namespace boost { namespace noopmpi {

/**
 * @brief An MPI communicator with a graph topology.
 *
 * This graph can only have one vertex and no edges.
 */
class BOOST_MPI_DECL graph_communicator : public communicator
{
  friend class communicator;

 public:
  /** @brief Catch all ctor. */
  template<typename C, typename... TS>
  graph_communicator(const C& comm, TS... unused) : communicator(&c_mpi::graph_comm) {}

 public:
  /// Build a single 1 node graph to export the BGL
  typedef int                                         vertex_t;
  typedef std::pair<vertex_t, vertex_t>               edge_t;
  typedef std::pair<vertex_t const*, vertex_t const*> vertex_range_t;
  typedef std::pair<edge_t const*, edge_t const*>     edge_range_t;
  
  static constexpr vertex_t master  = 0;
  static constexpr edge_t   edges[] = {};
  static constexpr vertex_range_t vertices_range = vertex_range_t(&master, &master+1);
  static constexpr edge_range_t   edges_range    = edge_range_t(edges, edges);

 private:
  friend class communicator;
};

// Incidence Graph requirements

/**
 * @brief Returns the source vertex from an non existing edge.
 */
inline int source(const std::pair<int, int>& /* edge */, const graph_communicator&)
{
  return detail::no_return<int>();
}

/**
 * @brief Returns the target vertex from a non existing edge.
 */
inline int target(const std::pair<int, int>& /* edge */, const graph_communicator&)
{
  return detail::no_return<int>();
}

/** @brief There are no edges. */
inline graph_communicator::edge_range_t
out_edges(int /* vertex */, const graph_communicator& /* comm */) { return graph_communicator::edges_range; }

/** @brief No outgoing vertices. */
inline int out_degree(int /* vertex */, const graph_communicator&  /* comm */) { return 0; }

// Adjacency Graph requirements

/** @brief There are no neighbors. */
inline graph_communicator::vertex_range_t
adjacent_vertices(int /* vertex */, const graph_communicator& /* comm */) {
  return graph_communicator::vertices_range;
}

// Vertex List Graph requirements

/** @brief Returns the single vertice iterator. */
inline graph_communicator::vertex_range_t
vertices(graph_communicator const& /* comm */) {
  return graph_communicator::vertices_range;
}

/** @brief There is only one vertex. */
inline int num_vertices(const graph_communicator& /* comm */) { return 1; }

// Edge List Graph requirements

/** @brief there are no edges. */
inline graph_communicator::edge_range_t
edges(const graph_communicator& /* comm */) {
  return graph_communicator::edges_range;
}

/** @brief There are no edges. */
inline int num_edges(const graph_communicator& /* comm */) { return 0; }

// Property Graph requirements

/**
 *  @brief Returns a property map that maps from vertices in a
 *  communicator's graph topology to their index values. 
 *
 *  Since the vertices are ranks in the communicator, the returned
 *  property map is the identity property map.
 */
inline identity_property_map get(vertex_index_t, const graph_communicator&)
{
  return identity_property_map();
}

/** @brief Returns the index of he master, which is 0. */
inline int get(vertex_index_t, const graph_communicator&, int /* vertex */) { return 0; }

} } // end namespace boost::mpi

namespace boost {

/**
 * @brief Traits structure that allows a communicator with graph
 * topology to be view as a graph by the Boost Graph Library.
 *
 * The specialization of @c graph_traits for an MPI communicator
 * allows a communicator with graph topology to be viewed as a
 * graph. An MPI communicator with graph topology meets the
 * requirements of the Graph, Incidence Graph, Adjacency Graph, Vertex
 * List Graph, and Edge List Graph concepts from the Boost Graph
 * Library.
 */
template<>
struct graph_traits<noopmpi::graph_communicator> {
  // Graph concept requirements
  typedef noopmpi::graph_communicator::vertex_t vertex_descriptor;
  typedef noopmpi::graph_communicator::edge_t   edge_descriptor;
  typedef directed_tag               directed_category;
  typedef disallow_parallel_edge_tag edge_parallel_category;
  
  /**
   * INTERNAL ONLY
   */
  struct traversal_category
    : incidence_graph_tag, 
      adjacency_graph_tag, 
      vertex_list_graph_tag, 
      edge_list_graph_tag 
  { 
  };

  /**
   * @brief Returns a vertex descriptor that can never refer to any
   * valid vertex.
   */
  static vertex_descriptor null_vertex() { return -1; }

  // Incidence Graph requirements
  typedef noopmpi::graph_communicator::edge_t* out_edge_iterator;
  typedef int                                  degree_size_type;

  // Adjacency Graph requirements
  typedef noopmpi::graph_communicator::vertex_t* adjacency_iterator;

  // Vertex List Graph requirements
  typedef noopmpi::graph_communicator::vertex_t* vertex_iterator;
  typedef int                                    vertices_size_type;

  // Edge List Graph requirements
  typedef noopmpi::graph_communicator::edge_t* edge_iterator;
  typedef int                                  edges_size_type;
};

// Property Graph requirements

/**
 * INTERNAL ONLY
 */
template<>
struct property_map<noopmpi::graph_communicator, vertex_index_t>
{
  typedef identity_property_map type;
  typedef identity_property_map const_type;
};

} // end namespace boost

#endif // BOOST_NOOPMPI_GRAPH_COMMUNICATOR_HPP
