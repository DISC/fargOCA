// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>
#include <string>

#include "environment.hpp"
#include "gasShape.hpp"
#include "diskProfiles.hpp"
#include "gridDispatch.hpp"
#include "arrayTestUtils.hpp"
#include "arrays.hpp"
#include "arraysUtils.hpp"

using namespace fargOCA;
namespace kk = Kokkos;

// This test simulate the dispatch of a Kokkos View other
// an MPI communicator.
// The trick is to automagically deal with the layout and
// memory space that can be different between the device
// and the host.
// Standard (through CPU) communication requires right
// layout en host memory.

/// \brief The expected value on a specific slot of a 3D view.
KOKKOS_INLINE_FUNCTION
int
value(int i, int h, int j) {
  return i*100+h*10+j;
}

// The global grid dimension.
constexpr int N0 = 10;
constexpr int N1 = 3;
constexpr int N2 = 2;
const std::string PASSED("PASSED");
const std::string FAILED("FAILED");

/// A 3D range on the execution space of the view
template<class View>
auto range3D(View v) {
  int n[] = { v.extent_int(0), v.extent_int(1), v.extent_int(2) };
  assert(n[0] <= N0);
  assert(n[1] <= N1);
  assert(n[2] <= N2);
  return Kokkos::MDRangePolicy<typename View::execution_space, Kokkos::Rank<3>>({0,0,0}, n);
}

/// Is it an host view.
template<class View>
bool hostView(View v) {
  return std::is_same<typename View::memory_space, Kokkos::HostSpace>::value;
}

/// Does is have right layout.
template<class View>
bool rightLayout(View v) {
  return std::is_same<typename View::array_layout, Kokkos::LayoutRight>::value;
}

/// Is it compatible with a plain host C array.
template<class View>
bool isRawCStuff(View v) {
  return (v.span_is_contiguous()
          && (std::is_same<typename View::memory_space, Kokkos::HostSpace>::value)
          && (std::is_same<typename View::array_layout, Kokkos::LayoutRight>::value));
}

/// A description of the view, name layout etc..
template<class View>
std::string desc(View v) {
  std::ostringstream fmt;
  fmt << std::boolalpha
      << v.label()
      << "[host:" << hostView(v)
      << ", right:" << rightLayout(v)
      << ", packed:" << v.span_is_contiguous()
      << ", raw:" << isRawCStuff(v)
      << "]{"
      << v.extent(0) << "," << v.extent(1) << "," << v.extent(2) << "}";
  return fmt.str();
}

/// \brief Check that a C like 3D view has the right values, as defined by the value function.
/// \param v the v
/// \param start0 if the view is the result of a split along the
/// first dimension, the first idex (default to 0).
/// \param rk the static dimension of the view.
template<class View>
bool testHostCTypeViewValues(View v, int start0, Kokkos::Rank<3> rk) {
  assert(isRawCStuff(v));

  std::cout << " testing on raw data." << std::flush;
  bool passed = true;
  int n[] = { v.extent_int(0), v.extent_int(1), v.extent_int(2) };
  for(int i = start0; i < n[0]; ++i) {
    for(int h = 0; h < n[1]; ++h) {
      for(int j = 0; j < n[2]; ++j) {
        typename View::const_value_type* ptr = v.data();
        int is = i - start0;
        auto v = ptr[j+h*n[2]+is*n[1]*n[2]];
        if (v != value(i,h,j)) {
          std::cout << "error at (" << i << "," << h << "," << j
                    << ") got " << v
                    << " expected " << value(i,h,j) << "\n";
          passed = false;
        }
      }
    }
  }
  std::cout << (passed ? PASSED : FAILED) << '\n';
  return passed;
}

/// \brief Check that contiguous 3D view has the right values, as defined by the value function.
/// \param v the v
/// \param start0 if the view is the result of a split along the
/// first dimension, the first idex (default to 0).
/// \param rk the static dimension of the view.
template<class View>
bool testPackedViewValues(View v, int start0, Kokkos::Rank<3> rk) {
  assert(v.span_is_contiguous());
  if constexpr (!std::is_same_v<typename View::memory_space, Kokkos::HostSpace>) {
    auto hv = Kokkos::create_mirror_view(v);
    Kokkos::deep_copy(hv, v);
    return testPackedViewValues(hv, start0, rk);
  } else {
    if constexpr (std::is_same_v<typename View::array_layout, Kokkos::LayoutRight>) {
      return testHostCTypeViewValues(v, start0, rk);
    } else {
      bool passed = true;
      int n[] = { v.extent_int(0), v.extent_int(1), v.extent_int(2) };
      for(int i = start0; i < n[0]; ++i) {
        for(int h = 0; h < n[1]; ++h) {
          for(int j = 0; j < n[2]; ++j) {
            if (v(i-start0,h,j) != value(i,h,j)) {
              std::cout << "error at (" << i << "," << h << "," << j
                        << ") got " << v(i,h,j)
                        << " expected " << value(i,h,j) << "\n";
              passed = false;
            }
          }
        }
      }
      std::cout << (passed ? PASSED : FAILED) << '\n';
      return passed;
    }
  }
}

/// \brief Check that an arbitrary 3D view has the right values, as defined by the value function.
/// \param v the v
/// \param start0 if the view is the result of a split along the
/// first dimension, the first idex (default to 0).
/// \param rk the static dimension of the view.
template<class... Properties>
bool testViewValues(Kokkos::View<Properties...> v, int start0, Kokkos::Rank<3> rk) {
  std::cout << "Testing " << desc(v) << " values. " << std::flush;\
  if (!v.span_is_contiguous()) {
    return testPackedViewValues(packed(v), start0, rk);
  } else {
    return testPackedViewValues(v, start0, rk);
  }
}

/// \brief Check that an arbitrary view (only 3D is supported for now)
/// has the right values, as defined by the value function.
/// \param v the v
/// \param start0 if the view is the result of a split along the
/// first dimension, the first idex (default to 0).
template<typename Ts, class... Properties>
bool testViewValues(Kokkos::View<Ts,Properties...> v, int start0 = 0) {
  return testViewValues(v, start0, Kokkos::Rank<v.rank>());
}

/// \brief Initialize a view with value(i,h,j)
template<class View>
void initView(View v) {
  Kokkos::parallel_for(range3D(v), KOKKOS_LAMBDA(int i, int h, int j) { v(i,h,j) = value(i,h,j); });
}

int
main(int argc, char* argv[]) {
  fargOCA::Environment  env(argc, argv);
  int failed = 0;
  // Test a regular device view
  Kokkos::View<int***> dev1("dev1", N0,N1,N2);
  initView(dev1);
  std::cout << "testing " << dev1.label() << '\n';
  if (!testViewValues(dev1)) { ++failed; };

  // test a device view with righ layout
  Kokkos::View<int***, Kokkos::LayoutRight> dev2("dev2", N0,N1,N2);
  initView(dev2);
  std::cout << "testing " << dev2.label() << '\n';
  if (!testViewValues(dev2)) { ++failed; };

  // test a host view with device layout
  auto dev3 = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace(), dev1);
  initView(dev3);
  std::cout << "testing " << dev3.label() << '\n';
  if (!testViewValues(dev3)) { ++failed; };

  // test the first half (in the first dimension) of a regulare device view
  std::cout << "testing subview(" << dev1.label() << ") first half\n";
  if (!testViewValues(Kokkos::subview(dev1, range(0,N0/2), Kokkos::ALL(), Kokkos::ALL()))) { ++failed; };
  std::cout << "testing subview(" << dev1.label() << ") second half\n";
  // test the second half (in the first dimension) of a regulare device view
  if (!testViewValues(Kokkos::subview(dev1, range(N0/2,N0), Kokkos::ALL(), Kokkos::ALL()), N0/2)) { ++failed; };

  // test the right layout copy of a regular device view
  std::cout << "testing rightview(" << dev1.label() << ")\n";
  if (!testViewValues(rightView(dev1))) { ++failed; };

  // test the right layout copy of a host view
  std::cout << "testing rightview(" << dev3.label() << ")\n";
  if (!testViewValues(rightView(dev3))) { ++failed; };

  std::cout << "Testing MPI input\n";
  // test the right layout copy of a host view
  std::cout << desc(rightView(dev3)) << '\n';
  if (!testViewValues(dev2CHost(rightView(dev3)))) { ++failed; };

  // test the MPI compatible input for a host view
  std::cout << desc(dev3) << '\n';
  if (!testViewValues(dev2CHost(dev3))) { ++failed; };
  // test the MPI compatible input for a device view
  std::cout << desc(dev1) << '\n';
  if (!testViewValues(dev2CHost(dev1))) { ++failed; };

  {
    auto dev1_1 = Kokkos::subview(dev1, range(0,N0/2), Kokkos::ALL(), Kokkos::ALL());
    std::cout << desc(dev1_1) << " mpi input buffer -> " << std::flush;
    auto dev1MpiIn_1 = dev2CHost(dev1_1);
    std::cout << desc(dev1MpiIn_1) << '\n';
    if (!isRawCStuff(dev1MpiIn_1)) {  ++failed; };
    if (!testViewValues(dev1MpiIn_1)) { ++failed; };
    
    auto dev1_2 = Kokkos::subview(dev1, range(N0/2,N0), Kokkos::ALL(), Kokkos::ALL());
    std::cout << desc(dev1_2) << " mpi input buffer -> " << std::flush;
    auto dev1MpiIn_2 = dev2CHost(dev1_2);
    std::cout << desc(dev1MpiIn_2) << '\n';
    if (!isRawCStuff(dev1MpiIn_2)) {  ++failed; };    
    if (!testViewValues(dev1MpiIn_2, N0/2)) { ++failed; };

    auto mergedBuffer = cHost2DevBuffer(dev1MpiIn_1, dev1);
    std::copy(dev1MpiIn_1.data(), dev1MpiIn_1.data() + dev1MpiIn_1.size(), mergedBuffer.data());
    std::copy(dev1MpiIn_2.data(), dev1MpiIn_2.data() + dev1MpiIn_2.size(), mergedBuffer.data()+dev1MpiIn_1.size());

    if (!testViewValues(mergedBuffer)) { ++failed; };
    decltype(dev1) dev1Check(dev1.label()+"merge_check",  dev1.extent(0),dev1.extent(1),dev1.extent(2));
    cHost2Dev(mergedBuffer, dev1Check);
    if (!testViewValues(dev1Check)) { ++failed; };    
  }
  return failed;
}
