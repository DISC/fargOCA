import numpy as np
import matplotlib.pyplot as plt 
import math as mt

m=np.loadtxt("orbitBigEart.dat") 

a=m[:,1];e=m[:,2];t=m[:,0]

R0=5.2
codet2y=R0**1.5/2./mt.pi

plt.xlabel('Time (y)')
plt.ylabel('a')
plt.plot(t*codet2y,a,'ro-',label='a',linewidth=0.5)
plt.plot(t*codet2y,a*(1-e),'r-.',label='a(1-e)',linewidth=0.5)
plt.plot(t*codet2y,a*(1+e),'r--',label='a(1+e)',linewidth=0.5)
plt.legend(loc=1)
plt.grid(True,linestyle='--')
filename='orbit'+'.png'
plt.savefig(filename,dpi=100,bbox_inches='tight')

plt.show()

