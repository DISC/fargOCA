// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <limits>
#include <algorithm>
#include <numeric>
#include <unistd.h>

#include <boost/lexical_cast.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "io.hpp"
#include "log.hpp"
#include "scalarField.hpp"
#include "cartesianGridCoords.hpp"
#include "gridCellSizes.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;
namespace kk = Kokkos;

/// \brief write cell data in h5 format, accounting for ghosts
bool
writeH5(GridDispatch const& grid, std::string fname, arr3d<real const> data) {
  ScalarField f("tmp", grid);
  kk::deep_copy(f.data(), data);
  h5::createH5(f, grid.comm(), fname);
  return true;
}

bool
test(fmpi::communicator const& world, size_t const radius, size_t const sector, size_t const layer, bool dump, std::string odir) {
  std::ostringstream fmt;
  fmt << "grid_" << radius << '_' << sector << '_' << layer;
  std::string fbase = fmt.str();
  std::string cellfname = fbase+"_cell.h5";
  std::string dthetafname = fbase+"_dtheta.h5";
  std::string dthetamedfname = fbase+"_dthetamed.h5";

  GasShape shape(0.05, 42, 56, 0.001, false);
  auto grid = GridDispatch::make(world, *Grid::make(shape, {radius, layer, sector}, GridSpacing::ARITHMETIC ));

  if (dump) {
    std::cout << "Writing\n";
    ::unlink(cellfname.c_str());
    ::unlink(dthetafname.c_str());
    ::unlink(dthetamedfname.c_str());
    
    bool celldump = writeH5(*grid, cellfname, grid->cellSizes().volume());
    bool dthetadump = writeH5(*grid, dthetafname, grid->cellSizes().dTheta());

    return celldump && dthetadump;
  } else {
    std::cout << "Testing\n";
    
    ScalarField cell("cell", *grid);
    cell.readH5(odir+"/"+cellfname);
    ScalarField dtheta("dtheta", *grid);
    dtheta.readH5(odir+"/"+dthetafname);
    ScalarField dthetamed("dthetamed", *grid);
    dthetamed.readH5(odir+"/"+dthetamedfname);
    bool const cellok   = checkAbsRelDiff(maxAbsRelDiff(cell.data(),   grid->cellSizes().volume()), 5e-8, 5e-10, std::cout); 
    bool const dthetaok = checkAbsRelDiff(maxAbsRelDiff(dtheta.data(), grid->cellSizes().dTheta()), 5e-11, 5e-8, std::cout); 
    
    std::cout << "cell avg: " << average(cell.data())   << '\n';
    std::cout << "dtheta avg: " << average(dtheta.data()) << '\n';
    
    
    bool const passed = cellok && dthetaok;
    
    if (passed) {
      std::cout << "PASSED\n";
    } else {
      std::cout << "FAILED\n";
    }
    return passed;
  }
}

int main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  bool check = argc == 2;
  std::string odir = ".";
  if (check) {
    odir = argv[1];
  }

  bool passed  = test(world,51,4,3,!check, odir);
  return passed ? 0 : 1;
}
