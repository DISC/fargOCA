// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_ARRAYS_UTILS_H
#define FARGOCA_ARRAYS_UTILS_H

#include <optional>

#include "arrays.hpp"

namespace fargOCA {

  /// \brief Packed a view if necessary.
  /// If the view is contiguous, return a const version of it. Othwerwise copy in a contiguous view with same layout.
  template<class ArrTp, class... Properties>
  auto
  packed(Kokkos::View<ArrTp, Properties...> v) {
    using ViewType = Kokkos::View<ArrTp, Properties...>;
    using ResultType = typename ViewType::const_type;
    if (v.span_is_contiguous()) {
      return ResultType{v};
    } else {
      using Layout   = typename ViewType::array_layout;
      Layout layout {v.extent(0), v.extent(1), v.extent(2), v.extent(3), v.extent(4), v.extent(5)};
      using NonConstView = typename ViewType::non_const_type;
      NonConstView cpy{std::string("packed_")+v.label(), layout};
      Kokkos::deep_copy(cpy, v);
      return ResultType{cpy};
    }
  }
  namespace details {
    /// \brief An identical view, except with right layout
    template<class View>
    using RightView = Kokkos::View<typename View::non_const_data_type,
                                   Kokkos::LayoutRight,
                                   typename View::memory_space,
                                   typename View::memory_traits>;
    
    /// \brief Failing specialization.
    /// If we hit this one, it means we've passed dimension 3
    /// We need to specialize for each dimension as Kokkos does
    /// not yet provide a way to copy the extends of an arbitrarly
    /// ranked view.
    template<class View, int N>
    void
    rightView(View v, std::integral_constant<int, N>) {
      static_assert(N<=3);
    }
    
    /// 3D specialization of a rightLayout view copy
    template<class View>
    auto
    rightView(View v, std::integral_constant<int, 3>) {
      RightView<View> rv(v.label()+"_right",
                         v.extent_int(0),
                         v.extent_int(1),
                         v.extent_int(2));
      Kokkos::deep_copy(rv, v);
      return rv;
    }
    
    template<class View>
    auto
    rightView(View v, std::integral_constant<int, 2>) {
      RightView<View> rv(v.label()+"_right",
                         v.extent_int(0),
                         v.extent_int(1));
      Kokkos::deep_copy(rv, v);
      return rv;
    }
    
    template<class View>
    auto
    rightView(View v, std::integral_constant<int, 1>) {
      RightView<View> rv(v.label()+"_right",
                         v.extent_int(0));
      Kokkos::deep_copy(rv, v);
      return rv;
    }
  }
  
  /// \brief Produce the copy of a view, except with right layout
  template<typename AT, class... P >
  auto
  rightView(Kokkos::View<AT,P...> v) {
    return details::rightView(v, std::integral_constant<int, int(Kokkos::View<AT,P...>::rank)>{});
  }
  
  template<class View>
  auto 
  optRightView(std::optional<View> ov)
  {
    std::optional<decltype(rightView(*ov))> res;
    if (ov) {
      res = std::make_optional(rightView(*ov));
    } 
    return res;
  }
  
  namespace details {
    // We are already in the right space with right layout.
    // We just need to make sure memory is packed
    template<class View>
    auto
    dev2CHost(View v, Kokkos::LayoutRight, Kokkos::HostSpace) {
      return packed(v);
    }
    
    template< class View, class Space>
    auto
    dev2CHost(View v, Kokkos::LayoutRight, Space) {
      return dev2CHost(Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace{}, v), Kokkos::LayoutRight{}, Kokkos::HostSpace{});
    }
    
    template< class View, class Layout, class Space>
    auto
    dev2CHost(View v, Layout, Space) {
      return dev2CHost(fargOCA::rightView(v), Kokkos::LayoutRight{}, Space{});
    }
  }
  
  /// \brief Given a view, convert it to a MPI compatible buffer.
  /// That is, a C like array
  template<class View>
  auto
  dev2CHost(View v) {
    return details::dev2CHost(v, typename View::array_layout{}, typename View::memory_space{});
  }

  /// \brief Given an optional view, convert it to a MPI compatible buffer.
  /// That is, a C like array
  template<class View>
  auto
  optDev2CHost(std::optional<View> v) {
    std::optional<decltype(dev2CHost(std::declval<View>()))> result;
    if (v) {
      result = dev2CHost(*v);
    }
    return result;
  }
  
  namespace details {
    template<class IView, class OView>
    auto
    cHost2DevBuffer(OView ov, std::integral_constant<int, 3>) {
      typename IView::non_const_type buff(ov.label()+"_mpi_buffer",
                                          ov.extent(0),
                                          ov.extent(1),
                                          ov.extent(2));
      return buff;
    }

    template<class IView, class OView>
    auto
    cHost2DevBuffer(OView ov, std::integral_constant<int, 2>) {
      typename IView::non_const_type buff(ov.label()+"_mpi_buffer",
                                          ov.extent(0),
                                          ov.extent(1));
      return buff;
    }

    template<class IView, class OView>
    auto
    cHost2DevBuffer(OView ov, std::integral_constant<int, 1>) {
      typename IView::non_const_type buff(ov.label()+"_mpi_buffer",
                                          ov.extent(0));
      return buff;
    }

  }
  /// \brief Produce a copy of and MPI output into the host space
  template<class IView, class OView>
  typename IView::non_const_type
  cHost2DevBuffer(IView, OView ov) {
    return details::cHost2DevBuffer<IView,OView>(ov, std::integral_constant<int, int(IView::rank)>{});
  }
  
  template<class IView, class OView>
  typename IView::non_const_type
  cHost2DevBuffer(std::optional<IView>, OView ov) {
    return details::cHost2DevBuffer<IView,OView>(ov, std::integral_constant<int, int(IView::rank)>{});
  }

  namespace details {
    template<class IView, class OView>
    auto
    outSpaceCopy(IView iv, OView ov, std::integral_constant<int, 3>) {
      Kokkos::View<typename IView::non_const_data_type,
                   typename IView::array_layout,
                   typename OView::memory_space,
                   typename OView::memory_traits> cv(iv.label()+"_to_"+ov.label(),
                                                     iv.extent(0),
                                                     iv.extent(1),
                                                     iv.extent(2));
      Kokkos::deep_copy(cv, iv);
      return cv;
    }
    
    template<class IView, class OView>
    auto
    outSpaceCopy(IView iv, OView ov, std::integral_constant<int, 2>) {
      Kokkos::View<typename IView::non_const_data_type,
                   typename IView::array_layout,
                   typename OView::memory_space,
                   typename OView::memory_traits> cv(iv.label()+"_to_"+ov.label(),
                                                     iv.extent(0),
                                                     iv.extent(1));
      Kokkos::deep_copy(cv, iv);
      return cv;
    }
    
    template<class IView, class OView>
    auto
    outSpaceCopy(IView iv, OView ov, std::integral_constant<int, 1>) {
      Kokkos::View<typename IView::non_const_data_type,
                   typename IView::array_layout,
                   typename OView::memory_space,
                   typename OView::memory_traits> cv(iv.label()+"_to_"+ov.label(),
                                                     iv.extent(0));
      Kokkos::deep_copy(cv, iv);
      return cv;
    }
    
    /// \brief Produce a copy of and MPI output into the target space
    template<class IView, class OView>
    auto
    outSpaceCopy(IView iv, OView ov) {
      return outSpaceCopy(iv, ov, std::integral_constant<int, int(IView::rank)>{});
    }    
    
    template<class IView, class OView>
    void cHost2Dev(IView iv, OView ov, std::true_type) {
      Kokkos::deep_copy(ov, iv);
    }
    
    template<class IView, class OView>
    void cHost2Dev(IView iv, OView ov, std::false_type) {
      Kokkos::deep_copy(ov, outSpaceCopy(iv,ov));
    }
  }
  
  /// \brief copy an MPI compatible view to a more general view
  template<class IView, class OView>
  void cHost2Dev(IView iv, OView ov) {
    static_assert(std::is_same<typename IView::non_const_data_type,typename OView::non_const_data_type>::value);
    static_assert(std::is_same<typename OView::data_type,typename OView::non_const_data_type>::value);
    assert(iv.span_is_contiguous());
    if (ov.span_is_contiguous()) {
      details::cHost2Dev(iv, ov, std::integral_constant<bool, std::is_same<typename OView::array_layout, typename IView::array_layout>::value>{});
    } else {
      typename OView::non_const_type pov(ov.label()+"_packed", ov.layout());
      details::cHost2Dev(iv, pov, std::integral_constant<bool, std::is_same<typename OView::array_layout, typename IView::array_layout>::value>{});
      Kokkos::deep_copy(ov, pov);
    }
  }
  
  /// \brief Return v->data() if v, nulptr otherwise.
  /// View must be contiguous
  template<typename AT, class... P >
  auto data(std::optional<Kokkos::View<AT,P...>> ov) -> typename Kokkos::View<AT,P...>::value_type *
  {
    if (ov) {
      assert(ov->span_is_contiguous());
      return ov->data();
    } else {
      return nullptr;
    }
  }

  /// \brief Returns the averaged profile of a 3D field overs sectors
  arr2d<real> averageOnTheta(arr3d<real const> field);
  void        averageOnTheta(arr3d<real const> field, arr2d<real> profile);
  /// \brief Returns the flattened averaged profile of a 3D field overs sectors 
  arr1d<real> averageOnThetaPhi(arr3d<real const> field);  
  void        averageOnThetaPhi(arr3d<real const> field, arr1d<real> profile);  
}

#endif
