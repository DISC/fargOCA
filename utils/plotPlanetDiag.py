import numpy as np
import matplotlib.pyplot as plt 
import math as mt
import argparse
import sys
import h5py as h5

parser = argparse.ArgumentParser(description=''' Display the evolution of semi-major axis and eccentricity providing the input file name'''.format(sys.argv[0]))

parser.add_argument('ifile', metavar='ORBIT.DAT', type=str, nargs=1,
                    help='file containing orbital elements')
parser.add_argument('ifile2', metavar='TQWK.DAT', type=str, nargs=1,
                    help='file containing ')
parser.add_argument('R0', nargs='?', type=float, default=5.2, 
                    help='Provide the unit of distance default 5.2(AU)')
parser.add_argument('-f', '--figname', type=str, dest='png_figure',
                    help='the name of the png fig, default is interactive')




args = parser.parse_args()
filename=args.ifile[0]
filenametq=args.ifile2[0]
print('Processing files:',filename,filenametq)
m=np.loadtxt(filename) 
a=m[:,1];e=m[:,2];t=m[:,0]

tq=np.loadtxt(filenametq)
time=tq[:,5];tqin=tq[:,3];tqout=tq[:,4]

R0scale = args.R0
codet2y=R0scale**1.5/2./mt.pi


physical_time={}
sigma={}


fig, axs = plt.subplots(2)
axs[0].plot(t*codet2y,a*R0scale,'r-',linewidth=1)
axs[0].grid(True,linestyle='--')
axs[0].label_outer()
axs[1].plot(t*codet2y,tqin+tqout,'b-',linewidth=1)
axs[1].grid(True,linestyle='--')
#axs[0].yaxis.tick_right()

y_label=['a (AU)','Torque (code units)']
x_label=['Time (y)','Time (y)']
for i,j,ax in zip(y_label,x_label,axs.flat):
        ax.set(xlabel=j, ylabel=i)
    

# Hide x labels and tick labels for top plots and y ticks for right plots.
#for ax in axs.flat:
#    ax.label_outer()

if args.png_figure:
    filename=args.png_figure+'.png'
    plt.savefig(filename,dpi=100,bbox_inches='tight')
else:
    plt.show()



