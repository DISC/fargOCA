// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <cctype>

#include "upgrade.hpp"

namespace fargOCA {
  template<> std::vector<DiskReferentialV0> const&
  enumValues<DiskReferentialV0>() {
    static std::vector<DiskReferentialV0> values = { DiskReferentialV0::CONSTANT, DiskReferentialV0::CIRCULAR_3BODY, DiskReferentialV0::COROTATING };
    return values;
  }

  std::ostream&
  operator<<(std::ostream& out, DiskReferentialV0 t) {
    switch (t) {
    case DiskReferentialV0::CONSTANT:
      out << "Constant";
      break;
    case DiskReferentialV0::CIRCULAR_3BODY:
      out << "Circular3Body";
      break;
    case DiskReferentialV0::COROTATING:
      out << "CoRotating";
      break;
    default:
      out << "Error";
      out.setstate(out.failbit);
    }
    return out;
  }
  std::istream&
  operator>>(std::istream& in, DiskReferentialV0& r) {
    std::string tk;
    in >> tk;
    std::transform(tk.begin(), tk.end(), tk.begin(), [](unsigned char c) { return std::toupper(c);});
    if (tk == "FIXED") {
      r = static_cast<DiskReferentialV0>(256);
      in.setstate(in.failbit);
    } else if (tk == "CONSTANT") {
      r = DiskReferentialV0::CONSTANT;
    } else if (tk == "CIRCULAR3BODY" || tk == "CIRCULAR_3BODY" || tk == "RC3BP") {
      r = DiskReferentialV0::CIRCULAR_3BODY;
    } else if (tk == "COROTATING") {
      r = DiskReferentialV0::COROTATING;
    } else {
      in.setstate(in.failbit);
      r = static_cast<DiskReferentialV0>(256);
    }
    return in;
  }
}

template <>
HighFive::DataType
HighFive::create_datatype<fargOCA::PlanetV0>()  {
  using namespace fargOCA;
  static HighFive::CompoundType tp({
      {"position", create_datatype<Triplet>()},
        {"velocity", create_datatype<Triplet>()},
          {"mass",     create_datatype<real>()}});
  return tp;
}

template <>
HighFive::DataType
HighFive::create_datatype<fargOCA::PlanetV1>()  {
  using namespace fargOCA;
  static HighFive::CompoundType tp({
      {"position", create_datatype<Triplet>()},
        {"velocity", create_datatype<Triplet>()},
          {"mass",     create_datatype<real>()},
            {"radius",   create_datatype<real>()},
              {"Hill_radius", create_datatype<real>()}});
  return tp;
}

template <>
HighFive::DataType
HighFive::create_datatype<fargOCA::TimeStamped<fargOCA::PlanetV0>>()  {
  return fargOCA::create_datatype_TimeStamped<fargOCA::PlanetV0>();
}

template <>
HighFive::DataType
HighFive::create_datatype<fargOCA::TimeStamped<fargOCA::PlanetV1>>()  {
  return fargOCA::create_datatype_TimeStamped<fargOCA::PlanetV1>();
}

namespace fargOCA {
  std::ostream&
  operator<<(std::ostream& out, ViscosityType l) {
    switch(l) {
    case ViscosityType::ALPHA:
      out << "alpha";
      break;
    case ViscosityType::CONSTANT:
      out << "constant";
      break;
    default:
      std::abort();
    }
    return out;
  }

  std::istream& 
  operator>>(std::istream& in, ViscosityType& l) {
    std::string tk;
    in >> tk;
    if (in) {
      std::transform(tk.begin(), tk.end(), tk.begin(), [](unsigned char c) { return std::toupper(c);});
      if (tk == "ALPHA") {
        l = ViscosityType::ALPHA;
      } else if  (tk == "CONSTANT") {
        l = ViscosityType::CONSTANT;
      } else {
        std::cerr << "Unknown ViscosityType: '" << tk << "'\n"
                  << "Possible values are: "
                  << ViscosityType::ALPHA << ' '
                  << ViscosityType::CONSTANT << '\n';
        in.setstate(std::ios_base::failbit);
        l = static_cast<ViscosityType>(-1);
      }
    } else {
      in.setstate(std::ios_base::failbit);
    }
    return in;
  }

  template<> std::vector<ViscosityType> const& enumValues<ViscosityType>() {
    static std::vector<ViscosityType> values = { ViscosityType::CONSTANT, ViscosityType::ALPHA };
    return values;
  }
}

