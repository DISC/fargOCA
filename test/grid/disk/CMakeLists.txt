# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

configure_file(config1.info.in config1.info @ONLY)
configure_file(config2.info.in config2.info @ONLY)
configure_file(test_import_gas.sh.in test_import_gas.sh @ONLY)

add_executable(import_gas import_gas.cpp)
target_link_libraries(import_gas fargort)
add_test(import_gas test_import_gas.sh 2)



