// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>
#include <map>
#include <ostream>
#include <sstream>
#include <iomanip>
#include <thread>
#include <iostream>
#include <filesystem>
#include <unistd.h>

#include "debug.hpp"
#include "disk.hpp"
#include "log.hpp"

namespace fargOCA {
  namespace debug {
    static bool all = false;
    
    void
    waitUser(fmpi::communicator const& comm, int rk) {
      if (rk == comm.rank()) {
	bool goOn = false;
	while (!goOn) {
	  std::cout << "Process " << getpid() << " is waiting for user at rank " << rk << ".\n";
	  std::this_thread::sleep_for(std::chrono::seconds(5));
	}
      }
    }

    namespace {
      std::filesystem::path      outputDirectory(".");
      std::map<std::string, int> indexes;
      std::string const inoutTag = "::inout";

      std::optional<std::string>
      inOutLabelBase(std::string label) {
	std::size_t found = label.rfind(inoutTag);
	bool const isInOut = ((found != label.npos)
			      && (found + inoutTag.size() == label.size()));
	if (isInOut) {
	  return label.substr(0, found);
	} else {
	  return std::nullopt;
	}
      }
    }

    bool
    activatedTrace(std::string label) {
      if (all) {
	return true;
      } else {
	auto base = inOutLabelBase(label);
	if (base) {
	  return activatedTrace(*base+"::in") && activatedTrace(*base+"::out");
	} else {
	  return bool(indexes.count(label));
	}
      }
    }
    
    void
    activateTrace(std::string label) {
      if (label == ALL) {
	all = true;
      } else {
	auto base = inOutLabelBase(label);
	if (base) {
	  activateTrace(*base+"::in");
	  activateTrace(*base+"::out");
	} else {
	  if (!bool(indexes.count(label))) {
	    indexes[label] = 0;
	    fargOCA::log(fmpi::communicator()) << "Activating trace " << label << '\n';
	  }
	}
      }
    }
    
    void
    outputDir(std::string odir) {
      outputDirectory.assign(odir);
    }

    int& 
    traceIndex(std::string label) {
      if (!activatedTrace(label)) {
	throw std::invalid_argument(label+" is not activated");
      }
      return indexes[label];
    }

    std::ofstream
    traceLog(std::string label) {
      if (!activatedTrace(label)) {
	throw std::invalid_argument(label+" is not activated");
      }
      std::ios_base::openmode mode = std::ios_base::out;
      int& index = indexes[label];
      // we just need a process specific tag, global communicator will do
      int rank = fmpi::communicator().rank();
      std::ostringstream fname;
      fname << label << '-' << rank << ".txt";
      std::filesystem::path ofile = outputDirectory / fname.str();
      if (index == 0) {
	mode |= std::ios_base::trunc;
      } else {
	mode |= std::ios_base::app;
      }
      std::ofstream log(ofile.native(), mode);
      char f = log.fill();
      log << "-" << std::setw(6) << std::setfill('-') << index
	  <<  "- " << std::setfill(f) << std::flush;
      index++;
      return std::move(log);
    }

    void
    check(std::ostream& out, std::string label) {
      if (!out.good()) {
	std::cerr << label << ": ";
	if (out.bad())  { std::cerr << "bad "; }
	if (out.fail()) { std::cerr << "fail "; }
	std::cerr << std::endl;
	out.clear();
      }
    }
    
    void
    dump(Disk const& disk, std::string label) {
      if (activatedTrace(label)) {
	int& index = indexes[label];
	std::ostringstream fmt;
	fmt << std::setfill('0') << std::setw(5) << index++ << "_" << label << ".h5";
	std::filesystem::path ofile = outputDirectory / fmt.str();
	fargOCA::log(fmpi::communicator()) << "writing " << ofile.native() << '\n';
	disk.writeH5(ofile.native());
      }
    }
    
    ScopedDump::ScopedDump(Disk const& disk, std::string label)
      : myDisk(disk), myLabel(label) {
      dump(myDisk, label+"::in");
    }

    ScopedDump::~ScopedDump() {
      dump(myDisk, myLabel+"::out");
    }
  }
}

