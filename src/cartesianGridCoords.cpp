// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include "cartesianGridCoords.hpp"
#include "grid.hpp"
#include "loops.hpp"

namespace fargOCA {
  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;
  
  namespace {
    template<GridSpacing GS>
    std::tuple<arr3d<real>, arr3d<real>, arr3d<real> >
    makeCoords(Coords<GS> const& coords) {
      arr3d<real> x = scalarView("x", coords);
      arr3d<real> y = scalarView("y", coords);
      arr3d<real> z = scalarView("z", coords);
      
      auto radMed    = coords.radiiMed();
      auto phiMed    = coords.phiMed();
      auto thetaMed  = coords.thetaMed();
      
      Kokkos::parallel_for("fill_cartesian_coords", fullRange(coords),
                           KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) {
                             x(i,h,j) = radMed(i) * sin(phiMed(h)) * cos(thetaMed(j));
                             y(i,h,j) = radMed(i) * sin(phiMed(h)) * sin(thetaMed(j));
                             z(i,h,j) = radMed(i) * cos(phiMed(h));
                           });
      return std::make_tuple(x,y,z);
    }
  
    std::tuple<arr3d<real>, arr3d<real>, arr3d<real> >
    makeCoords(Grid const& grid) {
      switch(grid.radialSpacing()) {
      case ARTH: return makeCoords(grid.as<ARTH>()); break;
      case LOGR: return makeCoords(grid.as<LOGR>()); break;
      }
      std::abort();
    }
  }
  
  CartesianGridCoords::CartesianGridCoords(std::tuple<arr3d<real const>, arr3d<real const>, arr3d<real const>> coords)
    : x(std::get<0>(coords)),
      y(std::get<1>(coords)),
      z(std::get<2>(coords)) {}
  
  CartesianGridCoords::CartesianGridCoords(Grid const& coords)
    : CartesianGridCoords(makeCoords(coords)) {}

  CartesianGridCoords::CartesianGridCoords()
    : x(), y(), z() {}

}

