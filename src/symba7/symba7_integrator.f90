! Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
!
! This file is part of FargOCA.
! FargOCA is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! any later version.
!
! FargOCA is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module symba7
  use, intrinsic::iso_C_binding
  implicit none

  type, bind(C) :: s7_integrator
     real(C_double) :: time
     integer(C_int) :: nbodies, n_massive_bodies, last_nbodies
     integer(C_int) :: take_referential_from_scratch 
     real(C_double) :: j2rp2,j4rp4 
     type(C_ptr)    :: mass, xh, yh, zh, vxh, vyh, vzh, rpl, rhill
     logical(C_INT16_T)  :: pclose ! merge close particles, 2 byte is for extra precision I guees...
     real(C_double) :: rmin, rmax, rmaxu, qmin
     real(C_double) :: tiny
  end type s7_integrator

  interface 
     function s7_new(n, t0, merge, tiny) bind(C)
       use, intrinsic::iso_C_binding
       integer(C_int), value :: n
       real(C_double), value :: t0
       logical(C_int16_t), value :: merge
       real(C_double), value :: tiny
       type(C_ptr) :: s7_new
     end function s7_new
     
     subroutine s7_delete(self) bind(C)
       use, intrinsic::iso_C_binding
       type(C_ptr), value :: self
     end subroutine s7_delete

     subroutine s7_set_boundaries(self, rmin, rmax, rmaxu, qmin) bind(C)
       use, intrinsic::iso_C_binding
       type(C_ptr), value :: self
       real(C_double), value :: rmin, rmax, rmaxu, qmin
     end subroutine s7_set_boundaries

     subroutine s7_set_planet(self, i, m, x,y,z, vx,vy,vz, r,rh) bind(C)
       use, intrinsic::iso_C_binding
       type(C_ptr), value :: self
       integer(C_int), value :: i
       real(C_double), value :: m, x,y,z, vx,vy,vz, r,rh
     end subroutine s7_set_planet
  end interface
  
contains
  function s7_last_massive(self) result(last)
    type(s7_integrator), intent(in) :: self
    integer :: last
    real(C_double), pointer, dimension(:) :: mass

    call c_f_pointer(self%mass, mass, [self%nbodies])
    call symba7_nbodm(self%nbodies, mass, self%tiny, last)
  end function s7_last_massive

  subroutine s7_write(self)
    type(s7_integrator), intent(in) :: self
    real(C_double), pointer, dimension(:) :: mass, xh, yh, zh, vxh, vyh, vzh
    call c_f_pointer(self%mass, mass, [self%nbodies])
    call c_f_pointer(self%xh, xh, [self%nbodies])
    call c_f_pointer(self%yh, yh, [self%nbodies])
    call c_f_pointer(self%zh, zh, [self%nbodies])
    call c_f_pointer(self%vxh, vxh, [self%nbodies])
    call c_f_pointer(self%vyh, vyh, [self%nbodies])
    call c_f_pointer(self%vzh, vzh, [self%nbodies])
    
    call io_write_frame_r(self%time, self%nbodies, 0, mass, &
         xh,yh,zh, vxh,vyh,vzh)
  end subroutine s7_write

  subroutine s7_step(self, dt,isenc, mergelst,mergecnt,iecnt,eoff)
    type(s7_integrator), intent(inout) :: self
    double precision, intent(in) :: dt
    integer, dimension(2,self%nbodies), intent(inout) :: mergelst
    integer, intent(inout) :: mergecnt
    integer, intent(out) :: isenc
    integer*2, dimension(self%nbodies), intent(out) :: iecnt
    real(C_double), pointer, dimension(:) :: mass, xh, yh, zh, vxh, vyh, vzh, rpl, rhill
    double precision, intent(in) :: eoff
    call c_f_pointer(self%mass, mass, [self%nbodies])
    call c_f_pointer(self%xh, xh, [self%nbodies])
    call c_f_pointer(self%yh, yh, [self%nbodies])
    call c_f_pointer(self%zh, zh, [self%nbodies])
    call c_f_pointer(self%vxh, vxh, [self%nbodies])
    call c_f_pointer(self%vyh, vyh, [self%nbodies])
    call c_f_pointer(self%vzh, vzh, [self%nbodies])
    call c_f_pointer(self%rpl, rpl, [self%nbodies])
    call c_f_pointer(self%rhill, rhill, [self%nbodies])

    call symba7_step_pl(self%take_referential_from_scratch, &
         self%time, self%nbodies, self%n_massive_bodies, mass,self%j2rp2, self%j4rp4, &
         xh,yh,zh, vxh,vyh,vzh, &
         dt,self%pclose,rpl,isenc, mergelst,mergecnt,iecnt,eoff,rhill,self%tiny)
    self%take_referential_from_scratch = 0 ! means true
  end subroutine s7_step

  subroutine s7_whole_step(self, dt)
    type(s7_integrator), intent(inout) :: self
    double precision, intent(in) :: dt

    integer*2, dimension(self%nbodies) :: iecnt
    double precision :: eoff
    integer :: isenc
    integer, dimension(2,self%nbodies) :: mergelst
    integer :: mergecnt

    if(self%last_nbodies .ne. self%nbodies) then
       self%n_massive_bodies = s7_last_massive(self)
    endif
    call s7_step(self,dt,isenc, mergelst,mergecnt,iecnt,eoff)
    self%time = self%time + dt
    self%last_nbodies = self%nbodies
    call s7_discard_massive(self,dt,isenc,mergelst,mergecnt,iecnt,eoff)
  end subroutine s7_whole_step

  subroutine s7_discard_massive(self, dt,isenc,mergelst,mergecnt,iecnt,eoff)
    type(s7_integrator), intent(inout) :: self
    double precision, intent(in) :: dt
    integer, intent(in) :: isenc
    integer, dimension(2,self%nbodies), intent(inout) :: mergelst
    integer, intent(inout) :: mergecnt
    integer*2, dimension(self%nbodies), intent(in) :: iecnt
    double precision, intent(in) :: eoff
    real(C_double), pointer, dimension(:) :: mass, xh, yh, zh, vxh, vyh, vzh, rpl,rhill
    call c_f_pointer(self%mass, mass, [self%nbodies])
    call c_f_pointer(self%xh, xh, [self%nbodies])
    call c_f_pointer(self%yh, yh, [self%nbodies])
    call c_f_pointer(self%zh, zh, [self%nbodies])
    call c_f_pointer(self%vxh, vxh, [self%nbodies])
    call c_f_pointer(self%vyh, vyh, [self%nbodies])
    call c_f_pointer(self%vzh, vzh, [self%nbodies])
    call c_f_pointer(self%rpl, rpl, [self%nbodies])
    call c_f_pointer(self%rhill, rhill, [self%nbodies])

    call discard_massive5(self%time,dt,self%nbodies,mass, &
         xh,yh,zh, vxh,vyh,vzh, &
         self%rmin, self%rmax, self%rmaxu, self%qmin, &
         self%pclose, rpl,rhill, isenc,mergelst,mergecnt,iecnt,eoff, &
         self%take_referential_from_scratch)
    self%take_referential_from_scratch = 0 ! mean true
  end subroutine s7_discard_massive

  subroutine C_s7_step(self, dt) bind(C, name='symba7_step')
    type(C_ptr), value :: self
    real(C_double), value :: dt
    type(s7_integrator), pointer :: obj
    call C_F_pointer(self, obj)
    call s7_whole_step(obj, dt)
  end subroutine C_s7_step

  subroutine C_s7_write(self) bind(C, name='symba7_write')
    type(C_ptr), value :: self
    type(s7_integrator), pointer :: obj
    call C_F_pointer(self, obj)
    
    call s7_write(obj)
  end subroutine C_s7_write

  function C_s7_nb_bodies(self) result(nb) bind(C, name='symba7_nb_bodies')
    type(C_ptr),    value :: self
    integer(C_int) :: nb
    type(s7_integrator), pointer :: obj
    call C_F_pointer(self, obj)
    nb = obj%nbodies
  end function C_s7_nb_bodies

  function C_s7_time(self) result(time) bind(C, name='symba7_time')
    type(C_ptr),    value :: self
    real(C_double) :: time
    type(s7_integrator), pointer :: obj
    call C_F_pointer(self, obj)
    time = obj%time
  end function C_s7_time
  
end module symba7
  
