# Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

set(SYMBA7_SRCS
  symba7/symba7_helio_drift.f
  symba7/symba7_chk.f
  symba7/symba7_step_interp.f
  symba7/symba7_step_recur.F
  symba7/symba7_step_pl.f
  symba7/symba7_helio_getacch.f
  symba7/symba7_merge.f
  symba7/symba7_nbodm.f
  symba7/symba7_step_helio.f
  symba7/symba7_kick.f
  symba7/symba7_getacch.f)

add_custom_target(untar_symba7 ALL)
add_custom_command(TARGET untar_symba7
  PRE_BUILD
  COMMAND ${CMAKE_COMMAND} -E tar xf ${CMAKE_BINARY_DIR}/contrib/symba7.tar
  COMMAND ${CMAKE_COMMAND} -E copy_if_different ../swift/swift.inc .
  DEPENDS ${CMAKE_BINARY_DIR}/contrib/symba7.tar untar_swift
  BYPRODUCTS ${SYMBA7_SRCS} main/swift_symba7.f
  COMMENT "Unpacking symba7 source files and kludging swift.inc.")
add_dependencies(untar_symba7 untar_swift)

set_source_files_properties(symba7/symba7_step_recur.F PROPERTIES COMPILE_FLAGS "-cpp -D_RECUR_SUB")

add_library(symba7 
  SHARED 
  ${SYMBA7_SRCS}
  symba7Engine.cpp
  symba7_integrator.f90 
  symba7_integrator_api.cpp
  swift_symba7_output.cpp)
set_target_properties(symba7 
  PROPERTIES
  Fortran_MODULE_DIRECTORY ${CMAKE_BINARY_DIR}/include/swift
  INCLUDE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/symba7
  POSITION_INDEPENDENT_CODE 1
  )
add_dependencies(symba7 untar_symba7)
target_link_libraries(symba7 PUBLIC Kokkos::kokkos HighFive swift nlohmann_json::nlohmann_json)
