// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <algorithm>
#include <functional>
#include <filesystem>

#include <boost/lexical_cast.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "scalarField.hpp"
#include "log.hpp"
#include "arrayTestUtils.hpp"

namespace fs  = std::filesystem;
namespace kk  = Kokkos;

using namespace fargOCA;

bool
test(fmpi::communicator const& world, size_t const radius, size_t const layer, size_t const sector) 
{
  std::ostream& trace = log(world.rank() == 0);
  trace << "Testing IO with " << radius << ", " << layer << ", " << sector << '\n';
  GasShape shape(1, 42, 56, 0.001, false);
  auto grid = GridDispatch::make(world,
				 *Grid::make(shape, {radius, layer, sector},GridSpacing::ARITHMETIC ));
  ScalarField out("out",*grid);
  ScalarField in("in",  *grid);
  kk::deep_copy(out.data(), real(world.rank())+0.5);
  out.setGhosts();
  std::string fname;
  {
    std::ostringstream out;
    out <<  "somegrid" << world.size() << "_" << radius << "_" << sector << "_" << layer << ".h5";
    fname = out.str();
  }
  trace << "Testing HDF5 format with " << fname << '\n';
  h5::createH5(out, world, fname);
  world.barrier();
  try {
    in.readH5(fname, "/");
  } catch (std::exception const& e) {
    trace << "Could not read back: " << e.what() << '\n';
    return false;
  }
  trace << "Write Read done,\nComparison\n";
  std::optional<arr3d<real const>> ovalues = out.merged(0);
  std::optional<arr3d<real const>> ivalues = in .merged(0);
  bool passed = ovalues && ivalues && checkAbsRelDiff(maxAbsRelDiff(*ivalues, *ovalues), 1e-17, 1e-8, trace);
  fmpi::broadcast(world, passed, 0);
  if (passed) {
    trace << "PASSED\n";
  } else {
    trace << "FAILED\n";
  }
  return passed;
}

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  
  if (argc != 4) { 
    std::cerr << "usage: " << argv[0] << " <radius> <sector> <layer>\n";
    return -1;
  }
  int radius = boost::lexical_cast<int>(argv[1]);
  int layer  = boost::lexical_cast<int>(argv[2]);
  int sector = boost::lexical_cast<int>(argv[3]);
  bool h5Passed  = test(world, radius, sector, layer);
  return h5Passed ? 0 : -1;
}
