// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <map>

#include "scalarFieldInterpolators.hpp"
#include "scalarField.hpp"
#include "details/barycentric_rational.hpp"

namespace kk = Kokkos;
#define LBD KOKKOS_LAMBDA

namespace fargOCA {
  namespace {
    typedef std::map<std::string, ScalarFieldInterpolators const*> Name2Iterpolators;
    Name2Iterpolators&
    name2Iterpolators() {
      static Name2Iterpolators map;
      return map;
    }
  }
  
  ScalarFieldInterpolators const&
  ScalarFieldInterpolators::get(std::string name) {
    auto found = name2Iterpolators().find(name);
    assert(found != name2Iterpolators().end());
    return *found->second;
  }
    
  ScalarFieldInterpolators::ScalarFieldInterpolators(std::string name)
    : myName(name) {
    auto insertion = name2Iterpolators().insert(Name2Iterpolators::value_type(name, this));
    if (!insertion.second) {
      std::cerr << "Error: an interpolator named " << name << " already exists.\n";
      std::abort();
    }
    assert (insertion.second);
  }
  
  ScalarFieldInterpolators::~ScalarFieldInterpolators() {
    name2Iterpolators().erase(name());
  }
  
  std::vector<std::string>
  ScalarFieldInterpolators::availables() {
    std::vector<std::string> names;
    for(auto const& p : name2Iterpolators()) {
      names.push_back(p.first);
    }
    return names;
  }
  
  namespace {
    template<class Wrapper>
    ScalarFieldInterpolators::Fct
    radialHelper(ScalarField const& f, int h, int j) {
      GridDispatch const& grid = f.dispatch();
      assert(grid.comm().size() == 1); // Only interpolating on one proc for now
      size_t const nr = grid.grid().global().sizes().nr;
      arr1d<real const> radii  = devRadiiValues(grid.grid().global(), 
						f.gridPos().radius,
						nr);
      arr1d<real> values("interpolator_values", nr);
      arr3d<real const> field = f.data();
      kk::parallel_for(nr, LBD(int i) { values(i) = field(i,h,j); });
      arr1d<real const> cvalues(values);
      return Wrapper(radii, values);
    }
    
    template<class Wrapper>
    ScalarFieldInterpolators::Fct
    phiHelper(ScalarField const& f, int const i, int const j) {
      GridDispatch const& grid = f.dispatch();
      auto layers = hostLayersValues(grid.grid().global(), f.gridPos().layer);
      size_t const ni = grid.grid().sizes().ni;
      arr1d<real> values("interpolator_values", ni);
      arr3d<real const> field = f.data();
      kk::parallel_for(ni, LBD(int h) {values(h) = field(i,h,j);});
      return Wrapper(layers, hostROValues(arr1d<real const>(values)));
    }

    template<class Wrapper>
    ScalarFieldInterpolators::Fct
    thetaHelper(ScalarField const& f, int const i, int const h) {
      // This one need to deal with the loop thing
      GridDispatch const& grid = f.dispatch();
      size_t const ns = grid.grid().sizes().ns;
      auto sectors = hostSectorsValues(grid.grid().global(), f.gridPos().sector, ns+1);
      arr1d<real> values("interpolator_values", ns+1);
      arr3d<real const> field = f.data();
      kk::parallel_for("extract_sectors", ns+1,
		       LBD(size_t const j) {
			 values(j) = j == ns ? field(i,h,0) : field(i,h,j);
		       });
      return Wrapper(sectors, hostROValues(values));
    }
    
    class BarycentricRationalO3Interpolators 
      : public ScalarFieldInterpolators {
      /// \brief A thin wrapper around barycentric_rational to deal with memory life span.
    public: // to make CUDA happy
      struct Wrapper {
        Wrapper(harr1d<real const> dpos, harr1d<real const> dval)
          : xs(dpos),
            fxs(dval),
            f(xs.data(), fxs.data(), fxs.size(), 3) {}
        
        template<class FCT>
        Wrapper(FCT fct, size_t n, arr1d<real const> dval)
	  : Wrapper(hostROValues<real,FCT>(fct,n),
                    hostROValues<real>(dval)) {}
        Wrapper(arr1d<real const> dpos, arr1d<real const> dval)
	  : Wrapper(hostROValues(dpos), hostROValues(dval)) {}
        Wrapper(harr1d<real const> dpos, arr1d<real const> dval)
	  : Wrapper(dpos, hostROValues(dval)) {}
        Wrapper(Wrapper&& w) = default;
        Wrapper(Wrapper const& w) = default;
        
        real operator()(real x) const { return f(x); }
        harr1d<real const> xs;
        harr1d<real const> fxs;
        details::barycentric_rational<real> f;
      };
      
    public:
      BarycentricRationalO3Interpolators() 
        : ScalarFieldInterpolators("barycentric_rational_order3") {}
      
      Fct  radial(ScalarField const& f, int h, int j) const override { return radialHelper<Wrapper>(f,h,j); }
      Fct  phi   (ScalarField const& f, int i, int j) const override { return phiHelper<Wrapper>(f,i,j); }
      Fct  theta (ScalarField const& f, int i, int h) const override { return thetaHelper<Wrapper>(f, i, h); }
    };


    class LinearInterpolators 
      : public ScalarFieldInterpolators {
    public: // to make CUDA happy
      struct Wrapper {
        Wrapper(harr1d<real const> p, harr1d<real const> v)
          : pos(p),
	    values(v) { assert(p.size() == v.size()); }
        template<typename FCT>
        Wrapper(FCT fct, size_t n, arr1d<real const> v)
          : Wrapper(hostROValues<real,FCT>(fct,n), hostROValues(v)) {}
        Wrapper(arr1d<real const> p, arr1d<real const> v)
          : Wrapper(hostROValues(p), hostROValues(v)) {}
          
        Wrapper(Wrapper&& w) = default;
        Wrapper(Wrapper const& w) = default;
        
        real operator()(real x) const {
          if (x <= pos(0)) {
            return values(0);
          } else if (x >= pos(pos.size()-1)) {
            return values(pos.size()-1);
          } else {
            int i = 0;
            while (pos(i+1) < x) {
              ++i;
            }
            assert(pos(i+1) >= x && x >= pos(i));
            real a = (values(i+1)-values(i))/(pos(i+1)-pos(i));
            real fx = a*(x-pos(i)) + values(i);
            return fx;
          }
        }
        harr1d<real const> pos;
        harr1d<real const> values;
      };
      
    public:
      LinearInterpolators() 
        : ScalarFieldInterpolators("linear") {}
      
      Fct  radial(ScalarField const& f, int h, int j) const override { return radialHelper<Wrapper>(f,h,j); }
      Fct  phi   (ScalarField const& f, int i, int j) const override { return phiHelper<Wrapper>(f,i,j); }
      Fct  theta (ScalarField const& f, int i, int h) const override { return thetaHelper<Wrapper>(f, i, h); }
    };
    
    [[maybe_unused]]BarycentricRationalO3Interpolators  barycentricRationalO3Interpolators;
    [[maybe_unused]]LinearInterpolators                 linearInterpolators;
  }
}

