// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>
#include <sysexits.h>

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/format/exceptions.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "utils.hpp"
#include "log.hpp"
#include "configLoader.hpp"
#include "simulationTrackingPlugins.hpp"
#include "simulation.hpp"
#include "gridDispatch.hpp"
#include "scalarField.hpp"
#include "h5io.hpp"
#include "h5Labels.hpp"

#include "upgrade.hpp"

namespace po  = boost::program_options;
namespace fs  = std::filesystem;
namespace HF  = HighFive;
namespace h5l = fargOCA::h5::labels;

using namespace fargOCA;
using namespace h5l;

typedef std::map<std::string, std::optional<bool>> status_map;

bool
read_cmd_line(int argc, char *argv[], po::variables_map& vm) {
  std::ostringstream usage;
  usage << argv[0] << " <disk>.h5\n"
        << "Try to upgrade an old disk file to format changes.\n"
        << "Input disk will be save in <disk>.h5-bak.\n";
  po::options_description opts(usage.str());
  opts.add_options()
    ("help", "Produce this message.")
    ("force-backup", "Force backup file overwrite if exists.")
    ("force-version", "Force version update on error(s).")
    ("reindex,i", po::value<std::string>()->implicit_value("disk%1%.h5"),
     "Make sure the file is named according to its step number in the simulation. "
     "The output format 'arg' will be used to establish the file component of the resulting file. "
     "It should contain a single `%1%` placehoder that will hold the step number.")
    ("disk-file", po::value<std::string>(), "The input disk file.");
  
  po::positional_options_description input_option;
  input_option.add("disk-file", 1);
  po::store(po::command_line_parser(argc, argv).
            options(opts).positional(input_option).run(), vm);
  po::notify(vm);
  if (bool(vm.count("help"))) {
    std::cout << opts;
    return false;
  }
  check_mandatory_options(opts, vm, {"disk-file"}, std::cerr);
  return true;
}

template<typename T> using opt = std::optional<T>;

opt<bool>
check_step(HighFive::Group root) {
  opt<int> step = h5::attributeValue<int>(root, STEP);
  if (!step) {
    std::cout << "could not find current step ('step') attribute.\n";
    opt<int> ouput_index      = h5::attributeValue<int>(root, "output_index");
    opt<int> steps_per_output = h5::attributeValue<int>(root, "steps_per_output");
    if (ouput_index && steps_per_output) {
      std::cout << "But got an 'output_index' of " << *ouput_index 
                << " and a 'steps_per_output' of " << *steps_per_output
                << std::endl;
      int default_step = *ouput_index * *steps_per_output;
      h5::writeAttribute(root, STEP, default_step);
      std::cout << "So " << default_step << " will do it.\n";
      return true;
    } else {
      std::cout << "And either 'output_index' or 'steps_per_output' were not found.\n";
      h5::writeAttribute(root, STEP, int(0));
      std::cout << "Y'know what? going with 0...\n";
      return true;
    }
  }
  return std::nullopt;
}

opt<bool>
check_nb_steps(HighFive::Group root) {
  opt<int> nb_steps = h5::attributeValue<int>(root, NB_STEPS);
  if (!nb_steps) {
    std::cout << "could not find the number of steps ('" << NB_STEPS << "') attribute.\n";
    opt<int> steps_left = h5::attributeValue<int>(root, "steps_left");
    opt<int> step       = h5::attributeValue<int>(root, STEP);
    if (steps_left) {
      std::cout << "But got a 'steps_left' attribute of value  " << *steps_left;
      if (step) {
        std::cout << " and a current step of " << *step << std::endl;
        int nb = *steps_left + *step;
      h5::writeAttribute(root, NB_STEPS, nb);
      std::cout << "So " << nb << " is ok.\n";
      return true;
    } else {
      std::cout << ", since current step is unknown, And either 'output_index' or 'steps_per_output' were not found.\n";
      h5::writeAttribute(root, NB_STEPS, *steps_left);
      std::cout << "So " << *steps_left << " will have to do.\n";
      return true;
      }
    } else {
      return false;
    }
  }
  return std::nullopt;
}

opt<bool>
check_release(HighFive::Group root) {
  using namespace h5l;
  opt<bool> status;
  std::string const RELEASE_PATH = path({PHYSIC, PLANETARY_SYSTEM, RELEASE});
  if (root.exist(RELEASE_PATH)) {
    auto release = root.getGroup(RELEASE_PATH);
    opt<real> start = h5::attributeValue<real>(release, "userStart");
    opt<real> end   = h5::attributeValue<real>(release, "userEnd");
    if (start) {
      h5::writeAttribute(release, "user_start", *start);
      std::cout << "release/userStart => release/user_start.\n";
      status = true;
    }
    if (end) {
      h5::writeAttribute(release, "user_end", *end);
      std::cout << "release/userEnd => release/user_end.\n";
      status = true;
    }
    if (bool(start) != bool(end)) {
      std::cerr << "Inconsistent encoding of \"physic/release\" section.\n";
      status = false;
    }
  }
  return status;
}

/// \brief Temporary fake configuration
ConfigMap
extractDefaultTracking(int /* nbSteps */, int modulo) {
  return ConfigMap{{{"step_timer", {}}, {"hdf5_logger", {}}}};
}

opt<bool>
check_output_config(fmpi::communicator const& /* world */, HighFive::Group root) {
  if (!root.exist(OUTPUT)) {
    opt<bool> status;
    std::cout << "No output configuration found, generating default one.\n";
    int steps_per_output = -1;
    if (root.hasAttribute("steps_per_output")) {
      steps_per_output = root.getAttribute("steps_per_output").read<int>();
      std::cout << "Found 'steps_per_output' attribute of value " << steps_per_output 
                << ", will use it to set 'hdf5_logger[modulo]' property.\n";
    } else {
      std::cout << "No 'steps_per_output' attribute found, will use 1.\n";
      steps_per_output = 1;
    }
    if (!root.hasAttribute(NB_STEPS)) {
      status = false; // should have been handled in check_nb_steps
      std::cout << "No 'nb_steps' attribute found, forcing to -1 'coz why not.\n";
    }
    ConfigMap trackerCfg{{{"step_timer", {}}, {"hdf5_logger", {}}}};
    std::cout << "The default tracker configuration is\n";
    HF::Group grp = root.createGroup(OUTPUT).createGroup(TRACKERS);
    Config::writeH5(trackerCfg, grp);
    if (!status) {
      status = true;
    }
    return status;
  } else {
    auto output = root.getGroup(OUTPUT);
    auto trackers = output.getGroup(TRACKERS);
    int moved = 0;
    {
      std::string const old_tracker = "planets_txt_logger";
      std::string const new_tracker = "planets_hdf5_logger";
      if (trackers.exist(old_tracker)) {
        std::cout << "Remove obsolete tracker '" << old_tracker << "'\n";
        trackers.unlink(old_tracker);
        if (trackers.exist(new_tracker)) {
          std::cout << "New tracker '" << new_tracker << "' is already there.\n";
        } else {
          Config logger{{{"ofile", "planets.h5"}, {"verbose", "true"}, {"when", "changed"}}};
          logger.writeH5(trackers, new_tracker);
        }
        ++moved;
      }
    }
    {
      std::string const old_tracker = "Hill_force_txt_logger";
      std::string const new_tracker = "Hill_force_hdf5_logger";
      if (trackers.exist(old_tracker)) {
        std::cout << "Remove obsolete tracker '" << old_tracker << "'\n";
        trackers.unlink(old_tracker);
        if (trackers.exist(new_tracker)) {
          std::cout << "New tracker '" << new_tracker << "' is already there.\n";
        } else {
          Config logger{{{"ofile", "Hill_forces.h5"}, {"verbose", "true"}, {"when", "changed"}}};
          logger.writeH5(trackers, new_tracker);
        }
        ++moved;
      }
    }
    if (moved>0) {
      return true;
    } else {
      return std::nullopt;
    }
  }
}

opt<bool>
reindex(std::string ifile, std::string fmt, std::string& ofile, bool force) {
  opt<int> step = h5::attributeValue<int>(HF::File(ifile, HF::File::ReadOnly).getGroup("/"), 
                                          STEP);
  if (!step) {
    std::cout << "Cannot reindex as no step was found in file.\n";
    return false;
  }
  fs::path ipath = ifile;
  fs::path opath = ipath.parent_path();
  try {
    opath /= (boost::format(fmt)%*step).str();
  } catch (boost::io::too_many_args& e) {
    std::cerr << "Invalid format string '" << fmt << "', should contain a '%1%' sequence as in disk%1%.h5.\n";
    return false;
  }
  if (ipath == opath) {
    std::cout << "No re-indexing required\n";
    return std::nullopt;
  } else {
    std::cout << "Reindexing required\n";
    try {
      if (fs::exists(opath)) {
        if (force) {
          fs::remove(opath);
        } else {
          std::cout << "But " << opath << " already exists.\n";
          return false;
        }
      }
      std::cout << "Re indexing: copying " << ipath << " to " << opath << '\n';
      fs::copy_file(ipath, opath);
    } catch (fs::filesystem_error const& e) {
      std::cout << "Could no copy file to " << opath.native() << " because: \n";
      std::cout << e.what();
      return false;
    }
    ofile = opath.native();
    return true;
  }
}

opt<bool>
check_referential_path(HighFive::Group root) {
  using namespace h5l;
  std::string new_location = path({PHYSIC,REFERENTIAL});
  std::string old_location = path({PHYSIC,PLANETARY_SYSTEM,REFERENTIAL});
  if (!root.exist(new_location)) {
    std::cout << "Could not find \"" << new_location << "\" group as expected.\n";
    if (root.exist(old_location)) {
      std::cout << "But found old format's \"" << old_location << "\" group.\n";
      std::cout << "Moving \"" << old_location << "\" to \"" << new_location << "\"..." << std::flush ;
      root.rename(old_location, new_location);
      std::cout << "done.\n";
      return true;
    } else {
      return false;
    }
  }
  return std::nullopt;
}

opt<bool>
check_viscosity(HighFive::Group root) {
  std::string visco_path = path({PHYSIC,VISCOSITY});
  if (!root.exist(visco_path)) {
    std::cerr << "No viscosity section, that's unexpected.\n";
    return false;
  } else {
    std::cout << "Are we still using ViscosityType enum ? " << std::flush;
    auto viscosity = root.getGroup(visco_path);
    if (viscosity.exist(CONFIGURATION)) {
      std::cout << " Nope, already moved to dynamic viscosity.\n";
      std::string type = h5::attributeValue<std::string>(viscosity, TYPE).value();
      if (type == "ViscoForCavity") {
	std::cout << "rename ViscoForCavity to viscosity_for_cavity.\n";
	viscosity.deleteAttribute(TYPE);
	type = "viscosity_for_cavity";
	h5::writeAttribute(viscosity, TYPE, type);
	return true;
      } else {
	return std::nullopt;
      }
    } else {
      opt<ViscosityType> type = h5::attributeValue<ViscosityType>(viscosity, TYPE);
      if (!type) {
	std::cerr << "none is found.\n";
	return false;
      } else {
        std::cerr << " Yes, moving to new style.\n";
	switch(*type) {
	case ViscosityType::CONSTANT:
	  {
	    real value = h5::attributeValue<real>(viscosity, VALUE).value();
	    viscosity.deleteAttribute(VALUE);
	    h5::writeAttribute(viscosity, TYPE, std::string("constant"));
	    Config::Properties map({{VALUE, std::to_string(value)}});
	    Config(map).writeH5(viscosity, CONFIGURATION);
	  }
	  break;
	case ViscosityType::ALPHA:
	  {
	    viscosity.rename(ALPHA_CONFIGURATION, CONFIGURATION);
	    std::string type = h5::attributeValue<std::string>(viscosity, "alpha").value();
	    viscosity.deleteAttribute("alpha");
	    if (type == "constant") { type = "constant_alpha"; }
	    h5::writeAttribute(viscosity, TYPE, type);
	  }
	  break;
	}
	return true;
      }
    }
  }
}

std::optional<std::string>
find_heaviest_planet(HighFive::Group root) {
  real mass = -1;
  std::string pname;
  if (!root.exist(PLANETARY_SYSTEM)) {
    std::cout << "\tNo planets found\n";
    return std::nullopt;
  } else {
    auto ps = root.getGroup(PLANETARY_SYSTEM);
    auto planetsGroup = ps.getGroup(PLANETS);
    for(std::string name : planetsGroup.listObjectNames()) {
      auto pgrp = planetsGroup.getGroup(name);
      if (!pgrp.exist("state")) {
        std::cerr << "Problem with planet " << name << ", skipping\n";
      } else {
        auto state = pgrp.getDataSet("state");
	Planet planet;
	state.read(planet);
	if (planet.mass() > mass) {
	  mass = planet.mass();
	  pname = name;
	}
      }
    }
  }
  if (mass < 0) {
    return std::nullopt;
  } else {
    return pname;
  }
}

opt<bool>
check_boundaries_conditions(HighFive::Group root) {
  auto physic = root.getGroup(PHYSIC);
  if (physic.exist(BOUNDARIES)) {
    auto bc = physic.getGroup(BOUNDARIES);
    std::string HANDLERS{"handlers"};
    if (bc.hasAttribute(HANDLERS)) {
      std::cout << "Old boundaries condition handler format, upgrading.\n";
      std::istringstream handlers{*h5::attributeValue<std::string>(bc, HANDLERS)};
      int step{0};
      ConfigMap bcs{};
      for(std::string handler; std::getline(handlers, handler, ',');) {
        bcs.push_back({handler, Config{{{STEP, std::to_string(++step)}}}});
      }
      PluginConfig cfg{bcs};
      physic.unlink(BOUNDARIES);
      cfg.writeH5(physic.createGroup(BOUNDARIES));
      return true;
    } else {
      return std::nullopt;
    }
  } else {
    std::cerr << "Disk has no boundaries conditions handler!!\n";
    return false;
  }
}

opt<bool>
check_referential_type(HighFive::Group root) {
  std::string ref_path = path({PHYSIC,REFERENTIAL});
  if (root.exist(ref_path)) {
    auto ref = root.getGroup(ref_path);
    try {
      opt<DiskReferentialV0> type = h5::attributeValue<DiskReferentialV0>(ref, TYPE);
      if (!type) {
	return false;
      } else {
	switch (*type) {
	case DiskReferentialV0::CIRCULAR_3BODY:
	  {
	    std::cout << "Rotation type '" << DiskReferentialV0::CIRCULAR_3BODY << "' not supported.\n";
	    std::cout << "Replacing with '" << DiskReferential::COROTATING << "' while hoping for the best.\n";
	    ref.deleteAttribute(TYPE);
	    h5::writeAttribute(ref, TYPE, DiskReferential::COROTATING);
	    std::optional<std::string> guide = find_heaviest_planet(root);
	    if (guide) {
	      h5::writeAttribute(ref, GUIDING_PLANET, *guide);
	      return true;
	    } else {
	      return false;
	    }
	  }
	  break;
	case DiskReferentialV0::COROTATING:
	  ref.deleteAttribute(TYPE);
	  h5::writeAttribute(ref, TYPE, DiskReferential::COROTATING);
	  break;
	case DiskReferentialV0::CONSTANT:
	  ref.deleteAttribute(TYPE);
	  h5::writeAttribute(ref, TYPE, DiskReferential::CONSTANT);
	  break;
	default:
	  return false; // should not be reached
	}
      }
    } catch (HighFive::AttributeException const& e) {
      // probably not a V0 attribute
      opt<DiskReferential> type = h5::attributeValue<DiskReferential>(root, TYPE);
      if (type) {
	std::cout << "DiskReferential already set to legal value " << *type << '\n';
      } else {
	std::cout << "Problem: not DiskReferential attribute.\n";
	return false;
      }
      return std::nullopt;
    }
  }
  return std::nullopt;
}

opt<bool>
check_mean_molecular_weight(HighFive::Group root) {
  if (root.exist(PHYSIC)) {
    auto physic = root.getGroup(PHYSIC);
    if (!physic.hasAttribute(MEAN_MOLECULAR_WEIGHT)) {
      std::cout << "Writing default mean molecular weight in physic.\n";
      h5::writeAttribute(physic, MEAN_MOLECULAR_WEIGHT, DEFAULT_MEAN_MOLECULAR_WEIGHT);
      return true;
    }
  }
  return std::nullopt;
}

opt<bool>
check_star_accretion_type(HighFive::Group root) {
  std::string star_accretion_path = "physic/star_accretion";
  if (root.exist(star_accretion_path)) {
    std::cout << "'physic/star_accretion' detected, check type" <<  "\n";
    auto grp = root.getGroup(star_accretion_path);
    try {
      opt<StarAccretion> type = h5::attributeValue<StarAccretion>(grp, TYPE); 
      if (!type) {
        std::cout << "No type attribute found in star_accretion section.";
        return false;
      } else {
        auto values = enumValues<StarAccretion>();
        if (std::find(values.begin(), values.end(), *type) == values.end()) {
          std::cout << "Star_accretion type of value " << *type << " not supported.\n";
          std::cout << "Remove star_accretion.\n";
          root.unlink(star_accretion_path);
          return true;
        }
      }
    } catch (std::exception const& err) {
      std::cout << "could not read StarAccretion, probably obsolete StarAccretion::CUSTOM."
                <<  "Remove star_accretion.\n";
      root.unlink(star_accretion_path);
      return true;
    }
  }
  return std::nullopt;
}

opt<bool>
check_gas_shape(HighFive::Group root) {
  std::string new_shape_path = "physic/gas_shape";
  std::string old_shape_path = "physic/shape/gas";
  if (!root.exist(new_shape_path)) {
    if (root.exist(old_shape_path)) {
      root.rename(old_shape_path, new_shape_path, true);
      return true;
    } else {
      std::cerr << "Could not find '" << new_shape_path << "', but no '" << old_shape_path << "' found either.\n";
      return false;
    }
  }
  return std::nullopt;
}

opt<bool>
check_density_cavity_width(HighFive::Group root) {
  std::string cavity_path = "physic/density/cavity";
  if (!root.exist(cavity_path)) {
    std::cout << "Could not find \"" << cavity_path << "\" group as expected.\n";
    return false;
  } else {
    auto grp = root.getGroup(cavity_path);
    opt<real> width = h5::attributeValue<real>(grp, "width");
    if (!width) {
      h5::writeAttribute(grp, "width", real(1));
      return true;
    } else {
      return std::nullopt;
    }
  }
  return std::nullopt;
}

opt<bool>
check_stellar_radiation_solver(fmpi::communicator const& world, HighFive::Group root) {
  std::string radiative_path = path({PHYSIC,ADIABATIC,RADIATIVE});
  if (root.exist(radiative_path)) {
    auto cfg = root.getGroup(radiative_path);
    opt<std::string> solver  = h5::attributeValue<std::string>(cfg, "solver");
    if (!solver) {
      std::cout << "No stellar radiative solver defined, adding \"legacy\".\n";
      h5::writeAttribute(cfg, "solver", std::string("legacy"));
      Config::Properties map;
      std::ostringstream epsilon;
      epsilon << h5::attributeValue(cfg, "epsilon", real(5.0e-7));
      map["epsilon"] = epsilon.str();
      Config(map).writeH5(cfg, "solver_configuration");
      return true;
    }
  }
  return std::nullopt;
}

opt<bool>
check_cooling(HighFive::Group root) {
  std::string cooling_path = path({PHYSIC,ADIABATIC,COOLING});
  if (root.exist(cooling_path)) {
    auto cfg = root.getGroup(cooling_path);
    opt<CoolingType> type  = h5::attributeValue<CoolingType>(cfg, TYPE);
    if (!type) {
      std::cout << "No cooling type specified, assuming \'" << CoolingType::BETA << "\'.\n";
      h5::writeAttribute(cfg, TYPE, CoolingType::BETA);
      return true;
    }
  }
  return std::nullopt;
}

opt<bool>
check_radiative_energy_derivative(fmpi::communicator const& world, HighFive::Group root) {
  if (root.exist("radiative_energy")
      && !root.exist("radiative_energy_derivative")) {
    try {
      std::cout << "Found \"radiative_energy\" field, but no possible derivative. Adding one set to 1.\n";
      GasShape shape; // we wont use that info, we just need to fill a 0 field
      auto grid = GridDispatch::make(world, *Grid::make(shape, root.getGroup("/grid")));
      ScalarField derivative("derivative",*grid);
      Kokkos::deep_copy(derivative.data(), real(0));
      std::optional<HighFive::Group> dgrp;
      if (world.rank() == 0) {
        dgrp = root.createGroup("radiative_energy_derivative");
      }
      derivative.writeH5(dgrp, 0);
      return true;
    } catch (std::exception const&  e) {
      return false;
    }
  }
  return std::nullopt;
};

namespace {
  /// do not rely on fargort, we want to stick the the old encoding
  /// when upgrading
  Triplet
  readTripletAttribute(HighFive::Group const& obj, std::string const& name) {
    Triplet value;
    auto attr = obj.getAttribute(name);
    attr.read(value);
    return value;
  }
}

opt<bool>
check_planet_radiuses(HighFive::Group root) {
  std::cerr << "Do we need to add radiuses (physical and Hill) to planets ?\n";
  if (!root.exist(PLANETARY_SYSTEM)) {
    std::cout << "\tNo planets anyway\n";
    return std::nullopt;
  } else {
    auto ps = root.getGroup(PLANETARY_SYSTEM);
    bool gotSome = false;
    bool failedSome = false;
    auto planetsGroup = ps.getGroup("planets");
    for(std::string name : planetsGroup.listObjectNames()) {
      auto pgrp = planetsGroup.getGroup(name);
      if (!pgrp.exist("state")) {
        std::cerr << "Problem with planet " << name << ", skipping\n";
        failedSome = true;
      } else {
        auto old = pgrp.getDataSet("state");
        if (Planet::hasRadiusAndHill(old)) {
          std::cout << "Planet " << name << " is up to date.\n";
        } else {
          PlanetV0 v0;
          old.read(v0);
          pgrp.unlink("state");
          PlanetV1 v1(v0);
          HighFive::DataSet ds = pgrp.createDataSet("state", v1);
          std::cout << "Planet " << name << " got updated.\n";
          gotSome = true;
        }
      }
    }
    if (gotSome && !failedSome) {
      return true;
    }
    if (failedSome) {
      return false;
    }
    return std::nullopt;
  }
}

/// \brief Check if planets are encoded with attribues in disk file.
/// If yes, encode them as datasets.
opt<bool>
check_planet_encoding(HighFive::Group root) {
  std::cerr << "Do we need to move planet restart data from attribute encoding to group encoding ?\n";
  if (!root.exist(PLANETARY_SYSTEM)) {
    std::cout << "\tNo planets anyway\n";
    return std::nullopt;
  } else {    
    auto ps = root.getGroup(PLANETARY_SYSTEM);
    bool good = true;
    bool up_to_date = true;
    bool had_some = false;
    for( auto name : ps.listObjectNames()) {
      auto pgrp = ps.getGroup(name);
      if (!pgrp.exist("state")) {
        // No state dataset, but do we have the Planet's attributes ?
        int nb = int(pgrp.hasAttribute("position")) + int(pgrp.hasAttribute("velocity")) + int(pgrp.hasAttribute("mass"));
        if (nb > 0) {
          up_to_date = false;
          had_some   = true;
          std::cout << "Attempting to move planet " << name << " from attributes to dataset...  "
                    << std::flush;
          if (nb != 3) {
            std::cout << "But only found a subset of the needed attributes.\n";
            good = false;
          } else {
            Triplet pos  = readTripletAttribute(pgrp, "position");
            Triplet vel  = readTripletAttribute(pgrp, "velocity");
            real    mass = *h5::attributeValue<real>(pgrp, "mass");
            Planet p(mass, pos, vel);
            p.writeH5(pgrp);
            pgrp.deleteAttribute("position");
            pgrp.deleteAttribute("velocity");
            pgrp.deleteAttribute("mass");
            std::cout << "done.\n";
          }
        } else {
          std::cout << "Ignoring 'planets' sub group " << name 
                    << " which does not contain either 'state' nor planets encoding attributes.\n";
        }
      }
    }
    if (up_to_date) {
      std::cout << "\tno\n";
      return std::nullopt;
    } else {
      // no empty system and it went ok
      return good && had_some; 
    }
    return true;
  }
}

template<typename T>
opt<bool>
move_attribute(HighFive::Group root, std::string from, std::string to, std::string oldname, std::string newname) {
  std::cout << "Need to move '" << from << '/' << oldname << "' to '" << to << '/' << newname << "?'\n";
  HighFive::Group from_group = root.getGroup(from); // must exist when called
  if (from_group.hasAttribute(oldname)) {
    std::cout << "Found attribute '" << oldname << "' in group '" << from 
              << "', moving to '" << to << "/" << newname << "', " << std::flush;
    if (!root.exist(to)) {
      std::cerr << " but group does not exists.\n";
      return false;
    }
    auto to_group   = root.getGroup(to); // should exist
    to_group.createAttribute(newname, from_group.getAttribute(oldname).read<T>());
    from_group.deleteAttribute(oldname);
    std::cout << "done\n";
    return true;
  } else {
    std::cout << "...no\n";
    return std::nullopt;
  }
}

template<typename T>
opt<bool>
move_attribute(HighFive::Group root, std::string from, std::string to, std::string name) {
  return move_attribute<T>(root, from, to, name, name);
}

status_map
check_old_star_attributes(HighFive::Group root) {
  std::cout << "check if radiative star configuration is too populated.\n";
  status_map statuses;
  typedef status_map::value_type pair;
  std::string from = path({PHYSIC,ADIABATIC,RADIATIVE,STAR});
  std::string to   = path({PHYSIC,ADIABATIC,RADIATIVE});
  if (!root.exist(from)) {
    std::cout << "No '" << from << "', group, nothing to do.\n";
    return statuses;
  }
  
  statuses.insert(pair("opacity_law", move_attribute<OpacityLaw>(root, from, to, "opacity_law")));;
  statuses.insert(pair("const_kappa", move_attribute<real>(root, from, to, "const_kappa")));
  statuses.insert(pair("dust_to_gas", move_attribute<real>(root, from, to, "dust_to_gas")));
  statuses.insert(pair("epsilon",     move_attribute<real>(root, from, to, "epsilon")));
  if (!root.exist(CODE_UNITS)) {
    root.createGroup(CODE_UNITS);
    real star_radius      = h5::attributeValue(root.getGroup(from), "rstar", real(1));
    real star_temperature = h5::attributeValue(root.getGroup(from), "fstar", real(4370));
    CodeUnits units(5.2, 1, star_radius, star_temperature);
    units.writeH5(root.getGroup(CODE_UNITS));
    statuses.insert(pair("code_unit_init", true));
  }
  
  return statuses;
}

status_map
check_code_units(HighFive::Group root) {
  typedef status_map::value_type pair;
  status_map statuses;
  if (!root.exist(CODE_UNITS)) {
    auto units = root.createGroup(CODE_UNITS);
    statuses.insert(pair(CODE_UNITS, true));
  }
  auto units = root.getGroup(CODE_UNITS);
  if (!units.hasAttribute(CENTRAL_BODY_RADIUS)) {
    h5::writeAttribute(units, CENTRAL_BODY_RADIUS, real(1));
    statuses.insert(pair(CENTRAL_BODY_RADIUS, true));
  }
  if (!units.hasAttribute(CENTRAL_BODY_MASS)) {
    h5::writeAttribute(units, CENTRAL_BODY_MASS, real(1));
    statuses.insert(pair(CENTRAL_BODY_MASS, true));
  }
  if (!units.hasAttribute(CENTRAL_BODY_TEMPERATURE)) {
    h5::writeAttribute(units, CENTRAL_BODY_TEMPERATURE, real(4370));
    statuses.insert(pair(CENTRAL_BODY_TEMPERATURE, true));
  }
  if (!units.hasAttribute(SEMI_MAJOR_AXIS)) {
    h5::writeAttribute(units, SEMI_MAJOR_AXIS, real(5.2));
    statuses.insert(pair(SEMI_MAJOR_AXIS, true));
  }
  return statuses;
}

opt<bool>
check_eneTimeStep(HighFive::Group root) {
  std::cout << "check if eneTimeStep needs to be added\n";
  std::string const ENERGY_PATH = path({PHYSIC,ADIABATIC,"full_energy"});
  if (!root.exist(ENERGY_PATH)) {
    std::cerr << "No '" << ENERGY_PATH << "' group !\n";
    return std::nullopt;
  } else {
    auto radiative = root.getGroup(ENERGY_PATH);
    if (!radiative.hasAttribute("eneTimeStep")) {
      std::cout << "No 'eneTimeStep' attribute, add it now\n";
      radiative.createAttribute("eneTimeStep", real(-1));
      return true;
    } else{
      return std::nullopt;
    }
  }
}

opt<bool>
check_nb_steps_in_root(HighFive::Group root) {
  if (!root.hasAttribute(NB_STEPS)) {
    if (!root.exist(OUTPUT)) {
      std::cerr << "No 'nb_steps' attribute in root, and no 'output' group !\n";
      return false;
    } else {
      auto trackers = root.getGroup(OUTPUT);
      if (!trackers.hasAttribute(NB_STEPS)) {
        std::cerr << "No 'nb_steps' attribute in root nor in 'output' group !\n";
        return false;
      } else {
        int nb_steps = trackers.getAttribute(NB_STEPS).read<int>();
        trackers.deleteAttribute(NB_STEPS);
        root.createAttribute(NB_STEPS, nb_steps);
        std::cerr << "Copied '" << NB_STEPS << "' attribute from '" << OUTPUT <<"' in root group !\n";
        return true;
      }
    }
  } else {
    return std::nullopt;
  }  
}

opt<bool>
check_adiabatic_index(HighFive::Group root) {
  std::cout << "Is adiabatic index update needed ?\n";
  std::string const ADIABATIC_PATH = path({PHYSIC,ADIABATIC});
  if (root.exist(ADIABATIC_PATH)) {
    return move_attribute<real>(root, PHYSIC, ADIABATIC_PATH, "adiabatic_index", "index");
  } else {
    std::cout << "\tNo adiabatic configuration found\n";
    auto physic = root.getGroup(PHYSIC);
    if (physic.hasAttribute("adiabatic_index")) {
      physic.deleteAttribute("adiabatic_index");
      std::cout << "\tremoved from source\n";
      return false;
    } else {
      return std::nullopt;
    }
  }
}

opt<bool>
check_init_plugins(HighFive::Group root) {
  using namespace h5l;
  std::string const INIT_PATH = path({PHYSIC,INIT});
  if (!root.exist(INIT_PATH)) {
    std::cout << "No group for init plugins, adding an empty one.\n";
    root.getGroup(PHYSIC).createGroup(INIT);
    return true;
  }
  return std::nullopt;
}

opt<bool>
check_orbital_elements(HighFive::Group root) {
  using namespace h5l;
  std::cout << "Are orbital element update needed ?\n";
  std::string const planets_path = path({PHYSIC,PLANETARY_SYSTEM,PLANETS});
  if (!root.exist(planets_path)) {
    std::cout << "no planets.\n";
    return std::nullopt;
  } else {
    auto planets = root.getGroup(planets_path);
    int upgrades = 0;
    int problems = 0;
    for (std::string name : planets.listObjectNames()) {
      auto planet = planets.getGroup(name);
      std::cout << "Planet '" << name << "'\n";
      if (planet.hasAttribute(DISTANCE)) {
        std::cout << "replacing 'distance' with 'semi_major_axis'\n";
        planet.createAttribute(SEMI_MAJOR_AXIS, 
                               planet.getAttribute(DISTANCE).read<real>());
        planet.deleteAttribute(DISTANCE);
        ++upgrades;
      }
      for(std::string oename : { MEAN_LONGITUDE, LONGITUDE_OF_PERICENTER, LONGITUDE_OF_NODE }) {
        if (!h5::attributeValue<real>(planet, oename)) {
          planet.createAttribute(oename, real(0));
          ++upgrades;
        }
      }
    }
    if (problems > 0) {
      return false;
    } else if (upgrades > 0) {
      return true;
    } else {
      return std::nullopt;
    }
  }
}

namespace names {
  std::string const SIZES   = "sizes";
  std::string const SPACING = "spacing";
  std::string const RADII   = "radii";
  std::string const LAYERS  = "layers";
  std::string const SECTORS = "sectors";
}
using namespace names;

GridSpacing
guess_spacing(arr1d<real const> coords) {
  real avg = 0;
  Kokkos::parallel_reduce("diff_sum", coords.size()-1,
			  KOKKOS_LAMBDA(int l, real& tmp) {
			    tmp += coords(l+1) - coords(l);
			  }, avg);
  avg /= coords.size()-1;
  real variation = 0;
  Kokkos::parallel_reduce("variation", coords.size()-1,
			  KOKKOS_LAMBDA(int l, real& tmp) {
			    tmp = fmax(std::abs(coords(l+1)-coords(l)-avg), tmp);
			  }, Kokkos::Max<real>(variation));
  
  if (variation < 1e-7) {
    return GridSpacing::ARITHMETIC;
  } else {
    return GridSpacing::LOGARITHMIC;
  }
}

std::optional<GridSpacing>
guess_spacing(HighFive::Group const& grid)  {
  if (grid.hasAttribute(names::SPACING)) {
    return grid.getAttribute(names::SPACING).read<GridSpacing>();
  } else {
    std::cout << "Not grid spacing attribute found. Please consider running 'disk_upgrade'.\n";
    if (!grid.exist(names::RADII)) {
      std::cerr << "No radial coords nor spacing specification in grid either ??? weird.\n";
      return std::nullopt;
    } else {
      return guess_spacing(readH5Coords(grid, names::RADII));
    }
  }
}

std::optional<GridSizes>
guess_grid_sizes(HighFive::Group const& grid)  {
  if (grid.hasAttribute(names::SIZES)) {
    return grid.getAttribute(names::SIZES).read<GridSizes>();
  } else {
    std::cout << "Not grid sizes attribute found. Please consider running 'disk_upgrade'.\n";
    if (!grid.exist(names::RADII)) {
      std::cerr << "No radial coords found either ??? weird.\n";
      return std::nullopt;
    }
    size_t const nr = readH5Coords(grid, names::RADII).size()-1;
    if (!grid.exist(names::SECTORS)) {
      std::cerr << "No sectors coords in grid either ??? weird.\n";
      return std::nullopt;
    }
    size_t const ns = readH5Coords(grid, names::SECTORS).size()-1;
    if (!grid.exist(names::LAYERS)) {
      std::cerr << "No layers coords in grid, assuming flat.\n";
    }
    size_t const ni = grid.exist(names::LAYERS) ? readH5Coords(grid, names::LAYERS).size()-1 : 1;
    return GridSizes({nr,ni,ns});
  }
}

opt<bool>
check_radial_spacing(HighFive::Group root) {
  if (!root.exist("grid")) {
    std::cerr << "No grid in disk file ??? weird.\n";
    return false;
  } 
  auto grid = root.getGroup("grid");
  if (grid.hasAttribute("spacing")) {
    return std::nullopt;
  } else {
    auto spacing = guess_spacing(grid);
    if (!spacing) {
      std::cerr << "No radial coords nor spacing specification in grid ??? weird.\n";
      return false;
    } else {
      h5::writeAttribute(grid, "spacing", *spacing);
      return true;
    }
  }
}

opt<bool>
check_time_step(HighFive::Group root) {
  using namespace h5l;
  if (!root.hasAttribute(TIME_STEP)) {
    std::cerr << "No time step at root, will look in physic section.\n";
    return move_attribute<real>(root, PHYSIC, "/", TIME_STEP, TIME_STEP);
  } else {
    return std::nullopt;
  }
}

opt<bool>
check_grid_sizes(HighFive::Group root) {
  if (!root.exist("grid")) {
    std::cerr << "No grid in disk file ??? weird.\n";
    return false;
  } else {
    auto grid = root.getGroup("grid");
    if (grid.hasAttribute("sizes")) {
      return std::nullopt;
    } else {
      auto sizes = guess_grid_sizes(grid);
      if (sizes) {
        h5::writeAttribute(grid, "sizes", *sizes);
        return true;
      } else {
        std::cerr << "Could not guess grid sizes.\n";
        return false;
      }
    }
  }
}

opt<bool>
check_hdf5_format_version(HighFive::Group root) {
  std::optional<h5::FormatVersion> found = h5::FormatVersion::read(root);
  if (!found || *found != h5::FormatVersion::current()) {
    try {
      h5::FormatVersion::current().write(root);
      return true;
    } catch (std::exception const& e) {
      std::cerr << "Problem while updating file format version: " << e.what() << '\n';
      return false;
    }
  }
  return std::nullopt;
}

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  po::variables_map vm;
  if (!read_cmd_line(argc, argv, vm)) {
    return EX_USAGE;
  }
  bool force = bool(vm.count("force-backup"));
  std::string ipath = vm["disk-file"].as<std::string>();
  backup_file(ipath, ".h5-bak", force);
  typedef status_map::value_type pair;
  status_map statuses;

  {
    HF::File ifile(ipath, HF::File::ReadWrite);
    auto root = ifile.getGroup("/");
    statuses.insert(pair("check_step", check_step(root))); 
    statuses.insert(pair("check_nb_steps", check_nb_steps(root)));
    statuses.insert(pair("check_referential_path", check_referential_path(root)));
    statuses.insert(pair("check_referential_type", check_referential_type(root)));
    statuses.insert(pair("check_star_accretion_type", check_star_accretion_type(root)));
    for(auto ks : check_old_star_attributes(root)) {
      statuses.insert(ks);
    }
    statuses.insert(pair("check_output_config", check_output_config(world, root)));
    statuses.insert(pair("check_radiative_energy_derivative", check_radiative_energy_derivative(world, root)));
    statuses.insert(pair("check_stellar_radiation_solver", check_stellar_radiation_solver(world, root)));
    statuses.insert(pair("check_density_cavity_width", check_density_cavity_width(root)));
    statuses.insert(pair("check_planet_encoding", check_planet_encoding(root)));
    statuses.insert(pair("check_planet_radiuses", check_planet_radiuses(root)));
    statuses.insert(pair("check_nb_steps_in_root", check_nb_steps_in_root(root)));
    statuses.insert(pair("check_adiabatic_index", check_adiabatic_index(root)));
    statuses.insert(pair("check_eneTimeStep", check_eneTimeStep(root)));
    statuses.insert(pair("check_orbital_elements", check_orbital_elements(root)));
    statuses.insert(pair("check_release", check_release(root)));
    statuses.insert(pair("check_gas_shape", check_gas_shape(root)));
    statuses.insert(pair("check_radial_spacing", check_radial_spacing(root)));
    statuses.insert(pair("check_grid_sizes", check_grid_sizes(root)));
    statuses.insert(pair("check_mean_molecular_weight", check_mean_molecular_weight(root)));
    statuses.insert(pair("check_cooling", check_cooling(root)));
    statuses.insert(pair("check_viscosity", check_viscosity(root)));
    for(auto ks : check_code_units(root)) {
      statuses.insert(ks);
    }
    statuses.insert(pair("check_time_step", check_time_step(root)));
    statuses.insert(pair("check_init_plugins", check_init_plugins(root)));
    statuses.insert(pair("check_boundaries_conditions", check_boundaries_conditions(root)));
    statuses.insert(pair("check_hdf5_format_version", check_hdf5_format_version(root)));
  }
  
  if (vm.count("reindex")) {
    std::string opath;
    std::string fmt = vm["reindex"].as<std::string>();
    statuses.insert(pair("reindex", reindex(ipath, fmt, opath, force)));
  }
  
  int problems = 0;
  int upgrades = 0;
  for (auto [key, status] : statuses) {
    std::cout << "Check " << key << "-> " << std::flush;
    if (status) { // was an upgrade needed ?
      ++upgrades;
      if (*status) { // did it go well ?
        std::cout << "updated\n";
      } else {
        std::cout << "had issues\n";
        ++problems;
      }
    } else {
      std::cout << "N/A\n";
    }
  }
  if (problems > 0) {
    std::cout << "Got " << problems << " issues that we could not fix.\n";
    return EX_SOFTWARE;
  }
  if (upgrades == 0) {
    std::cout << "No opportunity for upgrade detected.\n";
    fs::remove(fs::path(ipath).replace_extension(".h5-bak"));
  }
  return EX_OK;
}
