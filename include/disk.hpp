// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_DISK_H
#define FARGO_DISK_H

#include <list>
#include <map>
#include <forward_list>
#include <memory>
#include <limits>

#include <highfive/H5File.hpp>
#include <nlohmann/json_fwd.hpp>

#include "tuple.hpp"
#include "scalarField.hpp"
#include "vectorField.hpp"
#include "shared.hpp"
#include "cuda_workarounds.hpp"
#include "simulationConfig.hpp"
#include "diskInitPlugin.hpp"

namespace fargOCA {
  class PlanetarySystem;
  class DiskProfiles;
  class DiskPhysic;
  class Planet;
  class StellarRadiationSolver;
  class Disk;

  class Disk 
    : public std::enable_shared_from_this<Disk> {
  public:
    
    /// \brief Load from file.
    static shptr<Disk> make(fmpi::communicator const& comm, HighFive::Group const& root);
    /// \brief Make a disk in it's initial user specified state.
    static shptr<Disk> make(DiskPhysic const& physic, const GridDispatch& grid);
    virtual ~Disk();
    
    shptr<Disk>       shared()       { return shared_from_this(); }
    shptr<Disk const> shared() const { return shared_from_this(); }

    GridDispatch const& dispatch() const { return *myDispatch; }
    DiskPhysic   const& physic()   const { return *myPhysic; }

    /// \brief The planetary system is it exists.
    shptr<PlanetarySystem>       system()        { return mySystem; }
    /// \brief The planetary system is it exists.
    shptr<PlanetarySystem> const& system() const { return mySystem; }
    
    /// \brief The frame rotation velocity
    real frameVelocity() const;

    /// \brief Apply referential rotation velocity update.
    /// Adjust the frame velocity and gas velocity speed.
    /// Does not touch the system
    /// \param domega the rotation speed delta
    void  adjustFrameVelocity(real domega);

    /// \brief Current physical time in the simulation.
    real physicalTime()           const { return myPhysicalTime; }
    ///\brief Adjust physical time
    void addPhysicalTime(real dt)       { myPhysicalTime += dt; }
    
    // \brief Gas velocity
    VectorField&       velocity()       { return myVelocity; }
    VectorField const& velocity() const { return myVelocity; }

    /// \brief The modifiable density field
    ScalarField&            mdensity();
    /// \brief The non modifiable density field
    ScalarField const&      density() const                   { return myDensity; }
    /// \brief The modifiable energy field
    std::optional<ScalarField>&       menergy();
    /// \brief The non modifiable energy field
    std::optional<ScalarField> const& energy() const                    { return myEnergy; }
    std::optional<ScalarField>&       radiativeEnergy()                 { return myRadiativeEnergy; }
    std::optional<ScalarField> const& radiativeEnergy() const           { return myRadiativeEnergy; }

    /// \brief The stellar radiation solver.
    /// bool(physic().adiabatic->radiative) must be true
    StellarRadiationSolver&       stellarRadiationSolver()       { return *myStellarRadiationSolver; }
    StellarRadiationSolver const& stellarRadiationSolver() const { return *myStellarRadiationSolver; }
    void setGhosts();

    ScalarField const& soundSpeed() const;
    ScalarField const& pressure()   const;
    ScalarField const& viscosity()  const;

    DiskProfiles const& profiles() const { return *myProfiles; }
    void                flashProfiles();
    void                flashProfiles(DiskProfiles const& profiles);

    Triplet gasAcceleration() const;
    /// \brief Apply disk contribution to system movement
    /// \param gasAcceleration account for the indirect term.
    void advanceSystemFromDisk(real dt, Triplet gasAcceleration);
    
    /// \brief Return temperature field if radiative energy is activated
    /// Will be none if not in radiatife mode.
    /// The returned reference will be invalidated by any modification of density or energy.
    std::optional<ScalarField> const& temperature() const;
    /// \brief No idea
    /// Will be none if star accretion type is ot WIND
    /// The returned reference will be invalidated by anny modification of density
    std::optional<ScalarField> const& deadDensity() const;

    /// \brief Compute some potential based on the system. 
    /// \param gasAcceleration account for the indirect effect of gas acceleration
    void computeSystemPotential(std::optional<Triplet> gasAcceleration, arr3d<real> potential) const;
    
    /// \brief Condition Courant Friedichs Lewy applied to the gas.
    /// This value is limited to the local grid.
    real localConvergingTimeStep() const;

    /// \brief Compute a converging time step based on the state of the disk.
    real convergingTimeStep() const;

    /// \brief Return delaT/N with N=ceil(dtLeft/convergingtimestep())
    /// That is is, a "lower rounded" value of convergingTimeStep that 
    /// approximate a regular time step.
    real convergingTimeStep(real deltaT) const;

    real momentum()  const;
    real totalMass() const;
    real totalEnergy() const;
    real kineticRadial() const;
    real kineticPolar()  const;
    real kineticKeplerian() const;

    void writeH5(std::string fname) const;
    bool writeH5(std::optional<HighFive::Group> group, int writer = 0, bool indirectFields = false) const;

    /// \brief Add a new boundary condition function.
    /// Apply the boundary condition functions in the order in wich they were added.
    void applyBoundaryConditions(real dt);

    /// \brief Add a user field.
    /// The field can then be retreived by name and updated as needed.
    /// \param name the name of the field must be unique.
    /// \return true if the field was added, false if it already existed. In the
    /// later situation, the ouput and managedGhost flag will be applied.
    bool declareUserField(std::string name);
    /// \brief Lookup the field by name.
    /// The field must have been previously registred with addUserField
    ScalarField&       getUserField(std::string name);
    /// \override
    ScalarField const& getUserField(std::string name) const;
    
    /// \brief Add a marker.
    /// A marker is an arbitrary field that will just follow the gas
    /// whihout acting on it.
    /// Fails if there is a marker withs the same name is already registered.
    void addTracer(ScalarField const& field);
    void addTracer(ScalarField&& marker);
    /// \brief Is a marker with that name registered ?
    bool hasTracer(std::string marker) const;
    /// \brief retrieve an unordered list of registered tracers.
    std::forward_list<ScalarField> const& tracers() const { return myTracers; }
    std::forward_list<ScalarField>&       tracers()       { return myTracers; }

    /// \brief Rebind to a new grid.
    void rebind(GridDispatch const& nextGrid, ScalarFieldInterpolators const& interpolators);
    void rebind(GridDispatch const& nextGrid);

    /// \brief Return the acceleration that the gas applies onto the star
    Triplet computeGasAcceleration() const;
    
    /// \brief import gas from another disk
    /// \param gas the disk to import the gas frpm
    /// \param gridPrec the tolerance between the two grid coordinates 
    void importGas(Disk const& gas, real gridTol = 1e-16);

    bool readH5(HighFive::Group const& root);
    bool readH5(std::string fname);
    bool importGas(HighFive::Group const& root);
    
    class NamedFields;
    friend class NamedFields;
    /// \brief A read only name->field container
    NamedFields namedFields() const;

    static void abortOnGasCheck(bool v);
    static bool abortOnGasCheck();
    
    bool checkPhysicalQuantities() const;

  public:
    void trashSoundSpeed();
    void trashDensity();
    void trashEnergy();
    void trashPressure();
    void trashViscosity();
    void trashTemperature();
    void trashDeadDensity();
    void trashDerivedFields();
    void signal(DiskInitEvent);

  cuda_private:
    template<GridSpacing GS> real localConvergingTimeStep() const;
    template<GridSpacing GS> void computeSystemPotential(std::optional<Triplet> gasAcceleration, arr3d<real> potential) const;
    template<GridSpacing GS> void computeSoundSpeed();
    void computePressure();
    void computeViscosity();
    void computeTemperature();
    template<GridSpacing GS> void computeDeadDensity();
    void cleanGasMinimums();
    template<GridSpacing GS> void initRadialVelocity();
    template<GridSpacing GS> real momentum() const;
    template<GridSpacing GS> void initThetaVelocity();
    template<GridSpacing GS> void initVelocity();
    template<GridSpacing GS> void initDensity();
    template<GridSpacing GS> void initEnergy();
    
  private:

    struct NoInit {};
    Disk(shptr<PlanetarySystem>    sys,
         shptr<DiskPhysic const>   physic,
         shptr<GridDispatch const> grid);
    Disk(NoInit,
         shptr<PlanetarySystem>    sys,
         shptr<DiskPhysic const>   physic,
         shptr<GridDispatch const> grid);
    Disk(Disk const& other) = delete;

    void mapStateFields(std::function<void (ScalarField const&)> f) const;
    void mapStateFields(std::function<void (ScalarField&)> f);
    
    real initialFrameVelocity();
    void initGasMemory();
    template<GridSpacing GS> void initGasValue();

  private:
    shptr<PlanetarySystem>    mySystem;
    shptr<DiskPhysic const>   myPhysic;
    shptr<GridDispatch const> myDispatch;
    
    VectorField                myVelocity;
    ScalarField                myDensity;
    ScalarField                mySoundSpeed;
    ScalarField                myPressure;
    ScalarField                myViscosity;
    std::optional<ScalarField> myTemperature;
    std::optional<ScalarField> myDeadDensity;
    std::optional<ScalarField> myEnergy;
    std::optional<ScalarField> myRadiativeEnergy;
    bool                       mySoundSpeedClean;
    bool                       myPressureClean;
    bool                       myViscosityClean;
    std::unique_ptr<StellarRadiationSolver> myStellarRadiationSolver;
    std::unique_ptr<DiskProfiles>           myProfiles;
    
    std::map<std::string, ScalarField> myUserFields;
    std::forward_list<ScalarField>     myTracers;
    
    real       myFrameOmega;
    real       myPhysicalTime;
  };

  /// \brief Return a grid box containing the Roche sphere of the planet.
  /// Only the section of the box that intersect with the grid
  /// section managed by the current MPI process is returned.
  /// \returns The begin and ending coordinates of the box if the instersection is not empty.
  std::optional<std::pair<GridSizes, GridSizes>>  getRocheBox(Disk const& disk, Planet const& planet);
    
  /// \brief A range of string->scalar field pairs extracted from a disc.
  /// Only valid as long as no modifying operation is performed on the subject disk.
  class Disk::NamedFields {
  public:
    NamedFields(Disk const& disk);
    ~NamedFields();
    typedef std::map<std::string, ScalarField const&> Map;
    Map::const_iterator begin() const { return myMap.begin(); }
    Map::const_iterator end()   const { return myMap.end(); }
    Map::const_iterator find(std::string path)  const { return myMap.find(path); }
    ScalarField const& at(std::string path)  const { return myMap.at(path); }

  private:
    Map myMap;
  };
  
  inline
  Disk::NamedFields
  Disk::namedFields() const {
    return NamedFields(*this);
  }

  void to_json(nlohmann::json& j, SimuConfigWrapper<Disk> const& c);
  void from_json(nlohmann::json const& j, SimuConfigWrapper<Disk>& c);
}

#endif
