// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <istream>
#include <sstream>
#include <set>

#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/replace.hpp>

#include "optIO.hpp"
#include "configLoader.hpp"
#include "configProperties.hpp"
#include "boundariesConditionsPlugin.hpp"

namespace fargOCA {

  namespace pt = boost::property_tree;
  namespace cp = configProperties;
  using namespace configProperties;
  
  namespace {
    bool
    CheckSmoothingSize(pt::ptree const& pt, std::ostream& log) {
      real size = pt.get<real>(DISK/SMOOTHING/SIZE, real(0.5));
      if (size < 1e-3) {
        if (size < 0) {
          log << "Error: a negative smoothing of gravitational potential is used.\n";
          return false;
        }
        log << "Warning: a very low RocheSmoothing of gravitational potential is used. Check that it corresponds to minumum 2 gridcells.\n";
      }
      return true;
    }
    
    bool
    CheckRadiativeConfig(pt::ptree const& cfg, std::ostream& log) {
      if (ConfigLoader::enabled(cfg, DISK/ADIABATIC_EOS/FULL_ENERGY_EOS)) {
        auto layers = cfg.get_optional<std::string>(DISK/GRID/LAYERS);
        bool flat = false;
        if (!layers) {
          flat = true;
        } else {
          try {
            size_t const ni = boost::lexical_cast<int>(*layers);
            flat = ni == 1;
          } catch (boost::bad_lexical_cast&) {/* it's probably a file name */}
        }
        if (flat) {
          log << "Fatal: asked for '" << str(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS) << "' in 2D.\n"
              << "Energy equation with heating and cooling is not yet coded in 2D, you can use adiabatic with cooling.\n";
          return false;
        }
      }
      return true;
    }
    
    bool
    CheckAccretionTime(pt::ptree const& pt, std::ostream& log) {
      auto planets = pt.get_child_optional(PLANETARY_SYSTEM/PLANETS);
      if (planets) {
	bool good = true;
        for( auto const& [name, planet] : *planets) {
	  auto accTime = planet.get_optional<real>(ACCRETION_TIME);
	  if (accTime && *accTime <= 0) {
	    log << "Fatal: in planet '" << name << ", "<< str(ACCRETION_TIME) << " must be > 0.\n";
	    good = false;
	  }
	  auto accTimeInv = planet.get_optional<real>(ACCRETION_TIME_INVERSE);
	  if (accTimeInv) {
	    if (*accTimeInv <= 0) {
	      log << "Fatal: in planet '" << name << ", "<< str(ACCRETION_TIME_INVERSE) << " must be > 0.\n";
	      good = false;
	    }
	    log << "Info: in planet '" << name << ", use " << str(ACCRETION_TIME) << " instead of "
		<< str(ACCRETION_TIME_INVERSE) << '\n';
	  }
	  if (accTime && accTimeInv) {
	    log << "Fatal: in planet '" << name << ", can't specify both "<< str(ACCRETION_TIME_INVERSE)
                << " and " << str(ACCRETION_TIME) << '\n';
            good = false;
          }
	}
        log << std::flush;
        return good;
      } else {
        return true;
      }
    }    
  }
  
  ConfigLoader::ConfigLoader(std::ostream& log) 
    : myTreeChecks(), myTreePhysicChecks(), myLog(log),
      myAllowDefaults(true)
  {
    myTreeChecks.insert(myTreeChecks.begin(),
                        {CheckSmoothingSize,
                         CheckRadiativeConfig,
                         CheckAccretionTime});
  }
  
  ConfigLoader::ptree
  ConfigLoader::load(std::string fname) {
    ptree tmp;
    boost::property_tree::read_info(fname, tmp);
    myCfgRoot = tmp;
    bool ok = true;
    for(auto const& c : myTreeChecks) {
      if (!c(tmp, myLog)) {
        ok = false;
      }
    }
    if (!ok) {
      std::exit(EX_CONFIG);
    }
    return myCfgRoot;
  }
  
  bool
  ConfigLoader::enabled(ptree const& cfg, pt::path const& path) {
    return (cfg.get_child_optional(path) && cfg.get(path, true));
  }
  
  bool
  ConfigLoader::erase(ptree& cfg, path p) const {
    if (!p.empty()) { 
      std::string key = p.reduce();
      if (p.single()) {
        return cfg.erase(key) > 0;
      } else {
        auto child = cfg.get_child_optional(key);
        if (child) {
          bool erased = erase(*child, p);
          if (child->empty()) {
            cfg.erase(key);
          }
          return erased;
        }
      }
    }
    return false;
  }

  std::vector<std::string>
  ConfigLoader::strings(std::string seq, char sep) {
    std::istringstream ss(seq);
    std::string tk;
    std::vector<std::string> r;
    while (std::getline(ss, tk, sep)) {
      r.push_back(tk);
    }
    return r;
  }

  namespace {
    std::string
    MdBullet(int tab) {
      static std::string bullet = "*";
      static std::string space  = "   ";
      return tab == 0 ? bullet : (space + MdBullet(tab-1));
    }
  }
}
