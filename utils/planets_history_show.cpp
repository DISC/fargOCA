// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/algorithm/string.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "codeUnits.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "h5io.hpp"
#include "optIO.hpp"
#include "simulation.hpp"
#include "h5Labels.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;
using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extracts planet informations out of a log file.\n"
        << '\t' << cmd << " <file>.h5 [option]+ \nValid options are.\n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("ofile-fmt,o", po::value<std::string>()->default_value("planet%1%.txt"), "format of the ouput files names (ex: planet%1%.txt). Planet name will be used to distinguish them.")
    ("no-header", "Do not print the header in the output file.")
    ("format,f", po::value<std::string>()->default_value("p:v:r:h:m:k:e"), "What is printed in the output file:\n p: position\n v: velocity\n r: radius\n h: Hill radius m: mass\n k: Keplerian Frequency\n e: eccentricity.")
    ("path", po::value<std::string>()->default_value("/"), "if the log is not under root, indicate path.")
    ("code-units", po::value<std::string>(), "The file containing the code units to be used.")
    ("precision,p", po::value<int>()->default_value(8),"The printed precision.");

  po::options_description hidden(usage.str());
  hidden.add_options()
    ("file", po::value<std::string>(), "HDF5 file containing the disk file or planetary system log.");
  
  try {
    po::positional_options_description pos;
    pos.add("file", 1);

    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_USAGE);
  } 
  for(std::string opt : {"file"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);
  
  std::cout << "Warning: Will use default code units.\n";
  std::string ifname = vm["file"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadOnly);
  using h5::labels::CODE_UNITS;
  CodeUnits units = ifile.exist(CODE_UNITS) ? CodeUnits(ifile.getGroup(CODE_UNITS)) : CodeUnits();
  auto logs = PlanetarySystem::readH5Logs(ifile.getGroup("/"));
  boost::format ofmt(vm["ofile-fmt"].as<std::string>());
  std::vector<std::string> fields;
  boost::split(fields, vm["format"].as<std::string>(), boost::is_any_of(":"));
  for (std::string tk: fields) {
    if (std::set<std::string>( { "p", "v", "r", "h", "m", "k", "e"} ).count(tk) == 0) {
      std::cerr << tk << " is not a supported field.\n";
      std::exit(-1);
    }
  }
  for(auto const& kv : logs) { 
    std::string ofname = (ofmt%kv.first).str();
    log(world) << "writing '" << ofname << "'\n";
    std::ofstream ofile(ofname);
    if (!bool(vm.count("no-header"))) {
      ofile << "   Phys. time\t";
      for (std::string f: fields) {
        if (f == "p") {
          ofile << "\t\t\t\tPosition (x,y,z)\t\t";
        } else if (f == "v") {
          ofile << "\t\t\tVelocity (vx,vy,vz)\t\t";
        } else if (f == "m") {
          ofile << "\t\t\tMass\t\t";
        } else if (f == "r") {
          ofile << "\t\t\tRadius\t\t";
        } else if (f == "h") {
          ofile << "\t\t\tHill\t";
        } else if (f == "h") {
          ofile << "\t\t\tHill\t";
        } else if (f == "k") {
          ofile << "Keplerian freq.\t";
        } else if (f == "e") { 
          ofile << "Eccentricity";
        }
      }
      ofile << '\n';
    }
    ofile << std::scientific;
    ofile.precision(vm["precision"].as<int>());
    for(TimeStamped<Planet> const& ts : kv.second) {
      ofile << ts.time << '\t';
      for (std::string f: fields) {
        if (f == "p") {
          ofile << ts.state.position() << '\t';
        } else if (f == "v") {
          ofile << ts.state.velocity() << '\t';
        } else if (f == "m") {
            ofile << ts.state.mass() << '\t';
        } else if (f == "r") {
          ofile << ts.state.radius() << '\t' ;
        } else if (f == "h") {
          ofile << fargOCA::HillRadius(units, ts.state) << '\t' ;
        } else if (f == "k") {
          ofile << fargOCA::keplerianFrequency(units, ts.state) << '\t';
        } else if (f == "e") { 
          ofile << fargOCA::eccentricity(units, ts.state);
        }
      }
      ofile << '\n';
    }
  }
  return 0;
}
