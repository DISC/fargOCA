// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "vectorField.hpp"
#include "scalarFieldInterpolators.hpp"
#include "h5io.hpp"

namespace fargOCA {
  namespace {
    namespace labels {
      std::string const RADIAL = "radial";
      std::string const THETA  = "theta";
      std::string const PHI    = "phi";
    }
  }
  VectorField::VectorField(GridDispatch const& dispatch) 
    : myRadial(labels::RADIAL,  dispatch, GridPositions(GridPosition::INF, GridPosition::MED, GridPosition::MED)),
      myTheta(labels::THETA, dispatch, GridPositions(GridPosition::MED, GridPosition::INF, GridPosition::MED)),
      myPhi(labels::PHI,     dispatch, GridPositions(GridPosition::MED, GridPosition::MED, GridPosition::INF)) {}
  
  VectorField::~VectorField() {}

  void 
  VectorField::rebind(GridDispatch const& next, ScalarFieldInterpolators const& interpolators) {
    myRadial.rebind(next, interpolators);
    myTheta.rebind(next, interpolators);
    myPhi.rebind(next, interpolators);
  }
  
  void
  VectorField::rebind(GridDispatch const& dispatch) {
    rebind(dispatch, ScalarFieldInterpolators::get());
  }
  
  void
  VectorField::readH5(HighFive::Group const& group) {
    myRadial.readH5(group.getGroup(labels::RADIAL));
    myTheta.readH5(group.getGroup(labels::THETA));
    if (dispatch().grid().dim() > 2) {
      myPhi.readH5(group.getGroup(labels::PHI));
    }
  }

  void
  VectorField::writeH5(std::optional<HighFive::Group> group, int root) const {
    assert(bool(group) == (dispatch().comm().rank()==root));
    myRadial.writeH5(h5::createSubGroup(group, labels::RADIAL), root);
    myTheta.writeH5(h5::createSubGroup(group, labels::THETA), root);
    if (dispatch().grid().dim() > 2) {
      myPhi.writeH5(h5::createSubGroup(group, labels::PHI), root);
    }
  }
}
