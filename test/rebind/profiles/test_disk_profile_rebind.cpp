// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <string>

#include "environment.hpp"
#include "diskProfiles.hpp"
#include "gridDispatch.hpp"
#include "arrayTestUtils.hpp"
#include "loops.hpp"

using namespace fargOCA;
namespace HF = HighFive;


bool
relativeDiff(GridDispatch const& grid, std::string label, arr2d<real const> data, arr2d<real const> orig, real const tol) {
  return checkAbsRelDiff(maxAbsRelDiff(data, orig), tol, 1e-5, std::cout);  
}

int
relativeDiff(DiskProfiles const& p1, DiskProfiles const& p2) {
  int failed = 0;
  if (!relativeDiff(p1.dispatch(), "vrad",    p2.velocity().radial(), p1.velocity().radial(), 1e-7)) { ++failed; }
  if (!relativeDiff(p1.dispatch(), "vphi",    p2.velocity().phi(),    p1.velocity().phi(),    1e-7)) { ++failed; }
  if (!relativeDiff(p1.dispatch(), "vtheta",  p2.velocity().theta(),  p1.velocity().theta(),  1e-7)) { ++failed; }
  if (!relativeDiff(p1.dispatch(), "density", p2.density(),           p1.density(),           1e-7)) { ++failed; }
  if (p1.energy()) {
    if (!relativeDiff(p1.dispatch(),"energy",*p2.energy(),           *p1.energy(),            1e-7)) { ++failed; }
  }
  return failed;
}

int
main(int argc, char* argv[]) {

  Environment  env(argc, argv);
  fmpi::communicator world;
  
  size_t const gnr = 20*world.size();
  size_t const ni  = 10;
  size_t const ns  = 11;
  
  GasShape     shape(1, 2, 20, 0.1, false);
  std::cout << std::scientific;
  std::cout.precision(10);
  
  auto dispatch1 = GridDispatch::make(world, *Grid::make(shape, {gnr, ni, ns}, GridSpacing::ARITHMETIC));
  auto dispatch2 = GridDispatch::make(world, *Grid::make(shape, {gnr*2, ni*2, ns*2}, GridSpacing::ARITHMETIC));
  auto dispatch4 = GridDispatch::make(world, *Grid::make(shape, {gnr*4, ni*4, ns*4}, GridSpacing::ARITHMETIC));

  auto const& coords1 = dispatch1->grid();
  size_t const nr  = coords1.sizes().nr;

  arr2d<real> radv    = profileView("velocity/radial", coords1);
  arr2d<real> phiv    = profileView("velocity/phi", coords1);
  arr2d<real> thetav  = profileView("velocity/theta", coords1);
  arr2d<real> density = profileView("density", coords1);
  arr2d<real> energy  = profileView("energy", coords1);
  size_t const radialOffset1 = coords1.radialOffset();
  Kokkos::parallel_for("init", range2D({nr,ni}),
                       KOKKOS_LAMBDA(size_t const i, size_t const h) {
                         size_t const ii = radialOffset1 + i;
                         radv(i,h)    = ii*100+h+0.1;
                         phiv(i,h)    = ii*100+h+0.2;
                         thetav(i,h)  = ii*100+h+0.3;
                         density(i,h) = ii*100+h+0.4;
                         energy(i,h)  = ii*100+h+0.5;
                       });
  DiskProfiles profiles1(*dispatch1, radv, phiv, thetav, density, energy);
  DiskProfiles profiles2(*dispatch2, profiles1);
  DiskProfiles profiles3(*dispatch4, profiles2);
  DiskProfiles profiles4(*dispatch1, profiles3);
  
  int failed = 0;
  failed += relativeDiff(profiles1, profiles4);
  if (world.rank() == 0) {
    if (failed > 0) {
      std::cerr << "FAILED\n";
    } else {
      std::cerr << "PASSED\n";
    }
  }
  return failed;
}
