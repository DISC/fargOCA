// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_DETAILS_BARYCENTRIC_RATIONAL_H
#define FARGOCA_DETAILS_BARYCENTRIC_RATIONAL_H

#include <map>

#include <boost/math/interpolators/barycentric_rational.hpp>

namespace fargOCA {
  namespace details {  
#if BOOST_VERSION < 107700
    using boost::math::barycentric_rational;
#else
    using boost::math::interpolators::barycentric_rational;
#endif
  }
}
#endif
