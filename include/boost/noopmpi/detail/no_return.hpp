// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_NO_RETURN_HPP
#define BOOST_NOOPMPI_NO_RETURN_HPP

#include <cstdlib>
#include <type_traits>

#include <boost/noopmpi/config.hpp>

namespace boost { namespace noopmpi {
namespace detail {
template<typename T> T no_return() { typename std::add_pointer<T>::type t = nullptr; std::abort(); return *t; }
inline void no_return() { std::abort(); }
}
}}

#endif // BOOST_NOOPMPI_NO_RETURN_HPP
