// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>

namespace fargOCA {
  namespace py11 {
    void exportCommunicator(pybind11::module_& m);
    void exportEnvironment(pybind11::module_& m);
    void exportSimulation(pybind11::module_& m);
    void exportDisk(pybind11::module_& m);
    void exportScalarField(pybind11::module_& m);
    void exportVectorField(pybind11::module_& m);
    void exportPlanetarySystem(pybind11::module_& m);
  }
}
