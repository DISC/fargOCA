// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_DEBUG_HPP
#define FARGOCA_DEBUG_HPP

#include <string>
#include <ostream>
#include <fstream>

#include "allmpi.hpp"

namespace fargOCA {
  class Disk;
  
  namespace debug {
#if defined(NDEBUG)
    constexpr bool enabled = false;
#else
    constexpr bool enabled = true;
#endif
#if !defined(NDEBUG) && defined (__INTEL_LLVM_COMPILER) && __INTEL_LLVM_COMPILER > 20250000 && defined (SYCL_LANGUAGE_VERSION)
    // this compiler has a bug in assembly code generation related with __PRETTY_FUNCTION__
    constexpr bool assertInKokkosInline = false;
#else
    constexpr bool assertInKokkosInline = true;
#endif
    void waitUser(fmpi::communicator const& comm, int rk);

    void outputDir(std::string odir);
    void activateTrace(std::string label);
    bool activatedTrace(std::string label);
    
    int& traceIndex(std::string label);
    std::ofstream traceLog(std::string label);
    void check(std::ostream& out, std::string label);
    void dump(Disk const& disk, std::string label);
    
    class ScopedDump {
    public:
      ScopedDump(Disk const& disk, std::string label);
      ~ScopedDump();
      
    private:
      Disk const& myDisk;
      std::string myLabel;
    };
    // common tags
    static std::string const ALL = "all";
    static std::string const ACCRETED_MOMENTUMS    = "accreted_momentums";
    static std::string const BOUNDARIES_CONDITIONS = "boundaries_conditions";
    static std::string const COMPUTE_DT            = "compute_dt";
    static std::string const DT_FIELD              = "dt_field";
    static std::string const DT_VT                 = "dt_vt";
    static std::string const ENERGY_STEP           = "energy_step";
    static std::string const VELOCITY_STEP         = "velocity_step";
    static std::string const REMOVE_ACCRETED_GAS   = "remove_accreted_gas";
  }
}

#endif 
