// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <exception>

#include <Kokkos_Array.hpp>
#include "scalarField.hpp"
#include "loops.hpp"
#include "rebind.hpp"

namespace fargOCA {
  namespace kk = Kokkos;
  
  template<typename FCOORDS, typename TCOORDS>
  arr3d<real>
  radial_rebind(arr3d<real> from, FCOORDS from_coords, TCOORDS to_coords, size_t const to_nr) {
    size_t const from_nr = from.extent(0);
    arr3d<real> res { "radial", to_nr, from.extent(1), from.extent(2) };
    kk::parallel_for("radial_interpolation", range(res),
                     KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) {
                       // 3 points needs to fit
                       size_t const idx = kk::min(kk::max(std::size_t(1),
                                                          closest_coord_index(to_coords(i), from_coords, 0, from_nr-1)),
                                                  from_nr-2);
                       res(i,h,j) = interpolation({from_coords(idx-1),from_coords(idx),from_coords(idx+1)},
                                                  {from(idx-1,h,j), from(idx,h,j), from(idx+1,h,j)}, 
                                                  to_coords(i));
                     });
    return res;
  }

  template<GridSpacing FGS, GridSpacing TGS>
  arr3d<real>
  radial_rebind(arr3d<real> from, GridPosition pos, Coords<FGS> const& from_grid, Coords<TGS> const& to_grid) {
    if (FGS == TGS && from_grid.sizes().nr == to_grid.sizes().nr) {
      return from;
    } else {
      return {pos == GridPosition::INF
              ? radial_rebind(from, from_grid.radii(), to_grid.radii(), to_grid.sizes().nr)
              : radial_rebind(from, from_grid.radiiMed(), to_grid.radiiMed(), to_grid.sizes().nr)};
    }
  }
  
  template<class FCOORDS, class TCOORDS>
  arr3d<real>
  altitude_rebind(arr3d<real> from, FCOORDS from_coords, TCOORDS to_coords, size_t const to_ni) {
    size_t const from_ni = from.extent(1);
    arr3d<real> res { "altitude", from.extent(0), to_ni, from.extent(2) };
    kk::parallel_for("altitude_interpolation", range(res),
                     KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) {
                       real   const x   = to_coords(h);
                       // 3 points needs to fit
                       size_t const idx = kk::min(kk::max(std::size_t(1),
                                                          closest_coord_index(x, from_coords, 0, from_ni-1)),
                                                  from_ni-2);
                                                  
                       res(i,h,j) = interpolation({from_coords(idx-1), from_coords(idx), from_coords(idx+1)},
                                                  {from(i,idx-1,j), from(i,idx,j), from(i,idx+1,j)},
                                                  x);
                     });
    return res;
  }

  template<GridSpacing FGS, GridSpacing TGS>
  arr3d<real>
  altitude_rebind(arr3d<real> from, GridPosition pos, Coords<FGS> const& from_grid, Coords<TGS> const& to_grid) {
    size_t const from_ni = from.extent(1);
    size_t const to_ni   = to_grid.sizes().ni;
    if (from_ni == to_ni) {
      return from;
    } else {
      return (pos == GridPosition::INF
              ? altitude_rebind(from, from_grid.phi(),    to_grid.phi(),   to_ni)
              : altitude_rebind(from, from_grid.phiMed(), to_grid.phiMed(),to_ni)); 
    }
  }
    
  template<class COORDS>
  arr3d<real>
  azimutal_rebind(arr3d<real> from, COORDS from_coords, COORDS to_coords, size_t to_ns) {
    size_t const from_ns = from.extent(2);
    arr3d<real> res { "azimutal", from.extent(0), from.extent(1), to_ns };
    kk::parallel_for("azimutal_interpolation", range(res),
                     KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) {
                       using signed_index = long int;
                       real x = to_coords(j);
                       signed_index const idx   = closest_coord_index(x, from_coords, 0, from_ns)%from_ns;
                       res(i,h,j) = interpolation({from_coords(idx-1), from_coords(idx), from_coords(idx+1)},
                                                  {from(i,h,nsprev(from_ns,idx)), from(i,h,idx), from(i,h,nsnext(from_ns,idx))},
                                                  x);
                     });
    return res;
  }

  template<GridSpacing FGS, GridSpacing TGS>
  arr3d<real>
  azimutal_rebind(arr3d<real> from, GridPosition pos, Coords<FGS> const& from_coords, Coords<TGS> const& to_coords) {
    size_t const from_ns = from.extent(2);
    size_t const to_ns   = to_coords.sizes().ns;
    if (from_ns == to_ns) {
      return from;
    } else {
      return (pos == GridPosition::INF
              ? azimutal_rebind(from, from_coords.theta(),    to_coords.theta(),    to_ns)
              : azimutal_rebind(from, from_coords.thetaMed(), to_coords.thetaMed(), to_ns));
    }
  }
  
  template<GridSpacing FromGS, GridSpacing ToGS>
  void
  ScalarField::rebindImpl(GridDispatch const& to_dispatch) {
    auto const& from_grid = dispatch().grid().as<FromGS>();
    auto const& to_grid   = to_dispatch.grid().as<ToGS>();
    auto const  gpos      = gridPos();
    myDispatch = to_dispatch.shared();
    arr3d<real> rres {radial_rebind(data(), gpos.radius, from_grid, to_grid)};
    arr3d<real> tres {azimutal_rebind(rres, gpos.sector, from_grid, to_grid)};
    if (dispatch().grid().dim() == 3) {
      myData = altitude_rebind(tres, gpos.layer, from_grid, to_grid);
    } else {
      myData = std::move(tres);
    }
  }

  void
  ScalarField::rebindTo(GridDispatch const& to_dispatch)  {
    if (dispatch().comm().size() != 1)  { throw std::logic_error("Can only rebind on one process."); }
    if (to_dispatch.comm().size() != 1) { throw std::logic_error("Can only rebind on one process."); }
    if (max(diff(dispatch().grid().shape(), to_dispatch.grid().shape())) > std::numeric_limits<real>::epsilon()) {
      throw std::logic_error("Can only rebind on same shape.");
    }
    if (to_dispatch.grid().dim() != dispatch().grid().dim()) {
      throw std::logic_error("Can only rebind on same dim.");
    }
    GridSpacing from = dispatch().grid().radialSpacing();
    GridSpacing to   = to_dispatch.grid().radialSpacing();
    GridSpacing constexpr AR = GridSpacing::ARITHMETIC;
    GridSpacing constexpr LN = GridSpacing::LOGARITHMIC;
    if (from == AR) {
      if (to == AR) {rebindImpl<AR,AR>(to_dispatch);}
      else          {rebindImpl<AR,LN>(to_dispatch);}
    } else {
      if (to == AR) {rebindImpl<LN,AR>(to_dispatch);}
      else          {rebindImpl<LN,LN>(to_dispatch);}
    }
  }
}
