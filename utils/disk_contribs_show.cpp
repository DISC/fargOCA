// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <optional>
#include <fstream>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/algorithm/string.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "planetarySystem.hpp"
#include "hillForce.hpp"
#include "disk.hpp"
#include "h5io.hpp"
#include "optIO.hpp"
#include "h5Labels.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extracts gas contributions onto planets from a log file.\n"
        << '\t' << cmd << " <file>.h5 [option]+ \nValid options are.\n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("ofile-fmt,o", po::value<std::string>()->default_value("planet%1%.txt"), "format of the ouput files names (ex: planet%1%.txt). Planet name will be used to distinguish them.")
    ("no-header", "Do not print the header in the output file.")
    ("complete,c",      "With full disk contributions.\n.")
    ("hill-excluded,h", "Without Hill sphere contributions.\n.")
    ("split", "Split inner and outer contribution.\n.")
    ("diff", "Print complete-Hill excluded.\n.")
    ("dot-velocity", po::value<real>(), "Print the dot product between the specified forces and the position, scaled by arg.")
    ("position", po::value<real>(), "Print the angle between the the X axis and the planet scaled by arg.")
    ("distance", po::value<real>(), "Print the distante of planet to origin scaled by arg.")
    ("eccentricity", po::value<real>(), "Print the distante of planet to origin scaled by arg.")
    ("precision,p", po::value<int>()->default_value(8),"The printed precision.");

  po::options_description hidden(usage.str());
  hidden.add_options()
    ("file", po::value<std::string>(), "HDF5 file containing the disk file or planetary system log.");
  
  try {
    po::positional_options_description pos;
    pos.add("file", 1);

    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_USAGE);
  } 
  for(std::string opt : {"file"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '" << opt << "'. \nusage:\n"
                << visible << '\n';
      std::exit(EX_USAGE);
    }
  }
  if (vm.count("complete") + vm.count("hill-excluded") == 0) {
    std::cerr << "specify at least one of --complete or --hill-excluded. \nusage:\n"
              << visible << '\n';
    std::exit(EX_USAGE);
  }
}

void
print_forces(Triplet const& complete, Triplet const& noHillSphere, bool diff, std::ostream& out) {
  out << complete.norm() << (diff ? " - " : " \\ ") << noHillSphere.norm();
  if (diff) {
    out << " -> " << (complete - noHillSphere).norm() ;
  }
}

void
print_force(Triplet const& inner, Triplet const& outer, bool split, std::ostream& out) {
  if (split) {
    out << inner.norm() << " | " << outer.norm();
  } else {
    out << (inner + outer).norm();
  }
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  assert(env.world().size() == 1);
  
  std::string ifname = vm["file"].as<std::string>();
  boost::format ofmt(vm["ofile-fmt"].as<std::string>());
  
  HF::File ifile(ifname, HF::File::ReadOnly);
  std::map<std::string, std::vector<TimeStampedHillForce>> logs
    = TimeStampedHillForce::readH5Logs(ifile.getGroup("/"));
  using h5::labels::CODE_UNITS;
  CodeUnits units = ifile.exist(CODE_UNITS) ? CodeUnits(ifile.getGroup(CODE_UNITS)) : CodeUnits();
  
  bool noHeader     = bool(vm.count("no-header"));
  bool complete     = bool(vm.count("complete"));
  bool hillExcluded = bool(vm.count("hill-excluded"));
  bool split        = bool(vm.count("split"));
  bool diff         = bool(vm.count("diff"));
  
  real position     = bool(vm.count("position")) ? vm["position"].as<real>() : 0;
  real distance     = bool(vm.count("distance")) ? vm["distance"].as<real>() : 0;
  real eccentricity = bool(vm.count("eccentricity")) ? vm["eccentricity"].as<real>() : 0;
  real dotVelocity  = bool(vm.count("dot-velocity")) ? vm["dot-velocity"].as<real>() : 0;

  for(auto const& [planet, log] : logs) {
    std::string ofname = (ofmt%planet).str();
    std::ofstream out(ofname);
    for (auto tsHF : log) {
      out << tsHF.time << '\t';
      if (complete && hillExcluded) {
        if (split) {
          print_forces(tsHF.force.inner.complete, tsHF.force.inner.noHillSphere, diff, out);
          out << "\t\t";
          print_forces(tsHF.force.outer.complete, tsHF.force.outer.noHillSphere, diff, out);
        } else {
          auto complete     = tsHF.force.inner.complete     + tsHF.force.outer.complete;
          auto noHillSphere = tsHF.force.inner.noHillSphere + tsHF.force.outer.noHillSphere;
          print_forces(complete, noHillSphere, diff, out);
        }
      } else {
        if (complete) {
          print_force(tsHF.force.inner.complete, tsHF.force.outer.complete, split, out);
        } 
        if (hillExcluded) {
          print_force(tsHF.force.inner.noHillSphere, tsHF.force.outer.noHillSphere, split, out);
        } 
      }
      if (position > 0) {
        out << "\t" << std::arg(std::complex(tsHF.planet.position().y, tsHF.planet.position().x)) * position;
      }
      if (dotVelocity > 0) {
        if (complete) {
          Triplet complete     = tsHF.force.inner.complete     + tsHF.force.outer.complete;
          out << '\t' << dot(tsHF.planet.velocity(), complete) * dotVelocity;
        }
        if (hillExcluded) {
          Triplet noHillSphere     = tsHF.force.inner.noHillSphere     + tsHF.force.outer.noHillSphere;
          out << '\t' << dot(tsHF.planet.velocity(), noHillSphere) * dotVelocity;
        }
      }
      if (distance > 0) {
        out << "\t" << tsHF.planet.position().norm() * distance;
      }
      if (eccentricity > 0) {
        out << "\t" << fargOCA::eccentricity(units, tsHF.planet) * eccentricity;
      }
      out << '\n';
    }
  }
  return 0;
}
