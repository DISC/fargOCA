// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "allmpi.hpp"
#include "environment.hpp"
#include "scalarField.hpp"
#include "log.hpp"
#include "loops.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  
  real const tol = argc > 1 ? std::stol(argv[1]) : 1e-10;
  GridSizes sizes = {256, 16, 32};
  std::shared_ptr<Grid const> coords = Grid::make(GasShape(1, 10, 50, 0.001, false), sizes, GridSpacing::ARITHMETIC);
  auto grida = GridDispatch::make(world, *coords, 4);
  auto gridb = GridDispatch::make(world, *grida, 5);
  
  ScalarField bob("bob", *grida);
  auto values = bob.merged(0);
  
  real  sum = 0;
  if (world.rank() == 0) {
    auto v = *values;
    Kokkos::parallel_reduce("set_data", fullRange(sizes),
                            KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j, real& tmp) {
                              size_t idx = sizes.ni*sizes.ns*i + sizes.ns*h + j;
                              v(i,h,j) = real(idx);
                              tmp += real(idx);
                            }, sum);
  }
  bob.dispatch(values, 0);
  ScalarField alice(bob, *gridb);
  bool passed = false;
  {
    auto avalues = alice.merged(0);
    if (world.rank() == 0) {
      passed = checkAbsRelDiff(maxAbsRelDiff(*avalues, *values), tol, 1e-7, std::cout);
      if (passed) { std::cout << "PASSED\n"; }
      else        { std::cout << "FAILED\n"; }
    }
    fmpi::broadcast(world, passed, 0);
  }
  return passed ? 0 : -1;
}
