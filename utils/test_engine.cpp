// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>

#include <boost/program_options.hpp>

#include <highfive/H5File.hpp>

#include "environment.hpp"
#include "planetarySystem.hpp"
#include "planetarySystemEngine.hpp"
#include "precision.hpp"
#include "allmpi.hpp"
#include "h5Labels.hpp"
#include "simulation.hpp"
#include "disk.hpp"
#include "codeUnits.hpp"

namespace po  = boost::program_options;
namespace fs  = std::filesystem;
namespace HF  = HighFive;
namespace h5l = fargOCA::h5::labels;

using namespace std;
using namespace fargOCA;
typedef fargOCA::real real;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  ostringstream usage;
  string cmd = fs::path(argv[0]).filename().native();
  usage << "Test a planetary engine while ignoring the disk.\n"
        << '\t' << cmd << " <file>.h5 <engine> <dt> [option]+ \n"
        << "Available engines are: ";
  for(auto s : PlanetarySystemEngine::available()) { usage << " " << s; }
  usage << "\nValid options are.\n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("ofile,o", po::value<string>()->default_value("planets.h5"), "planet log file.")
    ("ignore-physic", "(not supported yet) Do not load physic from disk.")
    ("engine,e", po::value<string>(), "The engine name. Mandatory if physic ignored.")
    ("time-step,t", po::value<fargOCA::real>(), "The time step. Mandatory if physic ignored.")
    ("nb-iterations,n", po::value<int>()->default_value(1), "Number of iteration.")
    ;

  po::options_description hidden(usage.str());
  hidden.add_options()
    ("file", po::value<string>(), "Disk file containing the planetary system.");
  
  try {
    po::positional_options_description pos;
    pos.add("file", 1);
    
    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    cout << visible;
    exit(EX_USAGE);
  }
  vector<string> mandatory = {"file"};
  if (bool(vm.count("ignore-physic"))) {
    mandatory.push_back("engine");
    mandatory.push_back("time-step");
  }
  for(string opt : {"file"}) {
    if (vm.count(opt)==0) {
      cerr << "missing mandatory option '" << opt << "'. \nusage:\n"
                << opt << '\n';
      exit(EX_USAGE);
    }
  }
}

HF::Group
mandatory_group(HF::Group root, string name) {
  if (!root.exist(name))  {
    cerr << "Missing mandatory group '" << name << "'\n.";
    exit(EX_DATAERR);
  }
  return root.getGroup(name);
}

int
main(int argc, char *argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  assert(env.world().size() == 1);
  
  string ifname = vm["file"].as<string>();
  auto simulation{Simulation::make(env.world(), h5::getRoot(ifname), ".")};
  auto physic = simulation->disk().physic();
  auto system = simulation->disk().system();
  fargOCA::real dt = vm.count("time-step") ? vm["time-step"].as<fargOCA::real>() : simulation->userTimeStep();
  int  n  = vm.count("nb-iterations") ? vm["nb-iterations"].as<int>() : 1;
  map<string, vector<TimeStamped<Planet>>> log;
  fargOCA::real time = 0;
  string name = (vm.count("engine") 
                 ? vm["engine"].as<string>() 
                 : physic.planetarySystem()->engine.name);
  auto engine = PlanetarySystemEngine::make(name, *system, Config());
  std::cout << "Running " << n << " steps of dt " << dt << " with engine " << engine->name() << "\n";
  system->log(time, log);
  while (n-- > 0) {
    engine->advance(dt);
    time += dt;
    system->log(time, log);
  }
  string ofname = vm["ofile"].as<string>();
  HF::File ofile(ofname, HF::File::Overwrite);
  PlanetarySystem::writeH5Logs(ofile.getGroup("/"), log);
}
