// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_FIELD_STACK_H
#define FARGOCA_FIELD_STACK_H

#include <vector>
#include <string>

#include "arrays.hpp"
#include "grid.hpp"

namespace fargOCA {
  namespace detail {
    template<typename T, GridDirection... Ds> struct FieldViewUtils;
    
    template<typename T> 
    struct FieldViewUtils<T,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA> {
      typedef arr3d<T>       field_type;
      typedef arr3d<T const> const_field_type;
      
      static field_type make(GridSizes const& coords, std::string name);
    };
    
  }

  template<typename T, GridDirection... Ds> class FieldStack;

  template<>
  class FieldStack<void> {
  public:
    static void setUninitialized2NaN(bool v) { ourSetUninitialized2NaN = v; }
    static bool setUninitialized2NaN()       { return ourSetUninitialized2NaN; }

  protected:
    FieldStack() = default;

  private:
    static bool ourSetUninitialized2NaN;
  };
  
  /// \brief A simple stack of scalar views that can be used to
  /// obtain reusable locals.
  /// \param T the value_type of the fields, usually 'real'.
  /// \param Ds the dimensions of the space handled by those field. 
  /// sequence GridDirection::RADIAL,GridDirection::PHI indicate, for example, a 2D field 
  /// in the radial and azimutal directions.
  template<typename T, GridDirection... Ds>
  class FieldStack 
    : public FieldStack<void> {
  public:
    typedef detail::FieldViewUtils<T,Ds...> utils;

    typedef typename utils::field_type       field_type;
    typedef typename utils::const_field_type const_field_type;
    
    using FieldStack<void>::setUninitialized2NaN;

    FieldStack(GridSizes const& coords, std::string name = "temps", int hint = 0);
    FieldStack(FieldStack const&) = delete;
    ~FieldStack();
    
    std::string label() const { return myName; }
    void push() {
      if (myIdx == myStack.size()) {
        increase();
      }
      ++myIdx;
    }
    void push(int n) { while (n-- > 0) {push();}}
      
    void pop();
    void pop(int n) { while (n-- > 0) {pop();}}
    
    field_type at(int i) const { assert(i<myIdx); return myStack.at(i); }
    
    size_t base() const { return myIdx; }
    size_t watermark() const { return myStack.size(); }

  private:
    void increase(int n = 1);
    
  private:
    GridSizes               myExtents;
    std::string             myName;
    std::vector<field_type> myStack;
    size_t                  myIdx;
  };

  template<typename T> using FieldStack3D = FieldStack<T,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>;
  template<typename T> using FieldStack2D = FieldStack<T,GridDirection::RADIAL,GridDirection::PHI>;
  template<typename T> using FieldStack1D = FieldStack<T,GridDirection::RADIAL>;

  /// \brief A simple stack of scalar views that can be used to
  /// obtain reusable locals.
  /// \param T the value_type of the fields, usually 'real'.
  /// \param SIZE the number of locals
  /// \param Ds the dimensions of the space handled by those field. 
  /// sequence GridDirection::RADIAL,GridDirection::PHI indicate, for example, a 2D field 
  /// in the radial and azimutal directions.
  template<typename T, int SIZE, GridDirection... Ds>
  class LocalFieldStack {
  public:
    typedef typename FieldStack<T,Ds...>::field_type       field_type;
    typedef typename FieldStack<T,Ds...>::const_field_type const_field_type;
    
    LocalFieldStack(FieldStack<T,Ds...>& stack)
      : myStack(stack), myBase(stack.base()) { myStack.push(SIZE); }
    LocalFieldStack(LocalFieldStack<T,SIZE,Ds...> const&) = delete;
    ~LocalFieldStack() { myStack.pop(SIZE); }

    template<int IDX>
    field_type             get()    {
      static_assert(0 <= IDX && IDX<SIZE);
      return myStack.at(myBase+IDX);
    }

    template<int IDX>
    const_field_type const get() const   {
      return const_cast<LocalFieldStack*>(this)->get<IDX>();
    }

    template<int IDX>
    field_type init(T const& v)    {
      field_type f = get<IDX>();
      Kokkos::deep_copy(f, v);
      return f;
    }

    template<int IDX>
    field_type init(const_field_type v)    {
      field_type f = get<IDX>();
      Kokkos::deep_copy(f, v);
      return f;
    }

    template<int IDX>
    field_type init()    {
      field_type f = get<IDX>();
      typedef std::numeric_limits<T> limits;
      if (myStack.setUninitialized2NaN()) {
        if (limits::is_integer) {
          Kokkos::deep_copy(f, limits::max());
        } else {
          if (limits::has_quiet_NaN) {
            Kokkos::deep_copy(f, limits::quiet_NaN());
          } else {
            Kokkos::deep_copy(f, limits::max());
          }
        }
      }
      return f;
    }
    
  private:
    FieldStack<T,Ds...>& myStack;
    int                  myBase;
  };

  template<typename T, int SIZE> using LocalFieldStack3D = LocalFieldStack<T,SIZE,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>;
  template<typename T, int SIZE> using LocalFieldStack2D = LocalFieldStack<T,SIZE,GridDirection::RADIAL,GridDirection::PHI>;
  template<typename T, int SIZE> using LocalFieldStack1D = LocalFieldStack<T,SIZE,GridDirection::RADIAL>;

  template<typename T, int SIZE, GridDirection... Ds>
  LocalFieldStack<T,SIZE,Ds...> locals(FieldStack<T,Ds...>& stack) { return LocalFieldStack<T,SIZE,Ds...>(stack); } 
}

#endif
