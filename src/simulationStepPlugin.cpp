// Copyright 2025, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2025, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2025, Maya Tatarelli  maya.tatarelli<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <string>

#include <Kokkos_Core.hpp>

#include "simulationStepPlugin.hpp"
#include "pluginImpl.hpp"
#include "pluginUtils.hpp"

namespace fargOCA {
  namespace kk = Kokkos;
#define LBD KOKKOS_LAMBDA
  
  namespace {
    SimulationStepPlugin KokkosProfiling{
      "Kokkos_profiling",
      [](std::string, Simulation& simulation, SimulationStepEvent step) {
        namespace prof = Kokkos::Profiling;
        using Evt = SimulationStepEvent;
        switch(step) {
        case Evt::BEFORE_STEP: break;
        case Evt::BEFORE_ACCRETION_STEP: prof::pushRegion("accretion"); break;
        case Evt::AFTER_ACCRETION_STEP: prof::popRegion(); break;
        case Evt::BEFORE_VELOCITY_STEP: prof::pushRegion("velocity"); break;
        case Evt::AFTER_VELOCITY_STEP: prof::popRegion(); break;
        case Evt::BEFORE_VISCOSITY_STEP: prof::pushRegion("viscosity"); break;
        case Evt::AFTER_VISCOSITY_STEP: prof::popRegion(); break;
        case Evt::BEFORE_ADIABATIC_STEP: prof::pushRegion("adiabatic"); break;
        case Evt::AFTER_ADIABATIC_STEP: prof::popRegion(); break;
        case Evt::BEFORE_ENERGY_STEP: prof::pushRegion("energy"); break;
        case Evt::AFTER_ENERGY_STEP: prof::popRegion(); break;
        case Evt::BEFORE_TRANSPORT_STEP: prof::pushRegion("transport"); break;
        case Evt::AFTER_TRANSPORT_STEP: prof::popRegion();break;
        case Evt::AFTER_STEP: break;
        }
      },
      "Add a Kokkos profiling section arround the main steps. Kokkos profiling must be enabled.",
      {}
    };
  }
  
  template class Plugin<SimulationStepEvent, std::string, Simulation&, SimulationStepEvent>;
}
