#!/usr/bin/env bash
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

nmpi=$1
engine=$2
post_dir=@CMAKE_BINARY_DIR@/utils
check_dir=@CMAKE_SOURCE_DIR@/tools/check
outdir=@CMAKE_CURRENT_BINARY_DIR@/out$nmpi$engine
golddir=@CMAKE_CURRENT_SOURCE_DIR@/out$engine
export PYTHONPATH=$PYTHONPATH:@CMAKE_SOURCE_DIR@/tools/check

failed=0
rm -f $outdir/planet?.dat
for i in 0 1 2 3
do
    echo Checking Step $i
    $post_dir/extract_mdot_map $outdir/disk$i.h5  --path user/mdotmap || let failed++
    $post_dir/disk_diff $outdir/disk$i.h5  $golddir/disk$i.h5 --all \
	--atol 1e-12 --rtol 1e-8 \
	tracers/radius:5e-11:-1 \
	tracers/Hill:5e-11:-1 \
	user/mdotmap:6e-15:5e-5 \
	velocity/radial:1e-10:5e-05 \
	velocity/theta:1e-12:2e-8 \
	viscosity:1e-12:1e-08 \
	|| let failed++
    $post_dir/extract_torque $outdir/disk$i.h5 --output $outdir/gastorq$i.dat  || let failed++
    $post_dir/column_diff $golddir/gastorq$i.dat $outdir/gastorq$i.dat --verbose \
	2e-12:1e-12 1e-12:3e-6:1e-16 1e-12:3e-6:2e-16 \
	|| let failed++
    $post_dir/disk_radial_flow $outdir/disk$i.h5 --delta 0.0302076 --output $outdir/gasflow$i.dat  || let failed++
    @CMAKE_BINARY_DIR@/utils/column_diff --verbose \
	$golddir/gasflow$i.dat $outdir/gasflow$i.dat \
	1e-14:1e-14 1e-12:5e-7 \
	|| let failed++
done

@CMAKE_BINARY_DIR@/utils/planets_history_diff $outdir/planets.h5  $golddir/planets.h5 \
    --position-tolerance 1e-12:7e-1 \
    --velocity-tolerance 1e-12:3e-12 \
    --verbose --ofile $outdir/planetsdiff.h5 \
    || let failed++

ln -sf $goldir $outdir/gold

@CMAKE_BINARY_DIR@/utils/planets_tqwk_diff $golddir/tqwk.h5 $outdir/tqwk.h5 --verbose --tolerance 2e-15:1e-17 -o $outdir/tqdiff.h5 || let failed++

@CMAKE_BINARY_DIR@/utils/column_diff --verbose \
    $golddir/monitor.dat $outdir/monitor.dat \
    1e-12:1e-11 1e-12:1e-11 1e-12:1e-11 1e-12:1e-11 1e-12:1e-11 1e-12:1e-11 \
    || let failed++

if [[ $failed -gt 0 ]]
then 
    echo FAILED
    exit 1
else
    echo PASSED
    exit 0
fi
