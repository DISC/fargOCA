module purge

module load gcc/8.3.0
module load intelpython3/2019.3
module load intel/19.4
export FC=ifort
module load idb/19.4
#module load hdf5/1.10.5-intel-19.0.4-serial
module load intelmpi/2019.4.243
module load mkl/19.4 
module load cmake/3.16.1
module load git
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export BOOST_ROOT=/panfs/panasas/cnt0026/oca7233/SHARED/boost-1.73.0
export LD_LIBRARY_PATH=$BOOST_ROOT/lib:$LD_LIBRARY_PATH
export HDF5_ROOT=/panfs/panasas/cnt0026/oca7233/SHARED/hdf5-1.12.0-1-intel-2019.4
export LD_LIBRARY_PATH=$HDF5_ROOT/lib:$LD_LIBRARY_PATH
export HDF5_USE_FILE_LOCKING=FALSE
