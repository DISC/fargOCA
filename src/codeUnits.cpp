// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <sysexits.h>

#include <nlohmann/json.hpp>

#include "codeUnits.hpp"
#include "io.hpp"
#include "h5io.hpp"
#include "h5Labels.hpp"
#include "configProperties.hpp"
#include "simulationConfig.hpp"

namespace fargOCA {

  void
  CodeUnits::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Code Units:\n"
        << tabs(tab+1) << "Central Body Mass:\t" << myCentralBodyMass << '\n'
        << tabs(tab+1) << "Central Body Radius:\t" << myCentralBodyRadius << '\n'
        << tabs(tab+1) << "Central Body surface temperature:\t" << myCentralBodyTemperature << '\n'
        << tabs(tab+1) << "Semi Major Axis of reference:\t" << mySemiMajorAxis << '\n';
    out << "Radiation Constant: " << radiationCst() << '\n'
        << "Stefan Boltzmann constant: " << StefanBoltzmannCst() << '\n';
  }
  
  void
  CodeUnits::writeH5(HighFive::Group grp) const {
    using namespace h5::labels;
    h5::writeAttribute(grp, SEMI_MAJOR_AXIS, mySemiMajorAxis);
    h5::writeAttribute(grp, CENTRAL_BODY_MASS, myCentralBodyMass);
    h5::writeAttribute(grp, CENTRAL_BODY_RADIUS, myCentralBodyRadius);
    h5::writeAttribute(grp, CENTRAL_BODY_TEMPERATURE, myCentralBodyTemperature);
  }
  
  CodeUnits::CodeUnits(real sma, real cbMass, real cbRadius, real cbTemp)
    : mySemiMajorAxis(sma),
      myCentralBodyMass(cbMass),
      myCentralBodyRadius(cbRadius),
      myCentralBodyTemperature(cbTemp) {
    if (sma <= 0) {
      std::cerr << "Semi Major Axis of reference must be > 0.\n";
      std::exit(EX_CONFIG);
    }
    if (cbMass <= 0) {
      std::cerr << "Central Body Mass must be > 0.\n";
      std::exit(EX_CONFIG);
    }
    if (cbRadius <= 0) {
      std::cerr << "Central Body Radius must be > 0.\n";
      std::exit(EX_CONFIG);
    }
    if (cbTemp <= 0) {
      std::cerr << "Central Body Surface Temperature must be > 0.\n";
      std::exit(EX_CONFIG);
    }
  }

  using namespace configProperties;
  void
  to_json(json& j, CodeUnits const& cu) {
    j = {{snake(SEMI_MAJOR_AXIS), cu.semiMajorAxis()},
	 {snake(CENTRAL_BODY), {{snake(MASS), cu.centralBodyMass()},
			      {snake(RADIUS), cu.centralBodyRadius()},
			      {snake(TEMPERATURE), cu.centralBodyTemperature()}}}};
  }

  void
  from_json(json const& j, CodeUnits& cu) {
    cu = CodeUnits(j);
  }
  
  CodeUnits::CodeUnits(json const& j)
    : CodeUnits(j.at(snake(SEMI_MAJOR_AXIS)).get<real>(),
		j.at(snake(CENTRAL_BODY)).at(snake(MASS)).get<real>(),
		j.at(snake(CENTRAL_BODY)).at(snake(RADIUS)).get<real>(),
		j.at(snake(CENTRAL_BODY)).at(snake(TEMPERATURE)).get<real>()) {
  }

  namespace lb = h5::labels;
  
  CodeUnits::CodeUnits(HighFive::Group grp)
    : CodeUnits(*h5::attributeValue<real>(grp, lb::SEMI_MAJOR_AXIS),
		*h5::attributeValue<real>(grp, lb::CENTRAL_BODY_MASS),
		*h5::attributeValue<real>(grp, lb::CENTRAL_BODY_RADIUS),
		*h5::attributeValue<real>(grp, lb::CENTRAL_BODY_TEMPERATURE)) {}
  
}
