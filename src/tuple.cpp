// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <iostream>
#include <boost/throw_exception.hpp>

#include "tuple.hpp"

namespace fargOCA {
  std::ostream&
  Triplet::print(std::ostream& out, std::string sep) const {
    out << x << sep << y << sep << z;
    return out;
  }

  Triplet
  adiff(Triplet const& p1, Triplet const& p2) {
    return abs(p1-p2);
  }

  Triplet
  rdiff(Triplet const& p1, Triplet const& p2) {
    using std::abs;
    auto diff = [](real r1, real r2) {
                  if (r1 == r2) {
                    return real(0);
                  } else {
                    return abs(r1-r2) / (abs(r1)+abs(r2)) / 2;
                  }
                };
    return Triplet(diff(p1.x, p2.x), diff(p1.y, p2.y), diff(p1.z, p2.z));
  }

  Triplet
  max(Triplet const& p1, Triplet const& p2) {
    return Triplet(std::max(p1.x, p2.x), std::max(p1.y, p2.y), std::max(p1.z, p2.z));
  }

  HighFive::DataType
  Triplet::buildHFType() {
    using namespace HighFive;
    static CompoundType tp({
        { "x", create_datatype<real>() },
          { "y", create_datatype<real>() },
            { "z", create_datatype<real>() }
      });
    return tp;
  }  
}
