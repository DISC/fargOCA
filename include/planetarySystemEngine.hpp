// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_PLANETARY_SYSTEM_ENGINE_H
#define FARGOCA_PLANETARY_SYSTEM_ENGINE_H

#include <map>
#include <vector>
#include <string>
#include <optional>
#include <functional>

#include "precision.hpp"
#include "config.hpp"
#include "shared.hpp"

namespace fargOCA {

  class PlanetarySystem;

  /// \brief Default engine
  extern std::string const RUNGE_KUTTA;

  class PlanetarySystemEngine {
  public:
    virtual ~PlanetarySystemEngine();
    
    /// \brief Advance the system by dt
    virtual void advance(real dt) = 0;
    
    std::string name() const { return myName; }

    static std::vector<std::string> available();

    template<class Engine> struct Declared;
    static std::unique_ptr<PlanetarySystemEngine>  make(std::string name, PlanetarySystem& s, Config const& cfg);

  private:
    template<class Engine> friend class Declared;
    template<class Engine> static bool declareEngine(std::string);
    static bool undeclareEngine(std::string);
    
  protected:
    PlanetarySystemEngine(std::string name, PlanetarySystem& s, Config const& cfg);
    PlanetarySystem&  system() { return mySystem; }

    Config const& cfg() const { return myConfig; }

  protected:
    typedef std::function<std::unique_ptr<PlanetarySystemEngine> (std::string, PlanetarySystem&, Config const&)> EngineMaker;  
    typedef std::map<std::string, EngineMaker> MakerMap;
    static MakerMap& makers();

  private:
    PlanetarySystem&  mySystem;
    Config            myConfig;
    std::string       myName;
  };
  
  template<class Engine>
  struct PlanetarySystemEngine::Declared {
    Declared(std::string name)
      : myMaker(makers().insert(MakerMap::value_type(name, 
                                                     [](std::string nm, PlanetarySystem& s, Config const& c) {
                                                       return std::make_unique<Engine>(nm, s, c);
                                                     })).first) {}
    ~Declared() = default;
  private:
    PlanetarySystemEngine::MakerMap::iterator myMaker;
  };
}

#endif

