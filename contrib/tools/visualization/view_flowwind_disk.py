#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import sys
import h5py as h5
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse
import math

parser = argparse.ArgumentParser(description='''
Display the radial flow profile for 3D simulations, 
display the  averaged flow_map in r-z for 3D simulations 
it is required to first run extract_mdot_map with the argument:
--in-place mdotmap   

Example:
   {} ref/gas9.h5
'''.format(sys.argv[0]))
parser.add_argument('ifile', metavar='GAS.h5', type=str, nargs=1,
                    help='HDF5 gas description file.')
parser.add_argument('plname', nargs=1, type=str, #dest='plname',
                    help='Name of planet to follow')
parser.add_argument('paths', metavar='PATH', type=str, nargs='*',
                    help='Use Defaults: all.')
parser.add_argument('-yM','--yMAX', dest='sup', type=float,default=1.e-7, 
                    help='Sets the y axis upper limiti default 1.e-7 (Solar mass per year) ')
parser.add_argument('-ym','--yMIN', dest='inf', type=float,default=-1.e-7,
                    help='Sets the y axis lower limit (default -1.e-7 Solar mass per year)')
parser.add_argument('-R0', type=float, default=5.2, dest='R0',
                    help='Provide the unit of distance default 5.2(AU)')


args = parser.parse_args()
disk_filename = args.ifile[0]
disk = h5.File(disk_filename, 'r')
ymax =args.sup

ymin =args.inf
print(type(ymin))
plname = args.plname[0]

# FargOCA units
AU     = 1.496e13  # Astronomical Unit in [cm] 
GRAVC  = 6.67e-8    # Gravitation Constant in [cm3/g/s2]
XMSOL  = 1.989e33  # Solar Mass in [gr]
XMSTAR = 1.0       # Amount of Solar Masses
R0     = args.R0*AU    # Unit of distance in cm
R0scale = args.R0  # Unit of distance in AU
YEAR   = (365.2524*24.*3600.) # one year in s
VOL0   = (R0*R0*R0) # Volume unit in cm**3
TIME0  = (math.sqrt(VOL0 / GRAVC / (XMSTAR*XMSOL)))  # time unit in s
V0     = (R0 / TIME0)  # Velocity in cm/s
XM0    = (XMSOL * XMSTAR)  # Mass unit in g
RHO0   = (XM0 / VOL0)  # Density unit in g/cm**3
YEAR   = (365.2524*24.*3600.) # one year in s
mdotSMyear =2.*math.pi*R0*R0*RHO0*V0*YEAR/XMSOL
print('Mdot in Solar mass per year:',mdotSMyear)

def extract_field_paths(obj):
    """Extract all object paths storing a field from an hdf5 object.

    An object path is considered to store a field if it contains a
    'polar_grid' object which provide a 3D floating point array.
    2D object have a 2cnd dimention of size one.
    """
    
    fields = []
    def collect_fields(path): 
        if 'polar_grid' in obj[path]: fields.append(path)
    obj.visit(collect_fields)
    return fields

# Collect paths
paths = []

if len(args.paths) > 0:
    paths = args.paths
else:
    paths = extract_field_paths(disk)

if len(paths)==0:
    print("No field found in {}.".format(gas_filename))





# Display grid:
nb_col = 3
nb_line = (len(paths)+nb_col)/nb_col

def get_fields(disk, paths, data_space_name):
    fields = {}
    for p in paths:
        fields[p] = np.array(disk[p][data_space_name])
    return fields

fields = get_fields(disk, paths, 'polar_grid')


# Assume all shapes are equal
shape = disk[paths[0]]['polar_grid'].shape
flat   = shape[1] == 1
xymesh = None

if flat:
     print("this is a flat grid, displaying as it is.")
     radii   = np.array(disk['grid']['radii'])
     sectors = np.array(disk['grid']['sectors'])
     xymesh = np.meshgrid(sectors, radii)
     # drop layer dim
     for p,f in fields.items():
         fields[p] = f[:,0,:]
else:
     print("this is a fat grid displaying midplane")
     half = disk['physic/shape/gas'].attrs['half']
     radii   = np.array(disk['grid']['radii'])
     sectors  = np.array(disk['grid']['sectors'])
     layers    = np.array(disk['grid/layers'])
     xymesh  = np.meshgrid(sectors, radii)



# find midplane
     for p,f in fields.items():
         nr,nphi,nt=fields[p].shape
         midplane = int(nphi/2)
         if(half):  # half disk
             fields[p] = f[:,nphi-1,:]
         else: # full disk
             fields[p] = f[:,midplane,:]

# Select suited resonances to be added to the plot
def plot_resonances(ymin,ymax,RMIN,RMAX):
    dampin=RMIN * pow(1.5,2./3.);
    dampout=RMAX*pow(1.5,-2./3.);
    alp  = 0.5
    planet=disk['/planetary_system/planets/' + plname]['state']
    x,y,z=planet['position']
    erre = R0scale*(x**2+y**2)**0.5   #Valid only for planet at z=0
    posr12= erre*0.25**(1./3.)
    posr21= erre*4.**(1./3.)
    posr31=erre*9.**(1./3.)
    posr32=erre*(9./4.)**(1./3.)

    r12  = fig.plot([posr12,posr12],[ymin,ymax],'k--',alpha=alp)
    r21  = fig.plot([posr21,posr21],[ymin,ymax],'k--',alpha=alp)
    r32  = fig.plot([posr32,posr32],[ymin,ymax],'k--',alpha=alp)
    r31  = fig.plot([posr31,posr31],[ymin,ymax],'k--',alpha=alp)
    delta=ymax-ymin
    t12  = plt.text(posr12,ymax-0.04*delta,r'$1:2$',size=11,color='k',alpha=alp)
    t21  = plt.text(posr21,ymax-0.04*delta,r'$2:1$',size=11,color='k',alpha=alp)
    t32  = plt.text(posr32,ymax-0.04*delta,r'$3:2$',size=11,color='k',alpha=alp)
    t31  = plt.text(posr31,ymax-0.04*delta,r'$3:1$',size=11,color='k',alpha=alp)
    p0   = fig.plot([erre,erre],[0,0],'ko')
    binn = fig.plot([RMIN,RMAX],[0,0],'k-')
    bout = fig.plot([RMIN,RMAX],[-1.e-8,-1.e-8],'k-',alpha=alp)
    dinn = fig.plot([dampin,dampin],[ymin,ymax],'k--')
    dout = fig.plot([dampout,dampout],[ymin,ymax],'k--')
    return r12,r21,r32,r31,t12,t21,t32,t31,p0,binn,bout,dinn,dout

# Extract radial  mdotmap according to vertically integrated density
def simple_mean_flow(disk,activerho,disk_filename):
    radius  = np.array(disk['grid']['radii'])
    sectors  = np.array(disk['grid']['sectors'])
    phi    = np.array(disk['grid/layers'])
    rho=np.array(disk['density/polar_grid'])
    mdotmap=np.array(disk['mdotmap/polar_grid'])
    nr,nphi,nt=disk['density/polar_grid'].shape
    print(nr,nphi,nt)
    # We compute the vertically integrated surface density  and the flow in the active region and in the dead region
    surfdens = np.zeros(shape=(nr,nphi,nt))
    meansurfdens=np.zeros(shape=(nr,nphi))
    meanmdot=np.zeros(shape=(nr,nphi))
    mdotA    = np.zeros(shape=(nr))
    mdotD    = np.zeros(shape=(nr))
    
    half = disk['physic/shape/gas'].attrs['half']
    if(half):
        midplane = nphi
    else:
        midplane  = int(nphi/2)
    
     #north emisphere
    for i in range(0,nr):
        for h in range(0,midplane):
            for j in range(0,nt):
                if(h < 1):
                    surfdens[i,h,j] = rho[i,h,j]*radius[i]*RHO0*R0*(phi[1]-phi[0])
                else:
                    surfdens[i,h,j] = surfdens[i,h-1,j]+rho[i,h,j]*radius[i]*RHO0*R0*(phi[h+1]-phi[h])
                
 # south emisphere  only if full disk
    if( not half):
        for i in range(0,nr):
            for h in range(nphi-1,midplane-1,-1):
                for j in range(0,nt):
                    if(h >nphi-2):
                        surfdens[i,h,j] = rho[i,h,j]*radius[i]*RHO0*R0*(phi[nphi-1]-phi[nphi-2])
                    else:
                        surfdens[i,h,j] =  surfdens[i,h+1,j]+ rho[i,h,j]*radius[i]*RHO0*R0*(phi[h]-phi[h-1])
                        
    meansurfdens=surfdens.mean(axis=2)
    meanmdot=mdotmap.mean(axis=2)   # We do the average instead of the sum because we negletted dtheta=2pi/ns in extract_mdotmap
    for i in range(0,nr):
        for h in range(0,nphi):
             
            if meansurfdens[i,h] < 2.*activerho:
               mdotA[i] += meanmdot[i,h]
            else:
               mdotD[i] += meanmdot[i,h]
                  
    output_file= 'Flow'+disk_filename+'.dat'                  
    file = open(output_file,"w")      
    for i in range(1,nr-1):
        file.write(str(radius[i])+ ' '+ str((mdotD[i]+mdotA[i])*mdotSMyear)+ ' '+'\n')                                  
    return radius,mdotA,mdotD
    
fig=plt.subplot(1,1,1)
if(disk['physic/star_accretion/'].attrs['type'] == 0):
  activerho = 0
elif(disk['physic/star_accretion/'].attrs['type'] == 2):
  activerho = disk['physic/star_accretion/wind'].attrs['active_density']
dpi =100
radius,mdotA,mdotD = simple_mean_flow(disk,activerho,disk_filename)
half = disk['physic/shape/gas'].attrs['half']
if(half):
  mdotA = 2*mdotA
  mdotD = 2*mdotD
fig.plot(radius[:-1]*R0scale,mdotA*mdotSMyear,'g-',alpha=0.8,label='$\dot M _A$')
fig.plot(radius[:-1]*R0scale,mdotD*mdotSMyear,'b-',alpha=0.8,label='$\dot M_D$')
fig.plot(radius[:-1]*R0scale,(mdotA+mdotD)*mdotSMyear,'r-',alpha=0.8,label='Total $\dot M$')
plt.ylabel(r'$\dot{M}$ $[M_{\odot}/yr]$',size=16)
plt.xlabel(r'$r$ [au]',size=16)
RMIN=radius[0]*R0scale
RMAX=radius[nr-1]*R0scale
    
plt.xlim(RMIN,RMAX)
plt.ylim(ymin,ymax) 
    
#Resonances
plt.title('Radial flow')
plot_resonances(ymin,ymax,RMIN,RMAX)
plt.legend()
plt.tick_params(
    axis='both',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom='on',      # ticks along the bottom edge are off
    top='off',         # ticks along the top edge are off
    left='on',
    right='off',
    labelbottom='on', # labels along the bottom edge are off
    labelsize=16)
filename='mdot'+disk_filename+'.png'
plt.savefig(filename,dpi=dpi,bbox_inches='tight')
plt.show()


