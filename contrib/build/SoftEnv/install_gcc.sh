#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo "usage $0 <builddir> <installdir> [<gcc version>]"
    exit -1
fi
builddir=$1
installdir=$2
gcc_version=13.2.0
if [[ $# -gt 2 ]]
then
    gcc_version=$3
fi

mkdir -p $builddir $installdir

pushd $builddir

mkdir -p usr

if [[ ! -f ./usr/lib/libgmp.a ]]
then
    version=6.3.0
    wget https://gmplib.org/download/gmp/gmp-$version.tar.gz  
    tar xzf gmp-$version.tar.gz
    pushd gmp-$version || exit -
    ./configure --prefix=`pwd`/../usr/ --enable-shared=no
    make -j10
    make install || exit -1
    popd
else
    echo "GMP already installed"
fi

if [[ ! -f ./usr/lib/libmpfr.a ]]
then
    version=4.2.1
    wget https://www.mpfr.org/mpfr-current/mpfr-$version.tar.gz
    tar -xzf mpfr-$version.tar.gz
    pushd ./mpfr-$version || exit -1
    ./configure --prefix=`pwd`/../usr --with-gmp=`pwd`/../usr --enable-shared=no
    make -j10
    make install || exit -1
popd
else
    echo "MPFR already installed"
fi

if [[ ! -f ./usr/lib/libmpc.a ]]
then 
    wget --no-check-certificate https://ftp.gnu.org/gnu/mpc/mpc-1.2.1.tar.gz
    tar -xzf mpc-1.2.1.tar.gz
    pushd mpc-1.2.1 || exit -1
    ./configure --prefix=`pwd`/../usr --with-gmp=`pwd`/../usr --with-mpfr=`pwd`/../usr --enable-shared=no
    make -j 10
    make install || exit -1
    popd
else
    echo "MPC already installed"
fi

if [[ ! -d gcc ]]
then 
    git clone https://gcc.gnu.org/git/gcc.git ||exit -1
fi

pushd gcc

git checkout releases/gcc-$gcc_version || exit -1
git pull 
./configure --prefix=$installdir --with-gmp=`pwd`/../usr --with-mpfr=`pwd`/../usr --with-mpc=`pwd`/../usr --disable-multilib
make -j || exit -1
make install || exit -1

echo "pop gcc"
popd 
echo "pop $buildir"
popd 

(
    echo "
if [[ ! \$PATH =~ $installdir/bin ]]
then 
    export PATH=$installdir/bin:\$PATH
    export LD_LIBRARY_PATH=$installdir/lib:$installdir/lib64:\$LD_LIBRARY_PATH
    export CC=$installdir/bin/gcc
    export CXX=$installdir/bin/g++
    export FC=$installdir/bin/gfortran
fi
"
) > $installdir/env.sh

