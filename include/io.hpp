// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_IO_H
#define FARGOCA_IO_H

#include <string>
#include <cstdio>
#include <vector>
#include <initializer_list>

#include "mpicommunicator.hpp"

#include "precision.hpp"

namespace fargOCA {
  
  /** \brief Check if file exist.
   *
   * \param path the file path.
   * \return true if exists and can be read.
   */
  bool PathExists(std::string const& path);

  /** \brief Backup the specified file.
   *
   * If the input file doe not exists, false is returned.
   * If the backup file cannot be created, the program is aborted.
   * \return Wether everything went fine.
   */
  bool FileBackup(std::string const& absPath);

  // \brief Same as C's unlink, but with strings
  void Unlink(std::string path);
  
  std::string tabs(int t);
  std::string yesno(bool b);
}
#endif

