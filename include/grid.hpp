// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_GRID_H
#define FARGOCA_GRID_H

#include <string>
#include <iostream>
#include <optional>

#include <highfive/H5File.hpp>
#include <Kokkos_Core.hpp>
#include <nlohmann/json_fwd.hpp>

#include "precision.hpp"
#include "arrays.hpp"
#include "fundamental.hpp"
#include "gasShape.hpp"
#include "coords.hpp"
#include "mathAddOns.hpp"
#include "gridSpacing.hpp"

namespace fargOCA {
  typedef std::size_t size_t;  
  
  enum class GridDirection { RADIAL, PHI, THETA };
  void to_json(nlohmann::json&, GridDirection const&);
  void from_json(nlohmann::json const&, GridDirection&);

  enum class GridPosition { INF, MED };
  
  void to_json(nlohmann::json&, GridPosition const&);
  void from_json(nlohmann::json const&, GridPosition&);

  template<> std::vector<GridPosition> const& enumValues<GridPosition>();

  std::ostream& operator<<(std::ostream& out, GridPosition  p);
  std::istream& operator>>(std::istream& out, GridPosition& p);

  struct GridPositions {
    GridPosition radius, sector, layer;
    GridPositions( GridPosition rtag = GridPosition::MED, GridPosition stag = GridPosition::MED, GridPosition itag = GridPosition::MED )
      : radius(rtag), sector(stag), layer(itag) {}
  };
  
  bool operator==(GridPositions const& p1, GridPositions const& p2);
  bool operator!=(GridPositions const& p1, GridPositions const& p2);
  
  /// \brief 2d grid resolution
  struct GridSizes {
    size_t nr;
    size_t ni;
    size_t ns;
    
    KOKKOS_INLINE_FUNCTION
    size_t footprint() const { return nr*ni*ns; }
  };
  template<typename T> KOKKOS_INLINE_FUNCTION GridSizes sizes(arr3d<T> a) { return {a.extent(0),a.extent(1),a.extent(2)}; }
  
  void to_json(nlohmann::json&, GridSizes const&);
  void from_json(nlohmann::json const&, GridSizes&);
  
  bool operator==(GridSizes const& s1, GridSizes const& s2);
  bool operator!=(GridSizes const& s1, GridSizes const& s2);
  std::ostream& operator<<(std::ostream& out, GridSizes const& gs);

  bool
  compatible(GridSizes const& sz1, GridSizes const& sz2, std::string label = {});

  template<typename T>
  bool
  compatible(GridSizes const& sz, arr3d<T const> arr) {
    return compatible(sz, GridSizes({arr.extent(0), arr.extent(1), arr.extent(2)}), arr.label());
  }

  template<typename T>
  bool
  compatible(GridSizes const& sz, vec3d<T const> vec) {
    return compatible(sz, vec.radial) && compatible(sz, vec.phi) && compatible(sz, vec.theta);
  }

  template<typename T> auto compatible(GridSizes const& sz, arr3d<T> arr) { return compatible(sz, arr3d<T const>{arr}); }
  template<typename T> auto compatible(GridSizes const& sz, vec3d<T> vec) { return compatible(sz, vec3d<T const>{vec}); }

  template<typename T, class... Fields>
  bool compatible(GridSizes const& sz, arr3d<T> arr, Fields... fields) { return compatible(sz, arr) && compatible(sz, fields...); }
  template<typename T, class... Fields>
  bool compatible(GridSizes const& sz, arr3d<T const> arr, Fields... fields) { return compatible(sz, arr) && compatible(sz, fields...); }
  template<typename T, class... Fields>
  bool compatible(GridSizes const& sz, vec3d<T> vec, Fields... fields) { return compatible(sz, vec) && compatible(sz, fields...); }
  template<typename T, class... Fields>
  bool compatible(GridSizes const& sz, vec3d<T const> vec, Fields... fields) { return compatible(sz, vec) && compatible(sz, fields...); }
  
  template<GridSpacing GS> class Coords;

  /// \brief An implicit grid description.
  class Grid
    : public std::enable_shared_from_this<Grid> {
  protected:
    Grid(GasShape const& shape,
         GridSizes const& gsz,
         GridSpacing      sp);
    Grid(Grid const& g, range_t radialRange);
    Grid(GasShape const& shape, HighFive::Group const& group);
    Grid(GasShape const& shape, nlohmann::json const&);
    
  private:
    Grid(Grid const& g) : Grid(g, range_t(0u, g.sizes().nr)) {}

  public:
    virtual ~Grid();
    virtual std::shared_ptr<Grid const> clone(range_t radialRange) const = 0;
    std::shared_ptr<Grid const> clone() const { return shared(); }
    Grid const& global() const { return myGlobal ? *myGlobal : *this; }

    static std::shared_ptr<Grid const> make(GasShape const& shape, HighFive::Group const& group);
    static std::shared_ptr<Grid const> make(GasShape const& shape, GridSizes sizes, GridSpacing);
    static std::shared_ptr<Grid const> make(GasShape const& shape, nlohmann::json const& j);
    
    std::shared_ptr<Grid const> shared() const { return shared_from_this(); }
    
    GridSizes const& sizes() const { return mySizes; }
    GasShape  const& shape() const { return myShape; }
    virtual GridSpacing radialSpacing() const = 0;

    int         dim()           const { return mySizes.ni > 2 ? 3 : 2; }
    size_t      footprint()     const { return sizes().footprint(); }
    size_t      radialOffset()  const { return myRadialOffset; }    
    bool        isGlobal()      const { return !myGlobal; }

    template<GridSpacing GS>  Coords<GS> const& as() const;
    template<GridSpacing GS>  Coords<GS> const& as(GridSpacingTp<GS>) const { return as<GS>(); }
    virtual void writeH5(HighFive::Group group) const;

  private:
    GasShape    myShape;
    GridSizes   mySizes;
    size_t      myGlobalNRadii;
    size_t      myRadialOffset;
    std::shared_ptr<Grid const> myGlobal;
  };

  void to_json(nlohmann::json& j, Grid const& g);
  
  /// \brief Common interface to all concrete grids.
  class GridCoords : public Grid {
  protected:
    using Grid::Grid;
  public:
    real deltaPhi() const {
      real const ni      = sizes().ni;
      real const opening = shape().opening;
      return (shape().half
              ? (PI-2*opening)/2/(ni-1)
              : (PI-2*opening)/ni);
    }
    RegularCoords phi() const { 
      real const dx = deltaPhi();
      real const opening = shape().opening;
      return RegularCoords(opening, dx);
    }
    auto dPhi() const { return coordsDelta(phi()); }

    RegularCoords phiMed() const { 
      real const dx = deltaPhi();
      real const opening = shape().opening;
      return RegularCoords(opening+dx/2, dx);
    }
    auto dPhiMed() const { return coordsDelta(phiMed()); }
    
    RegularCoords phi(GridPosition p) const {
      real const dx = deltaPhi();
      real const shift = p == GridPosition::MED ? dx/2 : real(0);
      return RegularCoords(shape().opening+shift, dx);
    }
    
    real deltaTheta() const { return shape().sector/sizes().ns; }
    
    RegularCoords theta() const { 
      real const dx = deltaTheta();
      return RegularCoords(-shape().sector/2, dx);
    }
    auto dTheta() const { return coordsDelta(theta()); }
    
    RegularCoords thetaMed() const { 
      real const dx = deltaTheta();
      return RegularCoords(-shape().sector/2+dx/2, dx);
    }
    auto dThetaMed() const { return coordsDelta(thetaMed()); }
    
    RegularCoords theta(GridPosition p) const {
      real const dx = deltaTheta();
      real const shift = p == GridPosition::MED ? dx/2 : real(0);
      return RegularCoords(-shape().sector/2+shift, dx);
    }
    virtual void writeH5(HighFive::Group group) const override;
  };
  
  template<GridSpacing GS> class Coords;

  template<GridSpacing GS> void writeH5Coords(Coords<GS> const& grid, HighFive::Group group);

  /// \brief the grid has logarithmic spacing in the radial direction.
  /// Cells have similar sizes.
  template<> 
  class Coords<GridSpacing::LOGARITHMIC> : public GridCoords {
  public:
    static GridSpacing constexpr SPACING = GridSpacing::LOGARITHMIC;
    friend class Grid;

    static std::shared_ptr<Coords<SPACING> const> make(GasShape const& shape, HighFive::Group const& group);
    static std::shared_ptr<Coords<SPACING> const> make(GasShape const& shape, GridSizes const& sizes);

    virtual std::shared_ptr<Grid const> clone(range_t radialRange) const override;
    std::shared_ptr<Coords<SPACING> const> shared() const { return std::dynamic_pointer_cast<Coords<SPACING> const>(shared_from_this()); }

    virtual GridSpacing radialSpacing() const override;
    
    Coords<SPACING> const& global() const { return dynamic_cast<Coords<SPACING> const&>(Grid::global()); }
    
    auto radii()  const { return LogCoords(shape(), global().sizes().nr, radialOffset()); }
    auto dRadii() const { return coordsDelta(radii()); }
    
    auto radiiMed() const { return barycentricMedCoords(radii(), dim()); }
    
    virtual void writeH5(HighFive::Group group) const override {
      GridCoords::writeH5(group);
      fargOCA::writeH5Coords(*this, group);
    }

  private:
    Coords(GasShape const& shape, 
           GridSizes const& gsz)
      : GridCoords(shape,gsz, SPACING) {}
    
    Coords(Coords<SPACING> const& global, range_t radialRange) 
      : GridCoords(global, radialRange) {
      assert(radialSpacing() == global.radialSpacing());
      assert(radialSpacing() == SPACING);
    }
    Coords(GasShape const& shape, HighFive::Group const& group) 
      : GridCoords(shape, group) {
      assert(radialSpacing() == SPACING);
    }
  };

  /// \brief the grid has uniform spacing in the radial direction.
  /// So outer celles are bigger
  template<> 
  class Coords<GridSpacing::ARITHMETIC> : public GridCoords {
  public:
    static GridSpacing constexpr SPACING = GridSpacing::ARITHMETIC;
    friend class Grid;

    static std::shared_ptr<Coords<SPACING> const> make(GasShape const& shape, HighFive::Group const& group);
    static std::shared_ptr<Coords<SPACING> const> make(GasShape const& shape, GridSizes const& sizes);
    
    virtual std::shared_ptr<Grid const> clone(range_t radialRange) const override;
    std::shared_ptr<Coords<SPACING> const> shared() const { return std::dynamic_pointer_cast<Coords<SPACING> const>(shared_from_this()); }
    virtual GridSpacing radialSpacing() const override;

    Coords<SPACING> const& global() const { return dynamic_cast<Coords<SPACING> const&>(Grid::global()); }

    real radialDelta() const {
      auto const radius = shape().radius;
      return (radius.max - radius.min)/global().sizes().nr;
    }
    
    auto radii()  const {return RegularCoords(shape().radius.min, radialDelta(), radialOffset()); }
    auto dRadii() const { return coordsDelta(radii()); }

    static bool constexpr barycentricMed = true;
    
    template<bool BARYCENTRIC = barycentricMed>
    typename std::enable_if<BARYCENTRIC, BarycentricMedCoords<RegularCoords>>::type
    radiiMed() const { return barycentricMedCoords(radii(), dim()); }

    template<bool BARYCENTRIC = barycentricMed>
    typename std::enable_if<not BARYCENTRIC, RegularCoords>::type
    radiiMed() const { 
      real const dx = radialDelta();
      return RegularCoords(shape().radius.min+dx/2, dx, radialOffset());
    }
    
    virtual void writeH5(HighFive::Group group) const override {
      GridCoords::writeH5(group);
      fargOCA::writeH5Coords(*this, group);
    }
  
  private:
    Coords(GasShape const& shape, 
           GridSizes const& gsz)
      : GridCoords(shape, gsz, SPACING) {}
    
    Coords(Coords<SPACING> const& global, range_t radialRange) 
      : GridCoords(global, radialRange) {
      assert(radialSpacing() == SPACING);
    }
    Coords(GasShape const& shape, HighFive::Group const& group) 
      : GridCoords(shape, group) {
      assert(radialSpacing() == SPACING);
    }

  };

  typedef Coords<GridSpacing::ARITHMETIC>  ArCoords;
  typedef Coords<GridSpacing::LOGARITHMIC> LgCoords;
  
  std::ostream& operator<<(std::ostream& out, Grid const& coords);
  
  /// \brief Check if to coord sequences are close enough.
  std::optional<GasShape> compatible(Grid const& c1, Grid const& c2);
  bool compatible(Grid const& c1, Grid const& c2, real tol);

  /// \brief Check if to coord sequences are close enough.
  bool close(arr1d<real const> c1, arr1d<real const> c2, real tol, std::ostream& log);

  template<typename T = real>
  arr3d<T> scalarView(std::string label, GridSizes const& ext) {
    return arr3d<T>(label, ext.nr, ext.ni, ext.ns);
  }

  template<class COORDS, typename T = real>
  arr3d<T> scalarView(std::string label, COORDS const& g) {
    return scalarView<T>(label, g.sizes());
  }

  template<typename T = real>
  arr2d<T> profileView(std::string label, GridSizes const& ext) {
    return arr2d<T>(label, ext.nr, ext.ni);
  }
  
  template<class GRID, typename T = real>
  arr2d<T> profileView(std::string label, GRID const& g) {
    return profileView<T>(label, g.sizes());
  }

  arr1d<real const> readH5Coords(HighFive::Group const&  file, std::string name);
  
  harr1d<real const> hostRadiiValues(Grid const& grid, GridPosition pos, size_t n);
  harr1d<real const> hostRadiiValues(Grid const& grid, GridPosition pos);
  arr1d<real const> devRadiiValues(Grid const& grid, GridPosition pos, size_t n);
  arr1d<real const> devRadiiValues(Grid const& grid, GridPosition pos);
  harr1d<real const> hostLayersValues(Grid const& grid, GridPosition pos, size_t n);
  harr1d<real const> hostLayersValues(Grid const& grid, GridPosition pos);
  harr1d<real const> hostSectorsValues(Grid const& grid, GridPosition pos, size_t n);
  harr1d<real const> hostSectorsValues(Grid const& grid, GridPosition pos);

  /// \brief Deal with spacing at the cost of efficiency.
  std::function<real (size_t)> radialCoords(Grid const& grid);
  /// \brief Deal with spacing at the cost of efficiency.
  std::function<real (size_t)> radialMedCoords(Grid const& grid);
}

#include "h5io.hpp"

template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::GridPosition>() { 
  return fargOCA::h5::details::buildHFEnumDesc<fargOCA::GridPosition>(); 
}
template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::GridSpacing>() { 
  return fargOCA::h5::details::buildHFEnumDesc<fargOCA::GridSpacing>(); 
}
template<> HighFive::DataType HighFive::create_datatype<fargOCA::GridSizes>();

#endif
