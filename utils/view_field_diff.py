#!/usr/bin/env python3

import sys
import h5py as h5
import numpy as np
import argparse
from numpy import unravel_index

parser = argparse.ArgumentParser(description='''
Display the differences between a given field of two gas disk description files.
Example:
   {} ref/gas9.h5 out/gas9.h5 velocity/theta
'''.format(sys.argv[0]))
parser.add_argument('file1', metavar='H5FILE1', type=str, nargs=1,
                    help='HDF5 gas description file to be compared')
parser.add_argument('file2', metavar='H5FILE2', type=str, nargs=1,
                    help='HDF5 gas description file to be compared')
parser.add_argument('path', metavar='PATH', type=str, nargs=1,
                    help='Location of the field into the HDF5 file.')
parser.add_argument('--sector', metavar='J', type=int, dest='sector',
                    help='If 3D field, display that sector.')
parser.add_argument('--text', action='store_true',
                    help='Only print the difference  in text format.')
parser.add_argument('--max-diff', type=float, dest='max_diff', metavar='MAX',
                    help='Used with --text. Return status 0, if diff is smaller than MAX, -1 otherwise.')
parser.add_argument('--rel-diff', type=float, dest='rel_diff', metavar='MAX',
                    help='Used with --text. Return status 0, if relative diff is smaller than MAX, -1 otherwise.')

args = parser.parse_args()

path = args.path[0]
gas1 = h5.File(args.file1[0], 'r')
gas2 = h5.File(args.file2[0], 'r')

field1 = np.array(gas1[path]['polar_grid'])
field2 = np.array(gas2[path]['polar_grid'])
diff   = field1-field2
field_max = max(np.abs(field1).max(), np.abs(field2).max())
diff_max  = np.abs(diff).max()

if field1.shape != field2.shape:
    print("Incompatible disks.")
    sys.exit(-1)

flat   = field1.shape[1] == 1

if not args.text:
    import matplotlib as mpl
    mpl.use('TkAgg')
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    xymesh = None
    (nr,ni,ns) = field1.shape
    (radii,layers,sectors) = (range(nr), range(ni), range(ns))
    if 'grid' in gas1:
        (radii,layers,sectors) = (np.array(gas1['grid']['radii']),
                                  np.array(gas1['grid']['layers']),
                                  np.array(gas1['grid']['sectors']))
    if flat:
        print("this is a flat grid, displaying as it is.")
        xymesh = np.meshgrid(sectors, radii)
        field1  = np.mean(field1, 1)
        field2  = np.mean(field2, 1)
        diff    = field1-field2
    else:
        print("this is a fat grid. ")
        xymesh  = np.meshgrid(layers, radii)
        loc = unravel_index(diff.argmax(), diff.shape)
        print("max diff is at {}\n".format(loc))
        if args.sector:
            print(" Will display sector {}.".format(args.sector))
            field1  = field1[:,:,args.sector]
            field2  = field2[:,:,args.sector]
            diff    = field1-field2
        else:
            print(", merging sectors before displaying.")
            field1  = np.mean(field1, 2)
            field2  = np.mean(field2, 2)
            diff    = np.amax(diff, 2)
    def plot(f, xymesh, scl_min, scl_max, pos, name):
        plt.subplot(2,2,pos)
        plt.title(name)
        x,y = xymesh
        plt.pcolormesh(y,x,f, cmap='RdBu', vmin=scl_min, vmax=scl_max)
        plt.axis([y.min(), y.max(), x.min(), x.max()])
        plt.colorbar()

    plot(field1, xymesh, -field_max, field_max, 1, "{}-1".format(path))
    plot(field2, xymesh, -field_max, field_max, 2, "{}-2".format(path))
    plot(diff,   xymesh, -field_max, field_max, 3, "delta")
    plot(diff,   xymesh, -diff_max,  diff_max,  4, "enhanced delta")

    plt.show()
else:
    meanfield = np.abs(field1) + np.abs(field2) + np.finfo(float).eps
    reldiff = diff/meanfield
    print("Diff on {p} (mean: {m}): {a} (abs), {r} (rel).".format(p=path, a=diff_max, r=reldiff.max(), m=abs(meanfield).max()))
    
    close_enough = True
    if args.max_diff and (abs(diff).max() > args.max_diff): 
        print("Abs diff not close enough (> {m})".format(m=args.max_diff))
        close_enough = False
    if args.rel_diff  and (abs(reldiff).max() > args.rel_diff):
        print("Rel diff not close enough (> {m})".format(m=args.rel_diff))
        close_enough = False

    if not close_enough:
        sys.exit(-1)
    else:
        sys.exit(0)
