// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include <sysexits.h>
#include <filesystem>

#include <boost/program_options.hpp>
#include <nlohmann/json.hpp>

#include "allmpi.hpp"
#include "simulationConfig.hpp"
#include "simulationTrackingPlugin.hpp"
#include "boundariesConditionsPlugin.hpp"
#include "environment.hpp"
#include "viscosityFieldPlugin.hpp"
#include "planetarySystemEngine.hpp"
#include "configProperties.hpp"

namespace po  = boost::program_options;
namespace fs  = std::filesystem;

using namespace nlohmann::literals;
using json = nlohmann::ordered_json;
using namespace fargOCA::configProperties;
using fargOCA::snake;

template<typename V>
json
json_keys(std::map<std::string,V> const& m) {
  json keys;
  for(auto [k,v] : m) {
    keys.push_back(k);
  }
  return keys;
}

template<typename V>
json
json_keys(std::multimap<std::string,V> const& m) {
  json keys;
  for(auto [k,v] : m) {
    keys.push_back(k);
  }
  return keys;
}

json
json_keys(std::set<std::string> const& m) {
  json keys;
  for(auto const& k : m) {
    keys.push_back(k);
  }
  return keys;
}

json
json_keys(std::vector<std::string> const& m) {
  json keys;
  for(auto const& k : m) {
    keys.push_back(k);
  }
  return keys;
}

json
boundaries_json_schema() {
  json labels = json_keys(fargOCA::BoundariesConditionsPlugin::available());
  json j =
    {
     {"type", "object" },
     {"description", "Boundaries conditions handlers and configuration."},
     {"properties",
      {
       {snake(LABELS),
	{
	 {"type", "array" },
	 {"items",
	  {
	   {"type", "string" },
	   {"enum", labels },
	  }
	 }
        }
       },
       {snake(CONFIG),
        {
	 {"type", "object" },
	 {"description", "a key -> value map"},
	 {"additionalProperties",
	  {
	   {"type", "string" },
	   {"options", { {"disable_properties", false } } }
	  }
	 }
        }
       }
      }
     },
     { "required", {snake(LABELS), snake(CONFIG) } }
    };
  return j;
}

template<typename Plugin>
json
plugin_config_json_schema(std::vector<std::pair<std::string, json>> const& common, bool oneOf) {
  json alternatives;
  for ( std::string label : Plugin::available()) {
    Plugin const& plugin = Plugin::get(label);
    std::optional<std::set<std::string>> parameters = plugin.authorizedProperties();
    json config = { {"type", "object" } };
    if (parameters) {
      json jparams;
      for (std::string param: *parameters) {
        jparams[param] = { {"type", "string"} };      
      }
      config["properties"] = jparams;
    } else {
      config["additionalPproperties"] = {{"type", "string"}};
      config["options"]               = {{"disable_properties", false}};
    }
              
    json alternative =
      {
       {"title", label},
       {"description", plugin.description()},
       {"type", "object"},
       {"properties",
	{
	 {snake(TYPE),
	  {
	   {"type", "string"},
	   {"enum", {label}},
	   {"readOnly", true}
	  }
	 },
	 {snake(CONFIG), config}
        }
       },
      {"required", {snake(TYPE), snake(CONFIG)}}
    };
    for( auto const& [k,j] : common) {
      alternative["required"].push_back(k);
      alternative["properties"][k] = j;
    }
    alternatives.push_back(alternative);
  }
  if (oneOf) {
    return json({{"oneOf", alternatives}});
  } else {
    return alternatives;
  }
}

json
trackers_json_schema() {
  json trackers =
    {
     {"type", "object" },
     {"description",
      "Specify the trackers that will track the simulation. All output are produced through trackers so this should probably not be empty."},
     {"options", { {"disable_properties", false }}},
     {"properties", plugin_config_json_schema<fargOCA::SimulationTrackingPlugin>({}, false)}
    };
  return trackers;
}

json
viscosity_json_schema() {
  std::vector<std::pair<std::string,json>> common =
    {{snake(ARTIFICIAL),
       json({
	   {"type", "boolean"},
	   {"format", "checkbox"},
	   {"description", "add a term of artificial viscosity to avoid shocks."},
	   {"default", false}
	 })}
    };
  return plugin_config_json_schema<fargOCA::ViscosityFieldPlugin>(common, true);
}

json
output_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Describe the information that will be extracted from the simulation"},
    {"properties",
     {
       {snake(TIME_STEP),
        {
          {"type", "number"},
          {"description",
           "The user specified time step. "
           "It will be used as a unit for the number of steps. "
           "Internaly, it will be splitted into smaller time step to ensure convergence."
          },
          {"default", 6.2},
          {"exclusiveMinimum", 0}
        }
       },
       {snake(NB_STEP),
        {
          {"type", "integer"},
          {"description", "The user desired number of steps. Can also be specified on the simulation engine command line."},
          {"default", 100},
          {"exclusiveMinimum", 0}
        }
       },
       {snake(TRACKERS), trackers_json_schema()}
     }
    }
  };
  return j;
}

json
grid_json_schema() {
  json j = {
    {"type", "object"},
    {"description",
     "The disc gas is described through various scalar en vector fields. "
     "All these field are discretized on a polar grid obtained by shaping a "
     "3D grid into a sphere section (We need a reference here)."},
    {"properties",
     {
       {snake(RADII),
        {
          {"type", "integer"},
          {"description", "The radial resolution"},
          {"exclusiveMinimum", 3},
          {"default", 378}
        }
       },
       {snake(LAYERS),
        {
          {"type", "integer"},
          {"description", "The altidude resolution. A resolution of 1 indicate a 2D disk."},
          {"exclusiveMinimum", 0},
          {"default", 66}
        }
       },
       {snake(SECTORS),
        {
          {"type", "integer"},
          {"description", "The azimutal resolution."},
          {"exclusiveMinimum", 3},
          {"default", 4}
        }
       },
       {snake(SPACING),
        {
          {"type", "string"},
          {"enum", {"ARITHMETIC", "LOGARITHMIC"}},
          {"description", "Radial coordinates spacing."},
          {"default", "LOGARITHMIC"}
        }
       }
     }
    },
    {"required", {snake(RADII),snake(LAYERS),snake(SECTORS),snake(SPACING)}}
  };
  return j;
}

json
code_units_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Carateristic quantities of the studied disk. Some of those dimentions will be used as units."},
    {"properties",
     {
       {snake(CENTRAL_BODY),
        {
          {"type", "object"},
          {"description", "Caracteristics of the body at the center of the system (usually a star)."},
          {"properties",
           {
             {snake(MASS),
              {
                {"type", "number"},
                {"description", "The body mass in solar mass."},
                {"exclusiveMinimum", 0},
                {"default", 1.0}
              }
             },
             {snake(RADIUS),
              {
                {"type","number"},
                {"description", "The boy radius in solar radiuses"},
                {"exclusiveMinimum", 0},
                {"default", 1.0},
              }
             },
             {snake(TEMPERATURE),
              {
                {"type", "number"},
                {"description", "the body surface temperature in Kelvin."},
                {"exclusiveMinimum", 0},
                {"default", 4370.0}
              }
             }
           }
          },
          {"required", {snake(MASS),snake(RADIUS),snake(TEMPERATURE)}}
        }
       },
       {snake(SEMI_MAJOR_AXIS),
        {
          {"type", "number"},
          {"description", "Some important distance specified in astronomical unit. Typicaly the semi major axis of an important orbiting body."},
          {"exclusiveminimum", 0},
          {"default", 5.2},
        }
       }
     }
    },
    {"required", {snake(CENTRAL_BODY), snake(SEMI_MAJOR_AXIS)}}
  };
  return j;
}

json
gas_shape_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Describe the shape of the gas disk."},
    {"properties",
     {
       {snake(RADIUS),
        {
          {"type", "object"},
          {"description", "The radial span of the disk, in astronomical units (?)."},
          {"properties",
           {
             {snake(MIN),
              {
                {"type", "number"},
                {"description", "Span begining"},
                {"exclusiveMinimum", 0},
                {"default", 0.375},
              }
             },
             {snake(MAX),
              {
                {"type", "number"},
                {"description", "Span ending"},
                {"exclusiveMinimum", 0},
                {"default", 9.8}
              }
             },
           }
          },
          {"required", {snake(MIN),snake(MAX)}}
        }
       },
       {snake(HALF),
        {
          {"type", "boolean"},
          {"format", "checkbox"},
          {"description", "Disk is assumed symetric in the altitude axis and we only store half of it."},
          {"default", false},
        }
       },
       {snake(OPENING),
        {
          {"type", "number"},
          {"description", "The colatitude angle in radians."},
          {"default", 1.3613568},
          {"exclusiveMinimum", 0},
        }
       },
       {snake(ASPECT_RATIO),
        {
          {"$comment", "Need to find something short and clear."},
          {"type", "number"},
          {"description", "Describe the disc thickness in some way."},
          {"exclusiveminimum", 0},
          {"default", 1.4},
        }
       }
     }
    },
    {"required", {snake(RADIUS), snake(HALF), snake(OPENING), snake(ASPECT_RATIO)}}
  };
  return j;
}

json
referential_json_schema() {
  json j = {
    {"type","object"},
    {"title", snake(REFERENTIAL)},
    {"description", "Describe the referential frame rotation."},
    {"properties",
     {
       {snake(TYPE),
        {
          {"type", "string"},
          {"description",
           "Referential frame, must be one of CONSTANT "
           "(user specified constant) or COROTATING "
           "(match the speed of the planet specified by GuidingPlanet)."},
          {"enum", {"CONSTANT", "COROTATING"}},
          {"default", "CONSTANT"},
        }
       },
       {snake(OMEGA),
        {
          {"type", {"number", "null"}},
          {"description", "reference frame's  rotation frequency."},
        }
       },
       {snake(GUIDING_PLANET),
        {
          {"type", {"string", "null"}},
          {"description", "The name of the guiding planet."},
        }
       },
       {snake(INDIRECT_FORCES),
        {
          {"type", "boolean"},
          {"format", "checkbox"},
          {"description", "indirect forces act on planets and on disk because the system is centred on the star (non barycentric frame)."},
          {"default", true},
        }
       }
     }
    },
    {"required", {snake(TYPE), snake(INDIRECT_FORCES), snake(OMEGA), snake(GUIDING_PLANET)}}
  };
  return j;
}

json
opacity_json_schema() {
  json j= {
    {"type", "object"},
    {"description", "Gas opacity model"},
    {"properties",
     {
       {snake(TYPE),
        {
          {"type", "string"},
          {"description", "can be either constant (CONSTANT) or prescribed by Bell and Lin tables (BELL_LIN)"},
          {"enum", {"CONSTANT", "BELL_LIN"}},
          {"default", "BELL_LIN"}
        }
       },
       {snake(CONFIG),
        {
          {"type", "object"},
          {"additionalProperties",
           {
             {"type", {"string", "number"}}
           }
          },
          {"options",
           {
             {"disable_properties", false}
           }
          }
        }
       }
     }
    },
    {"required",{snake(TYPE), snake(CONFIG)}}
  };
  return j;
}

json
solver_json_schema() {
  json j= {
    {"type", "object"},
    {"description", "Select and configure the heat equation solver."},
    {"properties",
     {
       {snake(LABEL),
        {
          {"type", "string"},
          {"description", "The label of the registered solver to use."},
          {"enum", {"legacy"}},
          {"default", "legacy"}
        }
       },
       {snake(CONFIG),
        {
          {"type", "object"},
          {"description", "Solver configuration is specified as a set of key->value (both specified as strings)."},
          {"properties",
           {
             {"epsilon",
              {
                {"type", "string"},
                {"description", "stop when the normalized norm between two concecutive solution is < epsilon."},
                {"default", "5e-7"}
              }
             }
           }
          },
          {"options",
           {
             {"disable_properties", false}
           }
          },
          {"additionalProperties",
           {
             {"type", "string"}
           }
          },
          {"required", {"epsilon"}}
        }
       }
     }
    },
    {"required", {snake(LABEL), snake(CONFIG)}}
  };
  return j;
}

json
star_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Not sure...."},
    {"properties",
     {
       {snake(SHADOW_ANGLE),
        {
          {"type", "number"},
          {"description", "Opening angle (degrees) for shadowed stellar radiation."},
          {"default", 7}
        }
       }
     }
    },
    {"required", {snake(SHADOW_ANGLE)}}
  };
  return j;
}

json
radiative_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "We take into account the heat radiated by the star through a diffusion equation."},
    {"properties",
     {
       {snake(DUST_TO_GAS),
        {
          {"type", "number"},
          {"description", "Dust to gas ratio"},
          {"default", 0.01},
        }
       },
       {snake(Z_BOUNDARY_TEMPERATURE),
        {
          {"type", "number"},
          {"description", "Temperature at the vertical disk boundaries."},
          {"default", 3.0},
        }
       },
       {snake(OPACITY), opacity_json_schema()},
       {snake(SOLVER), solver_json_schema()},
       {snake(STAR), star_json_schema()}
     }
    },
    {"required", {snake(DUST_TO_GAS), snake(Z_BOUNDARY_TEMPERATURE), snake(OPACITY), snake(SOLVER)}}
  };
  return j;
}

json
adiabatic_json_schema() {
  json j = {
    {"type","object"},
    {"description",
     "We assume adiabatic evolution. If not activated, "
     "the evolution is isothermal."},
    {"properties",
     {
       {snake(INDEX),
        {
          {"type", "number"},
          {"description", "Adiabatic index, whatever that is."},
          {"default", 1.4}
        }
       },
       {snake(RADIATIVE), radiative_json_schema()}
     }
    },
    {"required", {snake(INDEX)}}
  };
  return j;
}

json
density_cavity_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Cavity modeled as in Robert Meheut 2020 2D case, here also modeled in 3D"},
    {"properties",
     {
       {snake(RADIUS),
        {
          {"type", "number"},
          {"description", "Maximum distance from the star at which the cavity is operating."},
          {"default", 0}
        }
       },
       {snake(RATIO),
        {
          {"type", "number"},
          {"description", "Normalization factor applied to the surface density for r<Cavity.Radius."},
          {"default", 1}
        }
       },
       {snake(WIDTH),
        {
          {"type", "number"},
          {"description", "Width of the cavity."},
          {"default", 1}
        }
       }
     }
    },
    {"required", {snake(RADIUS), snake(RATIO), snake(WIDTH)}}
  };
  return j;
}

json
density_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Configuration of the initial disk density radial profile"},
    {"properties",
     {
       {snake(CAVITY), density_cavity_json_schema()},
       {snake(MINIMUM),
        {
          {"type", "number"},
          {"description", "Volume (surface in 2D) density floor"},
          {"default", 1e-25}
        }
       },
       {snake(SLOPE),
        {
          {"type", "number"},
          {"description", "Slope of the power law's surface density."},
          {"default", 0}
        }
       },
       {snake(START),
        {
          {"type", "number"},
          {"description", "Surface density value at r=1, code units (typical value  6.e-4, i.e. $200g/cm^2$ in the default case with unit of distance R0=5.2AU)"},
          {"default", 6e-4}
        }
       }
     }
    },
    {"required", {snake(CAVITY), snake(MINIMUM), snake(SLOPE), snake(START)}}
  };
  return j;
}

json
smoothing_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Smoothing  the planet's potential  to avoid the singularity for r->0"},
    {"properties",
     {
       {snake(CHANGE),
        {
          {"type", "boolean"},
          {"format", "checkbox"},
          {"description", "if true: smoothly divide by 2 the smoothing lenght in a type provided by Smoothing.Taper"},
          {"default", false}
        }
       },
       {snake(FLAT),
        {
          {"type", "boolean"},
          {"format", "checkbox"},
          {"description", "if true, defines the planet potential as sqrt(GMp/(r+rsmoothing)), recommended in 2D simulations. False: the potential is set by a cubic function (recommended in 3D simulations) "},
          {"default", false}
        }
       },
       {snake(SIZE),
        {
          {"type", "number"},
          {"description", "size of the smoothing lenght in unit of Hill radius for Flat false, in unit of H for Flat true"},
          {"default", 0.5}
        }
       },
       {snake(TAPER),
        {
          {"type", "number"},
          {"description", "if Change, time in unit of 2pi for the change of the smoothing lenght by a factor of 2"},
          {"default", 1e-07}
        }
       }
     }
    },
    {"required", {snake(CHANGE), snake(FLAT), snake(SIZE)}}
  };
  return j;
}

json
star_accretion_json_schema() {
  json j = {
    {"type","object"},
    {"description", "Radial transport of gas to account for the observed stars' accretion rates."},
    {"properties",
     {
       {snake(TYPE),
        {
          {"type", "string"},
          {"description", "Choose CONSTANT for Mdot disk accreting at RATE, choose WIND for accretion occurring in the ionized layers"},
          {"enum", {"CONSTANT","WIND"}},
          {"default", "CONSTANT"}
        }
       },
       {snake(RATE),
        {
          {"type","number"},
          {"description","Accretion rate in Solar mass per year (typical values in the range 1.e-8-1.e-7)"},
          {"default", 1e-8}
        }
       },
       {snake(WIND),
        {
          {"type", "object"},
          {"description", "Configuration of accretion occurring in the ionized layers"},
          {"properties",
           {
             {snake(ACTIVE_DENSITY),
              {
                {"type","number"},
                {"description", "Surface density of the ionized layer (typical value 1 in $g/cm^2$)"},
                {"default", 1}
              }
             },
             {snake(FILTER),
              {
                {"type","number"},
                {"description", "Steepness of the transition between ionized and non ionized region (typical values in the range [5-10])"},
                {"default", 10}
              }
             }
           }
          },
          {"required", {snake(ACTIVE_DENSITY), snake(FILTER)}}
        }
       }
     }
    },
    {"required", {snake(TYPE), snake(RATE)}}
  };
  return j;
}

json
transport_json_schema() {
  json j = {
    {"type", "string"},
    {"description", "Transport can be FAST (FARGO algorithm) or STANDARD."},
    {"enum", {"FAST", "STANDARD"}},
    {"default", "FAST"}
  };
  return j;
}

json
physic_json_schema() {
  json j = {
    {"type","object"},
    {"description",
     "The physical caracteristics of the disk maninulated by the simulation. "
     "It also describe, if relevant, the caracteristics of the planets."},
    {"properties",
     {
       {snake(CODE_UNITS),   code_units_json_schema()},
       {snake(GAS_SHAPE),    gas_shape_json_schema()},
       {snake(REFERENTIAL), referential_json_schema()},
       {snake(ADIABATIC),   adiabatic_json_schema()},
       {snake(BOUNDARIES),  boundaries_json_schema()},
       {snake(DENSITY),     density_json_schema()},
       {snake(CFL_SECURITY),
        {
          {"type", "number"},
          {"description", "As a security check consider the timestep obtained with CFL condition normalized by a factor<1 (suggested 1/D D=dimension of the disk)"},
          {"default", 0.5},
          {"exclusiveMinimum", 0},
          {"exclusiveMaximum", 1}
        }
       },
       {snake(DRAGGING_COEF),
        {
          {"type", "number"},
          {"description", "???"},
          {"default", 1}
        }
       },
       {snake(FLARING_INDEX),
        {
          {"type", "number"},
          {"description", "Disk flaring index."},
          {"default", 0.28}
        }
       },
       {snake(HILL_CUT_FACTOR),
        {
          {"type", "number"},
          {"description", "Apply Hill cut on a region of size HillCutFactor in unit of Hill radius."},
          {"default", 0.6}
        }
       },
       {snake(MEAN_MOLECULAR_WEIGHT),
        {
          {"type", "number"},
          {"description", "Gas mean molecular weight"},
          {"default", 2.3}
        }
       },
       {snake(SMOOTHING),  smoothing_json_schema()},
       {snake(TRANSPORT),  transport_json_schema()},
       {snake(VISCOSITY),  viscosity_json_schema()},
       {snake(STAR_ACCRETION), star_accretion_json_schema()}
     }
    },
    {"required", {snake(CODE_UNITS), snake(GAS_SHAPE), snake(REFERENTIAL), snake(BOUNDARIES), snake(DENSITY), snake(CFL_SECURITY), snake(DRAGGING_COEF), snake(FLARING_INDEX), snake(HILL_CUT_FACTOR), snake(MEAN_MOLECULAR_WEIGHT), snake(SMOOTHING), snake(TRANSPORT), snake(VISCOSITY)}}
  };
  
  return j;
}

json
disk_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Provide the disk caracteristics, including its shape, resolution, physic and planetary system."},
    {"properties",
     {
       {snake(GRID), grid_json_schema()},
       {snake(PHYSIC), physic_json_schema()},
     }
    },
    {"required", {snake(GRID), snake(PHYSIC)}}
  };
  return j;
}

json
planets_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Planet description, at least one is required."},
    {"options",
     {
       {"disable_properties", false}
     }
    },
    {"additionalProperties",
     {
       {"type", "object"},
       {"description", "Planet description"},
       {"properties",
        {
          {snake(MASS),
           {
             {"type", "number"}
           }
          },
          {snake(SEMI_MAJOR_AXIS),
           {
             {"type", "number"},
             {"description", "The semi major axis of the planet orbit in code units."},
             {"exclusiveMinimum", 0},
             {"exclusiveMaximum", 1}
           }
          },
          {snake(ECCENTRICITY),
           {
             {"type", "number"},
             {"description", "The planet orbit eccentricity."},
             {"minimum", 0}
           }
          },
          {snake(INCLINATION),
           {
             {"type", "number"},
             {"description", "The planet orbit inclination."}
           }
          },
          {snake(MEAN_LONGITUDE),
           {
             {"type", "number"}
           }
          },
          {snake(LONGITUDE_OF_PERICENTER),
           {
             {"type", "number"}
           }
          },
          {snake(LONGITUDE_OF_NODE),
           {
             {"type", "number"}
           }
          },
          {snake(FEEL),
           {
             {"type", "object"},
             {"description", "Effects to with the planet is sensitive."},
             {"properties",
              {
                {snake(OTHERS),
                 {
                   {"type", "boolean"},
                   {"format", "checkbox"},
                   {"description", "Feels the gravitational pull of other planets."},
                   {"default", true}
                 }
                },
                {snake(DISK),
                 {
                   {"type", "boolean"},
                   {"format", "checkbox"},
                   {"description", "Feels the gravitational pull of gas."},
                   {"default", true}
                 }
                },
              }
             },
             {"required", {snake(OTHERS), snake(DISK)}}
           }
          }
        }
       },
       {"required", {snake(MASS), snake(SEMI_MAJOR_AXIS), snake(LONGITUDE_OF_NODE), snake(LONGITUDE_OF_PERICENTER),
		     snake(MEAN_LONGITUDE), snake(INCLINATION), snake(ECCENTRICITY), snake(FEEL) }}
     }
    }
  };
  return j;
}

json
planetary_system_engine_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Select and configure the planets engine."},
    {"properties",
     {
       {snake(LABEL),
        {
          {"type", "string"},
          {"description", "Engine name"},
          {"enum", json_keys(fargOCA::PlanetarySystemEngine::available())},
          {"default", "Runge_Kutta"}
        }
       },
       {snake(CONFIG),
        {
          {"type", "object"},
          {"description", "A key->value map that will be provided to the handler."},
          {"additionalProperties",
           {
             {"type", "string"},
             {"options", {{"disable_properties", false}}}
           }
          }
        }
       }
     }
    },
    {"required", {snake(LABEL), snake(CONFIG)}}
  };
  return j;
}

json
planetary_system_json_schema() {
  json j = {
    {"type", "object"},
    {"description", "Describe de planets orbital elements and interactions"},
    {"properties",
     {
       {snake(PLANETS), planets_json_schema()},
       {snake(EXCLUDE_HILL),
        {
          {"type", "boolean"},
          {"format", "checkbox"},
          {"description", "Exclude Hill region when computing torque acting on planet, choose true or false."},
          {"default", false}
        }
       },
       {snake(ENGINE), planetary_system_engine_json_schema()}
     }
    },
    {"required", {snake(EXCLUDE_HILL), snake(ENGINE), snake(PLANETS)}}
  };
  return j;
}

json
fargOCA_json_schema() {
  json fargOCA = {
    {"$schema", "https://json-schema.org/draft/2020-12/schema"},
    {"$id", "https://example.com/product.schema.json"},
    {"title", "FargOCA config file"},
    {"description", "An input file for a FargOCA simulation"},
    {"type", "object"},
    {"properties",
     {
       {snake(DISK),              disk_json_schema()},
       {snake(PLANETARY_SYSTEM),  planetary_system_json_schema()},
       {snake(OUTPUT),            output_json_schema()}
     }
    },
    {"options",
     {
       {"disable_edit_json", false}
     }
    },
    {"required", {snake(DISK), snake(OUTPUT)}}
  };
  return fargOCA;
}

int
main(int argc, char *argv[]) {
  std::string cmd = fs::path(argv[0]).filename().native();
  std::ostringstream usage;
  usage << "Print fargOCA plugins related informations.\n"
        << '\t' << cmd << " [option]+.\n";
  po::options_description opts(usage.str());
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("output,o", po::value<std::string>(), "output file, default to stdout.")
    ("var-name", po::value<std::string>()->default_value("fargOCASchema"), "When printing fargOCA config schema, force the JS variable name.")
    ;
  po::store(po::command_line_parser(argc, argv).options(opts).run(), vm);
  po::notify(vm);
  if (bool(vm.count("help"))) {
    std::cout << opts;
    return EXIT_SUCCESS;
  }
  std::unique_ptr<std::ofstream> ofile;
  if (bool(vm.count("output"))) {
    auto fname = vm["output"].as<std::string>();
    ofile.reset(new std::ofstream(fname, std::ios::out | std::ios::trunc));
    if (!ofile || !(*ofile)) {
      std::cerr << "Could not open output file '" << fname << std::endl;
      return EXIT_FAILURE;
    }
  }
  std::ostream& out = ofile ? *ofile : std::cout;
  
  out << "var " << vm["var-name"].as<std::string>() << " = "
      << fargOCA_json_schema().dump(2) << std::endl;

  return EXIT_SUCCESS;
}

