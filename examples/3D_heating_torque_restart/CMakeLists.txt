# Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
# Copyright 2018, Clément Robert, clement.robert<at>oca.eu
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

configure_file(config.info.in config.info @ONLY)
configure_file(../regold.sh.in regold.sh @ONLY)
configure_file(runMPI.sh.in runMPI.sh @ONLY)
configure_file(check.sh.in check.sh @ONLY)

if(ENABLE_PARALLEL)
  add_test(big_3D_heating_torque_restart4 runMPI.sh -n 4)
else()
  add_test(big_3D_heating_torque_restart1 runMPI.sh -n 1)
endif()

if(GENERATE_DOC AND PDFLATEX_COMPILER)
  add_custom_target(3D_heating_torque_config_tex ALL
    ${CMAKE_BINARY_DIR}/utils/config_doc config.info ${CMAKE_CURRENT_BINARY_DIR}/config.tex
    DEPENDS config_doc config.info.in)
  
  add_custom_target(3D_heating_torque_config_pdf
    ${PDFLATEX_COMPILER} -output-directory=${CMAKE_CURRENT_BINARY_DIR}  ${CMAKE_CURRENT_BINARY_DIR}/config.tex 
    DEPENDS 3D_heating_torque_config_tex)
endif()
  
add_custom_target(test_lum_dependencies ALL
  DEPENDS 
  fargOCA
  fargoInit
  tests_utils_dependencies )
