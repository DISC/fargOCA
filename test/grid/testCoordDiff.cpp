// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include "grid.hpp"
#include "environment.hpp"

using namespace fargOCA;

bool
test(std::string desc, std::shared_ptr<Grid const> g1, std::shared_ptr<Grid const> g2, real tol, bool pass) {
  std::cout << "Test " << desc << " -> " << std::flush;
  std::optional<GasShape> compatibility = compatible(*g1,*g2);
  bool goodEnough = compatibility && max(*compatibility) < tol;
  if (!goodEnough) {
    std::cout << "not " << std::flush;
  }
  std::cout << " close enough" << std::flush;
  if (pass == goodEnough) {
    std::cout << " as expected. passed.\n";
    return true;
  } else {
    std::cout << ". failed\n";
    return false;
  }
}

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);

  GasShape shape      (0.05, 42, 56,       0.001, false);
  GasShape shapeR1    (0.05, 43, 56,       0.001, false);
  GasShape shapeR2    (0.05, 42, 57,       0.001, false);
  GasShape shapeRClose(0.05, 42, 56+1e-15, 0.001, false);
  GasShape shapeO1    (0.06, 42, 56,       0.02,  false);
  
  int failed = 0;
  constexpr GridSpacing arith = GridSpacing::ARITHMETIC;
  constexpr GridSpacing logar = GridSpacing::LOGARITHMIC;
  if (!test("GridCoords(shape, {13,5,4}, arith), GridCoords(shape, {13,5,3}, arith)", Grid::make(shape, {13,5,4}, arith), Grid::make(shape, {13,5,3}, arith ), 1000, false))   { ++failed; }
  if (!test("GridCoords(shape, {13,5,4}, arith ), GridCoords(shape, {13,6,4}, arith )", Grid::make(shape, {13,5,4}, arith ), Grid::make(shape, {13,6,4}, arith ), 1000, false))   { ++failed; }
  if (!test("GridCoords(shape, {13,5,4}, arith ), GridCoords(shape, {12,5,4}, arith )", Grid::make(shape, {13,5,4}, arith ), Grid::make(shape, {12,5,4}, arith ), 1000, false))   { ++failed; }
  if (!test("GridCoords(shape, {13,5,4}, arith ), GridCoords(shape, {13,5,4}, arith )", Grid::make(shape, {13,5,4}, arith ), Grid::make(shape, {13,5,4}, arith ), 1e-17, true)) { ++failed; }
  
  if (!test("GridCoords(shape, {13,4 ), GridCoords}, arith(shape, 13,5 )", Grid::make(shape, {13,1,4}, arith ), Grid::make(shape, {13,1,5}, arith ), 1000, false)) { ++failed; }
  if (!test("GridCoords(shape, {13,4 ), GridCoords}, arith(shape, 12,4 )", Grid::make(shape, {13,1,4}, arith ), Grid::make(shape, {12,1,4}, arith ), 1000, false)) { ++failed; }
  if (!test("GridCoords(shape, {13,4 ), GridCoords}, arith(shape, 12,4 )", Grid::make(shape, {13,1,4}, arith ), Grid::make(shape, {12,1,4}, logar ), 1000, false)) { ++failed; }
  if (!test("GridCoords(shape, {13,4 ), GridCoords}, arith(shape, 13,4 )", Grid::make(shape, {13,1,4}, arith ), Grid::make(shape, {13,1,4}, arith ), 1e-17, true)) { ++failed; }
  if (!test("GridCoords(shape, {13,4 ), GridCoords}, arith(shape, 13,4 )", Grid::make(shape, {13,1,4}, arith ), Grid::make(shape, {13,1,4}, logar ), 1e-17, false)) { ++failed; }
  
  if (!test("GridCoords(shape, {13,20,4}, arith), GridCoords(shapeR2, {13,20,4}, arith)", Grid::make(shape, {13, 20, 4}, arith ), Grid::make(shapeR2, {13, 20, 4}, arith ), 1e-12, false)) { ++failed; }
  if (!test("GridCoords(shape, {13,20,4}, arith), GridCoords(shapeR1, {13,20,4}, arith )", Grid::make(shape, {13, 20, 4}, arith ), Grid::make(shapeR1, {13, 20, 4}, arith ), 1e-12, false)) { ++failed; }

  if (!test("GridCoords(shape, {13,20,4}, arith), GridCoords(shapeRClose, {13,20,4}, arith )", Grid::make(shape, {13, 20, 4}, arith ), Grid::make(shapeRClose, {13, 20, 4}, arith ), 1e-12, true)) { ++failed; }
  if (!test("GridCoords(shape, {13,20,4}, arith), GridCoords(shapeRClose, {13,20,4}, arith )", Grid::make(shape, {13, 20, 4}, arith ), Grid::make(shapeRClose, {13, 20, 4}, arith ), 1e-12, true)) { ++failed; }
  
  if (!test("GridCoords(shape, {13,4), GridCoords}, arith(shapeR1, 13,4)", Grid::make(shape, {13,1,4}, arith ), Grid::make(shapeR1, {13,1,4}, arith ), 1e-12, false)) { ++failed; }
  if (!test("GridCoords(shape, {13,4), GridCoords}, arith(shapeR2, 13,4)", Grid::make(shape, {13,1,4}, arith ), Grid::make(shapeR2, {13,1,4}, arith ), 1e-12, false)) { ++failed; }
  
  if (!test("GridCoords(shape, {13,20,4}, arith), GridCoords(shapeO1, {13,20,4}, arith)", Grid::make(shape, {13, 20, 4}, arith ), Grid::make(shapeO1, {13, 20, 4}, arith ), 1e-12, false)) { ++failed; }
  if (!test("GridCoords(shape, {13,4), GridCoords}, arith(shapeO1, 13,4)", Grid::make(shape, {13,1,4}, arith ), Grid::make(shapeO1, {13,1,4}, arith ), 1e-12, false)) { ++failed; }
  
  if (failed > 0) {
    std::cerr << "Failed " << failed << " test\n";
    return failed;
  } else {
    return 0;
  }
}
