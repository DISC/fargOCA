// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "planetarySystemPhysic.hpp"

using namespace fargOCA;

typedef PlanetarySystemPhysic::Planet::MassTaper MassTaper;

real const initial_mass = 0;
real const final_mass   = 4.2;
real const begin_time   = 2;
real const end_time     = 4;

MassTaper taper(initial_mass, final_mass, begin_time, end_time);

int failed = 0;

void
fail(std::string msg) {
  ++failed;
  std::cerr << msg << '\n';
}

void before() {
  if (!taper.before(begin_time/2)) fail("taper.before(begin_time/2))");
  if (!taper.before(std::nextafter(begin_time,real(0)))) fail("taper.before(std::nextafter(begin_time,real(0)))");
  if (!!taper.before(std::nextafter(begin_time,end_time))) fail("!taper.before(std::nextafter(begin_time,end_time))");
  if (!!taper.before((begin_time+end_time)/2)) fail("!taper.before((begin_time+end_time)/2)");
  if (!!taper.before(std::nextafter(end_time, begin_time))) fail("!taper.before(std::nextafter(end_time, begin_time))");
  if (!!taper.before(std::nextafter(end_time, end_time+1))) fail("!taper.before(std::nextafter(end_time, end_time+1))");
  if (!!taper.before(end_time+1)) fail("!taper.before(end_time+1)");
}

void active() {
  if (!!taper.active(begin_time/2)) fail("!taper.active(begin_time/2)");
  if (!!taper.active(std::nextafter(begin_time,real(0)))) fail("!taper.active(std::nextafter(begin_time,real(0)))");
  if (!taper.active(std::nextafter(begin_time,end_time))) fail("taper.active(std::nextafter(begin_time,end_time))");
  if (!taper.active((begin_time+end_time)/2)) fail("taper.active((begin_time+end_time)/2)");
  if (!taper.active(std::nextafter(end_time, begin_time))) fail("taper.active(std::nextafter(end_time, begin_time))");
  if (!!taper.active(std::nextafter(end_time, end_time+1))) fail("!taper.active(std::nextafter(end_time, end_time+1))");
  if (!!taper.active(end_time+1)) fail("!taper.active(end_time+1)");
}

void after() {
  if (!!taper.after(begin_time/2)) fail("!taper.after(begin_time/2)");
  if (!!taper.after(std::nextafter(begin_time,real(0)))) fail("!taper.after(std::nextafter(begin_time,real(0)))");
  if (!!taper.after(std::nextafter(begin_time,end_time))) fail("!taper.after(std::nextafter(begin_time,end_time))");
  if (!!taper.after((begin_time+end_time)/2)) fail("!taper.after((begin_time+end_time)/2)");
  if (!!taper.after(std::nextafter(end_time, begin_time))) fail("!taper.after(std::nextafter(end_time, begin_time))");
  if (!taper.after(std::nextafter(end_time, end_time+1))) fail("taper.after(std::nextafter(end_time, end_time+1))");
  if (!taper.after(end_time+1)) fail("taper.after(end_time+1)");
}

void
test_massAt(real time, real expected, real tol) {
  real const mass = taper.massAt(time);
  std::cout << "tapper.massAt(" << time << ") = " << mass << std::flush;
  if (std::abs(mass-expected) > tol) {
    std::cout << " while expecting " << expected << ", diff "
	      << std::abs(mass-expected) << " > " << tol << ", Failed\n";
    ++failed;
  } else {
    std::cout << "\n";
  }
}

void massAt() {
  test_massAt(begin_time/2, initial_mass,1e-20);
  test_massAt(std::nextafter(begin_time,real(0)), initial_mass, 1e-16);
  test_massAt(begin_time, initial_mass, 1e-20);
  test_massAt(2, 0, 1e-20);
  test_massAt(2.2000000000000000e+00, 1.0278131578017768e-01, real(1e-15));
  test_massAt(2.4000000000000000e+00,  4.0106431181261021e-01, real(1e-15));
  test_massAt(2.6000000000000000e+00,  8.6565097018580661e-01, real(1e-15));
  test_massAt(2.8000000000000000e+00,  1.4510643118126099e+00, real(1e-15));
  test_massAt(3.0000000000000000e+00,  2.1000000000000000e+00, real(5e-15));
  test_massAt(3.2000000000000000e+00,  2.7489356881873896e+00, real(1e-15));
  test_massAt(3.4000000000000000e+00,  3.3343490298141965e+00, real(5e-15));
  test_massAt(3.6000000000000000e+00,  3.7989356881873921e+00, real(5e-15));
  test_massAt(3.8000000000000000e+00,  4.0972186842198237e+00, real(5e-15));
  test_massAt(4.0000000000000018e+00,  4.2000000000000000e+00, real(1e-15));
  
  test_massAt(4.0000001000000000e+00, 4.2000000000000000e+00, real(1e-20));
  test_massAt(end_time, final_mass, real(1e-20));
  test_massAt(std::nextafter(end_time, end_time+1), final_mass, real(1e-20));
  test_massAt(end_time+42, final_mass, real(1e-20));
}

int main() {
  before();
  after();
  massAt();
  if (failed > 0) {
    std::cout << "FAILED " << failed << " tests\n";
    return failed;
  } else {
    std::cout << "PASSED\n";
    return 0;
  }
}
