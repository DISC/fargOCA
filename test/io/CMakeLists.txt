# Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
# Copyright 2018, Clément Robert, clement.robert<at>oca.eu
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

add_executable(h5triplet h5triplet.cpp)
target_link_libraries(h5triplet fargort)
add_test(h5triplet h5triplet)

if (ENABLE_PARALLEL)
  add_executable(testDiskProfileH5 testDiskProfileH5.cpp)
  target_link_libraries(testDiskProfileH5 fargort)
  add_test(testDiskProfileH53 ${MPILAUNCH} -n 3 ./testDiskProfileH5)
endif()
