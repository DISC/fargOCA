// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "gridCellSizes.hpp"
#include "grid.hpp"
#include "loops.hpp"
#include "mathAddOns.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace kkm = Kokkos::Experimental;

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

#define LBD KOKKOS_LAMBDA 
  
  using kk::pow;
  using kk::cos;
  using kk::sin;

  template <GridSpacing GS>
  void
  GridCellSizes::init(Coords<GS> const& coords) {
    auto theta    = coords.theta();
    auto thetaMed = coords.thetaMed();
    auto radMed   = coords.radiiMed();
    auto radii    = coords.radii();
    auto phi      = coords.phi();
    auto phiMed   = coords.phiMed();
    arr3d<real>       dTheta = myDTheta;
    arr3d<real>       volume = myVolume;
    auto dtheta   = coords.dTheta();
    
    // support non uniform grid for both 2D and 3D
    switch (coords.dim()) {
    case 3:
      kk::parallel_for("grid_cell_dtheta1_3d", fullRange(coords),
                       LBD(size_t const  i, size_t const  h, size_t const  j) {
                         real const radii3  = ipow<3>(radii(i));
                         real const radSup3 = ipow<3>(radii(i+1));
                         real const sinPhiMed = sin(phiMed(h));
                         volume(i,h,j)  = dtheta(j)*(radSup3 - radii3)*(cos(phi(h))-cos(phi(h+1)))/3;
                         dTheta(i,h,j)  = dtheta(j)*radMed(i)*sinPhiMed;
                       });
      break;
    case 2:
      kk::parallel_for("grid_cell_dtheta1_3d", fullFlatRange(coords),
                       LBD(size_t const  i, size_t const  j) {
                         real const radii2  = ipow<2>(radii(i));
                         real const radSup2 = ipow<2>(radii(i+1));
                         volume(i,0,j)    = dtheta(j)*(radSup2 - radii2)/2;
                         dTheta(i,0,j)    = dtheta(j)*radMed(i);
                       });
      break;
    default:
      std::abort();
    }
  }
    
  GridCellSizes::GridCellSizes(Grid const& grid)
    : myVolume(scalarView("grid_volumes",       grid)),
      myDTheta(scalarView("cell_inner_surface", grid)) {
    switch(grid.radialSpacing()) {
    case ARTH: init(grid.as<ARTH>()); break;
    case LOGR: init(grid.as<LOGR>()); break;
    }
  }
  
  GridCellSizes::~GridCellSizes() {}
}
