// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "hillForce.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace kk = Kokkos;
namespace HF = HighFive;

using std::optional;

void
write_torque_density(Disk const& disk, std::string planet, std::ostream& out) {
  std::pair<arr1d<real>, arr1d<real>> torques = GetTorqueContribs(disk, planet);
  if (disk.dispatch().master()) {
    using namespace boost;
    out << std::scientific << std::setprecision(12);
    auto gRadMed = radialMedCoords(disk.dispatch().grid().global());
    size_t const gnr = disk.dispatch().grid().global().sizes().nr;
    auto torque  = kk::create_mirror_view_and_copy(kk::HostSpace{}, torques.first);
    auto htorque = kk::create_mirror_view_and_copy(kk::HostSpace{}, torques.second);
    for (size_t i = 0; i < gnr; ++i) {
      out << gRadMed(i) << '\t' << torque(i) << '\t' << htorque(i) << '\n';
    }
  }
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract radial condributions to the torque of a planet.\n"
        << '\t' << cmd << " --disk <idisk>.h5 --planet <name> [--output <file>]: "
        << "print radial contributions to the torque of planet <name>.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("planet", po::value<std::string>(), "The name of the planet, will print for all planets if not specified.")
    ("output,o", po::value<std::string>(), "Output file, print on stdout by default.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  input_option.add("planet", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk" }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  Environment env(argc, argv);
  
  std::string ifname = vm["disk"].as<std::string>();
  auto disk = Disk::make(env.world(), HF::File(ifname, HF::File::ReadOnly).getGroup("/"));

  std::ofstream ofile;
  std::ostream& out = (bool(vm.count("output")) 
                       ? (ofile.open(vm["output"].as<std::string>()), ofile) 
                       : std::cout);

  if (bool(vm.count("planet"))) {
    std::string name = vm["planet"].as<std::string>();
    if (disk->system()->hasPlanet(name)) {
      write_torque_density(*disk, name, out);
    } else {
      std::cerr << "No planet '" << name << "' found.\n";
      return EX_USAGE;
    }
  } else {
    if (disk->system()) {
      for (auto const& np : disk->system()->planets()) {
        write_torque_density(*disk, np.first, out);
      }
    } else {
      std::cerr << "Not planetary system, nothing to extract.\n";
    }
  }

  return 0;
}
