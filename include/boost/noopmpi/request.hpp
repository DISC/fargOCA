// Copyright (C) 2006 Douglas Gregor <doug.gregor -at- gmail.com>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/** @file request.hpp
 *
 *  This header defines the class @c request, which contains a request
 *  for non-blocking communication.
 */
#ifndef BOOST_NOOPMPI_REQUEST_HPP
#define BOOST_NOOPMPI_REQUEST_HPP

#include <optional>

#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>
#include <boost/noopmpi/status.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/noopmpi/detail/no_return.hpp>

namespace boost { namespace noopmpi {

class status;

/**
 *  @brief A request for a non-blocking send or receive.
 *
 *  Cannot be instantiated in sequential mode.
 */
class BOOST_MPI_DECL request 
{
 public:
  request() { detail::no_return(); }

  /** @brief Cannot be called in sequential mode. */
  status wait();

  /** @brief Cannot be called in sequential mode. */
  std::nullopt_t test() { return std::nullopt; }

  /** @brief Cannot be called in sequential mode. */
  void cancel() { detail::no_return(); }
  
  /** @brief Cannot be called in sequential mode. */
 std::nullopt_t trivial() { return std::nullopt; }

  /** @brief Cannot be called in sequential mode. */
  bool active() const { return detail::no_return<bool>(); }

  /** @brief Cannot be called in sequential mode. */
  void preserve(boost::shared_ptr<void> /* d */) { detail::no_return(); }
};

} } // end namespace boost::noopmpi

#endif // BOOST_NOOPMPI_REQUEST_HPP
