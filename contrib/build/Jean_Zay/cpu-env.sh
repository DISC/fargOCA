# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

unset Kokkos_DIR

module purge

module load git/2.39.1 git-lfs/3.3.0
module load gcc/12.2.0
module load intel-oneapi-all/2023.1
module load hdf5/1.12.0
module loal cmake/3.25.2
module load ninja/1.10.0
module load boost/1.78.0-mpi

export CC=icx
export CXX=icpx
export FC=ifx
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
