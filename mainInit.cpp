// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <cassert>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include <filesystem>

#include <sysexits.h>

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/parsers.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "debug.hpp"
#include "disk.hpp"
#include "log.hpp"
#include "io.hpp"
#include "boundariesConditionsPlugin.hpp"
#include "version.hpp"
#include "hillForce.hpp"
#include "simulation.hpp"
#include "simulationTrackingPlugin.hpp"
#include "configProperties.hpp"
#include "simulationConfig.hpp"
#include "options.hpp"
#include "codeUnits.hpp"
#include "h5Labels.hpp"

using namespace fargOCA;
namespace fs      = std::filesystem;
namespace pt      = boost::property_tree;
namespace po      = boost::program_options;
namespace HF      = HighFive;
namespace opt     = fargOCA::options;
namespace cfg     = fargOCA::configProperties;
using namespace opt;
using namespace cfg;
template<typename T> using shared = std::shared_ptr<T>;

namespace {
  bool readCmdLine(int argc, char *argv[], std::ostream& log, po::variables_map& vm);
  std::optional<int> GetRestartNb(po::variables_map const& vm, std::ostream& log);
}

json
loadConfig(fmpi::communicator const& world, po::variables_map const& vm, std::ostream& log ) {
  fs::path input  = vm["config"].as<std::string>();
  if (!fs::exists(input)) {
    log << "Could not read configuration file '" << input << "', bye.\n";
    std::exit(EX_NOINPUT);
  }
  if (input.extension() == ".info") {
    return loadJsonFromPTree(world, input);
  } else if (input.extension() == ".json") {
    json config = json::parse(std::ifstream(input));
    if (!checkConfig(config, std::cerr)) {
      std::exit(EX_CONFIG);
    }
    return config;
  } else {
    log << "Unrecognized config file extension '" << input.extension() << '\n';
    std::exit(EX_CONFIG);
    return json{};
  }
}

std::optional<int>
buildStartPoint(fmpi::communicator const& world, po::variables_map const& vm, std::ostream& log ) {
  bool verbose           = bool(vm.count(opt::VERBOSE));
  std::optional<int> restartOutput = GetRestartNb(vm, log);
  std::string output     = vm["output"].as<std::string>();
  
  json config  = loadConfig(world, vm, log);
  int firstStep = restartOutput.value_or(0);

  auto physic = config.get<shared<DiskPhysic const>>();
  if (!physic->check(log)) {
    std::exit(EX_CONFIG);
  }

  shared<GridDispatch const> grid   = GridDispatch::make(world, *Grid::make(physic->shape, config.at(snake(DISK)).at(snake(GRID))));
  shared<Disk>               disk   = Disk::make(*physic, *grid);
  
  if (restartOutput) {
    disk->addPhysicalTime(vm["restart-time"].as<real>());
  }

  /* Only disk velocities remain to be initialized */
  world.barrier();
  if (restartOutput) {
    using namespace boost;
    disk->readH5(str(format("%1%/disk%2%.h5") %output % *restartOutput));
  }
  
  if (vm.count(GAS_FROM)) {
    std::string gfname = vm[GAS_FROM].as<std::string>();
    HF::File gfile(gfname, HF::File::ReadOnly);
    shared<Disk> gas = Disk::make(world, gfile.getGroup("/"));
    Grid const& gascoords  = gas->dispatch().grid().global();
    Grid const& diskcoords = disk->dispatch().grid().global();
    std::optional<GasShape> diff = compatible(gascoords, diskcoords);
    if (!diff) {
      log << "Cannot import gas from file '" << gfname << "': incompatible grids.\n";
      std::exit(EX_DATAERR);
    } else if (max(*diff) > std::numeric_limits<real>::epsilon()) {       
      log << "Warning: importing gas from a disk with different shape: "
          << gascoords.shape() 
          << "\nVS\n"
          << diskcoords.shape()
          << '\n';
    }
    disk->importGas(*gas);
  }
  auto tracking = config.at(snake(TRACKERS)).get<PluginConfig>();
  int  nbSteps  = config.at(snake(NB_STEP)).get<int>();
  real timeStep = config.at(snake(TIME_STEP)).get<real>();
  auto simulation{Simulation::make(*disk, tracking, nbSteps, timeStep, output, firstStep)};
  {
    std::string ofile = str(boost::format("%1%/disk%2%.h5") %output % simulation->step());
    std::optional<HF::File>  file(world.rank() == 0 ? std::optional<HF::File>(std::in_place, ofile, HF::File::Overwrite) : std::nullopt);
    std::optional<HF::Group> root(bool(file) ? std::optional<HF::Group>(file->getGroup("/")) : std::nullopt);
    simulation->writeH5(root);
    if (root) {
      h5::FormatVersion::current().write(*root);
    }
  }
  if (verbose) {
    simulation->dump(log);
  }
  return firstStep;
}

int
main(int argc,  char *argv[])
{
  Environment env(argc, argv);
  fmpi::communicator const& world = env.world();
  
  std::ostream& log    = fargOCA::log(world, std::cout);
  std::ostream& errlog = fargOCA::log(world, std::cerr);  
  po::variables_map vm;
  if (readCmdLine(argc, argv, log, vm)) {
    bool verbose = bool(vm.count(opt::VERBOSE));
    bool version = bool(vm.count("version"));
    if (verbose||version) {
      log << "Source code id: " << source_version() << '\n'
          << "Branch: " << source_branch() << '\n';
      if (version) {
        return EX_USAGE;
      }
    }
    if (bool(vm.count("uninit-temp-fields-to-NaN"))) {
      FieldStack<void>::setUninitialized2NaN(true);
    }
    std::optional<int> firstOutput = buildStartPoint(world, vm, errlog);
    if (firstOutput) {
      std::string dirname = vm["output"].as<std::string>();
      log << "Input data 'disk" << *firstOutput << ".h5' has been prepared in directory '" 
          << dirname << "'.\n"
          << "You can launch a simulation by running:\n"
          << "\t [mpi command] fargOCA " << dirname 
          << "\n";
      return 0;
    } else {
      return EX_NOINPUT;
    }
  } else {
    return EX_USAGE;
  }
}

namespace {
  bool
  readCmdLine(int argc, char *argv[], std::ostream& log, po::variables_map& vm) {
    using options::str;
    std::ostringstream usage;
    usage << "usage: " << argv[0] << " <odir> <config> [options]+\n"
          << "Write in <odir> the initial 'disk0.h5' file based on the configuration file <config>.\n"
          << "More documentation can be found at 'https://gitlab.oca.eu/DISC/fargOCA/wikis'.";    
    po::options_description visible(usage.str());
    std::ostringstream configDefaults;
    configDefaults << "Behavior w.r.t. default configuration properties. Possible values are\n"
		   << CD_DISABLE << ": default values are not authorized, all needed properties must be specified.\n"
      		   << CD_ENABLE << ": default values are authorized and silently used.\n"
      		   << CD_REPORT  << ": default values are authorized but missing values will be reported.";
    visible.add_options()
      (str(HELP_H), "Produce this help message.")
      (str(VERBOSE_V), "Display configuration and stuff.")
      (str(CONFIG_DEFAULTS), po::value<std::string>()->default_value(CD_REPORT), configDefaults.str().c_str())
      (str(VERSION), "Display version and exit.")
      ("uninit-temp-fields-to-NaN", "Set uninitialized temporari fields to NaN (or max value).")
      ("profile,p", "Print time steps durations.")
      (str(GAS_FROM), po::value<std::string>(), "Get gas from the specified hdf5 disk file, which must be matched to the same grid.")
      ("restart-gas,r",
       po::value<int>(),
       "Restart only gas. Useful when gas needs to reach an equilibrium before adding planets."
       " Takes #'number' files as initial conditions.")
      ("restart,s",
       po::value<int>(),
       "Restart simulation, taking #'number' files as initial conditions.")
      ("restart-time", po::value<real>()->default_value(0),
       "If restarting, force the start time to this value.")
      ;
    po::positional_options_description positional;
    positional.add("output", 1);
    positional.add("config", 2);
    po::options_description hidden;
    hidden.add_options()
      ("output,o", po::value<std::string>(), "Output directory, default is '.'.")
      ("config,c", po::value<std::string>(), "The configuration file.");
    
    po::options_description all;
    all.add(visible).add(hidden);
    all.add(Environment::cmdLineOptions());
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(all).positional(positional).run();
    po::store(parsed, vm);
    po::notify(vm);
    if (bool(vm.count(HELP))) {
      log << visible << '\n';
      return false;
    }
    if (bool(vm.count(VERSION))) {
      log << "Source code id: " << source_version() << '\n'
          << "Branch: " << source_branch() << '\n';
      return false;
    }
    if (!bool(vm.count("output"))) {
      log << "Must specify an output directory.\n"
          << visible << '\n';
      return false;
    } else {
      std::string odir = vm["output"].as<std::string>();
      if (!fs::exists(odir)) {
        log << "Output dir '" << odir << "' does not exists.\n";
        return false;
      } else if (!fs::is_directory(odir)) {
        log << "File '" << odir << "' is not a directory.\n";
        return false;
      }
    }
    return true;
  }  
  
  std::optional<int>
  GetRestartNb(po::variables_map const& vm, std::ostream& log) {
    std::optional<int> nb;
    if (bool(vm.count("restart")) && bool(vm.count("restart-gas"))) {
      log << "Cannot specify both '--restart' and '--restart-gas'. Pick one and relaunch.\n";
      std::abort();
      nb = std::nullopt;
    } else if (vm.count("restart")) {
      nb = vm["restart"].as<int>();
    } else if (vm.count("restart-gas")) {
      nb = vm["restart-gas"].as<int>();
    } else {
      nb = std::nullopt;
    }
    return nb;
  }
}
