// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_ARRAYS_TEST_UTILS_H
#define FARGOCA_ARRAYS_TEST_UTILS_H

#include <iostream>
#include <cmath>
#include <type_traits>
#include <optional>
#include <forward_list>

#include <Kokkos_Core.hpp>

#include "arrays.hpp"
#include "loops.hpp"
#include "arraysUtils.hpp"
#include "precision.hpp"

namespace fargOCA {
 
  template <typename V1, typename V2>
  real checkDims(V1 a1, V2 a2) {
    static_assert(int(a1.rank) == int(a2.rank), "Views must have de same rank");
    for(int i = 0; i < a1.rank; ++i) {
      if (a1.extent(i) != a2.extent(i)) {
        std::cerr << "Dimension mismatch at extent " << i << " between " 
                  << a1.label() << " and " << a2.label() << ": "
                  << a1.extent(i) << " vs " << a2.extent(i) << '\n';
        return false;
      }
    }
    return true;
  }

  template <typename T>
  real average(T const* p1, T const* p2, int sz) {
    T sum = T();
    for( int i = 0; i < sz; ++i) {
      sum += std::abs(p1[i]) + std::abs(p2[i]);
    }
    
    return sum/(2*sz);
  }

  template <typename T>
  typename Kokkos::View<T>::non_const_value_type
  average(Kokkos::View<T> a) {
    typedef typename Kokkos::View<T>::non_const_value_type V;
    using Kokkos::abs;
    V sum = 0;
    Kokkos::parallel_reduce("sum", a.size(),
                            KOKKOS_LAMBDA(int l, V& tmp) {
                              tmp += abs(a.data()[l]);
                            }, sum);
    return sum/a.size();
  }
  
  template <typename T1, typename T2>
  typename Kokkos::View<T1>::non_const_value_type
  average(Kokkos::View<T1> a1, Kokkos::View<T2> a2) {
    using Kokkos::abs;
    if (!checkDims(a1, a2)) { 
      std::abort();
    }
    typedef typename Kokkos::View<T1>::non_const_value_type V;
    V sum = 0;
    Kokkos::parallel_reduce("sum", a1.size(),
                            KOKKOS_LAMBDA(int l, V& tmp) {
                              tmp += abs(a1.data()[l]) + abs(a2.data()[l]);
                            }, sum);
    return sum/a1.size();
  }

  /// \brief Return the maximum absolute difference between to real arrays.
  /// \returns The max diff or std::nullopt if a NaN is present.
  std::optional<real> maxRelDiff(arr1d<real const> a1, arr1d<real const> a2);
  
  template <typename T1, typename T2>
  std::optional<std::enable_if_t<std::is_same_v<typename Kokkos::View<T1>::non_const_value_type, real>,real>>
  maxRelDiff(Kokkos::View<T1> a1, Kokkos::View<T2> a2) {
    if (!checkDims(a1, a2)) { 
      std::abort();
    }
    typedef typename Kokkos::View<T1>::non_const_value_type V1;
    typedef typename Kokkos::View<T2>::non_const_value_type V2;
    static_assert(std::is_same<V1,V2>::value);
    return maxRelDiff(arr1d<real const>(a1.data(), a1.size()), arr1d<real const>(a2.data(), a2.size()));
  }
  
  class GridDispatch;

  /// \brief Store the absolute and relative difference between two values
  template<typename T>
  struct AbsRelDiff {
    T absolute;
    T relative;
    
    KOKKOS_FORCEINLINE_FUNCTION
    bool operator>(AbsRelDiff const& v) const {
      return (absolute > v.absolute
              || (absolute == v.absolute && relative > v.relative));
    }
    KOKKOS_FORCEINLINE_FUNCTION
    bool operator==(AbsRelDiff const& v) const {
      return absolute == v.absolute && relative == v.relative;
    }
  };
  template<std::size_t N>
  using AbsRelDiffCoords = Kokkos::Array<std::size_t, N>;

  template <typename T, std::size_t N>
  using AbsRelDiffLocScalar  = Kokkos::ValLocScalar<AbsRelDiff<std::remove_cv_t<T>>, AbsRelDiffCoords<N>>;
  
  template<typename T>
  std::ostream&
  operator<<(std::ostream& out, AbsRelDiff<T> const& d) {
    out << "{abs:" << d.absolute << ",rel:" << d.relative << '}';
    return out;
  }

  template<typename T, std::size_t N>
  std::string to_string(AbsRelDiffLocScalar<T,N> d) {
    std::ostringstream out;
    out << '{' << d.val << ",{";
    for (int i=0; i<N; ++i) {
      out << d.loc[i] << (i==N-1 ? '}' : ',');
    }
    out << '}';
    return out.str();
  }
  
  template<typename T, std::size_t N>
  bool
  checkAbsRelDiff(std::optional<AbsRelDiffLocScalar<T,N>> diff, T atol, T rtol, std::ostream& log) {
    if (!diff) {
      log << "Array dimension mismatch\n";
      return false;
    } else {
      log << "localized max diff: " << to_string<T,N>(*diff);
      if (diff->val.absolute > atol) {
        log << " absolute tol. > " << atol;
        if (diff->val.relative > rtol) {
          log << " and relative tol. > " << rtol;
          return false;
        }
      }
    }
    return true;
  }
}

namespace Kokkos { //reduction identity must be defined in Kokkos namespace
  template<typename T>
  struct reduction_identity<fargOCA::AbsRelDiff<T>> {
    /// \brief Max diff are positive, so inits value is {0,0}
    KOKKOS_FORCEINLINE_FUNCTION static fargOCA::AbsRelDiff<T> max() {
      using limits = std::numeric_limits<T>;
      return fargOCA::AbsRelDiff<T>{T{0}, T{0}};
    }
  };

  template<std::size_t N>
  struct reduction_identity<Kokkos::Array<std::size_t, N>> {
    KOKKOS_FORCEINLINE_FUNCTION
    static auto cst(std::size_t v) {
      Kokkos::Array<std::size_t,N> values;
      for(std::size_t i = 0; i < N; ++i) { values[i] = v; }
      return values;
    }
      
    KOKKOS_FORCEINLINE_FUNCTION static auto min() { return cst(Kokkos::Experimental::finite_max_v<std::size_t>); }
    KOKKOS_FORCEINLINE_FUNCTION static auto max() { return cst(Kokkos::Experimental::finite_min_v<std::size_t>); }
  };
  
}

namespace fargOCA {
  // We need to find a trade off between code duplication and readability.

  template <typename T, std::size_t N>
  using AbsRelDiffLocReducer = Kokkos::MaxLoc<AbsRelDiff<std::remove_cv_t<T>>, Kokkos::Array<std::size_t,N>>;

  template<typename T1, typename ExpectedFct>
  std::enable_if_t<std::is_invocable_r_v<T1, ExpectedFct, std::size_t>, AbsRelDiffLocScalar<T1,1>>
  maxAbsRelDiff(arr1d<T1> a1, ExpectedFct f) {
    namespace kk = Kokkos;
    using std::size_t;
    using ArrVal        = std::remove_cv_t<T1>;
    using Reducer       = AbsRelDiffLocReducer<ArrVal, 1>;
    using LocalizedDiff = Reducer::value_type;
    static_assert(std::is_same_v<AbsRelDiffLocScalar<ArrVal,1>, LocalizedDiff>);
    constexpr ArrVal ValMax { kk::Experimental::finite_max_v<ArrVal> };
    
    LocalizedDiff localizedDiff;
    kk::parallel_reduce("reduce", range(a1),
                        KOKKOS_LAMBDA(size_t i, LocalizedDiff& p) {
                          ArrVal const absolute {std::abs(a1(i) - f(i))};
                          ArrVal c = p.val.absolute;
                          if (absolute > p.val.absolute) {
                            ArrVal const relative { a1(i) != 0 ? absolute/std::abs(a1(i)) : ValMax };
                            p.val = {absolute, relative};
                            p.loc = {i};
                          }
                        }, Reducer(localizedDiff));
    return localizedDiff;    
  }

  template<typename T1, typename T2>
  std::optional<AbsRelDiffLocScalar<T1,1>>
  maxAbsRelDiff(arr1d<T1> a1, arr1d<T2> a2) {
    static_assert(std::is_same_v<std::remove_cv_t<T1>, std::remove_cv_t<T2>>);
    if (!checkDims(a1, a2)) {
      return std::nullopt;
    } else {
      return maxAbsRelDiff(a1, KOKKOS_LAMBDA(std::size_t i) -> std::remove_cv_t<T1> { return a2(i); });
    }
  }

  template<typename T1, typename ExpectedFct>
  std::enable_if_t<std::is_invocable_r_v<T1, ExpectedFct, std::size_t, std::size_t>, AbsRelDiffLocScalar<T1,2>>
  maxAbsRelDiff(arr2d<T1> a1, ExpectedFct f) {
    namespace kk = Kokkos;
    using std::size_t;
    using ArrVal        = std::remove_cv_t<T1>;
    using Reducer       = AbsRelDiffLocReducer<ArrVal, 2>;
    using LocalizedDiff = Reducer::value_type;
    static_assert(std::is_same_v<AbsRelDiffLocScalar<ArrVal,2>, LocalizedDiff>);
    constexpr ArrVal ValMax { kk::Experimental::finite_max_v<ArrVal> };
    
    LocalizedDiff localizedDiff;
    kk::parallel_reduce("reduce", range(a1),
                        KOKKOS_LAMBDA(size_t i, size_t j,  LocalizedDiff& p) {
                          ArrVal const absolute {std::abs(a1(i,j) - f(i,j))};
                          if (absolute > p.val.absolute) {
                            ArrVal const relative { a1(i,j) != 0 ? absolute/std::abs(a1(i,j)) : ValMax };
                            p.val = {absolute, relative};
                            p.loc = {i,j};
                          }
                        }, Reducer(localizedDiff));
    return localizedDiff;
  }

  template<typename T1, typename T2>
  std::optional<AbsRelDiffLocScalar<T1,2>>
  maxAbsRelDiff(arr2d<T1> a1, arr2d<T2> a2) {
    static_assert(std::is_same_v<std::remove_cv_t<T1>, std::remove_cv_t<T2>>);
    if (!checkDims(a1, a2)) {
      return std::nullopt;
    } else {
      using std::size_t;
      return maxAbsRelDiff(a1, KOKKOS_LAMBDA(size_t i, size_t j) -> std::remove_cv_t<T1> { return a2(i,j); });
    }
  }

  template<typename T1, typename ExpectedFct>
  std::enable_if_t<std::is_invocable_r_v<T1, ExpectedFct, std::size_t, std::size_t, std::size_t>, AbsRelDiffLocScalar<T1,3>>
  maxAbsRelDiff(arr3d<T1> a1, ExpectedFct f) {
    namespace kk = Kokkos;
    using std::size_t;
    using ArrVal        = std::remove_cv_t<T1>;
    using Reducer       = AbsRelDiffLocReducer<ArrVal, 3>;
    using LocalizedDiff = Reducer::value_type;
    static_assert(std::is_same_v<AbsRelDiffLocScalar<ArrVal,3>, LocalizedDiff>);
    constexpr ArrVal ValMax { kk::Experimental::finite_max_v<ArrVal> };
    
    LocalizedDiff localizedDiff;
    kk::parallel_reduce("reduce", range(a1),
                        KOKKOS_LAMBDA(size_t i, size_t h, size_t j,  LocalizedDiff& p) {
                          ArrVal const absolute {std::abs(a1(i,h,j) - f(i,h,j))};
                          if (absolute > p.val.absolute) {
                            ArrVal const relative { a1(i,h,j) != 0 ? absolute/std::abs(a1(i,h,j)) : ValMax };
                            p.val = {absolute, relative};
                            p.loc = {i,h,j};
                          }
                        }, Reducer(localizedDiff));
    return localizedDiff;
  }

  template<typename T1, typename T2>
  std::optional<AbsRelDiffLocScalar<T1,3>>
  maxAbsRelDiff(arr3d<T1> a1, arr3d<T2> a2) {
    static_assert(std::is_same_v<std::remove_cv_t<T1>, std::remove_cv_t<T2>>);
    if (!checkDims(a1, a2)) {
      return std::nullopt;
    } else {
      using std::size_t;
      return maxAbsRelDiff(a1, KOKKOS_LAMBDA(size_t i, size_t h, size_t j) -> std::remove_cv_t<T1> { return a2(i,h,j); });
    }
  }

}
  
#endif
