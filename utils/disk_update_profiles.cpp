// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <sstream>
#include <filesystem>

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/program_options.hpp>

#include <highfive/H5File.hpp>

#include "environment.hpp"
#include "allmpi.hpp"

#include "disk.hpp"
#include "diskProfiles.hpp"
#include "loops.hpp"
#include "arraysUtils.hpp"

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace kk = Kokkos;
namespace HF = HighFive;

using namespace fargOCA;

bool
parse_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  usage << "Usage: " << argv[0]  << " <disk file> [opt]+\n"
	<< "Update the profiles of <disk file>. "
	<< "Possible options are";
  po::options_description visible(usage.str());
  visible.add_options()
    ("help", "this help message.")
    ("init", "reset the profiles to initial values.")
    ("average", po::value<std::string>(), "reset profiles to the average gas values of the provided disk.");
  po::options_description hidden("Hidden options");
  hidden.add_options()
    ("disk-file", "the disk those profiles are to be updated");
  po::positional_options_description input_option;
  input_option.add("disk-file", 1);
  try {
    po::options_description all;
    all.add(visible).add(hidden);
    po::parsed_options parsed = po::command_line_parser(argc, argv)
      .options(all)
      .positional(input_option).run();
    po::store(parsed, vm);
    po::notify(vm);
    if (vm.count("help") > 0) {
      std::cout << visible;
      return false;
    }
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    return false;
  }
  if (vm.count("init")+vm.count("average") == 0) {
    std::cerr << "either --init or --average <from disk file> should be used.\n";
    std::cout << visible;
    return false;
  }
  return true;
}

template<GridSpacing GS>
void
update_disk_profile(Disk& disk, Disk const& profiles, HF::Group igroup) {
  GridDispatch const& dispatch = disk.dispatch();
  auto const& coords = dispatch.grid().template as<GS>();
  GridSizes const ls = coords.sizes();

  auto radMed = coords.radiiMed();
  auto phiMed = coords.phiMed();
  
  arr2d<real const> vradial = averageOnTheta(profiles.velocity().radial().data());
  arr2d<real const> vphi    = averageOnTheta(profiles.velocity().phi().data());
  arr2d<real>       vtheta  = averageOnTheta(profiles.velocity().theta().data());
  // write the azimuthal velocity profile in nonrotating frame
  real const frameVelocity = disk.frameVelocity();
  kk::parallel_for("vtheta", range2D({ls.nr, ls.ni}),
		   KOKKOS_LAMBDA(size_t const i, size_t const h) {
		     vtheta(i,h) += frameVelocity*radMed(i)*sin(phiMed(h));
		   });
  arr2d<real const>  density = averageOnTheta(profiles.density().data());
  std::optional<arr2d<real const>> energy  = (bool(profiles.energy())
					      ? std::make_optional(averageOnTheta(profiles.energy()->data()))
					      : std::nullopt);
  disk.flashProfiles(DiskProfiles(disk.dispatch(),
				  vradial, vphi, vtheta,
				  density,
				  energy));
  disk.writeH5(igroup);
}

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  po::variables_map vm;
  if (!parse_cmd_line(argc, argv, vm)) {
    return -1;
  }
  std::string disk_name = vm["disk-file"].as<std::string>();
  if (vm.count("init") > 0) {
    HF::File ifile(disk_name, HF::File::ReadWrite);
    HF::Group igroup = ifile.getGroup("/");
    auto disk = Disk::make(world, igroup);
    disk->flashProfiles();
    disk->writeH5(igroup);
    return 0;
  } else if (vm.count("average") > 0) {
    std::string from_disk_filename = vm["average"].as<std::string>();
    auto from_disk = Disk::make(world, HF::File(from_disk_filename, HF::File::ReadOnly).getGroup("/"));
    HF::File ifile(disk_name, HF::File::ReadWrite);
    HF::Group igroup = ifile.getGroup("/");
    auto disk = Disk::make(world, igroup);
    if (!compatible(from_disk->dispatch().grid().global(), disk->dispatch().grid().global(), 10e-12)) {
      std::cerr << "grids not close enough, rebind first ?\n";
      return -1;
    }
    
    GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
    GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;
    switch(disk->dispatch().grid().radialSpacing()) {
    case ARTH: update_disk_profile<ARTH>(*disk, *from_disk, igroup); break;
    case LOGR: update_disk_profile<LOGR>(*disk, *from_disk, igroup); break;
    }
    
    return 0;
  }
}
