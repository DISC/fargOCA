// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_MATH_ADD_ONS_H
#define FARGOCA_MATH_ADD_ONS_H

#include "Kokkos_MathematicalFunctions.hpp"
#include "Kokkos_Core.hpp"

namespace fargOCA {
  /// \brief Powers for integer exponents
  template<int P> 
  KOKKOS_INLINE_FUNCTION
  real ipow(real r) {
    using Kokkos::pow;
    return pow(r,P);
  }

  template<> KOKKOS_INLINE_FUNCTION real ipow<2>(real r) { return r*r; }
  template<> KOKKOS_INLINE_FUNCTION real ipow<3>(real r) { return r*r*r; }
  template<> KOKKOS_INLINE_FUNCTION real ipow<4>(real r) { return ipow<2>(ipow<2>(r)); }
  template<> KOKKOS_INLINE_FUNCTION real ipow<5>(real r) { return ipow<4>(r)*r; }
  template<> KOKKOS_INLINE_FUNCTION real ipow<6>(real r) { return ipow<3>(ipow<2>(r)); }
  template<> KOKKOS_INLINE_FUNCTION real ipow<7>(real r) { return ipow<6>(r)*r; }

  /// \brief Config of the exponential function.
  template<typename T>
  struct ExpProps {
    static constexpr T MaxParam = 709.8;
  };

  template<>
  struct ExpProps<double> {
    static constexpr double MaxParam = 709.8;
  };

  template<typename T>
  inline constexpr T expMaxParam = ExpProps<T>::MaxParam;
}

#endif
