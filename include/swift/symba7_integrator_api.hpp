// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_SYMBA7_INTEGRATOR_API_H
#define FARGOCA_SYMBA7_INTEGRATOR_API_H

#include "tuple.hpp"
#include "planetarySystem.hpp"

extern "C"{ 
  /// \brief A thin wrapper "class" around the F90 integrator
  /// This struct must remain layout compatible this its FORTRAN counterpart
  /// in src/symba7/symba7_integrator.f90.
  struct s7_integrator {
    double  time;
    int     nbodies;
    int     n_massive_bodies;
    int     last_nbodies;
    int     take_referential_from_scratch;
    double  j2rp2;
    double  j4rp4;
    double* mass;
    double* xh, *yh, *zh;
    double* vxh, *vyh, *vzh;
    double* rpl;
    double* rhill;
    int16_t pclose; // merge close particles, 2 byte is for extra precision I guees...
    double  rmin, rmax, rmaxu, qmin;
    double  tiny;
    
    s7_integrator(int nb, double t0, int16_t merge, double small);
    ~s7_integrator();    

    void set_boundaries(double rmin, double rmax, double rmaxu, double qmin);     
    void set_planet(int i,
                    double m, 
                    double px, double py, double pz,
                    double vx, double vy, double vz,
                    double r, double rh);
    fargOCA::Planet  get_planet(int i) const;
    int get_nb_bodies() const;
    double get_time() const;

    void step(double dt);
    void write()  const;
  };
}

#endif 
