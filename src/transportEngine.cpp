// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cassert>
#include <numeric>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "allmpi.hpp"

#include "transportEngine.hpp"
#include "log.hpp"
#include "codeUnits.hpp"
#include "loops.hpp"
#include "scalarField.hpp"
#include "disk.hpp"
#include "gridCellSizes.hpp"
#include "fieldStack.hpp"
#include "arraysUtils.hpp"

namespace kk  = Kokkos;
namespace kkm = Kokkos::Experimental;
#define LBD KOKKOS_LAMBDA
#ifdef NDEBUG
#  define NLBD [&]
#else
#  define NLBD [=]
#endif

namespace fargOCA {

  using kk::pow;

  namespace {

    struct p3 { real prv,mid,nxt; };
    
    template<typename T>
    struct Momenta final {
      vec3d<T> inf, sup;
      Momenta(vec3d<T> i, vec3d<T> s) : inf(i), sup(s) {}      
      template<typename M,
               std::enable_if_t<std::is_const_v<T> && std::is_same_v<std::remove_cv_t<T>, M>,bool> = true>
      Momenta(Momenta<M> const& m) : inf(m.inf), sup(m.sup) {}
    };
    
    template<GridSpacing GS>
    struct Impl {
      GridDispatch const& dispatch;
      DiskPhysic   const& physic;
      Coords<GS>   const& coords;
      real                referentialRotationSpeed;

      Impl(GridDispatch const& d,
           DiskPhysic   const& p,
           real                omega)
        : dispatch(d),
          physic(p),
          referentialRotationSpeed(omega),
          coords(dispatch.grid().as<GS>()) {}
      Impl(Disk const& disk)
        : Impl(disk.dispatch(), disk.physic(), disk.frameVelocity()) {}

      // Cray/Clang bug
      // RadMedTp should be replaced with
      // decltype(std::declval<Impl<GS>>().coords.radiiMed())
      // check https://github.com/llvm/llvm-project/issues/115547
      template<class Normalized, typename RadMedType>
      KOKKOS_INLINE_FUNCTION
      static
      real transmitedFractionRadial(RadMedType radMed,
                                    Normalized qb,
                                    arr3d<real const> vr,
                                    real const dt,
                                    size_t const i, size_t const h, size_t const j) {
        static_assert(std::is_invocable_r_v<real,decltype(qb), size_t, size_t,size_t>);
        GridSizes const ls {sizes(vr)};
        if (i == 0 || i == ls.nr-1) {
          return 0;
        } else{
          auto dq = [&](size_t const ii) -> real {
            if (ii == 0 || ii == ls.nr-1) {
              return 0;
            } else{
              p3 rad { radMed(ii-1), radMed(ii), radMed(ii+1) };
              p3 nrm { qb(ii-1,h,j), qb(ii,h,j), qb(ii+1,h,j) };
              real const dqm = (nrm.mid - nrm.prv)/(rad.mid - rad.prv);
              real const dqp = (nrm.nxt - nrm.mid)/(rad.nxt - rad.mid);
              return dqp * dqm > 0 ? dqp*dqm/(dqp+dqm) : 0;
            }
          };
          
          return (vr(i,h,j) > 0
                  ? qb(i-1,h,j)+(radMed(i)  -radMed(i-1)-vr(i,h,j)*dt)*dq(i-1)
                  : qb(i,h,j)  -(radMed(i+1)-radMed(i)  +vr(i,h,j)*dt)*dq(i));
        }
      }

      template<class Normalized>
      KOKKOS_INLINE_FUNCTION
      static
      real
      transmitedFractionTheta(arr3d<real const> dtheta,
                              Normalized qb,
                              arr3d<real const> vt,
                              real const dt,
                              size_t const i, size_t const h, size_t const j) {
        static_assert(std::is_invocable_r_v<real,decltype(qb), size_t, size_t,size_t>);
        GridSizes const ls { sizes(vt) };
        auto dq = [&](size_t const jj) -> real {
          size_t const jp  = nsnext(ls.ns, jj);
          size_t const jm  = nsprev(ls.ns, jj);
          real const ddm = qb(i,h,jj )-qb(i,h,jm);
          real const ddp = qb(i,h,jp)-qb(i,h,jj );
          return ddm * ddp > 0 ? (ddp*ddm/(ddp+ddm))/dtheta(i,h,jj) : 0;
        };                           

        size_t const jm  = nsprev(ls.ns, j);
        return (vt(i,h,j) > 0
                ? qb(i,h,jm)+(dtheta(i,h,jm)-dt*vt(i,h,j))*dq(jm)
                : qb(i,h,j )-(dtheta(i,h,j )+dt*vt(i,h,j))*dq(j));
      }
      // Cray/Clang bug
      // RadMedTp and PhiMedTypeshould be replaced with
      // decltype(std::declval<Impl<GS>>().coords.radiiMed())
      // decltype(std::declval<Impl<GS>>().coords.phiMed()) phiMed,
      // check https://github.com/llvm/llvm-project/issues/115547
      template<class Normalized, typename RadMedType, typename PhiMedType>
      KOKKOS_INLINE_FUNCTION
      static 
      real
      transmitedFractionPhi(RadMedType radMed,
                            PhiMedType phiMed,
                            Normalized qb,
                            arr3d<real const> vp,
                            real const dt,
                            size_t const i, size_t const h, size_t const j) {
        static_assert(std::is_invocable_r_v<real,decltype(qb), size_t, size_t,size_t>);
        GridSizes const lz {sizes(vp)};
        if( h == 0 || h == lz.ni-1) {
          return 0;
        } else {
          auto dphi {[&](size_t const hh) { return radMed(i)*(phiMed(hh+1)-phiMed(hh));} };
          auto dq   {[&](size_t const hh) -> real {
            if(hh == 0 || hh == lz.ni-1)  {
              return 0;
            } else {  
              real const dqm {(qb(i,hh,j)-qb(i,hh-1,j)) / dphi(h)};
              real const dqp {(qb(i,hh+1,j)-qb(i,hh,j)) / dphi(h+1)};
              return dqp * dqm > 0 ? dqp*dqm/(dqp+dqm) : 0;
            }
          }};
          return (vp(i,h,j) > 0 
                  ? qb(i,h-1,j)+(dphi(h-1)-dt*vp(i,h,j))*dq(h-1)
                  : qb(i,h,j)  -(dphi(h)  +dt*vp(i,h,j))*dq(h));
        }
      }

      void
      getTransmitedFractionRadial(arr3d<real const> qb, arr3d<real const> vr, const real dt, arr3d<real> qs) const {
        auto radMed {coords.radiiMed()};
        kk::parallel_for("transport qs body", fullRange(coords),
                         LBD(size_t i, size_t h, size_t j) { qs(i,h,j) = transmitedFractionRadial(radMed, qb, vr, dt, i,h,j); });
      }

      void
      getTransmitedFractionTheta(arr3d<real const> qb, arr3d<real const> vt, real dt, arr3d<real> qstar) const {
        arr3d<real const> dtheta  {dispatch.cellSizes().dTheta()};
        kk::parallel_for("theta_fraction_util", fullRange(coords),
                         LBD(size_t i, size_t h, size_t j) { qstar(i,h,j) = transmitedFractionTheta(dtheta, qb, vt, dt, i,h,j); });
      }
      
      void
      getTransmitedFractionPhi(arr3d<real const> qb, arr3d<real const> vp, real dt, arr3d<real> qstar) const {
        auto radMed {coords.radiiMed()};
        auto phiMed {coords.phiMed()};
        kk::parallel_for("phi_fraction_util", fullRange(coords),
                         LBD(size_t i, size_t h, size_t j) { qstar(i,h,j) = transmitedFractionPhi(radMed, phiMed, qb, vp, dt, i,h,j); });
      }
      
      // This version is specialized for density. Meaning the normalized value is 1
      void
      VanLeerRadial(arr3d<real const>  rhos,
                    arr3d<real const>  vr,
                    arr3d<real> density,
                    real dt) const
      {
        size_t const nr {coords.sizes().nr};
        auto volume {dispatch.cellSizes().volume()};
        auto radii  {coords.radii()};
	auto phi    {coords.phi()};
        auto theta  {coords.theta()};
	auto dtheta {coords.dTheta()};
	
	auto const qrstar {1};
	int  const dim {coords.dim()};
	bool const flat{dim==2};
	kk::parallel_for("transport_van_leer_rad3d", fullRange(coords),
			 LBD(size_t const i, size_t const h, size_t const j) {
			   if (i < nr-1) {
			     real const surfr {flat ? dtheta(j) : (std::cos(phi(h))-std::cos(phi(h+1)))*(dtheta(j))};
			     auto transmited {[&](size_t ii) { return qrstar*pow(radii(ii),dim-1)*rhos(ii,h,j)*vr(ii,h,j);} };
			     density(i,h,j) += dt*surfr*(transmited(i)-transmited(i+1))/volume(i,h,j);
			   }
			 });
      }
      
      void
      VanLeerRadial(arr3d<real const>  density,
                    arr3d<real const>  rhos,
                    arr3d<real const>  vr,
                    arr3d<real> quantity,
                    real dt) const
      {
        GridSizes const ls{coords.sizes()};
        arr3d<real const> volume {dispatch.cellSizes().volume()};
        auto radiiMed {coords.radiiMed()};
        auto radii    {coords.radii()};
        auto phi      {coords.phi()};
        auto theta    {coords.theta()};
	auto dtheta   {coords.dTheta()};

        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        {// qstar uses quantity, which is used in next kernel. So we use an intermedate arrray
          arr3d<real> qrstar {locals.get<0>()};
          kk::parallel_for("van_leer_rad_normalized", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             auto normalized {[&](size_t const i, size_t const h, size_t const j) { return quantity(i,h,j)/density(i,h,j);} };
                             qrstar(i,h,j) = transmitedFractionRadial(radiiMed, normalized, vr, dt, i,h,j); 
                           });
        }
        arr3d<real const> qrstar{locals.get<0>()};
	int  const dim {coords.dim()};
	bool const flat{dim==2};
	size_t const nr {ls.nr};
	kk::parallel_for("transport_van_leer_rad3d", fullRange(ls),
			 LBD(size_t const i, size_t const h, size_t const j) {
			   if (i < nr-1) {
			     real const  surfr {flat? dtheta(j) : (cos(phi(h))-cos(phi(h+1))) * (dtheta(j))};
			     auto transmited {[&](size_t const i) { return pow(radii(i),dim-1)*qrstar(i,h,j)*rhos(i,h,j)*vr(i,h,j);} };
			     quantity(i,h,j) += dt*surfr*(transmited(i)-transmited(i+1))/volume(i,h,j);
			   }
			 });
      }
      
      void
      VanLeerTheta(arr3d<real const>  rhos,
                   arr3d<real const>  vt,
                   arr3d<real> quantity,
                   real dt) const {
        GridSizes const ls = coords.sizes();

        auto volume  {dispatch.cellSizes().volume()};
        auto radii   {coords.radii()};
        auto dradii  {coords.dRadii()};	
        auto phi     {coords.phi()};
        auto dphi    {coords.dPhi()};

	int  const dim  {coords.dim()};
	bool const flat {dim == 2};
	auto const qrstar{1};
	size_t const ns {ls.ns};
	kk::parallel_for("transport_van_leer_theta3d",
			 fullRange(coords),
			 LBD(size_t const i, size_t const h, size_t const j) {
			   real const surfr  {flat ? dradii(i) : (pow(radii(i+1),dim-1)-pow(radii(i),dim-1)) * dphi(h)/2};
			   auto transmited {[&](size_t jj) { return qrstar*rhos(i,h,jj)*vt(i,h,jj);} };
			   quantity(i,h,j) += dt*surfr*(transmited(j)-transmited(nsnext(ns, j)))/volume(i,h,j);
			 });
      }

      void
      VanLeerTheta(arr3d<real const>  density,
                   arr3d<real const>  rhos,
                   arr3d<real const>  vt,
                   arr3d<real> quantity,
                   real dt) const
      {
        GridSizes const ls {sizes(quantity)};
        auto volume {dispatch.cellSizes().volume()};
        auto radii  {coords.radii()};
        auto dradii {coords.dRadii()};
        auto phi    {coords.phi()};
	auto dphi   {coords.dPhi()};

        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        {
          arr3d<real> qrstar {locals.get<0>()};
          arr3d<real const> dtheta  {dispatch.cellSizes().dTheta()};
          kk::parallel_for("van_leer_rad_normalized", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             auto normalized {[&](size_t const i, size_t const h, size_t const j) { return quantity(i,h,j)/density(i,h,j);} };
                             qrstar(i,h,j) = transmitedFractionTheta(dtheta, normalized, vt, dt, i,h,j); 
                           });
        }
        arr3d<real const> qrstar{locals.get<0>()};
        int  const dim  {coords.dim()};
	bool const flat {dim == 2};
	kk::parallel_for("transport_van_leer_theta3d", fullRange(ls),
			 LBD(size_t const i, size_t const h, size_t const j) {
			   real const surfr {flat ? dradii(i) :  (ipow<2>(radii(i+1))-ipow<2>(radii(i))) * dphi(h)/2 };
			   auto transmited {[&](size_t const jj) { return qrstar(i,h,jj)*rhos(i,h,jj)*vt(i,h,jj); } };
			   quantity(i,h,j) += dt*surfr*(transmited(j)-transmited(nsnext(ls.ns, j)))/volume(i,h,j);
			 });
      }
  
      void
      VanLeerPhi(arr3d<real const> density,
                 arr3d<real const> rhos,
                 arr3d<real const> vp,
                 arr3d<real> quantity,
                 real dt) const
      {
        auto const ls {sizes(quantity)};
        auto volume {dispatch.cellSizes().volume()};
        
        auto radii     {coords.radii()};
        auto radiiMed  {coords.radiiMed()};
        auto phi       {coords.phi()};
        auto phiMed    {coords.phiMed()};
        auto theta     {coords.theta()};

        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        {
          arr3d<real> qrstar {locals.get<0>()};
          kk::parallel_for("van_leer_rad_normalized", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             auto normalized {[&](size_t const i, size_t const h, size_t const j) { return quantity(i,h,j)/density(i,h,j);} };
                             qrstar(i,h,j) = transmitedFractionPhi(radiiMed, phiMed, normalized, vp, dt, i,h,j); 
                           });
        }
        arr3d<real const> qrstar{locals.get<0>()};
        kk::parallel_for("transport_van_leer_phi", fullRange(coords),
                         LBD(size_t const i, size_t const h, size_t const j) {
                           if (h < ls.ni-1) {
                             real const surf {(ipow<2>(radii(i+1))-ipow<2>(radii(i)))*(theta(j+1)-theta(j))/2};
                             auto transmited {[&](size_t const h) { return kk::sin(phi(h))*qrstar(i,h,j)*rhos(i,h,j)*vp(i,h,j);} };
                             quantity(i,h,j) += dt*surf*(transmited(h)-transmited(h+1))/volume(i,h,j);
                           }
                         });
      }
      
      void
      VanLeerPhi(arr3d<real const> rhos,
                 arr3d<real const> vp,
                 arr3d<real> quantity,
                 real dt)  const
      {
        size_t const ni {coords.sizes().ni};
        auto radii  {coords.radii()};
        auto phi    {coords.phi()};
        auto theta  {coords.theta()};
	auto dtheta {coords.dTheta()};
        auto volume {dispatch.cellSizes().volume()};
        auto const qrstar{1};
        kk::parallel_for("transport_van_leer_phi", fullRange(coords),
                         LBD(size_t const i, size_t const h, size_t const j) {
                           if (h < ni-1) {
                             real const surfr  {(ipow<2>(radii(i+1))-ipow<2>(radii(i)))*dtheta(j)/2};
			     auto transmited {[&](size_t const hh) { return qrstar*sin(phi(hh))*rhos(i,hh,j)*vp(i,hh,j);} };
			     quantity(i,h,j) += dt*surfr*(transmited(h) - transmited(h+1))/volume(i,h,j);
                           }
                         });
      }

      std::vector<arr3d<real>>
      concat(Momenta<real> m, std::vector<arr3d<real>> extras, bool keepPhiIn2d = true) const {
        std::vector<arr3d<real>> v{};
        v.reserve(extras.size()+6);
        v.push_back(m.sup.radial);
        v.push_back(m.inf.radial);
        if (keepPhiIn2d || dispatch.grid().dim() == 3 ) {
          v.push_back(m.sup.phi);
          v.push_back(m.inf.phi);
        }
        v.push_back(m.sup.theta);
        v.push_back(m.inf.theta);
        v.insert(v.end(), extras.begin(), extras.end());
        return v;
      }

      std::vector<arr3d<real>>
      concat(Momenta<real> m, std::vector<arr3d<real>> extras, arr3d<real> density, bool keepPhiIn2D = true) const {
        auto v { concat(m, extras, keepPhiIn2D)};
        v.push_back(density);
        return v;
      }
      
      void
      oneWindRad(vec3d<real const> velocity, real dt,
                 arr3d<real> density,  Momenta<real> momenta, std::vector<arr3d<real>> const& transported) const
      {
        assert(compatible(dispatch.grid().sizes(), density, velocity, momenta.sup, momenta.inf));
        // We need the input density for all step except for density itself, so
        // we take a const version (to save a copy) and...
        arr3d<real const> cdensity {density};

        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        getTransmitedFractionRadial(cdensity, velocity.radial, dt, locals.template get<0>());
        arr3d<real const> transmited = locals.template get<0>();
        
        for (arr3d<real> field: concat(momenta, transported, false)) {
          VanLeerRadial(cdensity, transmited, velocity.radial, field, dt);
        }
        // ...process it last
        VanLeerRadial(transmited, velocity.radial, density, dt); /* MUST be the last line */
      }
    
      void
      oneWindPhi(vec3d<real const> velocity, real dt,
                 arr3d<real> density,  Momenta<real> momenta, std::vector<arr3d<real>> transported) const
      {
        assert(compatible(dispatch.grid().sizes(), density, velocity, momenta.sup, momenta.inf));
        // We need the input density for all step except for density itself, so
        // we take a const version (to save a copy) and...
        arr3d<real const> cdensity {density};
        enum { TRANSMITED };
        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        getTransmitedFractionPhi(cdensity, velocity.phi, dt, locals.get<TRANSMITED>());
        arr3d<real const> transmited = locals.get<TRANSMITED>();

        for (arr3d<real> field: concat(momenta, transported)) {
          VanLeerPhi(cdensity, transmited, velocity.phi, field, dt);
        }
        // ...process it last
        VanLeerPhi(transmited, velocity.phi, density, dt);
      }
    
      /// Re calibrate each ring on its average rotation speed.
      /// \param vtres 
      void
      cancelRotation(arr3d<real> vt, arr3d<real> vtres, real dt, arr3d<int> shifts) const
      {
        GridSizes const ls {coords.sizes()};
        auto radMed        {coords.radiiMed()};
        auto dtheta        {coords.dTheta()};
      
        arr1d<real>       averageVt = averageOnThetaPhi(vt);
      
        kk::parallel_for("tansport_res.", fullRange(coords),
                         LBD(size_t const i, size_t const h, size_t const j) {
                           real avgVt           = averageVt(i);
                           real cellWidth       = radMed(i)*dtheta(j);
                           real avgDistInNbCell = (avgVt*dt)/cellWidth;
                           int  thetaShift      = floor(avgDistInNbCell+0.5);  // rounded;
                           real avgResidual     = (avgDistInNbCell - thetaShift)*cellWidth/dt;
                         
                           vtres(i,h,j)  = vt(i,h,j) - avgVt;
                           vt(i,h,j)     = avgResidual;
                           int sftj = (int(j)-thetaShift) % int(ls.ns);
                           if (sftj < 0) { sftj += ls.ns;}
                           shifts(i,h,j) = size_t(sftj);
                         });
      }
    
      void
      addRotation(arr3d<real> field, arr3d<int const> indexes) const {
        enum { OLD };
        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        arr3d<real> old = locals.get<OLD>();
        kk::deep_copy(old, field);
        kk::parallel_for("add_rotation", fullRange(dispatch.grid()),
                         LBD(size_t const i, size_t const h, size_t const j) {
                           field(i,h,j) = old(i,h,indexes(i,h,j));
                         });
      }
  
      void
      oneWindThetaFast(vec3d<real> velocity, real dt,
                       arr3d<real> density,  Momenta<real> momenta, std::vector<arr3d<real>> const& transported) const
      {
        assert(compatible(dispatch.grid().sizes(), density, velocity, momenta.sup, momenta.inf));
      
        enum {VTHETA};
        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        arr3d<real> vtheta = locals.get<VTHETA>();
        kk::deep_copy(vtheta, velocity.theta);
        kk::deep_copy(velocity.theta, real(0));
        applyAdvection(vtheta, dt, density, momenta, transported);
      }
    
      void
      oneWindThetaSlow(vec3d<real> velocity, real dt,
                       arr3d<real> density,  Momenta<real> momenta, std::vector<arr3d<real>> const& transported) const
      {
        assert(compatible(dispatch.grid().sizes(), density, velocity, momenta.sup, momenta.inf));
        enum {VTRES};
        LocalFieldStack3D<real,1> rlocals(dispatch.temporaries3D<real>());
        enum {SHIFTS};
        LocalFieldStack3D<int,1> ilocals(dispatch.temporaries3D<int>());
      
        arr3d<real> vtres   = rlocals.get<VTRES>();
        cancelRotation(velocity.theta, vtres, dt, ilocals.get<SHIFTS>());
        arr3d<int const> const shifts = ilocals.get<SHIFTS>();
        /* Constant residual is in disk.velocity().theta() from now on */
        applyAdvection(vtres, dt, density, momenta, transported);
        applyAdvection(velocity.theta, dt, density, momenta, transported); /* Uniform Transport here */

        for (arr3d<real> field : concat(momenta, transported, density, false)) {
          addRotation(field, shifts);
        }
      }
    
      void
      applyAdvection(arr3d<real const> vtheta, real dt,
                     arr3d<real> density, Momenta<real> momenta, std::vector<arr3d<real>> transported) const
      {
        // We need the input density for all step except for density itself, so
        // we take a const version (to save a copy) and...
        arr3d<real const> cdensity { density };
        LocalFieldStack3D<real,1> locals(dispatch.temporaries3D<real>());
        getTransmitedFractionTheta(cdensity, vtheta, dt, locals.get<0>());
        arr3d<real const> transmited {locals.get<0>()};
        for (arr3d<real> field: concat(momenta, transported, false)) {
          VanLeerTheta(cdensity, transmited, vtheta, field, dt);
        }
        // ...process it last
        VanLeerTheta(transmited, vtheta, density, dt); 
      }
    
      void
      computeMomenta(arr3d<real const> density, vec3d<real const> velocity, Momenta<real> momenta) const {
        GridSizes const ls = coords.sizes();
      
        arr3d<real const> vr  = velocity.radial;
        arr3d<real const> vt  = velocity.theta;
        arr3d<real const> vp  = velocity.phi;
        arr3d<real>       rp  = momenta.sup.radial;
        arr3d<real>       rm  = momenta.inf.radial;
        arr3d<real>       tp  = momenta.sup.theta;
        arr3d<real>       tm  = momenta.inf.theta;
        arr3d<real>       pp  = momenta.sup.phi;
        arr3d<real>       pm  = momenta.inf.phi;
      
        auto radMed = coords.radiiMed();
        auto phiMed = coords.phiMed();
        real const omega {referentialRotationSpeed};
        switch (dispatch.grid().dim()) {
        case 3:
          kk::parallel_for("rmomenta3d", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             if (i < ls.nr-1 && h < ls.ni-1) {
                               real const sinPhiMed = std::sin(phiMed(h));
                               size_t const jp = nsnext(ls.ns, j);
                               rp(i,h,j) = density(i,h,j)*vr(i+1,h,j);
                               rm(i,h,j) = density(i,h,j)*vr(i,h,j);
                               tp(i,h,j) = density(i,h,j)*(vt(i,h,jp)+radMed(i)*omega*sinPhiMed)*radMed(i)*sinPhiMed; /* it is the angular momentum */
                               tm(i,h,j) = density(i,h,j)*(vt(i,h,j)+radMed(i)*omega*sinPhiMed)*radMed(i)*sinPhiMed;
                               pp(i,h,j) = density(i,h,j)*vp(i,h+1,j)*radMed(i);
                               pm(i,h,j) = density(i,h,j)*vp(i,h,j)*radMed(i);
                             } else {
                               rp(i,h,j) = 0;
                               rm(i,h,j) = 0;
                               tp(i,h,j) = 0;
                               tm(i,h,j) = 0;
                               pp(i,h,j) = 0;
                               pm(i,h,j) = 0;
                             }
                           });
          break;
        case 2:
          kk::parallel_for("rmomenta2d",
                           range2D({0,0}, {ls.nr, ls.ns}),
                           LBD(size_t const i, size_t const j) {
                             if (i < ls.nr-1) {
                               size_t const jp = nsnext(ls.ns, j);
                               rp(i,0,j) = density(i,0,j)*vr(i+1,0,j);
                               rm(i,0,j) = density(i,0,j)*vr(i,0,j);
                               tp(i,0,j) = density(i,0,j)*(vt(i,0,jp)+radMed(i)*omega)*radMed(i); /* it is the angular momentum */
                               tm(i,0,j) = density(i,0,j)*(vt(i,0,j)+radMed(i)*omega)*radMed(i);
                             } else {
                               rp(i,0,j) = 0;
                               rm(i,0,j) = 0;
                               tp(i,0,j) = 0;
                               tm(i,0,j) = 0;
                             }
                           });
          break;
        default:
          std::abort();
        }
      }
    
      void
      computeVelocities(Momenta<real const> momenta,
                        arr3d<real> density, vec3d<real> velocity) const
      {
        GridSizes const ls = coords.sizes();
      
        arr3d<real>       vr  = velocity.radial;
        arr3d<real>       vt  = velocity.theta;
        arr3d<real>       vp  = velocity.phi;
        arr3d<real const> rp  = momenta.sup.radial;
        arr3d<real const> rm  = momenta.inf.radial;
        arr3d<real const> tp  = momenta.sup.theta;
        arr3d<real const> tm  = momenta.inf.theta;
        arr3d<real const> pp  = momenta.sup.phi;
        arr3d<real const> pm  = momenta.inf.phi;
      
        auto radMed = coords.radiiMed();
        auto phiMed = coords.phiMed();
        real const densityMin = physic.density.minimum;
        CodeUnits const& units = physic.units;
        real const densityCGS  = units.density();
        real const distanceCGS = units.distance();
        switch(dispatch.grid().dim()) {
        case 3: // Density minimum check on volume density
          kk::parallel_for("transport_velocities1", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             if(density(i,h,j) > 0 && density(i,h,j)*densityCGS <= densityMin) {
                               density(i,h,j) = densityMin/densityCGS;
                             }
                           });
          break;
        case 2:
          kk::parallel_for("transport_velocities1",
                           range2D({0,0}, {ls.nr, ls.ns}),
                           LBD(size_t const i, size_t const j) {
                             if(density(i,0,j) > 0 && density(i,0,j)*densityCGS*distanceCGS <= densityMin) {
                               density(i,0,j) = densityMin/densityCGS/distanceCGS;
                             }
                           });
          break;
        default:
          std::abort();
        }
        real const omega {referentialRotationSpeed};
        kk::parallel_for("transport_velocities1", fullRange(coords),
                         LBD(size_t const i, size_t const h, size_t const j) {
                           if (i > 0) {
                             real const sinPhiMed = std::sin(phiMed(h));
                             size_t const jm = nsprev(ls.ns, j);
                             vr(i,h,j) = (rp(i-1,h,j)+rm(i,h,j))/(density(i,h,j)+density(i-1,h,j));
                             vt(i,h,j) = ((tp(i,h,jm)+tm(i,h,j))/(density(i,h,j)+density(i,h,jm))
                                          /(radMed(i)*sinPhiMed)
                                          -radMed(i)*omega*sinPhiMed);  /* It was the angular momentum */
                             if(h>=1){
                               vp(i,h,j) = (pp(i,h-1,j)+pm(i,h,j))/(density(i,h,j)+density(i,h-1,j))/radMed(i);
                             }
                           } else {
                             vr(i,h,j) = 0;
                             vt(i,h,j) = 0;
                             if(h>=1){
                               vp(i,h,j) = 0;
                             }
                           }
                         });
      }

      void operator()(real dt, arr3d<real> density, vec3d<real> velocity, std::vector<arr3d<real>> const& transported) const {
        LocalFieldStack3D<real,6> locals(dispatch.temporaries3D<real>());
        Momenta<real> momenta {{locals.get<0>(), locals.get<1>(), locals.get<2>()},
                               {locals.get<3>(), locals.get<4>(), locals.get<5>()}};
        computeMomenta(density, velocity, momenta);
        
        kk::Profiling::pushRegion("advance::oneWind");
        oneWindRad(velocity, dt, density, momenta, transported);
        if (dispatch.grid().dim() == 3){
          oneWindPhi(velocity, dt, density, momenta, transported);
        }
        if (physic.transport != fargOCA::Transport::FAST) {
          oneWindThetaFast(velocity, dt, density, momenta, transported);
        } else {
          oneWindThetaSlow(velocity, dt, density, momenta, transported);
        }
        kk::Profiling::popRegion();
        computeVelocities(momenta, density, velocity);
      }
    };
  }
  template<GridSpacing GS>  
  void
  applyTransport(Disk const& disk,
                 real dt, arr3d<real> density, vec3d<real> velocity, std::vector<arr3d<real>> const& transported) {
    Impl<GS>{disk} (dt, density, velocity, transported);
  }
  template void applyTransport<GridSpacing::ARITHMETIC>(Disk const& disk,
                                                        real dt, arr3d<real> density, vec3d<real> velocity,
                                                        std::vector<arr3d<real>> const& transported);
  template void applyTransport<GridSpacing::LOGARITHMIC>(Disk const& disk,
                                                         real dt, arr3d<real> density, vec3d<real> velocity,
                                                         std::vector<arr3d<real>> const& transported);
  
  arr3d<real>
  getTransmitedFractionRadial(Disk const& disk, 
                              arr3d<real const> density,
                              arr3d<real const> vradial,
                              real dt) {
    arr3d<real> fraction = scalarView("transmitted", disk.dispatch().grid());
    using GS = GridSpacing;
    switch(disk.dispatch().grid().radialSpacing()) {
    case GS::ARITHMETIC:  Impl<GS::ARITHMETIC>{disk} .getTransmitedFractionRadial(density, vradial, dt, fraction); break;
    case GS::LOGARITHMIC: Impl<GS::LOGARITHMIC>{disk}.getTransmitedFractionRadial(density, vradial, dt, fraction); break;
    }
    return fraction;
  }
  
  template<GridSpacing GS>
  arr1d<real>
  ComputeFlowImpl(Disk const& disk, real dt) {
    Impl<GS> transport{disk};
    arr3d<real> work { scalarView("work", transport.dispatch.grid())};
    kk::deep_copy(work, 1);
    
    auto const& coords { transport.coords };
    GridSizes const ls = coords.sizes();
    arr3d<real> qrs  {scalarView("qrs",  disk.dispatch().grid())};
    arr3d<real> rhos {scalarView("rhos", disk.dispatch().grid())};
    
    transport.getTransmitedFractionRadial(work,                  disk.velocity().radial().data(), dt, qrs);
    transport.getTransmitedFractionRadial(disk.density().data(), disk.velocity().radial().data(), dt, rhos);
    arr3d<real const> vr     = disk.velocity().radial().data();
    auto theta = coords.theta();
    auto radii  = coords.radii();
    auto phi    = coords.phi();
    
    arr1d<real> flow("radial_flow", ls.nr);
    switch(transport.dispatch.grid().dim()) {
    case 3:
      kk::parallel_for(kk::TeamPolicy<>(ls.nr, kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const i = team.league_rank();
                         using kk::pow;
                         using kk::cos;
                         real const radii2 = ipow<2>(radii(i));
                         kk::parallel_reduce(kk::TeamThreadRange(team, ls.ni),
                                             NLBD(size_t const h, real& partial) {
                                               real const surfr  = cos(phi(h))-cos(phi(h+1));
                                               real hflow;
                                               kk::parallel_reduce(kk::ThreadVectorRange(team, ls.ns),
                                                                   NLBD(size_t const j, real& partial) {
                                                                     partial += surfr*(theta(j+1)-theta(j))*radii2*qrs(i,h,j)*rhos(i,h,j)*vr(i,h,j);
                                                                   }, hflow);
                                               partial += hflow;
                                             }, flow(i));
                       });
      break;
    case 2:
      kk::parallel_for(kk::TeamPolicy<>(ls.nr, kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const i = team.league_rank();
                         kk::parallel_reduce(kk::TeamVectorRange(team, ls.ns), NLBD(size_t const j, real& partial) {
                             partial += (theta(j+1)-theta(j))*radii(i)*qrs(i,0,j)*rhos(i,0,j)*vr(i,0,j);
                           }, flow(i));
                       });
      break;
    }
    return flow;
  }
  
  arr1d<real>
  ComputeFlow(Disk const& disk, real dt) {  
    switch(disk.dispatch().grid().radialSpacing()) {
    case GridSpacing::ARITHMETIC:  return ComputeFlowImpl<GridSpacing::ARITHMETIC>(disk, dt);
    case GridSpacing::LOGARITHMIC: return ComputeFlowImpl<GridSpacing::LOGARITHMIC>(disk, dt);
    }
    std::abort();
  }
  
  template<GridSpacing GS>
  arr1d<real>
  ComputeAngularMomentumFlowImpl(Disk const& disk, real dt) {
    GridDispatch const& dispatch = disk.dispatch();    
    auto const& coords = dispatch.grid().as<GS>();
    GridSizes const ls = coords.sizes();
    
    auto localFields = dispatch.local3DFields<real,1>();
    arr3d<real>  tptm  = localFields->get<0>();

    arr3d<real const> vr     = disk.velocity().radial().data();
    auto localFields2 = dispatch.local3DFields<real,1>();
    arr3d<real> vrprime = localFields2->get<0>();
    arr3d<real const> vt     = disk.velocity().theta().data();
    auto localFields3 = dispatch.local3DFields<real,1>();
    arr3d<real> vtprime = localFields3->get<0>();
    arr3d<real const> rho    = disk.density().data();
        
    auto theta = coords.theta();
    auto radii  = coords.radii();
    auto phi    = coords.phi();

    auto radMed = coords.radiiMed();
    auto phiMed = coords.phiMed();
    real const omega = disk.frameVelocity();

    arr1d<real> vrmean("mean_vrad",ls.nr);
    arr1d<real> vtmean("mean_vtheta",ls.nr);
    

    // calculate the azimuthal mean of vr and vt for each radius
    // At the moment this is valid only for 2D
    for (int i=0; i<ls.nr; i++) {
      for (int j=0; j<ls.ns; j++) {
        vrmean(i) += vr(i,0,j);
        vtmean(i) += vt(i,0,j);
      }
    }
    // divide by the number of azimuthal cells to get the mean of vr
    for (int i = 0; i < ls.nr; i++) {
      vrmean(i)/=ls.ns;
      vtmean(i)/=ls.ns;
    }
    // calculate perturbations of vr and vt at the moment for the 2D case
    kk::parallel_for("vr_vt_prime", range2D({0,0}, {ls.nr, ls.ns}),
		     LBD(size_t const i, size_t const j) {
		       vrprime(i,0,j) = vr(i,0,j) - vrmean(i);
		       vtprime(i,0,j) = vt(i,0,j) - vtmean(i);
		     });
    
    
    switch (dispatch.grid().dim()) {
    case 3:
      kk::parallel_for("AM3d", fullRange(coords),
		       LBD(size_t const i, size_t const h, size_t const j) {
			 if (i < ls.nr-1 && h < ls.ni-1) {
			   real const sinPhiMed = std::sin(phiMed(h));
			   size_t const jp = nsnext(ls.ns, j);
			   tptm(i,h,j) = 0.5*((vt(i,h,jp)+radMed(i)*omega*sinPhiMed)+(vt(i,h,j)+radMed(i)*omega*sinPhiMed))*radMed(i)*sinPhiMed; /* it is the angular momentum average of left and right divided by rho -> i.e. we have the qantity to be transported trhough van Leer */
			 } else {
			   tptm(i,h,j) = 0;
			 }
		       });
      break;
    case 2:
      kk::parallel_for("AM2d",
		       range2D({0,0}, {ls.nr, ls.ns}),
		       LBD(size_t const i, size_t const j) {
			 if (i < ls.nr-1) {
			   size_t const jp = nsnext(ls.ns, j);
			   // tptm(i,0,j) = 0.5*((vt(i,0,jp)+radMed(i)*omega)+(vt(i,0,j)+radMed(i)*omega))*radMed(i); /* it is the angular momentumi average of left and right divided by density */
			   tptm(i,0,j) = 0.5*((vtprime(i,0,jp))+(vtprime(i,0,j)))*radMed(i);
			 } else {
			   tptm(i,0,j) = 0;
			 }
		       });
    }
    
    
    arr3d<real const> rhos =   getTransmitedFractionRadial(disk, rho, disk.velocity().radial().data(), dt);
    arr3d<real const> tptms =  getTransmitedFractionRadial(disk, tptm , vrprime, dt);
    
    arr1d<real> angmom_flow("radial_angmom_flow", ls.nr);
    switch(dispatch.grid().dim()) {
    case 3:
      kk::parallel_for(kk::TeamPolicy<>(ls.nr, kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const i = team.league_rank();
                         using kk::pow;
                         using kk::cos;
                         real const radii2 = ipow<2>(radii(i));
                         kk::parallel_reduce(kk::TeamThreadRange(team, ls.ni),
                                             NLBD(size_t const h, real& partial) {
                                               real const surfr  = cos(phi(h))-cos(phi(h+1));
                                               real hflow;
                                               kk::parallel_reduce(kk::ThreadVectorRange(team, ls.ns),
                                                                   NLBD(size_t const j, real& partial) {
                                                                     partial += surfr*(theta(j+1)-theta(j))*radii2*rhos(i,h,j)*tptms(i,h,j)*vr(i,h,j);
                                                                   }, hflow);
                                               partial += hflow;
                                             }, angmom_flow(i));
                       });
      break;
    case 2:
      kk::parallel_for(kk::TeamPolicy<>(ls.nr, kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const i = team.league_rank();
                         kk::parallel_reduce(kk::TeamVectorRange(team, ls.ns), NLBD(size_t const j, real& partial) {
                             partial += (theta(j+1)-theta(j))*radii(i)*rhos(i,0,j)*tptms(i,0,j)*vrprime(i,0,j);
                           }, angmom_flow(i));
                       });
      break;
    }
    return angmom_flow;
  }
  
  
  arr1d<real>
  ComputeAngularMomentumFlow(Disk const& disk, real dt) {
    switch(disk.dispatch().grid().radialSpacing()) {
    case GridSpacing::ARITHMETIC:
      return ComputeAngularMomentumFlowImpl<GridSpacing::ARITHMETIC>(disk, dt);
    case GridSpacing::LOGARITHMIC:
      return ComputeAngularMomentumFlowImpl<GridSpacing::LOGARITHMIC>(disk, dt);
    }
    std::abort();
  }

}
