// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <sysexits.h>
#include <filesystem>

#include <boost/algorithm/string.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/format/exceptions.hpp>

#include "environment.hpp"
#include "allmpi.hpp"
#include "utils.hpp"
#include "upgrade.hpp"
#include "log.hpp"
#include "configLoader.hpp"
#include "gridDispatch.hpp"
#include "scalarField.hpp"
#include "h5io.hpp"
#include "h5log.hpp"

namespace po = boost::program_options;
namespace fs = std::filesystem;

using namespace HighFive;

using namespace fargOCA;

bool
read_cmd_line(int argc, char *argv[], po::variables_map& vm) {
  std::ostringstream usage;
  usage << argv[0] << " <disk>.h5\n"
        << "Try to upgrade an old disk file to format changes.\n"
        << "Input disk will be save in <disk>.h5-bak.\n";
  po::options_description opts(usage.str());
  opts.add_options()
    ("help", "Produce this message.")
    ("force", "Force backup file overwrite if exists.")
    ("log-file", po::value<std::string>(), "The input log file.");
  
  po::positional_options_description input_option;
  input_option.add("log-file", 1);
  po::store(po::command_line_parser(argc, argv).
            options(opts).positional(input_option).run(), vm);
  po::notify(vm);
  if (bool(vm.count("help"))) {
    std::cout << opts;
    return false;
  }
  check_mandatory_options(opts, vm, {"log-file"}, std::cerr);
  return true;
}

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  po::variables_map vm;
  if (!read_cmd_line(argc, argv, vm)) {
    return EX_USAGE;
  }

  std::string ipath = vm["log-file"].as<std::string>();
  backup_file(ipath, ".h5-bak", bool(vm.count("force")));

  std::map<std::string, std::vector<TimeStamped<PlanetV1>>> logs;
  {
    File ifile(ipath, File::ReadWrite);
    auto root = ifile.getGroup("/");
    for(std::string name : root.listObjectNames()) {
      auto logGrp    = h5::getNamedLogGroup<TimeStamped<Planet>>(root, name);
      auto dataSet   = logGrp.getDataSet("log");
      DataType tp = dataSet.getDataType();
      if (tp.getClass() != DataTypeClass::Compound) {
        std::cerr << "Not a log file.\n";
        std::exit(EX_DATAERR);
      } else {
        CompoundType tst(std::move(tp));
        auto tsmbr = tst.getMembers();
        if (tsmbr.size() != 2 || tsmbr[0].name != "time") {
          std::cerr << "Does not contain a TimeStamped sequence, not a log file.\n";
          std::exit(EX_DATAERR);
        } else {
          CompoundType pt(std::move(tsmbr[1].base_type));
          auto pmbr = pt.getMembers();
          int missed = 2;
          for (auto mbr : pt.getMembers()) {
            if (mbr.name == "radius" || mbr.name == "Hill_radius") {
              std::cout << "Log of " << name << " already have " << mbr.name << "\n";
              missed--;
            }
          }
          if (missed == 1) {
            std::cout << "Put only one field is missing.\n";
            std::exit(EX_DATAERR);
          } else {
            if (missed == 2) {
              std::cout << "Upgrading log of " << name << "\n";
              std::vector<TimeStamped<PlanetV0>> oldLog = h5::readLog<TimeStamped<PlanetV0>>(root, name);
              std::vector<TimeStamped<PlanetV1>> newLog;
              logs[name] = std::vector<TimeStamped<PlanetV1>>();
              std::transform(oldLog.begin(), oldLog.end(),
                             std::back_inserter(logs[name]),
                             [](TimeStamped<PlanetV0> const& p) {
                               PlanetV1 v1(p.state);
                               TimeStamped<PlanetV1> ts1 = { p.time, v1 };
                               return ts1;
                             });
            } else {
              logs[name] = h5::readLog<TimeStamped<PlanetV1>>(root, name);
            }
          }
        }
      }
    }
  }
  File ofile(ipath, File::Overwrite);
  h5::writeLogs<TimeStamped<PlanetV1>>(ofile.getGroup("/"), logs);
}
