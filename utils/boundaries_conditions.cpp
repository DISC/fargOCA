// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <filesystem>

#include "boost/program_options.hpp"
#include "boost/algorithm/string.hpp"

#include "allmpi.hpp"
#include "boundariesConditionsPlugin.hpp"

namespace fargo = fargOCA;
namespace po    = boost::program_options;

int
main(int argc, char *argv[]) {
  for ( auto name : fargo::BoundariesConditionsPlugin::available() ) {
    auto& bc = fargo::BoundariesConditionsPlugin::get(name);
    std::cout << "\t* " << bc.name() << ":\t" << bc.description() << '\n';
  }
  return 0;
}
