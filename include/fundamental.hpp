// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_FUNDAMENTAL_H
#define FARGOCA_FUNDAMENTAL_H

#include <cmath>
#include "precision.hpp"

namespace fargOCA {
  real constexpr PI       = 3.14159265358979323844;
  real constexpr YEAR     = 365.25*24*3600; ///< one year in s
  real constexpr XMH      = 1.67e-24;  ///< Hydrogen mass
  real constexpr BOLTZ    = 1.38e-16; ///< Boltzmann Constant
  real constexpr CLIGHT   = 2.99792458e+10;  ///< Light speed in cm/s
  real constexpr GRAVC    = 6.6743e-08;  ///< Gravitation Constant in cm^3 x g^-1 x s^-2
  real constexpr RGAS     = 8.314e+07;  ///< Gas Constant
  real constexpr AR       = 7.56e-15;  ///< Radiation Constant
  real constexpr G        = 1;
  real constexpr R        = 1;  ///< Universal Gas Constant in code units

  /// Shocks are spread over CVNR zones:
  /// von Neumann-Richtmyer viscosity constant
  /// Beware of misprint in Stone and Norman's
  /// paper : use C2^2 instead of C2
  real const CVNR     = 1.41; 
}
#endif
