// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>

#include "KokkosExp_InterOp.hpp"

#include "allmpi.hpp"
#include "scalarField.hpp"
#include "vectorField.hpp"
#include "planetarySystem.hpp"
#include "fargOCApy.hpp"

namespace py = pybind11;

namespace fargOCA {
  namespace py11 {
    void exportPlanetarySystem(py::module_& m) {
      py::class_<PlanetarySystem, 
                 std::shared_ptr<PlanetarySystem>>(m, "PlanetarySystem")
        .def_property_readonly("planets",
                               [](PlanetarySystem& s) {
                                 return py::cast(s.planets());
                               },
                               "The plane dictionary.",
                               py::return_value_policy::reference_internal)
        .def("__repr__",
             [](PlanetarySystem const& d) {
               return "fargOCApy.PlanetarySystem";
             });
    }
  }
}
