// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <limits>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <istream>

#include <boost/algorithm/string.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/null.hpp>

#include "gridDispatch.hpp"
#include "gasShape.hpp"
#include "log.hpp"
#include "fundamental.hpp"
#include "cartesianGridCoords.hpp"
#include "gridCellSizes.hpp"
#include "arraysUtils.hpp"

namespace fargOCA {
  namespace kk = Kokkos;
#define LBD KOKKOS_LAMBDA

  namespace {
    range_t 
    locaIndexRange(size_t gnr, fmpi::communicator const& world, int ghosts) {
      auto radPerProc = std::div(gnr, world.size());;
      size_t sizeLow   = radPerProc.quot;
      size_t sizeHigh  = sizeLow+1;
      size_t remainder = radPerProc.rem;
      range_t idxs;
      if (sizeLow < 2*ghosts) {
        if (world.rank()==0) {
          std::cerr << "The number of processes is too large\n"
                    << "or the mesh is radially too narrow.\n";
        }
        std::abort();
      }
      if (world.rank() < int(remainder)) {
        idxs.first = sizeHigh*world.rank();
        idxs.second = idxs.first+sizeHigh;
      } else {
        idxs.first = sizeHigh*remainder+(world.rank()-remainder)*sizeLow;
        idxs.second = idxs.first+sizeLow;
      }
      if (world.rank() != 0) {
        idxs.first -= ghosts;
      }
      if (world.rank() < world.size()-1) { 
        idxs.second += ghosts;
      }
      return idxs;
    }
  }
  
  GridDispatch::GridDispatch(fmpi::communicator const& world, 
                             Grid const& ggrid, int nghosts)
    : myComm(world),
      myRadiiIndexRange(locaIndexRange(ggrid.sizes().nr, world, nghosts)),
      myGrid(ggrid.clone(myRadiiIndexRange)),
      myNbRadii("nb_radii_per_proc", comm().size()),
      myNbManagedRadii("nb_managed_radii", comm().size()),
      myNbRadiiDispls("radial_displ", comm().size()),
      myNbManagedRadiiDispls("managed_radial_displ", comm().size()),
      myGhostSize(nghosts),
      myCellSizes(nullptr),
      myCartesianCoords(nullptr),
      myRealTemporaries3D(std::make_unique<FieldStack3D<real>>(grid().sizes(), "grid_temp_3D", 0)),
      myIntTemporaries3D(std::make_unique<FieldStack3D<int>>(grid().sizes(), "grid_temp_3D", 0))
  {
    initRadiusSizes();    
    myCellSizes       = std::make_unique<GridCellSizes>(grid());
    myCartesianCoords = std::make_unique<CartesianGridCoords>(grid());
  }

  shptr<GridDispatch const>
  GridDispatch::make(fmpi::communicator const& comm, Grid const& grid, size_t ghosts) {
    return shptr<GridDispatch>(new GridDispatch(comm, grid, ghosts)); 
  }

  shptr<GridDispatch const>
  GridDispatch::make(GridDispatch const& grid, size_t ghosts) {
    if (ghosts == grid.ghostSize()) {
      return grid.shared();
    } else {
      return make(grid.comm(), grid, ghosts);
    }
  }

  shptr<GridDispatch const>
  GridDispatch::make(fmpi::communicator const& comm, GridDispatch const& grid, size_t ghosts) {
    return shptr<GridDispatch>(new GridDispatch(comm, grid.grid().global(), ghosts)); 
  }

  GridDispatch::~GridDispatch() {}

  void
  GridDispatch::initRadiusSizes() {
    int localNbRadius = myRadiiIndexRange.second-myRadiiIndexRange.first;
    fmpi::all_gather(myComm, localNbRadius, myNbRadii.data());
    // Most proc are missing ghost on both sides...
    for (int i = 0; i < int(comm().size()); ++i) {
      myNbManagedRadii(i) = myNbRadii(i) - 2*ghostSize();
    }
    // ...except those 2 that are just missing one.
    // They could be the same.
    myNbManagedRadii(0) += ghostSize();
    myNbManagedRadii(myNbManagedRadii.size()-1) += ghostSize();
    
    myNbManagedRadiiDispls(0) = 0;
    myNbRadiiDispls(0) = 0;
    for (int i = 1; i < int(comm().size()); ++i) {
      myNbManagedRadiiDispls(i) = myNbManagedRadiiDispls(i-1) + myNbManagedRadii(i-1);
      myNbRadiiDispls(i)        = myNbManagedRadiiDispls(i)   - ghostSize();
    }
  }
  
  std::ostream&
  GridDispatch::log(int p) const {
    return fargOCA::log(p == comm().rank());
  }

  template<typename AT, class... P >
  void 
  GridDispatch::joinManagedHelper(kk::View<AT,P...> local, std::optional<int> where, 
                                  std::optional<kk::View<typename kk::View<AT,P...>::non_const_data_type, P...>>& global) const
  {
    assert(!where || *where <= comm().size());
    assert(local.extent(0) == grid().sizes().nr);
    
    if (comm().size() == 1) {
      kk::deep_copy(*global, local);
    } else {
#if defined(ENABLE_PARALLEL)
      assert(comm().size() > 1);
      
      int status;
      int const slotSize = local.size()/grid().sizes().nr;
      harr1d<int const> managedStored       = nbManagedRadii(slotSize);
      harr1d<int const> managedDisplsStored = nbManagedRadiiDispls(slotSize);
      size_t const gnr = grid().global().sizes().nr;
      
      auto dataIn     = dev2CHost(managedView(local));
      assert(dataIn.size() == managedStored(comm().rank()));
      
      if (!where) { // means everywhere
        assert(global->size() == gnr*slotSize);
        auto dataOut = cHost2DevBuffer(dataIn, *global);
        status = MPI_Allgatherv(dataIn.data(), dataIn.size(), fmpi::get_mpi_datatype<real>(),
                                dataOut.data(), managedStored.data(), managedDisplsStored.data(), fmpi::get_mpi_datatype<real>(),
                                comm());
        cHost2Dev(dataOut, *global);
      } else {
        assert(*where != comm().rank() || global->size() == gnr*slotSize);
        if (global) {
          auto dataOut = cHost2DevBuffer(dataIn, *global);
          status = MPI_Gatherv(dataIn.data(), dataIn.size(), fmpi::get_mpi_datatype<real>(),
                               dataOut.data(), managedStored.data(), managedDisplsStored.data(), fmpi::get_mpi_datatype<real>(),
                               *where, comm());
          cHost2Dev(dataOut, *global);
        } else {
          status = MPI_Gatherv(dataIn.data(), dataIn.size(), fmpi::get_mpi_datatype<real>(),
                               nullptr, managedStored.data(), managedDisplsStored.data(), fmpi::get_mpi_datatype<real>(),
                               *where, comm());
        }
      }
      if (status != MPI_SUCCESS) { std::abort(); }
#else
      std::abort();
#endif     
    }
  }

  std::optional<arr3d<real>>
  GridDispatch::joinManaged(arr3d<real const> local, opt<int> where) const {
    std::optional<arr3d<real>> global;
    if (!where || *where == comm().rank()) {
      // we want it everywhere or just right here
      global = arr3d<real>("merged", grid().global().sizes().nr, local.extent(1), local.extent(2));
    }
    joinManagedHelper(local, where, global);
    return global;
  }

  std::optional<arr2d<real>>
  GridDispatch::joinManaged(arr2d<real const> local, opt<int> where) const {
    std::optional<arr2d<real>> global;
    if (!where || *where == comm().rank()) {
      // we want it everywhere or just right here
      global = arr2d<real>("merged", grid().global().sizes().nr, local.extent(1));
    }
    joinManagedHelper(local, where, global);
    return global;
  }

  std::optional<arr1d<real>>
  GridDispatch::joinManaged(arr1d<real const> local, opt<int> where) const {
    std::optional<arr1d<real>> global;
    if (!where || *where == comm().rank()) {
      // we want it everywhere or just right here
      global = arr1d<real>("merged", grid().global().sizes().nr);
    }
    joinManagedHelper(local, where, global);
    return global;
  }


  void
  GridDispatch::dispatch(std::optional<arr3d<real const>> merged, arr3d<real> local, int where) const {
    assert(bool(merged) == (where == comm().rank()));
    assert(local.size() == grid().footprint());
    
    if (comm().size() > 1) {
#if defined(ENABLE_PARALLEL)
      auto const lsz = grid().sizes();
      auto const gsz = grid().global().sizes();
      if (comm().rank() == where) {
        assert(gsz.nr == merged->extent(0));
        assert(gsz.ni == merged->extent(1));
        assert(gsz.ns == merged->extent(2));
      }
      harr1d<int const> memStored    = nbRadii(lsz.ns*lsz.ni);
      harr1d<int const> displsStored = nbRadiiDispls(lsz.ns*lsz.ni);
      auto dataIn     = optDev2CHost(merged);
      real const* input = data(dataIn);
      auto localBuffer = cHost2DevBuffer(dataIn, local);
      int status =  MPI_Scatterv(input, memStored.data(), displsStored.data(), fmpi::get_mpi_datatype(real()),
                                 localBuffer.data(), memStored(comm().rank()), fmpi::get_mpi_datatype(real()),
                                 where, comm());
      cHost2Dev(localBuffer, local);
      if (status != MPI_SUCCESS) { comm().abort(status); }
#endif
    } else {
      assert(merged->size() == local.size());
      kk::deep_copy(local, *merged);
    }
  }

  namespace {
    harr1d<int const>
    scaled(harr1d<int const> a, int sz) {
      if (sz == 1) {
        return a;
      } else {
        harr1d<int> scaled(a.label()+"_scaled", a.size());
        kk::parallel_for("scaled", kk::RangePolicy<kk::DefaultHostExecutionSpace>(0, a.size()),
                         LBD(int i) { scaled(i) = sz*a(i); });
        return scaled;
      }
    }
  }
  
  harr1d<int const> GridDispatch::nbRadii(int slotSize) const { return scaled(myNbRadii, slotSize); }
  harr1d<int const> GridDispatch::nbManagedRadii(int slotSize) const { return scaled(myNbManagedRadii, slotSize); }
  harr1d<int const> GridDispatch::nbRadiiDispls(int slotSize) const { return scaled(myNbRadiiDispls, slotSize); }
  harr1d<int const> GridDispatch::nbManagedRadiiDispls(int slotSize) const { return scaled(myNbManagedRadiiDispls, slotSize); }

  template <> FieldStack<real,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>& 
  GridDispatch::temporaries() const { return const_cast<FieldStack<real,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>&>(*myRealTemporaries3D); }
  
  template <> FieldStack<int,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>& 
  GridDispatch::temporaries() const { return const_cast<FieldStack<int,GridDirection::RADIAL,GridDirection::PHI,GridDirection::THETA>&> (*myIntTemporaries3D);  }


  void 
  GridDispatch::dumpMpiLayout() const {
    auto const lsz = grid().sizes();

    if (master()) {
      std::cout << "grid layout:\n"
                << "radial spacing: " << grid().radialSpacing() << '\n';
    }
    for(int p = 0; p < comm().size(); ++p) {
      comm().barrier();
      if (p == comm().rank()) {
        range_t const gradii  = radiiIndexRange();
        range_t const managed = managedRadii();
        std::cout << "\t - proc " << p << ", " << lsz << ": \n"
                  << "\t\t nb rad.: " << lsz.nr
                  << " from " << begin(gradii) << " to " << end(gradii) 
                  << ", local radial offset: " << grid().radialOffset() 
                  << ", global sizes: " << grid().sizes() << '\n'
                  << "\t\t managed: " << begin(managed) << " to " << end(managed) << std::endl;
      }
    }
  }
}
