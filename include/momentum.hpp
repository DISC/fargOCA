// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_MOMENTUM_H
#define FARGOCA_MOMENTUM_H

#include <cmath>
#include <iosfwd>

#include "Kokkos_Core.hpp"

#include "mpidatatype.hpp"

#include "precision.hpp"
#include "tuple.hpp"

namespace fargOCA {

  /// \brief A basic speed+mass utility struct.
  struct Momentum {
    /// \brief The mass*velocity vector
    Triplet  vector;
    /// \brief The mass.
    real     mass;
    
    KOKKOS_INLINE_FUNCTION
    Momentum(Triplet const& v, real m) : vector(m*v), mass(m) {}
    KOKKOS_INLINE_FUNCTION
    Momentum() : Momentum(Triplet(0,0,0), 0) {}
    KOKKOS_INLINE_FUNCTION
    void operator=(Momentum const& m) { vector = m.vector; mass = m.mass; }
    
    /// \brief vector/mass
    Triplet velocity() const { return mass > 0 ? vector/mass : Triplet(0,0,0); }

    KOKKOS_INLINE_FUNCTION
    void operator+=(Momentum const& m) { vector += m.vector; mass += m.mass; }

  private:
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* version */)
    {
      ar & vector; 
      ar & mass;
    }
  };
  
  inline
  Momentum
  operator+(Momentum const& m1, Momentum const& m2) {
    Momentum result;
    result.vector = m1.vector+m2.vector;
    result.mass   = m1.mass+m2.mass;
    return result;
  }

  std::ostream& operator<<(std::ostream& out, Momentum const& m);
}

namespace Kokkos {
  template<>
  struct reduction_identity<fargOCA::Momentum> {
    KOKKOS_FORCEINLINE_FUNCTION static fargOCA::Momentum sum() {
      return fargOCA::Momentum();
    }
  };
}

#if defined(ENABLE_PARALLEL)
namespace boost {
  namespace mpi {
    template <>
    struct is_mpi_datatype<fargOCA::Momentum> : mpl::true_ { };
  } 
}
#endif

#endif 
