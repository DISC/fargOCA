// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "environment.hpp"
#include "gasShape.hpp"
#include "diskProfiles.hpp"
#include "gridDispatch.hpp"
#include "arrayTestUtils.hpp"
#include "loops.hpp"

using namespace fargOCA;

template<class T>
std::string
label(Kokkos::View<T> v) {
  std::ostringstream fmt;
  fmt << v.label();
  for (int d = 0; d < v.rank; ++d) {
    fmt << "_" << v.extent(d);
  }
  return fmt.str();
}

void
display(arr1d<real const> g1, arr1d<real const> g2) {
  arr1d<real const>::HostMirror h1 = Kokkos::create_mirror(g1);
  arr1d<real const>::HostMirror h2 = Kokkos::create_mirror(g2);
  Kokkos::deep_copy(h1,g1);
  Kokkos::deep_copy(h2,g2);
  size_t const nr = h1.extent(0);
  for(size_t i = 0; i < nr; ++i) {
    std::cout << "i: " << i << '\n';
    std::cout << "\th1: " << h1(i) << '\n';
    std::cout << "\th2: " << h2(i) << '\n';
  }
}

void
display(arr2d<real const> g1, arr2d<real const> g2) {
  auto h1 = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace{}, g1);
  auto h2 = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace{}, g2);
  size_t const nr = h1.extent(0);
  size_t const ni = h1.extent(1);
    
  for(size_t i = 0; i < nr; ++i) {
    std::cout << "i: " << i << '\n';
    std::cout << "h1:\t";
    for(size_t h = 0; h < ni; ++h) {
      std::cout << h1(i,h) << '\t';
    }
    std::cout << "\nh2:\t";
    for(size_t h = 0; h < ni; ++h) {
      std::cout << h2(i,h) << '\t';
    }
    std::cout << '\n';
  }
}

void
display(arr3d<real const> g1, arr3d<real const> g2) {
  auto h1 = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace{}, g1);
  auto h2 = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace{}, g2);
  size_t const nr = h1.extent(0);
  size_t const ni = h1.extent(1);
  size_t const ns = h1.extent(2);

  for(size_t i = 0; i < nr; ++i) {
    std::cout << "#### i: " << i << '\n';
    for(size_t h = 0; h < ni; ++h) {
      std::cout << "# h: " << h << '\n';
      std::cout << "\nhost 1:\t";      
      for(size_t j = 0; j < ns; ++j) {
        std::cout << h1(i,h,j) << '\t';
      }
      std::cout << "\nhost 2:\t";
      for(size_t j = 0; j < ns; ++j) {
        std::cout << h2(i,h,j) << '\t';
      }
    }
    std::cout << '\n';
  }
}

template<typename T>
bool
test_join(GridDispatch const& grid, Kokkos::View<T> g) {
  grid.log() << "Test join\n";
  auto l = grid.localView(g);
  auto joined = grid.joinManaged(l);
  std::optional<real> jRelDiff = maxRelDiff(g, joined);
  bool passed = jRelDiff && *jRelDiff < std::numeric_limits<real>::epsilon()*10;
  for(int r = 0; r < grid.comm().size(); ++r) {
    grid.comm().barrier();
    if (r == grid.comm().rank()) {
      if (passed) {
        std::cout << "Proc " << r << ": PASSED\n";
        std::cout << "max rel diff : " << *jRelDiff << "\n";
      } else {
        std::cout << "Proc " << r << ": FAILED\n";
        if (jRelDiff) {
          std::cout << "max rel diff : " << *jRelDiff << "\n";
          display(g, joined);
        } else {
          std::cout << "dimension problem.\n";
        }
      } 
    } 
  }
  return passed;
}

template<typename T>
bool
test_join(GridDispatch const& grid, Kokkos::View<T> g, int master) {
  assert(master < grid.comm().size());
  grid.log(master) << "Test join with master " << master << "\n";
  
  auto l = grid.localView(g);
  auto joined = grid.joinManaged(l, master);
  bool passed;
  if (grid.comm().rank() == master) {
    std::optional<real> jRelDiff = maxRelDiff(g, *joined);
    passed = jRelDiff && *jRelDiff < std::numeric_limits<real>::epsilon()*10;
    if (passed) {
      std::cout << "PASSED\n";
      std::cout << "max rel diff : " << *jRelDiff << "\n";
    } else {
      std::cout << "FAILED\n";
      if (jRelDiff) {
        std::cout << "max rel diff : " << *jRelDiff << "\n";
      } else {
        std::cout << "dimension problem.\n";
      }
    }
  } else {
    passed = true;
  }
  return passed;
}

int
nbfailed(GridDispatch const& grid, bool ok) {
  return fmpi::all_reduce(grid.comm(), ok ? 0 : 1, std::plus<int>());
}

template<typename T>
int
test(GridDispatch const& grid, Kokkos::View<T> g) {
  grid.log(0) << "join " << label(g) << " test\n";
  int failed = 0;
  {
    bool passed = test_join(grid, g);
    failed += nbfailed(grid, passed);
  }
  for (int r = 0; r < grid.comm().size(); ++r) {
    bool passed = test_join(grid, g, r);
    failed += nbfailed(grid, passed);
  }
  return failed;
}

int
main(int argc, char* argv[]) {

  Environment  env(argc, argv);
  GasShape     shape(1, 2, 20, 0.1, false);
  size_t const gnr = 20*env.world().size();
  size_t const ni  = 5;
  size_t const ns  = 10;
  auto dispatch = GridDispatch::make(env.world(), *Grid::make(shape, {gnr, ni, ns}, GridSpacing::ARITHMETIC));
  size_t const nr  = dispatch->grid().sizes().nr;
  
  int failed = 0;

  std::cout.precision(10);
  {
    arr1d<real> g("global_field_1d", gnr);
    Kokkos::parallel_for(gnr, KOKKOS_LAMBDA(int i) { g(i) = i; });
    failed += test(*dispatch, arr1d<real const>(g));
  }
  {
    size_t const lni = 2;
    arr2d<real> g("global_field_2d_small", gnr, lni);
    Kokkos::parallel_for(range2D({gnr,lni}), KOKKOS_LAMBDA(int i, int h) { g(i,h) = i+real(h)/lni; });
    failed += test(*dispatch, arr2d<real const>(g));
  }
  {
    size_t const lni = ni;
    arr2d<real> g("global_field_2d", gnr, lni);
    Kokkos::parallel_for(range2D({gnr,lni}), KOKKOS_LAMBDA(int i, int h) { g(i,h) = i+real(h)/lni; });
    failed += test(*dispatch, arr2d<real const>(g));
  }

  {
    size_t const lni = ni+13;
    arr2d<real> g("global_field_2d_big", gnr, lni);
    Kokkos::parallel_for(range2D({0,0}, {gnr,lni}), KOKKOS_LAMBDA(int i, int h) { g(i,h) = i+real(h)/lni; });
    failed += test(*dispatch, arr2d<real const>(g));
  }

  {
    size_t const lni = ni;
    size_t const lns = ns;
    arr3d<real> g("global_field_3d", gnr, lni, lns);
    Kokkos::parallel_for(range3D({0,0,0}, {gnr,lni, lns}), KOKKOS_LAMBDA(int i, int h, int j) { g(i,h,j) = i*100+j+real(h)/lni; });
    failed += test(*dispatch, arr3d<real const>(g));
  }

  {
    size_t const lni = 2;
    size_t const lns = 3;
    arr3d<real> g("global_field_3d_small", gnr, lni, lns);
    Kokkos::parallel_for(range3D({0u,0u,0u}, {gnr,lni, lns}), KOKKOS_LAMBDA(int i, int h, int j) { g(i,h,j) = i*100+j+real(h)/lni; });
    failed += test(*dispatch, arr3d<real const>(g));
  }
  {
    size_t const lni = ni+2;
    size_t const lns = ns+3;
    arr3d<real> g("global_field_3d_big", gnr, lni, lns);
    Kokkos::parallel_for(range3D({0u,0u,0u}, {gnr,lni, lns}), KOKKOS_LAMBDA(int i, int h, int j) { g(i,h,j) = i*100+j+real(h)/lni; });
    failed += test(*dispatch, arr3d<real const>(g));
  }
  return failed;
}
