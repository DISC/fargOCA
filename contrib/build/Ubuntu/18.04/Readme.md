## General
### Environement choices

We will present an installation based on the most usual available tools for that platform (GNU compiler, OpenMPI).

Other tools are availables (Intel). This installation is not taillorded for maximum efficiency.

### Fargo specific 3rd party tools installation directory

We will assume we do not have root access (but that the official packages can be installed) and installed 3rd party tools in ~/fargopt. Feel free to install them somewhere else but adapt the scripts accordingly.

To make those tool availables, you can write en source the following:

```
$ more ~/fargopt/env.sh
export PATH=~/fargopt/bin:$PATH
export LD_LIBRARY_PATH=~/fargopt/lib:$LD_LIBRARY_PATH
$ source ~/fargopt/env.sh
```

### Compilers

This Ubuntu commes with g++ 7.5, which supports enough of C++14.

### OpenMPI

We use the platform OpenMPI:
```
$ which mpicc 
/usr/bin/mpicc
$ ls -l /usr/bin/mpicc
lrwxrwxrwx 1 root root 24 juin   2  2018 /usr/bin/mpicc -> /etc/alternatives/mpicc
$ ls -l /etc/alternatives/mpicc
lrwxrwxrwx 1 root root 23 août   3  2018 /etc/alternatives/mpicc -> /usr/bin/mpicc.openmpi
$ 
```

You only need the C MPI binding.

### Boost

This Ubuntu distribution commes with boost 1.65, which is good enough. You can install `libboost1.65-all-dev`.

### Python

We will use the distribution Python, `/usr/bin/python3.6`. The dev packages must be installed (package `python3.6-dev`):
```
alainm@jarvis:~/views/fargO2$ apt search libpython3.6-dev
Sorting... Done
Full Text Search... Done
libpython3-all-dev/bionic-updates 3.6.7-1~18.04 amd64
  package depending on all supported Python 3 development packages

libpython3.6-dev/bionic-updates,bionic-security,now 3.6.9-1~18.04ubuntu1.3 amd64 [installed,automatic]
  Header files and a static library for Python (v3.6)

alainm@jarvis:~/views/fargO2$ 
```

### CMake

The platform cmake version is 3.10, we need 3.12 or higher. If we don't have one, we need to install it.
We use the most recent stable release at the time of installation:

```
$ mkdir -p ~/fargobld/cmake
$ cd ~/fargobld/cmake
$ wget https://github.com/Kitware/CMake/releases/download/v3.18.4/cmake-3.18.4.tar.gz
...
$ tar xzf cmake-3.18.4.tar.gz
...
$ cd cmake-3.18.4
$ ./configure --prefix=~/fargopt
...
$ make
...
$ make install
...
$ which cmake
/home/<login>/fargopt/bin/cmake
```
