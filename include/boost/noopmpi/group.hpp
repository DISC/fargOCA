// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/** @file communicator.hpp
 *
 *  This header defines a noop @c communicator class.
 *  In a context of no underlying MPI communicator.
 *  It's for the case where we do not have, or want, 
 *  to use MPI, or running on a cluster/implementation
 *  that does not support direct invocation of one 
 *  process MPI jobs. 
 */
#ifndef BOOST_NOOPMPI_GROUP_HPP
#define BOOST_NOOPMPI_GROUP_HPP

#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>

namespace boost { namespace noopmpi {

    /**
     * @brief A dummy group, either empty or with a single process.
     */
    class BOOST_MPI_DECL group {
    public:
    /**
     * @brief Construct a group that can be either empty or full.
     */
    group(bool f = false) : full(f) {}
    
    /** 
     * @brief Rank of the calling process when relevant.
     * @returns if non empty, the rank 0, if not none
     */
    std::optional<int> rank() const { return full ? std::optional<int>(1) : std::nullopt; }
    /**
     * @brief Determine the number of processes in the group.
     *
     * @returns 0 if empty, 1 otherwise.
     */
    int size() const { return full ? 1 : 0; }
    
    /**
     * @brief Translates the ranks from one group into the ranks of the
     * same processes in another group.
     *
     * 0 is the only possible rank, so the result only depend on the emptyness of the groups.
     * 
     * @param first Beginning of the iterator range of ranks in the
     * current group.
     *
     * @param last Past the end of the iterator range of ranks in the
     * current group.
     *
     * @param to_group The group that we are translating ranks to.
     *
     * @param out The output iterator to which the translated ranks will
     * be written.
     *
     * @returns the output iterator, which points one step past the last
     * rank written.
     */
    template<typename InputIterator, typename OutputIterator>
    OutputIterator translate_ranks(InputIterator first, InputIterator last,
				   const group& to_group, OutputIterator out)
    {
     // can be either empty or one
     if (first != last) {
	*out++ = *first;
     }
     return out;
    }

    /**
     * @brief Determines whether the group is non-empty.
     *
     * @returns True if the group is not empty, false if it is empty.
     */
    operator bool() const { return full; }

    /**
     *  @brief Creates a new group including a subset of the processes
     *  in the current group.
     *
     *  @c first The beginning of the iterator range of ranks to include.
     *
     *  @c last Past the end of the iterator range of ranks to include.
     *
     *  @returns A new group containing those processes with ranks @c
     *  [first, last) in the current group.
     */
    template<typename InputIterator>
    group include(InputIterator first, InputIterator last) { 
      // At best, we want to include this
      return group(first != last);
    }

    /**
     *  @brief Creates a new group from all of the processes in the
     *  current group, exluding a specific subset of the processes.
     *
     *  @c first The beginning of the iterator range of ranks to exclude.
     *
     *  @c last Past the end of the iterator range of ranks to exclude.
     *
     *  @returns A new group containing all of the processes in the
     *  current group except those processes with ranks @c [first, last)
     *  in the current group. 
     */
    template<typename InputIterator>
    group exclude(InputIterator first, InputIterator last) {
      // at best, don't remove anything
      return group(first == last);
    }
  
    private:
    bool full;
    };

    /**
     * @brief Determines whether two process groups are identical.
     */
    inline bool operator==(const group& g1, const group& g2)
    {
      return bool(g1) == bool(g2);
    }

    /**
     * @brief Determines whether two process groups are not identical.
     */
    inline bool operator!=(const group& g1, const group& g2)
    { 
      return bool(g1) != bool(g2); 
    }
    
    /**
     * @brief Computes the union of two process groups.
     */
    inline group operator|(const group& g1, const group& g2)
    {
      return group(bool(g1) || bool(g2));
    }

    /**
     * @brief Computes the intersection of two process groups.
     */
    inline group operator&(const group& g1, const group& g2) 
    {
      return group(bool(g1) && bool(g2));
    }
    
    /**
     * @brief Computes the difference between two process groups.
     */
    inline group operator-(const group& g1, const group& g2) {
      return group(bool(g1) && !bool(g2));
    }
  }
}

#endif 
