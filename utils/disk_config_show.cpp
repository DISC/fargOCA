// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <filesystem>
#include <boost/program_options.hpp>

#include <highfive/H5File.hpp>
#include <nlohmann/json.hpp>

#include "allmpi.hpp"

#include "diskPhysic.hpp"
#include "simulationTrackingPlugin.hpp"
#include "simulationConfig.hpp"
#include "optIO.hpp"
#include "simulation.hpp"
#include "disk.hpp"
#include "h5Labels.hpp"
#include "codeUnits.hpp"

using namespace fargOCA;
namespace po  = boost::program_options;
namespace fs  = std::filesystem;
namespace HF  = HighFive;
namespace h5l = fargOCA::h5::labels;
using nlohmann::json;
template<class T> using opt = std::optional<T>;

bool
parse_options(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << '\t' << cmd << " [--input] <input>.h5 [options]+: extract physic configuration and write in plain text\n";

  po::options_description opts(usage.str());
  opts.add_options()
    ("help,h", "This help message.")
    ("physic", "print the physic configuration.")
    ("code-units", "print the code units values.")
    ("trackers", "print the trackers configuration.")
    ("all,a", "print all the configuration.")
    ("text", "print in text format.")
    ("json", "Print the whole configuration in json format.")
    ("input,i",  po::value<std::string>(), "HDF5 input file.")
    ("ofile,o",  po::value<std::string>(), "Output file, default to stdout.")
    ;
  po::positional_options_description pos; 
  pos.add("input", 1);
  try {
    po::store(po::command_line_parser(argc, argv).options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch(const po::error& e) {
    std::cerr << "Couldn't parse command line arguments properly:\n"
              << e.what() << '\n' << '\n'
              << opts << '\n';
    return false;
  }
  if(vm.count("help") || !vm.count("input")) {
    std::cout << opts << "\n";
    return false;
  }
  return true;
}

void
show(Simulation const& simulation, po::variables_map const& vm, std::ostream& out) {
  if (bool(vm.count("json"))) {
    out << std::setw(4) << json{configuration(simulation)}[0] << std::endl;
  } else {
    if (bool(vm.count("all"))) {
      simulation.dump(out);
    } else {
      if (bool(vm.count("code-units"))) {
	simulation.disk().physic().units.dump(out);
      }
      if (bool(vm.count("physic"))) {
	simulation.disk().physic().dump(out);
      }
      if (bool(vm.count("trackers"))) {
	simulation.trackingConfig().dump(out);
      }
    }
  }
}

int
main(int argc, char* argv[]) {
  std::string cmd = fs::path(argv[0]).filename().native();
  po::variables_map vm;
  if (!parse_options(argc, argv, vm)) {
    return 1;
  }
  try {
    Environment env(argc, argv);
    auto ifname {vm["input"].as<std::string>()};
    auto simulation{Simulation::make(env.world(), HF::File(ifname, HF::File::ReadOnly).getGroup("/"), ".")};
    if (bool(vm.count("ofile"))) {
      std::ofstream out{vm["ofile"].as<std::string>(),std::ios::trunc};
      show(*simulation, vm, out);
    } else {
      show(*simulation, vm, std::cout);
    }
  } catch (std::exception const& e) {
    std::cerr << "HDF5 File error: " << e.what() << '\n';
    return 1;
  }
  return 0;
}
