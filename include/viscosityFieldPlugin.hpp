// Copyright 2022, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_VISCOSITY_FIELD_PLUGIN_H
#define FARGO_VISCOSITY_FIELD_PLUGIN_H

#include "plugin.hpp"
#include "arrays.hpp"

namespace fargOCA {
  class Disk;
  struct ViscosityField final {};
  using ViscosityFieldPlugin  = Plugin<ViscosityField, Disk const&, arr3d<real>>;
  template<> inline std::string Plugin<ViscosityField, std::string, Disk&, arr3d<real>>::id() { return "viscosity_field"; }  
}
#endif 
