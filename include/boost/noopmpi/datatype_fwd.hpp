// Copyright (C) 2006 Douglas Gregor <doug.gregor -at- gmail.com>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/** @file datatype_fwd.hpp
 *
 *  This is a dummy header.
 *  It is empty since we are not suppoed to transmit any data.
 */
#ifndef BOOST_NOOPMPI_DATATYPE_FWD_HPP
#define BOOST_NOOPMPI_DATATYPE_FWD_HPP

namespace boost { namespace noopmpi {

struct packed {};
} } 

#endif // BOOST_NOOPMPI_MPI_DATATYPE_FWD_HPP
