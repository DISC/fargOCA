// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_SIMULATION_CONFIG_H
#define FARGOCA_SIMULATION_CONFIG_H

#include <string>
#include <nlohmann/json.hpp>
#include <boost/property_tree/ptree_fwd.hpp>

#include "allmpi.hpp"

namespace fargOCA {
  template <class SUBJECT>
  struct SimuConfigWrapper {
    SUBJECT const& subject;
  };
  
  template<class SUBJECT>
  SimuConfigWrapper<SUBJECT> configuration(SUBJECT const& s) { return SimuConfigWrapper<SUBJECT>{{s}}; }
  
  /// \brief Convert a legacy property_tree configuration file to a json tree.
  /// The conversion is quite blunt. The input file is used to build a simulation which
  /// is then converted to json.
  nlohmann::json loadJsonFromPTree(fmpi::communicator const& comm, std::string ptreeFname);
  bool checkConfig(nlohmann::json const& j, std::ostream& log);
  /// \brief Change to snake case
  std::string snake(std::string s);
  std::string snake(boost::property_tree::path const& p);
}

#endif
