// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#define BOOST_TEST_MODULE noop_comunicator
#include <boost/test/included/unit_test.hpp>

#include <iostream>
#include <iterator>

#include "boost/noopmpi/environment.hpp"
#include "boost/noopmpi/communicator.hpp"
#include "boost/noopmpi/graph_communicator.hpp"
#include "boost/noopmpi/intercommunicator.hpp"

namespace ut  = boost::unit_test;
namespace tt  = boost::test_tools;
namespace mpi = boost::noopmpi;

BOOST_AUTO_TEST_CASE( environement ) 
{
  BOOST_TEST(!mpi::environment::initialized());
  {
    mpi::environment env;
    BOOST_TEST(env.initialized());
    BOOST_TEST(!env.finalized());
    BOOST_TEST(env.thread_level() == mpi::threading::multiple);
  }
  BOOST_TEST(mpi::environment::finalized());
}

BOOST_AUTO_TEST_CASE( ctor_dtor ) 
{
  {
    mpi::communicator world;
    BOOST_TEST(bool(world));
  }

  {
    int const MPI_SOME_COMM = 42;
    mpi::graph_communicator world(MPI_SOME_COMM, 42);
    BOOST_TEST(bool(world));
  }
}

BOOST_AUTO_TEST_CASE( barrier ) 
{
  mpi::communicator world;
  world.barrier();
}

BOOST_AUTO_TEST_CASE( as_intercommunicator ) 
{
  mpi::communicator world;
  BOOST_TEST(!bool(world.as_intercommunicator()));
}

BOOST_AUTO_TEST_CASE( as_graph_communicator ) 
{
  {
    mpi::communicator world;
    BOOST_TEST(!world.has_graph_topology());
    BOOST_TEST(!world.as_graph_communicator());
  }
  {
    mpi::communicator world;
    /// we don't care about the actual parameter,
    /// it's a one node graph anyway
    mpi::graph_communicator graph(world, 42);
    BOOST_TEST(!world.has_graph_topology());
    BOOST_TEST(!world.as_graph_communicator());
  }
}
