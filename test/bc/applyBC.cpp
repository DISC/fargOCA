// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>

#include <boost/lexical_cast.hpp>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "disk.hpp"
#include "boundariesConditionsPlugin.hpp"
#include "environment.hpp"

#include "boost/program_options.hpp"

using namespace fargOCA;
namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

po::variables_map read_cmdline(int argc, char* argv[]);

std::string const INPUT_DISK  = "input-disk";
std::string const OUTPUT_DISK = "output-disk";
std::string const TIME_STEP   = "time-step";
std::string const BC_COND     = "boundary-condition";

int
main(int argc, char* argv[]) {
  Environment env(argc,argv);
  po::variables_map vm = read_cmdline(argc,argv);

  HF::File ifile(vm[INPUT_DISK].as<std::string>(), HF::File::ReadWrite);
  shptr<Disk> idisk = Disk::make(env.world(), ifile.getGroup("/"));
  using BC = BoundariesConditionsPlugin;
  auto& handler{BC::get(vm[BC_COND].as<std::string>())};
  real const dt{vm[TIME_STEP].as<real>()};
  handler(*idisk, dt);
  HF::File ofile(vm[OUTPUT_DISK].as<std::string>(), HF::File::Overwrite);
  idisk->writeH5(ofile.getGroup("/"));
  return 0;
}

po::variables_map
read_cmdline(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Test a boundary condition.\n"
        << '\t' << cmd << " <" << INPUT_DISK << "> <" << BC_COND << "> <" << OUTPUT_DISK << "> [options]\n"
        << "apply <bc name> condition to <disk file>.\n";
    
  po::options_description visible(usage.str());  
  visible.add_options()
    ("time-step", po::value<real>()->default_value(0.1),  "the time-step to apply.");
  
  po::options_description hidden;
  hidden.add_options()
    (INPUT_DISK.c_str(), po::value<std::string>(), "input file")
    (OUTPUT_DISK.c_str(), po::value<std::string>(), "output file")
    (BC_COND.c_str(), po::value<std::string>(), "boundary condition");
  po::positional_options_description positional;
  positional.add(INPUT_DISK.c_str(), 1);
  positional.add(BC_COND.c_str(), 1);
  positional.add(OUTPUT_DISK.c_str(), 1);
  
  po::variables_map vm;
  try {
    po::options_description opts;
    opts.add(visible).add(hidden);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(positional).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {INPUT_DISK, OUTPUT_DISK, BC_COND }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  return vm;
}

