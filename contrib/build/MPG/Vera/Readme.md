<!-- Copyright 2020, Gabriele Pichierri, pichierri@mpia.de

 This file is part of FargOCA.

 FargOCA is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.

 FargOCA is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.
-->

# CPU build

## Environement

You can load the environement with

```
$ source <srcdir|builddir>/contrib/build/Vera/env.sh
```

## Configuring and building

The commands to build the code are

```
$ cmake -DCMAKE_BUILD_TYPE=Release -DKokkos_ENABLE_OPENMP=On <srcdir>
$ make
```

# GPU build

## Environement

You can load the environement with

```
$ source <srcdir|builddir>/contrib/build/Vera/env_GPU.sh
```

## Configuring and building

The commands to build the code are

```
$ cmake -DCMAKE_BUILD_TYPE=Release -DKokkos_ENABLE_CUDA=On -DKokkos_ARCH_AMPERE80=On -DKokkos_ENABLE_OPENMP=On -DKokkos_ENABLE_CUDA_LAMBDA=On -DFARGO_NO_PYTHON=On -DGENERATE_DOC=NO <srcdir>
$ make
```

Note that the GPU nodes `verag[001-003]` are on the partition `p.gpu` (submit slurm job using `sbatch −−partition=p.gpu <slurm submit script>`).