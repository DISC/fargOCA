#!/bin/bash

if [[ $# -lt 2 ]]
then
    echo "usage $0 <builddir> <installdir> <hdf5 version>"
    exit -1
fi
builddir=$1
installdir=$2
h5_version=1.14.3
if [[ $# -gt 2 ]]
then
    h5_version=$3
fi

mkdir -p $builddir $installdir || exit -1

pushd $builddir || exit -1

mkdir -p usr

# extract
if [[ ! -f CMake-hdf5-$h5_version.tar.gz ]]
then 
    wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.3/src/CMake-hdf5-$h5_version.tar.gz
fi

[[ -d CMake-hdf5-$h5_version ]] || tar xvzf CMake-hdf5-$h5_version.tar.gz
   
# move into source
cd CMake-hdf5-$h5_version || exit -1

mkdir rel
cd rel
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$installdir ../hdf5-1.14.3 || exit -1
make -j install
popd

(
    echo "
if [[ ! \$PATH =~ $installdir/bin ]]
then 
    export PATH=$installdir/bin:\$PATH
    export LD_LIBRARY_PATH=$installdir/lib:$installdir/lib64:\$LD_LIBRARY_PATH
    export HDF5_ROOT=$installdir
    export HDF5_DIR=$installdir
    export CMAKE_PREFIX_PATH=$installdir:$CMAKE_PREFIX_PATH
fi
"
) > $installdir/env.sh


