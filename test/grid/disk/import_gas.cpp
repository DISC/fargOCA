// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <sysexits.h>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "disk.hpp"

using namespace fargOCA;
namespace HF = HighFive;

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  
  if (argc != 3) {
    return EX_USAGE;
  }
  shptr<Disk> disk1, disk2;
  { // use scope to make sure file1 is closed before being written
    // just in case..
    HF::File file1(argv[1], HF::File::ReadOnly);
    HF::File file2(argv[2], HF::File::ReadOnly);
    disk1 = Disk::make(world, file1.getGroup("/"));
    disk2 = Disk::make(world, file2.getGroup("/"));
    assert(disk1->checkPhysicalQuantities());
    assert(disk2->checkPhysicalQuantities());
  }
  disk1->importGas(*disk2);
  world.barrier();
  disk1->writeH5(argv[1]);
  return EX_OK;
}
