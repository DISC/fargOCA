// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <sysexits.h>
#include <optional>
#include <iostream>
#include <fstream>
#include <utility>
#include <filesystem>

#include <highfive/H5File.hpp>

#include <boost/program_options.hpp>
#include <boost/format.hpp>

#include <nlohmann/json.hpp>

#include "allmpi.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "configProperties.hpp"
#include "simulationConfig.hpp"
#include "diskPhysic.hpp"
#include "utils.hpp"

using namespace fargOCA;
using namespace fargOCA::configProperties;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace nl = nlohmann;
namespace HF = HighFive;
using std::optional;

GasShape
extract_shape(po::variables_map vm) {
  if (vm.count("config") == 0 && (vm.count("shape-file") == 0 || vm.count("shape-path") == 0)) {
    std::cerr << "Error: either provide '--config', or provide '--shape-file' and '--shape-path' options.\n";
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("config"))) {
    fs::path fname = vm["config"].as<std::string>();
    if (fname.extension() == fs::path(".json")) {
      nl::json jconfig = nl::json::parse(std::ifstream(fname));
      return jconfig.at(snake(DISK)).at(snake(PHYSIC)).at(snake(GAS_SHAPE)).get<GasShape>();
    } else {
      std::exit(EX_DATAERR);
      //std::unreachable();
    }
  } else {
    std::string fname = vm["shape-file"].as<std::string>();
    std::string path  = vm["shape-path"].as<std::string>();
    try {
      HF::File file(fname, HF::File::ReadOnly);
      auto  root = file.getGroup("/");
      if (!root.exist(path)) {
        std::cerr << "Could not open group " << path << '\n';
        std::exit(EX_DATAERR);
      } else {
        GasShape shape;
        shape.readH5(root.getGroup(path));
        return shape;
      }
    } catch (std::exception const& e) {
      std::cerr << "Could not read shape in file '" << fname << "' because:\n"
                << e.what() << '\n';
      std::exit(EX_DATAERR);
    }
    std::cerr << "Could not find shape group in file " << fname << ", bye\n";
    std::exit(EX_DATAERR);
  }
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Generate a grid in an HDF5 file.\n"
        << '\t' << cmd << " --config <file> ... : retrieve all grid informations from a file.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("adc", "converting for ADC (3D is assumed otherwise).")
    ("logarithmic", "using logarithmic spacing (arithmeric is assumed otherwise), used with --nbradii.")
    ("config,c", po::value<std::string>(), "Property file containing the shape description.")
    ("shape-file", po::value<std::string>(), "HDF5 file containing the input gas shape description.")
    ("shape-path", po::value<std::string>()->default_value("physic/shape/gas"), "HDF5 path to the input gas shape description.")
    ("nbsectors", po::value<int>(), "Number of sectors (2PI assumed), either that or --sector must be provided.")
    ("nbradii", po::value<int>(), "Number of radii, either that or --radii must be provided.")
    ("nblayers", po::value<int>(), "Number of layers (3D assumed), either that or --layers must be provided for 3D.")
    ("ofile,o", po::value<std::string>(), "Output file:\n");
  try {
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(opts).run();
    po::store(parsed, vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    return -1;
  }
  if (vm.count("help") > 0) {
    std::cout << opts;
    return 0;
  }
  
  check_mandatory_options(opts, vm, {"ofile"});
  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);

  GasShape shape = extract_shape(vm);
  GridSpacing spacing = vm.count("logarithmic")>0 ? GridSpacing::LOGARITHMIC : GridSpacing::ARITHMETIC;
  size_t const nr = vm["nbradii"].as<int>();
  size_t const ni = bool(vm.count("nblayers")) ? vm["nblayers"].as<int>() : 1;
  size_t const ns = vm["nbsectors"].as<int>();
  std::shared_ptr<Grid const> coords = Grid::make(shape, {nr,ni,ns}, spacing);
  
  HF::File grid_file(vm["ofile"].as<std::string>(), HF::File::Overwrite);
  auto grid_group = grid_file.createGroup("/grid");
  coords->writeH5(grid_group);
}
