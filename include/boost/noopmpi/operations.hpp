// Copyright (C) 2004 The Trustees of Indiana University.
// Copyright (C) 2005-2006 Douglas Gregor <doug.gregor -at- gmail.com>
// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

//  Authors: Douglas Gregor
//           Andrew Lumsdaine

/** @file operations.hpp
 *
 *  Provide MPI operators local replacements.
 */
#ifndef BOOST_NOOPMPI_IS_MPI_OP_HPP
#define BOOST_NOOPMPI_IS_MPI_OP_HPP

#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>
#include <functional>

namespace boost { namespace noopmpi {

template<typename Op, typename T> struct is_mpi_op;

/**
 * @brief Determine if a function object type is commutative.
 *
 * No used in sequential mode.
 */
template<typename Op, typename T>
struct is_commutative {};

/** @brief Compute the maximum of two values. */
template<typename T>
struct maximum
{
  typedef T first_argument_type;
  typedef T second_argument_type;
  typedef T result_type;
  /** @returns the maximum of x and y. */
  const T& operator()(const T& x, const T& y) const
  {
    return x < y? y : x;
  }
};

/** @brief Compute the minimum of two values. */
template<typename T>
struct minimum
{
  typedef T first_argument_type;
  typedef T second_argument_type;
  typedef T result_type;
  /** @returns the minimum of x and y. */
  const T& operator()(const T& x, const T& y) const
  {
    return x < y? x : y;
  }
};

/** @brief Compute the bitwise AND of two integral values. */
template<typename T>
struct bitwise_and
{
  typedef T first_argument_type;
  typedef T second_argument_type;
  typedef T result_type;
  /** @returns @c x & y. */
  T operator()(const T& x, const T& y) const
  {
    return x & y;
  }
};

/** @brief Compute the bitwise OR of two integral values. */
template<typename T>
struct bitwise_or
{
  typedef T first_argument_type;
  typedef T second_argument_type;
  typedef T result_type;
  /** @returns the @c x | y. */
  T operator()(const T& x, const T& y) const
  {
    return x | y;
  }
};

/** @brief Compute the logical exclusive OR of two integral values. */
template<typename T>
struct logical_xor
{
  typedef T first_argument_type;
  typedef T second_argument_type;
  typedef T result_type;
  /** @returns the logical exclusive OR of x and y. */
  T operator()(const T& x, const T& y) const
  {
    return (x || y) && !(x && y);
  }
};

/** @brief Compute the bitwise exclusive OR of two integral values. */
template<typename T>
struct bitwise_xor
{
  typedef T first_argument_type;
  typedef T second_argument_type;
  typedef T result_type;
  /** @returns @c x ^ y. */
  T operator()(const T& x, const T& y) const
  {
    return x ^ y;
  }
};

} } // end namespace boost::noopmpi

#endif // BOOST_NOOPMPI_GET_MPI_OP_HPP
