# Copyright 2020, Gabriele Pichierri, pichierri<a>mpia.de
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

module purge
source /mpcdf/soft/distribution/obs_modules.sh

module load git
module load git-lfs/2.6
git lfs install

module load gcc/9 impi/2018.4 cmake/3.13 git/2.26 hdf5-serial/1.8.21 boost/1.66 anaconda/3/2019.03
export CXX=$(which g++)
export CC=$(which gcc)
export FC=$(which gfortran)

export MPG_ISAAC_ENV_SOURCED=1
