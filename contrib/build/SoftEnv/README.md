## Warning

We include here the *last resort* script(s) that can be used to install fargOCA dependencies.
We just assume that a decent MPI implementation is present (assuming you want to build a MPI parallel version of fargOCA).

These scripts will try to install gcc, hdf5 and boost.

## Last resort ?

Before you try these script, you should consider the alternatives:

### On an HPC cluster
 1. if you are on an HPC facility, these dependencies should be installed. Check them out.
 2. if not, the facility support team should be able to install them.
 3. if they can't, but spack is available, installing them through spack is probably easier/safer.

### On a personnal machine

If you have admin permision, use you package manager.

## So, everything else has failed

Assuming you want to install all your dependencie under <softs> and have a temporary directory <build> you can use to download and compile code, you can try installing all the needed dependencies like this:

```
$ <path to here>/install_all.sh <builddir> <softs>
```

For example:

```
$ <path to here>/install_all.sh ~/code/build/ ~/code/softs/
```

in the end, you should have `<softs>/env.sh` file that you should source before atempting to build or run fargOCA:
```
$ source <softs>/env.sh
```

The script will also add a recent `cmake` in you `~/.local` directory, so adding *export PATH=~/.local/bin:$PATH* in your `/.bashrc` could be necessary.

