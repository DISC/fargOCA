// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_DISK_PHYSIC_H
#define FARGOCA_DISK_PHYSIC_H

#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <memory>
#include <optional>

#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <highfive/H5File.hpp>

#include "precision.hpp"
#include "fundamental.hpp"
#include "gasShape.hpp"
#include "planetarySystemPhysic.hpp"
#include "h5io.hpp"
#include "shared.hpp"
#include "arrays.hpp"
#include "mathAddOns.hpp"
#include "codeUnits.hpp"
#include "pluginConfig.hpp"

namespace fargOCA {
  
  enum class DiskReferential : int {
    CONSTANT = 0,       //< user specified constant
    COROTATING = 2     //< match the speed of the guiding planet
  };

  void to_json(json&, DiskReferential const&);
  void from_json(json const&, DiskReferential&);
  
  template<> std::vector<DiskReferential> const& enumValues<DiskReferential>();
  std::ostream& operator<<(std::ostream& out, DiskReferential t);
  std::istream& operator>>(std::istream& in, DiskReferential& t);
  
  enum class Transport {
    NORMAL = 0,
    FAST   = 1
  };

  void to_json(json&, Transport const&);
  void from_json(json const&, Transport&);

  template<> std::vector<Transport> const& enumValues<Transport>();  
  std::ostream& operator<<(std::ostream& out, Transport t);
  std::istream& operator>>(std::istream& in, Transport& t);
  
  enum class StarAccretion {
    CONSTANT = 1,
    WIND     = 2
  };

  void to_json(json&, StarAccretion const&);
  void from_json(json const&, StarAccretion&);
  
  template<> std::vector<StarAccretion> const& enumValues<StarAccretion>();  
  std::ostream& operator<<(std::ostream& out, StarAccretion  sa);
  std::istream& operator>>(std::istream& out, StarAccretion& sa);

  enum class OpacityLaw {
    BELL_LIN = 0,
    CONSTANT = 1
  };

  void to_json(json&, OpacityLaw const&);
  void from_json(json const&, OpacityLaw&);
  
  template<> std::vector<OpacityLaw> const& enumValues<OpacityLaw>();
  std::ostream&  operator<<(std::ostream& out, OpacityLaw l);
  std::istream&  operator>>(std::istream& in, OpacityLaw& l);
  
  enum class CoolingType {
    BETA       = 0, //< Default cooling
    RADIATIVE  = 1  //< Radiative cooling
  };

  void to_json(json&, CoolingType const&);
  void from_json(json const&, CoolingType&);
  
  template<> std::vector<CoolingType> const& enumValues<CoolingType>();
  std::ostream&  operator<<(std::ostream& out, CoolingType c);
  std::istream&  operator>>(std::istream& in, CoolingType& c);

  
  real constexpr DENSITY_MIN = 1.e-25;
  real constexpr DEFAULT_MEAN_MOLECULAR_WEIGHT = 2.3;

  class DiskPhysic
    : public std::enable_shared_from_this<DiskPhysic> {
  public:
    CodeUnits   units;
    
    GasShape shape;

    struct Referential {
      DiskReferential             type;
      std::optional<real>         omega;
      bool                        indirectForces;
      std::optional<bool>         guidingCenter;
      std::optional<std::string>  guidingPlanet;
      
      void dump(std::ostream& out, int tab=0) const;
      void writeH5(HighFive::Group grp) const;
      void readH5(HighFive::Group const& grp);
      
      static Referential constant(real omegaFrame, bool indirect) { return {DiskReferential::CONSTANT, std::optional<real>(omegaFrame), indirect, std::nullopt, std::nullopt}; }
      static Referential corotating( bool indirect, bool guiding, std::string planet) { return {DiskReferential::COROTATING,
                                                                                                std::nullopt,
                                                                                                indirect,
                                                                                                std::optional<bool>(guiding),
                                                                                                std::optional<std::string>(planet)}; }
      
      Referential(Referential const&) = default;
      Referential(Referential&&) = default;
      Referential() = default;
  
    private:
      friend class DiskPhysic;
      Referential( DiskReferential t,
                   std::optional<real> w,
                   bool indirect,
                   std::optional<bool> guiding,
                   std::optional<std::string> planet)
        : type(t), omega(w), indirectForces(indirect), guidingCenter(guiding), guidingPlanet(planet) {}
    };
    
    Referential referential;

    std::shared_ptr<PlanetarySystemPhysic const> planetarySystem() const {
      return (bool(myPlanetarySystem)
	      ? std::shared_ptr<PlanetarySystemPhysic const>(shared_from_this(), &(*myPlanetarySystem))
	      : std::shared_ptr<PlanetarySystemPhysic const>());
    }

    PluginConfig boundaries;
    PluginConfig init;
    PluginConfig steps;
    
    struct StarAccretion {
      typedef fargOCA::StarAccretion Type;
      struct Wind {
        /// \brief Surface density of the ionized layer in g/cm^2
        real userActiveDensity;
        real filter;
	Wind(real dens, real filt) : userActiveDensity(dens), filter(filt) {}
	Wind(HighFive::Group grp);
	Wind(Wind const&) = default;
	Wind() = default;
	
        /// \brief Surface density of the ionized layer in code unit
        real activeDensity() const;
        void dump(std::ostream& out, int tab = 0) const;
        void writeH5(HighFive::Group& grp) const;
	
      private:
	friend class DiskPhysic;
	std::weak_ptr<DiskPhysic const> cfg;
      };
      
      Type type;
      real userRate; //< In solar mass per year.
      std::optional<Wind> wind;
      
      StarAccretion() = default;
      StarAccretion(StarAccretion const&) = default;
      StarAccretion(Type tp, real rate, std::optional<Wind> w) : type(tp), userRate(rate), wind(w) {}
      StarAccretion(HighFive::Group grp);

      real  rate() const;
      
      static real convertUserAccretionRate(real userAccretionRate, CodeUnits const& units);

      // for some reason, user does not use the same unit system
      real convertUserAccretionRate(real userAccretionRate) const;

      real torqueCoeff() const { assert(bool(wind)); return rate()/4/PI/wind->activeDensity(); }
      
      void dump(std::ostream& out, int tab = 0) const;
      void writeH5(HighFive::Group& grp) const;

    private:
      friend class DiskPhysic;
      std::weak_ptr<DiskPhysic const> cfg;
    };
    std::optional<StarAccretion> starAccretion;
    
    struct Viscosity {
      static std::string id() { return "viscosity"; }
      bool        artificial; //< is it artificial ?
      std::string type;  //< type
      Config  config;
      
      void dump(std::ostream& out, int tab = 0) const;
      void writeH5(HighFive::Group& grp) const;
      void readH5(HighFive::Group const& grp);
    };
    Viscosity viscosity;
    
    struct Adiabatic {
      struct Radiative {
        /// \brief User provided ZBoundary temperature in Kelvin.
	struct Opacity {
	  OpacityLaw  type;
	  Config  config;
	  void dump(std::ostream& out, int tab = 0) const;
	  void writeH5(HighFive::Group& grp) const;
	  void readH5(HighFive::Group const& grp);
	};
	Opacity opacity;
	struct Solver {
	  std::string  label;
	  Config   config;

	  void dump(std::ostream& out, int tab = 0) const;
	  void writeH5(HighFive::Group& grp) const;
	  void readH5(HighFive::Group const& grp);
	};
	Solver solver;
	
	real                userZBoundaryTemperature;
        real                dustToGas;
        std::optional<real> energyTimeStep;

        /// \brief Z boundary condition in code units
        real zBoundaryTemperature() const;
        real zBoundaryEnergy()      const;
        
        void kappa(arr3d<real const> temperature, arr3d<real const> density, arr3d<real> out) const;
                          
        struct Star {
          real       shadowAngle;

          real fStar() const;

          void dump(std::ostream& out, int tab = 0) const;
          void writeH5(HighFive::Group& grp) const;
          void readH5(HighFive::Group const& grp);

          Star(real a) : shadowAngle(a), cfg() {}
          Star() = default;
          
        private:
          friend class DiskPhysic;
          std::weak_ptr<DiskPhysic const> cfg;
        };
        std::optional<Star> star;

        void dump(std::ostream& out, int tab = 0) const;
        void writeH5(HighFive::Group& grp) const;
        void readH5(HighFive::Group const& grp);
	
        Radiative() = default;
        Radiative(Opacity const&      opacity,
		  Solver  const&      solver, 
		  real                userZBoundaryTemperature,
                  real                dustToGas,
                  std::optional<real> energyTimeStep,
                  std::optional<Star> star);
      private:
        friend class DiskPhysic;
        std::weak_ptr<DiskPhysic const> cfg;
      };
      std::optional<Radiative> radiative;
      
      struct BetaCooling {
        real timeScale;

        void dump(std::ostream& out, int tab = 0) const;
        void writeH5(HighFive::Group& grp) const;
        void readH5(HighFive::Group const& grp);
      };
      struct RadiativeCooling {
        void dump(std::ostream& out, int tab = 0) const;
        void writeH5(HighFive::Group& grp) const;
        void readH5(HighFive::Group const& grp);
      };
      typedef std::variant<BetaCooling,RadiativeCooling> Cooling;
      std::optional<Cooling> cooling;
      real index;

      /// \brief Mean specific heat of the disk gas.
      /// Can only be called if the adiabatic config is extracted from a disk physic.
      real specificHeat() const;

      void dump(std::ostream& out, int tab = 0) const;
      void writeH5(HighFive::Group& grp) const;
      void readH5(HighFive::Group const& grp);
      
      Adiabatic() = default;
      Adiabatic(Adiabatic const&) = default;
      Adiabatic& operator=(Adiabatic const&) = default;
      Adiabatic(Adiabatic&&) = default;
      Adiabatic(std::optional<Radiative> r, std::optional<Cooling> c, real i);

    private:
      friend class DiskPhysic;
      std::weak_ptr<DiskPhysic const> cfg;
    };
    std::optional<Adiabatic> adiabatic;
    
    struct Density {
      real slope;
      real start;
      real minimum;
      struct Cavity { 
        real radius;
        real ratio;
        real width;

        static Cavity diff(Cavity const& c1, Cavity const& c2);
        void dump(std::ostream& out, int tab=0) const;
        void writeH5(HighFive::Group& grp) const;
        void readH5(HighFive::Group const& grp);
      };
      Cavity cavity;

      void dump(std::ostream& out, int tab = 0) const;
      void writeH5(HighFive::Group& grp) const;
      void readH5(HighFive::Group const& grp);
    };
    Density density;
 
    struct Smoothing {
      bool change;
      bool flat; // flat (or2DModel) mean non cubic
      real taper;
      real size;
      real smooth(real HillRadius, real physicalTime) const;
      void dump(std::ostream& out, int tab) const;
      void writeH5(HighFive::Group& grp) const;
      void readH5(HighFive::Group const& grp);
    };
    Smoothing smoothing;

    /// \brief Describe the flaring of the disk.
    /// Should go into GasShape
    real flaringIndex;
    real CFLSecurity;
    real draggingCoef;
    real hillCutFactor;
    Transport transport = Transport::NORMAL;
    real meanMolecularWeight;

    void writeH5(HighFive::Group grp) const;
    void readH5(HighFive::Group const& grp);

    real hillCut(real d, real rh) const;
    /// \brief Gives the aspect ratio as a function of the radius.
    /// Should go in GasShape
    struct AspectRatio {
      KOKKOS_INLINE_FUNCTION
      AspectRatio(AspectRatio const& ) = default;
      AspectRatio& operator=(AspectRatio const& ) = default;

      AspectRatio(real r, real f) : ratio(r), findex(f) {}
      /// \brief compute the aspect ratio at a specific distance
      /// \param r the distance to the origin of the system
      KOKKOS_INLINE_FUNCTION
      real operator()(real const r) const { return ratio * std::pow(r, findex); }
      
      // Make sure we do not call this function with an index:
      template<typename T>
      void operator()(T const r) const { static_assert(!std::is_integral_v<T>); }
      
    private:
      friend class DiskPhysic;
      AspectRatio();
      real ratio;
      real findex;
    };
    AspectRatio aspectRatio;

    /// \brief Shortcut:
    bool radiative() const { return adiabatic && bool(adiabatic->radiative); }

    ~DiskPhysic();

    void dump(std::ostream& out, int tab = 0) const;
    void dump() const { dump(std::cout, 0); }
    
    shptr<DiskPhysic const> shared() const { return shared_from_this(); }

    static shptr<DiskPhysic const> make(CodeUnits&& units,
                                        real flaringIdx,
                                        GasShape shape,
                                        Referential referential,
                                        PluginConfig bounds,
                                        PluginConfig init,
                                        PluginConfig steps,
                                        std::optional<StarAccretion> starAcc,
                                        Viscosity viscosity,
                                        std::optional<Adiabatic> adiabatic,
                                        Density density,
                                        Smoothing smoothing,
                                        std::optional<PlanetarySystemPhysic> planets,
                                        real CFLSecurity,
                                        real draggingCoef,
                                        real hillCutFactor,
                                        Transport transport,
                                        real meanMolecularWeigth);
    static shptr<DiskPhysic const> make(CodeUnits const& units, HighFive::Group const& group);
    
    bool check(std::ostream& log) const;

  private:
    /// \brief Fined grained initializer.
    DiskPhysic(CodeUnits&& units,
               real flaringIdx,
               GasShape shape,
               Referential referential,
               PluginConfig bounds,
               PluginConfig init,
               PluginConfig steps,
               std::optional<StarAccretion> starAcc,
               Viscosity viscosity,
               std::optional<Adiabatic> adiabatic,
               Density density,
               Smoothing smoothing,
               std::optional<PlanetarySystemPhysic> planets,
               real CFLSecurity,
               real draggingCoef,
               real hillCutFactor,
               Transport transport,
               real meanMolecularWeight);
    
    /// \brief Load from HDF5 storage.
    ///
    /// Same as DiskPhysic physic(..); physic.readH5(group);
    DiskPhysic(CodeUnits&& units, HighFive::Group group);

    void linkBack();

  private:
    std::optional<PlanetarySystemPhysic> myPlanetarySystem;
  };

  void to_json(json&, DiskPhysic::Referential const&);
  void from_json(json const&, DiskPhysic::Referential&);

  void to_json(json&, DiskPhysic::StarAccretion const&);
  void from_json(json const&, DiskPhysic::StarAccretion&);

  void to_json(json&, DiskPhysic::Viscosity const&);
  void from_json(json const&, DiskPhysic::Viscosity&);

  void to_json(json&, DiskPhysic::Adiabatic const&);
  void from_json(json const&, DiskPhysic::Adiabatic&);

  void to_json(json&, DiskPhysic::Density const&);
  void from_json(json const&, DiskPhysic::Density&);  

  void to_json(json&, DiskPhysic::Adiabatic::RadiativeCooling const&);
  void from_json(json const&, DiskPhysic::Adiabatic::RadiativeCooling&);  

  void to_json(json&, DiskPhysic::Adiabatic::BetaCooling const&);
  void from_json(json const&, DiskPhysic::Adiabatic::BetaCooling&);

  void to_json(json&, DiskPhysic::Adiabatic::Cooling const&);
  void from_json(json const&, DiskPhysic::Adiabatic::Cooling&);

  void to_json(json&, DiskPhysic::Adiabatic::Radiative::Solver const&);
  void from_json(json const&, DiskPhysic::Adiabatic::Radiative::Solver&);

  void to_json(json&, DiskPhysic::Adiabatic::Radiative::Opacity const&);
  void from_json(json const&, DiskPhysic::Adiabatic::Radiative::Opacity&);
  
  void to_json(json&, DiskPhysic::Adiabatic::Radiative::Star const&);
  void from_json(json const&, DiskPhysic::Adiabatic::Radiative::Star&);

  void to_json(json&, DiskPhysic::Adiabatic::Radiative const&);
  void from_json(json const&, DiskPhysic::Adiabatic::Radiative&);

  void to_json(json& j, DiskPhysic::Smoothing const& v);
  void from_json(json const& j, DiskPhysic::Smoothing& v);

  void to_json(json& j, DiskPhysic const& p);  
  void from_json(json const& j, std::shared_ptr<DiskPhysic const>& p);
}

template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::DiskReferential>() { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::DiskReferential>(); }
template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::StarAccretion>()   { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::StarAccretion>(); }
template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::CoolingType>()       { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::CoolingType>(); }
template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::OpacityLaw>()          { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::OpacityLaw>(); }
template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::Transport>()       { return fargOCA::h5::details::buildHFEnumDesc<fargOCA::Transport>(); }

#endif
