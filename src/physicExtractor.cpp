// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
// This file is part of FargOCA.
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "boost/property_tree/info_parser.hpp"
#include "boost/range/adaptors.hpp"
#include "boost/range/algorithm.hpp"

#include "configLoader.hpp"
#include "codeUnits.hpp"
#include "configProperties.hpp"
#include "io.hpp"
#include "simulationTrackingPlugins.hpp"
#include "simulation.hpp"
#include "planetarySystemEngine.hpp"
#include "planetarySystemPhysic.hpp"
#include "config.hpp"
#include "viscosityFieldPlugin.hpp"

namespace pt = boost::property_tree;
namespace kk = Kokkos;

namespace fargOCA {
  using namespace configProperties;

  template<>
  std::optional<std::vector<std::string>>
  ConfigLoader::pget_optional(path const& p) const {
    auto v = cfg().get_optional<std::string>(p);
    if (v) {
      return strings(*v, ',');
    } else {
      return std::nullopt;
    }
  }
  
  template<class T>
  void
  ConfigLoader::traceDefault(path const& p, T const& def) const {
    log() << std::boolalpha << "Relying on default value '" << def << "' for property '" << str(p) << "'";
    if (!allowDefaults()) {
      log() << " which is not authorized";
    }
    log() <<".\n";
  }

  template<>
  void
  ConfigLoader::traceDefault(path const& p, std::vector<std::string> const& def) const {
    if (def.size() == 1) {
      traceDefault(p, def[0]);
    } else {
      std::ostringstream fmt;
      std::copy(def.begin(), def.end(), std::ostream_iterator<std::string>(fmt, ","));
      traceDefault(p, fmt.str());
    }
  }
  
  template<typename T>
  std::optional<T>
  ConfigLoader::pget_optional(path const& p) const {
    auto v = cfg().get_optional<T>(p);
    if (v) {
      return *v;
    } else {
      return std::nullopt;
    }
  }

  template<typename T>
  T
  ConfigLoader::pget(path const& p, T const& def) const {
    auto v = pget_optional<T>(p);
    if (v) {
      return *v;
    } else {
      traceDefault(p, def);
      if (!allowDefaults()) {
	std::exit(EX_CONFIG);
      }
      return v ? *v : def;
    }
  }

  template<typename T>
  T
  ConfigLoader::pget(path const& p) const {
    return cfg().get<T>(p);
  }

  DiskPhysic::Density::Cavity
  ConfigLoader::extractCavity() const {
    return {pget<real>(DISK/DENSITY/CAVITY/RADIUS, 0),
	    pget<real>(DISK/DENSITY/CAVITY/RATIO,  1),
	    pget<real>(DISK/DENSITY/CAVITY/WIDTH,  1)};
  }
  
  CodeUnits
  ConfigLoader::extractCodeUnits() const {
    return CodeUnits(pget<real>(DISK/CODE_UNITS/SEMI_MAJOR_AXIS, 5.2),
		     pget<real>(DISK/CODE_UNITS/CENTRAL_BODY/MASS, 1),
		     pget<real>(DISK/CODE_UNITS/CENTRAL_BODY/RADIUS, 1),
		     pget<real>(DISK/CODE_UNITS/CENTRAL_BODY/TEMPERATURE, 4370));
  }
  
  GasShape
  ConfigLoader::extractShape() const {
    return {pget<real>(DISK/ASPECT_RATIO),
            pget<real>(DISK/GRID/RADII/MIN),
            pget<real>(DISK/GRID/RADII/MAX),
            pget<real>(DISK/GRID/OPENING, 0.001),
            pget<bool>(DISK/GRID/HALF, false)};
  }

  DiskPhysic::Viscosity
  ConfigLoader::extractViscosity() const {
    bool          artificial = pget<bool>(DISK/VISCOSITY/ARTIFICIAL, false);
    std::string   type       = pget<std::string>(DISK/VISCOSITY/TYPE);
    return { artificial, type, Config(cfg().get_child(DISK/VISCOSITY/TYPE)) };
  }
    
  std::optional<DiskPhysic::Adiabatic::Radiative::Star>
  ConfigLoader::extractRadiativeStar() const {
    if (enabled(cfg(), DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/STAR_RADIATION)) {
      return DiskPhysic::Adiabatic::Radiative::Star(pget<real>(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/STAR_RADIATION/SHADOW_ANGLE));
    } else {
      return std::nullopt;
    }
  }

  std::optional<DiskPhysic::Adiabatic::Cooling>
  ConfigLoader::extractCooling() const {
    if (enabled(cfg(), DISK/ADIABATIC_EOS/COOLING)) {
      std::string typeStr = pget<std::string>(DISK/ADIABATIC_EOS/COOLING);
      std::istringstream fmt(typeStr);
      CoolingType type;
      fmt >> type;
      if (fmt.fail()) {
	type = CoolingType::BETA;
	std::cerr << "Warning: invalid cooling type '" << typeStr << "', assuming " << type << '\n';
      }
      std::optional<DiskPhysic::Adiabatic::Cooling> cooling;
      switch (type) {
      case CoolingType::BETA: 
	cooling = DiskPhysic::Adiabatic::BetaCooling({ pget<real>(DISK/ADIABATIC_EOS/COOLING/TIMESCALE) });
	break;
      case CoolingType::RADIATIVE:
	cooling = DiskPhysic::Adiabatic::RadiativeCooling{};
	break;
      }
      return cooling;
    } else {
      return std::nullopt;
    }
  }

  std::optional<DiskPhysic::Adiabatic::Radiative>
  ConfigLoader::extractRadiative() const {
    if (enabled(cfg(), DISK/ADIABATIC_EOS/FULL_ENERGY_EOS)) {
      auto star = extractRadiativeStar();
      ptree empty;
      DiskPhysic::Adiabatic::Radiative::Opacity opacity{pget(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/OPACITY_LAW,       OpacityLaw::BELL_LIN), Config{}};
      if (opacity.type == OpacityLaw::CONSTANT) {
	Config::Properties map{{"kappa", pget<std::string>(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/OPACITY_LAW/VALUE)}};
	opacity.config = Config{map};
      }
      DiskPhysic::Adiabatic::Radiative::Solver solver(
						      {pget(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/SOLVER, std::string("legacy")),
						       Config(cfg().get_child(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/SOLVER, empty))
						      });
      DiskPhysic::Adiabatic::Radiative radiative(opacity,
						 solver,
						 pget(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/Z_BOUNDARY_TEMPERATURE, real(3)),
						 pget(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/DUST_TO_GAS,        real(0.01)),
                                                 pget_optional<real>(DISK/ADIABATIC_EOS/FULL_ENERGY_EOS/ENERGY_TIME_STEP),
                                                 star);
      return radiative;
    } else {
      return std::nullopt;
    }
  }

  std::optional<DiskPhysic::Adiabatic>
  ConfigLoader::extractAdiabatic() const {
    if (enabled(cfg(), DISK/ADIABATIC_EOS)) {
      auto radiativeCfg = extractRadiative();
      auto coolingCfg   = extractCooling();
      real index = pget<real>(DISK/ADIABATIC_EOS/INDEX, 1.4);
      return std::make_optional<DiskPhysic::Adiabatic>(radiativeCfg, coolingCfg, index);
    } else {
      return std::nullopt;
    }
  }
  
  DiskPhysic::StarAccretion::Wind
  ConfigLoader::extractWind() const {
    return DiskPhysic::StarAccretion::Wind(pget<real>(DISK/STAR_ACCRETION/WIND/ACTIVE_DENSITY),
					   pget<real>(DISK/STAR_ACCRETION/WIND/FILTER));
  }
  
  std::optional<DiskPhysic::StarAccretion>
  ConfigLoader::extractStarAccretion() const {
    if (cfg().get_child_optional(DISK/STAR_ACCRETION)) {
      StarAccretion type = pget(DISK/STAR_ACCRETION/TYPE, StarAccretion::CONSTANT);
      real rate = pget<real>(DISK/STAR_ACCRETION/RATE);
      std::optional<DiskPhysic::StarAccretion::Wind> wind;
      switch (type) {
      case StarAccretion::CONSTANT:
	wind = std::nullopt;
	break;
      case StarAccretion::WIND:
	wind = extractWind();
	break;
      }
      return DiskPhysic::StarAccretion(type, rate, wind);
    } else {
      return std::nullopt;
    }
  }
  
  DiskPhysic::Density
  ConfigLoader::extractDensity() const {
    DiskPhysic::Density density = {
      pget<real>(DISK/DENSITY/SLOPE),
      pget<real>(DISK/DENSITY/START),
      pget<real>(DISK/DENSITY/MINIMUM, DENSITY_MIN),
      extractCavity() 
    };

    return density;
  }
  
  DiskPhysic::Smoothing
  ConfigLoader::extractSmoothing() const {
    DiskPhysic::Smoothing smoothing  = {
      pget<bool>(DISK/SMOOTHING/CHANGE, false),
      pget<bool>(DISK/SMOOTHING/FLAT, false),
      pget<real>(DISK/SMOOTHING/TAPER, real(1e-7)),
      pget<real>(DISK/SMOOTHING/SIZE, real(0.5)) };
    return smoothing;
  }
  
  DiskPhysic::Referential
  ConfigLoader::extractReferential() const {
    bool indirectForces       = pget<bool>(DISK/REFERENTIAL/INDIRECT_FORCES, true);
    DiskReferential reftype   = pget<DiskReferential>(DISK/REFERENTIAL/TYPE);
    std::optional<std::string> planet = pget_optional<std::string>(DISK/REFERENTIAL/GUIDING_PLANET);

    using Ref = DiskPhysic::Referential;
    switch (reftype) {
    case DiskReferential::CONSTANT:
      return Ref::constant(pget<real>(DISK/REFERENTIAL/OMEGA), indirectForces);
      break;
    case DiskReferential::COROTATING:
      return Ref::corotating(indirectForces,
                             pget(DISK/REFERENTIAL/GUIDING_CENTER, false),
                             *planet);
      break;
    default:
      std::abort();
    }
  }

  std::shared_ptr<Grid const>
  ConfigLoader::extractGridCoords() const {
    path root = DISK/GRID;
    size_t const nr = pget<size_t>(root/RADII);
    size_t const ni = pget<size_t>(root/LAYERS, 1);
    size_t const ns = pget<size_t>(root/SECTORS);
    return  Grid::make(extractShape(),
                       {nr,ni,ns},
                       pget<GridSpacing>(root/RADII/SPACING, GridSpacing::ARITHMETIC));
  }

  std::variant<real,PlanetarySystemPhysic::Planet::MassTaper>
  ConfigLoader::extractMass(path init) const {
    if (pget<std::string>(init/MASS) == "TAPER") {
      PlanetarySystemPhysic::Planet::MassTaper taper = {
        pget<real>(init/MASS/INITIAL_MASS),
        pget<real>(init/MASS/FINAL_MASS),
        pget<real>(init/MASS/BEGIN_TIME),
        pget<real>(init/MASS/END_TIME)
      };
      return taper;
    } else {
      return pget<real>(init/MASS);
    }
  }
  template<typename T>
  std::optional<T> sopt(boost::optional<T> o) {
    if (o) { return *o; }
    else   { return std::nullopt; }
  }
  
  ConfigLoader::PlanetMap
  ConfigLoader::extractPlanetsPhysic() const {
    PlanetMap planets;
    path proot = PLANETARY_SYSTEM/PLANETS;
    auto planetsCfg = cfg().get_child_optional(proot);
    if (planetsCfg) {
      for( auto const& [name, cfg] : *planetsCfg) {
        path pproot = proot/path(name);
        opt<real> accretionTimeInverse = sopt(cfg.get_optional<real>(ACCRETION_TIME_INVERSE));
        opt<real> accretionTime        = sopt(cfg.get_optional<real>(ACCRETION_TIME));
        opt<real> solidAccretion       = sopt(cfg.get_optional<real>(SOLID_ACCRETION));
        if (solidAccretion && *solidAccretion == 0) {
          solidAccretion = std::nullopt;
        }
        opt<real> sma  =  pget_optional<real>(pproot/INIT/SEMI_MAJOR_AXIS);
        if (!sma) {
          sma = pget_optional<real>(pproot/INIT/DISTANCE);
        }
        if (!accretionTime && accretionTimeInverse) {
          accretionTime = 1/(*accretionTimeInverse);
        }
        PlanetarySystemPhysic::Planet planet{name,
                                             extractMass(pproot/INIT),
                                             {   *sma,
                                                 pget<real>(pproot/INIT/ECCENTRICITY),
                                                 pget<real>(pproot/INIT/INCLINATION, 0),
                                                 pget<real>(pproot/INIT/MEAN_LONGITUDE, 0),
                                                 pget<real>(pproot/INIT/LONGITUDE_OF_PERICENTER, 0),
                                                 pget<real>(pproot/INIT/LONGITUDE_OF_NODE, 0)},
                                             accretionTime,
                                             pget<bool>(pproot/FEEL/DISK, false),
                                             pget<bool>(pproot/FEEL/OTHERS, false),
                                             solidAccretion,
                                             pget_optional<real>(pproot/INIT/RADIUS),
                                             pget_optional<real>(pproot/INIT/HILL_RADIUS)};
        planets[name] = planet;
      }
    }
    return planets;
  }
  
  std::map<std::string, std::string> 
  ConfigLoader::extractMap(pt::path const& p) const {
    std::map<std::string, std::string> map;
    auto m = cfg().get_child_optional(p);
    if (m) {
      for (auto const& pv : *m) {
        std::string name  = pv.first;
        std::string value = pv.second.data();
        map[name] = value;
      }
    }
    return map;
  }

  std::optional<PlanetarySystemPhysic>
  ConfigLoader::extractSystemPhysic() const {
    // fill planet cache
    auto planets = extractPlanetsPhysic();
    if (!planets.empty()) {
      PlanetarySystemPhysic::Engine engine = { 
        pget(PLANETARY_SYSTEM/ENGINE, RUNGE_KUTTA),
        extractMap(PLANETARY_SYSTEM/ENGINE)};
      return PlanetarySystemPhysic{
        planets,
        pget(PLANETARY_SYSTEM/EXCLUDE_HILL, false),
        pget(PLANETARY_SYSTEM/ACCRETE_ONTO_PLANET, false),
        engine};
    } else {
      return std::nullopt;
    }
  }
  
  PluginConfig
  ConfigLoader::extractTrackingConfig() const {
    if (cfg().empty()) {
      throw std::logic_error("Asking for tracking configuration extraction while not configuration loaded.\n");
    }
    auto trackersCfg = cfg().get_child_optional(OUTPUT/TRACKERS);
    if (!trackersCfg) {
      myLog << "No tracker found.\n"
            << "It doesn't make much sense to launch a simulation without any trace\n";
      return PluginConfig{};
    } else {
      ConfigMap trackers;
      for( auto const& [name,cfg] : *trackersCfg) {
        trackers.push_back({name, Config(cfg)});
      }
      return trackers;
    }
  }

  ConfigMap
  ConfigLoader::extractConfigMap(path location) const {
    auto node = cfg().get_child_optional(location);
    if (!node) {
      return {};
    } else {
      ConfigMap configs;
      for( auto const& [name,cfg] : *node) {
        configs.push_back({name, Config{cfg}});
      }
      return configs;
    }
  }

  shptr<DiskPhysic const>
  ConfigLoader::extractPhysic() const {
    if (cfg().empty()) {
      log() << "Asking for physic extraction while not configuration loaded.\n";
      return nullptr;
    }
    CodeUnits                         units       = extractCodeUnits();
    GasShape                          shape       = extractShape();
    DiskPhysic::Referential           referential = extractReferential();
    auto                              bcNames     = pget_optional<std::vector<std::string>>(DISK/BOUNDARIES);
    PluginConfig                      boundaries;
    if (bcNames) {
      ConfigMap cfgs;
      int step = 0;
      for (auto name : *bcNames) {
        std::cerr << "Old boundaries condition handler specification. Translating.\n";
        cfgs.push_back({name, Config{{{snake(STEP), std::to_string(step++)}}}});
      }
      boundaries  = PluginConfig(cfgs);
    }
    PluginConfig                      init        = extractConfigMap(DISK/INIT);
    PluginConfig                      steps       = extractConfigMap(DISK/STEPS);    
    std::optional<DiskPhysic::StarAccretion>      starAcc = extractStarAccretion();
    DiskPhysic::Viscosity             viscosity   = extractViscosity();
    std::optional<DiskPhysic::Adiabatic> adiabatic = extractAdiabatic();
    DiskPhysic::Density               density     = extractDensity();
    DiskPhysic::Smoothing             smoothing   = extractSmoothing();
    opt<PlanetarySystemPhysic>        planets     = extractSystemPhysic();
    real                              CFLSecurity = pget<real>(DISK/CFL_SECURITY, 0.5);
    real                              kdrag       = pget<real>(DISK/DRAGGING_COEF, real(1));
    real                              hillCutFact = pget<real>(DISK/HILL_CUT_FACTOR, real(0.6));
    real                              timeStep    = pget<real>(OUTPUT/TIME_STEP);
    Transport                         transport   = pget<Transport>(DISK/TRANSPORT, Transport::FAST);
    real                              flaringIndex = pget<real>(DISK/FLARING_INDEX, 0);
    real                              meanMolecularWeight = pget<real>(DISK/MEAN_MOLECULAR_WEIGHT, DEFAULT_MEAN_MOLECULAR_WEIGHT);
    
    auto physic = DiskPhysic::make(std::move(units),
                                   flaringIndex,
                                   shape,
                                   referential,
                                   boundaries,
                                   init,
                                   steps,
                                   starAcc,
                                   viscosity,
                                   adiabatic,
                                   density,
                                   smoothing,
                                   planets,
                                   CFLSecurity,
                                   kdrag,
                                   hillCutFact,
                                   transport,
                                   meanMolecularWeight);
    {
      bool ok = true;
      for(auto const& c : myTreeChecks) {
        if (!c(myCfgRoot, myLog)) {
          ok = false;
        }
      }
      for(auto const& c : myTreePhysicChecks) {
        if (!c(myCfgRoot, *physic, myLog)) {
          ok = false;
        }
        myLog << std::flush;
      }
      if (!ok) { std::abort(); }
    }
    return physic;
  }
}
