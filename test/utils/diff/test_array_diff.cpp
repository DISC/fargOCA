
#include <iostream>
#include <limits>

#include <Kokkos_Core.hpp>

#include "precision.hpp"
#include "arrays.hpp"
#include "environment.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;
namespace kk = Kokkos;
using std::size_t;

template<size_t N> using LocalizedDiff = AbsRelDiffLocScalar<real,N>;
template<class T>  using Opt           = std::optional<T>;
int
main() {
  Environment env;

  auto checkDiff = []<size_t N>(std::optional<LocalizedDiff<N>> result, LocalizedDiff<N> expected) -> bool
    {
      bool passed = true;
      if (!result) {
        std::cerr << "bad array sizes.\n";
        passed = false;
      } else if (result->loc != expected.loc) {
        std::cerr << "bad location.\n";
        passed = false;
      } else if (std::abs(result->val.absolute - expected.val.absolute) > 1e-15) {
        std::cerr << "bad absolute diff max, delta is: " << std::abs(expected.val.absolute - result->val.absolute) << '\n';
        passed = false;
      } else if (std::abs(result->val.relative - expected.val.relative) > 1e-15) {
        std::cerr << "bad relative diff max, delta is: " << std::abs(expected.val.relative - result->val.relative) << '\n';
        passed = false;
      }
      if (passed) {
        std::cout << "PASSED\n";
      } else {
        std::cout << "FAILED\n";
      } 
      return passed;
    };
  
  bool passed {true};
  real const delta = 0.1;
  {
    size_t  constexpr N1 {10};
    arr1d<real> gold{"gold", N1};
    arr1d<real> test{"test", N1};
    size_t iloc {5};
    
    auto gold_value = KOKKOS_LAMBDA(size_t i) -> real { return i+i/real(i+1); };
    auto test_value = KOKKOS_LAMBDA(size_t i) -> real { return gold_value(i) + (i==iloc? delta : 0); };
    kk::parallel_for("init", N1,
                     KOKKOS_LAMBDA(size_t i) {
                       gold(i) = gold_value(i);
                       test(i) = test_value(i);
                     });
    std::cout << "1D test\n";
    for (size_t i = 0; i<N1; ++i) {
      std::cout << "i: " << i << " -> "<< gold_value(i) << " vs " << test_value(i) << '\n';
    }
    Opt<LocalizedDiff<1>> localizedDiff { maxAbsRelDiff(gold,test) };
    LocalizedDiff<1>      expected { {delta, delta/std::abs(gold_value(iloc))}, {iloc}};
    
    passed &= checkDiff(localizedDiff, expected);
  }
  {
    size_t  constexpr N1 {10};
    size_t  constexpr N2 {5};    
    arr2d<real> gold{"gold", N1, N2};
    arr2d<real> test{"test", N1, N2};
    using Idx = kk::Array<size_t,2>;
    Idx iloc {N1/2,N2/2};
    auto gold_value = KOKKOS_LAMBDA(size_t i, size_t j) -> real { return 1+i+kk::sin(j*kk::numbers::pi/N2); };
    auto test_value = KOKKOS_LAMBDA(size_t i, size_t j) -> real { return gold_value(i,j) + (iloc == Idx{i,j} ? delta : 0); };
    kk::parallel_for("init", range(gold),
                     KOKKOS_LAMBDA(size_t i, size_t j) {
                       gold(i,j) = gold_value(i,j);
                       test(i,j) = test_value(i,j);
                     });
    std::cout << "2D test\n";
    for (size_t i = 0; i<N1; ++i) {
      std::cout << "i: " << i << " -> ";
      for (size_t j = 0; j<N2; ++j) {
        std::cout << gold_value(i,j) << " vs " << test_value(i,j) << '\n';
      }
    }
    Opt<LocalizedDiff<2>> localizedDiff { maxAbsRelDiff(gold,test) };
    LocalizedDiff<2>      expected { {delta, delta/std::abs(gold_value(iloc[0], iloc[1]))}, iloc};
    
    passed &= checkDiff(localizedDiff, expected);
  }
  {
    size_t  constexpr N1 {7}; 
    size_t  constexpr N2 {5};    
    size_t  constexpr N3 {3};    
    arr3d<real> gold{"gold", N1, N2, N3};
    arr3d<real> test{"test", N1, N2, N3};
    using Idx = kk::Array<size_t,3>;
    Idx iloc {N1/2,N2/2,N3/2};
    auto gold_value = KOKKOS_LAMBDA(size_t i, size_t h, size_t j)
      {
       return 1+i+kk::sin(j*kk::numbers::pi/N2)+kk::cos(h*kk::numbers::pi/N3);
      };
    auto test_value = KOKKOS_LAMBDA(size_t i, size_t h, size_t j)
      {
       return gold_value(i,h,j) + (iloc == Idx{i,h,j} ? delta : 0);
      };
    kk::parallel_for("init", range(gold),
                     KOKKOS_LAMBDA(size_t i, size_t h, size_t j) {
                       gold(i,h,j) = gold_value(i,h,j);
                       test(i,h,j) = test_value(i,h,j);
                     });
    std::cout << "3D test\n";
    for (size_t i = 0; i<N1; ++i) {
      std::cout << "i: " << i << " -> ";
      for (size_t h = 0; h<N2; ++h) {
        for (size_t j = 0; j<N3; ++j) {
          std::cout << gold_value(i,h,j) << " vs " << test_value(i,h,j) << ", \t";
        }        
      }
      std::cout << 'n';
    }                                
    Opt<LocalizedDiff<3>> localizedDiff { maxAbsRelDiff(gold,test) };
    LocalizedDiff<3>      expected { {delta, delta/std::abs(gold_value(iloc[0], iloc[1], iloc[2]))}, iloc};
    
    passed &= checkDiff(localizedDiff, expected);
  }
  return passed ? 0 : 1;
}
