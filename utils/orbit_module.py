import numpy as np
import matplotlib.pyplot as plt
import math


def orbit(erre = 1):

        x = erre	
        y = -np.pi
        theta = 0.
        x_s = [x]
        y_s = [y]
        npoints = 100
        dtheta =  2*np.pi /float(npoints)

        for i in range(1,npoints+1):
                theta =  -np.pi+float(i)*dtheta
                x = erre
                y = theta
                x_s.append(x)
                y_s.append(y)
        return y_s,x_s



