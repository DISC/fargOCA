// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <sstream>
#include <istream>
#include <iterator>
#include <functional>
#include <cassert>
#include <cstddef>
#include <limits>

#include <nlohmann/json.hpp>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include <boost/algorithm/string.hpp>

#include "grid.hpp"
#include "loops.hpp"
#include "log.hpp"
#include "jsonio.hpp"
#include "configProperties.hpp"
#include "simulationConfig.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;
  namespace kkm = Kokkos::Experimental;

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;
  
#define LBD KOKKOS_LAMBDA
  
  std::ostream&
  operator<<(std::ostream& out, GridPosition  p) {
    switch(p) {
    case GridPosition::INF:
      out << "lower";
      break;
    case GridPosition::MED:
      out << "median";
      break;
    default:
      out << "<pos. tag err. (" << int(p) << ')';
      out.setstate(std::ios_base::failbit);
      break;
    }
    return out;
  }
  
  std::istream& operator>>(std::istream& in, GridPosition& p) {
    std::string str;
    in >> str;
    if (in) {
      boost::to_upper(str);
      if (str=="LOWER") {
        p = GridPosition::INF;
      } else if(str=="MEDIAN") {
        p = GridPosition::MED;
      } else {
        p = static_cast<GridPosition>(-1);
        in.setstate(std::ios_base::failbit);
      }
    }
    return in;
  }
  
  template<> std::vector<GridPosition> const& enumValues<GridPosition>() {
    static std::vector<GridPosition> values = { GridPosition::INF, GridPosition::MED };
    return values;
  }

  bool
  operator==(GridPositions const& p1, GridPositions const& p2) {
    return (p1.radius == p2.radius 
            && p1.layer == p2.layer
            && p1.sector == p2.sector);
  }
  
  bool
  operator!=(GridPositions const& p1, GridPositions const& p2) {
    return !(p1 == p2);
  }

  bool
  operator==(GridSizes const& s1, GridSizes const& s2) {
    return s1.nr == s2.nr && s1.ni == s2.ni && s1.ns == s2.ns; 
  }
  
  bool operator!=(GridSizes const& s1, GridSizes const& s2) { return !(s1 == s2); }

  using configProperties::str;
  namespace cp = configProperties;
  
  void
  to_json(nlohmann::json& j, GridSizes const& sz) {
    j[snake(cp::RADII)]   = sz.nr;
    j[snake(cp::LAYERS)]  = sz.ni;
    j[snake(cp::SECTORS)] = sz.ns;
  }
  void
  from_json(nlohmann::json const& j, GridSizes& sz) {
    sz = {
      j.at(snake(cp::RADII)).get<std::size_t>(),
      j.at(snake(cp::LAYERS)).get<std::size_t>(),
      j.at(snake(cp::SECTORS)).get<std::size_t>() };
  }
  
  std::shared_ptr<Grid const>
  Grid::make(GasShape const& shape, nlohmann::json const& j) {
    GridSizes sz;
    j.get_to(sz);
    return make(shape, sz, j.at(snake(cp::SPACING)).get<GridSpacing>());
  }

  void
  to_json(nlohmann::json& j, Grid const& g) {
    j = g.sizes();
    j[snake(cp::SPACING)] = g.radialSpacing();
  }

  std::ostream& 
  operator<<(std::ostream& out, GridSizes const& gs) {
    out << "[nr:" << gs.nr << ",ni:" << gs.ni << ",ns:" << gs.ns << "]";
    return out;
  }
  
  Grid::Grid(Grid const& global, range_t rg)
    : myShape(global.myShape),
      mySizes({size(rg), global.sizes().ni, global.sizes().ns}),
      myRadialOffset(rg.first),
      myGlobal(global.shared())
  {
    assert(size(rg) < global.sizes().nr);
  }
  
  Grid::Grid(GasShape const& shape,
             GridSizes const& gsz,
             GridSpacing k)
    : myShape(shape),
      mySizes(gsz),
      myRadialOffset(0u),
      myGlobal()
  {
    if (shape.half && mySizes.ni == 1) {
      std::cerr << "Can't have a half flat disk.\n";
      std::abort();
    } 
  }
  
  Grid::Grid(GasShape const& shape, HighFive::Group const& group) 
    : myShape(shape),
      mySizes(group.getAttribute(snake(cp::SIZES)).read<GridSizes>()),
      myRadialOffset(0u),
      myGlobalNRadii(mySizes.nr),
      myGlobal()
  {}
  
  Grid::~Grid() {}

  std::shared_ptr<Grid const>
  Grid::make(GasShape const& shape, HighFive::Group const& group) {
    GridSpacing spacing = group.getAttribute(snake(cp::SPACING)).read<GridSpacing>();
    switch(spacing) {
    case ARTH: return Coords<ARTH>::make(shape, group);
    case LOGR: return Coords<LOGR>::make(shape, group);
    }
    std::abort();
  }

  std::shared_ptr<Grid const>
  Grid::make(GasShape const& shape, GridSizes sizes, GridSpacing spacing) {
    switch(spacing) {
    case ARTH: return Coords<ARTH>::make(shape, sizes);
    case LOGR: return Coords<LOGR>::make(shape, sizes);
    }
    std::abort();
  }
  
  std::shared_ptr<Coords<ARTH> const>
  Coords<ARTH>::make(GasShape const& shape, HighFive::Group const& group) {
    return std::shared_ptr<Coords<ARTH> const>(new Coords<ARTH>(shape, group));
  }

  std::shared_ptr<Coords<ARTH> const>
  Coords<ARTH>::make(GasShape const& shape, GridSizes const& sizes) {
    return std::shared_ptr<Coords<ARTH> const>(new Coords<ARTH>(shape, sizes));
  }

  std::shared_ptr<Grid const>
  Coords<ARTH>::clone(range_t radialRange) const {
    if (size(radialRange) == sizes().nr) {
      return shared();
    } else {
      return std::shared_ptr<Coords<ARTH> const>(new Coords<ARTH>(*this, radialRange));
    }
  }

  GridSpacing Coords<ARTH>::radialSpacing() const { return ARTH; }

  std::shared_ptr<Coords<LOGR> const>
  Coords<LOGR>::make(GasShape const& shape, HighFive::Group const& group) {
    return std::shared_ptr<Coords<LOGR> const>(new Coords<LOGR>(shape, group));
  }

  std::shared_ptr<Coords<LOGR> const>
  Coords<LOGR>::make(GasShape const& shape, GridSizes const& sizes) {
    return std::shared_ptr<Coords<LOGR> const>(new Coords<LOGR>(shape, sizes));
  }
  
  std::shared_ptr<Grid const>
  Coords<LOGR>::clone(range_t radialRange) const {
    if (size(radialRange) == sizes().nr) {
      return shared();
    } else {
      return std::shared_ptr<Coords<LOGR> const>(new Coords<LOGR>(*this, radialRange));
    }
  }

  GridSpacing Coords<LOGR>::radialSpacing() const { return LOGR; }
  
  namespace h5details {
    bool
    writeH5Coords(HighFive::Group&  file, std::string name, arr1d<real const> coords) {
      using namespace HighFive;
      arr1d<real const>::HostMirror hcoords = kk::create_mirror_view(coords);
      kk::deep_copy(hcoords, coords);
      
      hsize_t dim = coords.size();
      DataSet coordsDS = file.createDataSet(name, DataSpace(coords.size()), create_datatype<real>());
      coordsDS.write(hcoords.data());
      return true;
    }
    template<class Fct>
    bool
    writeH5Coords(HighFive::Group&  group, std::string name, Fct fct, size_t const n) {
      using namespace HighFive;
      size_t const nb = n+1;
      harr1d<real> coords(name, nb);
      kk::parallel_for("stored coords", hRange1D(nb),
                       KOKKOS_LAMBDA(size_t const i) { coords(i) = fct(i); });
      DataSet coordsDS = group.createDataSet(name, DataSpace(coords.size()), create_datatype<real>());
      coordsDS.write(coords.data());
      return true;
    }
  }
  arr1d<real const>
  readH5Coords(HighFive::Group const&  file, std::string name) {
    using namespace HighFive;
    DataSet   gridDS = file.getDataSet(name);
    arr1d<real> coords(name, gridDS.getDimensions()[0]);
    arr1d<real>::HostMirror hcoords = kk::create_mirror_view(coords);
    gridDS.read(hcoords.data());
    kk::deep_copy(coords, hcoords);
    return coords;
  }
  
  void
  Grid::writeH5(HighFive::Group group) const {
    h5::writeAttribute(group, snake(cp::SPACING), radialSpacing());
    h5::writeAttribute(group, snake(cp::SIZES), sizes());
  }

  void
  GridCoords::writeH5(HighFive::Group group) const {
    Grid::writeH5(group);
  }

  std::ostream&
  operator<<(std::ostream& out, GridSpacing s) {
    switch(s) {
    case ARTH:
      out << "ARITHMETIC";
      break;
    case LOGR:
      out << "LOGARITHMIC";
      break;
    default:
      out << "<myInv Grid Spacing>";
    }
    return out;
  }
  
  std::istream&
  operator>>(std::istream& in, GridSpacing& s) {
    std::string tk;
    in >> tk;
    boost::to_upper(tk);
    if (tk == "ARITHMETIC") {
      s = ARTH;
    } else if (tk == "LOGARITHMIC") {
      s = LOGR;
    } else {
      std::abort();
    }
    return in;
  }
  
  bool
  compatible(GridSizes const& sz1, GridSizes const& sz2, std::string label) {
    bool yes = sz1 == sz2;
    if (!yes) {
      if (!label.empty()) {
        std::cerr << label << ": ";
      }
      std::cerr << sz1 << " and " << sz2 << " are incompatible.\n";
    }
    return yes;
  }
  
  std::optional<GasShape>
  compatible(Grid const& c1, Grid const& c2) {
    if (c1.sizes() == c2.sizes() 
        && c1.radialSpacing() == c2.radialSpacing()
        && c1.radialOffset()  == c2.radialOffset()) {
      return diff(c1.shape(), c2.shape());
    } else {
      return std::nullopt;
    }
  }
  bool
  compatible(Grid const& c1, Grid const& c2, real tol) {
    std::optional<GasShape> c = compatible(c1, c2);
    return bool(c) && max(*c) < tol;
  }

  std::ostream&
  operator<<(std::ostream& out, Grid const& coords) {
    out << "{shape:" << coords.shape()
        << ",sz:" << coords.sizes()
        << ",sp:" << coords.radialSpacing()
        << "}";
    return out;
  }
  
  template<GridSpacing GS>
  void
  writeH5Coords(Coords<GS> const& grid, HighFive::Group group) {
    GridSizes sz = grid.sizes();
    h5details::writeH5Coords(group, "radii",   grid.radii(), sz.nr);
    h5details::writeH5Coords(group, "sectors", grid.theta(), sz.ns);
    h5details::writeH5Coords(group, "layers",  grid.phi(),   sz.ni);

  }
  template void writeH5Coords(Coords<ARTH>  const&, HighFive::Group);
  template void writeH5Coords(Coords<LOGR> const&, HighFive::Group);

  template<class FCT> 
  harr1d<real const> hostRadiiValues(FCT f, int nr) {
    harr1d<real> radii("radii", nr);
    Kokkos::parallel_for("build radii", Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0, nr),
                         [=](size_t const i) { radii(i) = f(i); });
    return radii;
  }

  template<GridSpacing GS> 
  harr1d<real const> hostRadiiValues(Coords<GS> const& coords, GridPosition pos, size_t n) {
    if (pos == GridPosition::MED) {
      return hostRadiiValues(coords.radiiMed(), n);
    } else {
      return hostRadiiValues(coords.radii(), n);
    }
  }
  
  harr1d<real const>
  hostRadiiValues(Grid const& grid, GridPosition pos, size_t n) {
    switch (grid.radialSpacing()) {
    case ARTH: return hostRadiiValues(grid.as<ARTH>(),  pos, n);
    case LOGR: return hostRadiiValues(grid.as<LOGR>(), pos, n);
    }
    std::abort();
  }

  harr1d<real const>
  hostRadiiValues(Grid const& grid, GridPosition pos) {
    return hostRadiiValues(grid, pos, grid.sizes().nr);
  }
  
  template<class FCT>
  harr1d<real const> hostValues(FCT f, size_t n) {
    harr1d<real> v("values", n);
    Kokkos::parallel_for(Kokkos::RangePolicy<Kokkos::DefaultHostExecutionSpace>(0u, n),
                         [=](size_t const i) { v(i) = f(i); });
    return v;
  }
  
  template<GridSpacing GS>
  harr1d<real const> hostLayersValues(Coords<GS> const& grid, GridPosition pos, size_t n) {
    if (pos == GridPosition::MED) {
      return hostValues(grid.phiMed(), n);
    } else {
      return hostValues(grid.phi(), n);
    }
  }
  
  harr1d<real const> hostLayersValues(Grid const& grid, GridPosition pos, size_t n) {
    switch (grid.radialSpacing()) {
    case ARTH: return hostLayersValues(grid.as<ARTH>(),  pos, n);
    case LOGR: return hostLayersValues(grid.as<LOGR>(), pos, n);
    }
    std::abort();
  }
  
  harr1d<real const> hostLayersValues(Grid const& grid, GridPosition pos) {
    return hostLayersValues(grid, pos, grid.sizes().ni);
  }


  template<GridSpacing GS>
  harr1d<real const> hostSectorsValues(Coords<GS> const& grid, GridPosition pos, size_t n) {
    if (pos == GridPosition::MED) {
      return hostValues(grid.thetaMed(), n);
    } else {
      return hostValues(grid.theta(), n);
    }
  }
  
  harr1d<real const> hostSectorsValues(Grid const& grid, GridPosition pos, size_t n) {
    switch (grid.radialSpacing()) {
    case ARTH: return hostSectorsValues(grid.as<ARTH>(),  pos, n);
    case LOGR: return hostSectorsValues(grid.as<LOGR>(), pos, n);
    }
    std::abort();
  }
  
  harr1d<real const> hostSectorsValues(Grid const& grid, GridPosition pos) {
    return hostSectorsValues(grid, pos, grid.sizes().ns);
  }
    
  template<class FCT> 
  arr1d<real const> devRadiiValues(FCT f, int nr) {
    arr1d<real> radii("radii", nr);
    Kokkos::parallel_for("build radii", nr,
                         LBD(size_t const i) { radii(i) = f(i); });
    return radii;
  }

  template<GridSpacing GS> 
  arr1d<real const> devRadiiValues(Coords<GS> const& coords, GridPosition pos, size_t n) {
    if (pos == GridPosition::MED) {
      return devRadiiValues(coords.radiiMed(), n);
    } else {
      return devRadiiValues(coords.radii(), n);
    }
  }
  
  template<GridSpacing GS>
  Coords<GS> const& 
  Grid::as() const {
    return *dynamic_cast<Coords<GS> const*>(this);
  }
  
  template Coords<LOGR> const& Grid::as<LOGR>() const;
  template Coords<ARTH> const& Grid::as<ARTH>() const;
  
  arr1d<real const>
  devRadiiValues(Grid const& grid, GridPosition pos, size_t n) {
    switch (grid.radialSpacing()) {
    case ARTH:  return devRadiiValues(grid.as<ARTH>(),  pos, n); break;
    case LOGR: return devRadiiValues(grid.as<LOGR>(), pos, n); break;
    }
    std::abort();
  }

  arr1d<real const>
  devRadiiValues(Grid const& grid, GridPosition pos) {
    return devRadiiValues(grid, pos, grid.sizes().nr);
  }

  std::function<real (size_t const)> 
  radialCoords(Grid const& grid) {
    switch (grid.radialSpacing()) {
    case ARTH: return grid.as<ARTH>().radii();  break;
    case LOGR: return grid.as<LOGR>().radii(); break;
    }
    std::abort();
  }

  std::function<real (size_t const)> 
  radialMedCoords(Grid const& grid) {
    switch (grid.radialSpacing()) {
    case ARTH: return grid.as<ARTH>().radiiMed();  break;
    case LOGR: return grid.as<LOGR>().radiiMed(); break;
    }
    std::abort();
  }
}

template<>
HighFive::DataType
HighFive::create_datatype<fargOCA::GridSizes>() {
  using namespace HighFive;
  static CompoundType 
    tp(
       {{"nr",    create_datatype<size_t>()},
           {"ni",    create_datatype<size_t>()},
             {"ns",     create_datatype<size_t>()}});
  return tp;
}
