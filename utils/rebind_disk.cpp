// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <optional>
#include <filesystem>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"

#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "scalarFieldInterpolators.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Map an existing disk to a new grid.\n"
        << '\t' << cmd << " --input-disk <idisk>.h5 --grid <grid>.h5 --output-disk <odisk>.h5 ... :"
        << " map <idisk>.h5 to grid <grid>.h5 and write into <odisk>.h5.\n";
    
  po::options_description opts(usage.str());
  
  std::ostringstream interpolators_doc;
  interpolators_doc << "The interpolator to be used during the rebinding. Possible values are:";
  for (std::string inter : ScalarFieldInterpolators::availables()) {
    interpolators_doc << " '" << inter << '\'';;
  }
  interpolators_doc << ". Default is '" << ScalarFieldInterpolators::defaultName() << "'.";

  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("adc", "converting for ADC (3D is assumed otherwise).")
    ("input-disk,i", po::value<std::string>(), "HDF5 file containing the existing disk to be rebinded.")
    ("output-disk,o", po::value<std::string>(), "HDF5 file where to write the rebinded disk.")
    ("grid,g", po::value<std::string>(), "HDF5 file containing the grid.")
    ("interpolation", po::value<std::string>(), interpolators_doc.str().c_str())
    ("grid-path,p", po::value<std::string>(), "The path in the grid file where to find the grid (default is root '/').");
  try {
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(opts).run();
    po::store(parsed, vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    return -1;
  }
  if (vm.count("help") > 0) {
    std::cout << opts;
    return 0;
  }
  for(std::string opt : {"input-disk", "output-disk", "grid" }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  std::string ofname = vm["output-disk"].as<std::string>();
  if (fs::exists(ofname)) {
    log(world) << "Output file '" << ofname << "' already exists.\nBye\n";
    return EX_USAGE;
  }
  if (world.size() > 1) {
    return EX_USAGE;
  }
  
  std::string ifname = vm["input-disk"].as<std::string>();
  auto disk = Disk::make(world, HF::File(ifname, HF::File::ReadOnly).getGroup("/"));
  std::string gfname = vm["grid"].as<std::string>();
  HF::File gfile(gfname, HF::File::ReadOnly);
  std::string gpath = vm.count("grid-path")>0 ? vm["grid-path"].as<std::string>() : std::string("/grid");
  auto nextCoords = Grid::make(disk->physic().shape, gfile.getGroup(gpath));
  auto nextGrid = GridDispatch::make(world, *nextCoords);
  ScalarFieldInterpolators const& interpolator = ( bool(vm.count("interpolation")) 
                                                   ? ScalarFieldInterpolators::get(vm["interpolation"].as<std::string>())
                                                   : ScalarFieldInterpolators::get());
  disk->rebind(*nextGrid, interpolator);
  fs::copy_file(ifname, ofname);
  HF::File ofile(ofname, HF::File::ReadWrite);
  disk->writeH5(ofile.getGroup("/"));
  return EX_OK;
}
