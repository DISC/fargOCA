// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "environment.hpp"
#include "loops.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace kk = Kokkos;
namespace HF = HighFive;

using std::optional;

#define LBD KOKKOS_LAMBDA

namespace {
  ScalarField make_Hill_tracer   (Disk const& disk, std::string name, Planet const& planet, real ratioMin, real ratioMax);
  ScalarField make_radial_tracer (Disk const& disk, std::string name, real rmin, real rmax);
}

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Add a marker to a disk ranging from radius <gas rmin> to <gas rmax>.\n"
        << '\t' << cmd << " <idisk>.h5 [option]+.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("name", po::value<std::string>(), "Name of the marker.")
    ("radius", 
     "Add a radial marker with an optional range parameters specified by the --min and --max "
     "options (field will be set to 0 outside the range). "
     "The gas span between <disk min> and <disk max> and the value will go from 0 to 1 in that range.")
    ("hill", 
     "Each planet has a Hill radius, this tracer initialize a field at 1 in a section of the Hill radius "
     "specified by --min and --max option. The name of the planet if provided through the --planet option.")
    ("min", po::value<real>(), "Usage depends of the main option.")
    ("max", po::value<real>(), "Usage depends of the main option.")
    ("planet", po::value<std::string>(), "Provide the planet name when needed.")
    ;
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  input_option.add("name", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  {
    std::vector<std::string> mandatory = {"disk", "name"};
    if (bool(vm.count("hill"))) {
      mandatory.push_back("planet");
    }
    for(std::string opt : mandatory) {
      if (vm.count(opt)==0) {
        std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                  << opt << '\n';
        std::exit(EX_USAGE);
      }
    }
  }
  Environment env(argc, argv);
  fmpi::communicator world;
  std::string ifname = vm["disk"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadWrite);
  HF::Group igroup = ifile.getGroup("/");
  auto disk = Disk::make(world, igroup);

  std::string name = vm["name"].as<std::string>();
  if (disk->hasTracer(name)) {
    std::cerr << "A tracer named '" << name << "' is already declared.\n";
    return EX_DATAERR;
  }
  if (vm.count("radius")) {
    real rmin = disk->physic().shape.radius.min;
    real rmax = disk->physic().shape.radius.max;
    if (vm.count("min")) {
      rmin = vm["min"].as<real>();
    }
    if (vm.count("max")) {
      rmax = vm["max"].as<real>();
    }
    disk->addTracer(make_radial_tracer(*disk, name, rmin, rmax));
  } else if (vm.count("hill")) {
    if (!disk->system()) {
      std::cerr << "No planetary system, Hill force don't apply.\n";
      return EX_USAGE;
    } 
    real rmin = 0; 
    real rmax = 1;
    if (vm.count("min")) {
      rmin = vm["min"].as<real>();
    }
    if (vm.count("max")) {
      rmax = vm["max"].as<real>();
    }
    if (!bool(vm.count("planet"))) {
      std::cerr << "Missing mandatory planet option\n";
      return EX_USAGE;
    }
    std::string planet = vm["planet"].as<std::string>();
    auto const& planets = disk->system()->planets();
    if (!disk->system()->hasPlanet(planet)) {
      std::cerr << "Could not find planet " << planet << " in planetary system.\n"
                << "please choose one of: ";
      for (auto const& p : planets) {
        std::cerr << p.first << " ";
      }
      std::cerr << '\n';
      return EX_DATAERR;
    }
    disk->addTracer(make_Hill_tracer(*disk, name, disk->system()->planet(planet), rmin, rmax));
  } else {
    std::cerr << "Missing trace type option.\n";
    std::cerr << opts;
    return EX_USAGE;
  }
  disk->writeH5(igroup);
  std::cout << "Added '" << name << "' tracer to disk.\n";
  return 0;
}

namespace {
  template<GridSpacing GS>
  ScalarField
  make_radial_tracer_impl(Disk const& disk, std::string name, real rmin, real rmax) {
    ScalarField res(name, disk.dispatch());
    arr3d<real>  field = res.data();
    auto const& coords = disk.dispatch().grid().template as<GS>();
    auto radMed = coords.radiiMed();
    real gasMin = disk.physic().shape.radius.min;
    real gasMax = disk.physic().shape.radius.max;    
    kk::parallel_for("Hill", fullRange(disk.dispatch().grid()),
                     LBD(int i, int h, int j) {
                       real const r = radMed(i);
                       field(i,h,j) = ((r < rmin || r > rmax)
                                       ? real(0) 
                                       : (r-gasMin)/(gasMax-gasMin));
                     });
    return res;
  }

  ScalarField
  make_radial_tracer(Disk const& disk, std::string name, real rmin, real rmax) {
    GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
    GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;
    switch(disk.dispatch().grid().radialSpacing()) {
    case ARTH: return make_radial_tracer_impl<ARTH>(disk, name, rmin, rmax);
    case LOGR: return make_radial_tracer_impl<LOGR>(disk, name, rmin, rmax);
    }
    std::abort();
  }

  
  ScalarField
  make_Hill_tracer(Disk const& disk, std::string name, Planet const& planet, real ratioMin, real ratioMax) {
    ScalarField res(name, disk.dispatch());
    arr3d<real> field = res.data();
    real const rhill = disk.system()->HillRadius(planet);
    CartesianGridCoords cartesian = disk.dispatch().cartesian();
    Triplet center = planet.position();
    std::cout << " For planet at " << center << " of Hill radius " << rhill << ", ";
    real const min2 = ipow<2>(ratioMin*rhill);
    real const max2 = ipow<2>(ratioMax*rhill);
    int set = 0;
    kk::parallel_reduce("Hill", fullRange(disk.dispatch().grid()),
                        LBD(int i, int h, int j, int ones) {
                          Triplet cell = cartesian.coords(i,h,j);
                          real distance2 = (cell-center).norm2();
                          if (min2 < distance2 && distance2 < max2) {
                            ++ones;
                            field(i,h,j) = 1;
                          } else {
                            field(i,h,j) = 0;
                          }                               
                        }, set);
    std::cout << "Did set " << set << " cells in Hill tracer between " << min2 << " and " << max2 << ". \n";
    return res;
  }
}
