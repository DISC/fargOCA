// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_STELLAR_RADIATION_SOLVER_H
#define FARGOCA_STELLAR_RADIATION_SOLVER_H

#include <memory>
#include <optional>

#include "diskPhysic.hpp"
#include "scalarField.hpp"
#include "disk.hpp"

namespace fargOCA {

  /// \brief The base class for radiative step engine.
  /// The concrete Engine will be responsible for performing the step.
  /// The concrete Engine is required to be constructibe through
  /// ConcreteEngine(Disk&) and ConcreteEngine(Disk&, HighFive::Group, int where).
  /// The first ctor will be call at startup, the later at restart.
  
  class StellarRadiationSolver {
  protected:
    /// \brief Initial constructor.
    /// Called on an otherwise built disk.
    StellarRadiationSolver(Disk& disk) : myDisk(disk) {}

  public:
    virtual ~StellarRadiationSolver();

    Disk&       disk()       { return myDisk; }
    Disk const& disk() const { return myDisk; }

    /// \brief Actually perform the step. Must be implemented by actual engin.
    virtual void advance(real dt)  = 0;

    /// \brief Called by Disk::writeH5. 
    /// This is you chance to save some state.
    virtual void checkpoint(std::optional<HighFive::Group> group, int where = 0) const = 0;
    
    /// \brief Called by Disk::importGas. 
    virtual void importGas(StellarRadiationSolver const& solver) = 0;
    /// \brief Called by Disk::rebind. 
    virtual void rebind(GridDispatch const& nextGrid, ScalarFieldInterpolators const& interpolators) = 0;
    /// \brief Called by Disk when collecting named field. 
    /// If this class has field to export, this is the place.
    virtual void collectFields(Disk::NamedFields::Map& map) const {}
    
    /// \brief Get an initial engine bound to a disk.
    /// The engine class must be construtible through Engine(disk). 
    /// \param name the engine name
    /// \param disk the disk to advance
    static std::unique_ptr<StellarRadiationSolver>
    initial(Disk& disk);
    /// \brief Get an restart engine bound to a disk. 
    /// The engine class must be construtible through Engine(disk,group,where). 
    /// \param name the engine name
    /// \param disk the disk to advance
    /// \param group The group were to retrive the needed restart data, if any.
    /// \param where the rank where the data will actually be available. Will need to be broadcasted in relevant.
    static std::unique_ptr<StellarRadiationSolver>
    restart(Disk& disk, HighFive::Group group);
    
    template <template <GridSpacing> class Engine> struct Declarator;
    
  public:
    typedef std::function<std::unique_ptr<StellarRadiationSolver> (Disk& disk)>                  InitialCreator;
    typedef std::function<std::unique_ptr<StellarRadiationSolver> (Disk& disk, HighFive::Group)> RestartCreator;
    
    static std::map<std::string, InitialCreator>& initialCreators();
    static std::map<std::string, RestartCreator>& restartCreators();
    static std::set<std::string> available();

  private:
    
    Disk& myDisk;
  };

  template <template <GridSpacing> class Engine>
  struct StellarRadiationSolver::Declarator {
    Declarator(std::string name) {
      StellarRadiationSolver::initialCreators().insert(std::make_pair(name,[](Disk& disk) { 
            std::unique_ptr<StellarRadiationSolver> instance;
            switch (disk.dispatch().grid().radialSpacing()) {
            case GridSpacing::ARITHMETIC:
              instance = std::make_unique<Engine<GridSpacing::ARITHMETIC>>(disk); break;
            case GridSpacing::LOGARITHMIC:
              instance = std::make_unique<Engine<GridSpacing::LOGARITHMIC>>(disk); break;
            }
            return instance;
          }));
      StellarRadiationSolver::restartCreators().insert(std::make_pair(name,[](Disk& disk, HighFive::Group grp) {
            std::unique_ptr<StellarRadiationSolver> instance;
            switch (disk.dispatch().grid().radialSpacing()) {
            case GridSpacing::ARITHMETIC:
              instance = std::make_unique<Engine<GridSpacing::ARITHMETIC>>(disk, grp); break;
            case GridSpacing::LOGARITHMIC:
              instance = std::make_unique<Engine<GridSpacing::LOGARITHMIC>>(disk, grp); break;
            }
            return instance;
          }));
    }

  };
}

#endif // FARGOCA_STELLAR_RADIATION_SOLVER_H
