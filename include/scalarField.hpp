// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_SCALAR_FIELD_H
#define FARGOCA_SCALAR_FIELD_H

#include <map>

#include <highfive/H5File.hpp>

#include "precision.hpp"
#include "arrays.hpp"
#include "gridDispatch.hpp"

namespace fargOCA {
  
  class ScalarFieldInterpolators;
  
  /// \brief Scalar field on a polar grid.
  /// Represent a 3D field of floating point values dispatched other a communicator.
  class ScalarField {
  public:
    
    /// \brief Construct a field with a given name and undefined value.
    /// \param name the field name, mostly used for tracing and debug.
    /// \param grid the description of the grid on which the field is dispatched
    /// \param pos the position of the field points w.r.t the grid. Default is cell center
    ScalarField(std::string name, GridDispatch const& grid, 
                GridPositions const& pos = GridPositions() );
    
    /// \brief Construct a field with the same value as an existing field interpolated for a new grid.
    /// \param values existing field
    /// \param grid the new grid
    /// \param interpolator used to compute the new values on the new grid.
    ScalarField(ScalarField const& values,
                GridDispatch const& dispatch,
                ScalarFieldInterpolators const& interpolator);
    /// \brief Shortcut for ScalarField(pg.name(), pg, g, defaultInterpolator)
    /// With some optimisations when simple cases are detected.
    ScalarField(ScalarField const& pg, GridDispatch const& g);
    explicit ScalarField(ScalarField const&);
    ScalarField(ScalarField&& );
    ScalarField& operator=(ScalarField&& g);

    ~ScalarField();
    
    GridDispatch const& dispatch() const { return *myDispatch; }
    /// \brief Is the field in a valid state ?
    /// Typically, a field is in an invald state when it has been moved. 
    bool valid() const { return bool(myDispatch); }
    ///\brief same as valid()
    operator bool() const { return valid();}
    std::string name() const { return data().label(); }
    
    GridPositions const& gridPos() const { return myGridPos; }

    /// The contingous data stored on this MPI process. Use for transition to kokko
    arr3d<real>        data()        { return myData; }
    /// The contingous data stored on this MPI process. Use for transition to kokko
    arr3d<real const>  data()  const { return myData; }

    /// The minimum field value on all MPI proc.
    real min() const;

    /// \brief Update the ghost over MPI communicator
    void setGhosts();

    static void setGhosts(std::vector<ScalarField*> const& fields);

    ///\brief Same as readH5(H5File(fname), path)
    void readH5(std::string fname, std::string path = "/");
    ///\brief Same as readH5(file.openGroup(path)
    void readH5(HighFive::File const& file, std::string path = "/");
    ///\brief Read from group.
    /// Global operation
    void readH5(HighFive::Group const& group);
    
    /// \brief Write field into group.
    /// \param group the optional group to write into, must be set at root only.
    /// \param root the MPI proc on which to write.
    void writeH5(std::optional<HighFive::Group> group, int writer) const;
    
    /// \brief Merge all the field on one contiguous memory area.
    /// \param where on which proc, result will be empty otherwise on the other procs
    std::optional<arr3d<real>> merged(int where) const;

    /// \brief Rebind to a new grid.    
    void rebind(GridDispatch const& newGrid, ScalarFieldInterpolators const& interpolators);
    void rebind(GridDispatch const& newGrid);
    void rebindTo(GridDispatch const& newGrid);

    void ensureCleanGhosts(int nb);
    void trashGhosts(int nb = 1);
    
    /// \brief Dispatch the input data other all procs.
    /// \param where on which proc the data is actually provided.
    /// \param data  to be dispatched onto proc
    void dispatch(std::optional<arr3d<real const>> data, int where);

  private:
    friend void swap(ScalarField& g1, ScalarField& g2);
    friend class Disk;
    
    ScalarField(std::string const& name, shptr<GridDispatch const> g, GridPositions const& pos);
    ///\brief Write the merged field data in a group.
    /// Local utility method.
    void writeField(HighFive::Group group, arr3d<real const> merged) const;    
    void convert(ScalarField const& prev, ScalarFieldInterpolators const& interpolators, int where);

    template<GridSpacing FromGS, GridSpacing ToGS> void rebindImpl(GridDispatch const& to_dispatch);
    
  private:
    shptr<GridDispatch const>    myDispatch;
    arr3d<real>                  myData;
    GridPositions                myGridPos;
    int                          myNbDirtyGhosts;
  };

  /// \brief Your std friendly swap.
  void swap(ScalarField& g1, ScalarField& g2);
}

namespace std {
  template<> 
  struct less<fargOCA::ScalarField> {
    bool operator()(fargOCA::ScalarField const& f1, fargOCA::ScalarField const& f2) const {
      return std::less<std::string>()(f1.name(), f2.name());
    }
  };
}
#endif
