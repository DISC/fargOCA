#include <iostream>
#include <limits>

#include <highfive/H5File.hpp>

#include "h5io.hpp"
#include "tuple.hpp"

using namespace fargOCA;
using namespace HighFive;

int
main() {
  Triplet t1(1,2,3);
  std::string attname = "triplet";
  std::string fname = "triplet.h5";
  
  {
    File  file(fname, File::Truncate);
    Group root = file.createGroup("/triplet");
    h5::writeAttribute(root, attname,t1);
    std::cout << "wrote " << t1 << '\n';
      
  }
  {
    Triplet t1out;
    File  file(fname, File::ReadOnly);
    Group root = file.getGroup("/triplet");
    h5::readAttribute(root, attname, t1out);
    std::cout << "read " << t1out << '\n';
    if ((t1-t1out).norm() < std::numeric_limits<real>::epsilon()*10) {
      std::cout << "PASSED\n";
      return 0;
    } else {
      std::cout << "FAILED\n";
      return -1;
    }
  }
}
