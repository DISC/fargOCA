// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>

#include "simulation.hpp"
#include "disk.hpp"
#include "allmpi.hpp"

#include "fargOCApy.hpp"

namespace py = pybind11;

namespace fargOCA {
  namespace py11 {
    void exportSimulation(py::module_& m) {
      py::class_<Simulation, 
                 std::shared_ptr<Simulation>>(m, "Simulation")
        .def(py::init<std::string>())
        .def(py::init([](fmpi::communicator const& comm, std::string diskpath) {
              return std::make_shared<Simulation>(comm, diskpath);
            }))
        .def_property_readonly("nbSteps", [](Simulation const& s) { return s.nbSteps(); })
        .def_property_readonly("step",    [](Simulation const& s) { return s.step(); })
        .def_property_readonly("disk",    [](Simulation const& s) { return s.disk().shared(); })
        .def("__repr__",
             [](Simulation const& env) {
               return "fargOCApy.Simulation snapshot";
             });
    }
  }
}
