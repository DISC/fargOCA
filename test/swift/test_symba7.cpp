// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "planetarySystem.hpp"
#include "swift/symba7_integrator_api.hpp"

int
main() {
  int n = 3;
  double t0 = 0;
  double time_max = 100;
  double dt = 0.25;
  
  auto s7 = std::make_unique<s7_integrator>(n, t0, false, 1.e-8);
  s7->set_boundaries(-1.0, 1000.0, -1.0, -1.0);
  s7->set_planet(0,
                 1.0,
                 0.0, 0.0, 0.0,
                 0.0, 0.0, 0.0, 
                 0.0, 0.0);
  s7->set_planet(1,
                 0.0001,
                 1.0, 0.0, 0.0,
                 0.0, 1.0, 0.0,
                 0.001, 0.03218297949);
  s7->set_planet(2,
                 0.0001, 
                 1.3782407725, 0.0, 0.0,
                 0.0, 0.8517996421, 0.0,
                 0.001, 0.04435589451);
  
  s7->write();
  while ((s7->get_time() <= time_max)
         && (s7->get_nb_bodies() > 1)) {
    std::cout << "Time: " << s7->get_time() << ' ';
    s7->step(dt);
    s7->write();
  }
  std::cout << '\n';
}
