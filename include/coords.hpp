// Copyright 2023, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_COORDS_H
#define FARGOCA_COORDS_H

#include <utility>
#include <type_traits>

#include <Kokkos_Core.hpp>

#include "precision.hpp"
#include "gasShape.hpp"
#include "fundamental.hpp"
#include "mathAddOns.hpp"

namespace fargOCA {
  typedef std::size_t size_t;
  
  struct RegularCoords {
    KOKKOS_INLINE_FUNCTION
    RegularCoords(real b, real d, size_t shft = 0u) : base(b), dx(d), localOffset(shft) {}
    KOKKOS_INLINE_FUNCTION
    real operator()(size_t const i) const { return base+(localOffset+i)*dx; }
    
    KOKKOS_INLINE_FUNCTION
    RegularCoords shifted(size_t offset) const { return RegularCoords{base, dx, localOffset+offset}; }
    
    real   const base;
    real   const dx;
    size_t const localOffset;
  };
  
  struct LogCoords {
    KOKKOS_INLINE_FUNCTION    
    LogCoords(real m, real s, size_t o) : rmin(m), scaling(s), localOffset(o) {}
    
    LogCoords(GasShape const& shape, size_t nr, size_t sif) 
      : LogCoords(shape.radius.min,
                  std::log(shape.radius.max/shape.radius.min)/nr,
                  sif) {}
    
    KOKKOS_INLINE_FUNCTION
    real operator()(size_t i) const {
      return rmin*std::exp(real(localOffset+i)*scaling);
    }
    
    KOKKOS_INLINE_FUNCTION
    LogCoords shifted(size_t offset) const { return LogCoords{rmin, scaling, localOffset+offset}; }
    
    real   const rmin;
    real   const scaling;
    size_t const localOffset;
  };
  
  template <class FCT>
  struct BarycentricMedCoords {
    KOKKOS_INLINE_FUNCTION
    BarycentricMedCoords(FCT f, int d) : fct(f), dim(d) {}
    
    KOKKOS_INLINE_FUNCTION
    real operator()(size_t const i) const {
      if (dim == 3) {
        return (3*(ipow<4>(fct(i+1))-ipow<4>(fct(i))))/(4*(ipow<3>(fct(i+1))-ipow<3>(fct(i))));
      } else {
        return (2*(ipow<3>(fct(i+1))-ipow<3>(fct(i))))/(3*(ipow<2>(fct(i+1))-ipow<2>(fct(i))));
      }
    }
    
  private:
    FCT const fct;
    int const dim;
  };
    
  template <class FCT> BarycentricMedCoords<FCT> barycentricMedCoords(FCT f, int dim) { return  BarycentricMedCoords<FCT>(f, dim); }

  template<class FCT>
  struct CoordsDelta {
    //static_assert(std::is_invocable<real, FCT, size_t>::value,
    //              "CoordsDelta parameter FCT must be callable as `real r = FCT(size_t)`" );
    
    CoordsDelta(FCT const& coords) : fct(coords) {}
    KOKKOS_INLINE_FUNCTION
    real operator()(size_t const i) const { return (fct(i+1) - fct(i)); }

    FCT const fct;
  };

  template<class FCT>  auto coordsDelta(FCT const& f) { return CoordsDelta<FCT>(f); }

  template<>
  struct CoordsDelta<RegularCoords> {
    CoordsDelta(RegularCoords coords) : dx(coords.dx) {}
    KOKKOS_INLINE_FUNCTION
    real operator()(size_t) const { return dx; }
    
    real const dx;
  };

  template<>
  struct CoordsDelta<LogCoords> {
    CoordsDelta(LogCoords lcoords) : coords(lcoords) {}
    KOKKOS_INLINE_FUNCTION
    real operator()(size_t const i) const {
      return coords.rmin*(std::exp(real(coords.localOffset+i+1)*coords.scaling) 
                          - std::exp(real(coords.localOffset+i)*coords.scaling)); 
    }
    
    LogCoords const coords;
  };
  
}

#endif
