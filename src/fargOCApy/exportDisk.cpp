// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>

#include "allmpi.hpp"

#include "disk.hpp"
#include "planetarySystem.hpp"
#include "fargOCApy.hpp"

namespace py = pybind11;

namespace fargOCA {
  namespace py11 {
    void exportDisk(py::module_& m) {
      py::class_<Disk, 
                 std::shared_ptr<Disk>>(m, "Disk")
        .def(py::init([](std::string fname) {
              static boost::noopmpi::communicator world;
              HF::File file(fname, HF::File::ReadOnly);
              return Disk::make(world, file.openGroup("/"));
            }))
        .def_property_readonly("velocity",
                               [](Disk const& d) { return py::cast(d.velocity()); },
                               "The velocity vector field.",
                               py::return_value_policy::reference_internal)
        .def_property_readonly("physicalTime",
                               [](Disk const& d) { return d.physicalTime(); },
                               "The physical time spend in the simulation (in some unit).",
                               pybind11::return_value_policy::reference_internal)
        .def_property_readonly("density",
                               [](Disk const& d) { return py::cast(d.density()); },
                               "The density field.",
                               pybind11::return_value_policy::reference_internal)
        .def_property_readonly("soundSpeed",
                               [](Disk const& d) { return py::cast(d.soundSpeed()); },
                               "The soundspeed field.",
                               pybind11::return_value_policy::reference_internal)
        .def_property_readonly("pressure",
                               [](Disk const& d) { return py::cast(d.pressure()); },
                               "The pressure field.",
                               pybind11::return_value_policy::reference_internal)
        .def_property_readonly("system",
                               [](Disk const& d) { return py::cast(d.system()); },
                               "The planetary system, if any.",
                               pybind11::return_value_policy::reference_internal)
        .def("__repr__",
             [](Disk const& d) {
               return "fargOCApy.Disk";
             });
    }
  }
}
