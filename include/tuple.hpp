// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_TUPLE_H
#define FARGOCA_TUPLE_H

#include <limits>
#include <cmath>
#include <iosfwd>
#include <string>

#include <highfive/H5File.hpp>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "boost/serialization/access.hpp"
#include "mpidatatype.hpp"

#include "precision.hpp"
#include "kkmath.hpp"

namespace fargOCA {

  KOKKOS_INLINE_FUNCTION
  real norm2(real x, real y, real z) {
    return x*x+y*y+z*z;
  }

  KOKKOS_INLINE_FUNCTION
  real norm2(real x, real z) {
    return x*x+z*z;
  }

  KOKKOS_INLINE_FUNCTION
  real norm(real x, real y, real z) {
    return hypot3(x,y,z);
  }
  
  /** Set of three reals. */
  struct Triplet {
    real            x;
    real            y;
    real            z;

    KOKKOS_INLINE_FUNCTION
    constexpr Triplet(real x1, real x2, real x3) : x(x1), y(x2), z(x3) {}
    KOKKOS_INLINE_FUNCTION
    Triplet() : Triplet(0,0,0) {}

    KOKKOS_INLINE_FUNCTION
    constexpr explicit Triplet(real v) : Triplet(v,v,v) {}
    
    KOKKOS_INLINE_FUNCTION
    constexpr Triplet(Triplet const& ) = default;
    
    KOKKOS_INLINE_FUNCTION
    Triplet& operator+=(Triplet const& o) { x += o.x; y += o.y; z += o.z; return *this; }
    KOKKOS_INLINE_FUNCTION
    Triplet& operator-=(Triplet const& o) { x -= o.x; y -= o.y; z -= o.z; return *this; }

    KOKKOS_INLINE_FUNCTION
    Triplet& operator*=(real f)           { x *= f; y *= f; z *= f; return *this; }
    KOKKOS_INLINE_FUNCTION
    Triplet operator-() const { return Triplet(-x,-y,-z); }

    KOKKOS_INLINE_FUNCTION    
    real norm2() const { return fargOCA::norm2(x,y,z); }
    KOKKOS_INLINE_FUNCTION
    real norm()  const { return fargOCA::norm(x,y,z); }
    KOKKOS_INLINE_FUNCTION
    real sum()   const { return x+y+z; }

    std::ostream& print(std::ostream& out, std::string sep) const;

    static HighFive::DataType buildHFType();

  private:
    friend class boost::serialization::access;
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int /* version */)
    {
      ar & x;
      ar & y;
      ar & z;
    }
  };

  KOKKOS_INLINE_FUNCTION
  real
  dot(Triplet const& a, Triplet const& b) {
    return a.x*b.x + a.y*b.y + + a.z*b.z;
  }
  
  KOKKOS_INLINE_FUNCTION
  Triplet
  cross(Triplet const& a, Triplet const& b) {
    return Triplet(a.y*b.z - a.z*b.y,
                   a.z*b.x - a.x*b.z,
                   a.x*b.y - a.y*b.x);
  }

  KOKKOS_INLINE_FUNCTION
  Triplet
  operator-(Triplet const& t1, Triplet const& t2) {
    return Triplet(t1.x-t2.x, t1.y-t2.y, t1.z-t2.z);
  }

  KOKKOS_INLINE_FUNCTION
  Triplet
  operator+(Triplet const& t1, Triplet const& t2) {
    return Triplet(t1.x+t2.x, t1.y+t2.y, t1.z+t2.z);
  }

  KOKKOS_INLINE_FUNCTION
  Triplet
  operator*(real f, Triplet const& t) {
    return Triplet(f*t.x, f*t.y, f*t.z);
  }

  KOKKOS_INLINE_FUNCTION
  Triplet
  operator*(Triplet const& t, real f) {
    return f*t;
  }

  KOKKOS_INLINE_FUNCTION
  Triplet
  operator*(Triplet const& t1, Triplet const& t2) {
    return Triplet(t1.x*t2.x, t1.y*t2.y, t1.z*t2.z);
  }

  KOKKOS_INLINE_FUNCTION
  Triplet
  operator/(Triplet const& t, real f) {
    return (1/f)*t;
  }
  
  KOKKOS_INLINE_FUNCTION
  Triplet
  abs(Triplet const& p) {
    return Triplet(std::abs(p.x), std::abs(p.y), std::abs(p.z));
  }
  Triplet rdiff(Triplet const& p1, Triplet const& p2);
  Triplet adiff(Triplet const& p1, Triplet const& p2);
  Triplet max(Triplet const& p1, Triplet const& p2);

  inline
  std::ostream& operator<<(std::ostream& out, Triplet const& t) {
    return t.print(out, " ");
  }
}

#if defined(ENABLE_PARALLEL)
namespace boost { namespace mpi {
    template <>
    struct is_mpi_datatype<fargOCA::Triplet> : mpl::true_ { };
  }}
#endif

namespace Kokkos { //reduction identity must be defined in Kokkos namespace
  template<>
  struct reduction_identity<fargOCA::Triplet> {
    KOKKOS_FORCEINLINE_FUNCTION static fargOCA::Triplet sum() {
      return fargOCA::Triplet();
    }
  };
}

template <> inline HighFive::DataType HighFive::create_datatype<fargOCA::Triplet>() { return fargOCA::Triplet::buildHFType(); }

#endif
