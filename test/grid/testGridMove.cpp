// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <limits>
#include <algorithm>
#include <numeric>
#include <unistd.h>

#include <boost/lexical_cast.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "io.hpp"
#include "log.hpp"
#include "scalarField.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;
namespace kk = Kokkos;

ScalarField
rval(fmpi::communicator const& world, size_t const radius, size_t const sector, size_t const layer, bool /* dump */) {
  std::ostringstream fmt;
  fmt << "grid_" << radius << '_' << sector << '_' << layer;
  std::string fbase = fmt.str();

  GasShape shape(1, 42, 56, 0.001, false);
  auto grid = GridDispatch::make(world, *Grid::make(shape, {radius, layer, sector}, GridSpacing::ARITHMETIC ));
  ScalarField g("orig", *grid);
  kk::deep_copy(g.data(), world.rank());
  return g;
}

int main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  ScalarField fat  = rval(world,51,4,3,false);
  ScalarField flat = rval(world,51,4,1,false);
  arr3d<real> fatRankField  = scalarView("Rank value", fat.dispatch().grid());
  arr3d<real> flatRankField = scalarView("Rank value", flat.dispatch().grid());
  kk::deep_copy(fatRankField, world.rank());
  kk::deep_copy(flatRankField, world.rank());
  bool fatok  = fmpi::all_reduce(world, checkAbsRelDiff(maxAbsRelDiff(fat.data(),  fatRankField), 1e-17, 1e-17, std::cout), std::logical_and());
  bool flatok  = fmpi::all_reduce(world, checkAbsRelDiff(maxAbsRelDiff(flat.data(),  flatRankField), 1e-17, 1e-17, std::cout), std::logical_and());

  return (fatok && flatok) ? 0 : 1;
}
