// Copyright 2023, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include "environment.hpp"
#include "io.hpp"
#include "disk.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

int
main(int argc, char* argv[]) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Compute all available indirect fields and write them in disk file.\n"
        << '\t' << cmd << " <idisk>.h5 [option]+.\n";
  
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.");

  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }
  Environment env(argc, argv);
  fmpi::communicator world;
  std::string ifname = vm["disk"].as<std::string>();
  HF::File file = HF::File(ifname, HF::File::ReadWrite);
  auto disk = Disk::make(world, file.getGroup("/"));
  for(auto const& [name, field] : disk->namedFields()) {
    if (!file.exist(name)) {
      std::cout << "Need to persist field: '" << name << "'.." << std::flush;
      std::optional<HF::Group> storage;
      if (world.rank() == 0) { storage = file.createGroup(name); }
      field.writeH5(storage, 0);
      file.flush();
      std::cout << "..done.\n";
    }
  }
  return 0;
}
