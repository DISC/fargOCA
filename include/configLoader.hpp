// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_CONFIG_LOADER_H
#define FARGOCA_CONFIG_LOADER_H

#include <string>
#include <iostream>
#include <iomanip>
#include <functional>
#include <list>

#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include "diskPhysic.hpp"
#include "simulationTrackingPlugin.hpp"
#include "grid.hpp"
#include "shared.hpp"
#include "pluginConfig.hpp"

namespace fargOCA {
  /// \brief Extract physic description from configuration file.
  class ConfigLoader {
  public:
    
    template<class T> using opt = std::optional<T>;
    typedef boost::property_tree::ptree ptree;
    typedef boost::property_tree::path  path;

    /// \brief A check return true if ok (includes warning) flase if blocking.
    typedef std::function<bool (ptree const& pt, std::ostream&)> CheckTree;
    typedef std::function<bool (ptree const& pt, DiskPhysic const&, std::ostream&)> CheckTreePhysic;
    
    /// \brief Default ctor with default checks.
    /// \param log the error stream
    /// \param doDoc generat property documentation
    ConfigLoader(std::ostream& log);
    
    /// \brief Load physic description as a property tree file.
    /// \param file name
    /// \returns status
    ptree load(std::string fname);
    ptree const& cfg()    const { return myCfgRoot; }

    /// \brief Extract physic from a property tree, provided it is well formed.
    shptr<DiskPhysic const> extractPhysic() const;
    
    using PlanetMap = std::map<std::string, PlanetarySystemPhysic::Planet>;
    /// \brief Extract shape from property tree.
    PlanetMap extractPlanetsPhysic() const;
    /// \brief Extract shape from property tree.
    opt<PlanetarySystemPhysic> extractSystemPhysic() const;
    
    /// \brief Extract the simulation tracking configuration.
    /// \param cfg the configuration tree
    PluginConfig extractTrackingConfig() const;
    std::shared_ptr<Grid const> extractGridCoords() const;
    
    /// \brief The path exists and is not explicitly false
    static bool enabled(ptree const& cfg, path const& path);
    std::ostream& log() const { return myLog; }
    bool          allowDefaults()  const { return myAllowDefaults; }
    static std::vector<std::string> strings(std::string seq, char sep = ',');
    
  private:
    typedef DiskPhysic Phy;
    
    /// \brief Read a property value and mark it as read.
    /// \param p the property
    /// \param def the default value if property no set
    template<typename T> T pget(path const& p, T const& def) const;
    /// \brief Read a property value and mark it as read.
    /// Throw if property not set.
    /// \param p the property
    template<typename T> T pget(path const& p) const;
    template<typename T> opt<T> pget_optional(path const& p) const;

    /// \brief Remove property from property tree if found.
    bool erase(ptree& cfg, path p) const;

    std::map<std::string, std::string> extractMap(path const& p) const;

    CodeUnits extractCodeUnits() const;    
    GasShape extractShape() const;
    opt<Phy::StarAccretion> extractStarAccretion() const;
    Phy::Density::Cavity    extractCavity() const;
    Phy::Density            extractDensity() const;
    Phy::Smoothing          extractSmoothing() const;
    Phy::Viscosity          extractViscosity() const;
    opt<Phy::DiskPhysic::Adiabatic>      extractAdiabatic() const;
    opt<Phy::Adiabatic::Radiative>       extractRadiative() const;
    opt<Phy::Adiabatic::Radiative::Star> extractRadiativeStar() const;
    opt<Phy::Adiabatic::Cooling> extractCooling() const;
    Phy::StarAccretion::Wind     extractWind() const;
    ConfigMap                    extractConfigMap(path location) const;
    std::variant<real,PlanetarySystemPhysic::Planet::MassTaper> extractMass(path ini) const;
    ConfigLoader&           mtSelf() const { return const_cast<ConfigLoader&>(*this); }
    template<class T>
    void                    traceDefault(path const& p, T const& def) const;
    DiskPhysic::Referential extractReferential() const;

  
  private:
    ptree                                    myCfgRoot;
    std::list<CheckTree>                     myTreeChecks;
    std::list<CheckTreePhysic>               myTreePhysicChecks;
    std::ostream&                            myLog;
    bool                                     myAllowDefaults;  
  };

}

#endif
