#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
import sys
import math
import argparse
import matplotlib.animation as animation



parser = argparse.ArgumentParser(description='''
Displays a movie of the radial surface density, provided the initial and final disk numbers'''.format(sys.argv[0]))
parser.add_argument('starting_file_number', metavar='Ninit', type=int, 
                    help='Provide the initial number of disk file to be visualized')
parser.add_argument('final_file_number', metavar='Nfinal', type=int,     
                    help='Provide the final number  ofi disk file to be visualized')
parser.add_argument('-w','--view', dest='view_mode',choices=['gif'],default='interactive show',
                    help='choose gif to save the animation on density.gif and density_profile.gif')
parser.add_argument('R0', nargs='?', type=float, default=5.2, 
                    help='Provide the unit of distance default 5.2(AU)')

args = parser.parse_args()
ninit = args.starting_file_number
nfinal=args.final_file_number

# FargOCA units
AU     = 1.496e13  # Astronomical Unit in [cm] 
GRAVC  = 6.67e-8    # Gravitation Constant in [cm3/g/s2]
XMSOL  = 1.989e33  # Solar Mass in [gr]
XMSTAR = 1.0       # Amount of Solar Masses
R0scale = args.R0
R0     = R0scale*AU    # Unit of distance in cm
YEAR   = (365.2524*24.*3600.) # one year in s
VOL0   = (R0*R0*R0) # Volume unit in cm**3
TIME0  = (math.sqrt(VOL0 / GRAVC / (XMSTAR*XMSOL)))  # time unit in s
V0     = (R0 / TIME0)  # Velocity in cm/s
XM0    = (XMSOL * XMSTAR)  # Mass unit in g
RHO0   = (XM0 / VOL0)  # Density unit in g/cm**3
mdotSMyear =R0*R0*RHO0*V0*YEAR/XMSOL


sigma = {}
surf = {}
physical_time={}


diskinit='./disk'+str(ninit)+'.h5'
data = h5.File(diskinit)
radius = data['grid/radii'][:]
theta  = data['grid/sectors'][:]
phi = data['grid/layers'][:]
deltaphi=phi[1]-phi[0]  # valid only for uniform grid
th,rad = np.meshgrid(theta, radius)
half = data['physic/shape/gas'].attrs['half'] 
shape = data['density/polar_grid'].shape
steps_per_output = data['/'].attrs['steps_per_output']
nfiles=(int)((nfinal-ninit)/steps_per_output)
print('Number of processed files:',nfiles)

flat   = shape[1] == 1
print(shape)


if flat:
      print("this is a flat grid, displaying as it is.")
else:
       print("this is a 3 grid, compute surface density")
x = rad*np.cos(th)*R0scale
y = rad*np.sin(th)*R0scale
#print(x.shape)
fig,ax =plt.subplots()







for i in range(ninit,nfinal,steps_per_output):
      data = 'disk'+str(i)+'.h5'
      j = i/steps_per_output
      disk=h5.File(data,'r')
      physical_time[j-ninit/steps_per_output] = disk['/'].attrs['physical_time']
      if flat:
            sigma[j-ninit/steps_per_output]=disk['density/polar_grid'][:,0,:]
      else:
            sigma[j-ninit/steps_per_output]=disk['density/polar_grid'][:,:,:].sum(axis=1)  #  sum on vertical direction
            if(half):
                  sigma[j-ninit/steps_per_output]*=2



# Vertical integration
if(not flat):
      for h in range(nfiles):
            for i in range(shape[0]):
                  for j in range(shape[2]):
                        sigma[h][i,j]=sigma[h][i,j]*radius[i]*deltaphi
for h in range(nfiles):
            surf[h]=sigma[h].mean(axis=-1)
            

ma=np.zeros(nfiles)
mi=np.zeros(nfiles)
mis=np.zeros(nfiles)
mas=np.zeros(nfiles)
for i in range(nfiles):
    ma[i]=(sigma[i]).max()
    mi[i]=(sigma[i]).min()
    mas[i]=(surf[i]).max()
    mis[i]=(surf[i]).min()



mama=RHO0*R0*ma.max()
mimi=RHO0*R0*mi.min()
masma=RHO0*R0*mas.max()
mismi=RHO0*R0*mis.min()



def animate(i):
    plt.clf()
    f = np.log10(sigma[i]*RHO0*R0)
    im = plt.pcolormesh(x,y,f,
                        vmin=math.log10(mimi),
                        vmax=math.log10(mama),
                        cmap='gnuplot')
    timeyears = int(physical_time[i]*math.pow(R0scale,1.5)/math.pi/2.)
    plt.title(f'Time (years): {timeyears}')
    cbar=plt.colorbar(fraction=.15,pad=0.15,shrink=0.7,orientation='vertical')
    cbar.set_label('$\log(\\Sigma),g/cm^2$',size=7)
    cbar.ax.tick_params(labelsize=6) 
    plt.xlabel('X[AU]')
    plt.ylabel('Y[AU]')

    return im


ani = animation.FuncAnimation(fig, animate, frames = nfiles , interval= 6,repeat = False)
if(args.view_mode == 'gif'):
      ani.save('density.gif', fps=2, extra_args=['-vcodec', 'libx264'])
else:
      plt.show()

ymin= math.log10(mismi)
ymax= math.log10(masma)

def animatesurf(i):
    plt.clf()
    f = np.log10(surf[i]*RHO0*R0)
    im = plt.plot(radius[:-1],f,'r-',linewidth=2)
    timeyears = int(physical_time[i]*math.pow(R0scale,1.5)/math.pi/2.)
    plt.title(f'Time (years): {timeyears}')
    plt.xlabel('r[AU]')
    plt.ylabel('$\\Sigma (g/cm^2)$')
    plt.ylim(ymin,ymax) 
ani = animation.FuncAnimation(fig, animatesurf, frames = nfiles , interval= 6)
if(args.view_mode == 'gif'): 
      ani.save('density_profile.gif', fps=4, extra_args=['-vcodec', 'libx264'])
else:
      plt.show()

