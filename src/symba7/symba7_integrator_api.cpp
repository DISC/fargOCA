// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "swift/symba7_integrator_api.hpp"

extern "C" {
  // FORTRAN implementations:
  void   symba7_step(s7_integrator *self, double dt); 
  void   symba7_write(s7_integrator const*self);
  int    symba7_nb_bodies(s7_integrator const*self);
  double symba7_time(s7_integrator const*self);
  
  s7_integrator::s7_integrator(int nb, double t0, int16_t merge, double small)
    : time(t0),
      nbodies(nb),
      n_massive_bodies(-1),
      last_nbodies(0),
      take_referential_from_scratch(0),
      j2rp2(0),
      j4rp4(0),
      mass(new double[nb]),
      xh(new double[nb]),
      yh(new double[nb]),
      zh(new double[nb]),
      vxh(new double[nb]),
      vyh(new double[nb]),
      vzh(new double[nb]),
      rpl(new double[nb]),
      rhill(new double[nb]),
      pclose(merge),
      rmin(std::numeric_limits<double>::quiet_NaN()), 
    rmax(std::numeric_limits<double>::quiet_NaN()), 
    rmaxu(std::numeric_limits<double>::quiet_NaN()), 
    qmin(std::numeric_limits<double>::quiet_NaN()),
    tiny(small) {
  }

  s7_integrator::~s7_integrator() {
    delete[] mass;
    delete[] xh;
    delete[] yh;
    delete[] zh;
    delete[] vxh;
    delete[] vyh;
    delete[] vzh;
    delete[] rpl;
    delete[] rhill;
  }
  
  void
  s7_integrator::set_boundaries(double rmin, double rmax, double rmaxu, double qmin) {
    this->rmin  = rmin;
    this->rmax  = rmax;
    this->rmaxu = rmaxu;
    this->qmin  = qmin;
  }
  
  void 
  s7_integrator::set_planet(int i,
                            double m, 
                            double px, double py, double pz,
                            double vx, double vy, double vz,
                            double r, double rh) {
    mass[i] = m;
    xh[i]   = px;
    yh[i]   = py;
    zh[i]   = pz;
    vxh[i]  = vx;
    vyh[i]  = vy;
    vzh[i]  = vz;
    rpl[i]  = r;
    rhill[i]= rh;
  }
  
  fargOCA::Planet
  s7_integrator::get_planet(int i) const {
    using fargOCA::Triplet;
    return fargOCA::Planet(mass[i], Triplet(xh[i],yh[i],zh[i]), Triplet(vxh[i], vyh[i], vzh[i]), rpl[i], rhill[i]);
  }

  void
  s7_integrator::step(double dt) {
    symba7_step(this, dt);
  }

  void
  s7_integrator::write() const {
    symba7_write(this);
  }

  int
  s7_integrator::get_nb_bodies() const {
    return symba7_nb_bodies(this);
  }

  double
  s7_integrator::get_time() const {
    return symba7_time(this);
  }
}
