// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <type_traits>

#include "stellarRadiationSolver.hpp"

namespace fargOCA {
  
  namespace details {
    template<class Creator>
    std::optional<Creator>
    getSolverCreator(Disk const& disk, std::map<std::string, Creator>& creators) {
      if (!disk.physic().radiative()) { 
        disk.dispatch().log() << "Only radiative run have stellar radiation solvers.\n";
        return std::nullopt;
      } else {
        std::string key = disk.physic().adiabatic->radiative->solver.label;
        auto found = creators.find(key);
        if (found == creators.end()) {
          disk.dispatch().log() << "No solver found for solver name \"" << key << "\".\n";
          return std::nullopt;
        } else {
          return found->second;
        }
      }
    }
  }

  StellarRadiationSolver::~StellarRadiationSolver() {}  

  std::map<std::string, StellarRadiationSolver::RestartCreator>&
  StellarRadiationSolver::restartCreators() {
    static std::map<std::string, RestartCreator> creators;
    return creators;
  }
  
  std::map<std::string, StellarRadiationSolver::InitialCreator>&
  StellarRadiationSolver::initialCreators() {
    static std::map<std::string, InitialCreator> creators;
    return creators;
  }

  std::set<std::string>
  StellarRadiationSolver::available() {
    std::set<std::string> names;
    for (auto const& [name,f] : initialCreators()) {
      names.insert(name);
    }
    return names;
  }

  std::unique_ptr<StellarRadiationSolver>
  StellarRadiationSolver::initial(Disk& disk) {
    auto creator = details::getSolverCreator(disk, initialCreators());
    if (creator) {
      return (*creator)(disk);
    } else {
      return nullptr;
    }
  }

  std::unique_ptr<StellarRadiationSolver>
  StellarRadiationSolver::restart(Disk& disk, HighFive::Group group) {
    auto creator = details::getSolverCreator(disk, restartCreators());
    if (creator) {
      return (*creator)(disk, group);
    } else {
      return nullptr;
    }
  }
}
