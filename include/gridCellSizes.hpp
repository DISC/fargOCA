// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGOCA_GRID_VOLUMES_H
#define FARGOCA_GRID_VOLUMES_H

#include <memory>

#include "precision.hpp"
#include "arrays.hpp"
#include "grid.hpp"
#include "cuda_workarounds.hpp"

namespace fargOCA {

  /// \brief Memoized grid cell informations.
  /// 
  /// For both 2D or 3D
  class GridCellSizes {
  public:
    /// \brief No size willbe made available.
    GridCellSizes();

    /// \brief Sizes will be provided for the given grid
    GridCellSizes(Grid const& coords);
    ~GridCellSizes();
    
    /// \brief When multiplied with a density, provide a mass.
    /// The description is weird since this is used in both 2D and 3D.
    arr3d<real const> volume()    const { return myVolume; }
    
    /// \brief When multiplied radial unit flux, provide flux. 
    /// The description is weird since this is used in both 2D and 3D.
    arr3d<real const> dTheta()       const { return myDTheta; }
    
  cuda_private:
    template <GridSpacing GS>
    void init(Coords<GS> const& grid);
  private:
    arr3d<real> myVolume;
    arr3d<real> myDTheta;
  };
}

#endif 
