// Copyright 2022, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <cmath>
#include <cstdio>
#include <string>
#include <set>

#include <Kokkos_Core.hpp>

#include "viscosityFieldPlugin.hpp"
#include "diskPhysic.hpp"
#include "gridDispatch.hpp"
#include "disk.hpp"
#include "loops.hpp"
#include "config.hpp"
#include "pluginImpl.hpp"
#include "pluginUtils.hpp"

namespace fargOCA {
  namespace kk = Kokkos;
#define LBD KOKKOS_LAMBDA
  
  namespace plugin {
    
    template<class SoundSpeed, class RadMed>
    KOKKOS_INLINE_FUNCTION
    real
    alphaScaling(size_t const i, size_t const h, size_t const j,
		 size_t const midlayer, SoundSpeed soundspeed, RadMed radmed) {
      return ipow<2>(soundspeed(i,midlayer,j)) * pow(radmed(i),1.5);
    }
    
    template<GridSpacing GS>
    struct ConstantAlpha {
      void operator()(Disk const& disk, arr3d<real> viscosity) {
	Coords<GS> const& coords      = disk.dispatch().grid().as<GS>();
	size_t const ni               = disk.dispatch().grid().sizes().ni;
	size_t const midlayer         = disk.physic().shape.half ? ni-2 : ni/2;
	auto radmed = coords.radiiMed();
	arr3d<real const> soundspeed  = disk.soundSpeed().data();
	real const value = *disk.physic().viscosity.config.value<real>("value");
	kk::parallel_for("constant alpha",
			 fullRange(coords),
			 LBD(int i, int h, int j) {
			   real const scaling = alphaScaling(i, h, j, midlayer, soundspeed, radmed);
			   viscosity(i,h,j) = scaling*value;
			 });
      }
    };
    ViscosityFieldPlugin constantAlpha("constant_alpha",
			   ViscosityFieldPlugin::selectSpacing<ConstantAlpha>(),
			   "Viscosity is an alpha scaling (add some refrence here) applied to \"value\" parameter.",
			   {"value"});
    
    ViscosityFieldPlugin constant("constant",
			     [](Disk const& disk, arr3d<real> viscosity) -> void {
			       real const value = *disk.physic().viscosity.config.value<real>("value");
			       kk::deep_copy(viscosity, value);
			     },
			     "Viscosity is constant on the whole space and provided by the \"value\" parameter.",
			     {"value"});
    
    // Use an intermediate "normal" function to accomodate brain dead CUDA
    template<GridSpacing GS>
    struct MRIToDzAlpha {
      void operator()(Disk const& disk, arr3d<real> viscosity) const {
	Coords<GS> const& coords     = disk.dispatch().grid().as<GS>();
	Config     const& config     = disk.physic().viscosity.config;
	
	real   const value            {*config.value<real>("value")};
	real   const outerTemperature {*config.value<real>("outerTemperature")};
	real   const transitionRadius {*config.value<real>("transitionRadius")};
	real   const MRIAlpha         {*config.value<real>("MRIAlpha")};
	real   const MRITemperature   {*config.value<real>("MRITemperature")};
	size_t const ni       = disk.dispatch().grid().sizes().ni;
	size_t const midlayer = disk.physic().shape.half ? ni-2 : ni/2;
	
	auto radmed = coords.radiiMed();
	arr3d<real const> soundspeed  = disk.soundSpeed().data();
	
	kk::parallel_for("MRI alpha",
			 fullRange(coords),
			 LBD(int i, int h, int j) {
			   real const scaling = alphaScaling(i, h, j, midlayer, soundspeed, radmed);
			   real const transition = ((1-std::tanh((MRITemperature/outerTemperature)
								 *(1-transitionRadius/radmed(i))))
						    /2);
			   real const MRI2Dz = (MRIAlpha - value)*transition + value;
			   viscosity(i,h,j) = scaling*MRI2Dz;
			 });
      }
    };

    ViscosityFieldPlugin MRI2DzAlpha("MRIToDz",
				ViscosityFieldPlugin::selectSpacing<MRIToDzAlpha>(),
				"Visosity stellar model viscous transition as in Flock et al.2019.",
				{ "value", "outerTemperature",
				  "transitionRadius", "MRIAlpha", "MRITemperature"});
    

    template<GridSpacing GS>
    struct ViscosityForCavity {
      void operator()(Disk const& disk, arr3d<real> viscosity) {
	Coords<GS> const& coords     = disk.dispatch().grid().as<GS>();
	// Visosity computed to mantain a cavity stable
	auto radmed = coords.radiiMed();
	real const slope = disk.physic().density.slope;
	real const start = disk.physic().density.start;
	real const rcav = disk.physic().density.cavity.radius;
	real const width = disk.physic().density.cavity.width;
	real const rate  = disk.physic().starAccretion->rate();
	real const transition = rcav-3.0*width;   // transition of mdot from rcav -width to rmin 
	
	kk::parallel_for("Visco cavity",
			 fullRange(coords),
			 LBD(int i, int h, int j) {
			   viscosity(i,h,j) =2.0*pow(radmed(i)/rcav,slope)/(3.0*PI*start*(1.0+tanh((radmed(i)-rcav)/width)));
			   if(radmed(i) > transition) {
			     viscosity(i,h,j) *=rate;
			   } else {
			     viscosity(i,h,j) *=rate + (0.1*rate-rate)*pow((radmed(i)-transition)/(radmed(0)-transition),2);
			   }
			 });
      }
    };
    
    ViscosityFieldPlugin viscosityForCavity("viscosity_for_cavity",
				       ViscosityFieldPlugin::selectSpacing<ViscosityForCavity>(),
				       "Explanation is still pending.",
				       {});
  }

  // Instanciate the code here
  template class Plugin<ViscosityField, Disk const&, arr3d<real>>;
}
