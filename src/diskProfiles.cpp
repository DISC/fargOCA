// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clment Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurlien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frdric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#include <limits>

#include "diskProfiles.hpp"
#include "diskPhysic.hpp"
#include "scalarField.hpp"
#include "disk.hpp"
#include "arraysUtils.hpp"
#include "gridDispatch.hpp"
#include "loops.hpp"
#include "details/barycentric_rational.hpp"

namespace fargOCA {
  namespace kk  = Kokkos;

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

#define LBD KOKKOS_LAMBDA

  using kk::pow;
  using kk::sqrt;
  using kk::exp;
  using kk::sin;

  DiskProfiles::~DiskProfiles() {}

  namespace {
    bool
    writeProfile(GridDispatch const& dispatch, std::string name, arr2d<real const> profile,
                 std::optional<HighFive::Group> parent, int writer)  {
      assert(bool(parent) == (writer == dispatch.comm().rank())); // group only defined in writer
      auto const& gcoords = dispatch.grid().global();
      GridSizes const gs = gcoords.sizes();
      
      std::optional<arr2d<real>> merged = dispatch.joinManaged(profile, writer);
      if (bool(parent)) {
        using namespace HighFive;
        harr2d<real const> hmerged = dev2CHost(*merged);
        Group group = parent->createGroup(name);
        DataSet gridDataSet = group.createDataSet("profile", DataSpace(gs.nr, gs.ni), create_datatype<real>());
        gridDataSet.write_raw(hmerged.data());
      }
      return true;
    }

    std::optional<arr2d<real>>
    readProfile(GridDispatch const& dispatch, std::string name, HighFive::Group const& parent)  {
      if (!parent.exist(name)) {
        return std::nullopt;
      } else {
        using namespace HighFive;
        Group group = parent.getGroup(name);
        auto const& coords = dispatch.grid();
        GridSizes const ls = coords.sizes();
        
        arr2d<real>  profile  = profileView("profile", coords);
        harr2d<real> hprofile("hprofile", ls.nr, ls.ni);
        DataSet dataSet = group.getDataSet("profile");
        
        std::vector<size_t> dims({ls.nr, ls.ni});
        std::vector<size_t> offsets({dispatch.radiiIndexRange().first, size_t(0)});
        dataSet.select(offsets, dims).read(hprofile.data());
        cHost2Dev(hprofile, profile);
        return profile;
      }
    }
  }
  
  void
  DiskProfiles::readH5(HighFive::Group const& group) {
    auto velocityGroup = group.getGroup("velocity");
    myRadialVelocity = *readProfile(dispatch(), "velocity/radial", group);
    if (dispatch().grid().dim() == 3) {
      myPhiVelocity  = *readProfile(dispatch(), "velocity/phi", group);
    }
    myThetaVelocity  = *readProfile(dispatch(), "velocity/theta", group);
    myDensity        = *readProfile(dispatch(), "density", group);
    myEnergy         = readProfile(dispatch(), "energy", group);
  }
  
  void
  DiskProfiles::writeH5(std::optional<HighFive::Group> group, int writer) const {
    using namespace HighFive;
    if (writer == dispatch().comm().rank()) {
      group->createGroup("velocity");
    }
    writeProfile(dispatch(), "velocity/radial", myRadialVelocity, group, writer);
    if (dispatch().grid().dim() > 2) {
      writeProfile(dispatch(), "velocity/phi", myPhiVelocity, group, writer);
    }
    writeProfile(dispatch(), "velocity/theta", myThetaVelocity, group, writer);
    writeProfile(dispatch(), "density", myDensity, group, writer);
    if (myEnergy) {
      writeProfile(dispatch(), "energy", *myEnergy, group, writer);
    }
  }
  
  DiskProfiles::DiskProfiles(GridDispatch const& dispatch,
                             arr2d<real const> radialVelocity,
                             arr2d<real const> phiVelocity,
                             arr2d<real const> thetaVelocity,
                             arr2d<real const> density,
                             std::optional<arr2d<real const>> energy) 
    : myDispatch(dispatch.shared()),
      myRadialVelocity(profileView("velocity/radial", dispatch.grid())),
      myPhiVelocity(profileView("velocity/phi", dispatch.grid())),
      myThetaVelocity(profileView("velocity/theta", dispatch.grid())),
      myDensity(profileView("density", dispatch.grid())),
      myEnergy(std::nullopt)
  {
    kk::deep_copy(myRadialVelocity, radialVelocity);
    kk::deep_copy(myPhiVelocity,    phiVelocity);
    kk::deep_copy(myThetaVelocity,  thetaVelocity);
    kk::deep_copy(myDensity,        density);
    if (energy) { 
      myEnergy = profileView("energy", dispatch.grid());
      kk::deep_copy(*myEnergy, *energy);
    }
  }

  DiskProfiles::DiskProfiles(DiskProfiles const& other)
    : myDispatch(other.myDispatch),
      myRadialVelocity(other.myRadialVelocity),
      myPhiVelocity(other.myPhiVelocity),
      myThetaVelocity(other.myRadialVelocity),
      myDensity(other.myDensity),
      myEnergy(other.myEnergy)
  {
  }
  
  DiskProfiles::DiskProfiles(GridDispatch const& dispatch,  HighFive::Group const& group) 
    : myDispatch(dispatch.shared()) {
    readH5(group);
  }

  namespace {

    struct Interpolator {
      Interpolator(harr1d<real const> y, harr1d<real const> fy)
	: x(y),
	  fx(fy),
	  engine(x.data(), fx.data(), fx.size(), 3) {}
      
      Interpolator(arr1d<real const> y, arr1d<real const> fy)
	: Interpolator(hostROValues(y), hostROValues(fy)) {}

      Interpolator(harr1d<real const> y, arr1d<real const> fy)
	: Interpolator(y, hostROValues(fy)) {}
      
      real operator()(real const& xx) const { return engine(xx); }
      
      harr1d<real const> x;
      harr1d<real const> fx;
      details::barycentric_rational<real> engine;
    };

    arr2d<real> rebindProfile(GridDispatch const& prev, arr2d<real const> prevProfile,
                              GridDispatch const& next,
                              GridPosition radiusPos, GridPosition layerPos) {
      GridSizes const pls = prev.grid().sizes();
      GridSizes const pgs = prev.grid().global().sizes();
      GridSizes const nls = next.grid().sizes();
      GridSizes const ngs = next.grid().global().sizes();

      assert(int(prevProfile.size()) == pls.ni*pls.nr);
      auto gPrevProfile = *prev.joinManaged(prevProfile, std::nullopt);
      harr2d<real> radiusAdjustedProfile("adjusted_profile", ngs.nr, pgs.ni);
      { // rebind on radial direction
        auto prevRadii = hostRadiiValues(prev.grid().global(), radiusPos); // we need the exact coordinate to build the interpolation
        auto nextRadii = hostRadiiValues(next.grid().global(), radiusPos); // the next grid will be on the same position as the prev wrt the grid
        for(size_t h = 0; h < pgs.ni; ++h) {
          arr1d<real> prevFX("prev_fx", pgs.nr);
	  kk::parallel_for("build interpolator", pgs.nr,
			   LBD(size_t const i) { prevFX(i) = gPrevProfile(i,h); });
          Interpolator f(prevRadii, arr1d<real const>(prevFX));
	  kk::parallel_for("build interpolator",hRange1D<size_t>(0,ngs.nr),
			   [&](size_t const i) { radiusAdjustedProfile(i, h) = f(nextRadii(i)); });
        }
      }
      assert((ngs.ni != 1) == (pgs.ni != 1)); // same flatness at this point
      harr2d<real> nextProfile("profile", ngs.nr, ngs.ni);
      if (pgs.ni == 1) {
        if (ngs.ni > 1) {
          // Something should be done eventually
          throw std::domain_error("flat to thick disk rebind not supported yet.");
        }
        kk::deep_copy(nextProfile, radiusAdjustedProfile);
      } else {
        // rebind on thickness 
        auto prevLayers = hostLayersValues(prev.grid(), layerPos,  pgs.ni+1);
        auto nextLayers = hostLayersValues(next.grid(), layerPos,  ngs.ni+1);
        for(size_t i = 0; i < ngs.nr; ++i) {
          Interpolator f(prevLayers, harr1d<real const>(kk::subview(radiusAdjustedProfile, i, kk::ALL())));
	  kk::parallel_for("build interpolator",hRange1D<size_t>(0,ngs.ni),
			   [&](size_t const h) { nextProfile(i,h)  = f(nextLayers(h)); });
        }
      }
      arr2d<real> result("profile", nls.nr, nls.ni);
      auto resultMirror = kk::create_mirror_view_and_copy(kk::DefaultExecutionSpace{}, next.localView(nextProfile));
      kk::deep_copy(result, resultMirror);
      return result;
    }
  }

  DiskProfiles::DiskProfiles(GridDispatch const& next,  DiskProfiles const& prev) 
    : myDispatch(next.shared()),
      myRadialVelocity(rebindProfile(prev.dispatch(), prev.velocity().radial(), next, GridPosition::INF, GridPosition::MED)),
      myPhiVelocity(next.grid().dim() == 2
                    ? profileView("velocity/phi", dispatch().grid())
                    : rebindProfile(prev.dispatch(), prev.velocity().phi(), next, GridPosition::MED, GridPosition::INF)),
      myThetaVelocity(rebindProfile(prev.dispatch(), prev.velocity().theta(0), next, GridPosition::MED, GridPosition::MED)),
      myDensity(rebindProfile(prev.dispatch(), prev.myDensity, next, GridPosition::MED, GridPosition::MED)),
      myEnergy(bool(prev.energy())
               ? std::make_optional(rebindProfile(prev.dispatch(), *prev.energy(), next, GridPosition::MED, GridPosition::MED))
               : std::nullopt)
  {}
  
  namespace details {
    template<GridSpacing GS>
    arr2d<real>
    computeThetaVelocity(DiskProfiles const& profile, real frameVelocity) {
      auto const& coords = profile.dispatch().grid().as<GS>();
      auto radMed = coords.radiiMed();
      auto phiMed = coords.phiMed();
      
      arr2d<real>        vtheta  = profileView("velocity/theta/frame", coords);
      arr2d<real const>  vtheta0 = profile.thetaVelocity();
      kk::parallel_for("profile theta vel.", fullProfileRange(coords),
                       LBD(size_t const i, size_t const h) {
                         real const dist = radMed(i)*std::sin(phiMed(h));
                         // Azimuthal velocity in the rotating frame
                         vtheta(i,h) = vtheta0(i,h) - frameVelocity*dist;
                       });
      return vtheta;
    }
  }
  
  arr2d<real>
  DiskProfiles::thetaVelocity(real frameVelocity) const {
    switch (dispatch().grid().radialSpacing()) {
    case ARTH: return details::computeThetaVelocity<ARTH>(*this, frameVelocity); break;
    case LOGR: return details::computeThetaVelocity<LOGR>(*this, frameVelocity); break;
    }
    std::abort();
  }
}
