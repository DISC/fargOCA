# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load git git-lfs
module load gcc/8.3.1 cmake git-lfs/ boost/1.74.0-mpi kokkos/3.4.01-cuda-c++17 hdf5/1.12.0

export Kokkos_DIR=/gpfslocalsup/spack_soft/kokkos/3.4.01/gcc-8.3.1-okdtvzd64e6u5yz6evpp3mn5dcoqdum2
export CC=gcc
export CXX=g++
export FC=gfortran
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
if [[ x"$JOBSCRATCH" != "x" ]]
then 
    export TMPDIR=$JOBSCRATCH
fi
