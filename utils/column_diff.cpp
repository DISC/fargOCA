// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <sstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "boost/program_options.hpp"
#include "boost/algorithm/string.hpp"

#include "precision.hpp"
#include "log.hpp"

using fargOCA::real;

namespace po      = boost::program_options;
namespace fs      = std::filesystem;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Check the float differences between to column based file.\n"
        << '\t' << cmd << " <file1> <file2> [atol[:rtol[:gate]]]+ \n.";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("verbose", "Print more stuff.")
    ("ofile,o", po::value<std::string>()->default_value("planetdiff.h5"), "if present, will contains the diff.")
    ;
  po::options_description hidden;
  hidden.add_options()
    ("file1",  po::value<std::string>(), "first column file.")
    ("file2",  po::value<std::string>(), "second column file.")
    ("check",  po::value<std::vector<std::string>>(), 
     "test to perform on each column. Must have exactly one check per column. "
     "Check may be of the form 'ignore' or '<abs. tol.>:<rel. tol.>'.")
    ;
  
  try {
    po::positional_options_description pos;
    pos.add("file1", 1).add("file2",1).add("check", -1);

    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_USAGE);
  } 
  for(std::string opt : {"file1", "file2"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
}

struct tolerance {
  std::optional<real> absolute, relative, noise_gate;
};

std::vector<std::optional<tolerance>>
parseTolerances(std::vector<std::string> specs) {
  std::vector<std::optional<tolerance>> tolerances;
  for (std::string spec : specs) {
    if (spec == "ignore") {
      tolerances.push_back(std::nullopt);
    } else {
      std::vector<std::string> tokens;
      boost::split(tokens, spec, boost::is_any_of(":"));
      if (tokens.size() == 0 || tokens.size() > 3) {
        std::cerr << spec << " is not a well formed tolerance specification, must be of the form <absolute tolerance>[:<relative tolerance>[:<noise gate>]].\n";
        std::exit(-1);
      } else {
        try {
          tolerance tol;
          if (tokens.size() >= 1) {
            tol.absolute = boost::lexical_cast<real>(tokens[0]);
            if (*tol.absolute<0) {
              std::cerr << tokens[0] << " must be positive.\n";
              std::exit(-1);
            }
          }
          if (tokens.size() >= 2) {
            tol.relative = boost::lexical_cast<real>(tokens[1]);
            if (*tol.relative<0) {
              std::cerr << tokens[1] << " must be positive.\n";
              std::exit(-1);
            }
          }
          if (tokens.size() >= 3) {
            tol.noise_gate = boost::lexical_cast<real>(tokens[2]);
            if (*tol.noise_gate<0) {
              std::cerr << tokens[2] << " must be positive.\n";
              std::exit(-1);
            }
          }
          tolerances.push_back(tol);
        } catch (boost::bad_lexical_cast& e) {
          std::cerr << spec << " is not a correct tolerance specification, must use positive floating point.\n";
          std::exit(-1);
        }
      }
    }
  }
  return tolerances;
}

std::vector<std::vector<std::string>>
getLines(std::string fname) {
  std::vector<std::vector<std::string>> result;
  std::ifstream input(fname);
  if (!input) {
    std::cerr << "Could not open '" << fname << "'\n";
    std::exit(-1);
  }
  std::string line;
  while(std::getline(input, line)) {
    std::istringstream tokens(line);
    std::vector<std::string> words;
    std::copy(std::istream_iterator<std::string>(tokens),
              std::istream_iterator<std::string>(),
              std::back_inserter(words));
    result.push_back(std::move(words));
  }
  return result;
}

bool 
compare(std::vector<std::string> const& line1, std::vector<std::string> const& line2,
        std::vector<std::optional<tolerance>> const& checks,
        int lineno) {
  bool good = true; // prove me wrong
  if (line1.size() != line2.size()) {
    std::cerr << "problem line " << lineno << ": " << line1.size() << " tokens in first file and " << line2.size() << " in second.\n";
    return false;
  }
  if (line1.size() != checks.size()) {
    std::cerr << "expecting " << checks.size() << " tokens per lines, got " << line1.size() << '\n';
    return false;
  }
  for (int i = 0; i < int(line1.size()); ++i) {
    if (checks[i]) {
      real r1 = std::numeric_limits<real>::quiet_NaN();
      real r2 = std::numeric_limits<real>::quiet_NaN();
      try {
        r1 = boost::lexical_cast<real>(line1[i]);
      } catch (boost::bad_lexical_cast& err) {
        std::cerr << "in first file line " << lineno << ", could not convert " << line1[i] << " to real.\n";
        return false;
      }
      try {
        r2 = boost::lexical_cast<real>(line2[i]);
      } catch (boost::bad_lexical_cast& err) {
        std::cerr << "in second file file line " << lineno << ", could not convert " << line2[i] << " to real.\n";
        return false;
      }
      if (r1 != r2) {
        tolerance tol = *checks[i];
        if (tol.noise_gate
            && (std::abs(r1) < *tol.noise_gate 
                && std::abs(r2) < *tol.noise_gate)) {
          continue;
        }
        real adiff = std::abs(r1-r2);        
        if (tol.absolute) {
          if (adiff > *tol.absolute) {
            std::cerr << "line " << lineno << ", column " 
                      << i+1 << ": abs(" << r1 << " - " << r2 << ") == " 
                      << adiff << " > " << *tol.absolute << '\n';
            good = false;
          }
        }
        if (tol.relative) {
          real mag = (std::abs(r1)+std::abs(r2))/2;
          real rdiff = adiff / mag;
          if (rdiff > *tol.relative) {
            std::cerr << "line " << lineno << ", column " << i+1 
                      << ": abs(" << r1 << " - " << r2 << ")/" << mag << " == " 
                      << rdiff << "  > " << *tol.relative << '\n';
            good = false;
          }
        }
      }
    } else {
      continue;
    }
  }
  return good;
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  std::string fname1 = vm["file1"].as<std::string>();
  std::string fname2 = vm["file2"].as<std::string>();
  bool verbose = bool(vm.count("verbose"));
  std::ostream& vlog = fargOCA::log(verbose);
  vlog << "Comparing " << fname1 << " and " << fname2 << '\n';

  std::vector<std::vector<std::string>> lines1 = getLines(fname1);
  std::vector<std::vector<std::string>> lines2 = getLines(fname2);  
  bool good = true; // prove me wrong
  std::vector<std::optional<tolerance>> checks = parseTolerances(vm["check"].as<std::vector<std::string>>());
  if (lines1.size() != lines2.size()) {
    std::cerr << "first file has " << lines1.size() << " lines while second file has " << lines2.size() << " lines.\n";
    good = false;
  } else {
    std::cerr << std::scientific;
    std::cerr.precision(10);
    for (int i = 0; i < int(lines1.size()); ++i) {
      if (!compare(lines1[i], lines2[i], checks, i+1)) {
        good = false;
      }
    }
  }
  if (good) {
    vlog << "CLOSE ENOUGH\n";
    return 0;
  } else {
    vlog << "NOT CLOSE ENOUGH\n";
    return -1;
  }
}
  
