// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <highfive/H5File.hpp>
#include <nlohmann/json.hpp>

#include "simulationTrackingPlugin.hpp"

#include "io.hpp"
#include "h5io.hpp"
#include "h5Labels.hpp"
#include "pluginConfig.hpp"

namespace fargOCA {
  using namespace h5::labels;

  void
  PluginConfig::sort() {
    myConfigs.sort([](ConfigMap::value_type const& p1, ConfigMap::value_type const& p2) -> bool {
      // first, configs without explicit step are alphabeticaly ordered,
      // then, config with step are ordered by increasing step value.
      using h5::labels::STEP;
      auto step1 = p1.second.value<int>(STEP);
      auto step2 = p2.second.value<int>(STEP);
      auto name1 = p1.first;
      auto name2 = p2.first;
      if (bool(step1)) {
        if (bool(step2)) {
          if (*step1 == *step2) {
            return name1.compare(name2) < 0;
          } else {
            return *step1 < *step2;
          }
        } else {
          return false;
        }
      } else {
        if (bool(step2)) {
          return true;
        } else {
          return name1.compare(name2) < 0;
        }
      }
    });
  }
  
  PluginConfig::PluginConfig(ConfigMap const& cfgs)
    : myConfigs(cfgs) {
    auto names = enabled();
    std::sort(names.begin(), names.end());
    auto pte = std::unique(names.begin(), names.end());
    if (std::distance(names.begin(), pte) != myConfigs.size()) {
      std::ostringstream msg;
      msg << "There is a duplicate in a plugin config.\n"
          << "And there is a bug in Cray's OpenMP implementation "
          << "that make it too cumbersome to tell which one.\n";
      throw std::logic_error(msg.str());
    }
    sort();
  }

  PluginConfig::PluginConfig(std::initializer_list<std::pair<std::string, Config>> cfgs)
    : PluginConfig(ConfigMap{cfgs}) {}
  
  PluginConfig::PluginConfig(HighFive::Group container) {
    Config::readH5(myConfigs, container);
    sort();
  }
  
  Config const&
  PluginConfig::config(std::string name) const {
    auto found = std::find_if(myConfigs.begin(), myConfigs.end(),
                              [=](auto const& kv) { return kv.first == name; });
    if (found == myConfigs.end()) {
      throw std::out_of_range(name);
    } else {
      return found->second;
    }
  }

  std::vector<std::string>
  PluginConfig::enabled() const {
    std::vector<std::string> names;
    names.reserve(myConfigs.size());
    for(auto const& [name,cfg] : myConfigs) {
      names.push_back(name);
    }
    return names;
  }
  
  void
  PluginConfig::dump(std::ostream& out, int tab) const {
    out << tabs(tab) << "Plugin configuration:\n";
    for (auto const& [name, cfg] : namedConfigs()) {
      out << tabs(tab+1) << "* " << name << '\n';
      cfg.dump(out, tab+2);
    }
  }
  
  void
  PluginConfig::writeH5(HighFive::Group container) const {
    Config::writeH5(myConfigs, container);
  }

  void
  to_json(nlohmann::json& j, PluginConfig const& c) {
    to_json(j, c.namedConfigs());
  }
  
  void
  from_json (nlohmann::json const& j, PluginConfig& c) {
    ConfigMap configs;
    from_json(j, configs);
    c = PluginConfig(configs);
  }
}
