// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_OPT_IO_H
#define FARGOCA_OPT_IO_H

#include <optional>

namespace fargOCA {
  template<typename T>
  std::ostream&
  operator<<(std::ostream& out, std::optional<T> const& o) {
    if (o) {
      out << *o;
    } else {
      out << "<nullopt>";
    }
    return out;
  }
}

#endif
  
