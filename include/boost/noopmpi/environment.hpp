// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>
// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_ENVIRONMENT_HPP
#define BOOST_NOOPMPI_ENVIRONMENT_HPP

#include <boost/noopmpi/config.hpp>
#include <boost/noopmpi/c_mpi.hpp>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <optional>
#include <string>
#include <iosfwd>

namespace boost { namespace noopmpi {
namespace threading {
/** @brief specify the supported threading level. */
enum level { single, funneled, serialized, multiple };

/** Formated output for threading level. */
std::ostream& operator<<(std::ostream& out, level l);

/** Formated input for threading level. */
std::istream& operator>>(std::istream& in, level& l);
}

/** @brief Initialize, finalize, and query the MPI environment.
 *
 *  But we do not have one.
 */
class BOOST_MPI_DECL environment : noncopyable {
public:
  /** @brief No op */
  template<typename... TS>
    explicit environment(TS... /* unused */) { is_initialized = true;}
  ~environment() {is_initialized = false;}

  /** @brief Abort he only process. */
  static void abort(int /* errcode */) { std::abort(); }

  /** @brief Tell the MPI environment has already been initialized. */
  static bool initialized() { return is_initialized; }

  /** @brief Same as !initialized(). */
  /** Probably diferent from the C counterpart. */
  static bool finalized() { return !initialized(); }

  /** @brief Retrieves the maximum tag value. */
  static int max_tag();

  /** @The tag value used for collective operations. */
  static int collectives_tag();

  /** @returns 0 */
  static optional<int> host_rank() { return 0; }

  /** @return 0 */
  static optional<int> io_rank() { return 0; }

  /** @returns The name of this processor. */
  static std::string processor_name();

  /** @brief Query the current level of thread support.
   */
  static threading::level thread_level();

  /** @brief Are we in the main thread? (always return true)  */
  static bool is_main_thread();
  
  /** @brief MPI version.
   *
   * Returns a pair with the version and sub-version number.
   */
  static std::pair<int, int> version();

private:
  static bool is_initialized;
};

} }

#endif
