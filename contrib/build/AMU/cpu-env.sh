# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load userspace/all

export CC=gcc
export CXX=g++
export FC=gfortran

module load gcc/13.2.0
module load openmpi/gcc132-5.0.2
alias cmake=/scratch/aminiussi/soft/cmake-3.29.0/bin/cmake
source /scratch/aminiussi/soft/boost/1.84.0/env.sh
source /scratch/aminiussi/soft/hdf5/1.14.3/env.sh



