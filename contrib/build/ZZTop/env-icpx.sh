# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module use /opt/OCA/
module load compilers/intel/2022.2.0
module load boost/1.79.0
module load hdf5/1.12.2/gcc-12.2.0-cxx 
