// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <cmath>
#include <cstdio>
#include <cassert>

#include "allmpi.hpp"

#include "boundariesConditionsPlugin.hpp"
#include "loops.hpp"
#include "diskProfiles.hpp"
#include "pluginImpl.hpp"
#include "pluginUtils.hpp"

namespace fargOCA {
  // Instanciate the code here  
  template class Plugin<BoundariesConditions, std::string, Disk&, real>;

  // Somme common implementations:
  
  namespace kk  = Kokkos;
  using std::size_t;

#define LBD KOKKOS_LAMBDA
  
  namespace bc {
    namespace plugins {
      namespace impl {
        void
        openRadial(std::string name, Disk& disk, real ) {
          auto const& dispatch = disk.dispatch();
          auto const& coords   = dispatch.grid();
          GridSizes const ls   = coords.sizes();
          
          arr3d<real> vrad   = disk.velocity().radial().data();
          arr3d<real> vtheta = disk.velocity().theta().data();
          arr3d<real> vphi   = disk.velocity().phi().data();
          arr3d<real> dens   = disk.mdensity().data();
          
          bool const adiabatic = bool(disk.physic().adiabatic);
          bool const radiative = adiabatic && bool(disk.physic().adiabatic->radiative);
          DiskProfiles const& profiles = disk.profiles();
          arr2d<real const> vthetaProfile  = profiles.velocity().theta(disk.frameVelocity());  
          
          bool const first = dispatch.first();
          bool const last  = dispatch.last();
          arr3d<real> energy   = adiabatic ? disk.menergy()->data()         : arr3d<real>{};
          arr3d<real> radiated = radiative ? disk.radiativeEnergy()->data() : arr3d<real>{};
          
          kk::parallel_for("open_BC", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             if (first) {
                               if (i == 0) {
                                 vphi(i,h,j)   = vphi(i+1,h,j);
                                 dens(i,h,j)   = dens(i+1,h,j);
                                 vrad(i,h,j)   = 0;
                                 vtheta(i,h,j) = vthetaProfile(i+1,h); // really ?? why the +1?
                                 if (adiabatic) {
                                   energy(i,h,j) = energy(i+1,h,j);
                                   if (radiative) {
                                     radiated(i,h,j) = radiated(i+1,h,j);;
                                   }
                                 }
                               } else if (i == 1) {
                                 vrad(i,h,j) = vrad(i+1,h,j) > 0 ? real(0) : vrad(i+1,h,j);
                               }
                             }
                             if (last) {
                               if (i == ls.nr-1) {
                                 vphi(i,h,j) = vphi(i-1,h,j);
                                 dens(i,h,j) = dens(i-1,h,j);
                                 vrad(i,h,j) = fmax(vrad(i-1,h,j), real(0)); // we just allow outflow 
                                 vtheta(i,h,j) = vthetaProfile(i,h);
                                 if (adiabatic) {
                                   energy(i,h,j) = energy(i-1,h,j);
                                   if (radiative) {
                                     radiated(i,h,j) = radiated(i-1,h,j);
                                   }
                                 }
                               }
                             }
                           });
        };
      }
      
      BoundariesConditionsPlugin openRadialBoundaries{
        "open_radial",
        impl::openRadial,
        "let stuff get lost through the inner and outer boundaries of the disk.",
        {}
      };
      namespace obsolete {
        auto openRadialBoundaries = BoundariesConditionsPlugin::obsolete("open", plugins::openRadialBoundaries);
      }
      
      template<GridSpacing GS>
      struct ReflectingRadial final {
        void operator()(std::string name, Disk& disk, real /* dt */) const {
          GridDispatch const& dispatch = disk.dispatch();
          
          arr3d<real> vrad   = disk.velocity().radial().data();
          arr3d<real> vtheta = disk.velocity().theta().data();
          arr3d<real> vphi   = disk.velocity().phi().data();
          arr3d<real> dens   = disk.mdensity().data();
          
          auto const& coords = dispatch.grid().as<GS>();
          GridSizes const ls = coords.sizes();
          
          auto radMed = coords.radiiMed();
          auto phiMed = coords.phiMed();
          
          bool const adiabatic = bool(disk.physic().adiabatic);
          bool const radiative = adiabatic && bool(disk.physic().adiabatic->radiative);
          real const frameVelocity = disk.frameVelocity();
          bool const first = dispatch.first();
          bool const last  = dispatch.last();

          arr3d<real> energy    = adiabatic ? disk.menergy()->data() : arr3d<real>{};
          arr3d<real> radenergy = radiative ? disk.radiativeEnergy()->data() : arr3d<real>{};
                    
          // Reflecting boundaries on r direction 
          kk::parallel_for("reflecting_radial_bc", fullRange(coords),
                           LBD(size_t const i, size_t const h, size_t const j) {
                             if (first) {
                               if (i == 0) {
                                 vrad(i,h,j) = -vrad(i+2,h,j);
                                 vphi(i,h,j) = vphi(i+1,h,j);
                                 dens(i,h,j) = dens(i+1,h,j);
                                 vtheta(i,h,j) = (sqrt(G/radMed(i)) - frameVelocity*radMed(i))*std::sin(phiMed(h));
                                 if (adiabatic) {
                                   energy(i,h,j) = energy(i+1,h,j);
                                   if (radiative){
                                     radenergy(i,h,j) = radenergy(i+1,h,j);
                                   }
                                 }
                               } else if(i == 1) {
                                 vrad(i,h,j) = 0;
                               }
                             }
                             if (last) {
                               if (i == ls.nr-1) {
                                 vrad(i,h,j) = 0;
                                 vphi(i,h,j) = vphi(i-1,h,j);
                                 dens(i,h,j) = dens(i-1,h,j);
                                 vtheta(i,h,j) = (sqrt(G/radMed(i)) - frameVelocity*radMed(i))*std::sin(phiMed(h));
                                 if (adiabatic) {
                                   energy(i,h,j) = energy(i-1,h,j);
                                   if (radiative) {  
                                     radenergy(i,h,j) = radenergy(i-1,h,j);
                                   }
                                 }
                               }
                             }
                           });
        }
      };
        
      BoundariesConditionsPlugin reflectingRadial{
        "reflecting_radial",
        BoundariesConditionsPlugin::selectSpacing<ReflectingRadial>(),
        "The stuff is reflected on the raidal borders of the disk.",
        {}
      };
      
      namespace obsolete {
        auto reflectingRadial {BoundariesConditionsPlugin::obsolete("reflectingR", plugins::reflectingRadial)};
      }
      
      namespace impl {
        void
        reflectingZ(std::string name, Disk& disk, real /* dt */) {
          GridDispatch const& dispatch = disk.dispatch();
          
          arr3d<real> vrad   = disk.velocity().radial().data();
          arr3d<real> vtheta = disk.velocity().theta().data();
          arr3d<real> vphi   = disk.velocity().phi().data();
          arr3d<real> dens   = disk.mdensity().data();
          
          auto const& coords = dispatch.grid();
          GridSizes const ls = coords.sizes();
          
          auto const& physic = disk.physic();
          bool half = disk.physic().shape.half;
          bool adiabatic = bool(physic.adiabatic);
          bool radiative = bool(physic.adiabatic) && bool(physic.adiabatic->radiative);
          
          real nan = std::numeric_limits<real>::quiet_NaN();
          real specificHeat    = adiabatic ? disk.physic().adiabatic->specificHeat() : nan;
          real zTemperature    = radiative ? physic.adiabatic->radiative->zBoundaryTemperature() : nan;
          real zBoundaryEnergy = radiative ? physic.adiabatic->radiative->zBoundaryEnergy() : nan;
          
          // Reflecting boundaries on z direction 
          arr3d<real> energy   = adiabatic ? disk.menergy()->data() : arr3d<real>();
          arr3d<real> energrad = radiative ? disk.radiativeEnergy()->data() : arr3d<real>();
          
          kk::parallel_for(range2D({0,0},{ls.nr,ls.ns}), LBD(int i, int j) {
              vrad(i,0,j) = vrad(i,1,j);
              vtheta(i,0,j) = vtheta(i,1,j);
              vphi(i,0,j) = -vphi(i,2,j); 
              vphi(i,1,j) = 0;
              dens(i,0,j) = dens(i,1,j);
              if (adiabatic) {
                if (radiative) {
                  energy(i,0,j)   = zTemperature*specificHeat*dens(i,0,j);
                  energrad(i,0,j) = zBoundaryEnergy;
                } else {
                  energy(i,0,j) = energy(i,1,j);
                }
              }
              
              vrad(i, ls.ni-1,j)   = vrad(i,ls.ni-2,j);
              vtheta(i, ls.ni-1,j) = vtheta(i,ls.ni-2,j);
              vphi(i, ls.ni-1,j)  = 0;
              dens(i, ls.ni-1,j)   = dens(i,ls.ni-2,j);
              
              // No extra condition for the half disk since there is no boundary temperature 
              if(adiabatic) {
                if (radiative) {
                  if(!half){
                    energy(i,ls.ni-1,j)   = zTemperature*specificHeat*dens(i,ls.ni-1,j);
                    energrad(i,ls.ni-1,j) = zBoundaryEnergy;
                  } else {
                    energy(i,ls.ni-1,j)   = energy(i,ls.ni-2,j);
                    energrad(i,ls.ni-1,j) = energrad(i,ls.ni-2,j);
                  }
                } else {
                  energy(i,ls.ni-1,j) = energy(i,ls.ni-2,j);
                }
              }
            });
        }
      }
      
      BoundariesConditionsPlugin reflectingAltitude{
        "reflecting_altitude",
        impl::reflectingZ,
        "Make stuff bounce on the top an down border.",
        {}
      };
      namespace obsolete {
        auto reflectingAltitude = BoundariesConditionsPlugin::obsolete("reflectingZ", plugins::reflectingAltitude);
      }
  
      namespace impl {
        //\brief{Computation of damping boundaries in 2D, replaces  waveKiller}
        template<GridSpacing GS>
        struct Evanescent2D {
          void operator()(std::string name, Disk& disk, real step) const {
            GridDispatch const& dispatch = disk.dispatch();
            
            arr3d<real> vrad   = disk.velocity().radial().data();
            arr3d<real> vtheta = disk.velocity().theta().data();
            arr3d<real> dens   = disk.mdensity().data();
            real const xlambda = 20; 
            
            auto const& lcoords = dispatch.grid().as<GS>();
            auto const& gcoords = lcoords.global();
            GridSizes const ls = lcoords.sizes();
            size_t const gnr = gcoords.sizes().nr;
            
            auto radMed  = lcoords.radiiMed();
            auto gRadMed = gcoords.radiiMed();
            real const gRadMedMin = gRadMed(0);
            real const gRadMedMax = gRadMed(gnr-1);
            
            DiskProfiles const& profiles = disk.profiles();
            arr2d<real const> vthetaProfile  = profiles.velocity().theta(disk.frameVelocity());
            arr2d<real const> vradialProfile = profiles.velocity().radial();
            arr2d<real const> densityProfile = profiles.density();
            real const xfactor = 1.5;
            real const DRMIN   = gRadMedMin * ipow<2>(std::cbrt(xfactor));
            real const DRMAX   = gRadMedMax / ipow<2>(std::cbrt(xfactor));
            real const Tin     = 2*PI*ipow<3>(std::sqrt(gRadMedMin));
            real const Tout    = 2*PI*ipow<3>(std::sqrt(gRadMedMax));
            
            kk::parallel_for("evanescent_BC_2D", range2D({0,0}, {ls.nr, ls.ns}),
                             LBD(size_t const i, size_t const j) {
                               // Damping operates only inside the wave killing zones 
                               // Prescription to damping boundaries of Benitez-Llambay et al. 2016
                               
                               if (radMed(i) <= DRMIN) {
                                 // Orbital period at inner boundary 
                                 real const damping = (radMed(i)-DRMIN)/(gRadMedMin-DRMIN);
                                 real const lambda  = ipow<2>(damping)*xlambda*step/Tin;
                                 if(i == 0){
                                   vtheta(i,0,j) = vthetaProfile(i,0);
                                   vrad(i,0,j)   = vradialProfile(i,0);
                                   dens(i,0,j)   = densityProfile(i,0);
                                 } else{
                                   vrad(i,0,j)     = (vrad(i,0,j)+lambda*vradialProfile(i,0))/(1+lambda);
                                   dens(i,0,j)     = (dens(i,0,j)+lambda*densityProfile(i,0))/(1+lambda);
                                 }
                               }
                               
                               if (radMed(i) >= DRMAX) {
                                 // Orbital period at outer boundary 
                                 real const damping = (radMed(i)-DRMAX)/(gRadMedMax-DRMAX);
                                 real const lambda  = damping*damping*xlambda*step/Tout;
                                 if (i == ls.nr-1){
                                   vtheta(i,0,j) = vthetaProfile(i,0);
                                   vrad(i,0,j)   = vradialProfile(i,0);
                                   dens(i,0,j)   = densityProfile(i,0);
                                 } else{
                                   vrad(i,0,j)   = (vrad(i,0,j)+lambda*vradialProfile(i,0))/(1+lambda);
                                   dens(i,0,j)   = (dens(i,0,j)+lambda*densityProfile(i,0))/(1+lambda);
                                 }
                               }
                             });
          }
        };
      }

      BoundariesConditionsPlugin evanescent2D{
        "evanescent2D",
        BoundariesConditionsPlugin::selectSpacing<impl::Evanescent2D>(),
        "Let stuff go through",
        {}
      };

      namespace impl {
        template<GridSpacing GS>
        struct EvanescentBoundaryIn {
          void operator()(std::string name, Disk& disk, real step) const {
            GridDispatch const& dispatch = disk.dispatch();
            auto const& cfg = disk.physic().boundaries.config(name);
            
            arr3d<real> vrad   = disk.velocity().radial().data();
            arr3d<real> vtheta = disk.velocity().theta().data();
            arr3d<real> vphi   = disk.velocity().phi().data();
            arr3d<real> dens   = disk.mdensity().data();
            real const xfactor = cfg.value<real>("xfactor").value_or(1.5);
            real const xlambda = cfg.value<real>("xlambda").value_or(20);
            
            auto const& lcoords = dispatch.grid().as<GS>();
            auto const& gcoords = lcoords.global();
            GridSizes const ls = lcoords.sizes();
            size_t const gnr = gcoords.sizes().nr;
            
            auto radMed = lcoords.radiiMed();
            // Orbital period at inner and outer boundary 
            auto gRadMed = gcoords.radiiMed();
            real const gRadMedMin = gRadMed(0);
            real const gRadMedMax = gRadMed(gnr-1);
            real const Tin        = 2*PI*ipow<3>(std::sqrt(gRadMedMin));
            
            // Prescription to damping boundaries of Benitez-Llambay et al. 2016
            real const DRMIN = gRadMedMin * ipow<2>(std::cbrt(xfactor));
            DiskProfiles const& profiles = disk.profiles();
            arr2d<real const> vthetaProfile  = profiles.velocity().theta(disk.frameVelocity());
            arr2d<real const> vradialProfile = profiles.velocity().radial();
            arr2d<real const> vphiProfile    = profiles.velocity().phi();
            arr2d<real const> densityProfile = profiles.density();
            kk::parallel_for(fullRange(lcoords),
                             LBD(int i, int h, int j) {
                               // Damping operates only inside the wave killing zones 
                               if (radMed(i) < DRMIN) {
                                 real const damping = (radMed(i)-DRMIN)/(gRadMedMin-DRMIN);
                                 real const lambda = damping*damping*xlambda*step/Tin;
                                 
                                 real vtheta0 = vthetaProfile(i,h);
                                 real vrad0   = vradialProfile(i,h);
                                 real vphi0   = vphiProfile(i,h);
                                 real dens0   = densityProfile(i,h);
                                 
                                 vrad(i,h,j)   = (vrad(i,h,j)+lambda*vrad0)/(1+lambda);
                                 vtheta(i,h,j) = (vtheta(i,h,j)+lambda*vtheta0)/(1+lambda);
                                 vphi(i,h,j)   = (vphi(i,h,j)+lambda*vphi0)/(1+lambda);
                                 dens(i,h,j)   = (dens(i,h,j)+lambda*dens0)/(1+lambda);
                               }
                             });
          }
        };
        
        template<GridSpacing GS>
        struct EvanescentBoundaryOut {
          void operator()(std::string name, Disk& disk, real step) const {
            GridDispatch const& dispatch = disk.dispatch();
            auto const& cfg = disk.physic().boundaries.config(name);
            
            arr3d<real> vrad   = disk.velocity().radial().data();
            arr3d<real> vtheta = disk.velocity().theta().data();
            arr3d<real> vphi   = disk.velocity().phi().data();
            arr3d<real> dens   = disk.mdensity().data();
            real const xfactor = cfg.value<real>("xfactor").value_or(1.5);
            real const xlambda = cfg.value<real>("xlambda").value_or(20);
            
            auto const& lcoords = dispatch.grid().as<GS>();
            auto const& gcoords = lcoords.global();
            GridSizes const ls = lcoords.sizes();
            size_t const gnr = gcoords.sizes().nr;
            
            auto radMed  = lcoords.radiiMed();
            auto gRadMed = gcoords.radiiMed();
            
            DiskProfiles const& profiles = disk.profiles();
            arr2d<real const> vthetaProfile  = profiles.velocity().theta(disk.frameVelocity());
            arr2d<real const> vradialProfile = profiles.velocity().radial();
            arr2d<real const> vphiProfile    = profiles.velocity().phi();
            arr2d<real const> densityProfile = profiles.density();    
            kk::parallel_for(fullRange(lcoords),
                             LBD(int i, int h, int j) {
                               real const gRadMedMax = gRadMed(gnr-1);
                               // Prescription to damping boundaries of Benitez-Llambay et al. 2016
                               real const DRMAX = gRadMedMax*pow(xfactor,-2./3.);
                               if (radMed(i) > DRMAX) {
                                 real const Tout    = 2*PI*ipow<3>(std::sqrt(gRadMedMax));
                                 real const damping = (radMed(i)-DRMAX)/(gRadMedMax-DRMAX);
                                 real const lambda  = damping*damping*xlambda*step/Tout;
                                 
                                 real vtheta0 = vthetaProfile(i,h);
                                 real vrad0   = vradialProfile(i,h);
                                 real vphi0   = vphiProfile(i,h);
                                 real dens0   = densityProfile(i,h);
                                 
                                 vrad(i,h,j)   =   (vrad(i,h,j)+lambda*vrad0)/(1+lambda);
                                 vtheta(i,h,j) = (vtheta(i,h,j)+lambda*vtheta0)/(1+lambda);
                                 vphi(i,h,j)   = (vphi(i,h,j)+lambda*vphi0)/(1+lambda);
                                 dens(i,h,j)   = (dens(i,h,j)+lambda*dens0)/(1+lambda);
                               }
                             });
          }
        };
      }
      
      BoundariesConditionsPlugin evanescentIn{
        "inner_evanescent",
        BoundariesConditionsPlugin::selectSpacing<impl::EvanescentBoundaryIn>(),
        "Let stuff before DRMIN go through the inner radial border.",
        {"xfactor", "xlambda"}
      };

      BoundariesConditionsPlugin evanescentOut{
        "outer_evanescent",
        BoundariesConditionsPlugin::selectSpacing<impl::EvanescentBoundaryOut>(),
        "Let stuff after DRMAX go through the outer radial border.",
        {"xfactor", "xlambda"}
      };      
      
      namespace obsolete {
        auto evanescentIn  =  BoundariesConditionsPlugin::obsolete("evanescentIn", plugins::evanescentIn);
        auto evanescentOut =  BoundariesConditionsPlugin::obsolete("evanescentOut", plugins::evanescentOut);
      }
      
      namespace impl {
        std::string
        evanescentDesc() {
          std::ostringstream desc;
          desc << "Apply '" << evanescentIn.name() << "' and, the the star is not heating, "
               << evanescentIn.name();
          return desc.str();
        }
      }
      BoundariesConditionsPlugin evanescent{
        "evanescent", 
        [](std::string name, Disk& disk, real dt) -> void {
          reflectingRadial(name, disk, dt);
          evanescentIn(name, disk, dt);
          // Radial dumping in the outer region doesn't operate in case of stellar heating
          auto adiabatic = disk.physic().adiabatic; 
          bool warmRadiativeStar = (adiabatic 
                                    && adiabatic->radiative && adiabatic->radiative->star  
                                    && disk.physic().units.centralBodyTemperature() > 0); 
          if (!warmRadiativeStar) {
            evanescentOut(name, disk, dt);
          }
        },
        impl::evanescentDesc(),
        {}
      };
    }
  }
}
