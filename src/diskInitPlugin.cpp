// Copyright 2024, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2025, Maya Tatarelli  maya.tatarelli<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <cmath>
#include <cstdio>
#include <string>
#include <set>
#include <random>

#include <Kokkos_Core.hpp>

#include "diskInitPlugin.hpp"
#include "diskPhysic.hpp"
#include "gridDispatch.hpp"
#include "simulation.hpp"
#include "disk.hpp"
#include "loops.hpp"
#include "pluginImpl.hpp"
#include "pluginUtils.hpp"

namespace fargOCA {
  namespace kk = Kokkos;
#define LBD KOKKOS_LAMBDA
  
  namespace {
    template<GridSpacing GS>  struct DensityWithMRI {
      void operator()(std::string name, Disk& disk, DiskInitEvent event) const {
	if (event == DiskInitEvent::DENSITY) {
	  /// MRI only make sense if we have 2D MRI Viscosity
	  /// Then we can reuse the viscosity configuration.
	  Config config = disk.physic().viscosity.config;
	  real const value            {*config.value<real>("value")};
	  real const outerTemperature {*config.value<real>("outerTemperature")};
	  real const transitionRadius {*config.value<real>("transitionRadius")};
	  real const MRIAlpha         {*config.value<real>("MRIAlpha")};
	  real const MRITemperature   {*config.value<real>("MRITemperature")};
	  
	  auto        aspectRatio {disk.physic().aspectRatio};
	  real const  rate        {disk.physic().starAccretion->rate()};
	  auto const& coords      {disk.dispatch().grid().as<GS>()};
	  auto        radMed      {coords.radiiMed()};
	  auto        density     {disk.mdensity().data()};
	  kk::parallel_for(fullFlatRange(coords),
			   LBD(size_t const  i, size_t const  j) {
			     real const r = radMed(i);
                             real const transition = ((1-std::tanh((MRITemperature/outerTemperature)
                                                                   *(1-transitionRadius/r)))
                                                      /2);
                             real const MRI2Dz = (MRIAlpha - value)*transition + value;
                             
			     density(i,0, j) = (rate
						/(3*PI*MRI2Dz
						  *pow(radMed(i)
						       *aspectRatio(r),2)
						  *sqrt(G/(pow(r,3)))));
			   });
	}
      }
    };
  
    DiskInitPlugin MRIDensity{
      "MRI_to_dz_density",
      DiskInitPlugin::selectSpacing<DensityWithMRI>(),
      "Case of a viscosity transition (MRIToDz) and constant mdot disk "
      "the initial surface density is then known to be mdot/(3*pi*nu), nu=alpha*h**2*r**2*OmegaK(r)."
      "Configuration is extracted from viscosity configuration.",
      {"value", "outerTemperature", "transitionRadius", "MRIAlpha", "MRITemperature"}
    };

    // Velocity noise plugin
    void
    noise(GridDispatch const& dispatch, std::mt19937& gen, real min, real max, arr3d<real> chaos) {
      auto hnz = kk::create_mirror(chaos);
      std::uniform_real_distribution<real> dist(min, max);
      
      auto const& coords = dispatch.grid();
      GridSizes const ls = coords.sizes();
      size_t const gnr   = coords.global().sizes().nr;
      
      harr3d<real> grand("global random field", gnr,ls.ni,ls.ns);
      // no parallelism, we need to be able to reproduce
      for (size_t i = 0; i < gnr; ++i) {
	for (size_t h = 0; h < ls.ni; ++h) {
	  for (size_t j = 0; j < ls.ns; ++j) {
	    grand(i,h,j) = dist(gen); 
	  }
	}
      }
      kk::deep_copy(hnz, dispatch.localView(grand));
      kk::deep_copy(chaos, hnz);
    }
  
    template<GridSpacing GS> struct AddVelocityNoise {
      void operator()(std::string name, Disk& disk, DiskInitEvent event) const {
	if (event == DiskInitEvent::VELOCITY) {
	  std::mt19937 gen;
	  auto const& physic = disk.physic();
	  auto const& grid   = disk.dispatch().grid();
	  
	  real const rcav= physic.density.cavity.radius;
	  
	  arr3d<real> vradial = disk.velocity().radial().data();
	  arr3d<real> vphi    = disk.velocity().phi().data();
	  arr3d<real> vtheta  = disk.velocity().theta().data();
	  auto const& coords  = grid.as<GS>();
	  
	  if(rcav > 0) {
	    real const  sigma     = physic.density.cavity.width;
	    GridSizes const ls    = coords.sizes();
	    size_t    const fldSz = coords.footprint();
	    auto      radMed      = coords.radiiMed();
	    
	    arr3d<real> rand = scalarView("noise", grid);
	    
	    noise(disk.dispatch(), gen, -0.1, 0.1, rand);
	    arr1d<real> rnoise("noise", ls.nr);
	    kk::parallel_for(kk::RangePolicy<>(0,ls.nr),
			     LBD(int i) {
			       rnoise(i) = kk::exp(-ipow<2>((radMed(i)-rcav))/10/ipow<2>(sigma)); 
			     });
	    kk::parallel_for("noise", fullRange(coords),
			     LBD(size_t const i, size_t const h, size_t const j) {
			       vradial(i,h,j) += rnoise(i)*rand(i,h,j)*vtheta(i,h,j);
			     });
	  } else {
	    arr3d<real const> cs           = disk.soundSpeed().data();
	    arr3d<real> vradialnoise = scalarView("vrnoise", grid);
	    arr3d<real> vthetanoise  = scalarView("vthetanoise", grid);
	    arr3d<real> vphinoise    = scalarView("vphinoise", grid);
	    noise(disk.dispatch(), gen, -1e-2, 1e-2, vradialnoise);
	    noise(disk.dispatch(), gen, -1e-2, 1e-2, vthetanoise);
	    noise(disk.dispatch(), gen, -1e-2, 1e-2, vphinoise);	  
	    kk::parallel_for("rand_noise", fullRange(coords),
			     LBD(size_t const i, size_t const h, size_t const j) { 
			       vradial(i,h,j) += vradialnoise(i,h,j)*cs(i,h,j);
			       vphi(i,h,j)    += vphinoise(i,h,j)*cs(i,h,j);
			       vtheta(i,h,j)  += vthetanoise(i,h,j)*cs(i,h,j);
			     });
	  }
	}
      }
    };

    DiskInitPlugin VelocityNoise{
      "velocity_noise",
      DiskInitPlugin::selectSpacing<AddVelocityNoise>(),
      "Add noise to the velocity field.",
      {}
    };
  }
  
  template class Plugin<DiskInitEvent, std::string, Disk&, DiskInitEvent>;
}
