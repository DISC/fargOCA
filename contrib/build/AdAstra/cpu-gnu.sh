module purge

module load develop

export SPACK_USER_PREFIX=/lus/work/CT4/cin7233/SHARED/spack
if [[ ! "${SPACK_USER_PREFIX}/bin" =~ ${PATH} ]]
then
    export PATH=${SPACK_USER_PREFIX}/bin:${PATH}
fi
module load GCC-CPU-4.0.0
module load gcc/13.2.0
module load cray-python/3.11.5
module load boost/1.86.0-mpi

export HDF5_DIR=/lus/work/CT4/cin7233/SHARED/opt/hdf5/1.14.5
if [[ ! "${HDF5_DIR}/cmake" =~ ${CMAKE_PREFIX_PATH} ]]
then
    export CMAKE_PREFIX_PATH=${HDF5_DIR}/cmake:${CMAKE_PREFIX_PATH}
fi

export CC=gcc
export CXX=g++
export FC=gfortran
export MPICH_GPU_SUPPORT_ENABLED=0
export CTI_SLURM_OVERRIDE_MC=1
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
