// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <sysexits.h>
#include <filesystem>

#include "utils.hpp"

namespace po = boost::program_options;
namespace fs = std::filesystem;

void
check_mandatory_options(po::options_description const opts,
                        po::variables_map const& vm,
                        std::initializer_list<std::string> mandatory,
                        std::ostream& log) {
  bool missing = false;
  for (std::string opt : mandatory ) {
    if (!bool(vm.count(opt))) {
      log << "missing mandatory option --" << opt << "\n";
      missing = true;
    }
  }
  if (missing) {
    log << "Usage:\n" << opts;
    std::exit(EX_USAGE);
  }
}

int
backup_file(std::string ifile, std::string new_ext, bool force, bool abort_on_error) {
  fs::path ipath = ifile;
  int status;
  if (!fs::exists(ipath)) {
    std::cerr << "input file '" << ifile << "' does not exist.\n";
    status = EX_NOINPUT;
  } else {
    fs::path backup = ipath;
    backup.replace_extension(new_ext);
    if (force) {
      if (fs::exists(backup)) {
        fs::remove(backup);
      } 
    }
    if (fs::exists(backup)) {
      std::cerr << "backup file  '" << backup.native() << "' already exist.\n";
      status = EX_NOPERM;
    } else {
      try {
        fs::copy_file(ipath, backup);
        status = EX_OK;
      } catch(fs::filesystem_error const& e) {
        std::cerr << "Could not backup file because:\n"
                  << e.what() << '\n';
        status = EX_IOERR;
      }
    }
  }
  if (abort_on_error && status != EX_OK) {
    std::exit(status);
  }
  return status;
}

