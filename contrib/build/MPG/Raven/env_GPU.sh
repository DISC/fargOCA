# Copyright 2020, Gabriele Pichierri, pichierri@mpia.de
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load gnuplot/5.4 gcc/11 anaconda/3/2021.11 openmpi/4 cuda/11.4 hdf5-serial/1.12.2  doxygen/1.8 boost-mpi/1.74

export CC=gcc
export FC=gfortran
export CXX=g++
