// Copyright 2021, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_CUDA_WORKAROUNDS_H
#define FARGOCA_CUDA_WORKAROUNDS_H

#if defined(__CUDACC_EXTENDED_LAMBDA__)
# define cuda_protected public
# define cuda_private   public
#else
# define cuda_protected protected
# define cuda_private   private
#endif

#endif
