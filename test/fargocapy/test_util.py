import numpy as np
import sys

def tprint(out,t,s):
    out.write(t*"  "+s+"\n")

def display_referential(out,t,r):
    tprint(out,t, "Referential:")
    tprint(out,t+1,"type: {}".format(r.type))
    tprint(out,t+1,"omega: {}".format(r.omega))
    tprint(out,t+1,"indirectForces: {}".format(r.indirectForces))
    tprint(out,t+1,"guidingCenter: {}".format(r.guidingCenter))
    tprint(out,t+1,"guidingPlanet: {}".format(r.guidingPlanet))

def display_boundaries(out,t,b):
    tprint(out,t, "Boundaries:")
    tprint(out,t+1,"handlers: {}".format(b.handlers))
    tprint(out,t+1,"params: {}".format(b.params))

def display_viscosity(out,t,v,radius):
    tprint(out,t, "Viscosity:")
    tprint(out,t+1, "type: {}".format(v.type))
#   tprint(out,t+1, "value: {}".format(v.value))
    tprint(out,t+1, "artificial: {}".format(v.artificial))

def display_star_accretion(out,t,a):
    tprint(out,t, "Star Accretion:")
    tprint(out,t+1,"type: {}".format(a.type))
    tprint(out,t+1,"user rate: {}".format(a.userRate))
    tprint(out,t+1,"rate: {}".format(a.rate))
    if not a.wind:
        tprint(out,t+1,"Wind: Nope")
    else:
        tprint(out,t+1, "Wind:")
        tprint(out,t+2, "user active density: {}".format(a.wind.userActiveDensity))
        tprint(out,t+2, "filter: {}".format(a.wind.filter))
        tprint(out,t+2, "active density: {}".format(a.wind.activeDensity))
        tprint(out,t+2, "torque coeff: {}".format(a.torqueCoeff))

def display_adiabatic(out,t,a):
    tprint(out,t, "Adiabatic:")
    if not a.radiative:
        tprint(out,t+1,"Radiative: Nope")
    else:
        tprint(out,t+1, "Radiative:")
        tprint(out,t+2, "Boundary Temperature: {}".format(a.radiative.userZBoundaryTemperature))
        tprint(out,t+2, "opacity Law: {}".format(a.radiative.opacityLaw))
        tprint(out,t+2, "const Kappa: {}".format(a.radiative.constKappa))
        tprint(out,t+2, "dust to Gas: {}".format(a.radiative.dustToGas))
        tprint(out,t+2, "epsilon: {}".format(a.radiative.epsilon))
        if not a.radiative.energyTimeStep:
            tprint(out,t+2,"star: Nope")
        else:
            tprint(out,t+2, "Star:")
            tprint(out,t+3, "tStar: {}".format(a.radiative.star.tStar))
            if not a.radiative.star.shadowAngle:
                tprint(out,t+3,"shadowAngle: Nope")
            else:
                tprint(out,t+3, "shadowAngle: {}".format(a.radiative.star.shadowAngle))
            tprint(out,t+3, "fStar: {}".format(a.radiative.star.fStar))
    if not a.cooling:
        tprint(out,t+1,"Cooling: Nope")
    else:
        tprint(out,t+1, "Cooling: ")
        tprint(out,t+2, "timeScale: {}".format(a.cooling.timeScale))
        tprint(out,t+2, "cooling Time: {}".format(a.cooling.coolingTime(1)))
    tprint(out,t+1, "index: {}".format(a.index))
    tprint(out,t+1, "specificHeat: {}".format(a.specificHeat))

def display_velocities(out,t,a):
    tprint(out,t, "Velocities:")
    tprint(out,t+1, "with random noise: {}".format(a.withRandomNoise))

def display_density(out,t,a):
    tprint(out,t, "Density:")
    tprint(out,t+1, "slope: {}".format(a.slope))
    tprint(out,t+1, "start: {}".format(a.start))
    tprint(out,t+1, "minimum: {}".format(a.minimum))
    tprint(out,t+1, "Cavity:")
    tprint(out,t+2, "radius: {}".format(a.cavity.radius))
    tprint(out,t+2, "ratio: {}".format(a.cavity.ratio))
    tprint(out,t+2, "width: {}".format(a.cavity.width))

def display_smoothing(out,t,a):
    tprint(out,t, "Smoothing:")
    tprint(out,t+1, "change: {}".format(a.change))
    tprint(out,t+1, "flat: {}".format(a.flat))
    tprint(out,t+1, "taper: {}".format(a.taper))
    tprint(out,t+1, "size: {}".format(a.size))
    tprint(out,t+1, "smooth: {}".format(a.smooth(1,1)))

def display_orbitalElements(out,t,orb):
    tprint(out,t, "Orbital elements: ")
    tprint(out,t+1, "semi major axis: {}"        .format(orb.semiMajorAxis))
    tprint(out,t+1, "eccentricity: {}"           .format(orb.eccentricity))
    tprint(out,t+1, "inclination: {}"            .format(orb.inclination))
    tprint(out,t+1, "mean longitude: {}"         .format(orb.meanLongitude))
    tprint(out,t+1, "longitude of pericenter: {}".format(orb.longitudeOfPericenter))
    tprint(out,t+1, "longitude of node: {}"      .format(orb.longitudeOfNode))

def display_massTaper(out,t,ms):
    tprint(out,t, "Mass taper:")
    tprint(out,t+1, "initial mass: {}" .format(ms.initialMass))
    tprint(out,t+1, "final mass: {}"   .format(ms.finalMass))
    tprint(out,t+1, "begin time: {}"   .format(ms.beginTime))
    tprint(out,t+1, "end time: {}"     .format(ms.endTime))

    tprint(out,t+1, "before: {}"       .format(ms.before(1)))
    tprint(out,t+1, "active: {}"       .format(ms.active(1)))
    tprint(out,t+1, "after: {}"        .format(ms.after(1)))
    tprint(out,t+1, "mass at: {}"      .format(ms.massAt(1)))

def display_planetarySystemPhysic(out,t,ps):
      tprint(out,t, "Planetary system Physic:")
      tprint(out,t+1, "exclude hill: {}".format(ps.excludeHill))
      tprint(out,t+1, "accrete onto planets: {}".format(ps.accreteOntoPlanets))
      tprint(out,t+1, "Planets:")

      for planet in ps.planets.values(): 
        tprint(out,t+2, "{}: ".format(planet.name))
        display_orbitalElements(out,t+3, planet.orbitalElements)
        tprint(out,t+3, "feel others: {}".format(planet.feelOthers))

        if not planet.luminosity: 
            tprint(out,t+3, "Luminosity: Nope")
        else: 
          tprint(out,t+3, "Luminosity:")
          tprint(out,t+4, "user solid accretion: {} " .format(planet.luminosity.userSolidAccretion))
          tprint(out,t+4, "solid accretion: {} "      .format(planet.luminosity.solidAccretion))
        
        if type(planet.mass)==float:
          tprint(out,t+3, "mass: {}".format(planet.mass))
        else:
          display_massTaper(out,t+3,planet.mass)
        
        tprint(out,t+3, "initial mass: {}" .format(planet.initialMass))
        if not planet.radius:
          tprint(out,t+3, "radius: Nope")
        else:
          tprint(out,t+3, "radius: {}" .format(planet.radius))        

        if not planet.hillRadius:
          tprint(out,t+3, "hill radius: Nope")
        else:
          tprint(out,t+3, "hill radius: {}" .format(planet.hillRadius))
       
      tprint(out,t+1, "Engine:")
      tprint(out,t+2, "name: {}".format(ps.engine.name))
      tprint(out,t+2, "user config: {}" .format(ps.engine.config.registered))

def display_physic(out,t,phy):
    tprint(out,t, "Physic:")
    tprint(out,t+1, "time step: {}".format(phy.timeStep))
    display_referential(out,t+1, phy.referential)
    display_boundaries(out,t+1, phy.boundaries)
    #display_star_accretion(out,t+1, phy.starAccretion)
    display_viscosity(out,t+1, phy.viscosity,phy.shape.gas.radius)
    if not phy.adiabatic:
        tprint(out,t+1, "Adiabatic : Nope")
    else:
        display_adiabatic(out,t+1,phy.adiabatic)
    display_velocities(out,t+1, phy.velocities)
    display_density(out,t+1, phy.density)
    display_smoothing(out,t+1, phy.smoothing)
    if not phy.planetarySystem:
      tprint(out,t+1, "Planetary system: Nope")
    else:
      display_planetarySystemPhysic(out,t+1, phy.planetarySystem)

def explore(simulation,out):
    disk = simulation.disk
    outFile = sys.stdout
    if out:
      outFile = open(out, "w")
    display_physic(outFile,1, disk.physic)
    outFile.write("Simulation.nbSteps: {}".format(simulation.nbSteps))
    outFile.write("Simulation.disk.density.shape: {}".format(simulation.disk.density.values.shape))
    for (n,f) in simulation.disk.fieldsSnapshot.items():
        outFile.write("Field {} shape is {}".format(n,f.values.shape))

    coords = disk.density.grid.coords
    full = coords.full
    outFile.write("radii: {}\nlayers: {}\nsectors{}\n".format(full.radii, full.layers, full.sectors))
