// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "gridDispatch.hpp"
#include "fieldStack.hpp"
#include "environment.hpp"

using namespace fargOCA;
namespace kk = Kokkos;

void
print(std::string tag, FieldStack3D<real> const& stack) {
  std::cout << tag << '\n'
            << "Stack label -> " << stack.label() << '\n'
            << " * base  -> " << stack.base() << '\n'
            << " * watermark  -> " << stack.watermark() << '\n';
  for (size_t i = 0; i < stack.base(); ++i) {
    std::cout << "   - " << stack.at(i).label() << '\n';
  }
}

int
check(FieldStack3D<real> const& stack, size_t wmark, size_t base) {
  int failed = 0;
  if (stack.base() != base) {
    std::cout << "expected base value " << base << " but got " << stack.base() << '\n';
    ++failed;
  }
  if (stack.watermark() != wmark) {
    std::cout << "expected watermark value " << wmark << " but got " << stack.watermark() << '\n';
    ++failed;
  }
  return failed;
}

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  GasShape shape(0.05, 42, 56, 0.001, false);
  auto grid = GridDispatch::make(env.world(), *Grid::make(shape, {10, 20, 30}, GridSpacing::ARITHMETIC ));
  FieldStack3D<real> stack(grid->grid().sizes(), "znort");

  int failed = 0;

  print("empty", stack);  
  LocalFieldStack3D<real,12> level1(stack);
  print("level 1, pushed 12", stack);
  failed += check(stack, 12, 12);
  {
    print("level 2", stack);
    LocalFieldStack3D<real,1> level2(stack);
    print("level 2, pushed 1", stack);  
    failed += check(stack, 13, 13);
  }
  print("back to level, pushed 12", stack);  
  failed += check(stack, 13, 12);
  {
    print("back to level 2", stack);  
    LocalFieldStack3D<real,1> level2(stack);
    print("back to level 2, pushed 1", stack);  
    failed += check(stack, 13, 13);
    {
      print("level 3", stack);  
      LocalFieldStack3D<real,5> level3(stack);
      print("back to level 3, pushed 5", stack);  
      failed += check(stack, 18, 18);
    }
    print("back to level 2", stack);  
    failed += check(stack, 18, 13);
  }
  print("back to level 1", stack); 
  failed += check(stack, 18, 12); 
  return failed;
}
