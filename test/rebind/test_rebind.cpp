// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <cmath>

#include "scalarField.hpp"
#include "arrayTestUtils.hpp"
#include "loops.hpp"

using namespace fargOCA;
namespace kk = Kokkos;
namespace HF = HighFive;

using std::size_t;

struct loc { int i,h,j; };
using max_loc = kk::MaxLoc<real, loc>;
using max_type = max_loc::value_type;

namespace Kokkos { //reduction identity must be defined in Kokkos namespace
  template<>
  struct reduction_identity<loc> {
    KOKKOS_FORCEINLINE_FUNCTION static loc min() { return {0,0,0}; }
  };
}

template<class CINF, class CMED>
arr1d<real>
coord_values(CINF cinf, CMED cmed, GridPosition pos, size_t n) {
  arr1d<real> coords {"coords", n};
  kk::parallel_for("coords", n,
                   KOKKOS_LAMBDA(size_t i) {
                     coords(i) = pos == GridPosition::INF ? cinf(i) : cmed(i);
                   });
  return coords;
}

template<GridSpacing GS>
std::array<arr1d<real>,3>
coord_values(ScalarField const& f) {
  auto const& grid = f.dispatch().grid();
  auto        gpos = f.gridPos();
  auto const& coords = grid.as<GS>();
  arr1d<real> radii { coord_values(coords.radii(), coords.radiiMed(), gpos.radius, coords.sizes().nr)};
  arr1d<real> phi   { coord_values(coords.phi(), coords.phiMed(), gpos.layer, coords.sizes().ni)};
  arr1d<real> theta { coord_values(coords.theta(), coords.thetaMed(), gpos.sector, coords.sizes().ns)};
  return {radii,phi,theta};
}

template<GridSpacing GS>
bool
test_content_impl(std::string label, ScalarField const& field, real atol, real rtol) {
  std::cout << ">>>>>>>>>>>>>>>>>>>>>>>> " << label << '\n';
  auto data = field.data();
  auto coords {coord_values<GS>(field)};
  auto radii {coords[0]};
  auto phi   {coords[1]};
  auto theta {coords[2]};  

  auto diff = maxAbsRelDiff(data,
                             KOKKOS_LAMBDA(size_t i, size_t h, size_t j) -> real {
                               return radii(i) + kk::pow(phi(h),2) + kk::sin(theta(j));
                             });
  
  std::cout << "Test " << label
            << ", max diff between " << field.name()
            << " and expected is " << to_string<real,3>(diff) << '\n';
  
  auto file = h5::createFile(label+".h5", true);
  std::optional<HF::Group> field_group {h5::getRoot(file)->createGroup("field")};
  HF::Group grid_group                 {h5::getRoot(file)->createGroup("grid")};
  field.writeH5(field_group, 0);
  field.dispatch().grid().writeH5(grid_group);
  bool passed = diff.val.absolute <= atol || diff.val.relative <= rtol;
  if (passed) { std::cout << "PASSED\n"; }
  else        { std::cout << "FAILED\n"; }
  return passed;
}

bool
test_content(std::string label, ScalarField const& field, real atol, real rtol) {
  if (field.dispatch().grid().radialSpacing() == GridSpacing::ARITHMETIC) {
    return test_content_impl<GridSpacing::ARITHMETIC>(label, field, atol, rtol);
  } else {
    return test_content_impl<GridSpacing::LOGARITHMIC>(label, field, atol, rtol);
  }
}

int
main() {
  auto constexpr AR = GridSpacing::ARITHMETIC;
  auto constexpr LN = GridSpacing::LOGARITHMIC;
  Environment env;
  int failed = 0;
  {
    auto dispatch_a = GridDispatch::make(env.world(),
                                         *Grid::make(GasShape{1, 2, 6, 0.001, false},
                                                     GridSizes{40, 20, 80},
                                                     AR));
    ScalarField field_a("field_a", *dispatch_a);
    {
      auto data_a = field_a.data();
      auto coords {coord_values<AR>(field_a)};
      auto radii {coords[0]};
      auto phi   {coords[1]};
      auto theta {coords[2]};
      kk::parallel_for("init_field_a", range(data_a),
                       KOKKOS_LAMBDA(size_t const i,size_t const h, size_t const j) {
                         data_a(i,h,j) = radii(i) + kk::pow(phi(h),2) + kk::sin(theta(j));
                     });
      failed += test_content("original", field_a, 5e-15, 5e-16) ? 0 : 1;
    }
    {
      ScalarField field_b {field_a};
      field_b.rebindTo(*dispatch_a);
      failed += test_content("identity_arithmetic", field_b, 5e-15, 5e-16) ? 0 : 1;
    }
    {
      auto dispatch_b = GridDispatch::make(env.world(),
                                           *Grid::make(dispatch_a->grid().shape(),
                                                       GridSizes{80, 20, 80},
                                                       AR));
      ScalarField field_b {field_a};
      field_b.rebindTo(*dispatch_b);
      failed += test_content("double_nr", field_b, 1e-14, 1e-15) ? 0 : 1;
    }
    {
      auto dispatch_b = GridDispatch::make(env.world(),
                                           *Grid::make(dispatch_a->grid().shape(),
                                                       GridSizes{40, 20, 120},
                                                       AR));
      ScalarField field_b {field_a};
      field_b.rebindTo(*dispatch_b);
      failed += test_content("double_ns", field_b, 1e-4, 1e-5) ? 0 : 1;
    }
    {
      auto dispatch_b = GridDispatch::make(env.world(),
                                           *Grid::make(dispatch_a->grid().shape(),
                                                       GridSizes{40, 20, 80},
                                                       LN));
      ScalarField field_b {field_a};
      field_b.rebindTo(*dispatch_b);
      failed += test_content("arithmetic_to_logarithmic", field_b, 2e-14, 5e-15) ? 0 : 1;
    }

    {
      auto dispatch_b = GridDispatch::make(env.world(),
                                           *Grid::make(dispatch_a->grid().shape(),
                                                       GridSizes{80, 20, 80},
                                                       LN));
      ScalarField field_b {field_a};
      field_b.rebindTo(*dispatch_b);
      failed += test_content("arithmetic_to_Logarithmic_double_nr", field_b, 5e-14, 1e-14) ? 0 : 1;
    }

    {
      auto dispatch_b = GridDispatch::make(env.world(),
                                           *Grid::make(dispatch_a->grid().shape(),
                                                       GridSizes{70, 15, 120},
                                                       LN));
      ScalarField field_b {field_a};
      field_b.rebindTo(*dispatch_b);
      failed += test_content("arithmetic_to_logarithmic_almost_double", field_b, 8e-5, 1e-6) ? 0 : 1;
    }
  }
  std::cout << "Failed " << failed << " tests.\n";
  return failed == 0 ? 0 : 1;
}
