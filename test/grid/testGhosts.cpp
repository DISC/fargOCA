// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <limits>
#include <valarray>
#include <boost/lexical_cast.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "gridDispatch.hpp"
#include "scalarField.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;
namespace kk = Kokkos;

static bool testGhostCommunication(GridDispatch const& grid, std::string name);

int
main(int argc, char* argv[]) {
  Environment  env(argc, argv);
  fmpi::communicator world;
  using namespace fargOCA;
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " <globalNRad>\n";
    return -1;
  }
  size_t const globalNRad = boost::lexical_cast<int>(argv[1]);
  GasShape shape(1, 42, 56, 0.001, false);
  auto fatGrid  = GridDispatch::make(world, *Grid::make(shape, {globalNRad, 10u, 33u}, GridSpacing::ARITHMETIC ));
  auto flatGrid = GridDispatch::make(world, *Grid::make(shape, {globalNRad, 1u,  33u}, GridSpacing::ARITHMETIC));
  
  bool passed = true;
  if (!testGhostCommunication(*flatGrid, "flat")) { passed = false; }
  if (!testGhostCommunication(*fatGrid, "fat"))   { passed = false; }
  passed = fmpi::all_reduce(world, passed, std::logical_and<bool>());
  if (world.rank() == 0) {
    if (passed) {
      std::cout << "PASSED\n";
    } else {
      std::cout << "FAILED\n";
    }
  }
  return passed ? 0 : 1;
}

bool
testGhostCommunication(GridDispatch const& dispatch, std::string name) {
  fmpi::communicator const& comm = dispatch.comm();
  ScalarField pg(name, dispatch);  
  // to keep track where the values commes from
  kk::deep_copy(pg.data(), comm.rank());
  // ghosts should have value rank-1 and rank+1:
  pg.setGhosts();
  auto const& coords = dispatch.grid();
  GridSizes const ls = coords.sizes();

  arr3d<real> pgv   = pg.data();
  size_t const ghostNRad = dispatch.ghostSize();
  
  enum Area { INNER = 0, MANAGED = 1, OUTER = 2 };
  real const atol {std::numeric_limits<real>::epsilon() * 20};
  real const rtol {std::numeric_limits<real>::epsilon() * 20};
  bool passed = true;

  if (!dispatch.first()) {
    arr3d<real const> ghost = kk::subview(pgv, range(0, dispatch.ghostSize()), kk::ALL(), kk::ALL());
    arr3d<real>       expct = arr3d<real>("expected", dispatch.ghostSize(), ls.ni, ls.ns);
    kk::deep_copy(expct, real(comm.rank()-1));
    if(!checkAbsRelDiff(maxAbsRelDiff(ghost, expct), atol, rtol, std::cout)) { passed = false; }
  }
  {
    arr3d<real const> managed = dispatch.managedView(pgv);
    arr3d<real>       expct = arr3d<real>("expected", dispatch.nbManagedRadii()(comm.rank()), ls.ni, ls.ns);
    kk::deep_copy(expct, real(comm.rank()));
    if (!checkAbsRelDiff(maxAbsRelDiff(managed, expct), atol, rtol, std::cout)) { passed = false; }
  }
  if (!dispatch.last()) {
    arr3d<real const> ghost = kk::subview(pgv, range(dispatch.managedRadii().second, ls.nr), kk::ALL(), kk::ALL());
    arr3d<real>       expct = arr3d<real>("expected", dispatch.ghostSize(), ls.ni, ls.ns);
    kk::deep_copy(expct, real(comm.rank()+1));
    if (!checkAbsRelDiff(maxAbsRelDiff(ghost, expct), atol, rtol, std::cout)) { passed = false;}
  }

  auto statusLabel = [](bool b) { return b ? std::string("PASSED") : std::string("FAILED");};
  
  for (int rk = 0; rk < comm.size(); ++rk) {
    comm.barrier();
    if (rk == comm.rank()) {
      std::cout << "Grid " << pg.name() 
                << "\t Communication at rank " << rk << statusLabel(passed)
                << '\n';
    }
  }
  return passed;
}
