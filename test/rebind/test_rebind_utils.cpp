// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <cmath>

#include "coords.hpp"
#include "rebind.hpp"

using namespace fargOCA;
namespace kk = Kokkos;
using std::size_t;

template<class COORDS>
bool
test_index_search_impl(COORDS r, real x, size_t first, size_t last, size_t expected) {
  size_t found {closest_coord_index(x, r, first, last)};
  std::cout << "\npos " << x << " found closer to coord " << found << " (" << r(found) << "), "
            << "expected " << expected;
  bool passed = found == expected;
  if (passed) { std::cout << " PASSED\n";}
  else        { std::cout << " FAILED\n";}
  return passed;
}

bool
test_index_search(RegularCoords r, real x, size_t first, size_t last, size_t expected) {
  std::cout << "Regular{"<<  r.base << ',' << r.dx << ',' << r.localOffset << '}';
  for(size_t i = first; i <= last; ++i) {
    std::cout << ' ' << r(i);
  }
  return test_index_search_impl(r, x, first, last, expected);
}

bool
test_index_search(LogCoords r, real x, size_t first, size_t last, size_t expected) {
  std::cout << "Log{" << r.rmin << ',' << r.scaling << ',' << r.localOffset << '}';
  for(size_t i = first; i <= last; ++i) {
    std::cout << ' ' << r(i);
  }
  return test_index_search_impl(r, x, first, last, expected);
}

int
main() {
  int failed = 0;
  failed += test_index_search(RegularCoords{0, 1, 0}, 0.1, 0, 10, 0) ? 0 : 1;
  failed += test_index_search(RegularCoords{0, 1, 0}, 0.9, 0, 10, 1) ? 0 : 1;
  failed += test_index_search(RegularCoords{0, 1, 0}, 1.2, 0, 10, 1) ? 0 : 1;
  failed += test_index_search(RegularCoords{0, 1, 0}, 8, 0, 10, 8) ? 0 : 1;
  failed += test_index_search(RegularCoords{0, 1, 0}, 5.8, 0, 10, 6) ? 0 : 1;    
  failed += test_index_search(RegularCoords{0, 1, 0}, 6.4, 0, 10, 6) ? 0 : 1;    
  failed += test_index_search(RegularCoords{0, 1, 0}, 4.8, 0, 10, 5) ? 0 : 1;    
  failed += test_index_search(RegularCoords{0, 1, 0}, 5.4, 0, 10, 5) ? 0 : 1;    
  failed += test_index_search(RegularCoords{0, 1, 0}, 9.9, 0, 10, 10) ? 0 : 1;
  failed += test_index_search(RegularCoords{0, 1, 0}, 10.1, 0, 10, 10) ? 0 : 1;
  {
    size_t nr = 10;
    GasShape shape{0.09, 2, 6, 1.45, false};
    failed += test_index_search(LogCoords{shape, nr, 0}, 2,    0, 20, 0) ? 0 : 1;
    failed += test_index_search(LogCoords{shape, nr, 0}, 2.2,  0, 20, 1) ? 0 : 1;
    failed += test_index_search(LogCoords{shape, nr, 0}, 2.24, 0, 20, 1) ? 0 : 1;
    failed += test_index_search(LogCoords{shape, nr, 0}, 2.3,  0, 20, 1) ? 0 : 1;
    failed += test_index_search(LogCoords{shape, nr, 0}, 2.4,  0, 20, 2) ? 0 : 1;
    failed += test_index_search(LogCoords{shape, nr, 0}, 2.5,  0, 20, 2) ? 0 : 1;
    failed += test_index_search(LogCoords{shape, nr, 0}, 2.5,  0, 20, 2) ? 0 : 1;
  }
  return failed > 0;
}
