// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_CODE_UNITS_H
#define FARGOCA_CODE_UNITS_H

#include <boost/property_tree/ptree.hpp>
#include <highfive/H5File.hpp>
#include <nlohmann/json_fwd.hpp>

#include "fundamental.hpp"
#include "precision.hpp"
#include "mathAddOns.hpp"

namespace fargOCA {
  using json = nlohmann::json;

  /// Conversion factors between Code Units and CGS.
  /// For convenience, the code uses units based on object of interest
  /// such as the central body and an orbiting body.
  /// This oject provide the conversion factors to CGS.
  class CodeUnits {
  public:
    /// \brief semi major axis of the earth in cm
    static constexpr real astronomicalUnit()  { return 1.495978707e+13;  }
    /// \brief out good old sun mass in g
    static constexpr real sunMass()           { return 1.9885e+33; }
    /// \brief out good old sun equatorial radius in cm
    static constexpr real sunRadius()         { return 6.96342e+10; }

    /// \brief Unit distance in Astronomical Units (AU)
    /// This is the semi major axis of the main orbiting body of reference in AU.
    /// Default is Jupiter 5.2
    real semiMajorAxis()          const { return mySemiMajorAxis; }
    /// \brief Mass of the central body in Sun mass
    real centralBodyMass()        const { return myCentralBodyMass; }
    /// \brief Radius of the central body in Sun radius
    real centralBodyRadius()      const { return myCentralBodyRadius; }
    /// \brief Surface temperature of the central body in K
    real centralBodyTemperature() const { return myCentralBodyTemperature; } 

    // Code Units in CGS
    /// \brief Distance unit in cm
    real distance() const { return semiMajorAxis()*astronomicalUnit(); }
    /// \brief Dolume unit in cm^3
    real volume()   const { return ipow<3>(distance()); }
    /// \brief Mass unit in gram
    real mass()     const { return centralBodyMass()*sunMass(); }
    /// \brief Time unit in s
    /// That's the orbiting period of a body located at the unit distance of the central body divided by 2PI
    real time()     const { return std::sqrt(volume()/GRAVC/mass()); }
    /// \brief Velocity unit in cgs
    real velocity()     const { return distance()/time(); }
    /// \brief Temperature unit in cgs
    real temperature() const { return ipow<2>(velocity())/RGAS; }
    /// \brief Density unit in cgs
    real density() const { return centralBodyMass()*sunMass()/volume(); }

    // Physical Constants (in code units ?)
    /// \brief Light sspeed in Code Units
    real lightSpeed() const { return CLIGHT/velocity(); }
    /// \brief Radiation constant in Code Units
    real radiationCst() const { return AR*ipow<4>(temperature())/(density()*ipow<2>(velocity())); }
    /// \brief Steffan Boltzmann constant in Code Units
    real StefanBoltzmannCst() const { return radiationCst()*lightSpeed()/4; }

    void dump(std::ostream& out, int tab = 0) const;
    void writeH5(HighFive::Group grp) const;
    
    /// \param sma the semimajor axe of the orbiting body expressed in Astronomical Unit.
    /// \param cbMass central body mass expressed in sun mass.
    /// \param cbRadius central body radius expressed in sun radius.
    CodeUnits(real sma, real cbMass, real cbRadius, real cbTemp);
    CodeUnits() : CodeUnits(5.2, 1, 1, 4370) {}
    CodeUnits(HighFive::Group grp);
    CodeUnits(json const&);

  private:
    real mySemiMajorAxis;
    real myCentralBodyMass;
    real myCentralBodyRadius;
    real myCentralBodyTemperature;
  };

  /// \brief Json export utilitie.
  /// This allows treating CodeUnit as a basic Json type.
  void to_json(json& cfg, CodeUnits const& cu);
  /// \brief Json export utilitie.
  void from_json(json const& cfg, CodeUnits& cu);
}

#endif
