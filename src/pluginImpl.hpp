// Copyright 2024, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_PLUGIN_IMPL_H
#define FARGO_PLUGIN_IMPL_H

#include <string>
#include <type_traits>

#include "plugin.hpp"
#include "pluginConfig.hpp"
#include "grid.hpp"
#include "disk.hpp"
#include "simulation.hpp"
#include "h5Labels.hpp"

namespace fargOCA {
  template<class ID, typename... Args>
  typename Plugin<ID, Args...>::EngineMap&
  Plugin<ID, Args...>::engines() {
    static EngineMap engines;
    return engines;
  }

  template<class ID, typename... Args>
  Plugin<ID, Args...> const&
  Plugin<ID, Args...>::get(std::string name) {
    try {
      return *engines().at(name);
    } catch( std::out_of_range const& e) {
      std::cerr << "Invalid handler for plugin '" << id() << "'. "
                << "must be one of ";
      for (std::string h : available()) { std::cerr << "'" << h << "' "; }
      std::cerr << '\n';
      throw;
    }
  }

  template<class ID, typename... Args>
  Plugin<ID, Args...> const&
  Plugin<ID, Args...>::get(std::string name, Config const& config) {
    using h5::labels::HANDLER;
    return get(config.value<std::string>(HANDLER).value_or(name));
  }
  

  template<class ID, typename... Args>
  Plugin<ID, Args...>::Plugin(std::string name, Evaluator fct, std::string desc,
                         std::set<std::string> properties,
                         ConfigCheckFct check)
    : myName(name),
      myEvaluator(fct),
      myAuthorizedProperties(properties),
      myConfigChecker(check),
      myDescription(desc) {
    using h5::labels::HANDLER;
    if (properties.contains(HANDLER)) {
      std::ostringstream msg;
      msg << "'" << HANDLER << "' is a reseverd property name and cannot "
          << "be used in plugin '" << name << "' of the '"
          << id() << "' available plugins.";
      throw std::invalid_argument(msg.str());
    }
    auto insertion = engines().insert({name,this});
    if (!insertion.second) {
      std::ostringstream msg;
      msg << "A Plugin with type " << name << " already exist.";
      throw std::invalid_argument(msg.str());
    }
  }

  template<class ID, typename... Args>
  Plugin<ID, Args...>::Plugin(std::string name, Evaluator fct, std::string desc,
                         std::set<std::string> properties)
    : Plugin(name, fct, desc, properties, propertyNameChecker(properties)) {}

  template<class ID, typename... Args>
  Plugin<ID, Args...>
  Plugin<ID, Args...>::obsolete(std::string name, Plugin const& fresh) {
    std::ostringstream desc;
    desc << "Plugin '" << id() << "' label '" << name
         << "' is obsolete. Please use '" << fresh.name() << "' instead.";
    return Plugin{name, fresh.evaluator(), desc.str(), fresh.authorizedProperties(), fresh.configChecker()};
  }
  
  template<class ID, typename... Args>
  Plugin<ID, Args...>::~Plugin() {
    engines().erase(myName);
  }

  template<class ID, typename... Args>
  bool
  Plugin<ID, Args...>::declared(std::string name) {
    return bool(available().count(name));
  }
  
  template<class ID, typename... Args>
  std::set<std::string>
  Plugin<ID, Args...>::available() {
    std::set<std::string> names;
    for(auto const& [name,fct] : engines()) {
      names.insert(name);
    }
    return names;
  }

  template<class ID, typename... Args>
  bool
  Plugin<ID, Args...>::check(PluginConfig const& pconfig, std::ostream& log) {
    int errors = 0;
    for(auto const& [name, config]: pconfig.namedConfigs()) {
      using h5::labels::HANDLER;
      auto handler {config.value<std::string>(HANDLER)};
      if (handler) {
        if (!declared(*handler)) {
          log << "In plugin '" << name << "', could not find "
              << "explicitly specified handler '" << *handler
              << "' in '" << id() << "' plugin set.";
          ++errors;
        } else {
          auto const& checker {get(*handler).configChecker()};
          std::ostringstream out;
          if (!checker(config, out)) {
            log << "Problem in plugin '" << name << "': " << out.str() << std::endl;
            ++errors;
          }
        }
      } else {
        if (!declared(name)) {
          log << "In '" << id() << "'plugin set, could not find "
              << "implicitly specified handler '" << name << "'.";
          ++errors;
        } else {
          auto const& checker {get(name).configChecker()};
          std::ostringstream out;
          if (!checker(config, out)) {
            log << "Problem in plugin '" << name << "': " << out.str() << std::endl;
            ++errors;
          }
        }
      }
    }
    return errors == 0;
  }

}
#endif 
