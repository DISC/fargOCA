// Copyright 2022, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <pybind11/pybind11.h>

#include "KokkosExp_InterOp.hpp"

#include "allmpi.hpp"
#include "scalarField.hpp"
#include "vectorField.hpp"
#include "fargOCApy.hpp"

namespace py = pybind11;

namespace fargOCA {
  namespace py11 {
    void exportScalarField(py::module_& m) {
      py::class_<ScalarField, 
                 std::shared_ptr<ScalarField>>(m, "ScalarField")
        .def_property_readonly("field",
                               [](ScalarField& v) {
                                 return Kokkos::Experimental::as_python_type(v.data());
                               },
                               "The Kokkos::View of the field.",
                               py::return_value_policy::reference_internal)
        .def_property_readonly("min",
                               [](ScalarField& v) {
                                 return v.min();
                               },
                               "The minimum value of the field.",
                               py::return_value_policy::copy)
        .def("__repr__",
             [](ScalarField const& d) {
               return "fargOCApy.ScalarField";
             });
    }
    void exportVectorField(py::module_& m) {
      py::class_<VectorField>(m, "VectorField")
        .def_property_readonly("radial",
                               [](VectorField& v) { return py::cast(v.radial()); },
                               "The radial component of the vector field.",
                               py::return_value_policy::reference_internal)
        .def_property_readonly("theta",
                               [](VectorField& v) { return py::cast(v.theta()); },
                               "The azymutal component of the vector field.",
                               py::return_value_policy::reference_internal)
        .def_property_readonly("phi",
                               [](VectorField& v) { return py::cast(v.phi()); },
                               "The elevation component of the vector field.",
                               py::return_value_policy::reference_internal)
        .def("__repr__",
             [](VectorField const& d) {
               return "fargOCApy.VectorField";
             });
      
    }
  }
}
