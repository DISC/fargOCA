#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import sys
import h5py as h5
import argparse

parser = argparse.ArgumentParser(description='''
Extract the grids values from a hdf5 file and write them into text files.
Example:
    {} grid.h5 radii.txt layers.txt sectors.txt [--path=<some path>]
'''.format(sys.argv[0]))
parser.add_argument('grid', metavar='grid.h5', type=str, nargs=1,
                    help='HDF5 grid description file.')
parser.add_argument('radii', metavar='radfile.dat', type=str, nargs=1,
                    help='The file wehre to put the radii.')
parser.add_argument('layers', metavar='radfile.dat', type=str, nargs=1,
                    help='The file wehre to put the layers.')
parser.add_argument('sectors', metavar='radfile.dat', type=str, nargs=1,
                    help='The file wehre to put the sectors.')
parser.add_argument('--path', metavar='PATH', type=str, default='grid', action='store',
                    help='The grid description path into the file.')

args = parser.parse_args()

grid = h5.File(args.grid[0], 'r')
path = args.path

radiitxt=open(args.radii[0], 'w')
for r in grid[path+'/radii']:
     print("{}".format(r), file=radiitxt)
layerstxt=open(args.layers[0], 'w')
for l in grid[path+'/layers']:
     print("{}".format(l), file=layerstxt)
sectorstxt=open(args.sectors[0], 'w')
for s in grid[path+'/sectors']:
     print("{}".format(s), file=sectorstxt)

