// Copyright 2025 Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include "gridSpacing.hpp"
#include "jsonio.hpp"
#include "simulation.hpp"
#include "disk.hpp"
#include "gridDispatch.hpp"

namespace fargOCA {
  namespace jsonio {
    template<>
    Enum2StrVec<GridSpacing>
    buildEnumMap<GridSpacing>() {
      return {
	{GridSpacing::ARITHMETIC, "ARITHMETIC"},
	{GridSpacing::LOGARITHMIC, "LOGARITHMIC"}
      };
    }
  }
  
  void to_json(nlohmann::json& j,         GridSpacing const& gs) { jsonio::to_json  (j, gs); }
  void from_json(nlohmann::json const& j, GridSpacing& gs)       { jsonio::from_json(j, gs); }
  
  template<> std::vector<GridSpacing> const& enumValues<GridSpacing>() {
    static std::vector<GridSpacing> values = { GridSpacing::ARITHMETIC, GridSpacing::LOGARITHMIC };
    return values;
  }

  GridSpacing radialSpacing(Grid const& d)         { return d.radialSpacing(); }
  GridSpacing radialSpacing(GridDispatch const& d) { return d.grid().radialSpacing(); }
  GridSpacing radialSpacing(Disk const& d)         { return d.dispatch().grid().radialSpacing(); }
  GridSpacing radialSpacing(Simulation const& s)   { return s.disk().dispatch().grid().radialSpacing(); }
}
