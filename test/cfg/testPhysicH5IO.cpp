// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include <highfive/H5File.hpp>
#include "allmpi.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "io.hpp"
#include "optIO.hpp"
#include "diskPhysic.hpp"
#include "planetarySystemPhysic.hpp"

using namespace fargOCA;
namespace HF = HighFive;

struct TestTrace {
  TestTrace(std::string name) {
    std::cerr << "Testing " << name << std::flush;
  }
  ~TestTrace() {
    std::cerr << "\n";
  }
};

std::string 
status(bool pass) {
  return pass ? std::string("PASSED") : std::string("FAILED");
}

template<class S, typename T>
bool
comp(std::string name, T S::*mbr, S const& s1, S const& s2, T tolerance ) {
  if (std::abs(s1.*mbr - s2.*mbr) > tolerance) {
    std::cerr << "Test failed on " << name << ": " << s1.*mbr << " vs " << s2.*mbr << '\n';
    return false;
  } else {
    return true;
  }
}

template<class S, typename T>
bool
comp(std::string name, T S::*mbr, S const& s1, S const& s2) {
  if (s1.*mbr != s2.*mbr) {
    std::cerr << "Test failed on " << name << ": " << s1.*mbr << " vs " << s2.*mbr << '\n';
    return false;
  } else {
    return true;
  }
}

template<class S, typename T>
bool
comp(std::string name, T (S::*mbr)() const, S const& s1, S const& s2, T tolerance ) {
  if (std::abs((s1.*mbr)() - (s2.*mbr)()) > tolerance) {
    std::cerr << "Test failed on " << name << ": " << (s1.*mbr)() << " vs " << (s2.*mbr)() << '\n';
    return false;
  } else {
    return true;
  }
}

template<class S, typename T>
bool
comp(std::string name, std::optional<T> S::*mbr, S const& s1, S const& s2, T tol) {
  if (bool(s1.*mbr) != bool(s2.*mbr)) {
    std::cerr << "failed " << typeid(T).name() << ": " << bool(s1.*mbr) << " vs " << bool(s2.*mbr) << '\n';
    return false;
  } else {
    if (!bool(s1.*mbr)) { 
      return true;
    } else {
      if (std::abs(*(s1.*mbr) - *(s2.*mbr)) > tol) {
        std::cerr << "Test failed on " << name << ": " << *(s1.*mbr) << " vs " << *(s2.*mbr) << '\n';
        return false; 
      } else {
        return true;
      }
    }
  }
}

typedef DiskPhysic::Adiabatic::Radiative::Star Star;

bool
comp(Star const& s1, Star const& s2) {
  int failed = 0;
  if (!comp("shadowAngle", &Star::shadowAngle, s1, s2)) {++failed;}
  return failed == 0;
}

typedef PlanetarySystemPhysic::Planet::Luminosity Luminosity;

bool
comp(Luminosity const& l1, Luminosity const& l2) {
  int failed = 0;
  if (!comp("solidAccretion", &Luminosity::userSolidAccretion, l1, l2)) {++failed;}
  return failed == 0;
}

typedef DiskPhysic::Referential Ref;

bool 
comp(Ref const& ref1, Ref const& ref2) {
  return (comp("Referential.type", &Ref::type, ref1, ref2)
          && comp("Referential.omega", &Ref::omega, ref1, ref2, 1e-20)
          && comp("Referential.indirectForces", &Ref::indirectForces, ref1, ref2));
}

typedef PlanetarySystemPhysic::Planet::MassTaper Taper;

bool 
comp(std::variant<real,Taper> const& m1, std::variant<real,Taper> const& m2) {
  if (std::holds_alternative<real>(m1) != std::holds_alternative<real>(m2)) {
    std::cerr << "m1 and m2 do not hold the same type.\n";
    return false;
  }
  if (std::holds_alternative<real>(m1)) {
    real mass1 = std::get<real>(m1);
    real mass2 = std::get<real>(m2);
    if (mass1 != mass2) {
      std::cerr << "  MassTaper<real> m1: " << mass1
		<< ", MassTaper<real> m2: " << mass2 << '\n';
      return false;
    } else {
      return true;
    }
  } else if (std::holds_alternative<Taper>(m1)) {
    Taper t1 = std::get<Taper>(m1);
    Taper t2 = std::get<Taper>(m2);
    return (comp("MassTaper.initialMass", &Taper::initialMass, t1, t2)
            && comp("MassTaper.beginTime", &Taper::beginTime, t1, t2)
            && comp("MassTaper.finalMass", &Taper::finalMass, t1, t2)
            && comp("MassTaper.endTime", &Taper::endTime, t1, t2));
  } else {
    return false;
  }
}

bool 
comp(OrbitalElements const& o1, OrbitalElements const& o2) {
  return (comp("OrbitalElement.semiMajorAxis",            &OrbitalElements::semiMajorAxis, o1, o2, 1e-20)
          && comp("OrbitalElement.eccentricity",          &OrbitalElements::eccentricity, o1, o2, 1e-20)
          && comp("OrbitalElement.inclination",           &OrbitalElements::inclination, o1, o2, 1e-20)
          && comp("OrbitalElement.longitudeOfPericenter", &OrbitalElements::longitudeOfPericenter, o1, o2, 1e-20)
          && comp("OrbitalElement.longitudeOfNode",       &OrbitalElements::longitudeOfNode, o1, o2, 1e-20)
          && comp("OrbitalElement.meanLongitude",         &OrbitalElements::meanLongitude, o1, o2, 1e-20));
}

bool
comp(PlanetarySystemPhysic::Planet const& p1, PlanetarySystemPhysic::Planet const& p2) {
  std::cout << "\nTesting planet";
  int failed = 0;
  if (!comp("name", &PlanetarySystemPhysic::Planet::name, p1, p2)) {++failed;}
  if (!comp(p1.mass, p2.mass)) {++failed;}
  if (!comp(p1.orbitalElements, p2.orbitalElements)) {++failed;}
  if (!comp("accretionTime", &PlanetarySystemPhysic::Planet::accretionTime, p1, p2)) {++failed;}
  if (!comp("feelDisk", &PlanetarySystemPhysic::Planet::feelDisk, p1, p2)) {++failed;}
  if (!comp("feelOthers", &PlanetarySystemPhysic::Planet::feelOthers, p1, p2)) {++failed;}
  if (bool(p1.luminosity) != bool(p2.luminosity)) {
    ++failed;
    std::cerr << "Failed on Luminosity " << bool(p1.luminosity) << " vs " << bool(p2.luminosity) << '\n';
  } else {
    if (bool(p1.luminosity)) {
      if (!comp(*p1.luminosity, *p2.luminosity)) {++failed;}
    }
  }
  if (failed > 0) {
    std::cerr << "\nFailed on Planet " <<  p1.name << '\n';
    p1.dump(std::cerr, 0);
    std::cerr << " vs:" << p2.name << '\n';
    p2.dump(std::cerr, 0);
  }
  return failed == 0;
}

bool
comp(PlanetarySystemPhysic const& s1, PlanetarySystemPhysic const& s2) {
  int failed = 0;
  typedef PlanetarySystemPhysic H;
  if (!comp("exclude_hill", &H::excludeHill, s1, s2)) {++failed;}
  if (!comp("accrete_onto_planets", &H::accreteOntoPlanets, s1, s2)) {++failed;}
  
  if (s1.planets.size() != s2.planets.size()) {
    std::cerr << "Test failed on " << "PlanetarySystemPhysic::planets" << ": " << s1.planets.size() << " vs " << s1.planets.size() << '\n';
    ++failed;
  } else {
    for(auto const& pp : s1.planets) {
      PlanetarySystemPhysic::Planet p1 = pp.second;
      if (!comp(p1, s2.planets.find(p1.name)->second)) {++failed;}
    }
  }
  return failed == 0;
}

bool
comp(std::string label, Config const& p1, Config const& p2) {
  auto const& m1 = p1.registered();
  auto const& m2 = p2.registered();
  int failed = 0;
  if (m1.size() != m2.size()) {
    std::cerr << "Config have different sizes\n";
    ++failed;
  } else {
    for( auto const& [k,v] : m1) {
      try {
	if (m2.at(k) != v) {
	  std::cerr << "different values for key '"<< k << "' in " << label << '\n';
	  ++failed;
	}
      } catch (std::out_of_range const& err) {
	std::cerr << "key '"<< k << "' not found in " << label << '\n';
	++failed;
      }
    }
  }
  return failed == 0;
}

typedef DiskPhysic::Adiabatic::Radiative::Opacity Opacity;

bool
comp(std::string label, Opacity const& f1, Opacity const& f2) {
  int failed = 0;
  if (!comp("type", &Opacity::type, f1, f2)) {++failed;}
  if (!comp("config", f1.config, f2.config)) {++failed;}
  return failed == 0;
}

typedef DiskPhysic::Adiabatic::Radiative::Solver Solver;

bool
comp(std::string label, Solver const& f1, Solver const& f2) {
  int failed = 0;
  if (!comp("label", &Solver::label, f1, f2)) {++failed;}
  if (!comp("config", f1.config, f2.config)) {++failed;}
  return failed == 0;
}
  
typedef DiskPhysic::Adiabatic::Radiative Radiative;

bool
comp(Radiative const& f1, Radiative const& f2) {
  int failed = 0;
  if (!comp("userZBoundaryTemperature", &Radiative::userZBoundaryTemperature, f1, f2)) {++failed;}
  if (!comp("opacity", f1.opacity, f2.opacity)) {++failed;}
  if (!comp("dustToGas", &Radiative::dustToGas, f1, f2)) {++failed;}
  if (!comp("solver", f1.solver, f2.solver)) {++failed;}

  if (bool(f1.star) != bool(f2.star)) {
    ++failed;
    std::cerr << "Failed on star " << bool(f1.star) << " vs " << bool(f2.star) << '\n';
  } else {
    if (bool(f1.star)) {
      if (!comp(*f1.star, *f2.star)) {++failed;}
    }
  }
  return failed == 0;
}

typedef DiskPhysic::Adiabatic::Cooling Cooling;
typedef DiskPhysic::Adiabatic::BetaCooling BetaCooling;

bool
comp(Cooling const& c1,Cooling const& c2) {
  int failed = 0;
  typedef DiskPhysic::Adiabatic::BetaCooling      Beta;
  typedef DiskPhysic::Adiabatic::RadiativeCooling Radiative;
  try {
    if (std::holds_alternative<Beta>(c1)) {
      if (!comp("timeScale", &Beta::timeScale, std::get<Beta>(c1), std::get<Beta>(c2))) {++failed;}
    } else {
      if (std::get_if<Radiative>(&c1)  == nullptr || std::get_if<Radiative>(&c2)  == nullptr) {
	std::cerr << "Bad Cooling type.\n";
	++failed;
      }
    }
  } catch(std::bad_variant_access const& e) {
    std::cerr << "Got " << e.what() << '\n';
    ++failed;
  }
  return failed == 0;
};

typedef DiskPhysic::Adiabatic Adiabatic;

bool
comp(Adiabatic const& a1, Adiabatic const& a2) {
  int failed = 0;
  if (!comp("index", &Adiabatic::index, a1, a2)) {++failed;}
  if ((bool(a1.radiative) != bool(a2.radiative))) {
    ++failed;
    std::cerr << "Failed on fullStar " << bool(a1.radiative) << " vs " << bool(a2.radiative) << '\n';
  }
  if ((bool(a1.cooling) != bool(a2.cooling))) {
    ++failed;
    std::cerr << "Failed on fullStar " << bool(a1.cooling) << " vs " << bool(a2.cooling) << '\n';
  }
  return failed == 0;
}

typedef DiskPhysic::Density::Cavity Cavity;

bool
comp(Cavity const& cavity1, Cavity const& cavity2) {
  return (comp("Cavity.radius",&Cavity::radius, cavity1, cavity2, 1e-20)
          && comp("Cavity::ratio", &Cavity::ratio, cavity1, cavity2, 1e-20)
          && comp("Cavity::width", &Cavity::width, cavity1, cavity2, 1e-20));
}

bool
testCavity() {
  TestTrace trace{"cavity"};
  Cavity cavity1{0.1, 1.2, 1.3};
  {
    HF::File  file{"cavityShape.h5", HF::File::Truncate};
    auto g = file.getGroup("/");
    cavity1.writeH5(g);
  }
  HF::File  file("cavityShape.h5", HF::File::ReadOnly);
  real nan = std::numeric_limits<real>::quiet_NaN();
  Cavity cavity2{nan, nan, nan};
  cavity2.readH5(file.getGroup("/"));
  return comp(cavity1, cavity2);
}

typedef DiskPhysic::Density Density;
bool
comp(Density const& d1, Density const& d2) {
  int failed = 0;
  if (!comp("slope", &Density::slope, d1, d2)) {++failed;}
  if (!comp("start", &Density::start, d1, d2)) {++failed;}
  if (!comp("minimum", &Density::minimum, d1, d2)) {++failed;}
  if (!comp(d1.cavity, d2.cavity)) {++failed;}
  return failed == 0;
};

typedef DiskPhysic::Smoothing Smoothing;
bool
comp(Smoothing const& s1, Smoothing const& s2) {
  int failed = 0;
  if (!comp("change", &Smoothing::change, s1, s2)) {++failed;}
  if (!comp("taper", &Smoothing::taper, s1, s2)) {++failed;}
  if (!comp("size", &Smoothing::size, s1, s2)) {++failed;}
  return failed == 0;
}

bool
comp(GasShape const& gas1, GasShape const& gas2) {
  bool radius = true;
  
  if (std::abs(gas1.radius.min - gas2.radius.min) > 1e-20
      && std::abs(gas1.radius.max - gas2.radius.max) > 1e-20) {
    radius = false;
    std::cerr << "gas.radius failed\n";
  }
  return (radius
          && comp("Gas.aspectRatio", &GasShape::aspectRatio, gas1, gas2, 1e-20)
          && comp("Gas.opening", &GasShape::opening, gas1, gas2, 1e-20)
          && comp("Gas.half", &GasShape::half, gas1, gas2));
}

bool
testGas() {
  TestTrace trace("gas");
  GasShape gas1(1, 42, 56, 0.01, true );
  {
    HF::File  file("gasShape.h5", HF::File::Overwrite);
    auto g = file.getGroup("/");
    gas1.writeH5(g);
  }
  HF::File  file("gasShape.h5", HF::File::ReadOnly);
  real nan = std::numeric_limits<real>::quiet_NaN();
  GasShape gas2(nan, nan, nan, nan, false );
  gas2.readH5(file.getGroup("/"));
  return comp(gas1, gas2);
}

typedef DiskPhysic::Viscosity Viscosity; 

bool 
comp(Viscosity const& b1, Viscosity const& b2) {
  int failed = 0;
  if (!comp("type", &Viscosity::type, b1, b2)) {++failed;}
  if (!comp("artificial", &Viscosity::artificial, b1, b2)) {++failed;}
  return failed == 0;
}

bool 
comp(DiskPhysic::StarAccretion const& b1, DiskPhysic::StarAccretion const& b2) {
  int failed = 0;
  if (!comp("type", &DiskPhysic::StarAccretion::type, b1, b2)) {++failed;}
  if (!comp("userRate", &DiskPhysic::StarAccretion::userRate, b1, b2)) {++failed;}
  if (bool(b1.wind) != bool(b2.wind)) {
    std::cerr << "wind failed" << bool(b1.wind) << " vs " << bool(b2.wind) << '\n';
    ++failed;
  } else {
    if (bool(b1.wind)) {
      DiskPhysic::StarAccretion::Wind w1 = *b1.wind;
      DiskPhysic::StarAccretion::Wind w2 = *b2.wind;
      if (!comp("userActiveDensity", &DiskPhysic::StarAccretion::Wind::userActiveDensity, w1, w2)) {++failed;}
      if (!comp("filter", &DiskPhysic::StarAccretion::Wind::filter, w1, w2)) {++failed;}
    }
  }
  
  return failed == 0;
}

template<class C>
bool
comp(std::optional<C> const& p1, std::optional<C> const& p2) {
  if (bool(p1) != bool(p2)) {
    std::cerr << "failed " << typeid(C).name() << ": " << bool(p1) << " vs " << bool(p2) << '\n';
    return false;
  } else {
    return (!bool(p1) || comp(*p1, *p2));
  }
}

bool 
comp(DiskPhysic const& p1,DiskPhysic const& p2) {
  int failed = 0;
  if (!comp(p1.referential, p2.referential)) {++failed;}
  if (!comp(p1.shape, p2.shape)) {++failed;}
  if (!comp(p1.starAccretion, p2.starAccretion)) {++failed;}
  if (!comp(p1.viscosity, p2.viscosity)) {++failed;}
  if (!comp(p1.adiabatic, p2.adiabatic)) {++failed;}
  if (!comp(p1.smoothing, p2.smoothing)) {++failed;}
  
  if (!comp("flaringIndex", &DiskPhysic::flaringIndex, p1, p2))     {++failed;}
  if (!comp("CFLSecurity", &DiskPhysic::CFLSecurity, p1, p2))       {++failed;}
  if (!comp("draggingCoef", &DiskPhysic::draggingCoef, p1, p2))     {++failed;}
  if (!comp("hillCutFactor", &DiskPhysic::hillCutFactor, p1, p2))   {++failed;}
  if (!comp("transport", &DiskPhysic::transport, p1, p2))           {++failed;}

  return failed == 0;
}

template<class C>
bool
testComp(std::string name) {
  C c1;
  TestTrace trace(name);
  std::string fname = name + ".h5";
  {
    HF::File  file(fname, HF::File::Overwrite);
    auto g = file.getGroup("/");
    c1.writeH5(g);
  }
  HF::File  file(fname, HF::File::ReadOnly);
  C c2;
  c2.readH5(file.getGroup("/"));
  return comp(c1, c2);
}

bool
testStarAccretion(bool wind = false) {
  std::string name = "staraccretion";
  TestTrace trace(name);
  std::string fname = name + ".h5";
  
  DiskPhysic::StarAccretion b1(StarAccretion::CONSTANT,
			       0.42,
			       wind
			       ? std::make_optional<DiskPhysic::StarAccretion::Wind>(1.42, 2.42)
			       : std::optional<DiskPhysic::StarAccretion::Wind>());
  {
    HF::File  file(fname, HF::File::Overwrite);
    auto g = file.getGroup("/");
    b1.writeH5(g);
  }
  HF::File  file(fname, HF::File::ReadOnly);
  DiskPhysic::StarAccretion b2(file.getGroup("/"));
  return comp(b1, b2);
}

bool
testViscosity() {
  Viscosity b1;
  std::string name = "viscosity";
  TestTrace trace(name);
  std::string fname = name + ".h5";
  {
    b1 = {
      true,
      std::string("constant"),
      Config({std::make_pair<std::string>( "value", "1.42")})
    };
    HF::File  file(fname, HF::File::Overwrite);
    auto g = file.getGroup("/");
    b1.writeH5(g);
  }
  HF::File  file(fname, HF::File::ReadOnly);
  Viscosity b2;
  b2.readH5(file.getGroup("/"));
  return comp(b1, b2);
}

bool
testPlanetarySystemPhysic() {
  PlanetarySystemPhysic::Planet p = {"planetgirl", 
                                     real(55),
                                     { real(1), real(0), real(0), real(0), real(0), real(0) },
                                     real(10),
                                     true, false};
  std::map<std::string, PlanetarySystemPhysic::Planet> planets;
  planets.insert(std::make_pair(p.name, p));
  PlanetarySystemPhysic b1(std::move(planets),
                           true, false, {"Runge_Kutta", Config()} );
  std::string name = "planetary_system_physic";
  TestTrace trace(name);
  std::string fname = name + ".h5";
  {
    HF::File  file(fname, HF::File::Overwrite);
    auto g = file.getGroup("/");
    b1.writeH5(g);
  }
  HF::File  file(fname, HF::File::ReadOnly);
  PlanetarySystemPhysic b2(file.getGroup("/"));
  return comp(b1, b2);
}

bool
testDiskPhysic(bool dust, bool wind, bool adiabatic, bool full, bool star, bool cooling) {
  std::cout
    << "dust: "      << yesno(dust)      << ", "
    << "wind: "      << yesno(wind)      << ", "
    << "adiabatic: " << yesno(adiabatic) << ", "
    << "full: "      << yesno(full)      << ", "
    << "star: "      << yesno(star)      << ", "
    << "cooling: "   << yesno(cooling)   << '\n';
  
  auto keep1 = DiskPhysic::make(CodeUnits(),
                                real(0),
                                GasShape(),
                                DiskPhysic::Referential::constant(real(0), false),
                                {},
                                {},
                                {},
                                std::nullopt,
                                { true, "constant",
				  Config({std::make_pair<std::string>("value", "0")})},
                                std::nullopt,
                                {0},
                                {false, false, real(1e-7), real(0.5)},
                                std::nullopt,
                                real(0.5),
                                real(1),
                                real(0.6),
                                Transport::FAST,
                                real(2.3));
  DiskPhysic *p1 = const_cast<DiskPhysic*>(keep1.get());
  std::string name = "physic";
  TestTrace trace(name);
  std::string fname = name + ".h5";
  {
    GasShape    gas;
    p1->shape = { gas };
    p1->starAccretion = DiskPhysic::StarAccretion(StarAccretion::CONSTANT, 0.42, std::nullopt);
    if (wind) {
      p1->starAccretion->wind = DiskPhysic::StarAccretion::Wind(1.42, 2.42);
    }
    
    p1->viscosity = { true, "constant", Config({std::make_pair<std::string>("value", "1.42")})};
    
    if (adiabatic) {
      p1->adiabatic = Adiabatic();
      p1->adiabatic->index = 0.3;
      if (full) {
	Opacity opacity{OpacityLaw::CONSTANT, Config::Properties{{"kappa","12.42"}}};
	Solver  solver{std::string{"legacy"}, Config{}};
        p1->adiabatic->radiative = Radiative(opacity,
					     solver,
					     10.42, // userzTemp
					     13.42, // dust to gas
                                             std::nullopt,
                                             std::nullopt);
        
        if (star) {
          p1->adiabatic->radiative->star = 
            Star(26.42);  //  shadowAngle;
        }
      }
      if (cooling) {
        p1->adiabatic->cooling = BetaCooling({34.42});
      }
    }
    DiskPhysic::Density::Cavity cavity{ 0, 1, 1};
    p1->density = {0.1,0.2,0.3, cavity };
    p1->smoothing = { true, false, 9.0, 8.0};
    p1->flaringIndex   = 0.4;
    p1->CFLSecurity    = 0.5;
    p1->draggingCoef   = 0.6;
    p1->hillCutFactor  = 0.7;
    
    HF::File  file(fname, HF::File::Overwrite);
    p1->writeH5(file.getGroup("/"));
  }
  HF::File  file(fname, HF::File::ReadWrite);
  auto keep2 = DiskPhysic::make(CodeUnits(), file.getGroup("/"));
  DiskPhysic *p2 = const_cast<DiskPhysic*>(keep2.get());
  return comp(*p1, *p2);
}

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  int failed = 0;
  
  failed += int(!testGas());
  failed += int(!testCavity());
  failed += int(!testStarAccretion());
  failed += int(!testStarAccretion(true));
  failed += int(!testViscosity());
  failed += int(!testPlanetarySystemPhysic());
  failed += int(!testDiskPhysic(false, false, false, false, false, false));
  failed += int(!testDiskPhysic(true, false, false, false, false, false));
  failed += int(!testDiskPhysic(true, true, false, false, false, false));
  failed += int(!testDiskPhysic(true, true, true, false, false, false));
  failed += int(!testDiskPhysic(true, true, true, true, false, false));
  failed += int(!testDiskPhysic(true, true, true, true, true, false));
  failed += int(!testDiskPhysic(true, true, true, true, true, true));
  return failed;
}
