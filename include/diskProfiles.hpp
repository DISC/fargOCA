// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.


#ifndef FARGO_DISK_PROFILES_H
#define FARGO_DISK_PROFILES_H

#include <iosfwd>
#include <optional>

#include <highfive/H5File.hpp>

#include "precision.hpp"
#include "shared.hpp"
#include "arrays.hpp"

namespace fargOCA {
  class DiskPhysic;
  class GridDispatch;

  /// \brief Original disk profiles.
  ///
  /// Needed for some boundary conditions and when coolong stuff down.
  class DiskProfiles {
  public:
    class VectorField {
    public:
      VectorField(DiskProfiles const& profile) : myOwner(profile) {}
      ~VectorField() = default;
      arr2d<real const> radial() const { return myOwner.radialVelocity(); }
      arr2d<real const> phi()    const { return myOwner.phiVelocity(); }
      arr2d<real>       theta(real frameVelocity)  const { return myOwner.thetaVelocity(frameVelocity); }
      arr2d<real const> theta()                    const { return myOwner.thetaVelocity(); }
      
    private:
      friend class DiskProfiles;
      DiskProfiles const& myOwner;
    };
    DiskProfiles(GridDispatch const& grid,
                 arr2d<real const> radialVelocity,
                 arr2d<real const> phiVelocity,
                 arr2d<real const> thetaVelocity,
                 arr2d<real const> density,
                 std::optional<arr2d<real const>> energy);
    /// \field Initialize from a group
    DiskProfiles(GridDispatch const& grid, HighFive::Group const& group);
    DiskProfiles(DiskProfiles const& profiles);
    /// \field Copy with rebind
    DiskProfiles(GridDispatch const& grid, DiskProfiles const& profiles);
    ~DiskProfiles();
    
    GridDispatch const& dispatch() const { return *myDispatch; }
    VectorField  velocity()    const { return VectorField(*this); }
    std::optional<arr2d<real const>> energy() const { return myEnergy; }
    arr2d<real const> density()  const { return myDensity; }

    arr2d<real>       thetaVelocity(real frameVelocity) const;
    arr2d<real const> thetaVelocity()  const { return myThetaVelocity; }
    arr2d<real const> phiVelocity()    const { return myPhiVelocity; }
    arr2d<real const> radialVelocity() const { return myRadialVelocity; }
  
    void writeH5(std::optional<HighFive::Group> group, int root) const;
    void readH5(HighFive::Group const& group);

  private:
    shptr<GridDispatch const> myDispatch;
    arr2d<real> myRadialVelocity;
    arr2d<real> myPhiVelocity;
    arr2d<real> myThetaVelocity;
    
    arr2d<real> myDensity;
    std::optional<arr2d<real>> myEnergy;
  };
}

#endif 
