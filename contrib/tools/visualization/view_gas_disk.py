#!/usr/bin/env python3
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

import sys
import h5py as h5
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import argparse

parser = argparse.ArgumentParser(description='''
Display all the fields of a gas disk, possibly averaging along sectors for 3D.
Example:
   {} ref/gas9.h5
'''.format(sys.argv[0]))
parser.add_argument('ifile', metavar='GAS.h5', type=str, nargs=1,
                    help='HDF5 gas description file.')
parser.add_argument('paths', metavar='PATH', type=str, nargs='*',
                    help='The explicit list of fields to display. Defaults: all.')
parser.add_argument('-p,--profiles', dest='show_profiles', action='store_true',
                    help='The explicit list of fields to display. Defaults: all.')

args = parser.parse_args()
disk_filename = args.ifile[0]
disk = h5.File(disk_filename, 'r')


data_space_name = None

if args.show_profiles:
    data_space_name = 'profile'
else:
    data_space_name = 'polar_grid'

def extract_field_paths(obj, data_space):
    """Extract all object paths storing a field from an hdf5 object.

    An object path is considered to store a field if it contains a
    object named data_space object which provide a 2/3D floating point array.
    """
    
    fields = []
    def collect_fields(path):
        try:
            if data_space in obj[path]: fields.append(path)
        except TypeError:
            pass
    obj.visit(collect_fields)
    return fields

# Collect paths
paths = []

if len(args.paths) > 0:
    paths = args.paths
else:
    paths = extract_field_paths(disk, data_space_name)

if len(paths)==0:
    print("No field found in {}.".format(disk_filename))
else:
    print(paths)

# Display grid:
nb_col = 3
nb_line = (len(paths)+nb_col)/nb_col

def get_fields(disk, paths, data_space_name):
    fields = {}
    for p in paths:
        fields[p] = np.array(disk[p][data_space_name])
    return fields

fields = get_fields(disk, paths, data_space_name)
                        

# Assume all shapes are equal
shape = disk[paths[0]][data_space_name].shape

xymesh = None

flat   = shape[1] == 1

if args.show_profiles:
    if flat:
        radii   = np.array(disk['grid']['radii'])
        sectors = [ 0 ]
        xymesh = np.meshgrid(sectors, radii)
        # drop layer dim
        for p,f in fields.items():
            fields[p] = f
    else:
        print("this is a fat grid")
        radii   = np.array(disk['grid']['radii'])
        layers  = np.array(disk['grid']['layers'])
        xymesh  = np.meshgrid(layers, radii)
        # flatten along sectors
        for p,f in fields.items():
            fields[p] = f
else:
    if flat:
        print("this is a flat grid, displaying as it is.")
        radii   = np.array(disk['grid']['radii'])
        sectors = np.array(disk['grid']['sectors'])
        xymesh = np.meshgrid(sectors, radii)
        # drop layer dim
        for p,f in fields.items():
            fields[p] = f[:,0,:]
    else:
        print("this is a fat grid, merging sectors before displaying.")
        radii   = np.array(disk['grid']['radii'])
        layers  = np.array(disk['grid']['layers'])
        xymesh  = np.meshgrid(layers, radii)
        # flatten along sectors
        for p,f in fields.items():
            fields[p] = np.mean(fields[p], 2)

def plot(f, xymesh, pos, name):
    plt.subplot(nb_col, nb_line,pos)
    plt.title(name)
    x,y = xymesh
    plt.pcolormesh(y,x,f,
                   **{ True:  {'cmap' : 'RdBu',    'vmin' : - np.abs(f.max()), 'vmax' : + np.abs(f.max())},
                       False: {'cmap' : 'viridis', 'vmin' : f.min(),           'vmax' : f.max()}
                   }[ "velocity" in name ])
    plt.axis([y.min(), y.max(), x.min(), x.max()])
    plt.colorbar()

for i,(p,f) in enumerate(fields.items()):
    plot(f, xymesh, i+1, p)

plt.show()
