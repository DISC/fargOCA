// Copyright 2018, Elena Lega, elena.lega<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include <highfive/H5File.hpp>

#include "environment.hpp"
#include "allmpi.hpp"
#include "log.hpp"
#include "codeUnits.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "loops.hpp"
#include "hillForce.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace kk = Kokkos;
namespace HF = HighFive;

using std::optional;

template<GridSpacing GS>
void
write_surf_dens_impl(Disk const& gas, std::ostream& out)
{
  GridDispatch const& dispatch = gas.dispatch();
  auto const& coords = dispatch.grid().template as<GS>();
  GridSizes const ls = coords.sizes();

  range_t const managedRadii = dispatch.managedRadii();
  auto radMed = coords.radiiMed();
  auto phi = coords.phi();
  
  arr1d<real>        surfdens("surface_density", ls.nr);
  arr3d<real const>  dens = gas.density().data();

  CodeUnits const& units = gas.physic().units;
  real const densityCGS  = units.density();
  real const distanceCGS = units.distance();
  
  switch (dispatch.grid().dim()) {
  case 2: // Simple azimuthal average for 2D disk
    kk::parallel_for("surface density 2D", kk::TeamPolicy<>(size(managedRadii), kk::AUTO),
                     KOKKOS_LAMBDA(kk::TeamPolicy<>::member_type const& team) {
                       size_t const i = team.league_rank()+begin(managedRadii);
                       real rdensity = 0;
                       kk::parallel_reduce(kk::TeamThreadRange(team, ls.ns),
                                           [&](size_t const j, real& partial) {
                                             partial += dens(i,0,j)*densityCGS*distanceCGS/ls.ns;
                                           }, rdensity);
                       kk::single(kk::PerTeam(team), [&]() { surfdens(i) += rdensity; });
                     });
    break;
  case 3: // 3D: Here we compute the vertically integrated density field
    kk::parallel_for("surface density 2D", kk::TeamPolicy<>(size(managedRadii), kk::AUTO),
                     KOKKOS_LAMBDA(kk::TeamPolicy<>::member_type const& team) {
                       size_t const i = team.league_rank()+begin(managedRadii);
                       real density = 0;
                       kk::parallel_reduce(kk::TeamThreadRange(team, ls.ni-2),
                                           [&](size_t const hm, real& ipartial) {
                                             size_t const h = hm+1;
                                             real average = 0;
                                             real const deltaphi = distanceCGS*radMed(i)*(phi(h+1)-phi(h));
                                             kk::parallel_reduce(kk::ThreadVectorRange(team, ls.ns),
                                                                 [=](size_t const j, real& jpartial) {
                                                                   jpartial += dens(i,h,j)*densityCGS;
                                                                 }, average);
                                             ipartial += average*deltaphi/ls.ns; 
                                           }, density);
                       kk::single(kk::PerTeam(team), [&]() { surfdens(i) += density; });
                     });
    break;
  default:
    std::abort();
  }
  auto gsurfdens = kk::create_mirror_view_and_copy(kk::HostSpace{}, dispatch.joinManaged(surfdens));
  if (dispatch.master()) {
    out << std::scientific;
    auto const& gcoords = dispatch.grid().global().template as<GS>();
    size_t const gnr = gcoords.sizes().nr;
    auto gRadMed = gcoords.radiiMed();
    for (size_t i = 0; i < gnr; ++i) {
      if (gas.physic().shape.half){ // Multiply by 2 in case of half disk
	gsurfdens(i) *= 2;
      }
      out << std::setprecision(8) << gRadMed(i) << '\t'
          << std::setprecision(16) << gsurfdens(i) << '\n';
    }
  }
}

void
write_surf_dens(Disk const& gas, std::ostream& out) {
  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;
  switch(gas.dispatch().grid().radialSpacing()) {
  case ARTH: write_surf_dens_impl<ARTH>(gas, out); break;
  case LOGR: write_surf_dens_impl<LOGR>(gas, out); break;
  }
}

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract radial surface density \n"
        << '\t' << cmd << " --disk <idisk>.h5 :\n"
        << "col 1 radius rmed [code units] \n"
        << "col 2 surface density [g/cm^2] \n"
        << "vert. average not perpendicular to midlplane, \n"
        << "but taken along curve of constant R. \n";
  
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("output,o", po::value<std::string>(), "Output file, print on stdout by default.");
  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk" }) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(1);
    }
  }

  std::string ifname = vm["disk"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadOnly);
  auto disk = Disk::make(env.world(), ifile.getGroup("/"));
  
  std::ofstream ofile;
  std::ostream& out = (bool(vm.count("output")) 
                       ? (ofile.open(vm["output"].as<std::string>()), ofile) 
                       : std::cout);

  write_surf_dens(*disk, out);
  return 0;
}
