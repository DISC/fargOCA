#!/usr/bin/env bash

# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

# This one is on a 365x66x10 grid
shapein=@CMAKE_CURRENT_SOURCE_DIR@/refdisk.h5

cd @CMAKE_CURRENT_BINARY_DIR@

nproc=$1

rm -rf oprof
mkdir oprof
@CMAKE_BINARY_DIR@/fargoInit --verbose oprof small.info || exit -1
@MPILAUNCH@ -n $nproc @CMAKE_CURRENT_BINARY_DIR@/radiation_step oprof/disk0.h5 --verbose -o oprof/small.h5 || exit -1

failed=0
gpu_device=@Kokkos_ENABLE_CUDA@@Kokkos_ENABLE_HIP@

case ${gpu_device^^} in
    *ON*)
	@CMAKE_BINARY_DIR@/utils/disk_diff oprof/small.h5 @CMAKE_CURRENT_SOURCE_DIR@/small.h5 \
	    --verbose --all \
	    velocity/radial:1e-20:1e-20 \
	    velocity/phi:1e-20:1e-20 \
	    velocity/theta:1e-14:3e-14 \
	    density:2e-15:2e-13 \
	    energy:6e-10:-1 \
	    radiative_energy:4e-13:-1 \
	    radiative_energy_derivative:1e-6:0.8 \
	    pressure:3e-10:-1 \
	    sound_speed:1e-02:-1 \
	    temperature:4e-4:-1 \
	    || let failed++
	;;
    *)
	@CMAKE_BINARY_DIR@/utils/disk_diff oprof/small.h5 @CMAKE_CURRENT_SOURCE_DIR@/small.h5 \
	    --verbose --all \
	    velocity/radial:1e-20:1e-20 \
	    velocity/phi:1e-20:1e-20 \
	    velocity/theta:4e-15:2e-14 \
	    density:2e-15:2e-13 \
	    energy:4e-11:1e-4 \
	    radiative_energy:3e-13:6e-4 \
	    radiative_energy_derivative:1e-6:0.8 \
	    pressure:2e-11:6e-5 \
	    sound_speed:2e-6:3e-5 \
	    temperature:5e-7:6e-5 \
	    || let failed++
	;;
esac

if [[ $failed -ne 0 ]]
then
    echo FAILED
    exit 1
else
    echo PASSED
    exit 0
fi

