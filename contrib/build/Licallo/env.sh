# Copyright 2023, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load intel-oneapi/2023.1.0
module load intel-oneapi-mpi/2021.9.0
module load boost/1.82.0/oneapi-2023.1.0-mpi 
module load hdf5/1.14.1-2/oneapi-2023.1.0-cxx-fortran
module load cmake/3.26.2
module load ninja/1.11.1
# Those morons redefines sudo...
#module load make
alias make=/opt/ohpc/pub/oca/apps/make/4.2.1/bin/make

export OMP_PROC_BIND=spread
export OMP_PLACES=threads

unset __INTEL_POST_CFLAGS 
unset __INTEL_POST_FFLAGS 

