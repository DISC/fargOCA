// Copyright (C) 2006 Douglas Gregor <doug.gregor -at- gmail.com>.
// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu >.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

// Message Passing Interface

//  See www.boost.org/libs/mpi for documentation.

/** @file noopmpi.hpp
 *
 *  This file is a top-level convenience header that includes all of
 *  the Boost.NoOpMPI library headers. Users concerned about compile time
 *  may wish to include only specific headers from the Boost.NoOpMPI
 *  library.
 *
 */
#ifndef BOOST_NOOPMPI_HPP
#define BOOST_NOOPMPI_HPP

//#include <boost/noopmpi/allocator.hpp>
#include <boost/noopmpi/collectives.hpp>
#include <boost/noopmpi/communicator.hpp>
//#include <boost/noopmpi/datatype.hpp>
#include <boost/noopmpi/environment.hpp>
#include <boost/noopmpi/graph_communicator.hpp>
#include <boost/noopmpi/group.hpp>
#include <boost/noopmpi/intercommunicator.hpp>
#include <boost/noopmpi/nonblocking.hpp>
#include <boost/noopmpi/operations.hpp>
//#include <boost/noopmpi/skeleton_and_content.hpp>
//#include <boost/noopmpi/timer.hpp>

#endif // BOOST_NOOPMPI_HPP
