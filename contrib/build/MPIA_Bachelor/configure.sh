#!/usr/bin/env bash
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
# Copyright 2020, Camille Bergez-Casalou, bergez <at> mpia.de
# Copyright 2020, Gabriele Pichierri, pichierri<a>mpia.de
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

here=$(dirname $0)
srcdir=$here/../../../
blddir=$(pwd)

if [[ -f $blddir/CMakeLists.txt ]]
then
    echo "Please do not build from the source code directory. Go somewhere else and retry."
    exit -1
fi

if [[ "$MPIA_GRADUATE_ENV_SOURCED" -eq "" ]]
then
    echo Environment not loaded, please source $here/env.sh
fi

usage() {
    echo "$0 [option]+"
    echo "This command configure a build system in the current directory which must NOT be $srcdir."
    echo "options:"
    echo "  -h: this message"
    echo "  -d: configure for debug mode"
    echo "  -r: configure for release mode (default)"
    echo "  -e: source the environement before configuring."
}

mode=Release

while getopts "drh" opt
do
    case $opt in
	e)
	    source $here/env.sh
	    ;;
	r)
	    mode=Release
	    ;;
	d)
	    mode=Release
	    ;;
	h)
	    usage
	    exit 1
	    ;;
	\?)
	    echo "$0: unsupported option -$OPTARG" >&2
	    exit -1
	    ;;
    esac
done

cd $blddir
rm -vf CMakeCache.txt
cmake -DCMAKE_BUILD_TYPE=$mode $srcdir

if [[ $? -eq 0 ]]
then
    echo "Configuration succeeded, try to build by running 'make' or 'make -j10' if you have 10 cores."
    exit 0
else
    echo "Something went wrong, contact the team."
fi

