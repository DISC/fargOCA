# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

add_fargo_test_executable(testJoinDispatch testJoinDispatch.cpp)

add_test(testJoinDispatch1 ${MPILAUNCH} -n 1 ./testJoinDispatch)
if(ENABLE_PARALLEL)
  add_test(testJoinDispatch2 ${MPILAUNCH} -n 2 ./testJoinDispatch)
  add_test(testJoinDispatch3 ${MPILAUNCH} -n 3 ./testJoinDispatch)
endif()

add_fargo_test_executable(testViewPack testViewPack.cpp)
add_test(testViewPack ${MPILAUNCH} -n 1 ./testViewPack)
