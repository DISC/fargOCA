// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <filesystem>

#include "boost/program_options.hpp"

#include "environment.hpp"
#include "allmpi.hpp"
#include "scalarField.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "diskProfiles.hpp"

using namespace fargOCA;

namespace po      = boost::program_options;
namespace fs      = std::filesystem;

int
main(int argc, char *argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;

  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract the aspect ratio from a gasDisk.\n"
        << '\t' << cmd << " <gas file> <config file>.\n"
  po::options_description opts(usage.str());
  po::variables_map vm;  
  opts.add_options()
    ("help,h", "This help message.")
    ("h5file", po::value<std::string>(), "The gas file (HDF5).")
    ("config", po::value<std::string>(), "The config file (property tree).");
    try {
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(opts).run();
    po::store(parsed, vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    return -1;
  }
  if (vm.count("help") > 0) {
    std::cout << opts;
    return 0;
  }
}
