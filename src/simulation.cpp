// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <cmath>
#include <time.h>
#include <cstdio>
#include <cstdlib>
#include <valarray>
#include <filesystem>

#include <highfive/H5File.hpp>
#include <nlohmann/json.hpp>

#include "allmpi.hpp"

#include "Kokkos_MathematicalFunctions.hpp"

#include "simulation.hpp"
#include "codeUnits.hpp"
#include "h5io.hpp"
#include "log.hpp"
#include "loops.hpp"
#include "configLoader.hpp"
#include "stellarRadiationSolver.hpp"
#include "transportEngine.hpp"
#include "viscosityTensor.hpp"
#include "gridCellSizes.hpp"
#include "diskProfiles.hpp"
#include "simulationConfig.hpp"
#include "simulationTrackingPlugin.hpp"
#include "cartesianGridCoords.hpp"
#include "h5Labels.hpp"
#include "configProperties.hpp"

namespace fs  = std::filesystem;
namespace kk  = Kokkos;
namespace kkm = Kokkos::Experimental;
namespace HF  = HighFive;
namespace cfg = fargOCA::configProperties;

#define LBD KOKKOS_LAMBDA
#ifdef NDEBUG
#  define NLBD [&]
#else
#  define NLBD [=]
#endif

namespace fargOCA {
  using namespace h5::labels;

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

  namespace details {
    std::string dirname(std::string filepath) {
      fs::path fpath(filepath);
      if (fpath.has_parent_path()) {
        return fpath.parent_path().native();
      } else {
        return ".";
      }
    }
  }
  
  Simulation::Simulation(shptr<Disk> disk,
			 PluginConfig const& trackingConfig,
                         int nbSteps,
                         real timeStep,
			 std::string dir,
                         int step) 
    : myDisk(disk),
      myTrackingConfig(trackingConfig),
      myODir(dir),
      myNbSteps(nbSteps),
      myStep(step),
      myUserTimeStep(timeStep)
  {
    if (!SimulationTrackingPlugin::check(myTrackingConfig, disk->dispatch().log())) {
      disk->dispatch().log() << std::flush;
      throw std::invalid_argument("Bad tracker configuration.");
    }
  }
  
  std::shared_ptr<Simulation>
  Simulation::make(Disk& disk, PluginConfig const& tracking, int nbSteps, real timeStep, std::string odir, int step) {
    return std::shared_ptr<Simulation>(new Simulation(disk.shared(), tracking, nbSteps, timeStep, odir, step), [](Simulation *s) {delete s;});
  }
  
  std::shared_ptr<Simulation>
  Simulation::make(fmpi::communicator const& comm, HighFive::Group root, std::string odir) {
    return std::shared_ptr<Simulation>(new Simulation(comm, root, odir), [](Simulation *s) {delete s;});
  }
  
  std::string 
  Simulation::opath(std::string ofile) const {
    std::filesystem::path path{ofile};
    if (path.is_absolute()
        || (path.has_parent_path()
            && (path.begin()->native() == "."
                || path.begin()->native() == ".."))) {
      return ofile;
    } else {
      return (odir()/ofile).native();
    }
  }

  namespace {
    void
    checkFileFormatVersion(HighFive::Group root, std::ostream& log) {
      std::optional<h5::FormatVersion> found = h5::FormatVersion::read(root);
      h5::FormatVersion current = h5::FormatVersion::current();
      if (!found) {
	log << "No file format version found, current format is " << current << ". Please consider upgrading with 'disk_upgrade'.\n";
      } else {
	if (*found < current) {
	  log << "Found format version " << *found << ", current is " << current << ". Please consider upgrading with 'disk_upgrade'.\n";
	}
      }
    }
  }
  
  Simulation::Simulation(fmpi::communicator const& comm, HighFive::Group group, std::filesystem::path odir) 
    : Simulation(Disk::make(comm, group),
                 PluginConfig{group.getGroup(OUTPUT).getGroup(TRACKERS)},
                 group.getAttribute(NB_STEPS).read<int>(),
                 group.getAttribute(TIME_STEP).read<real>(),
                 odir,
                 group.getAttribute(STEP).read<int>()) {
    checkFileFormatVersion(group, disk().dispatch().log());
  }

  Simulation::~Simulation() {}

  void
  Simulation::signal(SimulationEvent event) const {
    for (auto& [name,cfg]: trackingConfig().namedConfigs()) {
      auto tracker = SimulationTrackingPlugin::get(name).evaluator();
      tracker(name, *this, event);
    }
  }

  void
  Simulation::signal(SimulationStepEvent event) {
    for (auto& [name,cfg]: disk().physic().steps.namedConfigs()) {
      auto const& cb = SimulationStepPlugin::get(name);
      cb(name, *this, event);
    }
  }
    void
  Simulation::start() {
    signal(SimulationEvent::STARTUP);
  }

  void
  Simulation::writeH5(std::optional<HighFive::Group> root, int writer, bool indirectFields) const {
    if ((disk().dispatch().comm().rank() == writer) != bool(root)) {
      std::cerr << "Write hdf5 group should only be provided on writer rank.\n";
      std::abort();
    }
    if (root) {
      root->createAttribute(STEP,      myStep);
      root->createAttribute(NB_STEPS,  myNbSteps);
      root->createAttribute(TIME_STEP, myUserTimeStep);
      h5::FormatVersion::current().write(*root);
      
      trackingConfig().writeH5(root->createGroup(OUTPUT).createGroup(TRACKERS));
      writer = disk().dispatch().comm().rank();
    }
    // agree on writer
    writer = fmpi::all_reduce(disk().dispatch().comm(), writer, std::plus<int>());
    disk().writeH5(root, writer, indirectFields);
  }
  
  template<GridSpacing GS>
  void
  Simulation::planetAccretionStep()
  {
    signal(SimulationStepEvent::BEFORE_ACCRETION_STEP);    
    real dt = *computedTimeStep();
    removeAccretedGas<GS>(dt);
    signal(SimulationEvent::GAS_ACCRETION);
    applyAccretion();
    signal(SimulationStepEvent::AFTER_ACCRETION_STEP);    
  }
  
  template<GridSpacing GS>
  void
  Simulation::doStep() {
    signal(SimulationEvent::BEFORE_USER_STEP);

    Disk& disk = this->disk();
    assert(disk.checkPhysicalQuantities());
    
    GridDispatch const& dispatch = disk.dispatch();
    real dtDone   = 0.0;
    real dtEnergy = 0.0;
    int countEnergySteps = 1;
    disk.setGhosts();
    while (dtDone < userTimeStep()) {
      assert(disk.checkPhysicalQuantities());
      disk.setGhosts();
      real dtLeft = userTimeStep()-dtDone;
      myComputedTimeStep = disk.convergingTimeStep(dtLeft);
      real const dt = *computedTimeStep();
      signal(SimulationEvent::BEFORE_STEP);
      signal(SimulationStepEvent::BEFORE_STEP);
      dtDone += dt;
      planetAccretionStep<GS>();
      std::optional<Triplet> prevPosition;
      if (disk.physic().referential.type == DiskReferential::COROTATING) {
        prevPosition = disk.system()->guidingPlanetPosition();
      }
      if (disk.system()) {
	debug::ScopedDump dump(disk, "mass_taper");
        disk.system()->applyMassTaper(disk.physicalTime());
      }
      Triplet gasAcceleration = disk.computeGasAcceleration();
      LocalFieldStack3D<real, 1> locals(dispatch.temporaries3D<real>());
      arr3d<real> systemPotential = locals.init<0>();
      disk.computeSystemPotential(gasAcceleration, systemPotential);
      if (disk.system()) {
	disk.advanceSystemFromDisk(dt, gasAcceleration);
	disk.system()->advance(dt);
      }
      disk.addPhysicalTime(dt);
      if (disk.physic().referential.type == DiskReferential::COROTATING) {
	real omega    = disk.frameVelocity();
	Triplet nextPosition = *disk.system()->guidingPlanetPosition();
	real newOmega = disk.system()->frequency(*prevPosition, nextPosition, dt);
	real domega = newOmega-omega;
	disk.adjustFrameVelocity(domega);
      }
      if (disk.system()) {
	disk.system()->rotate(disk.frameVelocity()*dt);
	signal(SimulationEvent::PLANETS_CHANGED);
      }
      disk.applyBoundaryConditions(dt);
      dispatch.log() << "." << std::flush;
      
      velocityStep<GS>(systemPotential);

      if(disk.physic().viscosity.artificial){
        viscosityStep<GS>();
      }
      disk.applyBoundaryConditions(dt);
      if (disk.physic().adiabatic) {
        adiabaticStep<GS>();
        disk.applyBoundaryConditions(dt);
        if (disk.physic().adiabatic->radiative) {
          std::optional<real> energyTimeStep = disk.physic().adiabatic->radiative->energyTimeStep;
          if (bool(energyTimeStep) // Compute energy step every energyTimeStep
              && (userTimeStep() >= (countEnergySteps+1)*(*energyTimeStep))) {  // Advance energy with dtEne
            dtEnergy += dt;
            if (dtEnergy >= *energyTimeStep) {
              energyStep(dtEnergy);
              disk.applyBoundaryConditions(dtEnergy);
              dtEnergy = 0;	
              countEnergySteps++;
            }
          } else { // Fill the  timeStep with Advance energy with dt  
            // Default:  compute energyStep with hydroStep dt
            energyStep(dt);
            disk.applyBoundaryConditions(dt);
          }
        } 
      }
      transportStep<GS>(dt);
      disk.applyBoundaryConditions(dt);
      signal(SimulationEvent::AFTER_STEP);
      signal(SimulationStepEvent::AFTER_STEP);
      myComputedTimeStep = std::nullopt;
    }
    dispatch.log() << "\n";
    assert(disk.checkPhysicalQuantities());
    ++myStep;
    signal(SimulationEvent::AFTER_USER_STEP);
  }

  template<GridSpacing GS>
  void
  Simulation::transportStep(real dt) {
    signal(SimulationStepEvent::BEFORE_TRANSPORT_STEP); 
    std::vector<arr3d<real>> transported{};
    if (disk().physic().adiabatic) { transported.push_back(disk().menergy()->data()); }

    arr3d<real const> density {disk().density().data()};
    for( ScalarField& field : disk().tracers()) {
      arr3d<real> tracer {field.data()};
      transported.push_back(tracer);
      kk::parallel_for("bind_tracers", fullRange(disk().dispatch().grid()),
                       LBD(size_t const i, size_t const h, size_t const j) { tracer(i,h,j) *= density(i,h,j); });
    }
    
    applyTransport<GS>(disk(), dt, disk().mdensity().data(), disk().velocity().data(), transported);
    
    for (ScalarField& field: disk().tracers()) {
      arr3d<real> tracer = field.data();
      kk::parallel_for("restore_tracers", fullRange(disk().dispatch().grid()),
                       LBD(size_t const i, size_t const h, size_t const j) { 
                         tracer(i,h,j) /= density(i,h,j);
                       });
    }
    if (disk().physic().adiabatic) { disk().trashEnergy(); }
    signal(SimulationStepEvent::AFTER_TRANSPORT_STEP);     
  }
  
  template<GridSpacing GS>
  void
  Simulation::velocityStep(arr3d<real const> systemPotential) {
    signal(SimulationStepEvent::BEFORE_VELOCITY_STEP); 
    debug::ScopedDump dump(disk(), debug::VELOCITY_STEP);
    GridDispatch const& dispatch = disk().dispatch();

    auto const& coords = dispatch.grid().as<GS>();
    GridSizes const ls = coords.sizes();
    
    enum { VR, VP, VT };
    LocalFieldStack3D<real,3> locals(dispatch.temporaries3D<real>());
    arr3d<real>       vrad1     = disk().velocity().radial().data();
    arr3d<real>       vtheta1   = disk().velocity().theta().data();
    arr3d<real>       vphi1     = disk().velocity().phi().data();
    arr3d<real const> vrad0     = locals.init<VR>(vrad1);
    arr3d<real const> vtheta0   = locals.init<VT>(vtheta1);
    arr3d<real const> vphi0     = locals.init<VP>(vphi1);

    arr3d<real const> rho              = disk().density().data();
    arr3d<real const> pressure         = disk().pressure().data();

    auto radMed = coords.radiiMed();
    auto radii  = coords.radii();
    auto phi    = coords.phi();
    auto phiMed = coords.phiMed();
    arr3d<real const> dtheta = dispatch.cellSizes().dTheta();
    
    /* In this substep we take into account     */
    /* the source part of Euler equations       */
    /* (i.e. the R.H.S. in classical notation). */
    real const omega = disk().frameVelocity();
    bool const starAccretionWind = disk().physic().starAccretion && disk().physic().starAccretion->type == StarAccretion::WIND;
    bool const flatDisk = coords.dim() != 3;

    real const NaN = std::numeric_limits<real>::quiet_NaN();
    arr3d<real const> deaddens = starAccretionWind ? disk().deadDensity()->data() : arr3d<real>();
    real const activeDensity = starAccretionWind ? disk().physic().starAccretion->wind->userActiveDensity : NaN;
    real const torqueCoeff   = starAccretionWind ? disk().physic().starAccretion->torqueCoeff()           : NaN;
    real const steepness     = starAccretionWind ? disk().physic().starAccretion->wind->filter            : NaN;
    real const dt = *computedTimeStep();
    {
      debug::ScopedDump dump(disk(), debug::VELOCITY_STEP+"_kernel");
      kk::parallel_for("vel_step", fullRange(coords),
		       LBD(size_t const i, size_t const h, size_t const j) {
			 size_t const jp = nsnext(ls.ns, j);
			 if (i>0) {
			   real const gradp         = (pressure(i,h,j)-pressure(i-1,h,j))*2/(rho(i,h,j)+rho(i-1,h,j));
			   real const gradphi       = (systemPotential(i,h,j)-systemPotential(i-1,h,j));
			   real const grad          = (gradp+gradphi)/(radMed(i)-radMed(i-1));
			   real const vthetaAverage = (vtheta0(i,h,j)+vtheta0(i,h,jp)+vtheta0(i-1,h,j)+vtheta0(i-1,h,jp))/4;
			   if (flatDisk) {
			     real const vt2 = ipow<2>(vthetaAverage + radii(i)*omega);
			     vrad1(i,h,j) = vrad0(i,h,j)+dt*(-grad+vt2/radii(i));
			   } else {
			     real const vt2 = ipow<2>(vthetaAverage + radii(i)*omega*kk::sin(phiMed(h)));
			     real const vp2 = ipow<2>(h < ls.ni-1
						      ? (vphi0(i,h,j)+vphi0(i,h+1,j)+vphi0(i-1,h,j)+vphi0(i-1,h+1,j))/4
						      : (vphi0(i,h,j)+vphi0(i-1,h,j))/2);
			     vrad1(i,h,j) = vrad0(i,h,j)+dt*(-grad+(vt2+vp2)/radii(i));
			   } 
			 }
			 {
			   int const jm = j == 0 ? ls.ns-1 : j-1;
			   real const gradp   = 2*(pressure(i,h,j)-pressure(i,h,jm))/(rho(i,h,j)+rho(i,h,jm));
			   real const gradphi = (systemPotential(i,h,j)-systemPotential(i,h,jm));
			   real const grad    = (gradp+gradphi)/dtheta(i,h,j);   
			   vtheta1(i,h,j) = vtheta0(i,h,j) - dt*grad;
			   
			   if(starAccretionWind) {
			     // Impose an acceleration in the azimuthal direction to generate the torque in case of Disk-wind driven transport model
			     real const x = -steepness*(1-deaddens(i,h,j)/activeDensity);
			     if (x < expMaxParam<real>) {
			       vtheta1(i,h,j) -= dt*torqueCoeff*kk::pow(radMed(i),-2.5)/(1+std::exp(x));
			     }
			   }
			 }
			 if (h>0) {
			   real const sinPhi = kk::sin(phi(h));
			   real const cosPhi = kk::cos(phi(h));
			   real const gradp   = 2*(pressure(i,h,j)-pressure(i,h-1,j))/(rho(i,h,j)+rho(i,h-1,j));
			   real const gradphi = (systemPotential(i,h,j)-systemPotential(i,h-1,j));
			   real const grad    = (gradp+gradphi)/(radMed(i)*(phiMed(h+1)-phiMed(h)));
			   real const vt2 = ipow<2>((vtheta0(i,h,j)+vtheta0(i,h,jp)+vtheta0(i,h-1,j)+vtheta0(i,h-1,jp))/4
						    + radMed(i)*omega*sinPhi);
			   vphi1(i,h,j) = vphi0(i,h,j) + dt*(-grad+vt2*cosPhi/sinPhi/radMed(i));      
			 }
		       });
    }
    disk().applyBoundaryConditions(dt);
    ViscosityTensor::advanceVelocity(disk(), dt);
    signal(SimulationStepEvent::AFTER_VELOCITY_STEP); 
  }
  
  void
  Simulation::doStep() {
    Grid const& grid = disk().dispatch().grid();
    switch (grid.radialSpacing()) {
    case ARTH: Simulation::doStep<ARTH>(); break;
    case LOGR: Simulation::doStep<LOGR>(); break;
    }
  }
  
  template<GridSpacing GS>
  void
  Simulation::viscosityStep()
  {
    signal(SimulationStepEvent::BEFORE_VISCOSITY_STEP); 
    debug::ScopedDump dump(disk(), "viscosity_step");
    GridDispatch const& dispatch = disk().dispatch();    
    auto const& coords = dispatch.grid().as<GS>();
    GridSizes const ls = coords.sizes();
    
    arr3d<real> rho    = disk().mdensity().data();
    arr3d<real> vrad   = disk().velocity().radial().data();
    arr3d<real> vtheta = disk().velocity().theta().data();
    arr3d<real> vphi   = disk().velocity().phi().data();

    enum { QR, QT, QP };
    LocalFieldStack3D<real,3> locals(dispatch.temporaries3D<real>());
    
    arr3d<real> qr = locals.init<QR>();
    arr3d<real> qt = locals.init<QT>();
    arr3d<real> qp = locals.init<QP>();
    
    arr3d<real const> dtheta = dispatch.cellSizes().dTheta();
    
    auto radii  = coords.radii();
    auto radMed = coords.radiiMed();
    auto phiMed = coords.phiMed();
    
    bool const flat      = coords.dim() != 3;
    bool const adiabatic = bool(disk().physic().adiabatic);
    arr3d<real> energy   = adiabatic ? disk().menergy()->data() : arr3d<real>();
    real const dt = *computedTimeStep();
    kk::parallel_for("visc_init_q", fullRange(coords),
                     LBD(size_t const i, size_t const h, size_t const j) {
                       // Clean up the borders
                       if (i == ls.nr-1) {
                         qr(i,h,j) = 0;
                         qt(i,h,j) = 0;
                         qp(i,h,j) = 0;
                       } else {
                         {
                           real const dv = vrad(i+1,h,j)-vrad(i,h,j);
                           qr(i,h,j) = dv < 0 ? ipow<2>(CVNR*dv)*rho(i,h,j) : 0;
                         }
                         {
                           size_t const jp = nsnext(ls.ns, j);
                           real const dv = vtheta(i,h,jp)-vtheta(i,h,j);
                           qt(i,h,j) = dv < 0 ? ipow<2>(CVNR*dv)*rho(i,h,j) : 0;
                         }
                         if (!flat) {
                           if (h == ls.ni-1) {
                             qr(i,h,j) = 0;
                             qt(i,h,j) = 0;
                             qp(i,h,j) = 0;
                           } else {                       
                             real const dv = vphi(i,h+1,j)-vphi(i,h,j);
                             qp(i,h,j) = dv < 0 ? ipow<2>(CVNR*dv)*rho(i,h,j) : 0;
                           }
                         }
                       } 
                     });
    
    kk::parallel_for("vis_step_2", range3D({0,0,0},{ls.nr,ls.ni,ls.ns}),
                     LBD(size_t const i, size_t const h, size_t const j) {
                       if (i>0) {
                         vrad(i,h,j)    -= dt*2/(rho(i,h,j)+rho(i-1,h,j))*(qr(i,h,j)-qr(i-1,h,j))/(radMed(i)-radMed(i-1));
                       }
                       {
                         int jm = j == 0 ? ls.ns-1 : j-1;
                         vtheta(i,h,j)  -= dt*2/(rho(i,h,j)+rho(i,h,jm))*(qt(i,h,j)-qt(i,h,jm))/dtheta(i,h,j);
                       }
                       if (!flat){
                         if (h>0) {
                           vphi(i,h,j) -= dt*2/(rho(i,h,j)+rho(i,h-1,j))*(qp(i,h,j)-qp(i,h-1,j))/(radMed(i)*(phiMed(h+1)-phiMed(h)));
                         }
                       }
                     });
    
    if (adiabatic) {
      /* If disk disk is adiabatic, we add artificial viscosity as a source */
      /* term for advection of thermal energy polargrid */
      kk::parallel_for("vis_step_2", range3D({0,0,0},{ls.nr,ls.ni,ls.ns}),
                       LBD(size_t const i, size_t const h, size_t const j) {
                         if (i<ls.nr-1 && h<ls.ni-1) {
                           real const invDiffRad = 1/(radii(i+1)-radii(i));
                           real const invDPhiMed = 1/(radMed(i)*(phiMed(h+1)-phiMed(h)));
                           size_t const jp = nsnext(ls.ns, j);
                           energy(i,h,j) -= dt* (qr(i,h,j)*(vrad(i+1,h,j)-vrad(i,h,j))*invDiffRad 
                                                 + qt(i,h,j)*(vtheta(i,h,jp)-vtheta(i,h,j))/dtheta(i,h,j)
                                                 + qp(i,h,j)*(vphi(i,h+1,j)-vphi(i,h,j))*invDPhiMed);
                         }
                       });
    }
    assert(disk().checkPhysicalQuantities());
    signal(SimulationStepEvent::AFTER_VISCOSITY_STEP); 
  }
  
  template <GridSpacing GS>
  void
  Simulation::adiabaticStep()
  {
    signal(SimulationStepEvent::BEFORE_ADIABATIC_STEP); 
    debug::ScopedDump dump(disk(), "adiabatic_step");
    Disk& disk = *myDisk;
    GridDispatch const& dispatch = disk.dispatch();
    auto const& coords = dispatch.grid().as<GS>();

    arr3d<real const> density    = disk.density().data();
    arr3d<real const> pressure   = disk.pressure().data();
    arr3d<real>       energy     = disk.menergy()->data();
    arr3d<real const> vdiv       = divergence(disk.velocity());
    auto radMed = coords.radiiMed();

    real const dt = *computedTimeStep();
    kk::parallel_for("adia_energy", fullRange(coords),  
                     LBD(size_t const i, size_t const h, size_t const j) {
                       energy(i,h,j) -= dt*pressure(i,h,j)*vdiv(i,h,j); 
                     });
    
    assert(disk.energy()->min() >= 0);
    /* Restoring  initial temperature : T(t+dt)=T(t)-DELTAT*(1-exp(-dt/tau))
       adapted to 3D from the 2D version in:SourceEuler_adiab-cool.c */
    typedef DiskPhysic::Adiabatic::BetaCooling Beta;
    typedef DiskPhysic::Adiabatic::RadiativeCooling Radiative;
    if(disk.physic().adiabatic->cooling){
      if (std::holds_alternative<Beta>(*disk.physic().adiabatic->cooling)) {
	DiskProfiles const& profiles  = disk.profiles();
	arr2d<real const> densityMed  = profiles.density();
	arr2d<real const> energyMed   = *(profiles.energy());
	Beta const& cooling = std::get<Beta>(*disk.physic().adiabatic->cooling);
	real const timeScale = cooling.timeScale;
	
	kk::parallel_for("adia_energy2", fullRange(coords),  
			 LBD(size_t const i, size_t const h, size_t const j) {
			   real const coolingTime = timeScale*2*PI*kk::pow(radMed(i), real(3)/2);
			   real const frac = kk::exp(-dt/coolingTime)-1;
			   energy(i,h,j) += (energy(i,h,j) - (energyMed(i,h)/densityMed(i,h))*density(i,h,j))*frac;
			 });
      } else if (std::holds_alternative<Radiative>(*disk.physic().adiabatic->cooling)) {
	bool const flat = dispatch.grid().dim() == 2;
	if (flat) {
	  Radiative const& cooling = std::get<Radiative>(*disk.physic().adiabatic->cooling);
	  arr3d<real const> temperature = disk.temperature()->data();
	  arr3d<real const> viscosity   = disk.viscosity().data();
	  CodeUnits const& cunits = disk.physic().units;
	  
	  real const distanceCGS     = cunits.distance();
	  real const densityCGS      = cunits.density();
	  real const centralBodyMass = cunits.centralBodyMass();
	  real const BoltzmannCst    = cunits.StefanBoltzmannCst();

	  LocalFieldStack3D<real,1> locals(disk.dispatch().template temporaries3D<real>());
	  ViscosityTensor::computeQPlus(disk, locals.get<0>());
	  arr3d<real const> qplus            = locals.get<0>();

	  kk::parallel_for("adia_energy2", fullRange(coords),
			   LBD(size_t const i, size_t const h, size_t const j) {
			     real const opacity = 1*densityCGS*distanceCGS;   // We start with opacity of 1 cm^2/g and move to code unities
			     real const tau_eff = 0.5*density(i,h,j)*opacity;
			     real const Omega_k2 = G*centralBodyMass/ipow<3>(radMed(i));
			     //real const qplus = (9.0/8.0)*density(i,h,j)*viscosity(i,h,j)*Omega_k2;
			     real const qminus = 2.0*BoltzmannCst*ipow<4>(temperature(i,h,j))/tau_eff;
			     energy(i,h,j) += dt*(qplus(i,h,j)-qminus);
			   });
	}
      } else {
	std::abort();
      }
    }
    signal(SimulationStepEvent::AFTER_ADIABATIC_STEP); 
  }

  void
  Simulation::energyStep(real dt)
  {
    signal(SimulationStepEvent::BEFORE_ENERGY_STEP); 
    debug::ScopedDump dump(disk(), debug::ENERGY_STEP);
    assert(disk().checkPhysicalQuantities());
    disk().stellarRadiationSolver().advance(dt);
    assert(disk().checkPhysicalQuantities());
    signal(SimulationStepEvent::AFTER_ENERGY_STEP);     
  }

  template<GridSpacing GS>
  void
  Simulation::removeAccretedGas(real dt) {
    debug::ScopedDump dump(disk(), debug::REMOVE_ACCRETED_GAS);
    std::map<std::string,Momentum> retrieved;
    if (disk().system()) {
      for (auto& [name, planet] : disk().system()->planets()) {
        PlanetarySystemPhysic::Planet const planetPhysic = disk().physic().planetarySystem()->planet(name);
        if (planetPhysic.accretionTime) {
          removePlanetAccretedGas<GS>(name, dt);
        }
      }
    }
  }

  template <GridSpacing GS>
  void 
  Simulation::removePlanetAccretedGas(std::string name, real dt) {
    GridDispatch const& dispatch = disk().dispatch();
    assert(disk().system());
    PlanetarySystemPhysic::Planet const planetPhysic = disk().physic().planetarySystem()->planet(name);
    assert(bool(planetPhysic.accretionTime));
    Planet const& planet       = disk().system()->planet(name);
    
    auto box = getRocheBox(disk(), planet);
    Momentum gained;

    if (box) {
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const ls = coords.sizes();
      
      arr3d<real>       dens   = disk().mdensity().data();
      arr3d<real const> vrad   = disk().velocity().radial().data();
      arr3d<real const> vtheta = disk().velocity().theta().data();
      arr3d<real const> vphi   = disk().velocity().phi().data();
      arr3d<real const> volume = dispatch.cellSizes().volume();
      auto radMed = coords.radiiMed();
      auto radii = coords.radii();
      auto phi    = coords.phi();
      auto phiMed = coords.phiMed();
      auto theta  = coords.theta();
      
      bool half = disk().physic().shape.half;
      
      // Initialization of W. Kley's parameters 
      real const facc = dt/(*planetPhysic.accretionTime);
      real const frac1 = 0.6;
      real const frac2 = 0.3;

      CodeUnits const& units = disk().physic().units;
      real const planetDist = planet.position().norm();
      real const RocheLimit = std::cbrt(units.centralBodyMass()/3*planet.mass())*planetDist; // Central mass is 1
      
      range_t const managedRadii = dispatch.managedRadii();
      auto cartesian = dispatch.cartesian();
      bool const flat = dispatch.grid().dim() == 2;
      size_t const iBeg {box->first.nr};
      size_t const iEnd {box->second.nr};
      size_t const hMin = box->first.ni;
      size_t const hMax = box->second.ni;
      size_t const jMin = box->first.ns;
      size_t const jMax = box->second.ns;
      real const frameVelocity = disk().frameVelocity();
      kk::parallel_reduce("momentum_transfert", range3D({iBeg, hMin, jMin}, {iEnd, hMax, jMax}),
                          LBD(size_t const i, size_t const h, size_t const j, Momentum& partial) {
			    real const sinPhiMed = kk::sin(phiMed(h));
			    real const cosPhiMed = kk::cos(phiMed(h));
			    size_t const jp = nsnext(ls.ns, j);
			    real const distance = (planet.position()-cartesian.coords(i,h,j)).norm();
			    if (distance < frac1*RocheLimit) {
			      real const vtcell = (vtheta(i,h,j)+vtheta(i,h,jp))/2+radMed(i)*frameVelocity;
			      real const vrcell = (vrad(i,h,j)+vrad(i+1,h,j))/2;
			      real const vpcell = flat ? real(0) : 0.5*(vphi(i,h,j)+vphi(i,h+1,j));
			      Triplet vcell(kk::cos(theta(j))*(vrcell*sinPhiMed+vpcell*cosPhiMed)-vtcell*kk::sin(theta(j)),
					    kk::sin(theta(j))*(vrcell*sinPhiMed+vpcell*cosPhiMed)
					    +  vtcell*cos(theta(j)),
					    vrcell*cosPhiMed-vpcell*sinPhiMed);
			      if (distance >= frac2*RocheLimit) {
				real const faccfilter = ipow<2>(sin(M_PI/2.*(distance/((frac2-frac1)*RocheLimit)-frac1/(frac2-frac1))))*facc;
				real deltaM     = faccfilter*dens(i,h,j)*volume(i,h,j);
				if(half && !flat) { deltaM *= 2; } // Take into account the contribution from the whole disk 
				dens(i,h,j) *= (1 - faccfilter);
				partial    += Momentum(vcell, deltaM);
			      } else {
				real deltaM  = facc*dens(i,h,j)*volume(i,h,j);
				dens(i,h,j) *= (1 - facc);
				if(half) { deltaM *= 2; } // Take into account the contribution from the whole disk
				partial += Momentum(vcell, deltaM);
			      }
			    }
                          }, kk::Sum<Momentum>(gained));
    }
    myAccretedGas[name] = fmpi::all_reduce( dispatch.comm(), gained,  std::plus<Momentum>());
  }
  
  void
  Simulation::applyAccretion() {
    for (auto const& [pname, delta] : myAccretedGas) {
      PlanetarySystemPhysic::Planet const planetPhysic = disk().physic().planetarySystem()->planet(pname);
      if (planetPhysic.accretionTime) {
        Planet&                planet = disk().system()->planet(pname);
        Momentum const planetMomentum = planet.momentum() + delta;
        if (disk().physic().planetarySystem()->accreteOntoPlanets){
          if (planetPhysic.feelDisk) {
            planet.setVelocity(planetMomentum.velocity());
          }
          planet.setMass(planetMomentum.mass);
        } else {
          disk().system()->looseOnAccretion(planetMomentum.mass);
        }
      }
    }
    myAccretedGas.clear();
  }
  
  void
  Simulation::dump(std::ostream& log) const {
    disk().dispatch().dumpMpiLayout();
    disk().physic().units.dump(log);
    disk().physic().dump(log);
    if (disk().system()) {
      disk().system()->dump(log);
    }
    myTrackingConfig.dump(log);
    log << "User time step: " << userTimeStep() << '\n';
  }
  
  void Simulation::dump() const { dump(disk().dispatch().log()); }
  namespace cp     = fargOCA::configProperties;

  std::shared_ptr<Simulation>
  Simulation::makeFromPropertyTree(fmpi::communicator const& comm, std::string configFile) {
    if (!fs::exists(configFile)) {
      std::cerr << "Could not read configuration file '" << configFile << "', bye.\n";
      return nullptr;
    }
    ConfigLoader loader(std::cerr);
    loader.load(configFile);
    shptr<DiskPhysic const>  physic = loader.extractPhysic();  
    shptr<GridDispatch const> grid   = GridDispatch::make(comm, *loader.extractGridCoords());
    shptr<Disk>               disk   = Disk::make(*physic, *grid);
    auto tracking = loader.extractTrackingConfig();
    int  nbSteps  = loader.cfg().get<int>(cp::OUTPUT/cp::NB_STEP);
    real timeStep = loader.cfg().get<real>(cp::OUTPUT/cp::TIME_STEP);
    return Simulation::make(*disk, tracking, nbSteps, timeStep, "./", 1);
  }

  void
  to_json(nlohmann::json& j, SimuConfigWrapper<Simulation> const& c) {
    using namespace cp;
    j[snake(cp::DISK)] = configuration(c.subject.disk());
    j[snake(cp::TRACKERS)] = c.subject.trackingConfig();
    j[snake(cp::NB_STEP)] = c.subject.nbSteps();
    j[snake(cp::TIME_STEP)] = c.subject.userTimeStep();
  }
}
