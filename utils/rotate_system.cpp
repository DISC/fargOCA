// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <optional>
#include <filesystem>
#include <sysexits.h>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "h5io.hpp"
#include "h5Labels.hpp"

using namespace fargOCA;

namespace po      = boost::program_options;
namespace fs      = std::filesystem;
namespace HF      = HighFive;
namespace h5l     = fargOCA::h5::labels;

using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Rotate the planetary system stored in the file.\n"
        << '\t' << cmd << " <file>.h5 <angle> [option]+: rotate the system stored in <file>.h5 by the angle <angle>.\n";
  
  po::options_description opts(usage.str());
  
  opts.add_options()
    ("help,h", "This help message.")
    ("file,f", po::value<std::string>(), "HDF5 file containing the existing disk to be rebinded.")
    ("angle,a", po::value<real>(), "The rotation angle.")
    ("output,o", po::value<std::string>(), "HDF5 file where to write the rebinded disk. Defaults to input.")
    ("path,p", po::value<std::string>()->default_value(h5l::PLANETARY_SYSTEM),
     "The path where the planetary system is stored.")
    ("physic", po::value<std::string>()->default_value(h5l::PHYSIC),
     "The path where the physic is stored.");

  po::positional_options_description input_option;
  input_option.add("file", 1);
  input_option.add("angle", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"file", "angle"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);

  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);
  
  std::string ifname = vm["file"].as<std::string>();
  HF::File ifile(ifname, HF::File::ReadWrite);
  auto root = ifile.getGroup("/");
  std::string path = vm["path"].as<std::string>();
  if (!root.exist(path)) {
    std::cerr << "could not find group '" << path << "', Bye\n.";
    std::exit(EX_USAGE);
  }
  auto system_group = root.getGroup(path);
  std::string physic_path = vm["physic"].as<std::string>();
  if (!root.exist(physic_path)) {
    std::cerr << "could not find group '" << physic_path << "', Bye\n.";
    std::exit(EX_USAGE);
  }
  auto physic_group = root.getGroup(physic_path);
  auto system = PlanetarySystem::make(*DiskPhysic::make(CodeUnits(), physic_group), system_group);
  system->rotate(vm["angle"].as<real>());
  root.unlink(path);
  auto rotated_group = root.createGroup(path);
  system->writeH5(rotated_group);
  return 0;
}
