# Copyright 2020, Alain Miniussi, alain.miniussi<at>oca.eu
# Copyright 2020, Camille Bergez-Casalou, bergez <at> mpia.de
# Copyright 2020, Gabriele Pichierri, pichierri<a>mpia.de
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

module purge
module load openmpi-4.0.1

export BOOST_ROOT=/usr/local2/misc/boost/boost_1_73_0/build
export HDF5_ROOT=/usr/local2/misc/hdf5/hdf5-1.12.0/build
export PATH=/usr/local2/misc/cmake/cmake-master/bin:$PATH
export LD_LIBRARY_PATH=$BOOST_PATH/lib:$HDF5_ROOT/lib:$LD_LIBRARY_PATH

export MPIA_GRADUATE_ENV_SOURCED=1
