// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iomanip>
#include <fstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include <highfive/H5File.hpp>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "log.hpp"
#include "grid.hpp"
#include "planetarySystem.hpp"
#include "gridDispatch.hpp"
#include "configLoader.hpp"
#include "diskPhysic.hpp"
#include "disk.hpp"
#include "transportEngine.hpp"
#include "loops.hpp"
#include "simulation.hpp"

using namespace fargOCA;

namespace po      = boost::program_options;
namespace fs      = std::filesystem;
namespace kk      = Kokkos;
namespace kkm     = Kokkos::Experimental;
namespace HF      = HighFive;

using std::optional;

template<GridSpacing GS>
ScalarField
compute_mdot_map_impl(Disk const& disk, real dt) 
{
  GridDispatch const& dispatch = disk.dispatch();
  ScalarField mdot("gasmdotmap", dispatch);
  auto const& coords = dispatch.grid().as<GS>();
  GridSizes const ls = coords.sizes();
  
  arr3d<real> one = scalarView("One", dispatch.grid());
  kk::deep_copy(one, 1);
  auto qrs  = getTransmitedFractionRadial(disk, one, disk.velocity().radial().data(), dt);
  auto rhos = getTransmitedFractionRadial(disk, disk.density().data(), disk.velocity().radial().data(), dt);
  auto vr   = disk.velocity().radial().data();
  
  auto radii = coords.radii();
  auto phi = coords.phi();
  arr3d<real>  mdotmap = mdot.data();

  // The mdotmap is computed for each cell assuming that the grid is regular and omitting the multyplication by dtheta=2pi/ns
  // one would like to have: mdotmap[l]= rhos[l]*qrs[l]*vr[l]*radii2[i]*(theta[j+1]-theta[j])*surfr;
  // remember to add the factor 2pi/ns when using the data
  kk::parallel_for(range3D({0,0,0}, {ls.nr-1,ls.ni,ls.ns}),
                   KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j) {
                     real const radii2 = kk::pow(radii(i), 2);
                     real const surfr  = kk::cos(phi(h))-kk::cos(phi(h+1));
                     mdotmap(i,h,j) = rhos(i,h,j)*qrs(i,h,j)*vr(i,h,j)*radii2*surfr;
                   });
  return mdot;
}

std::string 
output_fname(po::variables_map& vm) {
  std::string ifile = vm["disk"].as<std::string>();
  std::string basename = ifile;
  {
    auto pos = ifile.find_last_of(".h5");
    if (pos != std::string::npos) {
      basename = ifile.substr(0, pos-2);
    }
  }
  return basename + "_mdot.h5";
}

GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

ScalarField
compute_mdot_map(Disk const& disk, real dt) {
  auto const& coords = disk.dispatch().grid();
  switch (coords.radialSpacing()) {
  case ARTH: return compute_mdot_map_impl<ARTH>(disk, dt);
  case LOGR: return compute_mdot_map_impl<LOGR>(disk, dt);
  }
  std::abort();
}

int
main(int argc, char* argv[]) {
  Environment env(argc, argv);
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Extract radial flow.\n"
        << '\t' << cmd << " <idisk>.h5 [option]+.\n";
    
  po::options_description opts(usage.str());
  
  po::variables_map vm;
  opts.add_options()
    ("help,h", "This help message.")
    ("disk", po::value<std::string>(), "HDF5 file containing the disk.")
    ("delta", po::value<real>(), "The delta time. If not provided, will try comething.")
    ("ofile,o", po::value<std::string>(), "Output file. Default on input file.")
    ("path", po::value<std::string>()->default_value("mdotmap"), "The path, in the output file, where the disk is written.")
    ;

  po::positional_options_description input_option;
  input_option.add("disk", 1);
  try {
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(input_option).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cerr << unknown.what() << '\n' << "Usage:\n" << opts << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << opts;
    std::exit(EX_CANTCREAT);
  } 
  for(std::string opt : {"disk"}) {
    if (vm.count(opt)==0) {
      std::cerr << "missing mandatory option '--" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }

  std::string ifname = vm["disk"].as<std::string>();
  if (env.world().size() > 1) {
    std::cerr << "As for now, '" << argv[0] << "' must be launched wit only one proc.\n";
    return EX_USAGE;
  }
  std::optional<ScalarField> mdot;
  {
    auto simulation{Simulation::make(env.world(), h5::getRoot(ifname), ".")};
    real dt = bool(vm.count("delta")) ? vm["delta"].as<real>() : simulation->disk().convergingTimeStep(simulation->userTimeStep());
    mdot = compute_mdot_map(simulation->disk(), dt);
  }
  std::string ofname = bool(vm.count("ofile")) ? vm["ofile"].as<std::string>() : ifname;
  std::string path   = vm["path"].as<std::string>();
  HF::File  ofile(ifname, HF::File::OpenOrCreate);
  std::optional<HF::Group> root = (env.world().rank() == 0 
                                   ? std::optional<HF::Group>(ofile.exist(path)
                                                              ? ofile.getGroup(path)
                                                              : ofile.createGroup(path)) 
                                   : std::nullopt);
  mdot->writeH5(root, 0);
  
  return 0;
}
