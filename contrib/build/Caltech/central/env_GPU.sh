# Copyright 2023, Gabriele Pichierri, gabe@caltech.edu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

module purge

module load gcc/9.2.0 openmpi/4.1.1_gcc-9.2.0 boost/1_81_0_openmpi-4.1.1_gcc-9.2.0 cmake/3.18.0 hdf5/1.12.1 openssl/3.0.3 python3/3.10.7

module load cuda/11.3

export BOOST_HOME=/central/software/boost/1_81_0_openmpi-4.1.1_gcc-9.2.0/
export BOOST_ROOT=/central/software/boost/1_81_0_openmpi-4.1.1_gcc-9.2.0/
