# Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
# Copyright 2018, Clément Robert, clement.robert<at>oca.eu
# Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
#
# This file is part of FargOCA.
#
# FargOCA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# FargOCA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

configure_file(slurmTest.sh.in slurmTest.sh @ONLY)
configure_file(slurmTrackTest.sh.in slurmTrackTest.sh @ONLY)
configure_file(test.slurm.in test.slurm @ONLY)

configure_file(licalloSlurmPrefix.txt licalloSlurmPrefix.txt @ONLY)
configure_file(licTest.sh.in          licTest.sh @ONLY)
configure_file(licTrackTest.sh.in     licTrackTest.sh @ONLY)

configure_file(adastraSlurmPrefix.txt.in adastraSlurmPrefix.txt @ONLY)
configure_file(aaTest.sh.in              aaTest.sh @ONLY)
configure_file(aaTrackTest.sh.in         aaTrackTest.sh @ONLY)

configure_file(jeanzaySlurmPrefix.txt.in jeanzaySlurmPrefix.txt @ONLY)
configure_file(jzTest.sh.in      jzTest.sh @ONLY)
configure_file(jzTrackTest.sh.in jzTrackTest.sh @ONLY)

configure_file(mpiablrSlurmPrefix.txt mpiablrSlurmPrefix.txt @ONLY)
configure_file(mpiablrTest.sh.in      mpiablrTest.sh @ONLY)
configure_file(mpiablrTrackTest.sh.in mpiablrTrackTest.sh @ONLY)

configure_file(lunarcTest.sh.in         lunarcTest.sh         @ONLY)
configure_file(lunarcTrackTest.sh.in    lunarcTrackTest.sh    @ONLY)

configure_file(amuSlurmPrefix.txt     amuSlurmPrefix.txt @ONLY)
configure_file(amuTest.sh.in          amuTest.sh @ONLY)
configure_file(amuTrackTest.sh.in     amuTrackTest.sh @ONLY)

configure_file(ireneSlurmPrefix.txt.in  ireneSlurmPrefix.txt @ONLY)
configure_file(ireneTest.sh.in          ireneTest.sh @ONLY)
configure_file(ireneTrackTest.sh.in     ireneTrackTest.sh @ONLY)

# MISC
configure_file(genYamlCTest.sh.in genYamlCTest.sh @ONLY)
configure_file(makeTags.sh        makeTags.sh     @ONLY)

