// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
// Copyright 2011, Frédéric Masset
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <numeric>
#include <cmath>
#include <iomanip>

#include <Kokkos_Core.hpp>
#include <Kokkos_MathematicalFunctions.hpp>

#include "allmpi.hpp"
#include "mpiinplace.hpp"

#include "hillForce.hpp"
#include "tuple.hpp"
#include "log.hpp"
#include "loops.hpp"
#include "gridDispatch.hpp"
#include "scalarField.hpp"
#include "gridCellSizes.hpp"
#include "cartesianGridCoords.hpp"
#include "h5log.hpp"

namespace Kokkos {
  template<>
  struct reduction_identity<fargOCA::HillForce> {
    KOKKOS_FORCEINLINE_FUNCTION static fargOCA::HillForce sum() {
      fargOCA::HillForce zero;
      zero.inner.complete     = {0,0,0};
      zero.inner.noHillSphere = {0,0,0};
      zero.outer.complete     = {0,0,0};
      zero.outer.noHillSphere = {0,0,0};
      return zero;
    }
  };
}

namespace fargOCA {
  namespace kk = Kokkos;
  namespace kkm = Kokkos::Experimental;

  GridSpacing constexpr ARTH = GridSpacing::ARITHMETIC;
  GridSpacing constexpr LOGR = GridSpacing::LOGARITHMIC;

#define LBD KOKKOS_LAMBDA
#ifdef NDEBUG
#  define NLBD [&]
#else
#  define NLBD [=]
#endif

  namespace details {
    // Although the planet mass is
    // given as an argument, this mass is used only to
    // specify the distance cutoff in the case of the Hill sphere avoidance.
    // The force returned is a specific force. It has therefore the dimension
    // of an acceleration (LT^-2).
    template<GridSpacing GS>
    arr1d<HillForce> localContribs(Disk const& disk, std::string name) {
      Planet const& planet = disk.system()->planet(name);
      real planet2Star = planet.position().norm();
      // When smoothing type is Flat we  consider that the smoothing lenght in units of disk scale height, default unit is the Hill radius
      real smoothingLengthUnit = (disk.physic().smoothing.flat
                                  ? disk.physic().aspectRatio(planet2Star)*planet2Star  // Flaring index is taken into account already in physic().aspectRatio 
                                  : disk.system()->HillRadius(planet));
      real rsmoothing          = disk.physic().smoothing.smooth(smoothingLengthUnit, disk.physicalTime());
      GridDispatch const& dispatch = disk.dispatch();
      range_t const managedRadii = dispatch.managedRadii();
      
      arr1d<HillForce> vf("local Hill contribs", size(managedRadii));
      real nan = std::numeric_limits<real>::quiet_NaN();
      
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const ls = coords.sizes();
      
      arr3d<real const> dens   = disk.density().data();
      arr3d<real const> volume = dispatch.cellSizes().volume();
      auto cartesian = dispatch.cartesian();
      auto radMed    = coords.radiiMed();
      
      real const rh       = disk.system()->HillRadius(planet);
      bool const halfDisk = disk.physic().shape.half;
      
      bool const flatSmoothing = disk.physic().smoothing.flat;
      kk::parallel_for(kk::TeamPolicy<>(size(managedRadii), kk::AUTO),
                       LBD(kk::TeamPolicy<>::member_type const& team) {
                         size_t const i = team.league_rank() + begin(managedRadii);
                         kk::parallel_reduce(kk::TeamThreadRange(team, halfDisk ? ls.ni - 1 : ls.ni),
                                             NLBD(size_t const h, HillForce& hForce) {
                                               HillForce jForce = kk::reduction_identity<HillForce>::sum();
                                               kk::parallel_reduce(kk::ThreadVectorRange(team, ls.ns),
                                                                   NLBD(size_t const j, HillForce& partial) {
                                                                     using kk::exp;
                                                                     real const cellMass      = volume(i,h,j)*dens(i,h,j);
                                                                     Triplet const posWrtCell = cartesian.coords(i,h,j) - planet.position();
                                                                     real const dist2Cell2    = posWrtCell.norm2();
                                                                     real const dist2Cell     = kk::sqrt(dist2Cell2);
                                                                     real const hillCut       = (rh > 0
                                                                                                 ? 1/(exp(-(dist2Cell-0.8*rh)/(0.08*rh))+1)
                                                                                                 : real(1));
                                                                     Triplet force(nan,nan,nan);
                                                                     if (flatSmoothing) {
                                                                       force = (G*cellMass*kk::pow(dist2Cell2 + ipow<2>(rsmoothing), -real(3)/2))*posWrtCell;
                                                                     } else {
                                                                       if(dist2Cell <=  rsmoothing){
                                                                         real potsmooth = 4-3*dist2Cell/rsmoothing;
                                                                         force = (G*cellMass*potsmooth/ipow<3>(rsmoothing))*posWrtCell;
                                                                       } else {
                                                                         force = (G*cellMass/ipow<3>(dist2Cell))*posWrtCell;
                                                                       }
                                                                     }
                                                                     if (radMed(i) < planet2Star) {
                                                                       partial.inner.complete     += force;
                                                                       partial.inner.noHillSphere += hillCut*force;
                                                                     } else {
                                                                       partial.outer.complete      += force;
                                                                       partial.outer.noHillSphere  += hillCut*force;
                                                                     }
                                                                 }, kk::Sum<HillForce>(jForce));
                                               kk::single(kk::PerThread(team), [&]() { hForce += jForce; });
                                             }, kk::Sum<HillForce>(vf(team.league_rank())));
                       });
      return vf;
    }
  }
  
  HillForce
  operator+(HillForce const& f1, HillForce const& f2) {
    HillForce result;
    result.inner.complete     = f1.inner.complete + f2.inner.complete;
    result.inner.noHillSphere = f1.inner.noHillSphere + f2.inner.noHillSphere;
    result.outer.complete     = f1.outer.complete + f2.outer.complete;
    result.outer.noHillSphere = f1.outer.noHillSphere + f2.outer.noHillSphere;
    return result;
  }

  void
  HillForce::operator*=(real f) {
    inner.complete     *= f;
    inner.noHillSphere *= f;
    outer.complete     *= f;
    outer.noHillSphere *= f;
  }

  HillForce::HillForce(Disk const& disk, std::string planet)
    : HillForce() {
    arr1d<HillForce> contribs;
    switch (disk.dispatch().grid().radialSpacing()) {
    case ARTH: contribs = details::localContribs<ARTH>(disk, planet); break;
    case LOGR: contribs = details::localContribs<LOGR>(disk, planet); break;
    }
    arr1d<HillForce>::HostMirror hcontribs = kk::create_mirror(contribs);
    kk::deep_copy(hcontribs, contribs);
    *this = fmpi::all_reduce(disk.dispatch().comm(), 
                             std::accumulate( hcontribs.data(), hcontribs.data()+hcontribs.size(),  HillForce()),
                             std::plus<HillForce>());
    if (disk.physic().shape.half) {
      *this *= 2;
    }
  }
  
  namespace details {
    template<GridSpacing GS>
    std::pair<arr1d<real>, arr1d<real>>
    GetTorqueContribs(Disk const& disk, std::string planet) {
      GridDispatch const& dispatch   = disk.dispatch();
      auto const& coords = dispatch.grid().as<GS>();
      GridSizes const ls =coords.sizes();
      range_t const managedRadii = dispatch.managedRadii();
      Triplet position = disk.system()->planet(planet).position();
      arr1d<HillForce> vf = localContribs<GS>(disk, planet);
      auto radii = coords.radii();
      
      arr1d<real> local("local", ls.nr);
      arr1d<real> localh("locals", ls.nr);
      bool const half = disk.physic().shape.half;
      size_t const nrMin = begin(managedRadii);
      size_t const nrMax = end(managedRadii);
      kk::parallel_for(kk::RangePolicy<>(nrMin, nrMax),
                       LBD(size_t const i) {
                         real const dr = radii(i+1)-radii(i);
                         local(i)  = ((position.x*(vf(i-nrMin).inner.complete.y+vf(i-nrMin).outer.complete.y)-position.y*(vf(i-nrMin).inner.complete.x+vf(i-nrMin).outer.complete.x))
                                      /dr);
                         localh(i) = ((position.x*(vf(i-nrMin).inner.noHillSphere.y+vf(i-nrMin).outer.noHillSphere.y)-position.y*(vf(i-nrMin).inner.noHillSphere.x+vf(i-nrMin).outer.noHillSphere.x))
                                      /dr);
                         if(half) {
                           local(i)  *= 2;
                           localh(i) *= 2;
                         }
                       });
      return std::make_pair(dispatch.joinManaged(local), dispatch.joinManaged(localh));
    }
  }

  std::pair<arr1d<real>, arr1d<real>>
  GetTorqueContribs(Disk const& disk, std::string planet) {
    switch (disk.dispatch().grid().radialSpacing()) {
    case ARTH: return details::GetTorqueContribs<ARTH>(disk, planet); break;
    case LOGR: return details::GetTorqueContribs<LOGR>(disk, planet); break;
    }
    std::abort();
  }

  void
  TimeStampedHillForce::writeH5Log(HighFive::Group root, std::string name, std::vector<TimeStampedHillForce> const& log) {
    return h5::writeLog(root, name, log);
  }
  
  void
  TimeStampedHillForce::writeH5Logs(HighFive::Group root, std::map<std::string, std::vector<TimeStampedHillForce>> const& logs) {
    return h5::writeLogs(root, logs);
  }
  
  std::vector<TimeStampedHillForce>
  TimeStampedHillForce::readH5Log(HighFive::Group root, std::string name) {
    return h5::readLog<TimeStampedHillForce>(root, name);
  }
  
  std::map<std::string, std::vector<TimeStampedHillForce>>
  TimeStampedHillForce::readH5Logs(HighFive::Group root) {
    return h5::readLogs<TimeStampedHillForce>(root);
  }
  
  std::ostream&
  operator<<(std::ostream& out, HillForce const& h) {
    out << "{in:{cpl:" << h.inner.complete 
        << ",nh:" << h.inner.noHillSphere 
        << "},out:{cpl:" <<  h.outer.complete
        << ",nh:" << h.inner.noHillSphere << "}";
    return out;
  }
}

template<>
HighFive::DataType
HighFive::create_datatype<fargOCA::HillForce::HillAreas>() {
  using namespace HighFive;
  using namespace fargOCA;
  static CompoundType ct({{"complete",       create_datatype<Triplet>()},
			  {"no_Hill_sphere", create_datatype<Triplet>()}});
  return ct;
}

template<>
HighFive::DataType
HighFive::create_datatype<fargOCA::HillForce>() {
  using namespace HighFive;
  static CompoundType ct({{"inner",    create_datatype<fargOCA::HillForce::HillAreas>()},
			  {"complete", create_datatype<fargOCA::HillForce::HillAreas>()}});
  return ct;
}

template<>
HighFive::DataType
HighFive::create_datatype<fargOCA::TimeStampedHillForce>() {
  using namespace HighFive;
  using namespace fargOCA;
  using fargOCA::real;
  static CompoundType ct({{"time",       create_datatype<real>()},
			  {"planet",     create_datatype<Planet>()},
			  {"Hill_force", create_datatype<HillForce>()}});
  return ct;
}
