// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGO_SIMULATION_H
#define FARGO_SIMULATION_H

#include <vector>
#include <map>
#include <optional>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <highfive/H5File.hpp>
#include <nlohmann/json_fwd.hpp>

#include "precision.hpp"
#include "allmpi.hpp"
#include "shared.hpp"
#include "fieldStack.hpp"
#include "cuda_workarounds.hpp"
#include "simulationConfig.hpp"
#include "simulationTrackingPlugin.hpp"
#include "simulationStepPlugin.hpp"
#include "momentum.hpp"
#include "pluginConfig.hpp"

namespace fargOCA {

  class Disk;
  class ScalarField;
  
  /// \brief Drive the evolution of a planetary disk through time.
  class Simulation
    : public std::enable_shared_from_this<Simulation> { 
  public:
    /// \brief Create a new simulation.
    /// \param disk the manipulated disk.
    /// \param tracking disk tracking callbacks configuration.
    /// \param odir output directory
    static std::shared_ptr<Simulation> make(Disk& disk, PluginConfig const& tracking, int nbSteps, real timeStep, std::string odir = ".", int step = 0);

    /// \brief Create a new simulation.
    /// \param root the HDF5 group where the simulation is stored.
    static std::shared_ptr<Simulation> make(fmpi::communicator const& comm, HighFive::Group root, std::string odir = ".");

    static std::shared_ptr<Simulation> makeFromPropertyTree(fmpi::communicator const& comm, std::string fname);

    std::shared_ptr<Simulation>       shared()       { return shared_from_this(); }
    std::shared_ptr<Simulation const> shared() const { return shared_from_this(); }

    /// \brief The manipulated disk.
    Disk&        disk()       { return *myDisk; }
    Disk const&  disk() const { return *myDisk; }
    
    template<GridSpacing GS> void velocityStep(arr3d<real const> systemPotential);
    void energyStep(real dt);
    template<GridSpacing GS> void transportStep(real dt);
    template<GridSpacing GS> void adiabaticStep();
    template<GridSpacing GS> void viscosityStep();
    template<GridSpacing GS> void planetAccretionStep();
    /// \brief Remove the gas accreted by the planets from the disk and transfert to accretedGas()
    template<GridSpacing GS> void removeAccretedGas(real dt);
    /// \brief Flush accretedGas() where required
    void applyAccretion();

    template<GridSpacing GS> void doStep();
    template<GridSpacing GS> void removePlanetAccretedGas(std::string planet, real dt);

    PluginConfig const&  trackingConfig() const { return myTrackingConfig; }
    
    /// \brief Initiate the simulation.
    void start();
    
    /// \brief Execute one time step of the simulaton.
    void doStep();

    /// \brief Current step in the simulation.
    int  step()              const { return myStep; }
    /// \brief Total number of steps in the whole simulation, regardless of restarts
    int  nbSteps()           const { return myNbSteps; }
    /// \brief The user specified time step.
    real userTimeStep()          const { return myUserTimeStep; }

    /// \brief The current simulation time step.
    /// It is only relevant between the begining of a step and the end of a step,
    /// and will not be defined otherwise.
    std::optional<real> computedTimeStep() const { return myComputedTimeStep; }    
    
    /// \brief Write simulation state in hdf5 group.
    /// \param root the state root on writer, std::nullopt everywhere else.
    void writeH5(std::optional<HighFive::Group> root, int writer=0, bool indirectFields = false) const;
    
    void dump(std::ostream& log) const;
    void dump() const;
    
    std::filesystem::path odir() const { return myODir; };
    /// \brief Produce an output file name.
    /// If ofile is an absolupe path or stats with "./", returns ofile.
    /// Otherwise returns simulation().odir()/ofile
    std::string opath(std::string ofile) const;
    
    /// \brief A planet->accreted gas momentum map.
    /// A binding is available for a given planet between the momment
    /// the matchin gas has been extracted and the momment it is consumed.
    std::map<std::string, Momentum>  accretedGas() const { return myAccretedGas; }
    
  private:
    void signal(SimulationEvent) const;
    void signal(SimulationStepEvent) ;
    void initRadialVelocity();
    void initPhiVelocity();
    void initThetaVelocity();
    
    Simulation(shptr<Disk>         disk,
               PluginConfig const& trackingConfig,
               int                 nbSteps,
               real                timeStep,
               std::string         odir,
               int                 step);
    Simulation(fmpi::communicator const& comm,
               HighFive::Group           root,
               std::filesystem::path odir);
    ~Simulation();

  private:
    shptr<Disk>                      myDisk;
    PluginConfig                     myTrackingConfig;
    std::filesystem::path            myODir;
    int                              myNbSteps;
    int                              myStep;
    real                             myUserTimeStep;
    std::optional<real>              myComputedTimeStep;    
    std::map<std::string, Momentum>  myAccretedGas;
  };

  void to_json(nlohmann::json& j, SimuConfigWrapper<Simulation> const& c);
  
  class DiskPhysic;
  class ScalarField;
  class Planet;
  
  void ComputeColumnDensity(DiskPhysic const& physic, ScalarField const& Rho, ScalarField& DeadDens);
}
#endif

