// Copyright 2018, Elena Lega,     elena.lega<at>oca.eu
// Copyright 2018, Clément Robert, clement.robert<at>oca.eu
// Copyright 2018, Aurélien Crida, aurelien.crida<at>oca.eu
// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <random>
#include <limits>
#include <algorithm>
#include <numeric>

#include <boost/lexical_cast.hpp>
#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "environment.hpp"
#include "scalarField.hpp"
#include "debug.hpp"
#include "loops.hpp"
#include "arrayTestUtils.hpp"

using namespace fargOCA;
namespace kk  = Kokkos;
namespace kkm = Kokkos::Experimental;
namespace hf  = HighFive;

std::string const FILE_NAME("test_grid.h5");

bool
test(GridDispatch const& grid,int seed) {
  /// we generate a random field, dump it, read it back, 
  // and make sure we get back the same values

  fmpi::communicator const& comm = grid.comm();
  
  auto const& coords = grid.grid();
  GridSizes const ls = coords.sizes();
  size_t const gnr = grid.grid().global().sizes().nr;
  
  /// Generate the global values of the field. We must generate the sames
  // values for all procs
  arr3d<real> dev("global_dev_values", gnr,ls.ni,ls.ns);
  {
    // generate global random grid values
    std::default_random_engine generator(seed);
    std::exponential_distribution<real> distribution(3.5);
    auto rand = std::bind( distribution, generator);
    auto host = kk::create_mirror_view(dev);
    for (int i=0; i < gnr; ++i) {
      for (int h=0; h < ls.ni; ++h) {
        for (int j=0; j < ls.ns; ++j) {
          host(i,h,j) = -42*rand() + 21;
        }
      }
    }
    kk::deep_copy(dev, host);
  }
  arr3d<real const> local_dev = grid.localView(dev);
  {
    // extract local section of grid
    ScalarField bob("bob", grid);
    kk::deep_copy(bob.data(), local_dev);
    h5::writeH5OverGrid(bob, FILE_NAME);
  }
  // Make sure everyone wait for the file to be written before reading it back
  comm.barrier();
  ScalarField bob("bob", grid);
  h5::readH5OverGrid(bob, FILE_NAME);
  arr3d<real const> fresh_data = bob.data();
  real max_diff = 0;
  kk::parallel_reduce("compare", fullRange(coords),
                      KOKKOS_LAMBDA(size_t const i, size_t const h, size_t const j, real& tmp) {
                        real const diff = kk::abs(fresh_data(i,h,j) - local_dev(i,h,j));
                        if (tmp < diff) { tmp = diff; }
                      }, max_diff);
  real tolerance = std::numeric_limits<real>::epsilon() * 10;
  std::cerr << "On proc " << comm.rank() << ", max_diff = " << max_diff;
  if (max_diff > tolerance) {
    std::cerr << ", > " << tolerance << " => FAILED\n";
    return false;
  } else {
    std::cerr << ", <= " << tolerance << " => PASSED\n";
    return true;
  }
}

int main(int argc, char* argv[]) {
  Environment env(argc, argv);
  fmpi::communicator world;
  using namespace fargOCA;
  size_t const globalNRad = argc == 1 ? 199u : boost::lexical_cast<size_t>(argv[1]);
  
  GasShape shape(1, 42, 56, 0.001, false);
  size_t const nSec = 33;
  size_t const nInc = 3;
  
  auto grid = GridDispatch::make(world, *Grid::make(shape, {globalNRad, nInc, nSec}, GridSpacing::ARITHMETIC ));
  
  int failed = 0;
  if (fmpi::all_reduce(grid->comm(), test(*grid, 42), std::logical_and<bool>())) {
    std::cout << "Min. PASSED\n";
  } else {
    std::cout << "Min. FAILED\n";
    ++failed;
  }
  
  return failed > 0 ? 1 : 0;
}
