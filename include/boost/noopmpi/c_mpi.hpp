// Copyright (C) 2020 Alain Miniussi <alain.miniussi -at- oca.eu>.

// Use, modification and distribution is subject to the Boost Software
// License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_NOOPMPI_C_MPI_HPP
#define BOOST_NOOPMPI_C_MPI_HPP

/** 
 * @file This file mimic the minimal C MPI implementatoin needed to
 * make sense to Boost.MPI
 */
#include "boost/noopmpi/config.hpp"


#if defined(MPI_UNDEFINED)
#error "Already got another (probably dummy) implementation of sequential MPI"
#endif 

namespace boost { namespace noopmpi {

class c_mpi;

struct MPI_NoOpComm {
  MPI_NoOpComm() = default;
};
    //typedef MPI_NoOpComm const* MPI_Comm;

struct MPI_NoOpGroup {
  MPI_NoOpGroup(bool isfull = false) : full(isfull) {}
  bool full;
};
typedef MPI_NoOpGroup const* MPI_Group;

class c_mpi {
public:
  static constexpr MPI_NoOpComm  world_comm  = {};
  static constexpr MPI_NoOpComm  graph_comm  = {};
  static constexpr MPI_NoOpComm  cart_comm   = {};
};

struct MPI_NoOpRequest {};
struct MPI_NoOpStatus {};

}}

#endif 
