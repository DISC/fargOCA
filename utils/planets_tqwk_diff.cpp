// Copyright 2023, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <sstream>
#include <optional>
#include <filesystem>

#include <sysexits.h>

#include "allmpi.hpp"
#include "boost/program_options.hpp"
#include "boost/format.hpp"
#include "boost/algorithm/string.hpp"

#include "environment.hpp"
#include "log.hpp"
#include "planetarySystem.hpp"
#include "disk.hpp"
#include "h5io.hpp"
#include "hillForce.hpp"
#include "simulation.hpp"

using namespace fargOCA;

namespace po = boost::program_options;
namespace fs = std::filesystem;
namespace HF = HighFive;

using std::optional;

void
read_cmd_line(int argc, char* argv[], po::variables_map& vm) {
  std::ostringstream usage;
  std::string cmd = fs::path(argv[0]).filename().native();
  usage << "Check the difference in planets log.\n"
        << '\t' << cmd << " <file1>.h5 <file2>.h5 [option]+ \nValid options are.\n";
  
  po::options_description visible(usage.str());
  visible.add_options()
    ("help,h", "This help message.")
    ("verbose", "Print more stuff.")
    ("ofile,o", po::value<std::string>()->default_value("planetsdiff.h5"), "if present, will contains the diff.")
    ("tolerance,t", po::value<std::string>()->default_value("1e-13:1e-13"), "absolute and relative tolerance on force vector norms")
    ;
  po::options_description hidden;
  hidden.add_options()
    ("file1", po::value<std::string>(), "HDF5 file containing the first  planetary system log.")
    ("file2", po::value<std::string>(), "HDF5 file containing the second planetary system log.");
  
  try {
    po::positional_options_description pos;
    pos.add("file1", 1).add("file2",1);

    po::options_description opts;
    opts.add(hidden).add(visible);
    po::store(po::command_line_parser(argc, argv).
              options(opts).positional(pos).run(), vm);
    po::notify(vm);
  } catch (po::unknown_option& unknown) {
    std::cout << unknown.what() << '\n' << "Usage:\n" << visible << '\n';
    std::exit(EX_USAGE);
  }
  if (bool(vm.count("help"))) {
    std::cout << visible;
    std::exit(EX_USAGE);
  } 
  for(std::string opt : {"file1", "file2"}) {
    if (vm.count(opt)==0) {
      std::cout << "missing mandatory option '" << opt << "'. \nusage:\n"
                << opt << '\n';
      std::exit(EX_USAGE);
    }
  }
}

std::pair<real, real>
parseTolerance(std::string str) {
  std::vector<std::string> tuple;
  boost::split(tuple, str, boost::is_any_of(":"));
  if (tuple.size() != 2) {
    std::cout << str << " is not a well formed tolerance specification, must be of the form <absolute tolerance>:<relative tolerance>.\n";
    std::exit(-1);
  }
  try {
    real abs = boost::lexical_cast<real>(tuple[0]);
    real rel = boost::lexical_cast<real>(tuple[1]);
    if (abs<0) {
      std::cout << tuple[0] << " must be positive.\n";
      std::exit(-1);
    }
    if (rel<0) {
      std::cout << tuple[1] << " must be positive.\n";
      std::exit(-1);
    }
    return std::make_pair(abs, rel);
  } catch (boost::bad_lexical_cast& e) {
    std::cout << str << " is not a correct tolerance specification, must use positive floating point.\n";
    std::exit(-1);
  }
}

int
main(int argc, char* argv[]) {
  po::variables_map vm;
  read_cmd_line(argc, argv, vm);
  
  Environment env(argc, argv);
  fmpi::communicator world;
  assert(world.size() == 1);
  
  std::string fname1 = vm["file1"].as<std::string>();
  std::string fname2 = vm["file2"].as<std::string>();

  auto logs1 = TimeStampedHillForce::readH5Logs(HF::File(fname1, HF::File::ReadOnly).getGroup("/"));
  auto logs2 = TimeStampedHillForce::readH5Logs(HF::File(fname2, HF::File::ReadOnly).getGroup("/"));

  bool good       = true; // prove me wrong
  bool compatible = true;
  if (logs1.size() != logs2.size()) {
    good       = false;
    compatible = false;
    std::cout << "File '" << fname1 << "' contains the log for " << logs1.size() 
              << "' planet when '" << fname2 << "' contains the logs for " << logs2.size() << "\n";
  }
  auto tol = parseTolerance(vm["tolerance"].as<std::string>());
  real const abstol = tol.first;
  real const reltol = tol.second;
  std::map<std::string, std::vector<TimeStampedHillForce>> diffLogs;
  
  for(auto const& [planet,log1] : logs1) {
    std::cout << "comparing log of planet '" << planet << "'\n";
    if (logs2.find(planet) == logs2.end()) {
      std::cout << "No log for '" << planet << "' in '" << fname2 << "'\n";
      good = false;
      compatible = false;
    } else {
      auto log2 = logs2.at(planet);
      if (log1.size() != log2.size()) {
        std::cout << "Logs for '" << planet << "' do not have the same size in both files (" 
                  << log1.size() << " vs " << log2.size() << ")\n";
        good = false;
        compatible = false;
      } else {
        int logSz = log1.size();
        std::cout << "Number of records: " << logSz << "'\n";
        if (logSz > 0) {
          std::vector<TimeStampedHillForce> diff(logSz);
          for(int i = 0; i < logSz; ++i) {
            HillForce const& f1 = log1[i].force;
            HillForce const& f2 = log2[i].force;
            diff[i] = { log1[i].time, log1[i].planet,
                        HillForce(f1.inner.complete     - f2.inner.complete,
                                  f1.inner.noHillSphere - f2.inner.noHillSphere,
                                  f1.outer.complete     - f2.outer.complete,
                                  f1.outer.noHillSphere - f2.outer.noHillSphere)};
            HillForce const& delta = diff[i].force;
            if (!(delta.inner.complete.norm()     <= abstol) &&
                !(delta.inner.noHillSphere.norm() <= abstol) && 
                !(delta.outer.complete.norm()     <= abstol) &&
                !(delta.outer.noHillSphere.norm() <= abstol)) {
              good = false;
              std::cout << "planet " << planet << ", idx " << i << ", time " << log1[i].time 
                        << ", abs. missmatch:\n\t" << f1 << "\n vs \n\t" << f2 << "\n <>\n\t " << delta << '\n';
              real const epsilon = std::numeric_limits<real>::epsilon();
              if (!(delta.inner.complete.norm()     <= reltol*(f1.inner.complete.norm()+epsilon)) &&
                  !(delta.inner.noHillSphere.norm() <= reltol*(f1.inner.noHillSphere.norm()+epsilon)) && 
                  !(delta.outer.complete.norm()     <= reltol*(f1.outer.complete.norm()+epsilon)) &&
                  !(delta.outer.noHillSphere.norm() <= reltol*(f1.outer.noHillSphere.norm()+epsilon))) {
                good = false;
                std::cout << "planet " << planet << ", idx " << i << ", time " << log1[i].time 
                          << ", abs. missmatch:\n\t" << f1 << "\n vs \n\t" << f2 << "\n <>\n\t" << delta << '\n';
              }
            }
          }
          diffLogs[planet] = std::move(diff);
        }
      }
    }
  }
  if (compatible) {
    if  (vm.count("ofile")) {
      std::string ofname = vm["ofile"].as<std::string>();
      HF::File ofile(ofname, HF::File::Overwrite);
      TimeStampedHillForce::writeH5Logs(ofile.getGroup("/"), diffLogs);
      std::cout << "Output written in '" << ofname << "'\n";
    }
  }
  
  std::string status = "NOT CLOSE ENOUGH";
  if (good) {
    status = "CLOSE ENOUGH";
  }
  std::cout << status << '\n';
  return good ? 0 : -1;
}
