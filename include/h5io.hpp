// Copyright 2018, Alain Miniussi, alain.miniussi<at>oca.eu
//
// This file is part of FargOCA.
//
// FargOCA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.

// FargOCA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FargOCA.  If not, see <https://www.gnu.org/licenses/>.

#ifndef FARGOCA_H5IO_H
#define FARGOCA_H5IO_H

#include <vector>
#include <string>
#include <map>
#include <type_traits>
#include <optional>
#include <sstream>
#include <iostream>
#include <sysexits.h>

#include <highfive/H5File.hpp>

#include "allmpi.hpp"
#include "precision.hpp"
#include "arrays.hpp"

/// \file IO factorization header, should only be used in implemetation files.

namespace fargOCA {
  struct Triplet;
  struct GridPositions;
  enum class DiskReferential : int;
  enum class DiskReferentialV0 : int;
 
  /// \brief A list of possible enum values.
  /// Must be explicitly specialized for each type
  template<typename Enum> std::vector<Enum> const& enumValues();  

  namespace h5 {
    struct FormatVersion {
      int major, minor;
      
      void write(HighFive::Group& root) const;
      static std::optional<FormatVersion> read(HighFive::Group const& root);
      static FormatVersion current();
    };
    inline bool 
    operator<(FormatVersion const& v1, FormatVersion const& v2) {
      return v1.major < v2.major || (v1.major == v2.major && v1.minor < v2.minor);
    }
    inline bool
    operator>(FormatVersion const& v1, FormatVersion const& v2) { return v2<v1; }

    inline bool
    operator==(FormatVersion const& v1, FormatVersion const& v2) {
      return v1.major == v2.major && v1.minor == v2.minor;
    }
    inline bool
    operator!=(FormatVersion const& v1, FormatVersion const& v2) { return !(v1==v2);}

    std::ostream& operator << ( std::ostream& out, FormatVersion const& v);
    
    static std::string const RADIUS_POSITION = "radius_position";
    static std::string const LAYER_POSITION  = "layer_position";
    static std::string const SECTOR_POSITION = "sector_position";
    static std::string const POLAR_GRID      = "polar_grid";

    template<typename T> std::optional<T> attributeValue(HighFive::Group const& obj, std::string const& name);

    /// \brief Create file if cond holds.
    std::optional<HighFive::File> createFile(std::string fname, bool cond);

    /// \brief If bool(file),  get the root
    std::optional<HighFive::Group> getRoot(std::optional<HighFive::File>& file);

    /// \brief Return the file root group.
    /// Will crash if fname does not identify an HDF5 archive
    HighFive::Group getRoot(std::string fname, unsigned int mode = HighFive::File::ReadOnly );
    
    template<class S>
    void
    createH5(S const& subject, fmpi::communicator const& comm, std::string fname, int writer = 0) {
      assert(writer < comm.size());
      auto file = createFile(fname, comm.rank() == writer);
      subject.writeH5(getRoot(file), writer);
    }

    template<class S>
    void
    createH5(std::shared_ptr<S> subject, fmpi::communicator const& comm, std::string fname, int writer = 0) {
      createH5(*subject, comm, fname, writer);
    }

    template<class S>
    void readH5Fix(S& subject, HighFive::File const& f) {
      subject.readH5(f.getGroup("/"));
    }
    template<class S>
    void readH5Fix(std::shared_ptr<S> subject, HighFive::File const& f) {
      subject->readH5(f.getGroup("/"));
    }

    /// \brief Create a new file and write object in it specifying a writer.
    /// \param fname the name of the file. Will be truncated if necessary.
    /// \param subject the object to be written. Must be distributed on its existing grid attribute.
    /// \param writer the rank of the writer
    template<class S> void writeH5OverGrid(S const& subject, std::string fname, unsigned int mode = HighFive::File::Overwrite, int writer = 0);
    
    /// \brief Read an object distributed over a grid from an HDF5 file.
    /// \param fname the name of the file. Will be truncated if necessary.
    /// \param subject the object to be loaded. Must be distributed on its existing grid attribute.
    /// \param reader the rank of the writer
    template<class S> void readH5OverGrid(S& subject, std::string fname);
    
    /// \brief Create a sub group, possibly removing existing one.
    /// \param force if true and group exists, unlink first, crash otherwise
    HighFive::Group
    createSubGroup(HighFive::Group parent, std::string name);

    /// \brief Create a sub group if parent group exist.
    /// Typically used in collective write, where only one writer must create the group.
    /// \param parent the parent group, sub group is created if parent exist.
    /// \param name sub group name
    /// \param force unlink existing group
    /// \returns the created group is parent existed
    std::optional<HighFive::Group>
    createSubGroup(std::optional<HighFive::Group> parent, std::string name);
    
    namespace details {
      
      template<typename Enum>
      HighFive::DataType
      buildHFEnumDesc() {
        std::vector<typename HighFive::EnumType<Enum>::member_def> desc;
        for (auto const& v : enumValues<Enum>()) {
          std::ostringstream fmt;
          fmt << v;
          desc.push_back({fmt.str(), v});
        }
        return HighFive::EnumType<Enum>(desc);
      }
      
    }
    /// \brief Write an attribute in a group
    template<typename T>
    inline
    void
    writeAttribute(HighFive::Group& obj, std::string const& name, T const& value) {
      if (obj.hasAttribute(name)) {
        obj.deleteAttribute(name);
      }
      obj.createAttribute(name, value);
    }
    
    template<typename T>
    void writeAttribute(HighFive::Group& obj, std::string const& name, std::optional<T> value) {
      if (value) { writeAttribute(obj, name, *value); }
    }

    inline
    void
    writeAttribute(HighFive::Group& obj, std::string const& name, bool value) {
      writeAttribute(obj, name, int(value));
    }
    
    /// \brief Read attribute from group.
    /// Will crash with a hopefully helpfull message if the expected attribute is not there.
    template <typename T> void  readAttribute(HighFive::Group const& obj, std::string const& name, T& value);
    template <> void readAttribute<std::string>(HighFive::Group const& obj, std::string const& name, std::string& value);
    template <> void readAttribute<bool>(HighFive::Group const& obj, std::string const& name, bool& value);

    template<typename T>
    void
    readAttribute(HighFive::Group const& obj, std::string const& name, std::optional<T>& opt) {
      if (obj.hasAttribute(name)) {
        opt = attributeValue<T>(obj, name);
      } else {
        opt = std::nullopt;
      }
    }
    template<> void readAttribute<DiskReferential>(HighFive::Group const& obj, std::string const& name, std::optional<DiskReferential>& opt);
    template<> void readAttribute<bool>(HighFive::Group const& obj, std::string const& name, std::optional<bool>& opt);
    template<> void readAttribute<std::string>(HighFive::Group const& obj, std::string const& name, std::optional<std::string>& opt);

    template <typename T>
    void
    readAttribute(HighFive::Group const& obj, std::string const& name, T& value) {
      std::optional<T> buf;
      readAttribute<T>(obj, name, buf);
      value = std::move(*buf);
    }
    
    template<typename T>
    std::optional<T> attributeValue(HighFive::Group const& obj, std::string const& name) {
      if (obj.hasAttribute(name)) {
        return obj.getAttribute(name).read<T>();
      } else {
        return std::nullopt;
      }
    }

    template<> std::optional<std::string> attributeValue(HighFive::Group const& obj, std::string const& name);
    
    template<typename T>
    T attributeValue(HighFive::Group const& obj, std::string const& name, T const& def) {
      std::optional<T> res = attributeValue<T>(obj, name);
      return bool(res) ? *res : def;
    }
    
    /// \brief Convenience short cut for model IO.
    /// Load an object from a HDF5 group.
    /// Same as obj.readH5(*openSubGroup(parent, grpName))
    template<class T>
    void
    loadDispatched(T& obj, HighFive::Group const& parent, std::string grpName) {
      obj.readH5(parent.getGroup(grpName));
    }
    
    void writePolarGrid(HighFive::Group group, arr3d<real const> grid, GridPositions const& pos);
  }
}

#endif 
