!**********************************************************************
!		      SWIFT_SYMBA7.F
!**********************************************************************
!
!                 To run, need 2 input files. The code prompts for
!                 the file names, but examples are :
!
!                   parameter file like       param.in
!		    planet file like          pl.in
!
!  NOTE:  No test particles in this code and the massive bodies 
!         are dimensioned at NTPMAX
!
! Authors:  Hal Levison \& Martin Duncan
! Date:    11/21/96
! Last revision: 12/27/96

      program test_symba7
      implicit NONE             ! you got it baby
      
!...  Maximum array size
      integer  NTPMAX
      parameter  (NTPMAX = 1001) ! max number of test particles
      
      real*8 mass(NTPMAX),j2rp2,j4rp4
      real*8 xh(NTPMAX),yh(NTPMAX),zh(NTPMAX)
      real*8 vxh(NTPMAX),vyh(NTPMAX),vzh(NTPMAX)
      
      integer ntp
      
      integer nbod,i1st,i,nbodm,nbodo
      
      real*8 t0,tstop,dt
      real*8 t,eoff
      real*8 rpl(NTPMAX),rhill(NTPMAX)
      
      real*8 rmin,rmax,rmaxu,qmin,mtiny
      logical*2 lclose 
      integer isenc
      integer mergelst(2,NTPMAX),mergecnt
      integer*2 iecnt(NTPMAX)
      
!...  Executable code 
      
      ntp = 0
      
! param.in
! 0.0 100.0 0.25
      t0 = 0.
      tstop = 100.0
      dt = 0.25D0
! F T F T T F flags, already proagated
! -1. 1000. -1. -1. F
      rmin = -1.
      rmax = 1000.
      rmaxu = -1.
      qmin  = -1.
      lclose = .false.

! Test input file
! 3
      nbod = 3
! 1 
      mass(1) = 1 
!     Since !flag(5) :
      j2rp2 = 0.  
      j4rp4 = 0.  
!     by laws of nature
      rpl(1) = 0.
      rhill(1) = 0.
!     Sun pos 
!     0  0  0
      xh(1) = 0.
      yh(1) = 0.
      zh(1) = 0.
!     0  0  0
      vxh(1) = 0.
      vyh(1) = 0.
      vzh(1) = 0.

!     1.E-4  0.03218297949  0.001
      mass(2) = 1.d-4
      rhill(2) = 0.03218297949
      rpl(2) = 0.001

!     1  0  0
      xh(2) = 1.
      yh(2) = 0.
      zh(2) = 0.

!     0  1  0
      vxh(2) = 0.
      vyh(2) = 1.
      vzh(2) = 0.

!     1.E-4  0.04435589451  0.001
      mass(3) = 1.0d-4
      rhill(3) = 0.04435589451D+0
      rpl(3) = 0.001D+0

!     1.3782407725  0  0
      xh(3) = 1.3782407725D+0 
      yh(3) = 0.
      zh(3) = 0.

!     0  0.8517996421  0
      vxh(3) = 0.
      vyh(3) = 0.8517996421D+0
      vzh(3) = 0.

      mtiny = 1.d-8

! Initialize initial time and times for first output and first dump
      t = t0

      call io_write_frame_r(t0,nbod,ntp,mass,xh,yh,zh,vxh,vyh,vzh)

!...  Calculate the location of the last massive particle
      call symba7_nbodm(nbod,mass,mtiny,nbodm)

      i1st = 0

      do while ( (t .le. tstop) .and. (nbod.gt.1) )
         call symba7_step_pl(i1st,t,nbod,nbodm,mass,j2rp2,j4rp4,
     &        xh,yh,zh,vxh,vyh,vzh,dt,lclose,rpl,isenc,
     &        mergelst,mergecnt,iecnt,eoff,rhill,mtiny)
         t = t + dt
         nbodo = nbod
         call discard_massive5(t,dt,nbod,mass,xh,yh,zh,
     &        vxh,vyh,vzh,rmin,rmax,rmaxu,qmin,lclose,
     &        rpl,rhill,isenc,mergelst,mergecnt,
     &        iecnt,eoff,i1st)
         if(nbodo.ne.nbod) then
            call symba7_nbodm(nbod,mass,mtiny,nbodm)
         endif

         call  io_write_frame_r(t,nbod,ntp,mass,xh,yh,zh,vxh,
     &        vyh,vzh)
         
      enddo
      call util_exit(0)
      end program
